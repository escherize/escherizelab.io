---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

### Hello!

My name is Bryan Maass, I am a software engineer currently living in Austin Texas.

I have an *insatiable* need to build cool things. Checkout the projects on my homepage for an incomplete sample.

### I've lived in some fun places, like:

- Los Angeles, Ca
- San Francisco, Ca
- Sydney, Australia

### Plus,

- I'm extremely loyal to my friends
- I am into woodworking
- I like bowling
