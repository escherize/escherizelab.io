# Hey!

I have a whole data driven workflow to create this thing, using [edn](https://github.com/edn-format/edn) and latex templating which I'll document someday.

### Anyways

Here's the pdf of my [resume](BryanMaass.pdf).

And here it is inline (which looks really weird right?? :smile: ):

<embed src="BryanMaass.pdf" type="application/pdf" width="100%" height="600px" />
