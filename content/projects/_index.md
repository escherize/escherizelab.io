# PROJECTS
## Here are some things I built:

### [Tracks](https://github.com/escherize/tracks")
- Clojure(Script) library to manipulate the shape of data.
- Over 100 :star:s

### [Looper](../looper)
- A graph manipulation toy, language + viewer

### [Restructure](https://github.com/escherize/restructure)
- Clojure/script library for simple and declarative datastructure manipulation.

### [Blindfold](../blindfold)
- A prototype for customizing weighted average with your preferences

### [CLJS Fiddle](../cljsfiddle")
- Bootstrapped reagent playground which saves gists on github.

### [Color Explorer](../colors)
- Check out `Hyper Mode`: Look in the upper left area and then move your mouse over the color buttons

### [Transpose](../transpose)
- A card game for two players that seemed like it would work better on computer."
- Built in Vietnam

### [Catan Dice Roller 2](../catan)
- An app I wrote one afternoon after having a long Catan session with crooked dice."

### [Hiccup space](../hiccup.space)
- Simple example of html as data.

<hr>

... and a whole lot more that didn't make the cut!
