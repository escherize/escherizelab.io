---
layout: post
title: Side Effects Aren't Legos
subtitle: Don't mix side-effects with pure code
date: "2018-11-14"
---

A `referentially transparent` bit of code can be replaced with the value it produces without altering the behavior of our program.<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup> Approaching problems using them makes programming an elegant process of building up transformations like Legos until you have a rad logical structure.

Like Legos, each function call is directly and concretely connected to the piece above and below it.

Also like Legos, mixing non-Lego-like objects tends to make building things much more difficult. In progrmaming terms, mixing referentially transparent expressions with side-effecting code hinders our ability to produce high quality programs.

In this installment of the <span class="underline">Side Effects Aren't Legos</span> series we will investigate how mixing Legos with, say, oranges or spaghetti makes it more difficult with respect to testing.


# Testing

Here's a simple example, to show why you should try to separate side effects from pure data transformations.

Check it out:
#### `Sample A:`

{{< highlight clojure  >}}
(defn my-print [s path]
  (spit path (str "I was given: " s)))
{{< / highlight >}}

`Sample A` defines a function called `my-print`, which takes a string `s`, and a file `path`, and writes `s` with `"I was given: "` prepended to it onto the file at `path`.

How would you test it?

In the real world,the data transformations our program makes are typically the tricky bits that we want to test. So here, we would want to check that "I was given: " is correctly appended to s.


## How to test it:

-   Construct a path
    -   Ensure the path's directory exists, or make it
    -   Ensure there's no file at path
        -   Delete the file if there is one
-   Call `my-print` on a string like "abc"
-   Test that the file at path's contents equals `"I printed: abc"`
-   Finally, remove the file at path, and whatever directories it needed

This test is pretty complicated - also since it is safe to assume clojure.core/spit works, we are introducing unnecessary complexity. We only care about ensuring the proper transformation was made on our string (`s`). However since we are testing a side effect at the same time, there is maybe 7x as much work.

Forgive me for not writing a code sample to do that. :)


## Lego Mode:

Instead, let's snap together 2 functions to handle this.

Compare `Sample A` with:

{{< highlight clojure  >}}
(defn my-transform [s] (str "I was given: " s))

(defn my-print [s path] (spit path s))
{{< / highlight >}}

1.  Call `my-transform` on a string (say "abc")
2.  Test the return value is "I was given: "abc"

Now, just test that my-transform works!

{{< highlight clojure  >}}
(deftest my-transform-test
    (is (= "I printed: abc" (my-transform "abc"))))
{{< / highlight >}}

Note that when testing my-transform we are free to use a string generator, without adding too much "thought overhead".

{{< highlight clojure  >}}
(defn generate-string []
  (->> "abcdef" shuffle (apply str)))

;; can be run once with a generated string:
(deftest one-my-transform-test
  (let [a-string (generate-string)]
    (is (= (str "I was given: " a-string)
           (my-transform a-string)))))

;; or a thousand times
(deftest one-thousand-my-transform-test
  (let [strings (repeatedly 1000 generate-string)]
    (doseq [s strings]
      (is (= (my-transform s)
             (str "I was given: " s))))))
{{< / highlight >}}

## Extra Credit

Even better is to make my-print take a function to apply on s. Then we can pass in any pure function, and have the side-effect separated by design.

{{< highlight clojure  >}}
(defn my-print [s path & [f]]
  (let [f (or f identity)]
    (spit path (f s))))
{{< / highlight >}}
## Conclusion

Avoid mixing side effecting code into the beautiful world of data driven development!

## Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> John C. Mitchell (2002). Concepts in Programming Languages. Cambridge University Press. p. 78
