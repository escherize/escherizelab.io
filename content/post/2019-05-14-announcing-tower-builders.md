---
layout: post
title: Realtime multiplayer board game in Clojure
date: "2019-05-14"
truncated: true
summary: I released a Santorini Clone!
---

I'm pleased to announce the release of a game I've been working on for a while: https://buildtowers.club, where you can play [Santorini](https://boardgamegeek.com/boardgame/194655/santorini).

## More Details

This app was built with Clojure and ClojureScript, and benefits from: functional programming, immutability, and the use of the polymorphic features.

The game rules were written once. Notice though - in local multiplayer mode are executed on the client, and when playing against an opponent online the move commands are sent over a websocket, executed on the server, and relayed to the clients in realtime.

I hope you enjoy it!
