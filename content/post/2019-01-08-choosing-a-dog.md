---
layout: post
title: Choosing a dog breed the 'Hard Way'
subtitle: "Linear combinations can make life easier"
date: "2019-01-08"
---

# How did you choose what dog breed to get?

When my fiance and I decided to get a puppy, we didn't know much about dogs, dog breeds, or how or much work it would be!

So I decided to do some hacking.

1. Gather data about dog breeds.
2. Use that information to figure out what breeds worked best for our life.
3. ???
4. Profit.

### How to gather this data?

I could have spent 10 hours or hired someone to do [this...](https://www.freelancer.com.ar/projects/data-entry/researching-dog-breed-data-complete/)

But instead I scraped 198 pages of a dog website that rates 50 dog breed attributes. Next I tossed the results into a [dog csv](/dogs.csv) where each row is a dog breed, and every column is an attribute!

### How to analyze this data

What I really wanted was to say was:

> "I care about `columnA` at weight 5, `columnD` at weight 8, and `columnG` at weight 2"

and then recieve a ranking of dog breeds to investigate.

This is do-able in Excel and Google Sheets, by adding a column to the right, which is a function of the table itself.

Every cell in the column should have a function that's something like:

``` python
=A1 * 5 + A4 * 8 + A6 * 2
```

That can be acheived by filling the first cell of the new column with your function, then double clicking the handle on the bottom right to fill the column with this formula. Next, sort on that column and you get back the ranking.

<!-- ### A better way -->

# How'd it turn out?

The dog we decided on was in the top 10 of dog breeds that we ranked according to our preferences!

Number 1 was a `Norfolk Terrier` which we couldn't find anywhere.

Afterall we went with a Golden Retriever + Poodle mix and we named him:

## Jingo

![Doggie](/doggie.jpeg)
