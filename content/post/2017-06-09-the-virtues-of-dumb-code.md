---
layout: post
title: The Virtures of Dumb Code
excerpt: make your code as unsophisticated as possible
date: "2017-06-09"
---

I advocate making your code as unsophisticated as possible.

## What is dumb code?
Dumb code is code that is understandable for your audience without being reasoned about too much. A key feature is [simplicity](https://github.com/matthiasn/talk-transcripts/blob/master/Hickey_Rich/SimpleMadeEasy.md).

The best thing to happen to programming is that people started using JSON. Like a cave painting that explains where to hunt, data at rest is the dumbest, simplest artifact you can generate or work with.

As children, we studied these:

```
f(x) = 3
f(y) = y + 1
f(y, z) = y * z + 1
f(y) = square_root(y)
```

These types of functions should be the foundation for your software project. They have three important properties.

![](https://s3.us-east-2.amazonaws.com/photoblobs/Side_effects__Clojure_Practicalli_2017-06-09_12-55-22.png "Effects")

1. Pure

Pureness means that the same input will always give the same output.

2. No Side Effects

This function is only being run for the output it returns, and does not change anything.  Changing anything means printing, setting variables, updating databases, or using api calls. Even modifying a variable that was passed into your function is a side effect.

3. No Side Causes

A side cause is less commented on, but is very important for keeping your code dumb.
Consider:

```
x = -1

def magic-multiply (y):
  return x * y;

magic-multiply(10)
# => -10

x = 0

magic-multiply(10)
# => 0
```
Notice `magic-multiply` has no side-effects. But clearly it has side-causes. The value of x will determine the output of `magic-multiply` as much as `magic-multiply`'s own input would. However it may be pure (provided its side causes stay the same).

Dumb code has less moving parts, and is therefore more obvious. Some argue that it is only more obvious to _beginners_, but when taking a break from a codebase, and lacking the nessecary context, experts are beginners again.

## Why Dumb code?
The problem with complex, intertwined code is that [complexity does not add up linearly](http://scattered-thoughts.net/blog/2015/10/25/complexity-budgets/). The number of channels of communication for a strongly connected group of n systems is n * (n-1)/2 which is O(n^2). Even if the systems are not completely connected (the more realistic case), the number of communication channels increases much faster than 1:1. Because the problem space increases in size so energetically, opportunities for complexity reduction are lost. When a system is too complicated to fit into one debugger's head, hints about what could be going wrong will dlack meaning to her. Explaination and inspection of dumb code is much more accessable. When dealing with pure functions and a data goes in, data goes out approach there are not many areas for the reader to be confused.

# Call to action
Please consider the poor souls who will program and debug in your wake before adding a new stateful interconnected piece of code.

### Sources
* [Rich Hickey - Simple Made Easy (transcript)](https://github.com/matthiasn/talk-transcripts/blob/master/Hickey_Rich/SimpleMadeEasy.md)
* [Clojure.practical.li](https://practicalli.github.io/clojure/thinking-functionally/side-effects.html)
* [Complexity Budgets](http://scattered-thoughts.net/blog/2015/10/25/complexity-budgets/)
* [Out of the Tar Pit](http://shaffner.us/cs/papers/tarpit.pdf)
