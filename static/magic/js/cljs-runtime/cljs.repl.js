goog.provide('cljs.repl');
goog.require('cljs.core');
goog.require('cljs.spec.alpha');
goog.require('goog.string');
goog.require('goog.string.format');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__29754){
var map__29755 = p__29754;
var map__29755__$1 = (((((!((map__29755 == null))))?(((((map__29755.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__29755.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__29755):map__29755);
var m = map__29755__$1;
var n = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29755__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29755__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["-------------------------"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (){var or__4131__auto__ = new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return [(function (){var temp__5735__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5735__auto__)){
var ns = temp__5735__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})(),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('');
}
})()], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Protocol"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__29771_30156 = cljs.core.seq(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__29772_30157 = null;
var count__29773_30158 = (0);
var i__29774_30159 = (0);
while(true){
if((i__29774_30159 < count__29773_30158)){
var f_30162 = chunk__29772_30157.cljs$core$IIndexed$_nth$arity$2(null,i__29774_30159);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_30162], 0));


var G__30164 = seq__29771_30156;
var G__30165 = chunk__29772_30157;
var G__30166 = count__29773_30158;
var G__30167 = (i__29774_30159 + (1));
seq__29771_30156 = G__30164;
chunk__29772_30157 = G__30165;
count__29773_30158 = G__30166;
i__29774_30159 = G__30167;
continue;
} else {
var temp__5735__auto___30168 = cljs.core.seq(seq__29771_30156);
if(temp__5735__auto___30168){
var seq__29771_30169__$1 = temp__5735__auto___30168;
if(cljs.core.chunked_seq_QMARK_(seq__29771_30169__$1)){
var c__4550__auto___30170 = cljs.core.chunk_first(seq__29771_30169__$1);
var G__30171 = cljs.core.chunk_rest(seq__29771_30169__$1);
var G__30172 = c__4550__auto___30170;
var G__30173 = cljs.core.count(c__4550__auto___30170);
var G__30174 = (0);
seq__29771_30156 = G__30171;
chunk__29772_30157 = G__30172;
count__29773_30158 = G__30173;
i__29774_30159 = G__30174;
continue;
} else {
var f_30176 = cljs.core.first(seq__29771_30169__$1);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_30176], 0));


var G__30177 = cljs.core.next(seq__29771_30169__$1);
var G__30178 = null;
var G__30179 = (0);
var G__30180 = (0);
seq__29771_30156 = G__30177;
chunk__29772_30157 = G__30178;
count__29773_30158 = G__30179;
i__29774_30159 = G__30180;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_30181 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__4131__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([arglists_30181], 0));
} else {
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first(arglists_30181)))?cljs.core.second(arglists_30181):arglists_30181)], 0));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Special Form"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.contains_QMARK_(m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
} else {
return null;
}
} else {
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Macro"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["REPL Special Function"], 0));
} else {
}

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__29808_30190 = cljs.core.seq(new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__29809_30191 = null;
var count__29810_30192 = (0);
var i__29811_30193 = (0);
while(true){
if((i__29811_30193 < count__29810_30192)){
var vec__29859_30194 = chunk__29809_30191.cljs$core$IIndexed$_nth$arity$2(null,i__29811_30193);
var name_30195 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29859_30194,(0),null);
var map__29862_30196 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29859_30194,(1),null);
var map__29862_30197__$1 = (((((!((map__29862_30196 == null))))?(((((map__29862_30196.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__29862_30196.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__29862_30196):map__29862_30196);
var doc_30198 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29862_30197__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_30199 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29862_30197__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_30195], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_30199], 0));

if(cljs.core.truth_(doc_30198)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_30198], 0));
} else {
}


var G__30205 = seq__29808_30190;
var G__30206 = chunk__29809_30191;
var G__30207 = count__29810_30192;
var G__30208 = (i__29811_30193 + (1));
seq__29808_30190 = G__30205;
chunk__29809_30191 = G__30206;
count__29810_30192 = G__30207;
i__29811_30193 = G__30208;
continue;
} else {
var temp__5735__auto___30209 = cljs.core.seq(seq__29808_30190);
if(temp__5735__auto___30209){
var seq__29808_30213__$1 = temp__5735__auto___30209;
if(cljs.core.chunked_seq_QMARK_(seq__29808_30213__$1)){
var c__4550__auto___30214 = cljs.core.chunk_first(seq__29808_30213__$1);
var G__30215 = cljs.core.chunk_rest(seq__29808_30213__$1);
var G__30216 = c__4550__auto___30214;
var G__30217 = cljs.core.count(c__4550__auto___30214);
var G__30218 = (0);
seq__29808_30190 = G__30215;
chunk__29809_30191 = G__30216;
count__29810_30192 = G__30217;
i__29811_30193 = G__30218;
continue;
} else {
var vec__29877_30219 = cljs.core.first(seq__29808_30213__$1);
var name_30220 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29877_30219,(0),null);
var map__29880_30221 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29877_30219,(1),null);
var map__29880_30222__$1 = (((((!((map__29880_30221 == null))))?(((((map__29880_30221.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__29880_30221.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__29880_30221):map__29880_30221);
var doc_30223 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29880_30222__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_30224 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29880_30222__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_30220], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_30224], 0));

if(cljs.core.truth_(doc_30223)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_30223], 0));
} else {
}


var G__30238 = cljs.core.next(seq__29808_30213__$1);
var G__30239 = null;
var G__30240 = (0);
var G__30241 = (0);
seq__29808_30190 = G__30238;
chunk__29809_30191 = G__30239;
count__29810_30192 = G__30240;
i__29811_30193 = G__30241;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5735__auto__ = cljs.spec.alpha.get_spec(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name(n)),cljs.core.name(nm)));
if(cljs.core.truth_(temp__5735__auto__)){
var fnspec = temp__5735__auto__;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));

var seq__29887 = cljs.core.seq(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__29888 = null;
var count__29889 = (0);
var i__29890 = (0);
while(true){
if((i__29890 < count__29889)){
var role = chunk__29888.cljs$core$IIndexed$_nth$arity$2(null,i__29890);
var temp__5735__auto___30252__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5735__auto___30252__$1)){
var spec_30254 = temp__5735__auto___30252__$1;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_30254)], 0));
} else {
}


var G__30258 = seq__29887;
var G__30259 = chunk__29888;
var G__30260 = count__29889;
var G__30261 = (i__29890 + (1));
seq__29887 = G__30258;
chunk__29888 = G__30259;
count__29889 = G__30260;
i__29890 = G__30261;
continue;
} else {
var temp__5735__auto____$1 = cljs.core.seq(seq__29887);
if(temp__5735__auto____$1){
var seq__29887__$1 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(seq__29887__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__29887__$1);
var G__30262 = cljs.core.chunk_rest(seq__29887__$1);
var G__30263 = c__4550__auto__;
var G__30264 = cljs.core.count(c__4550__auto__);
var G__30265 = (0);
seq__29887 = G__30262;
chunk__29888 = G__30263;
count__29889 = G__30264;
i__29890 = G__30265;
continue;
} else {
var role = cljs.core.first(seq__29887__$1);
var temp__5735__auto___30271__$2 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5735__auto___30271__$2)){
var spec_30273 = temp__5735__auto___30271__$2;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_30273)], 0));
} else {
}


var G__30274 = cljs.core.next(seq__29887__$1);
var G__30275 = null;
var G__30276 = (0);
var G__30277 = (0);
seq__29887 = G__30274;
chunk__29888 = G__30275;
count__29889 = G__30276;
i__29890 = G__30277;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Constructs a data representation for a Error with keys:
 *  :cause - root cause message
 *  :phase - error phase
 *  :via - cause chain, with cause keys:
 *           :type - exception class symbol
 *           :message - exception message
 *           :data - ex-data
 *           :at - top stack element
 *  :trace - root cause stack elements
 */
cljs.repl.Error__GT_map = (function cljs$repl$Error__GT_map(o){
var base = (function (t){
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),(((t instanceof cljs.core.ExceptionInfo))?new cljs.core.Symbol(null,"ExceptionInfo","ExceptionInfo",294935087,null):(((t instanceof EvalError))?new cljs.core.Symbol("js","EvalError","js/EvalError",1793498501,null):(((t instanceof RangeError))?new cljs.core.Symbol("js","RangeError","js/RangeError",1703848089,null):(((t instanceof ReferenceError))?new cljs.core.Symbol("js","ReferenceError","js/ReferenceError",-198403224,null):(((t instanceof SyntaxError))?new cljs.core.Symbol("js","SyntaxError","js/SyntaxError",-1527651665,null):(((t instanceof URIError))?new cljs.core.Symbol("js","URIError","js/URIError",505061350,null):(((t instanceof Error))?new cljs.core.Symbol("js","Error","js/Error",-1692659266,null):null
)))))))], null),(function (){var temp__5735__auto__ = cljs.core.ex_message(t);
if(cljs.core.truth_(temp__5735__auto__)){
var msg = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"message","message",-406056002),msg], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = cljs.core.ex_data(t);
if(cljs.core.truth_(temp__5735__auto__)){
var ed = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),ed], null);
} else {
return null;
}
})()], 0));
});
var via = (function (){var via = cljs.core.PersistentVector.EMPTY;
var t = o;
while(true){
if(cljs.core.truth_(t)){
var G__30302 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(via,t);
var G__30303 = cljs.core.ex_cause(t);
via = G__30302;
t = G__30303;
continue;
} else {
return via;
}
break;
}
})();
var root = cljs.core.peek(via);
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"via","via",-1904457336),cljs.core.vec(cljs.core.map.cljs$core$IFn$_invoke$arity$2(base,via)),new cljs.core.Keyword(null,"trace","trace",-1082747415),null], null),(function (){var temp__5735__auto__ = cljs.core.ex_message(root);
if(cljs.core.truth_(temp__5735__auto__)){
var root_msg = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cause","cause",231901252),root_msg], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = cljs.core.ex_data(root);
if(cljs.core.truth_(temp__5735__auto__)){
var data = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),data], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358).cljs$core$IFn$_invoke$arity$1(cljs.core.ex_data(o));
if(cljs.core.truth_(temp__5735__auto__)){
var phase = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"phase","phase",575722892),phase], null);
} else {
return null;
}
})()], 0));
});
/**
 * Returns an analysis of the phase, error, cause, and location of an error that occurred
 *   based on Throwable data, as returned by Throwable->map. All attributes other than phase
 *   are optional:
 *  :clojure.error/phase - keyword phase indicator, one of:
 *    :read-source :compile-syntax-check :compilation :macro-syntax-check :macroexpansion
 *    :execution :read-eval-result :print-eval-result
 *  :clojure.error/source - file name (no path)
 *  :clojure.error/line - integer line number
 *  :clojure.error/column - integer column number
 *  :clojure.error/symbol - symbol being expanded/compiled/invoked
 *  :clojure.error/class - cause exception class symbol
 *  :clojure.error/cause - cause exception message
 *  :clojure.error/spec - explain-data for spec error
 */
cljs.repl.ex_triage = (function cljs$repl$ex_triage(datafied_throwable){
var map__29954 = datafied_throwable;
var map__29954__$1 = (((((!((map__29954 == null))))?(((((map__29954.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__29954.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__29954):map__29954);
var via = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29954__$1,new cljs.core.Keyword(null,"via","via",-1904457336));
var trace = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29954__$1,new cljs.core.Keyword(null,"trace","trace",-1082747415));
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__29954__$1,new cljs.core.Keyword(null,"phase","phase",575722892),new cljs.core.Keyword(null,"execution","execution",253283524));
var map__29955 = cljs.core.last(via);
var map__29955__$1 = (((((!((map__29955 == null))))?(((((map__29955.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__29955.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__29955):map__29955);
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29955__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var message = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29955__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var data = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29955__$1,new cljs.core.Keyword(null,"data","data",-232669377));
var map__29956 = data;
var map__29956__$1 = (((((!((map__29956 == null))))?(((((map__29956.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__29956.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__29956):map__29956);
var problems = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29956__$1,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814));
var fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29956__$1,new cljs.core.Keyword("cljs.spec.alpha","fn","cljs.spec.alpha/fn",408600443));
var caller = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29956__$1,new cljs.core.Keyword("cljs.spec.test.alpha","caller","cljs.spec.test.alpha/caller",-398302390));
var map__29957 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.first(via));
var map__29957__$1 = (((((!((map__29957 == null))))?(((((map__29957.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__29957.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__29957):map__29957);
var top_data = map__29957__$1;
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29957__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3((function (){var G__29983 = phase;
var G__29983__$1 = (((G__29983 instanceof cljs.core.Keyword))?G__29983.fqn:null);
switch (G__29983__$1) {
case "read-source":
var map__29985 = data;
var map__29985__$1 = (((((!((map__29985 == null))))?(((((map__29985.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__29985.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__29985):map__29985);
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29985__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29985__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var G__29990 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.second(via)),top_data], 0));
var G__29990__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__29990,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__29990);
var G__29990__$2 = (cljs.core.truth_((function (){var fexpr__29992 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__29992.cljs$core$IFn$_invoke$arity$1 ? fexpr__29992.cljs$core$IFn$_invoke$arity$1(source) : fexpr__29992.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__29990__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__29990__$1);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__29990__$2,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__29990__$2;
}

break;
case "compile-syntax-check":
case "compilation":
case "macro-syntax-check":
case "macroexpansion":
var G__29994 = top_data;
var G__29994__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__29994,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__29994);
var G__29994__$2 = (cljs.core.truth_((function (){var fexpr__29995 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__29995.cljs$core$IFn$_invoke$arity$1 ? fexpr__29995.cljs$core$IFn$_invoke$arity$1(source) : fexpr__29995.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__29994__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__29994__$1);
var G__29994__$3 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__29994__$2,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__29994__$2);
var G__29994__$4 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__29994__$3,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__29994__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__29994__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__29994__$4;
}

break;
case "read-eval-result":
case "print-eval-result":
var vec__29997 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29997,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29997,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29997,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__29997,(3),null);
var G__30001 = top_data;
var G__30001__$1 = (cljs.core.truth_(line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__30001,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),line):G__30001);
var G__30001__$2 = (cljs.core.truth_(file)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__30001__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file):G__30001__$1);
var G__30001__$3 = (cljs.core.truth_((function (){var and__4120__auto__ = source__$1;
if(cljs.core.truth_(and__4120__auto__)){
return method;
} else {
return and__4120__auto__;
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__30001__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null))):G__30001__$2);
var G__30001__$4 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__30001__$3,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__30001__$3);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__30001__$4,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__30001__$4;
}

break;
case "execution":
var vec__30007 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30007,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30007,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30007,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30007,(3),null);
var file__$1 = cljs.core.first(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(((function (vec__30007,source__$1,method,file,line,G__29983,G__29983__$1,map__29954,map__29954__$1,via,trace,phase,map__29955,map__29955__$1,type,message,data,map__29956,map__29956__$1,problems,fn,caller,map__29957,map__29957__$1,top_data,source){
return (function (p1__29952_SHARP_){
var or__4131__auto__ = (p1__29952_SHARP_ == null);
if(or__4131__auto__){
return or__4131__auto__;
} else {
var fexpr__30015 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__30015.cljs$core$IFn$_invoke$arity$1 ? fexpr__30015.cljs$core$IFn$_invoke$arity$1(p1__29952_SHARP_) : fexpr__30015.call(null,p1__29952_SHARP_));
}
});})(vec__30007,source__$1,method,file,line,G__29983,G__29983__$1,map__29954,map__29954__$1,via,trace,phase,map__29955,map__29955__$1,type,message,data,map__29956,map__29956__$1,problems,fn,caller,map__29957,map__29957__$1,top_data,source))
,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(caller),file], null)));
var err_line = (function (){var or__4131__auto__ = new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(caller);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return line;
}
})();
var G__30018 = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type], null);
var G__30018__$1 = (cljs.core.truth_(err_line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__30018,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),err_line):G__30018);
var G__30018__$2 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__30018__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__30018__$1);
var G__30018__$3 = (cljs.core.truth_((function (){var or__4131__auto__ = fn;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
var and__4120__auto__ = source__$1;
if(cljs.core.truth_(and__4120__auto__)){
return method;
} else {
return and__4120__auto__;
}
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__30018__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(function (){var or__4131__auto__ = fn;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null));
}
})()):G__30018__$2);
var G__30018__$4 = (cljs.core.truth_(file__$1)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__30018__$3,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file__$1):G__30018__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__30018__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__30018__$4;
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__29983__$1)].join('')));

}
})(),new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358),phase);
});
/**
 * Returns a string from exception data, as produced by ex-triage.
 *   The first line summarizes the exception phase and location.
 *   The subsequent lines describe the cause.
 */
cljs.repl.ex_str = (function cljs$repl$ex_str(p__30047){
var map__30048 = p__30047;
var map__30048__$1 = (((((!((map__30048 == null))))?(((((map__30048.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__30048.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__30048):map__30048);
var triage_data = map__30048__$1;
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__30048__$1,new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358));
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__30048__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__30048__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__30048__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var symbol = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__30048__$1,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994));
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__30048__$1,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890));
var cause = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__30048__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742));
var spec = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__30048__$1,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595));
var loc = [cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4131__auto__ = source;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "<cljs repl>";
}
})()),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4131__auto__ = line;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (1);
}
})()),(cljs.core.truth_(column)?[":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join(''):"")].join('');
var class_name = cljs.core.name((function (){var or__4131__auto__ = class$;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "";
}
})());
var simple_class = class_name;
var cause_type = ((cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["RuntimeException",null,"Exception",null], null), null),simple_class))?"":[" (",simple_class,")"].join(''));
var format = goog.string.format;
var G__30068 = phase;
var G__30068__$1 = (((G__30068 instanceof cljs.core.Keyword))?G__30068.fqn:null);
switch (G__30068__$1) {
case "read-source":
return (format.cljs$core$IFn$_invoke$arity$3 ? format.cljs$core$IFn$_invoke$arity$3("Syntax error reading source at (%s).\n%s\n",loc,cause) : format.call(null,"Syntax error reading source at (%s).\n%s\n",loc,cause));

break;
case "macro-syntax-check":
var G__30069 = "Syntax error macroexpanding %sat (%s).\n%s";
var G__30070 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__30071 = loc;
var G__30072 = (cljs.core.truth_(spec)?(function (){var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__30078_30429 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__30079_30430 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__30080_30431 = true;
var _STAR_print_fn_STAR__temp_val__30081_30432 = ((function (_STAR_print_newline_STAR__orig_val__30078_30429,_STAR_print_fn_STAR__orig_val__30079_30430,_STAR_print_newline_STAR__temp_val__30080_30431,sb__4661__auto__,G__30069,G__30070,G__30071,G__30068,G__30068__$1,loc,class_name,simple_class,cause_type,format,map__30048,map__30048__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__30078_30429,_STAR_print_fn_STAR__orig_val__30079_30430,_STAR_print_newline_STAR__temp_val__30080_30431,sb__4661__auto__,G__30069,G__30070,G__30071,G__30068,G__30068__$1,loc,class_name,simple_class,cause_type,format,map__30048,map__30048__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__30080_30431;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__30081_30432;

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),((function (_STAR_print_newline_STAR__orig_val__30078_30429,_STAR_print_fn_STAR__orig_val__30079_30430,_STAR_print_newline_STAR__temp_val__30080_30431,_STAR_print_fn_STAR__temp_val__30081_30432,sb__4661__auto__,G__30069,G__30070,G__30071,G__30068,G__30068__$1,loc,class_name,simple_class,cause_type,format,map__30048,map__30048__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (_STAR_print_newline_STAR__orig_val__30078_30429,_STAR_print_fn_STAR__orig_val__30079_30430,_STAR_print_newline_STAR__temp_val__30080_30431,_STAR_print_fn_STAR__temp_val__30081_30432,sb__4661__auto__,G__30069,G__30070,G__30071,G__30068,G__30068__$1,loc,class_name,simple_class,cause_type,format,map__30048,map__30048__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (p1__30031_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__30031_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
});})(_STAR_print_newline_STAR__orig_val__30078_30429,_STAR_print_fn_STAR__orig_val__30079_30430,_STAR_print_newline_STAR__temp_val__30080_30431,_STAR_print_fn_STAR__temp_val__30081_30432,sb__4661__auto__,G__30069,G__30070,G__30071,G__30068,G__30068__$1,loc,class_name,simple_class,cause_type,format,map__30048,map__30048__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
,probs);
});})(_STAR_print_newline_STAR__orig_val__30078_30429,_STAR_print_fn_STAR__orig_val__30079_30430,_STAR_print_newline_STAR__temp_val__30080_30431,_STAR_print_fn_STAR__temp_val__30081_30432,sb__4661__auto__,G__30069,G__30070,G__30071,G__30068,G__30068__$1,loc,class_name,simple_class,cause_type,format,map__30048,map__30048__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
)
);
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__30079_30430;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__30078_30429;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
})():(format.cljs$core$IFn$_invoke$arity$2 ? format.cljs$core$IFn$_invoke$arity$2("%s\n",cause) : format.call(null,"%s\n",cause)));
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__30069,G__30070,G__30071,G__30072) : format.call(null,G__30069,G__30070,G__30071,G__30072));

break;
case "macroexpansion":
var G__30091 = "Unexpected error%s macroexpanding %sat (%s).\n%s\n";
var G__30092 = cause_type;
var G__30093 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__30094 = loc;
var G__30095 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__30091,G__30092,G__30093,G__30094,G__30095) : format.call(null,G__30091,G__30092,G__30093,G__30094,G__30095));

break;
case "compile-syntax-check":
var G__30097 = "Syntax error%s compiling %sat (%s).\n%s\n";
var G__30098 = cause_type;
var G__30099 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__30100 = loc;
var G__30101 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__30097,G__30098,G__30099,G__30100,G__30101) : format.call(null,G__30097,G__30098,G__30099,G__30100,G__30101));

break;
case "compilation":
var G__30108 = "Unexpected error%s compiling %sat (%s).\n%s\n";
var G__30109 = cause_type;
var G__30110 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__30111 = loc;
var G__30112 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__30108,G__30109,G__30110,G__30111,G__30112) : format.call(null,G__30108,G__30109,G__30110,G__30111,G__30112));

break;
case "read-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "print-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "execution":
if(cljs.core.truth_(spec)){
var G__30113 = "Execution error - invalid arguments to %s at (%s).\n%s";
var G__30114 = symbol;
var G__30115 = loc;
var G__30116 = (function (){var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__30120_30448 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__30121_30449 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__30122_30450 = true;
var _STAR_print_fn_STAR__temp_val__30123_30451 = ((function (_STAR_print_newline_STAR__orig_val__30120_30448,_STAR_print_fn_STAR__orig_val__30121_30449,_STAR_print_newline_STAR__temp_val__30122_30450,sb__4661__auto__,G__30113,G__30114,G__30115,G__30068,G__30068__$1,loc,class_name,simple_class,cause_type,format,map__30048,map__30048__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__30120_30448,_STAR_print_fn_STAR__orig_val__30121_30449,_STAR_print_newline_STAR__temp_val__30122_30450,sb__4661__auto__,G__30113,G__30114,G__30115,G__30068,G__30068__$1,loc,class_name,simple_class,cause_type,format,map__30048,map__30048__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__30122_30450;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__30123_30451;

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),((function (_STAR_print_newline_STAR__orig_val__30120_30448,_STAR_print_fn_STAR__orig_val__30121_30449,_STAR_print_newline_STAR__temp_val__30122_30450,_STAR_print_fn_STAR__temp_val__30123_30451,sb__4661__auto__,G__30113,G__30114,G__30115,G__30068,G__30068__$1,loc,class_name,simple_class,cause_type,format,map__30048,map__30048__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (_STAR_print_newline_STAR__orig_val__30120_30448,_STAR_print_fn_STAR__orig_val__30121_30449,_STAR_print_newline_STAR__temp_val__30122_30450,_STAR_print_fn_STAR__temp_val__30123_30451,sb__4661__auto__,G__30113,G__30114,G__30115,G__30068,G__30068__$1,loc,class_name,simple_class,cause_type,format,map__30048,map__30048__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (p1__30034_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__30034_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
});})(_STAR_print_newline_STAR__orig_val__30120_30448,_STAR_print_fn_STAR__orig_val__30121_30449,_STAR_print_newline_STAR__temp_val__30122_30450,_STAR_print_fn_STAR__temp_val__30123_30451,sb__4661__auto__,G__30113,G__30114,G__30115,G__30068,G__30068__$1,loc,class_name,simple_class,cause_type,format,map__30048,map__30048__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
,probs);
});})(_STAR_print_newline_STAR__orig_val__30120_30448,_STAR_print_fn_STAR__orig_val__30121_30449,_STAR_print_newline_STAR__temp_val__30122_30450,_STAR_print_fn_STAR__temp_val__30123_30451,sb__4661__auto__,G__30113,G__30114,G__30115,G__30068,G__30068__$1,loc,class_name,simple_class,cause_type,format,map__30048,map__30048__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
)
);
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__30121_30449;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__30120_30448;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
})();
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__30113,G__30114,G__30115,G__30116) : format.call(null,G__30113,G__30114,G__30115,G__30116));
} else {
var G__30132 = "Execution error%s at %s(%s).\n%s\n";
var G__30133 = cause_type;
var G__30134 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__30135 = loc;
var G__30136 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__30132,G__30133,G__30134,G__30135,G__30136) : format.call(null,G__30132,G__30133,G__30134,G__30135,G__30136));
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__30068__$1)].join('')));

}
});
cljs.repl.error__GT_str = (function cljs$repl$error__GT_str(error){
return cljs.repl.ex_str(cljs.repl.ex_triage(cljs.repl.Error__GT_map(error)));
});

//# sourceMappingURL=cljs.repl.js.map
