goog.provide('reagent.debug');
goog.require('cljs.core');
reagent.debug.has_console = (typeof console !== 'undefined');
reagent.debug.tracking = false;
if((typeof reagent !== 'undefined') && (typeof reagent.debug !== 'undefined') && (typeof reagent.debug.warnings !== 'undefined')){
} else {
reagent.debug.warnings = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
if((typeof reagent !== 'undefined') && (typeof reagent.debug !== 'undefined') && (typeof reagent.debug.track_console !== 'undefined')){
} else {
reagent.debug.track_console = (function (){var o = ({});
o.warn = ((function (o){
return (function() { 
var G__30981__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"warn","warn",-436710552)], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__30981 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__30984__i = 0, G__30984__a = new Array(arguments.length -  0);
while (G__30984__i < G__30984__a.length) {G__30984__a[G__30984__i] = arguments[G__30984__i + 0]; ++G__30984__i;}
  args = new cljs.core.IndexedSeq(G__30984__a,0,null);
} 
return G__30981__delegate.call(this,args);};
G__30981.cljs$lang$maxFixedArity = 0;
G__30981.cljs$lang$applyTo = (function (arglist__30985){
var args = cljs.core.seq(arglist__30985);
return G__30981__delegate(args);
});
G__30981.cljs$core$IFn$_invoke$arity$variadic = G__30981__delegate;
return G__30981;
})()
;})(o))
;

o.error = ((function (o){
return (function() { 
var G__30986__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"error","error",-978969032)], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__30986 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__30987__i = 0, G__30987__a = new Array(arguments.length -  0);
while (G__30987__i < G__30987__a.length) {G__30987__a[G__30987__i] = arguments[G__30987__i + 0]; ++G__30987__i;}
  args = new cljs.core.IndexedSeq(G__30987__a,0,null);
} 
return G__30986__delegate.call(this,args);};
G__30986.cljs$lang$maxFixedArity = 0;
G__30986.cljs$lang$applyTo = (function (arglist__30988){
var args = cljs.core.seq(arglist__30988);
return G__30986__delegate(args);
});
G__30986.cljs$core$IFn$_invoke$arity$variadic = G__30986__delegate;
return G__30986;
})()
;})(o))
;

return o;
})();
}
reagent.debug.track_warnings = (function reagent$debug$track_warnings(f){
reagent.debug.tracking = true;

cljs.core.reset_BANG_(reagent.debug.warnings,null);

(f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null));

var warns = cljs.core.deref(reagent.debug.warnings);
cljs.core.reset_BANG_(reagent.debug.warnings,null);

reagent.debug.tracking = false;

return warns;
});

//# sourceMappingURL=reagent.debug.js.map
