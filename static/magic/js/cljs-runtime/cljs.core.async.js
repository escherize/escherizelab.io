goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.ioc_helpers');
goog.require('goog.array');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__29912 = arguments.length;
switch (G__29912) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(f,true);
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async29918 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async29918 = (function (f,blockable,meta29919){
this.f = f;
this.blockable = blockable;
this.meta29919 = meta29919;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async29918.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_29920,meta29919__$1){
var self__ = this;
var _29920__$1 = this;
return (new cljs.core.async.t_cljs$core$async29918(self__.f,self__.blockable,meta29919__$1));
});

cljs.core.async.t_cljs$core$async29918.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_29920){
var self__ = this;
var _29920__$1 = this;
return self__.meta29919;
});

cljs.core.async.t_cljs$core$async29918.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async29918.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async29918.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
});

cljs.core.async.t_cljs$core$async29918.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
});

cljs.core.async.t_cljs$core$async29918.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta29919","meta29919",1362231674,null)], null);
});

cljs.core.async.t_cljs$core$async29918.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async29918.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async29918";

cljs.core.async.t_cljs$core$async29918.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async29918");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async29918.
 */
cljs.core.async.__GT_t_cljs$core$async29918 = (function cljs$core$async$__GT_t_cljs$core$async29918(f__$1,blockable__$1,meta29919){
return (new cljs.core.async.t_cljs$core$async29918(f__$1,blockable__$1,meta29919));
});

}

return (new cljs.core.async.t_cljs$core$async29918(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
});

cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2;

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer(n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__29989 = arguments.length;
switch (G__29989) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,xform,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.cljs$core$IFn$_invoke$arity$3(((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer(buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
});

cljs.core.async.chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__30005 = arguments.length;
switch (G__30005) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1(null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2(xform,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(cljs.core.async.impl.buffers.promise_buffer(),xform,ex_handler);
});

cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout(msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__30025 = arguments.length;
switch (G__30025) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3(port,fn1,true);
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(ret)){
var val_33392 = cljs.core.deref(ret);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_33392) : fn1.call(null,val_33392));
} else {
cljs.core.async.impl.dispatch.run(((function (val_33392,ret){
return (function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_33392) : fn1.call(null,val_33392));
});})(val_33392,ret))
);
}
} else {
}

return null;
});

cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3;

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn1 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn1 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__30083 = arguments.length;
switch (G__30083) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5733__auto__)){
var ret = temp__5733__auto__;
return cljs.core.deref(ret);
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4(port,val,fn1,true);
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(temp__5733__auto__)){
var retb = temp__5733__auto__;
var ret = cljs.core.deref(retb);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
} else {
cljs.core.async.impl.dispatch.run(((function (ret,retb,temp__5733__auto__){
return (function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
});})(ret,retb,temp__5733__auto__))
);
}

return ret;
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4;

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_(port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__4607__auto___33439 = n;
var x_33442 = (0);
while(true){
if((x_33442 < n__4607__auto___33439)){
(a[x_33442] = x_33442);

var G__33444 = (x_33442 + (1));
x_33442 = G__33444;
continue;
} else {
}
break;
}

goog.array.shuffle(a);

return a;
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async30148 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30148 = (function (flag,meta30149){
this.flag = flag;
this.meta30149 = meta30149;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async30148.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_30150,meta30149__$1){
var self__ = this;
var _30150__$1 = this;
return (new cljs.core.async.t_cljs$core$async30148(self__.flag,meta30149__$1));
});})(flag))
;

cljs.core.async.t_cljs$core$async30148.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_30150){
var self__ = this;
var _30150__$1 = this;
return self__.meta30149;
});})(flag))
;

cljs.core.async.t_cljs$core$async30148.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async30148.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref(self__.flag);
});})(flag))
;

cljs.core.async.t_cljs$core$async30148.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async30148.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.flag,null);

return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async30148.getBasis = ((function (flag){
return (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta30149","meta30149",1999745318,null)], null);
});})(flag))
;

cljs.core.async.t_cljs$core$async30148.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async30148.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30148";

cljs.core.async.t_cljs$core$async30148.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async30148");
});})(flag))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async30148.
 */
cljs.core.async.__GT_t_cljs$core$async30148 = ((function (flag){
return (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async30148(flag__$1,meta30149){
return (new cljs.core.async.t_cljs$core$async30148(flag__$1,meta30149));
});})(flag))
;

}

return (new cljs.core.async.t_cljs$core$async30148(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async30201 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30201 = (function (flag,cb,meta30202){
this.flag = flag;
this.cb = cb;
this.meta30202 = meta30202;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async30201.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30203,meta30202__$1){
var self__ = this;
var _30203__$1 = this;
return (new cljs.core.async.t_cljs$core$async30201(self__.flag,self__.cb,meta30202__$1));
});

cljs.core.async.t_cljs$core$async30201.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30203){
var self__ = this;
var _30203__$1 = this;
return self__.meta30202;
});

cljs.core.async.t_cljs$core$async30201.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async30201.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.flag);
});

cljs.core.async.t_cljs$core$async30201.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async30201.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit(self__.flag);

return self__.cb;
});

cljs.core.async.t_cljs$core$async30201.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta30202","meta30202",-1025684880,null)], null);
});

cljs.core.async.t_cljs$core$async30201.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async30201.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30201";

cljs.core.async.t_cljs$core$async30201.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async30201");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async30201.
 */
cljs.core.async.__GT_t_cljs$core$async30201 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async30201(flag__$1,cb__$1,meta30202){
return (new cljs.core.async.t_cljs$core$async30201(flag__$1,cb__$1,meta30202));
});

}

return (new cljs.core.async.t_cljs$core$async30201(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
if((cljs.core.count(ports) > (0))){
} else {
throw (new Error(["Assert failed: ","alts must have at least one channel operation","\n","(pos? (count ports))"].join('')));
}

var flag = cljs.core.async.alt_flag();
var n = cljs.core.count(ports);
var idxs = cljs.core.async.random_array(n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ports,idx);
var wport = ((cljs.core.vector_QMARK_(port))?(port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((0)) : port.call(null,(0))):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = (port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((1)) : port.call(null,(1)));
return cljs.core.async.impl.protocols.put_BANG_(wport,val,cljs.core.async.alt_handler(flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__30267_SHARP_){
var G__30298 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__30267_SHARP_,wport], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__30298) : fret.call(null,G__30298));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.alt_handler(flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__30269_SHARP_){
var G__30306 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__30269_SHARP_,port], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__30306) : fret.call(null,G__30306));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref(vbox),(function (){var or__4131__auto__ = wport;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return port;
}
})()], null));
} else {
var G__33525 = (i + (1));
i = G__33525;
continue;
}
} else {
return null;
}
break;
}
})();
var or__4131__auto__ = ret;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
if(cljs.core.contains_QMARK_(opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5735__auto__ = (function (){var and__4120__auto__ = flag.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1(null);
if(cljs.core.truth_(and__4120__auto__)){
return flag.cljs$core$async$impl$protocols$Handler$commit$arity$1(null);
} else {
return and__4120__auto__;
}
})();
if(cljs.core.truth_(temp__5735__auto__)){
var got = temp__5735__auto__;
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__4736__auto__ = [];
var len__4730__auto___33532 = arguments.length;
var i__4731__auto___33533 = (0);
while(true){
if((i__4731__auto___33533 < len__4730__auto___33532)){
args__4736__auto__.push((arguments[i__4731__auto___33533]));

var G__33535 = (i__4731__auto___33533 + (1));
i__4731__auto___33533 = G__33535;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__30339){
var map__30340 = p__30339;
var map__30340__$1 = (((((!((map__30340 == null))))?(((((map__30340.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__30340.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__30340):map__30340);
var opts = map__30340__$1;
throw (new Error("alts! used not in (go ...) block"));
});

cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq30323){
var G__30324 = cljs.core.first(seq30323);
var seq30323__$1 = cljs.core.next(seq30323);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__30324,seq30323__$1);
});

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__30370 = arguments.length;
switch (G__30370) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3(from,to,true);
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__29782__auto___33559 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto___33559){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto___33559){
return (function (state_30501){
var state_val_30506 = (state_30501[(1)]);
if((state_val_30506 === (7))){
var inst_30489 = (state_30501[(2)]);
var state_30501__$1 = state_30501;
var statearr_30516_33571 = state_30501__$1;
(statearr_30516_33571[(2)] = inst_30489);

(statearr_30516_33571[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30506 === (1))){
var state_30501__$1 = state_30501;
var statearr_30518_33572 = state_30501__$1;
(statearr_30518_33572[(2)] = null);

(statearr_30518_33572[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30506 === (4))){
var inst_30456 = (state_30501[(7)]);
var inst_30456__$1 = (state_30501[(2)]);
var inst_30459 = (inst_30456__$1 == null);
var state_30501__$1 = (function (){var statearr_30522 = state_30501;
(statearr_30522[(7)] = inst_30456__$1);

return statearr_30522;
})();
if(cljs.core.truth_(inst_30459)){
var statearr_30527_33578 = state_30501__$1;
(statearr_30527_33578[(1)] = (5));

} else {
var statearr_30528_33581 = state_30501__$1;
(statearr_30528_33581[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30506 === (13))){
var state_30501__$1 = state_30501;
var statearr_30529_33582 = state_30501__$1;
(statearr_30529_33582[(2)] = null);

(statearr_30529_33582[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30506 === (6))){
var inst_30456 = (state_30501[(7)]);
var state_30501__$1 = state_30501;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_30501__$1,(11),to,inst_30456);
} else {
if((state_val_30506 === (3))){
var inst_30496 = (state_30501[(2)]);
var state_30501__$1 = state_30501;
return cljs.core.async.impl.ioc_helpers.return_chan(state_30501__$1,inst_30496);
} else {
if((state_val_30506 === (12))){
var state_30501__$1 = state_30501;
var statearr_30540_33589 = state_30501__$1;
(statearr_30540_33589[(2)] = null);

(statearr_30540_33589[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30506 === (2))){
var state_30501__$1 = state_30501;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_30501__$1,(4),from);
} else {
if((state_val_30506 === (11))){
var inst_30482 = (state_30501[(2)]);
var state_30501__$1 = state_30501;
if(cljs.core.truth_(inst_30482)){
var statearr_30547_33597 = state_30501__$1;
(statearr_30547_33597[(1)] = (12));

} else {
var statearr_30548_33598 = state_30501__$1;
(statearr_30548_33598[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30506 === (9))){
var state_30501__$1 = state_30501;
var statearr_30555_33599 = state_30501__$1;
(statearr_30555_33599[(2)] = null);

(statearr_30555_33599[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30506 === (5))){
var state_30501__$1 = state_30501;
if(cljs.core.truth_(close_QMARK_)){
var statearr_30556_33607 = state_30501__$1;
(statearr_30556_33607[(1)] = (8));

} else {
var statearr_30557_33608 = state_30501__$1;
(statearr_30557_33608[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30506 === (14))){
var inst_30487 = (state_30501[(2)]);
var state_30501__$1 = state_30501;
var statearr_30561_33610 = state_30501__$1;
(statearr_30561_33610[(2)] = inst_30487);

(statearr_30561_33610[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30506 === (10))){
var inst_30479 = (state_30501[(2)]);
var state_30501__$1 = state_30501;
var statearr_30565_33617 = state_30501__$1;
(statearr_30565_33617[(2)] = inst_30479);

(statearr_30565_33617[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30506 === (8))){
var inst_30473 = cljs.core.async.close_BANG_(to);
var state_30501__$1 = state_30501;
var statearr_30571_33619 = state_30501__$1;
(statearr_30571_33619[(2)] = inst_30473);

(statearr_30571_33619[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto___33559))
;
return ((function (switch__28688__auto__,c__29782__auto___33559){
return (function() {
var cljs$core$async$state_machine__28689__auto__ = null;
var cljs$core$async$state_machine__28689__auto____0 = (function (){
var statearr_30578 = [null,null,null,null,null,null,null,null];
(statearr_30578[(0)] = cljs$core$async$state_machine__28689__auto__);

(statearr_30578[(1)] = (1));

return statearr_30578;
});
var cljs$core$async$state_machine__28689__auto____1 = (function (state_30501){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_30501);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e30583){if((e30583 instanceof Object)){
var ex__28692__auto__ = e30583;
var statearr_30587_33630 = state_30501;
(statearr_30587_33630[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_30501);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30583;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33631 = state_30501;
state_30501 = G__33631;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$state_machine__28689__auto__ = function(state_30501){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28689__auto____1.call(this,state_30501);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28689__auto____0;
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28689__auto____1;
return cljs$core$async$state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto___33559))
})();
var state__29784__auto__ = (function (){var statearr_30592 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_30592[(6)] = c__29782__auto___33559);

return statearr_30592;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto___33559))
);


return to;
});

cljs.core.async.pipe.cljs$lang$maxFixedArity = 3;

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var results = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var process = ((function (jobs,results){
return (function (p__30602){
var vec__30607 = p__30602;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30607,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30607,(1),null);
var job = vec__30607;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((1),xf,ex_handler);
var c__29782__auto___33647 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto___33647,res,vec__30607,v,p,job,jobs,results){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto___33647,res,vec__30607,v,p,job,jobs,results){
return (function (state_30618){
var state_val_30619 = (state_30618[(1)]);
if((state_val_30619 === (1))){
var state_30618__$1 = state_30618;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_30618__$1,(2),res,v);
} else {
if((state_val_30619 === (2))){
var inst_30614 = (state_30618[(2)]);
var inst_30615 = cljs.core.async.close_BANG_(res);
var state_30618__$1 = (function (){var statearr_30630 = state_30618;
(statearr_30630[(7)] = inst_30614);

return statearr_30630;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_30618__$1,inst_30615);
} else {
return null;
}
}
});})(c__29782__auto___33647,res,vec__30607,v,p,job,jobs,results))
;
return ((function (switch__28688__auto__,c__29782__auto___33647,res,vec__30607,v,p,job,jobs,results){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____0 = (function (){
var statearr_30635 = [null,null,null,null,null,null,null,null];
(statearr_30635[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__);

(statearr_30635[(1)] = (1));

return statearr_30635;
});
var cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____1 = (function (state_30618){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_30618);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e30638){if((e30638 instanceof Object)){
var ex__28692__auto__ = e30638;
var statearr_30644_33656 = state_30618;
(statearr_30644_33656[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_30618);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30638;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33657 = state_30618;
state_30618 = G__33657;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__ = function(state_30618){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____1.call(this,state_30618);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto___33647,res,vec__30607,v,p,job,jobs,results))
})();
var state__29784__auto__ = (function (){var statearr_30648 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_30648[(6)] = c__29782__auto___33647);

return statearr_30648;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto___33647,res,vec__30607,v,p,job,jobs,results))
);


cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});})(jobs,results))
;
var async = ((function (jobs,results,process){
return (function (p__30655){
var vec__30656 = p__30655;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30656,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30656,(1),null);
var job = vec__30656;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
(xf.cljs$core$IFn$_invoke$arity$2 ? xf.cljs$core$IFn$_invoke$arity$2(v,res) : xf.call(null,v,res));

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});})(jobs,results,process))
;
var n__4607__auto___33667 = n;
var __33668 = (0);
while(true){
if((__33668 < n__4607__auto___33667)){
var G__30662_33673 = type;
var G__30662_33674__$1 = (((G__30662_33673 instanceof cljs.core.Keyword))?G__30662_33673.fqn:null);
switch (G__30662_33674__$1) {
case "compute":
var c__29782__auto___33676 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__33668,c__29782__auto___33676,G__30662_33673,G__30662_33674__$1,n__4607__auto___33667,jobs,results,process,async){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (__33668,c__29782__auto___33676,G__30662_33673,G__30662_33674__$1,n__4607__auto___33667,jobs,results,process,async){
return (function (state_30680){
var state_val_30681 = (state_30680[(1)]);
if((state_val_30681 === (1))){
var state_30680__$1 = state_30680;
var statearr_30688_33679 = state_30680__$1;
(statearr_30688_33679[(2)] = null);

(statearr_30688_33679[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30681 === (2))){
var state_30680__$1 = state_30680;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_30680__$1,(4),jobs);
} else {
if((state_val_30681 === (3))){
var inst_30677 = (state_30680[(2)]);
var state_30680__$1 = state_30680;
return cljs.core.async.impl.ioc_helpers.return_chan(state_30680__$1,inst_30677);
} else {
if((state_val_30681 === (4))){
var inst_30666 = (state_30680[(2)]);
var inst_30668 = process(inst_30666);
var state_30680__$1 = state_30680;
if(cljs.core.truth_(inst_30668)){
var statearr_30692_33687 = state_30680__$1;
(statearr_30692_33687[(1)] = (5));

} else {
var statearr_30697_33689 = state_30680__$1;
(statearr_30697_33689[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30681 === (5))){
var state_30680__$1 = state_30680;
var statearr_30699_33693 = state_30680__$1;
(statearr_30699_33693[(2)] = null);

(statearr_30699_33693[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30681 === (6))){
var state_30680__$1 = state_30680;
var statearr_30700_33696 = state_30680__$1;
(statearr_30700_33696[(2)] = null);

(statearr_30700_33696[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30681 === (7))){
var inst_30675 = (state_30680[(2)]);
var state_30680__$1 = state_30680;
var statearr_30702_33699 = state_30680__$1;
(statearr_30702_33699[(2)] = inst_30675);

(statearr_30702_33699[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__33668,c__29782__auto___33676,G__30662_33673,G__30662_33674__$1,n__4607__auto___33667,jobs,results,process,async))
;
return ((function (__33668,switch__28688__auto__,c__29782__auto___33676,G__30662_33673,G__30662_33674__$1,n__4607__auto___33667,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____0 = (function (){
var statearr_30705 = [null,null,null,null,null,null,null];
(statearr_30705[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__);

(statearr_30705[(1)] = (1));

return statearr_30705;
});
var cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____1 = (function (state_30680){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_30680);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e30707){if((e30707 instanceof Object)){
var ex__28692__auto__ = e30707;
var statearr_30710_33709 = state_30680;
(statearr_30710_33709[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_30680);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30707;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33711 = state_30680;
state_30680 = G__33711;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__ = function(state_30680){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____1.call(this,state_30680);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__;
})()
;})(__33668,switch__28688__auto__,c__29782__auto___33676,G__30662_33673,G__30662_33674__$1,n__4607__auto___33667,jobs,results,process,async))
})();
var state__29784__auto__ = (function (){var statearr_30714 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_30714[(6)] = c__29782__auto___33676);

return statearr_30714;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(__33668,c__29782__auto___33676,G__30662_33673,G__30662_33674__$1,n__4607__auto___33667,jobs,results,process,async))
);


break;
case "async":
var c__29782__auto___33717 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__33668,c__29782__auto___33717,G__30662_33673,G__30662_33674__$1,n__4607__auto___33667,jobs,results,process,async){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (__33668,c__29782__auto___33717,G__30662_33673,G__30662_33674__$1,n__4607__auto___33667,jobs,results,process,async){
return (function (state_30729){
var state_val_30730 = (state_30729[(1)]);
if((state_val_30730 === (1))){
var state_30729__$1 = state_30729;
var statearr_30739_33718 = state_30729__$1;
(statearr_30739_33718[(2)] = null);

(statearr_30739_33718[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30730 === (2))){
var state_30729__$1 = state_30729;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_30729__$1,(4),jobs);
} else {
if((state_val_30730 === (3))){
var inst_30727 = (state_30729[(2)]);
var state_30729__$1 = state_30729;
return cljs.core.async.impl.ioc_helpers.return_chan(state_30729__$1,inst_30727);
} else {
if((state_val_30730 === (4))){
var inst_30719 = (state_30729[(2)]);
var inst_30720 = async(inst_30719);
var state_30729__$1 = state_30729;
if(cljs.core.truth_(inst_30720)){
var statearr_30746_33721 = state_30729__$1;
(statearr_30746_33721[(1)] = (5));

} else {
var statearr_30747_33723 = state_30729__$1;
(statearr_30747_33723[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30730 === (5))){
var state_30729__$1 = state_30729;
var statearr_30750_33725 = state_30729__$1;
(statearr_30750_33725[(2)] = null);

(statearr_30750_33725[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30730 === (6))){
var state_30729__$1 = state_30729;
var statearr_30751_33729 = state_30729__$1;
(statearr_30751_33729[(2)] = null);

(statearr_30751_33729[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30730 === (7))){
var inst_30725 = (state_30729[(2)]);
var state_30729__$1 = state_30729;
var statearr_30756_33731 = state_30729__$1;
(statearr_30756_33731[(2)] = inst_30725);

(statearr_30756_33731[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__33668,c__29782__auto___33717,G__30662_33673,G__30662_33674__$1,n__4607__auto___33667,jobs,results,process,async))
;
return ((function (__33668,switch__28688__auto__,c__29782__auto___33717,G__30662_33673,G__30662_33674__$1,n__4607__auto___33667,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____0 = (function (){
var statearr_30762 = [null,null,null,null,null,null,null];
(statearr_30762[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__);

(statearr_30762[(1)] = (1));

return statearr_30762;
});
var cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____1 = (function (state_30729){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_30729);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e30765){if((e30765 instanceof Object)){
var ex__28692__auto__ = e30765;
var statearr_30768_33737 = state_30729;
(statearr_30768_33737[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_30729);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30765;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33738 = state_30729;
state_30729 = G__33738;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__ = function(state_30729){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____1.call(this,state_30729);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__;
})()
;})(__33668,switch__28688__auto__,c__29782__auto___33717,G__30662_33673,G__30662_33674__$1,n__4607__auto___33667,jobs,results,process,async))
})();
var state__29784__auto__ = (function (){var statearr_30771 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_30771[(6)] = c__29782__auto___33717);

return statearr_30771;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(__33668,c__29782__auto___33717,G__30662_33673,G__30662_33674__$1,n__4607__auto___33667,jobs,results,process,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__30662_33674__$1)].join('')));

}

var G__33739 = (__33668 + (1));
__33668 = G__33739;
continue;
} else {
}
break;
}

var c__29782__auto___33740 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto___33740,jobs,results,process,async){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto___33740,jobs,results,process,async){
return (function (state_30803){
var state_val_30804 = (state_30803[(1)]);
if((state_val_30804 === (7))){
var inst_30794 = (state_30803[(2)]);
var state_30803__$1 = state_30803;
var statearr_30838_33741 = state_30803__$1;
(statearr_30838_33741[(2)] = inst_30794);

(statearr_30838_33741[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30804 === (1))){
var state_30803__$1 = state_30803;
var statearr_30845_33744 = state_30803__$1;
(statearr_30845_33744[(2)] = null);

(statearr_30845_33744[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30804 === (4))){
var inst_30775 = (state_30803[(7)]);
var inst_30775__$1 = (state_30803[(2)]);
var inst_30778 = (inst_30775__$1 == null);
var state_30803__$1 = (function (){var statearr_30858 = state_30803;
(statearr_30858[(7)] = inst_30775__$1);

return statearr_30858;
})();
if(cljs.core.truth_(inst_30778)){
var statearr_30864_33747 = state_30803__$1;
(statearr_30864_33747[(1)] = (5));

} else {
var statearr_30868_33748 = state_30803__$1;
(statearr_30868_33748[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30804 === (6))){
var inst_30775 = (state_30803[(7)]);
var inst_30782 = (state_30803[(8)]);
var inst_30782__$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var inst_30785 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_30786 = [inst_30775,inst_30782__$1];
var inst_30787 = (new cljs.core.PersistentVector(null,2,(5),inst_30785,inst_30786,null));
var state_30803__$1 = (function (){var statearr_30882 = state_30803;
(statearr_30882[(8)] = inst_30782__$1);

return statearr_30882;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_30803__$1,(8),jobs,inst_30787);
} else {
if((state_val_30804 === (3))){
var inst_30796 = (state_30803[(2)]);
var state_30803__$1 = state_30803;
return cljs.core.async.impl.ioc_helpers.return_chan(state_30803__$1,inst_30796);
} else {
if((state_val_30804 === (2))){
var state_30803__$1 = state_30803;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_30803__$1,(4),from);
} else {
if((state_val_30804 === (9))){
var inst_30791 = (state_30803[(2)]);
var state_30803__$1 = (function (){var statearr_30888 = state_30803;
(statearr_30888[(9)] = inst_30791);

return statearr_30888;
})();
var statearr_30889_33757 = state_30803__$1;
(statearr_30889_33757[(2)] = null);

(statearr_30889_33757[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30804 === (5))){
var inst_30780 = cljs.core.async.close_BANG_(jobs);
var state_30803__$1 = state_30803;
var statearr_30892_33758 = state_30803__$1;
(statearr_30892_33758[(2)] = inst_30780);

(statearr_30892_33758[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30804 === (8))){
var inst_30782 = (state_30803[(8)]);
var inst_30789 = (state_30803[(2)]);
var state_30803__$1 = (function (){var statearr_30896 = state_30803;
(statearr_30896[(10)] = inst_30789);

return statearr_30896;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_30803__$1,(9),results,inst_30782);
} else {
return null;
}
}
}
}
}
}
}
}
}
});})(c__29782__auto___33740,jobs,results,process,async))
;
return ((function (switch__28688__auto__,c__29782__auto___33740,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____0 = (function (){
var statearr_30899 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_30899[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__);

(statearr_30899[(1)] = (1));

return statearr_30899;
});
var cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____1 = (function (state_30803){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_30803);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e30900){if((e30900 instanceof Object)){
var ex__28692__auto__ = e30900;
var statearr_30903_33770 = state_30803;
(statearr_30903_33770[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_30803);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30900;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33773 = state_30803;
state_30803 = G__33773;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__ = function(state_30803){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____1.call(this,state_30803);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto___33740,jobs,results,process,async))
})();
var state__29784__auto__ = (function (){var statearr_30906 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_30906[(6)] = c__29782__auto___33740);

return statearr_30906;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto___33740,jobs,results,process,async))
);


var c__29782__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto__,jobs,results,process,async){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto__,jobs,results,process,async){
return (function (state_30952){
var state_val_30953 = (state_30952[(1)]);
if((state_val_30953 === (7))){
var inst_30947 = (state_30952[(2)]);
var state_30952__$1 = state_30952;
var statearr_30961_33777 = state_30952__$1;
(statearr_30961_33777[(2)] = inst_30947);

(statearr_30961_33777[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30953 === (20))){
var state_30952__$1 = state_30952;
var statearr_30963_33778 = state_30952__$1;
(statearr_30963_33778[(2)] = null);

(statearr_30963_33778[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30953 === (1))){
var state_30952__$1 = state_30952;
var statearr_30966_33786 = state_30952__$1;
(statearr_30966_33786[(2)] = null);

(statearr_30966_33786[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30953 === (4))){
var inst_30911 = (state_30952[(7)]);
var inst_30911__$1 = (state_30952[(2)]);
var inst_30912 = (inst_30911__$1 == null);
var state_30952__$1 = (function (){var statearr_30970 = state_30952;
(statearr_30970[(7)] = inst_30911__$1);

return statearr_30970;
})();
if(cljs.core.truth_(inst_30912)){
var statearr_30972_33792 = state_30952__$1;
(statearr_30972_33792[(1)] = (5));

} else {
var statearr_30973_33793 = state_30952__$1;
(statearr_30973_33793[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30953 === (15))){
var inst_30926 = (state_30952[(8)]);
var state_30952__$1 = state_30952;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_30952__$1,(18),to,inst_30926);
} else {
if((state_val_30953 === (21))){
var inst_30942 = (state_30952[(2)]);
var state_30952__$1 = state_30952;
var statearr_30974_33799 = state_30952__$1;
(statearr_30974_33799[(2)] = inst_30942);

(statearr_30974_33799[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30953 === (13))){
var inst_30944 = (state_30952[(2)]);
var state_30952__$1 = (function (){var statearr_30975 = state_30952;
(statearr_30975[(9)] = inst_30944);

return statearr_30975;
})();
var statearr_30976_33803 = state_30952__$1;
(statearr_30976_33803[(2)] = null);

(statearr_30976_33803[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30953 === (6))){
var inst_30911 = (state_30952[(7)]);
var state_30952__$1 = state_30952;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_30952__$1,(11),inst_30911);
} else {
if((state_val_30953 === (17))){
var inst_30936 = (state_30952[(2)]);
var state_30952__$1 = state_30952;
if(cljs.core.truth_(inst_30936)){
var statearr_30982_33804 = state_30952__$1;
(statearr_30982_33804[(1)] = (19));

} else {
var statearr_30983_33805 = state_30952__$1;
(statearr_30983_33805[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30953 === (3))){
var inst_30949 = (state_30952[(2)]);
var state_30952__$1 = state_30952;
return cljs.core.async.impl.ioc_helpers.return_chan(state_30952__$1,inst_30949);
} else {
if((state_val_30953 === (12))){
var inst_30921 = (state_30952[(10)]);
var state_30952__$1 = state_30952;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_30952__$1,(14),inst_30921);
} else {
if((state_val_30953 === (2))){
var state_30952__$1 = state_30952;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_30952__$1,(4),results);
} else {
if((state_val_30953 === (19))){
var state_30952__$1 = state_30952;
var statearr_30991_33811 = state_30952__$1;
(statearr_30991_33811[(2)] = null);

(statearr_30991_33811[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30953 === (11))){
var inst_30921 = (state_30952[(2)]);
var state_30952__$1 = (function (){var statearr_30992 = state_30952;
(statearr_30992[(10)] = inst_30921);

return statearr_30992;
})();
var statearr_30993_33818 = state_30952__$1;
(statearr_30993_33818[(2)] = null);

(statearr_30993_33818[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30953 === (9))){
var state_30952__$1 = state_30952;
var statearr_30994_33819 = state_30952__$1;
(statearr_30994_33819[(2)] = null);

(statearr_30994_33819[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30953 === (5))){
var state_30952__$1 = state_30952;
if(cljs.core.truth_(close_QMARK_)){
var statearr_30995_33823 = state_30952__$1;
(statearr_30995_33823[(1)] = (8));

} else {
var statearr_30997_33824 = state_30952__$1;
(statearr_30997_33824[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30953 === (14))){
var inst_30930 = (state_30952[(11)]);
var inst_30926 = (state_30952[(8)]);
var inst_30926__$1 = (state_30952[(2)]);
var inst_30929 = (inst_30926__$1 == null);
var inst_30930__$1 = cljs.core.not(inst_30929);
var state_30952__$1 = (function (){var statearr_31003 = state_30952;
(statearr_31003[(11)] = inst_30930__$1);

(statearr_31003[(8)] = inst_30926__$1);

return statearr_31003;
})();
if(inst_30930__$1){
var statearr_31004_33828 = state_30952__$1;
(statearr_31004_33828[(1)] = (15));

} else {
var statearr_31005_33829 = state_30952__$1;
(statearr_31005_33829[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30953 === (16))){
var inst_30930 = (state_30952[(11)]);
var state_30952__$1 = state_30952;
var statearr_31006_33830 = state_30952__$1;
(statearr_31006_33830[(2)] = inst_30930);

(statearr_31006_33830[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30953 === (10))){
var inst_30918 = (state_30952[(2)]);
var state_30952__$1 = state_30952;
var statearr_31012_33835 = state_30952__$1;
(statearr_31012_33835[(2)] = inst_30918);

(statearr_31012_33835[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30953 === (18))){
var inst_30933 = (state_30952[(2)]);
var state_30952__$1 = state_30952;
var statearr_31014_33836 = state_30952__$1;
(statearr_31014_33836[(2)] = inst_30933);

(statearr_31014_33836[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30953 === (8))){
var inst_30915 = cljs.core.async.close_BANG_(to);
var state_30952__$1 = state_30952;
var statearr_31017_33838 = state_30952__$1;
(statearr_31017_33838[(2)] = inst_30915);

(statearr_31017_33838[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto__,jobs,results,process,async))
;
return ((function (switch__28688__auto__,c__29782__auto__,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____0 = (function (){
var statearr_31022 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_31022[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__);

(statearr_31022[(1)] = (1));

return statearr_31022;
});
var cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____1 = (function (state_30952){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_30952);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e31023){if((e31023 instanceof Object)){
var ex__28692__auto__ = e31023;
var statearr_31027_33843 = state_30952;
(statearr_31027_33843[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_30952);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31023;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33844 = state_30952;
state_30952 = G__33844;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__ = function(state_30952){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____1.call(this,state_30952);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__28689__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto__,jobs,results,process,async))
})();
var state__29784__auto__ = (function (){var statearr_31029 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_31029[(6)] = c__29782__auto__);

return statearr_31029;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto__,jobs,results,process,async))
);

return c__29782__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__31035 = arguments.length;
switch (G__31035) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5(n,to,af,from,true);
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_(n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
});

cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5;

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__31037 = arguments.length;
switch (G__31037) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5(n,to,xf,from,true);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6(n,to,xf,from,close_QMARK_,null);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
});

cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6;

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__31046 = arguments.length;
switch (G__31046) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4(p,ch,null,null);
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(t_buf_or_n);
var fc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(f_buf_or_n);
var c__29782__auto___33869 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto___33869,tc,fc){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto___33869,tc,fc){
return (function (state_31080){
var state_val_31081 = (state_31080[(1)]);
if((state_val_31081 === (7))){
var inst_31075 = (state_31080[(2)]);
var state_31080__$1 = state_31080;
var statearr_31089_33873 = state_31080__$1;
(statearr_31089_33873[(2)] = inst_31075);

(statearr_31089_33873[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31081 === (1))){
var state_31080__$1 = state_31080;
var statearr_31090_33878 = state_31080__$1;
(statearr_31090_33878[(2)] = null);

(statearr_31090_33878[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31081 === (4))){
var inst_31055 = (state_31080[(7)]);
var inst_31055__$1 = (state_31080[(2)]);
var inst_31056 = (inst_31055__$1 == null);
var state_31080__$1 = (function (){var statearr_31093 = state_31080;
(statearr_31093[(7)] = inst_31055__$1);

return statearr_31093;
})();
if(cljs.core.truth_(inst_31056)){
var statearr_31094_33885 = state_31080__$1;
(statearr_31094_33885[(1)] = (5));

} else {
var statearr_31095_33886 = state_31080__$1;
(statearr_31095_33886[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31081 === (13))){
var state_31080__$1 = state_31080;
var statearr_31097_33887 = state_31080__$1;
(statearr_31097_33887[(2)] = null);

(statearr_31097_33887[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31081 === (6))){
var inst_31055 = (state_31080[(7)]);
var inst_31062 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_31055) : p.call(null,inst_31055));
var state_31080__$1 = state_31080;
if(cljs.core.truth_(inst_31062)){
var statearr_31100_33894 = state_31080__$1;
(statearr_31100_33894[(1)] = (9));

} else {
var statearr_31101_33895 = state_31080__$1;
(statearr_31101_33895[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31081 === (3))){
var inst_31077 = (state_31080[(2)]);
var state_31080__$1 = state_31080;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31080__$1,inst_31077);
} else {
if((state_val_31081 === (12))){
var state_31080__$1 = state_31080;
var statearr_31102_33902 = state_31080__$1;
(statearr_31102_33902[(2)] = null);

(statearr_31102_33902[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31081 === (2))){
var state_31080__$1 = state_31080;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31080__$1,(4),ch);
} else {
if((state_val_31081 === (11))){
var inst_31055 = (state_31080[(7)]);
var inst_31066 = (state_31080[(2)]);
var state_31080__$1 = state_31080;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31080__$1,(8),inst_31066,inst_31055);
} else {
if((state_val_31081 === (9))){
var state_31080__$1 = state_31080;
var statearr_31104_33906 = state_31080__$1;
(statearr_31104_33906[(2)] = tc);

(statearr_31104_33906[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31081 === (5))){
var inst_31059 = cljs.core.async.close_BANG_(tc);
var inst_31060 = cljs.core.async.close_BANG_(fc);
var state_31080__$1 = (function (){var statearr_31106 = state_31080;
(statearr_31106[(8)] = inst_31059);

return statearr_31106;
})();
var statearr_31107_33910 = state_31080__$1;
(statearr_31107_33910[(2)] = inst_31060);

(statearr_31107_33910[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31081 === (14))){
var inst_31073 = (state_31080[(2)]);
var state_31080__$1 = state_31080;
var statearr_31108_33913 = state_31080__$1;
(statearr_31108_33913[(2)] = inst_31073);

(statearr_31108_33913[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31081 === (10))){
var state_31080__$1 = state_31080;
var statearr_31109_33914 = state_31080__$1;
(statearr_31109_33914[(2)] = fc);

(statearr_31109_33914[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31081 === (8))){
var inst_31068 = (state_31080[(2)]);
var state_31080__$1 = state_31080;
if(cljs.core.truth_(inst_31068)){
var statearr_31110_33915 = state_31080__$1;
(statearr_31110_33915[(1)] = (12));

} else {
var statearr_31111_33916 = state_31080__$1;
(statearr_31111_33916[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto___33869,tc,fc))
;
return ((function (switch__28688__auto__,c__29782__auto___33869,tc,fc){
return (function() {
var cljs$core$async$state_machine__28689__auto__ = null;
var cljs$core$async$state_machine__28689__auto____0 = (function (){
var statearr_31112 = [null,null,null,null,null,null,null,null,null];
(statearr_31112[(0)] = cljs$core$async$state_machine__28689__auto__);

(statearr_31112[(1)] = (1));

return statearr_31112;
});
var cljs$core$async$state_machine__28689__auto____1 = (function (state_31080){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_31080);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e31113){if((e31113 instanceof Object)){
var ex__28692__auto__ = e31113;
var statearr_31116_33918 = state_31080;
(statearr_31116_33918[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31080);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31113;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33919 = state_31080;
state_31080 = G__33919;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$state_machine__28689__auto__ = function(state_31080){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28689__auto____1.call(this,state_31080);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28689__auto____0;
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28689__auto____1;
return cljs$core$async$state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto___33869,tc,fc))
})();
var state__29784__auto__ = (function (){var statearr_31119 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_31119[(6)] = c__29782__auto___33869);

return statearr_31119;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto___33869,tc,fc))
);


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});

cljs.core.async.split.cljs$lang$maxFixedArity = 4;

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__29782__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto__){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto__){
return (function (state_31148){
var state_val_31149 = (state_31148[(1)]);
if((state_val_31149 === (7))){
var inst_31142 = (state_31148[(2)]);
var state_31148__$1 = state_31148;
var statearr_31155_33923 = state_31148__$1;
(statearr_31155_33923[(2)] = inst_31142);

(statearr_31155_33923[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31149 === (1))){
var inst_31123 = init;
var state_31148__$1 = (function (){var statearr_31157 = state_31148;
(statearr_31157[(7)] = inst_31123);

return statearr_31157;
})();
var statearr_31158_33924 = state_31148__$1;
(statearr_31158_33924[(2)] = null);

(statearr_31158_33924[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31149 === (4))){
var inst_31128 = (state_31148[(8)]);
var inst_31128__$1 = (state_31148[(2)]);
var inst_31129 = (inst_31128__$1 == null);
var state_31148__$1 = (function (){var statearr_31159 = state_31148;
(statearr_31159[(8)] = inst_31128__$1);

return statearr_31159;
})();
if(cljs.core.truth_(inst_31129)){
var statearr_31160_33926 = state_31148__$1;
(statearr_31160_33926[(1)] = (5));

} else {
var statearr_31162_33927 = state_31148__$1;
(statearr_31162_33927[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31149 === (6))){
var inst_31133 = (state_31148[(9)]);
var inst_31128 = (state_31148[(8)]);
var inst_31123 = (state_31148[(7)]);
var inst_31133__$1 = (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(inst_31123,inst_31128) : f.call(null,inst_31123,inst_31128));
var inst_31134 = cljs.core.reduced_QMARK_(inst_31133__$1);
var state_31148__$1 = (function (){var statearr_31168 = state_31148;
(statearr_31168[(9)] = inst_31133__$1);

return statearr_31168;
})();
if(inst_31134){
var statearr_31169_33928 = state_31148__$1;
(statearr_31169_33928[(1)] = (8));

} else {
var statearr_31171_33929 = state_31148__$1;
(statearr_31171_33929[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31149 === (3))){
var inst_31144 = (state_31148[(2)]);
var state_31148__$1 = state_31148;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31148__$1,inst_31144);
} else {
if((state_val_31149 === (2))){
var state_31148__$1 = state_31148;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31148__$1,(4),ch);
} else {
if((state_val_31149 === (9))){
var inst_31133 = (state_31148[(9)]);
var inst_31123 = inst_31133;
var state_31148__$1 = (function (){var statearr_31181 = state_31148;
(statearr_31181[(7)] = inst_31123);

return statearr_31181;
})();
var statearr_31182_33932 = state_31148__$1;
(statearr_31182_33932[(2)] = null);

(statearr_31182_33932[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31149 === (5))){
var inst_31123 = (state_31148[(7)]);
var state_31148__$1 = state_31148;
var statearr_31183_33933 = state_31148__$1;
(statearr_31183_33933[(2)] = inst_31123);

(statearr_31183_33933[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31149 === (10))){
var inst_31140 = (state_31148[(2)]);
var state_31148__$1 = state_31148;
var statearr_31184_33935 = state_31148__$1;
(statearr_31184_33935[(2)] = inst_31140);

(statearr_31184_33935[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31149 === (8))){
var inst_31133 = (state_31148[(9)]);
var inst_31136 = cljs.core.deref(inst_31133);
var state_31148__$1 = state_31148;
var statearr_31188_33937 = state_31148__$1;
(statearr_31188_33937[(2)] = inst_31136);

(statearr_31188_33937[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto__))
;
return ((function (switch__28688__auto__,c__29782__auto__){
return (function() {
var cljs$core$async$reduce_$_state_machine__28689__auto__ = null;
var cljs$core$async$reduce_$_state_machine__28689__auto____0 = (function (){
var statearr_31192 = [null,null,null,null,null,null,null,null,null,null];
(statearr_31192[(0)] = cljs$core$async$reduce_$_state_machine__28689__auto__);

(statearr_31192[(1)] = (1));

return statearr_31192;
});
var cljs$core$async$reduce_$_state_machine__28689__auto____1 = (function (state_31148){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_31148);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e31193){if((e31193 instanceof Object)){
var ex__28692__auto__ = e31193;
var statearr_31194_33941 = state_31148;
(statearr_31194_33941[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31148);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31193;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33942 = state_31148;
state_31148 = G__33942;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__28689__auto__ = function(state_31148){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__28689__auto____1.call(this,state_31148);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__28689__auto____0;
cljs$core$async$reduce_$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__28689__auto____1;
return cljs$core$async$reduce_$_state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto__))
})();
var state__29784__auto__ = (function (){var statearr_31196 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_31196[(6)] = c__29782__auto__);

return statearr_31196;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto__))
);

return c__29782__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = (xform.cljs$core$IFn$_invoke$arity$1 ? xform.cljs$core$IFn$_invoke$arity$1(f) : xform.call(null,f));
var c__29782__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto__,f__$1){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto__,f__$1){
return (function (state_31206){
var state_val_31207 = (state_31206[(1)]);
if((state_val_31207 === (1))){
var inst_31201 = cljs.core.async.reduce(f__$1,init,ch);
var state_31206__$1 = state_31206;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31206__$1,(2),inst_31201);
} else {
if((state_val_31207 === (2))){
var inst_31203 = (state_31206[(2)]);
var inst_31204 = (f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(inst_31203) : f__$1.call(null,inst_31203));
var state_31206__$1 = state_31206;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31206__$1,inst_31204);
} else {
return null;
}
}
});})(c__29782__auto__,f__$1))
;
return ((function (switch__28688__auto__,c__29782__auto__,f__$1){
return (function() {
var cljs$core$async$transduce_$_state_machine__28689__auto__ = null;
var cljs$core$async$transduce_$_state_machine__28689__auto____0 = (function (){
var statearr_31212 = [null,null,null,null,null,null,null];
(statearr_31212[(0)] = cljs$core$async$transduce_$_state_machine__28689__auto__);

(statearr_31212[(1)] = (1));

return statearr_31212;
});
var cljs$core$async$transduce_$_state_machine__28689__auto____1 = (function (state_31206){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_31206);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e31213){if((e31213 instanceof Object)){
var ex__28692__auto__ = e31213;
var statearr_31214_33951 = state_31206;
(statearr_31214_33951[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31206);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31213;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33952 = state_31206;
state_31206 = G__33952;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__28689__auto__ = function(state_31206){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__28689__auto____1.call(this,state_31206);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__28689__auto____0;
cljs$core$async$transduce_$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__28689__auto____1;
return cljs$core$async$transduce_$_state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto__,f__$1))
})();
var state__29784__auto__ = (function (){var statearr_31215 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_31215[(6)] = c__29782__auto__);

return statearr_31215;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto__,f__$1))
);

return c__29782__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__31225 = arguments.length;
switch (G__31225) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__29782__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto__){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto__){
return (function (state_31262){
var state_val_31263 = (state_31262[(1)]);
if((state_val_31263 === (7))){
var inst_31244 = (state_31262[(2)]);
var state_31262__$1 = state_31262;
var statearr_31269_33958 = state_31262__$1;
(statearr_31269_33958[(2)] = inst_31244);

(statearr_31269_33958[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31263 === (1))){
var inst_31231 = cljs.core.seq(coll);
var inst_31232 = inst_31231;
var state_31262__$1 = (function (){var statearr_31270 = state_31262;
(statearr_31270[(7)] = inst_31232);

return statearr_31270;
})();
var statearr_31271_33961 = state_31262__$1;
(statearr_31271_33961[(2)] = null);

(statearr_31271_33961[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31263 === (4))){
var inst_31232 = (state_31262[(7)]);
var inst_31242 = cljs.core.first(inst_31232);
var state_31262__$1 = state_31262;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31262__$1,(7),ch,inst_31242);
} else {
if((state_val_31263 === (13))){
var inst_31256 = (state_31262[(2)]);
var state_31262__$1 = state_31262;
var statearr_31272_33964 = state_31262__$1;
(statearr_31272_33964[(2)] = inst_31256);

(statearr_31272_33964[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31263 === (6))){
var inst_31247 = (state_31262[(2)]);
var state_31262__$1 = state_31262;
if(cljs.core.truth_(inst_31247)){
var statearr_31274_33965 = state_31262__$1;
(statearr_31274_33965[(1)] = (8));

} else {
var statearr_31275_33969 = state_31262__$1;
(statearr_31275_33969[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31263 === (3))){
var inst_31260 = (state_31262[(2)]);
var state_31262__$1 = state_31262;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31262__$1,inst_31260);
} else {
if((state_val_31263 === (12))){
var state_31262__$1 = state_31262;
var statearr_31277_33974 = state_31262__$1;
(statearr_31277_33974[(2)] = null);

(statearr_31277_33974[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31263 === (2))){
var inst_31232 = (state_31262[(7)]);
var state_31262__$1 = state_31262;
if(cljs.core.truth_(inst_31232)){
var statearr_31278_33981 = state_31262__$1;
(statearr_31278_33981[(1)] = (4));

} else {
var statearr_31279_33983 = state_31262__$1;
(statearr_31279_33983[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31263 === (11))){
var inst_31253 = cljs.core.async.close_BANG_(ch);
var state_31262__$1 = state_31262;
var statearr_31284_33984 = state_31262__$1;
(statearr_31284_33984[(2)] = inst_31253);

(statearr_31284_33984[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31263 === (9))){
var state_31262__$1 = state_31262;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31285_33985 = state_31262__$1;
(statearr_31285_33985[(1)] = (11));

} else {
var statearr_31289_33986 = state_31262__$1;
(statearr_31289_33986[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31263 === (5))){
var inst_31232 = (state_31262[(7)]);
var state_31262__$1 = state_31262;
var statearr_31290_33987 = state_31262__$1;
(statearr_31290_33987[(2)] = inst_31232);

(statearr_31290_33987[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31263 === (10))){
var inst_31258 = (state_31262[(2)]);
var state_31262__$1 = state_31262;
var statearr_31291_33988 = state_31262__$1;
(statearr_31291_33988[(2)] = inst_31258);

(statearr_31291_33988[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31263 === (8))){
var inst_31232 = (state_31262[(7)]);
var inst_31249 = cljs.core.next(inst_31232);
var inst_31232__$1 = inst_31249;
var state_31262__$1 = (function (){var statearr_31292 = state_31262;
(statearr_31292[(7)] = inst_31232__$1);

return statearr_31292;
})();
var statearr_31293_33995 = state_31262__$1;
(statearr_31293_33995[(2)] = null);

(statearr_31293_33995[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto__))
;
return ((function (switch__28688__auto__,c__29782__auto__){
return (function() {
var cljs$core$async$state_machine__28689__auto__ = null;
var cljs$core$async$state_machine__28689__auto____0 = (function (){
var statearr_31294 = [null,null,null,null,null,null,null,null];
(statearr_31294[(0)] = cljs$core$async$state_machine__28689__auto__);

(statearr_31294[(1)] = (1));

return statearr_31294;
});
var cljs$core$async$state_machine__28689__auto____1 = (function (state_31262){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_31262);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e31295){if((e31295 instanceof Object)){
var ex__28692__auto__ = e31295;
var statearr_31296_34000 = state_31262;
(statearr_31296_34000[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31262);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31295;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34002 = state_31262;
state_31262 = G__34002;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$state_machine__28689__auto__ = function(state_31262){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28689__auto____1.call(this,state_31262);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28689__auto____0;
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28689__auto____1;
return cljs$core$async$state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto__))
})();
var state__29784__auto__ = (function (){var statearr_31298 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_31298[(6)] = c__29782__auto__);

return statearr_31298;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto__))
);

return c__29782__auto__;
});

cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
var ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.bounded_count((100),coll));
cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2(ch,coll);

return ch;
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
var x__4433__auto__ = (((_ == null))?null:_);
var m__4434__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4434__auto__.call(null,_));
} else {
var m__4431__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4431__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Mux.muxch*",_);
}
}
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4434__auto__.call(null,m,ch,close_QMARK_));
} else {
var m__4431__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4431__auto__.call(null,m,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Mult.tap*",m);
}
}
}
});

cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4434__auto__.call(null,m,ch));
} else {
var m__4431__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4431__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mult.untap*",m);
}
}
}
});

cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4434__auto__.call(null,m));
} else {
var m__4431__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4431__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mult.untap-all*",m);
}
}
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async31310 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async31310 = (function (ch,cs,meta31311){
this.ch = ch;
this.cs = cs;
this.meta31311 = meta31311;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async31310.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_31312,meta31311__$1){
var self__ = this;
var _31312__$1 = this;
return (new cljs.core.async.t_cljs$core$async31310(self__.ch,self__.cs,meta31311__$1));
});})(cs))
;

cljs.core.async.t_cljs$core$async31310.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_31312){
var self__ = this;
var _31312__$1 = this;
return self__.meta31311;
});})(cs))
;

cljs.core.async.t_cljs$core$async31310.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async31310.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(cs))
;

cljs.core.async.t_cljs$core$async31310.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async31310.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async31310.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch__$1);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async31310.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async31310.getBasis = ((function (cs){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta31311","meta31311",1773835328,null)], null);
});})(cs))
;

cljs.core.async.t_cljs$core$async31310.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async31310.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async31310";

cljs.core.async.t_cljs$core$async31310.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async31310");
});})(cs))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async31310.
 */
cljs.core.async.__GT_t_cljs$core$async31310 = ((function (cs){
return (function cljs$core$async$mult_$___GT_t_cljs$core$async31310(ch__$1,cs__$1,meta31311){
return (new cljs.core.async.t_cljs$core$async31310(ch__$1,cs__$1,meta31311));
});})(cs))
;

}

return (new cljs.core.async.t_cljs$core$async31310(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = ((function (cs,m,dchan,dctr){
return (function (_){
if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,true);
} else {
return null;
}
});})(cs,m,dchan,dctr))
;
var c__29782__auto___34030 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto___34030,cs,m,dchan,dctr,done){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto___34030,cs,m,dchan,dctr,done){
return (function (state_31489){
var state_val_31490 = (state_31489[(1)]);
if((state_val_31490 === (7))){
var inst_31485 = (state_31489[(2)]);
var state_31489__$1 = state_31489;
var statearr_31491_34033 = state_31489__$1;
(statearr_31491_34033[(2)] = inst_31485);

(statearr_31491_34033[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (20))){
var inst_31381 = (state_31489[(7)]);
var inst_31400 = cljs.core.first(inst_31381);
var inst_31401 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31400,(0),null);
var inst_31402 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31400,(1),null);
var state_31489__$1 = (function (){var statearr_31492 = state_31489;
(statearr_31492[(8)] = inst_31401);

return statearr_31492;
})();
if(cljs.core.truth_(inst_31402)){
var statearr_31494_34035 = state_31489__$1;
(statearr_31494_34035[(1)] = (22));

} else {
var statearr_31496_34036 = state_31489__$1;
(statearr_31496_34036[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (27))){
var inst_31437 = (state_31489[(9)]);
var inst_31341 = (state_31489[(10)]);
var inst_31432 = (state_31489[(11)]);
var inst_31430 = (state_31489[(12)]);
var inst_31437__$1 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_31430,inst_31432);
var inst_31438 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_31437__$1,inst_31341,done);
var state_31489__$1 = (function (){var statearr_31499 = state_31489;
(statearr_31499[(9)] = inst_31437__$1);

return statearr_31499;
})();
if(cljs.core.truth_(inst_31438)){
var statearr_31500_34038 = state_31489__$1;
(statearr_31500_34038[(1)] = (30));

} else {
var statearr_31501_34040 = state_31489__$1;
(statearr_31501_34040[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (1))){
var state_31489__$1 = state_31489;
var statearr_31502_34041 = state_31489__$1;
(statearr_31502_34041[(2)] = null);

(statearr_31502_34041[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (24))){
var inst_31381 = (state_31489[(7)]);
var inst_31407 = (state_31489[(2)]);
var inst_31408 = cljs.core.next(inst_31381);
var inst_31350 = inst_31408;
var inst_31351 = null;
var inst_31352 = (0);
var inst_31353 = (0);
var state_31489__$1 = (function (){var statearr_31505 = state_31489;
(statearr_31505[(13)] = inst_31352);

(statearr_31505[(14)] = inst_31353);

(statearr_31505[(15)] = inst_31350);

(statearr_31505[(16)] = inst_31407);

(statearr_31505[(17)] = inst_31351);

return statearr_31505;
})();
var statearr_31507_34051 = state_31489__$1;
(statearr_31507_34051[(2)] = null);

(statearr_31507_34051[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (39))){
var state_31489__$1 = state_31489;
var statearr_31511_34052 = state_31489__$1;
(statearr_31511_34052[(2)] = null);

(statearr_31511_34052[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (4))){
var inst_31341 = (state_31489[(10)]);
var inst_31341__$1 = (state_31489[(2)]);
var inst_31342 = (inst_31341__$1 == null);
var state_31489__$1 = (function (){var statearr_31512 = state_31489;
(statearr_31512[(10)] = inst_31341__$1);

return statearr_31512;
})();
if(cljs.core.truth_(inst_31342)){
var statearr_31513_34054 = state_31489__$1;
(statearr_31513_34054[(1)] = (5));

} else {
var statearr_31514_34055 = state_31489__$1;
(statearr_31514_34055[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (15))){
var inst_31352 = (state_31489[(13)]);
var inst_31353 = (state_31489[(14)]);
var inst_31350 = (state_31489[(15)]);
var inst_31351 = (state_31489[(17)]);
var inst_31377 = (state_31489[(2)]);
var inst_31378 = (inst_31353 + (1));
var tmp31508 = inst_31352;
var tmp31509 = inst_31350;
var tmp31510 = inst_31351;
var inst_31350__$1 = tmp31509;
var inst_31351__$1 = tmp31510;
var inst_31352__$1 = tmp31508;
var inst_31353__$1 = inst_31378;
var state_31489__$1 = (function (){var statearr_31518 = state_31489;
(statearr_31518[(13)] = inst_31352__$1);

(statearr_31518[(14)] = inst_31353__$1);

(statearr_31518[(15)] = inst_31350__$1);

(statearr_31518[(17)] = inst_31351__$1);

(statearr_31518[(18)] = inst_31377);

return statearr_31518;
})();
var statearr_31522_34064 = state_31489__$1;
(statearr_31522_34064[(2)] = null);

(statearr_31522_34064[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (21))){
var inst_31411 = (state_31489[(2)]);
var state_31489__$1 = state_31489;
var statearr_31530_34065 = state_31489__$1;
(statearr_31530_34065[(2)] = inst_31411);

(statearr_31530_34065[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (31))){
var inst_31437 = (state_31489[(9)]);
var inst_31441 = done(null);
var inst_31442 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_31437);
var state_31489__$1 = (function (){var statearr_31535 = state_31489;
(statearr_31535[(19)] = inst_31441);

return statearr_31535;
})();
var statearr_31536_34066 = state_31489__$1;
(statearr_31536_34066[(2)] = inst_31442);

(statearr_31536_34066[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (32))){
var inst_31429 = (state_31489[(20)]);
var inst_31431 = (state_31489[(21)]);
var inst_31432 = (state_31489[(11)]);
var inst_31430 = (state_31489[(12)]);
var inst_31444 = (state_31489[(2)]);
var inst_31445 = (inst_31432 + (1));
var tmp31526 = inst_31429;
var tmp31527 = inst_31431;
var tmp31528 = inst_31430;
var inst_31429__$1 = tmp31526;
var inst_31430__$1 = tmp31528;
var inst_31431__$1 = tmp31527;
var inst_31432__$1 = inst_31445;
var state_31489__$1 = (function (){var statearr_31540 = state_31489;
(statearr_31540[(22)] = inst_31444);

(statearr_31540[(20)] = inst_31429__$1);

(statearr_31540[(21)] = inst_31431__$1);

(statearr_31540[(11)] = inst_31432__$1);

(statearr_31540[(12)] = inst_31430__$1);

return statearr_31540;
})();
var statearr_31544_34069 = state_31489__$1;
(statearr_31544_34069[(2)] = null);

(statearr_31544_34069[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (40))){
var inst_31457 = (state_31489[(23)]);
var inst_31461 = done(null);
var inst_31462 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_31457);
var state_31489__$1 = (function (){var statearr_31545 = state_31489;
(statearr_31545[(24)] = inst_31461);

return statearr_31545;
})();
var statearr_31546_34070 = state_31489__$1;
(statearr_31546_34070[(2)] = inst_31462);

(statearr_31546_34070[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (33))){
var inst_31448 = (state_31489[(25)]);
var inst_31450 = cljs.core.chunked_seq_QMARK_(inst_31448);
var state_31489__$1 = state_31489;
if(inst_31450){
var statearr_31550_34071 = state_31489__$1;
(statearr_31550_34071[(1)] = (36));

} else {
var statearr_31551_34072 = state_31489__$1;
(statearr_31551_34072[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (13))){
var inst_31367 = (state_31489[(26)]);
var inst_31374 = cljs.core.async.close_BANG_(inst_31367);
var state_31489__$1 = state_31489;
var statearr_31557_34073 = state_31489__$1;
(statearr_31557_34073[(2)] = inst_31374);

(statearr_31557_34073[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (22))){
var inst_31401 = (state_31489[(8)]);
var inst_31404 = cljs.core.async.close_BANG_(inst_31401);
var state_31489__$1 = state_31489;
var statearr_31561_34074 = state_31489__$1;
(statearr_31561_34074[(2)] = inst_31404);

(statearr_31561_34074[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (36))){
var inst_31448 = (state_31489[(25)]);
var inst_31452 = cljs.core.chunk_first(inst_31448);
var inst_31453 = cljs.core.chunk_rest(inst_31448);
var inst_31454 = cljs.core.count(inst_31452);
var inst_31429 = inst_31453;
var inst_31430 = inst_31452;
var inst_31431 = inst_31454;
var inst_31432 = (0);
var state_31489__$1 = (function (){var statearr_31566 = state_31489;
(statearr_31566[(20)] = inst_31429);

(statearr_31566[(21)] = inst_31431);

(statearr_31566[(11)] = inst_31432);

(statearr_31566[(12)] = inst_31430);

return statearr_31566;
})();
var statearr_31568_34075 = state_31489__$1;
(statearr_31568_34075[(2)] = null);

(statearr_31568_34075[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (41))){
var inst_31448 = (state_31489[(25)]);
var inst_31464 = (state_31489[(2)]);
var inst_31465 = cljs.core.next(inst_31448);
var inst_31429 = inst_31465;
var inst_31430 = null;
var inst_31431 = (0);
var inst_31432 = (0);
var state_31489__$1 = (function (){var statearr_31574 = state_31489;
(statearr_31574[(20)] = inst_31429);

(statearr_31574[(27)] = inst_31464);

(statearr_31574[(21)] = inst_31431);

(statearr_31574[(11)] = inst_31432);

(statearr_31574[(12)] = inst_31430);

return statearr_31574;
})();
var statearr_31577_34078 = state_31489__$1;
(statearr_31577_34078[(2)] = null);

(statearr_31577_34078[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (43))){
var state_31489__$1 = state_31489;
var statearr_31578_34079 = state_31489__$1;
(statearr_31578_34079[(2)] = null);

(statearr_31578_34079[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (29))){
var inst_31473 = (state_31489[(2)]);
var state_31489__$1 = state_31489;
var statearr_31580_34080 = state_31489__$1;
(statearr_31580_34080[(2)] = inst_31473);

(statearr_31580_34080[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (44))){
var inst_31482 = (state_31489[(2)]);
var state_31489__$1 = (function (){var statearr_31589 = state_31489;
(statearr_31589[(28)] = inst_31482);

return statearr_31589;
})();
var statearr_31591_34081 = state_31489__$1;
(statearr_31591_34081[(2)] = null);

(statearr_31591_34081[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (6))){
var inst_31421 = (state_31489[(29)]);
var inst_31420 = cljs.core.deref(cs);
var inst_31421__$1 = cljs.core.keys(inst_31420);
var inst_31422 = cljs.core.count(inst_31421__$1);
var inst_31423 = cljs.core.reset_BANG_(dctr,inst_31422);
var inst_31428 = cljs.core.seq(inst_31421__$1);
var inst_31429 = inst_31428;
var inst_31430 = null;
var inst_31431 = (0);
var inst_31432 = (0);
var state_31489__$1 = (function (){var statearr_31599 = state_31489;
(statearr_31599[(20)] = inst_31429);

(statearr_31599[(30)] = inst_31423);

(statearr_31599[(29)] = inst_31421__$1);

(statearr_31599[(21)] = inst_31431);

(statearr_31599[(11)] = inst_31432);

(statearr_31599[(12)] = inst_31430);

return statearr_31599;
})();
var statearr_31600_34092 = state_31489__$1;
(statearr_31600_34092[(2)] = null);

(statearr_31600_34092[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (28))){
var inst_31448 = (state_31489[(25)]);
var inst_31429 = (state_31489[(20)]);
var inst_31448__$1 = cljs.core.seq(inst_31429);
var state_31489__$1 = (function (){var statearr_31603 = state_31489;
(statearr_31603[(25)] = inst_31448__$1);

return statearr_31603;
})();
if(inst_31448__$1){
var statearr_31604_34093 = state_31489__$1;
(statearr_31604_34093[(1)] = (33));

} else {
var statearr_31611_34094 = state_31489__$1;
(statearr_31611_34094[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (25))){
var inst_31431 = (state_31489[(21)]);
var inst_31432 = (state_31489[(11)]);
var inst_31434 = (inst_31432 < inst_31431);
var inst_31435 = inst_31434;
var state_31489__$1 = state_31489;
if(cljs.core.truth_(inst_31435)){
var statearr_31614_34098 = state_31489__$1;
(statearr_31614_34098[(1)] = (27));

} else {
var statearr_31615_34099 = state_31489__$1;
(statearr_31615_34099[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (34))){
var state_31489__$1 = state_31489;
var statearr_31616_34100 = state_31489__$1;
(statearr_31616_34100[(2)] = null);

(statearr_31616_34100[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (17))){
var state_31489__$1 = state_31489;
var statearr_31617_34101 = state_31489__$1;
(statearr_31617_34101[(2)] = null);

(statearr_31617_34101[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (3))){
var inst_31487 = (state_31489[(2)]);
var state_31489__$1 = state_31489;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31489__$1,inst_31487);
} else {
if((state_val_31490 === (12))){
var inst_31416 = (state_31489[(2)]);
var state_31489__$1 = state_31489;
var statearr_31618_34110 = state_31489__$1;
(statearr_31618_34110[(2)] = inst_31416);

(statearr_31618_34110[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (2))){
var state_31489__$1 = state_31489;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31489__$1,(4),ch);
} else {
if((state_val_31490 === (23))){
var state_31489__$1 = state_31489;
var statearr_31619_34115 = state_31489__$1;
(statearr_31619_34115[(2)] = null);

(statearr_31619_34115[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (35))){
var inst_31471 = (state_31489[(2)]);
var state_31489__$1 = state_31489;
var statearr_31620_34117 = state_31489__$1;
(statearr_31620_34117[(2)] = inst_31471);

(statearr_31620_34117[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (19))){
var inst_31381 = (state_31489[(7)]);
var inst_31392 = cljs.core.chunk_first(inst_31381);
var inst_31393 = cljs.core.chunk_rest(inst_31381);
var inst_31394 = cljs.core.count(inst_31392);
var inst_31350 = inst_31393;
var inst_31351 = inst_31392;
var inst_31352 = inst_31394;
var inst_31353 = (0);
var state_31489__$1 = (function (){var statearr_31621 = state_31489;
(statearr_31621[(13)] = inst_31352);

(statearr_31621[(14)] = inst_31353);

(statearr_31621[(15)] = inst_31350);

(statearr_31621[(17)] = inst_31351);

return statearr_31621;
})();
var statearr_31622_34118 = state_31489__$1;
(statearr_31622_34118[(2)] = null);

(statearr_31622_34118[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (11))){
var inst_31381 = (state_31489[(7)]);
var inst_31350 = (state_31489[(15)]);
var inst_31381__$1 = cljs.core.seq(inst_31350);
var state_31489__$1 = (function (){var statearr_31623 = state_31489;
(statearr_31623[(7)] = inst_31381__$1);

return statearr_31623;
})();
if(inst_31381__$1){
var statearr_31624_34120 = state_31489__$1;
(statearr_31624_34120[(1)] = (16));

} else {
var statearr_31628_34124 = state_31489__$1;
(statearr_31628_34124[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (9))){
var inst_31418 = (state_31489[(2)]);
var state_31489__$1 = state_31489;
var statearr_31629_34125 = state_31489__$1;
(statearr_31629_34125[(2)] = inst_31418);

(statearr_31629_34125[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (5))){
var inst_31348 = cljs.core.deref(cs);
var inst_31349 = cljs.core.seq(inst_31348);
var inst_31350 = inst_31349;
var inst_31351 = null;
var inst_31352 = (0);
var inst_31353 = (0);
var state_31489__$1 = (function (){var statearr_31630 = state_31489;
(statearr_31630[(13)] = inst_31352);

(statearr_31630[(14)] = inst_31353);

(statearr_31630[(15)] = inst_31350);

(statearr_31630[(17)] = inst_31351);

return statearr_31630;
})();
var statearr_31631_34129 = state_31489__$1;
(statearr_31631_34129[(2)] = null);

(statearr_31631_34129[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (14))){
var state_31489__$1 = state_31489;
var statearr_31634_34130 = state_31489__$1;
(statearr_31634_34130[(2)] = null);

(statearr_31634_34130[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (45))){
var inst_31479 = (state_31489[(2)]);
var state_31489__$1 = state_31489;
var statearr_31635_34131 = state_31489__$1;
(statearr_31635_34131[(2)] = inst_31479);

(statearr_31635_34131[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (26))){
var inst_31421 = (state_31489[(29)]);
var inst_31475 = (state_31489[(2)]);
var inst_31476 = cljs.core.seq(inst_31421);
var state_31489__$1 = (function (){var statearr_31637 = state_31489;
(statearr_31637[(31)] = inst_31475);

return statearr_31637;
})();
if(inst_31476){
var statearr_31638_34132 = state_31489__$1;
(statearr_31638_34132[(1)] = (42));

} else {
var statearr_31639_34133 = state_31489__$1;
(statearr_31639_34133[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (16))){
var inst_31381 = (state_31489[(7)]);
var inst_31386 = cljs.core.chunked_seq_QMARK_(inst_31381);
var state_31489__$1 = state_31489;
if(inst_31386){
var statearr_31642_34134 = state_31489__$1;
(statearr_31642_34134[(1)] = (19));

} else {
var statearr_31643_34135 = state_31489__$1;
(statearr_31643_34135[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (38))){
var inst_31468 = (state_31489[(2)]);
var state_31489__$1 = state_31489;
var statearr_31646_34136 = state_31489__$1;
(statearr_31646_34136[(2)] = inst_31468);

(statearr_31646_34136[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (30))){
var state_31489__$1 = state_31489;
var statearr_31647_34141 = state_31489__$1;
(statearr_31647_34141[(2)] = null);

(statearr_31647_34141[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (10))){
var inst_31353 = (state_31489[(14)]);
var inst_31351 = (state_31489[(17)]);
var inst_31366 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_31351,inst_31353);
var inst_31367 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31366,(0),null);
var inst_31368 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31366,(1),null);
var state_31489__$1 = (function (){var statearr_31653 = state_31489;
(statearr_31653[(26)] = inst_31367);

return statearr_31653;
})();
if(cljs.core.truth_(inst_31368)){
var statearr_31655_34148 = state_31489__$1;
(statearr_31655_34148[(1)] = (13));

} else {
var statearr_31656_34149 = state_31489__$1;
(statearr_31656_34149[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (18))){
var inst_31414 = (state_31489[(2)]);
var state_31489__$1 = state_31489;
var statearr_31657_34150 = state_31489__$1;
(statearr_31657_34150[(2)] = inst_31414);

(statearr_31657_34150[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (42))){
var state_31489__$1 = state_31489;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31489__$1,(45),dchan);
} else {
if((state_val_31490 === (37))){
var inst_31448 = (state_31489[(25)]);
var inst_31341 = (state_31489[(10)]);
var inst_31457 = (state_31489[(23)]);
var inst_31457__$1 = cljs.core.first(inst_31448);
var inst_31458 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_31457__$1,inst_31341,done);
var state_31489__$1 = (function (){var statearr_31658 = state_31489;
(statearr_31658[(23)] = inst_31457__$1);

return statearr_31658;
})();
if(cljs.core.truth_(inst_31458)){
var statearr_31660_34158 = state_31489__$1;
(statearr_31660_34158[(1)] = (39));

} else {
var statearr_31661_34163 = state_31489__$1;
(statearr_31661_34163[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31490 === (8))){
var inst_31352 = (state_31489[(13)]);
var inst_31353 = (state_31489[(14)]);
var inst_31356 = (inst_31353 < inst_31352);
var inst_31357 = inst_31356;
var state_31489__$1 = state_31489;
if(cljs.core.truth_(inst_31357)){
var statearr_31663_34164 = state_31489__$1;
(statearr_31663_34164[(1)] = (10));

} else {
var statearr_31666_34165 = state_31489__$1;
(statearr_31666_34165[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto___34030,cs,m,dchan,dctr,done))
;
return ((function (switch__28688__auto__,c__29782__auto___34030,cs,m,dchan,dctr,done){
return (function() {
var cljs$core$async$mult_$_state_machine__28689__auto__ = null;
var cljs$core$async$mult_$_state_machine__28689__auto____0 = (function (){
var statearr_31670 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_31670[(0)] = cljs$core$async$mult_$_state_machine__28689__auto__);

(statearr_31670[(1)] = (1));

return statearr_31670;
});
var cljs$core$async$mult_$_state_machine__28689__auto____1 = (function (state_31489){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_31489);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e31671){if((e31671 instanceof Object)){
var ex__28692__auto__ = e31671;
var statearr_31672_34166 = state_31489;
(statearr_31672_34166[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31489);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31671;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34167 = state_31489;
state_31489 = G__34167;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__28689__auto__ = function(state_31489){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__28689__auto____1.call(this,state_31489);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__28689__auto____0;
cljs$core$async$mult_$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__28689__auto____1;
return cljs$core$async$mult_$_state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto___34030,cs,m,dchan,dctr,done))
})();
var state__29784__auto__ = (function (){var statearr_31675 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_31675[(6)] = c__29782__auto___34030);

return statearr_31675;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto___34030,cs,m,dchan,dctr,done))
);


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__31679 = arguments.length;
switch (G__31679) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(mult,ch,true);
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_(mult,ch,close_QMARK_);

return ch;
});

cljs.core.async.tap.cljs$lang$maxFixedArity = 3;

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_(mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_(mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4434__auto__.call(null,m,ch));
} else {
var m__4431__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4431__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.admix*",m);
}
}
}
});

cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4434__auto__.call(null,m,ch));
} else {
var m__4431__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4431__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.unmix*",m);
}
}
}
});

cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4434__auto__.call(null,m));
} else {
var m__4431__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4431__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mix.unmix-all*",m);
}
}
}
});

cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4434__auto__.call(null,m,state_map));
} else {
var m__4431__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4431__auto__.call(null,m,state_map));
} else {
throw cljs.core.missing_protocol("Mix.toggle*",m);
}
}
}
});

cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4434__auto__.call(null,m,mode));
} else {
var m__4431__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4431__auto__.call(null,m,mode));
} else {
throw cljs.core.missing_protocol("Mix.solo-mode*",m);
}
}
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__4736__auto__ = [];
var len__4730__auto___34206 = arguments.length;
var i__4731__auto___34207 = (0);
while(true){
if((i__4731__auto___34207 < len__4730__auto___34206)){
args__4736__auto__.push((arguments[i__4731__auto___34207]));

var G__34208 = (i__4731__auto___34207 + (1));
i__4731__auto___34207 = G__34208;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((3) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4737__auto__);
});

cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__31710){
var map__31713 = p__31710;
var map__31713__$1 = (((((!((map__31713 == null))))?(((((map__31713.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31713.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31713):map__31713);
var opts = map__31713__$1;
var statearr_31721_34210 = state;
(statearr_31721_34210[(1)] = cont_block);


var temp__5735__auto__ = cljs.core.async.do_alts(((function (map__31713,map__31713__$1,opts){
return (function (val){
var statearr_31722_34214 = state;
(statearr_31722_34214[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state);
});})(map__31713,map__31713__$1,opts))
,ports,opts);
if(cljs.core.truth_(temp__5735__auto__)){
var cb = temp__5735__auto__;
var statearr_31724_34217 = state;
(statearr_31724_34217[(2)] = cljs.core.deref(cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
});

cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3);

/** @this {Function} */
cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq31704){
var G__31705 = cljs.core.first(seq31704);
var seq31704__$1 = cljs.core.next(seq31704);
var G__31706 = cljs.core.first(seq31704__$1);
var seq31704__$2 = cljs.core.next(seq31704__$1);
var G__31707 = cljs.core.first(seq31704__$2);
var seq31704__$3 = cljs.core.next(seq31704__$2);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__31705,G__31706,G__31707,seq31704__$3);
});

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();
var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;
var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){
return cljs.core.reduce_kv(((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){
if(cljs.core.truth_((attr.cljs$core$IFn$_invoke$arity$1 ? attr.cljs$core$IFn$_invoke$arity$1(v) : attr.call(null,v)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,c);
} else {
return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;
var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){
var chs = cljs.core.deref(cs);
var mode = cljs.core.deref(solo_mode);
var solos = pick(new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick(new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick(new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_(solos))))))?cljs.core.vec(solos):cljs.core.vec(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(pauses,cljs.core.keys(chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async31740 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async31740 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta31741){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta31741 = meta31741;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async31740.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_31742,meta31741__$1){
var self__ = this;
var _31742__$1 = this;
return (new cljs.core.async.t_cljs$core$async31740(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta31741__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31740.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_31742){
var self__ = this;
var _31742__$1 = this;
return self__.meta31741;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31740.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async31740.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31740.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async31740.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31740.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31740.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31740.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.merge_with,cljs.core.merge),state_map);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31740.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.solo_modes.cljs$core$IFn$_invoke$arity$1 ? self__.solo_modes.cljs$core$IFn$_invoke$arity$1(mode) : self__.solo_modes.call(null,mode)))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_(self__.solo_mode,mode);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31740.getBasis = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta31741","meta31741",-1214705404,null)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31740.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async31740.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async31740";

cljs.core.async.t_cljs$core$async31740.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async31740");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async31740.
 */
cljs.core.async.__GT_t_cljs$core$async31740 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function cljs$core$async$mix_$___GT_t_cljs$core$async31740(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta31741){
return (new cljs.core.async.t_cljs$core$async31740(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta31741));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

}

return (new cljs.core.async.t_cljs$core$async31740(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__29782__auto___34252 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto___34252,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto___34252,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_31884){
var state_val_31885 = (state_31884[(1)]);
if((state_val_31885 === (7))){
var inst_31776 = (state_31884[(2)]);
var state_31884__$1 = state_31884;
var statearr_31892_34255 = state_31884__$1;
(statearr_31892_34255[(2)] = inst_31776);

(statearr_31892_34255[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (20))){
var inst_31791 = (state_31884[(7)]);
var state_31884__$1 = state_31884;
var statearr_31893_34256 = state_31884__$1;
(statearr_31893_34256[(2)] = inst_31791);

(statearr_31893_34256[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (27))){
var state_31884__$1 = state_31884;
var statearr_31894_34257 = state_31884__$1;
(statearr_31894_34257[(2)] = null);

(statearr_31894_34257[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (1))){
var inst_31758 = (state_31884[(8)]);
var inst_31758__$1 = calc_state();
var inst_31762 = (inst_31758__$1 == null);
var inst_31763 = cljs.core.not(inst_31762);
var state_31884__$1 = (function (){var statearr_31900 = state_31884;
(statearr_31900[(8)] = inst_31758__$1);

return statearr_31900;
})();
if(inst_31763){
var statearr_31901_34258 = state_31884__$1;
(statearr_31901_34258[(1)] = (2));

} else {
var statearr_31904_34259 = state_31884__$1;
(statearr_31904_34259[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (24))){
var inst_31851 = (state_31884[(9)]);
var inst_31818 = (state_31884[(10)]);
var inst_31831 = (state_31884[(11)]);
var inst_31851__$1 = (inst_31818.cljs$core$IFn$_invoke$arity$1 ? inst_31818.cljs$core$IFn$_invoke$arity$1(inst_31831) : inst_31818.call(null,inst_31831));
var state_31884__$1 = (function (){var statearr_31906 = state_31884;
(statearr_31906[(9)] = inst_31851__$1);

return statearr_31906;
})();
if(cljs.core.truth_(inst_31851__$1)){
var statearr_31907_34260 = state_31884__$1;
(statearr_31907_34260[(1)] = (29));

} else {
var statearr_31908_34261 = state_31884__$1;
(statearr_31908_34261[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (4))){
var inst_31779 = (state_31884[(2)]);
var state_31884__$1 = state_31884;
if(cljs.core.truth_(inst_31779)){
var statearr_31909_34262 = state_31884__$1;
(statearr_31909_34262[(1)] = (8));

} else {
var statearr_31910_34263 = state_31884__$1;
(statearr_31910_34263[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (15))){
var inst_31810 = (state_31884[(2)]);
var state_31884__$1 = state_31884;
if(cljs.core.truth_(inst_31810)){
var statearr_31911_34264 = state_31884__$1;
(statearr_31911_34264[(1)] = (19));

} else {
var statearr_31912_34265 = state_31884__$1;
(statearr_31912_34265[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (21))){
var inst_31817 = (state_31884[(12)]);
var inst_31817__$1 = (state_31884[(2)]);
var inst_31818 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_31817__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_31821 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_31817__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_31822 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_31817__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_31884__$1 = (function (){var statearr_31914 = state_31884;
(statearr_31914[(10)] = inst_31818);

(statearr_31914[(12)] = inst_31817__$1);

(statearr_31914[(13)] = inst_31821);

return statearr_31914;
})();
return cljs.core.async.ioc_alts_BANG_(state_31884__$1,(22),inst_31822);
} else {
if((state_val_31885 === (31))){
var inst_31863 = (state_31884[(2)]);
var state_31884__$1 = state_31884;
if(cljs.core.truth_(inst_31863)){
var statearr_31920_34267 = state_31884__$1;
(statearr_31920_34267[(1)] = (32));

} else {
var statearr_31925_34268 = state_31884__$1;
(statearr_31925_34268[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (32))){
var inst_31830 = (state_31884[(14)]);
var state_31884__$1 = state_31884;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31884__$1,(35),out,inst_31830);
} else {
if((state_val_31885 === (33))){
var inst_31817 = (state_31884[(12)]);
var inst_31791 = inst_31817;
var state_31884__$1 = (function (){var statearr_31931 = state_31884;
(statearr_31931[(7)] = inst_31791);

return statearr_31931;
})();
var statearr_31934_34270 = state_31884__$1;
(statearr_31934_34270[(2)] = null);

(statearr_31934_34270[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (13))){
var inst_31791 = (state_31884[(7)]);
var inst_31799 = inst_31791.cljs$lang$protocol_mask$partition0$;
var inst_31800 = (inst_31799 & (64));
var inst_31801 = inst_31791.cljs$core$ISeq$;
var inst_31802 = (cljs.core.PROTOCOL_SENTINEL === inst_31801);
var inst_31803 = ((inst_31800) || (inst_31802));
var state_31884__$1 = state_31884;
if(cljs.core.truth_(inst_31803)){
var statearr_31938_34271 = state_31884__$1;
(statearr_31938_34271[(1)] = (16));

} else {
var statearr_31939_34272 = state_31884__$1;
(statearr_31939_34272[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (22))){
var inst_31831 = (state_31884[(11)]);
var inst_31830 = (state_31884[(14)]);
var inst_31829 = (state_31884[(2)]);
var inst_31830__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31829,(0),null);
var inst_31831__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31829,(1),null);
var inst_31833 = (inst_31830__$1 == null);
var inst_31834 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_31831__$1,change);
var inst_31835 = ((inst_31833) || (inst_31834));
var state_31884__$1 = (function (){var statearr_31942 = state_31884;
(statearr_31942[(11)] = inst_31831__$1);

(statearr_31942[(14)] = inst_31830__$1);

return statearr_31942;
})();
if(cljs.core.truth_(inst_31835)){
var statearr_31943_34274 = state_31884__$1;
(statearr_31943_34274[(1)] = (23));

} else {
var statearr_31944_34276 = state_31884__$1;
(statearr_31944_34276[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (36))){
var inst_31817 = (state_31884[(12)]);
var inst_31791 = inst_31817;
var state_31884__$1 = (function (){var statearr_31945 = state_31884;
(statearr_31945[(7)] = inst_31791);

return statearr_31945;
})();
var statearr_31946_34277 = state_31884__$1;
(statearr_31946_34277[(2)] = null);

(statearr_31946_34277[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (29))){
var inst_31851 = (state_31884[(9)]);
var state_31884__$1 = state_31884;
var statearr_31947_34282 = state_31884__$1;
(statearr_31947_34282[(2)] = inst_31851);

(statearr_31947_34282[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (6))){
var state_31884__$1 = state_31884;
var statearr_31950_34284 = state_31884__$1;
(statearr_31950_34284[(2)] = false);

(statearr_31950_34284[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (28))){
var inst_31845 = (state_31884[(2)]);
var inst_31847 = calc_state();
var inst_31791 = inst_31847;
var state_31884__$1 = (function (){var statearr_31951 = state_31884;
(statearr_31951[(7)] = inst_31791);

(statearr_31951[(15)] = inst_31845);

return statearr_31951;
})();
var statearr_31952_34285 = state_31884__$1;
(statearr_31952_34285[(2)] = null);

(statearr_31952_34285[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (25))){
var inst_31877 = (state_31884[(2)]);
var state_31884__$1 = state_31884;
var statearr_31953_34286 = state_31884__$1;
(statearr_31953_34286[(2)] = inst_31877);

(statearr_31953_34286[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (34))){
var inst_31875 = (state_31884[(2)]);
var state_31884__$1 = state_31884;
var statearr_31954_34287 = state_31884__$1;
(statearr_31954_34287[(2)] = inst_31875);

(statearr_31954_34287[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (17))){
var state_31884__$1 = state_31884;
var statearr_31955_34288 = state_31884__$1;
(statearr_31955_34288[(2)] = false);

(statearr_31955_34288[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (3))){
var state_31884__$1 = state_31884;
var statearr_31960_34289 = state_31884__$1;
(statearr_31960_34289[(2)] = false);

(statearr_31960_34289[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (12))){
var inst_31879 = (state_31884[(2)]);
var state_31884__$1 = state_31884;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31884__$1,inst_31879);
} else {
if((state_val_31885 === (2))){
var inst_31758 = (state_31884[(8)]);
var inst_31766 = inst_31758.cljs$lang$protocol_mask$partition0$;
var inst_31768 = (inst_31766 & (64));
var inst_31769 = inst_31758.cljs$core$ISeq$;
var inst_31770 = (cljs.core.PROTOCOL_SENTINEL === inst_31769);
var inst_31771 = ((inst_31768) || (inst_31770));
var state_31884__$1 = state_31884;
if(cljs.core.truth_(inst_31771)){
var statearr_31962_34291 = state_31884__$1;
(statearr_31962_34291[(1)] = (5));

} else {
var statearr_31964_34292 = state_31884__$1;
(statearr_31964_34292[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (23))){
var inst_31830 = (state_31884[(14)]);
var inst_31840 = (inst_31830 == null);
var state_31884__$1 = state_31884;
if(cljs.core.truth_(inst_31840)){
var statearr_31965_34294 = state_31884__$1;
(statearr_31965_34294[(1)] = (26));

} else {
var statearr_31966_34295 = state_31884__$1;
(statearr_31966_34295[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (35))){
var inst_31866 = (state_31884[(2)]);
var state_31884__$1 = state_31884;
if(cljs.core.truth_(inst_31866)){
var statearr_31967_34299 = state_31884__$1;
(statearr_31967_34299[(1)] = (36));

} else {
var statearr_31968_34300 = state_31884__$1;
(statearr_31968_34300[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (19))){
var inst_31791 = (state_31884[(7)]);
var inst_31814 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_31791);
var state_31884__$1 = state_31884;
var statearr_31971_34301 = state_31884__$1;
(statearr_31971_34301[(2)] = inst_31814);

(statearr_31971_34301[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (11))){
var inst_31791 = (state_31884[(7)]);
var inst_31796 = (inst_31791 == null);
var inst_31797 = cljs.core.not(inst_31796);
var state_31884__$1 = state_31884;
if(inst_31797){
var statearr_31973_34302 = state_31884__$1;
(statearr_31973_34302[(1)] = (13));

} else {
var statearr_31974_34303 = state_31884__$1;
(statearr_31974_34303[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (9))){
var inst_31758 = (state_31884[(8)]);
var state_31884__$1 = state_31884;
var statearr_31975_34304 = state_31884__$1;
(statearr_31975_34304[(2)] = inst_31758);

(statearr_31975_34304[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (5))){
var state_31884__$1 = state_31884;
var statearr_31976_34305 = state_31884__$1;
(statearr_31976_34305[(2)] = true);

(statearr_31976_34305[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (14))){
var state_31884__$1 = state_31884;
var statearr_31977_34306 = state_31884__$1;
(statearr_31977_34306[(2)] = false);

(statearr_31977_34306[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (26))){
var inst_31831 = (state_31884[(11)]);
var inst_31842 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cs,cljs.core.dissoc,inst_31831);
var state_31884__$1 = state_31884;
var statearr_31978_34307 = state_31884__$1;
(statearr_31978_34307[(2)] = inst_31842);

(statearr_31978_34307[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (16))){
var state_31884__$1 = state_31884;
var statearr_31983_34308 = state_31884__$1;
(statearr_31983_34308[(2)] = true);

(statearr_31983_34308[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (38))){
var inst_31871 = (state_31884[(2)]);
var state_31884__$1 = state_31884;
var statearr_31986_34309 = state_31884__$1;
(statearr_31986_34309[(2)] = inst_31871);

(statearr_31986_34309[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (30))){
var inst_31818 = (state_31884[(10)]);
var inst_31831 = (state_31884[(11)]);
var inst_31821 = (state_31884[(13)]);
var inst_31857 = cljs.core.empty_QMARK_(inst_31818);
var inst_31859 = (inst_31821.cljs$core$IFn$_invoke$arity$1 ? inst_31821.cljs$core$IFn$_invoke$arity$1(inst_31831) : inst_31821.call(null,inst_31831));
var inst_31860 = cljs.core.not(inst_31859);
var inst_31861 = ((inst_31857) && (inst_31860));
var state_31884__$1 = state_31884;
var statearr_31992_34310 = state_31884__$1;
(statearr_31992_34310[(2)] = inst_31861);

(statearr_31992_34310[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (10))){
var inst_31758 = (state_31884[(8)]);
var inst_31786 = (state_31884[(2)]);
var inst_31787 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_31786,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_31789 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_31786,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_31790 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_31786,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_31791 = inst_31758;
var state_31884__$1 = (function (){var statearr_31998 = state_31884;
(statearr_31998[(16)] = inst_31789);

(statearr_31998[(7)] = inst_31791);

(statearr_31998[(17)] = inst_31790);

(statearr_31998[(18)] = inst_31787);

return statearr_31998;
})();
var statearr_32000_34312 = state_31884__$1;
(statearr_32000_34312[(2)] = null);

(statearr_32000_34312[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (18))){
var inst_31807 = (state_31884[(2)]);
var state_31884__$1 = state_31884;
var statearr_32005_34313 = state_31884__$1;
(statearr_32005_34313[(2)] = inst_31807);

(statearr_32005_34313[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (37))){
var state_31884__$1 = state_31884;
var statearr_32006_34314 = state_31884__$1;
(statearr_32006_34314[(2)] = null);

(statearr_32006_34314[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31885 === (8))){
var inst_31758 = (state_31884[(8)]);
var inst_31782 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_31758);
var state_31884__$1 = state_31884;
var statearr_32007_34315 = state_31884__$1;
(statearr_32007_34315[(2)] = inst_31782);

(statearr_32007_34315[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto___34252,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;
return ((function (switch__28688__auto__,c__29782__auto___34252,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var cljs$core$async$mix_$_state_machine__28689__auto__ = null;
var cljs$core$async$mix_$_state_machine__28689__auto____0 = (function (){
var statearr_32010 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32010[(0)] = cljs$core$async$mix_$_state_machine__28689__auto__);

(statearr_32010[(1)] = (1));

return statearr_32010;
});
var cljs$core$async$mix_$_state_machine__28689__auto____1 = (function (state_31884){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_31884);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e32011){if((e32011 instanceof Object)){
var ex__28692__auto__ = e32011;
var statearr_32012_34319 = state_31884;
(statearr_32012_34319[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31884);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32011;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34322 = state_31884;
state_31884 = G__34322;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__28689__auto__ = function(state_31884){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__28689__auto____1.call(this,state_31884);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__28689__auto____0;
cljs$core$async$mix_$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__28689__auto____1;
return cljs$core$async$mix_$_state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto___34252,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();
var state__29784__auto__ = (function (){var statearr_32013 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_32013[(6)] = c__29782__auto___34252);

return statearr_32013;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto___34252,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_(mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_(mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_(mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_(mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_(mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4434__auto__.call(null,p,v,ch,close_QMARK_));
} else {
var m__4431__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4431__auto__.call(null,p,v,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Pub.sub*",p);
}
}
}
});

cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4434__auto__.call(null,p,v,ch));
} else {
var m__4431__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4431__auto__.call(null,p,v,ch));
} else {
throw cljs.core.missing_protocol("Pub.unsub*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__32040 = arguments.length;
switch (G__32040) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4434__auto__.call(null,p));
} else {
var m__4431__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4431__auto__.call(null,p));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4434__auto__.call(null,p,v));
} else {
var m__4431__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4431__auto__.call(null,p,v));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2;


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__32055 = arguments.length;
switch (G__32055) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3(ch,topic_fn,cljs.core.constantly(null));
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = ((function (mults){
return (function (topic){
var or__4131__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(mults),topic);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(mults,((function (or__4131__auto__,mults){
return (function (p1__32050_SHARP_){
if(cljs.core.truth_((p1__32050_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p1__32050_SHARP_.cljs$core$IFn$_invoke$arity$1(topic) : p1__32050_SHARP_.call(null,topic)))){
return p1__32050_SHARP_;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__32050_SHARP_,topic,cljs.core.async.mult(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((buf_fn.cljs$core$IFn$_invoke$arity$1 ? buf_fn.cljs$core$IFn$_invoke$arity$1(topic) : buf_fn.call(null,topic)))));
}
});})(or__4131__auto__,mults))
),topic);
}
});})(mults))
;
var p = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32073 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32073 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta32074){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta32074 = meta32074;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async32073.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_32075,meta32074__$1){
var self__ = this;
var _32075__$1 = this;
return (new cljs.core.async.t_cljs$core$async32073(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta32074__$1));
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32073.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_32075){
var self__ = this;
var _32075__$1 = this;
return self__.meta32074;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32073.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32073.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32073.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32073.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = (self__.ensure_mult.cljs$core$IFn$_invoke$arity$1 ? self__.ensure_mult.cljs$core$IFn$_invoke$arity$1(topic) : self__.ensure_mult.call(null,topic));
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(m,ch__$1,close_QMARK_);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32073.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5735__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self__.mults),topic);
if(cljs.core.truth_(temp__5735__auto__)){
var m = temp__5735__auto__;
return cljs.core.async.untap(m,ch__$1);
} else {
return null;
}
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32073.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_(self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32073.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32073.getBasis = ((function (mults,ensure_mult){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta32074","meta32074",1175675589,null)], null);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32073.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async32073.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32073";

cljs.core.async.t_cljs$core$async32073.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async32073");
});})(mults,ensure_mult))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32073.
 */
cljs.core.async.__GT_t_cljs$core$async32073 = ((function (mults,ensure_mult){
return (function cljs$core$async$__GT_t_cljs$core$async32073(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta32074){
return (new cljs.core.async.t_cljs$core$async32073(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta32074));
});})(mults,ensure_mult))
;

}

return (new cljs.core.async.t_cljs$core$async32073(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__29782__auto___34343 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto___34343,mults,ensure_mult,p){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto___34343,mults,ensure_mult,p){
return (function (state_32198){
var state_val_32199 = (state_32198[(1)]);
if((state_val_32199 === (7))){
var inst_32190 = (state_32198[(2)]);
var state_32198__$1 = state_32198;
var statearr_32202_34344 = state_32198__$1;
(statearr_32202_34344[(2)] = inst_32190);

(statearr_32202_34344[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (20))){
var state_32198__$1 = state_32198;
var statearr_32203_34346 = state_32198__$1;
(statearr_32203_34346[(2)] = null);

(statearr_32203_34346[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (1))){
var state_32198__$1 = state_32198;
var statearr_32208_34347 = state_32198__$1;
(statearr_32208_34347[(2)] = null);

(statearr_32208_34347[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (24))){
var inst_32172 = (state_32198[(7)]);
var inst_32182 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(mults,cljs.core.dissoc,inst_32172);
var state_32198__$1 = state_32198;
var statearr_32216_34349 = state_32198__$1;
(statearr_32216_34349[(2)] = inst_32182);

(statearr_32216_34349[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (4))){
var inst_32110 = (state_32198[(8)]);
var inst_32110__$1 = (state_32198[(2)]);
var inst_32111 = (inst_32110__$1 == null);
var state_32198__$1 = (function (){var statearr_32217 = state_32198;
(statearr_32217[(8)] = inst_32110__$1);

return statearr_32217;
})();
if(cljs.core.truth_(inst_32111)){
var statearr_32218_34353 = state_32198__$1;
(statearr_32218_34353[(1)] = (5));

} else {
var statearr_32219_34354 = state_32198__$1;
(statearr_32219_34354[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (15))){
var inst_32166 = (state_32198[(2)]);
var state_32198__$1 = state_32198;
var statearr_32220_34355 = state_32198__$1;
(statearr_32220_34355[(2)] = inst_32166);

(statearr_32220_34355[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (21))){
var inst_32187 = (state_32198[(2)]);
var state_32198__$1 = (function (){var statearr_32221 = state_32198;
(statearr_32221[(9)] = inst_32187);

return statearr_32221;
})();
var statearr_32222_34358 = state_32198__$1;
(statearr_32222_34358[(2)] = null);

(statearr_32222_34358[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (13))){
var inst_32135 = (state_32198[(10)]);
var inst_32145 = cljs.core.chunked_seq_QMARK_(inst_32135);
var state_32198__$1 = state_32198;
if(inst_32145){
var statearr_32223_34359 = state_32198__$1;
(statearr_32223_34359[(1)] = (16));

} else {
var statearr_32224_34360 = state_32198__$1;
(statearr_32224_34360[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (22))){
var inst_32179 = (state_32198[(2)]);
var state_32198__$1 = state_32198;
if(cljs.core.truth_(inst_32179)){
var statearr_32225_34361 = state_32198__$1;
(statearr_32225_34361[(1)] = (23));

} else {
var statearr_32228_34362 = state_32198__$1;
(statearr_32228_34362[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (6))){
var inst_32110 = (state_32198[(8)]);
var inst_32174 = (state_32198[(11)]);
var inst_32172 = (state_32198[(7)]);
var inst_32172__$1 = (topic_fn.cljs$core$IFn$_invoke$arity$1 ? topic_fn.cljs$core$IFn$_invoke$arity$1(inst_32110) : topic_fn.call(null,inst_32110));
var inst_32173 = cljs.core.deref(mults);
var inst_32174__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32173,inst_32172__$1);
var state_32198__$1 = (function (){var statearr_32231 = state_32198;
(statearr_32231[(11)] = inst_32174__$1);

(statearr_32231[(7)] = inst_32172__$1);

return statearr_32231;
})();
if(cljs.core.truth_(inst_32174__$1)){
var statearr_32235_34366 = state_32198__$1;
(statearr_32235_34366[(1)] = (19));

} else {
var statearr_32239_34367 = state_32198__$1;
(statearr_32239_34367[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (25))){
var inst_32184 = (state_32198[(2)]);
var state_32198__$1 = state_32198;
var statearr_32240_34368 = state_32198__$1;
(statearr_32240_34368[(2)] = inst_32184);

(statearr_32240_34368[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (17))){
var inst_32135 = (state_32198[(10)]);
var inst_32155 = cljs.core.first(inst_32135);
var inst_32156 = cljs.core.async.muxch_STAR_(inst_32155);
var inst_32157 = cljs.core.async.close_BANG_(inst_32156);
var inst_32158 = cljs.core.next(inst_32135);
var inst_32120 = inst_32158;
var inst_32121 = null;
var inst_32122 = (0);
var inst_32123 = (0);
var state_32198__$1 = (function (){var statearr_32241 = state_32198;
(statearr_32241[(12)] = inst_32157);

(statearr_32241[(13)] = inst_32121);

(statearr_32241[(14)] = inst_32123);

(statearr_32241[(15)] = inst_32122);

(statearr_32241[(16)] = inst_32120);

return statearr_32241;
})();
var statearr_32242_34370 = state_32198__$1;
(statearr_32242_34370[(2)] = null);

(statearr_32242_34370[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (3))){
var inst_32192 = (state_32198[(2)]);
var state_32198__$1 = state_32198;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32198__$1,inst_32192);
} else {
if((state_val_32199 === (12))){
var inst_32168 = (state_32198[(2)]);
var state_32198__$1 = state_32198;
var statearr_32243_34373 = state_32198__$1;
(statearr_32243_34373[(2)] = inst_32168);

(statearr_32243_34373[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (2))){
var state_32198__$1 = state_32198;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32198__$1,(4),ch);
} else {
if((state_val_32199 === (23))){
var state_32198__$1 = state_32198;
var statearr_32245_34376 = state_32198__$1;
(statearr_32245_34376[(2)] = null);

(statearr_32245_34376[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (19))){
var inst_32110 = (state_32198[(8)]);
var inst_32174 = (state_32198[(11)]);
var inst_32177 = cljs.core.async.muxch_STAR_(inst_32174);
var state_32198__$1 = state_32198;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32198__$1,(22),inst_32177,inst_32110);
} else {
if((state_val_32199 === (11))){
var inst_32135 = (state_32198[(10)]);
var inst_32120 = (state_32198[(16)]);
var inst_32135__$1 = cljs.core.seq(inst_32120);
var state_32198__$1 = (function (){var statearr_32246 = state_32198;
(statearr_32246[(10)] = inst_32135__$1);

return statearr_32246;
})();
if(inst_32135__$1){
var statearr_32247_34380 = state_32198__$1;
(statearr_32247_34380[(1)] = (13));

} else {
var statearr_32248_34381 = state_32198__$1;
(statearr_32248_34381[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (9))){
var inst_32170 = (state_32198[(2)]);
var state_32198__$1 = state_32198;
var statearr_32253_34382 = state_32198__$1;
(statearr_32253_34382[(2)] = inst_32170);

(statearr_32253_34382[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (5))){
var inst_32117 = cljs.core.deref(mults);
var inst_32118 = cljs.core.vals(inst_32117);
var inst_32119 = cljs.core.seq(inst_32118);
var inst_32120 = inst_32119;
var inst_32121 = null;
var inst_32122 = (0);
var inst_32123 = (0);
var state_32198__$1 = (function (){var statearr_32254 = state_32198;
(statearr_32254[(13)] = inst_32121);

(statearr_32254[(14)] = inst_32123);

(statearr_32254[(15)] = inst_32122);

(statearr_32254[(16)] = inst_32120);

return statearr_32254;
})();
var statearr_32256_34390 = state_32198__$1;
(statearr_32256_34390[(2)] = null);

(statearr_32256_34390[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (14))){
var state_32198__$1 = state_32198;
var statearr_32260_34394 = state_32198__$1;
(statearr_32260_34394[(2)] = null);

(statearr_32260_34394[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (16))){
var inst_32135 = (state_32198[(10)]);
var inst_32149 = cljs.core.chunk_first(inst_32135);
var inst_32150 = cljs.core.chunk_rest(inst_32135);
var inst_32151 = cljs.core.count(inst_32149);
var inst_32120 = inst_32150;
var inst_32121 = inst_32149;
var inst_32122 = inst_32151;
var inst_32123 = (0);
var state_32198__$1 = (function (){var statearr_32262 = state_32198;
(statearr_32262[(13)] = inst_32121);

(statearr_32262[(14)] = inst_32123);

(statearr_32262[(15)] = inst_32122);

(statearr_32262[(16)] = inst_32120);

return statearr_32262;
})();
var statearr_32263_34401 = state_32198__$1;
(statearr_32263_34401[(2)] = null);

(statearr_32263_34401[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (10))){
var inst_32121 = (state_32198[(13)]);
var inst_32123 = (state_32198[(14)]);
var inst_32122 = (state_32198[(15)]);
var inst_32120 = (state_32198[(16)]);
var inst_32128 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_32121,inst_32123);
var inst_32129 = cljs.core.async.muxch_STAR_(inst_32128);
var inst_32130 = cljs.core.async.close_BANG_(inst_32129);
var inst_32131 = (inst_32123 + (1));
var tmp32257 = inst_32121;
var tmp32258 = inst_32122;
var tmp32259 = inst_32120;
var inst_32120__$1 = tmp32259;
var inst_32121__$1 = tmp32257;
var inst_32122__$1 = tmp32258;
var inst_32123__$1 = inst_32131;
var state_32198__$1 = (function (){var statearr_32264 = state_32198;
(statearr_32264[(17)] = inst_32130);

(statearr_32264[(13)] = inst_32121__$1);

(statearr_32264[(14)] = inst_32123__$1);

(statearr_32264[(15)] = inst_32122__$1);

(statearr_32264[(16)] = inst_32120__$1);

return statearr_32264;
})();
var statearr_32265_34408 = state_32198__$1;
(statearr_32265_34408[(2)] = null);

(statearr_32265_34408[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (18))){
var inst_32163 = (state_32198[(2)]);
var state_32198__$1 = state_32198;
var statearr_32267_34409 = state_32198__$1;
(statearr_32267_34409[(2)] = inst_32163);

(statearr_32267_34409[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32199 === (8))){
var inst_32123 = (state_32198[(14)]);
var inst_32122 = (state_32198[(15)]);
var inst_32125 = (inst_32123 < inst_32122);
var inst_32126 = inst_32125;
var state_32198__$1 = state_32198;
if(cljs.core.truth_(inst_32126)){
var statearr_32268_34410 = state_32198__$1;
(statearr_32268_34410[(1)] = (10));

} else {
var statearr_32269_34413 = state_32198__$1;
(statearr_32269_34413[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto___34343,mults,ensure_mult,p))
;
return ((function (switch__28688__auto__,c__29782__auto___34343,mults,ensure_mult,p){
return (function() {
var cljs$core$async$state_machine__28689__auto__ = null;
var cljs$core$async$state_machine__28689__auto____0 = (function (){
var statearr_32270 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32270[(0)] = cljs$core$async$state_machine__28689__auto__);

(statearr_32270[(1)] = (1));

return statearr_32270;
});
var cljs$core$async$state_machine__28689__auto____1 = (function (state_32198){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_32198);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e32271){if((e32271 instanceof Object)){
var ex__28692__auto__ = e32271;
var statearr_32273_34426 = state_32198;
(statearr_32273_34426[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_32198);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32271;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34429 = state_32198;
state_32198 = G__34429;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$state_machine__28689__auto__ = function(state_32198){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28689__auto____1.call(this,state_32198);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28689__auto____0;
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28689__auto____1;
return cljs$core$async$state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto___34343,mults,ensure_mult,p))
})();
var state__29784__auto__ = (function (){var statearr_32274 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_32274[(6)] = c__29782__auto___34343);

return statearr_32274;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto___34343,mults,ensure_mult,p))
);


return p;
});

cljs.core.async.pub.cljs$lang$maxFixedArity = 3;

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__32277 = arguments.length;
switch (G__32277) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4(p,topic,ch,true);
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_(p,topic,ch,close_QMARK_);
});

cljs.core.async.sub.cljs$lang$maxFixedArity = 4;

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_(p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__32284 = arguments.length;
switch (G__32284) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1(p);
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2(p,topic);
});

cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2;

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__32296 = arguments.length;
switch (G__32296) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3(f,chs,null);
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec(chs);
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var cnt = cljs.core.count(chs__$1);
var rets = cljs.core.object_array.cljs$core$IFn$_invoke$arity$1(cnt);
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2(((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){
return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,rets.slice((0)));
} else {
return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.cljs$core$IFn$_invoke$arity$1(cnt));
var c__29782__auto___34495 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto___34495,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto___34495,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_32374){
var state_val_32375 = (state_32374[(1)]);
if((state_val_32375 === (7))){
var state_32374__$1 = state_32374;
var statearr_32379_34496 = state_32374__$1;
(statearr_32379_34496[(2)] = null);

(statearr_32379_34496[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32375 === (1))){
var state_32374__$1 = state_32374;
var statearr_32380_34497 = state_32374__$1;
(statearr_32380_34497[(2)] = null);

(statearr_32380_34497[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32375 === (4))){
var inst_32324 = (state_32374[(7)]);
var inst_32326 = (inst_32324 < cnt);
var state_32374__$1 = state_32374;
if(cljs.core.truth_(inst_32326)){
var statearr_32383_34501 = state_32374__$1;
(statearr_32383_34501[(1)] = (6));

} else {
var statearr_32384_34509 = state_32374__$1;
(statearr_32384_34509[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32375 === (15))){
var inst_32369 = (state_32374[(2)]);
var state_32374__$1 = state_32374;
var statearr_32385_34510 = state_32374__$1;
(statearr_32385_34510[(2)] = inst_32369);

(statearr_32385_34510[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32375 === (13))){
var inst_32362 = cljs.core.async.close_BANG_(out);
var state_32374__$1 = state_32374;
var statearr_32386_34511 = state_32374__$1;
(statearr_32386_34511[(2)] = inst_32362);

(statearr_32386_34511[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32375 === (6))){
var state_32374__$1 = state_32374;
var statearr_32387_34512 = state_32374__$1;
(statearr_32387_34512[(2)] = null);

(statearr_32387_34512[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32375 === (3))){
var inst_32371 = (state_32374[(2)]);
var state_32374__$1 = state_32374;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32374__$1,inst_32371);
} else {
if((state_val_32375 === (12))){
var inst_32358 = (state_32374[(8)]);
var inst_32358__$1 = (state_32374[(2)]);
var inst_32359 = cljs.core.some(cljs.core.nil_QMARK_,inst_32358__$1);
var state_32374__$1 = (function (){var statearr_32391 = state_32374;
(statearr_32391[(8)] = inst_32358__$1);

return statearr_32391;
})();
if(cljs.core.truth_(inst_32359)){
var statearr_32396_34513 = state_32374__$1;
(statearr_32396_34513[(1)] = (13));

} else {
var statearr_32397_34514 = state_32374__$1;
(statearr_32397_34514[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32375 === (2))){
var inst_32323 = cljs.core.reset_BANG_(dctr,cnt);
var inst_32324 = (0);
var state_32374__$1 = (function (){var statearr_32399 = state_32374;
(statearr_32399[(7)] = inst_32324);

(statearr_32399[(9)] = inst_32323);

return statearr_32399;
})();
var statearr_32401_34515 = state_32374__$1;
(statearr_32401_34515[(2)] = null);

(statearr_32401_34515[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32375 === (11))){
var inst_32324 = (state_32374[(7)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame(state_32374,(10),Object,null,(9));
var inst_32342 = (chs__$1.cljs$core$IFn$_invoke$arity$1 ? chs__$1.cljs$core$IFn$_invoke$arity$1(inst_32324) : chs__$1.call(null,inst_32324));
var inst_32343 = (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(inst_32324) : done.call(null,inst_32324));
var inst_32344 = cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2(inst_32342,inst_32343);
var state_32374__$1 = state_32374;
var statearr_32407_34516 = state_32374__$1;
(statearr_32407_34516[(2)] = inst_32344);


cljs.core.async.impl.ioc_helpers.process_exception(state_32374__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32375 === (9))){
var inst_32324 = (state_32374[(7)]);
var inst_32347 = (state_32374[(2)]);
var inst_32349 = (inst_32324 + (1));
var inst_32324__$1 = inst_32349;
var state_32374__$1 = (function (){var statearr_32408 = state_32374;
(statearr_32408[(10)] = inst_32347);

(statearr_32408[(7)] = inst_32324__$1);

return statearr_32408;
})();
var statearr_32409_34517 = state_32374__$1;
(statearr_32409_34517[(2)] = null);

(statearr_32409_34517[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32375 === (5))){
var inst_32356 = (state_32374[(2)]);
var state_32374__$1 = (function (){var statearr_32411 = state_32374;
(statearr_32411[(11)] = inst_32356);

return statearr_32411;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32374__$1,(12),dchan);
} else {
if((state_val_32375 === (14))){
var inst_32358 = (state_32374[(8)]);
var inst_32364 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,inst_32358);
var state_32374__$1 = state_32374;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32374__$1,(16),out,inst_32364);
} else {
if((state_val_32375 === (16))){
var inst_32366 = (state_32374[(2)]);
var state_32374__$1 = (function (){var statearr_32415 = state_32374;
(statearr_32415[(12)] = inst_32366);

return statearr_32415;
})();
var statearr_32416_34525 = state_32374__$1;
(statearr_32416_34525[(2)] = null);

(statearr_32416_34525[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32375 === (10))){
var inst_32334 = (state_32374[(2)]);
var inst_32335 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec);
var state_32374__$1 = (function (){var statearr_32417 = state_32374;
(statearr_32417[(13)] = inst_32334);

return statearr_32417;
})();
var statearr_32418_34527 = state_32374__$1;
(statearr_32418_34527[(2)] = inst_32335);


cljs.core.async.impl.ioc_helpers.process_exception(state_32374__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32375 === (8))){
var inst_32353 = (state_32374[(2)]);
var state_32374__$1 = state_32374;
var statearr_32419_34529 = state_32374__$1;
(statearr_32419_34529[(2)] = inst_32353);

(statearr_32419_34529[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto___34495,chs__$1,out,cnt,rets,dchan,dctr,done))
;
return ((function (switch__28688__auto__,c__29782__auto___34495,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var cljs$core$async$state_machine__28689__auto__ = null;
var cljs$core$async$state_machine__28689__auto____0 = (function (){
var statearr_32420 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32420[(0)] = cljs$core$async$state_machine__28689__auto__);

(statearr_32420[(1)] = (1));

return statearr_32420;
});
var cljs$core$async$state_machine__28689__auto____1 = (function (state_32374){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_32374);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e32421){if((e32421 instanceof Object)){
var ex__28692__auto__ = e32421;
var statearr_32422_34535 = state_32374;
(statearr_32422_34535[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_32374);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32421;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34536 = state_32374;
state_32374 = G__34536;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$state_machine__28689__auto__ = function(state_32374){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28689__auto____1.call(this,state_32374);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28689__auto____0;
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28689__auto____1;
return cljs$core$async$state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto___34495,chs__$1,out,cnt,rets,dchan,dctr,done))
})();
var state__29784__auto__ = (function (){var statearr_32423 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_32423[(6)] = c__29782__auto___34495);

return statearr_32423;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto___34495,chs__$1,out,cnt,rets,dchan,dctr,done))
);


return out;
});

cljs.core.async.map.cljs$lang$maxFixedArity = 3;

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__32429 = arguments.length;
switch (G__32429) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2(chs,null);
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__29782__auto___34542 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto___34542,out){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto___34542,out){
return (function (state_32468){
var state_val_32469 = (state_32468[(1)]);
if((state_val_32469 === (7))){
var inst_32446 = (state_32468[(7)]);
var inst_32447 = (state_32468[(8)]);
var inst_32446__$1 = (state_32468[(2)]);
var inst_32447__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32446__$1,(0),null);
var inst_32448 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32446__$1,(1),null);
var inst_32449 = (inst_32447__$1 == null);
var state_32468__$1 = (function (){var statearr_32473 = state_32468;
(statearr_32473[(7)] = inst_32446__$1);

(statearr_32473[(9)] = inst_32448);

(statearr_32473[(8)] = inst_32447__$1);

return statearr_32473;
})();
if(cljs.core.truth_(inst_32449)){
var statearr_32477_34543 = state_32468__$1;
(statearr_32477_34543[(1)] = (8));

} else {
var statearr_32478_34544 = state_32468__$1;
(statearr_32478_34544[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32469 === (1))){
var inst_32431 = cljs.core.vec(chs);
var inst_32432 = inst_32431;
var state_32468__$1 = (function (){var statearr_32479 = state_32468;
(statearr_32479[(10)] = inst_32432);

return statearr_32479;
})();
var statearr_32481_34545 = state_32468__$1;
(statearr_32481_34545[(2)] = null);

(statearr_32481_34545[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32469 === (4))){
var inst_32432 = (state_32468[(10)]);
var state_32468__$1 = state_32468;
return cljs.core.async.ioc_alts_BANG_(state_32468__$1,(7),inst_32432);
} else {
if((state_val_32469 === (6))){
var inst_32464 = (state_32468[(2)]);
var state_32468__$1 = state_32468;
var statearr_32484_34552 = state_32468__$1;
(statearr_32484_34552[(2)] = inst_32464);

(statearr_32484_34552[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32469 === (3))){
var inst_32466 = (state_32468[(2)]);
var state_32468__$1 = state_32468;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32468__$1,inst_32466);
} else {
if((state_val_32469 === (2))){
var inst_32432 = (state_32468[(10)]);
var inst_32439 = cljs.core.count(inst_32432);
var inst_32440 = (inst_32439 > (0));
var state_32468__$1 = state_32468;
if(cljs.core.truth_(inst_32440)){
var statearr_32488_34555 = state_32468__$1;
(statearr_32488_34555[(1)] = (4));

} else {
var statearr_32489_34556 = state_32468__$1;
(statearr_32489_34556[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32469 === (11))){
var inst_32432 = (state_32468[(10)]);
var inst_32457 = (state_32468[(2)]);
var tmp32486 = inst_32432;
var inst_32432__$1 = tmp32486;
var state_32468__$1 = (function (){var statearr_32493 = state_32468;
(statearr_32493[(11)] = inst_32457);

(statearr_32493[(10)] = inst_32432__$1);

return statearr_32493;
})();
var statearr_32494_34558 = state_32468__$1;
(statearr_32494_34558[(2)] = null);

(statearr_32494_34558[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32469 === (9))){
var inst_32447 = (state_32468[(8)]);
var state_32468__$1 = state_32468;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32468__$1,(11),out,inst_32447);
} else {
if((state_val_32469 === (5))){
var inst_32462 = cljs.core.async.close_BANG_(out);
var state_32468__$1 = state_32468;
var statearr_32529_34559 = state_32468__$1;
(statearr_32529_34559[(2)] = inst_32462);

(statearr_32529_34559[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32469 === (10))){
var inst_32460 = (state_32468[(2)]);
var state_32468__$1 = state_32468;
var statearr_32534_34562 = state_32468__$1;
(statearr_32534_34562[(2)] = inst_32460);

(statearr_32534_34562[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32469 === (8))){
var inst_32446 = (state_32468[(7)]);
var inst_32448 = (state_32468[(9)]);
var inst_32432 = (state_32468[(10)]);
var inst_32447 = (state_32468[(8)]);
var inst_32452 = (function (){var cs = inst_32432;
var vec__32442 = inst_32446;
var v = inst_32447;
var c = inst_32448;
return ((function (cs,vec__32442,v,c,inst_32446,inst_32448,inst_32432,inst_32447,state_val_32469,c__29782__auto___34542,out){
return (function (p1__32427_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(c,p1__32427_SHARP_);
});
;})(cs,vec__32442,v,c,inst_32446,inst_32448,inst_32432,inst_32447,state_val_32469,c__29782__auto___34542,out))
})();
var inst_32453 = cljs.core.filterv(inst_32452,inst_32432);
var inst_32432__$1 = inst_32453;
var state_32468__$1 = (function (){var statearr_32543 = state_32468;
(statearr_32543[(10)] = inst_32432__$1);

return statearr_32543;
})();
var statearr_32544_34566 = state_32468__$1;
(statearr_32544_34566[(2)] = null);

(statearr_32544_34566[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto___34542,out))
;
return ((function (switch__28688__auto__,c__29782__auto___34542,out){
return (function() {
var cljs$core$async$state_machine__28689__auto__ = null;
var cljs$core$async$state_machine__28689__auto____0 = (function (){
var statearr_32549 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32549[(0)] = cljs$core$async$state_machine__28689__auto__);

(statearr_32549[(1)] = (1));

return statearr_32549;
});
var cljs$core$async$state_machine__28689__auto____1 = (function (state_32468){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_32468);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e32550){if((e32550 instanceof Object)){
var ex__28692__auto__ = e32550;
var statearr_32551_34568 = state_32468;
(statearr_32551_34568[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_32468);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32550;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34570 = state_32468;
state_32468 = G__34570;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$state_machine__28689__auto__ = function(state_32468){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28689__auto____1.call(this,state_32468);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28689__auto____0;
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28689__auto____1;
return cljs$core$async$state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto___34542,out))
})();
var state__29784__auto__ = (function (){var statearr_32556 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_32556[(6)] = c__29782__auto___34542);

return statearr_32556;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto___34542,out))
);


return out;
});

cljs.core.async.merge.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce(cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__32574 = arguments.length;
switch (G__32574) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3(n,ch,null);
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__29782__auto___34582 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto___34582,out){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto___34582,out){
return (function (state_32606){
var state_val_32607 = (state_32606[(1)]);
if((state_val_32607 === (7))){
var inst_32588 = (state_32606[(7)]);
var inst_32588__$1 = (state_32606[(2)]);
var inst_32589 = (inst_32588__$1 == null);
var inst_32590 = cljs.core.not(inst_32589);
var state_32606__$1 = (function (){var statearr_32609 = state_32606;
(statearr_32609[(7)] = inst_32588__$1);

return statearr_32609;
})();
if(inst_32590){
var statearr_32612_34583 = state_32606__$1;
(statearr_32612_34583[(1)] = (8));

} else {
var statearr_32614_34584 = state_32606__$1;
(statearr_32614_34584[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32607 === (1))){
var inst_32583 = (0);
var state_32606__$1 = (function (){var statearr_32616 = state_32606;
(statearr_32616[(8)] = inst_32583);

return statearr_32616;
})();
var statearr_32619_34585 = state_32606__$1;
(statearr_32619_34585[(2)] = null);

(statearr_32619_34585[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32607 === (4))){
var state_32606__$1 = state_32606;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32606__$1,(7),ch);
} else {
if((state_val_32607 === (6))){
var inst_32601 = (state_32606[(2)]);
var state_32606__$1 = state_32606;
var statearr_32625_34587 = state_32606__$1;
(statearr_32625_34587[(2)] = inst_32601);

(statearr_32625_34587[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32607 === (3))){
var inst_32603 = (state_32606[(2)]);
var inst_32604 = cljs.core.async.close_BANG_(out);
var state_32606__$1 = (function (){var statearr_32626 = state_32606;
(statearr_32626[(9)] = inst_32603);

return statearr_32626;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_32606__$1,inst_32604);
} else {
if((state_val_32607 === (2))){
var inst_32583 = (state_32606[(8)]);
var inst_32585 = (inst_32583 < n);
var state_32606__$1 = state_32606;
if(cljs.core.truth_(inst_32585)){
var statearr_32629_34591 = state_32606__$1;
(statearr_32629_34591[(1)] = (4));

} else {
var statearr_32632_34592 = state_32606__$1;
(statearr_32632_34592[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32607 === (11))){
var inst_32583 = (state_32606[(8)]);
var inst_32593 = (state_32606[(2)]);
var inst_32594 = (inst_32583 + (1));
var inst_32583__$1 = inst_32594;
var state_32606__$1 = (function (){var statearr_32635 = state_32606;
(statearr_32635[(10)] = inst_32593);

(statearr_32635[(8)] = inst_32583__$1);

return statearr_32635;
})();
var statearr_32637_34594 = state_32606__$1;
(statearr_32637_34594[(2)] = null);

(statearr_32637_34594[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32607 === (9))){
var state_32606__$1 = state_32606;
var statearr_32641_34595 = state_32606__$1;
(statearr_32641_34595[(2)] = null);

(statearr_32641_34595[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32607 === (5))){
var state_32606__$1 = state_32606;
var statearr_32644_34597 = state_32606__$1;
(statearr_32644_34597[(2)] = null);

(statearr_32644_34597[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32607 === (10))){
var inst_32598 = (state_32606[(2)]);
var state_32606__$1 = state_32606;
var statearr_32648_34598 = state_32606__$1;
(statearr_32648_34598[(2)] = inst_32598);

(statearr_32648_34598[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32607 === (8))){
var inst_32588 = (state_32606[(7)]);
var state_32606__$1 = state_32606;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32606__$1,(11),out,inst_32588);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto___34582,out))
;
return ((function (switch__28688__auto__,c__29782__auto___34582,out){
return (function() {
var cljs$core$async$state_machine__28689__auto__ = null;
var cljs$core$async$state_machine__28689__auto____0 = (function (){
var statearr_32655 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_32655[(0)] = cljs$core$async$state_machine__28689__auto__);

(statearr_32655[(1)] = (1));

return statearr_32655;
});
var cljs$core$async$state_machine__28689__auto____1 = (function (state_32606){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_32606);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e32661){if((e32661 instanceof Object)){
var ex__28692__auto__ = e32661;
var statearr_32663_34599 = state_32606;
(statearr_32663_34599[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_32606);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32661;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34600 = state_32606;
state_32606 = G__34600;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$state_machine__28689__auto__ = function(state_32606){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28689__auto____1.call(this,state_32606);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28689__auto____0;
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28689__auto____1;
return cljs$core$async$state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto___34582,out))
})();
var state__29784__auto__ = (function (){var statearr_32664 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_32664[(6)] = c__29782__auto___34582);

return statearr_32664;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto___34582,out))
);


return out;
});

cljs.core.async.take.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32669 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32669 = (function (f,ch,meta32670){
this.f = f;
this.ch = ch;
this.meta32670 = meta32670;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async32669.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32671,meta32670__$1){
var self__ = this;
var _32671__$1 = this;
return (new cljs.core.async.t_cljs$core$async32669(self__.f,self__.ch,meta32670__$1));
});

cljs.core.async.t_cljs$core$async32669.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32671){
var self__ = this;
var _32671__$1 = this;
return self__.meta32670;
});

cljs.core.async.t_cljs$core$async32669.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32669.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
});

cljs.core.async.t_cljs$core$async32669.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
});

cljs.core.async.t_cljs$core$async32669.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32669.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_(self__.ch,(function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32681 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32681 = (function (f,ch,meta32670,_,fn1,meta32682){
this.f = f;
this.ch = ch;
this.meta32670 = meta32670;
this._ = _;
this.fn1 = fn1;
this.meta32682 = meta32682;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async32681.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_32683,meta32682__$1){
var self__ = this;
var _32683__$1 = this;
return (new cljs.core.async.t_cljs$core$async32681(self__.f,self__.ch,self__.meta32670,self__._,self__.fn1,meta32682__$1));
});})(___$1))
;

cljs.core.async.t_cljs$core$async32681.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_32683){
var self__ = this;
var _32683__$1 = this;
return self__.meta32682;
});})(___$1))
;

cljs.core.async.t_cljs$core$async32681.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32681.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.fn1);
});})(___$1))
;

cljs.core.async.t_cljs$core$async32681.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
});})(___$1))
;

cljs.core.async.t_cljs$core$async32681.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit(self__.fn1);
return ((function (f1,___$2,___$1){
return (function (p1__32668_SHARP_){
var G__32691 = (((p1__32668_SHARP_ == null))?null:(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(p1__32668_SHARP_) : self__.f.call(null,p1__32668_SHARP_)));
return (f1.cljs$core$IFn$_invoke$arity$1 ? f1.cljs$core$IFn$_invoke$arity$1(G__32691) : f1.call(null,G__32691));
});
;})(f1,___$2,___$1))
});})(___$1))
;

cljs.core.async.t_cljs$core$async32681.getBasis = ((function (___$1){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta32670","meta32670",1657685403,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async32669","cljs.core.async/t_cljs$core$async32669",-701865215,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta32682","meta32682",-1232106664,null)], null);
});})(___$1))
;

cljs.core.async.t_cljs$core$async32681.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async32681.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32681";

cljs.core.async.t_cljs$core$async32681.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async32681");
});})(___$1))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32681.
 */
cljs.core.async.__GT_t_cljs$core$async32681 = ((function (___$1){
return (function cljs$core$async$map_LT__$___GT_t_cljs$core$async32681(f__$1,ch__$1,meta32670__$1,___$2,fn1__$1,meta32682){
return (new cljs.core.async.t_cljs$core$async32681(f__$1,ch__$1,meta32670__$1,___$2,fn1__$1,meta32682));
});})(___$1))
;

}

return (new cljs.core.async.t_cljs$core$async32681(self__.f,self__.ch,self__.meta32670,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__4120__auto__ = ret;
if(cljs.core.truth_(and__4120__auto__)){
return (!((cljs.core.deref(ret) == null)));
} else {
return and__4120__auto__;
}
})())){
return cljs.core.async.impl.channels.box((function (){var G__32696 = cljs.core.deref(ret);
return (self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(G__32696) : self__.f.call(null,G__32696));
})());
} else {
return ret;
}
});

cljs.core.async.t_cljs$core$async32669.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32669.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
});

cljs.core.async.t_cljs$core$async32669.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta32670","meta32670",1657685403,null)], null);
});

cljs.core.async.t_cljs$core$async32669.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async32669.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32669";

cljs.core.async.t_cljs$core$async32669.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async32669");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32669.
 */
cljs.core.async.__GT_t_cljs$core$async32669 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async32669(f__$1,ch__$1,meta32670){
return (new cljs.core.async.t_cljs$core$async32669(f__$1,ch__$1,meta32670));
});

}

return (new cljs.core.async.t_cljs$core$async32669(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32698 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32698 = (function (f,ch,meta32699){
this.f = f;
this.ch = ch;
this.meta32699 = meta32699;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async32698.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32700,meta32699__$1){
var self__ = this;
var _32700__$1 = this;
return (new cljs.core.async.t_cljs$core$async32698(self__.f,self__.ch,meta32699__$1));
});

cljs.core.async.t_cljs$core$async32698.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32700){
var self__ = this;
var _32700__$1 = this;
return self__.meta32699;
});

cljs.core.async.t_cljs$core$async32698.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32698.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
});

cljs.core.async.t_cljs$core$async32698.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32698.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async32698.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32698.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(val) : self__.f.call(null,val)),fn1);
});

cljs.core.async.t_cljs$core$async32698.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta32699","meta32699",537200921,null)], null);
});

cljs.core.async.t_cljs$core$async32698.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async32698.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32698";

cljs.core.async.t_cljs$core$async32698.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async32698");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32698.
 */
cljs.core.async.__GT_t_cljs$core$async32698 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async32698(f__$1,ch__$1,meta32699){
return (new cljs.core.async.t_cljs$core$async32698(f__$1,ch__$1,meta32699));
});

}

return (new cljs.core.async.t_cljs$core$async32698(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32709 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32709 = (function (p,ch,meta32710){
this.p = p;
this.ch = ch;
this.meta32710 = meta32710;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async32709.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32711,meta32710__$1){
var self__ = this;
var _32711__$1 = this;
return (new cljs.core.async.t_cljs$core$async32709(self__.p,self__.ch,meta32710__$1));
});

cljs.core.async.t_cljs$core$async32709.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32711){
var self__ = this;
var _32711__$1 = this;
return self__.meta32710;
});

cljs.core.async.t_cljs$core$async32709.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32709.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
});

cljs.core.async.t_cljs$core$async32709.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
});

cljs.core.async.t_cljs$core$async32709.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32709.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async32709.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32709.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.p.cljs$core$IFn$_invoke$arity$1 ? self__.p.cljs$core$IFn$_invoke$arity$1(val) : self__.p.call(null,val)))){
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box(cljs.core.not(cljs.core.async.impl.protocols.closed_QMARK_(self__.ch)));
}
});

cljs.core.async.t_cljs$core$async32709.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta32710","meta32710",-1973178686,null)], null);
});

cljs.core.async.t_cljs$core$async32709.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async32709.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32709";

cljs.core.async.t_cljs$core$async32709.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async32709");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32709.
 */
cljs.core.async.__GT_t_cljs$core$async32709 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async32709(p__$1,ch__$1,meta32710){
return (new cljs.core.async.t_cljs$core$async32709(p__$1,ch__$1,meta32710));
});

}

return (new cljs.core.async.t_cljs$core$async32709(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_(cljs.core.complement(p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__32733 = arguments.length;
switch (G__32733) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__29782__auto___34631 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto___34631,out){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto___34631,out){
return (function (state_32755){
var state_val_32756 = (state_32755[(1)]);
if((state_val_32756 === (7))){
var inst_32751 = (state_32755[(2)]);
var state_32755__$1 = state_32755;
var statearr_32761_34634 = state_32755__$1;
(statearr_32761_34634[(2)] = inst_32751);

(statearr_32761_34634[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32756 === (1))){
var state_32755__$1 = state_32755;
var statearr_32762_34635 = state_32755__$1;
(statearr_32762_34635[(2)] = null);

(statearr_32762_34635[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32756 === (4))){
var inst_32737 = (state_32755[(7)]);
var inst_32737__$1 = (state_32755[(2)]);
var inst_32738 = (inst_32737__$1 == null);
var state_32755__$1 = (function (){var statearr_32763 = state_32755;
(statearr_32763[(7)] = inst_32737__$1);

return statearr_32763;
})();
if(cljs.core.truth_(inst_32738)){
var statearr_32764_34636 = state_32755__$1;
(statearr_32764_34636[(1)] = (5));

} else {
var statearr_32765_34637 = state_32755__$1;
(statearr_32765_34637[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32756 === (6))){
var inst_32737 = (state_32755[(7)]);
var inst_32742 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_32737) : p.call(null,inst_32737));
var state_32755__$1 = state_32755;
if(cljs.core.truth_(inst_32742)){
var statearr_32769_34638 = state_32755__$1;
(statearr_32769_34638[(1)] = (8));

} else {
var statearr_32770_34639 = state_32755__$1;
(statearr_32770_34639[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32756 === (3))){
var inst_32753 = (state_32755[(2)]);
var state_32755__$1 = state_32755;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32755__$1,inst_32753);
} else {
if((state_val_32756 === (2))){
var state_32755__$1 = state_32755;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32755__$1,(4),ch);
} else {
if((state_val_32756 === (11))){
var inst_32745 = (state_32755[(2)]);
var state_32755__$1 = state_32755;
var statearr_32772_34640 = state_32755__$1;
(statearr_32772_34640[(2)] = inst_32745);

(statearr_32772_34640[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32756 === (9))){
var state_32755__$1 = state_32755;
var statearr_32774_34641 = state_32755__$1;
(statearr_32774_34641[(2)] = null);

(statearr_32774_34641[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32756 === (5))){
var inst_32740 = cljs.core.async.close_BANG_(out);
var state_32755__$1 = state_32755;
var statearr_32779_34642 = state_32755__$1;
(statearr_32779_34642[(2)] = inst_32740);

(statearr_32779_34642[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32756 === (10))){
var inst_32748 = (state_32755[(2)]);
var state_32755__$1 = (function (){var statearr_32780 = state_32755;
(statearr_32780[(8)] = inst_32748);

return statearr_32780;
})();
var statearr_32781_34647 = state_32755__$1;
(statearr_32781_34647[(2)] = null);

(statearr_32781_34647[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32756 === (8))){
var inst_32737 = (state_32755[(7)]);
var state_32755__$1 = state_32755;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32755__$1,(11),out,inst_32737);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto___34631,out))
;
return ((function (switch__28688__auto__,c__29782__auto___34631,out){
return (function() {
var cljs$core$async$state_machine__28689__auto__ = null;
var cljs$core$async$state_machine__28689__auto____0 = (function (){
var statearr_32782 = [null,null,null,null,null,null,null,null,null];
(statearr_32782[(0)] = cljs$core$async$state_machine__28689__auto__);

(statearr_32782[(1)] = (1));

return statearr_32782;
});
var cljs$core$async$state_machine__28689__auto____1 = (function (state_32755){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_32755);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e32783){if((e32783 instanceof Object)){
var ex__28692__auto__ = e32783;
var statearr_32784_34658 = state_32755;
(statearr_32784_34658[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_32755);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32783;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34663 = state_32755;
state_32755 = G__34663;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$state_machine__28689__auto__ = function(state_32755){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28689__auto____1.call(this,state_32755);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28689__auto____0;
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28689__auto____1;
return cljs$core$async$state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto___34631,out))
})();
var state__29784__auto__ = (function (){var statearr_32785 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_32785[(6)] = c__29782__auto___34631);

return statearr_32785;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto___34631,out))
);


return out;
});

cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__32787 = arguments.length;
switch (G__32787) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(cljs.core.complement(p),ch,buf_or_n);
});

cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3;

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__29782__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto__){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto__){
return (function (state_32852){
var state_val_32853 = (state_32852[(1)]);
if((state_val_32853 === (7))){
var inst_32848 = (state_32852[(2)]);
var state_32852__$1 = state_32852;
var statearr_32858_34685 = state_32852__$1;
(statearr_32858_34685[(2)] = inst_32848);

(statearr_32858_34685[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (20))){
var inst_32817 = (state_32852[(7)]);
var inst_32829 = (state_32852[(2)]);
var inst_32830 = cljs.core.next(inst_32817);
var inst_32803 = inst_32830;
var inst_32804 = null;
var inst_32805 = (0);
var inst_32806 = (0);
var state_32852__$1 = (function (){var statearr_32859 = state_32852;
(statearr_32859[(8)] = inst_32803);

(statearr_32859[(9)] = inst_32806);

(statearr_32859[(10)] = inst_32829);

(statearr_32859[(11)] = inst_32804);

(statearr_32859[(12)] = inst_32805);

return statearr_32859;
})();
var statearr_32861_34687 = state_32852__$1;
(statearr_32861_34687[(2)] = null);

(statearr_32861_34687[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (1))){
var state_32852__$1 = state_32852;
var statearr_32862_34688 = state_32852__$1;
(statearr_32862_34688[(2)] = null);

(statearr_32862_34688[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (4))){
var inst_32792 = (state_32852[(13)]);
var inst_32792__$1 = (state_32852[(2)]);
var inst_32793 = (inst_32792__$1 == null);
var state_32852__$1 = (function (){var statearr_32866 = state_32852;
(statearr_32866[(13)] = inst_32792__$1);

return statearr_32866;
})();
if(cljs.core.truth_(inst_32793)){
var statearr_32867_34690 = state_32852__$1;
(statearr_32867_34690[(1)] = (5));

} else {
var statearr_32868_34691 = state_32852__$1;
(statearr_32868_34691[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (15))){
var state_32852__$1 = state_32852;
var statearr_32872_34692 = state_32852__$1;
(statearr_32872_34692[(2)] = null);

(statearr_32872_34692[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (21))){
var state_32852__$1 = state_32852;
var statearr_32873_34693 = state_32852__$1;
(statearr_32873_34693[(2)] = null);

(statearr_32873_34693[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (13))){
var inst_32803 = (state_32852[(8)]);
var inst_32806 = (state_32852[(9)]);
var inst_32804 = (state_32852[(11)]);
var inst_32805 = (state_32852[(12)]);
var inst_32813 = (state_32852[(2)]);
var inst_32814 = (inst_32806 + (1));
var tmp32869 = inst_32803;
var tmp32870 = inst_32804;
var tmp32871 = inst_32805;
var inst_32803__$1 = tmp32869;
var inst_32804__$1 = tmp32870;
var inst_32805__$1 = tmp32871;
var inst_32806__$1 = inst_32814;
var state_32852__$1 = (function (){var statearr_32874 = state_32852;
(statearr_32874[(8)] = inst_32803__$1);

(statearr_32874[(9)] = inst_32806__$1);

(statearr_32874[(14)] = inst_32813);

(statearr_32874[(11)] = inst_32804__$1);

(statearr_32874[(12)] = inst_32805__$1);

return statearr_32874;
})();
var statearr_32875_34697 = state_32852__$1;
(statearr_32875_34697[(2)] = null);

(statearr_32875_34697[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (22))){
var state_32852__$1 = state_32852;
var statearr_32876_34698 = state_32852__$1;
(statearr_32876_34698[(2)] = null);

(statearr_32876_34698[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (6))){
var inst_32792 = (state_32852[(13)]);
var inst_32801 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_32792) : f.call(null,inst_32792));
var inst_32802 = cljs.core.seq(inst_32801);
var inst_32803 = inst_32802;
var inst_32804 = null;
var inst_32805 = (0);
var inst_32806 = (0);
var state_32852__$1 = (function (){var statearr_32877 = state_32852;
(statearr_32877[(8)] = inst_32803);

(statearr_32877[(9)] = inst_32806);

(statearr_32877[(11)] = inst_32804);

(statearr_32877[(12)] = inst_32805);

return statearr_32877;
})();
var statearr_32879_34700 = state_32852__$1;
(statearr_32879_34700[(2)] = null);

(statearr_32879_34700[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (17))){
var inst_32817 = (state_32852[(7)]);
var inst_32822 = cljs.core.chunk_first(inst_32817);
var inst_32823 = cljs.core.chunk_rest(inst_32817);
var inst_32824 = cljs.core.count(inst_32822);
var inst_32803 = inst_32823;
var inst_32804 = inst_32822;
var inst_32805 = inst_32824;
var inst_32806 = (0);
var state_32852__$1 = (function (){var statearr_32880 = state_32852;
(statearr_32880[(8)] = inst_32803);

(statearr_32880[(9)] = inst_32806);

(statearr_32880[(11)] = inst_32804);

(statearr_32880[(12)] = inst_32805);

return statearr_32880;
})();
var statearr_32881_34704 = state_32852__$1;
(statearr_32881_34704[(2)] = null);

(statearr_32881_34704[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (3))){
var inst_32850 = (state_32852[(2)]);
var state_32852__$1 = state_32852;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32852__$1,inst_32850);
} else {
if((state_val_32853 === (12))){
var inst_32838 = (state_32852[(2)]);
var state_32852__$1 = state_32852;
var statearr_32882_34718 = state_32852__$1;
(statearr_32882_34718[(2)] = inst_32838);

(statearr_32882_34718[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (2))){
var state_32852__$1 = state_32852;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32852__$1,(4),in$);
} else {
if((state_val_32853 === (23))){
var inst_32846 = (state_32852[(2)]);
var state_32852__$1 = state_32852;
var statearr_32883_34731 = state_32852__$1;
(statearr_32883_34731[(2)] = inst_32846);

(statearr_32883_34731[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (19))){
var inst_32833 = (state_32852[(2)]);
var state_32852__$1 = state_32852;
var statearr_32885_34741 = state_32852__$1;
(statearr_32885_34741[(2)] = inst_32833);

(statearr_32885_34741[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (11))){
var inst_32803 = (state_32852[(8)]);
var inst_32817 = (state_32852[(7)]);
var inst_32817__$1 = cljs.core.seq(inst_32803);
var state_32852__$1 = (function (){var statearr_32886 = state_32852;
(statearr_32886[(7)] = inst_32817__$1);

return statearr_32886;
})();
if(inst_32817__$1){
var statearr_32887_34757 = state_32852__$1;
(statearr_32887_34757[(1)] = (14));

} else {
var statearr_32888_34759 = state_32852__$1;
(statearr_32888_34759[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (9))){
var inst_32840 = (state_32852[(2)]);
var inst_32841 = cljs.core.async.impl.protocols.closed_QMARK_(out);
var state_32852__$1 = (function (){var statearr_32889 = state_32852;
(statearr_32889[(15)] = inst_32840);

return statearr_32889;
})();
if(cljs.core.truth_(inst_32841)){
var statearr_32890_34770 = state_32852__$1;
(statearr_32890_34770[(1)] = (21));

} else {
var statearr_32892_34775 = state_32852__$1;
(statearr_32892_34775[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (5))){
var inst_32795 = cljs.core.async.close_BANG_(out);
var state_32852__$1 = state_32852;
var statearr_32894_34786 = state_32852__$1;
(statearr_32894_34786[(2)] = inst_32795);

(statearr_32894_34786[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (14))){
var inst_32817 = (state_32852[(7)]);
var inst_32820 = cljs.core.chunked_seq_QMARK_(inst_32817);
var state_32852__$1 = state_32852;
if(inst_32820){
var statearr_32896_34796 = state_32852__$1;
(statearr_32896_34796[(1)] = (17));

} else {
var statearr_32897_34801 = state_32852__$1;
(statearr_32897_34801[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (16))){
var inst_32836 = (state_32852[(2)]);
var state_32852__$1 = state_32852;
var statearr_32900_34802 = state_32852__$1;
(statearr_32900_34802[(2)] = inst_32836);

(statearr_32900_34802[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32853 === (10))){
var inst_32806 = (state_32852[(9)]);
var inst_32804 = (state_32852[(11)]);
var inst_32811 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_32804,inst_32806);
var state_32852__$1 = state_32852;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32852__$1,(13),out,inst_32811);
} else {
if((state_val_32853 === (18))){
var inst_32817 = (state_32852[(7)]);
var inst_32827 = cljs.core.first(inst_32817);
var state_32852__$1 = state_32852;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32852__$1,(20),out,inst_32827);
} else {
if((state_val_32853 === (8))){
var inst_32806 = (state_32852[(9)]);
var inst_32805 = (state_32852[(12)]);
var inst_32808 = (inst_32806 < inst_32805);
var inst_32809 = inst_32808;
var state_32852__$1 = state_32852;
if(cljs.core.truth_(inst_32809)){
var statearr_32901_34807 = state_32852__$1;
(statearr_32901_34807[(1)] = (10));

} else {
var statearr_32902_34808 = state_32852__$1;
(statearr_32902_34808[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto__))
;
return ((function (switch__28688__auto__,c__29782__auto__){
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__28689__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__28689__auto____0 = (function (){
var statearr_32903 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32903[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__28689__auto__);

(statearr_32903[(1)] = (1));

return statearr_32903;
});
var cljs$core$async$mapcat_STAR__$_state_machine__28689__auto____1 = (function (state_32852){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_32852);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e32908){if((e32908 instanceof Object)){
var ex__28692__auto__ = e32908;
var statearr_32909_34813 = state_32852;
(statearr_32909_34813[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_32852);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32908;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34818 = state_32852;
state_32852 = G__34818;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__28689__auto__ = function(state_32852){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__28689__auto____1.call(this,state_32852);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__28689__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__28689__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto__))
})();
var state__29784__auto__ = (function (){var statearr_32914 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_32914[(6)] = c__29782__auto__);

return statearr_32914;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto__))
);

return c__29782__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__32921 = arguments.length;
switch (G__32921) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3(f,in$,null);
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return out;
});

cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__32926 = arguments.length;
switch (G__32926) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3(f,out,null);
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return in$;
});

cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__32928 = arguments.length;
switch (G__32928) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2(ch,null);
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__29782__auto___34833 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto___34833,out){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto___34833,out){
return (function (state_32955){
var state_val_32956 = (state_32955[(1)]);
if((state_val_32956 === (7))){
var inst_32950 = (state_32955[(2)]);
var state_32955__$1 = state_32955;
var statearr_32957_34834 = state_32955__$1;
(statearr_32957_34834[(2)] = inst_32950);

(statearr_32957_34834[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32956 === (1))){
var inst_32932 = null;
var state_32955__$1 = (function (){var statearr_32958 = state_32955;
(statearr_32958[(7)] = inst_32932);

return statearr_32958;
})();
var statearr_32960_34835 = state_32955__$1;
(statearr_32960_34835[(2)] = null);

(statearr_32960_34835[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32956 === (4))){
var inst_32935 = (state_32955[(8)]);
var inst_32935__$1 = (state_32955[(2)]);
var inst_32936 = (inst_32935__$1 == null);
var inst_32937 = cljs.core.not(inst_32936);
var state_32955__$1 = (function (){var statearr_32964 = state_32955;
(statearr_32964[(8)] = inst_32935__$1);

return statearr_32964;
})();
if(inst_32937){
var statearr_32965_34842 = state_32955__$1;
(statearr_32965_34842[(1)] = (5));

} else {
var statearr_32966_34847 = state_32955__$1;
(statearr_32966_34847[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32956 === (6))){
var state_32955__$1 = state_32955;
var statearr_32969_34849 = state_32955__$1;
(statearr_32969_34849[(2)] = null);

(statearr_32969_34849[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32956 === (3))){
var inst_32952 = (state_32955[(2)]);
var inst_32953 = cljs.core.async.close_BANG_(out);
var state_32955__$1 = (function (){var statearr_32971 = state_32955;
(statearr_32971[(9)] = inst_32952);

return statearr_32971;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_32955__$1,inst_32953);
} else {
if((state_val_32956 === (2))){
var state_32955__$1 = state_32955;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32955__$1,(4),ch);
} else {
if((state_val_32956 === (11))){
var inst_32935 = (state_32955[(8)]);
var inst_32944 = (state_32955[(2)]);
var inst_32932 = inst_32935;
var state_32955__$1 = (function (){var statearr_32972 = state_32955;
(statearr_32972[(7)] = inst_32932);

(statearr_32972[(10)] = inst_32944);

return statearr_32972;
})();
var statearr_32973_34873 = state_32955__$1;
(statearr_32973_34873[(2)] = null);

(statearr_32973_34873[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32956 === (9))){
var inst_32935 = (state_32955[(8)]);
var state_32955__$1 = state_32955;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32955__$1,(11),out,inst_32935);
} else {
if((state_val_32956 === (5))){
var inst_32932 = (state_32955[(7)]);
var inst_32935 = (state_32955[(8)]);
var inst_32939 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_32935,inst_32932);
var state_32955__$1 = state_32955;
if(inst_32939){
var statearr_32975_34883 = state_32955__$1;
(statearr_32975_34883[(1)] = (8));

} else {
var statearr_32976_34885 = state_32955__$1;
(statearr_32976_34885[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32956 === (10))){
var inst_32947 = (state_32955[(2)]);
var state_32955__$1 = state_32955;
var statearr_32977_34893 = state_32955__$1;
(statearr_32977_34893[(2)] = inst_32947);

(statearr_32977_34893[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32956 === (8))){
var inst_32932 = (state_32955[(7)]);
var tmp32974 = inst_32932;
var inst_32932__$1 = tmp32974;
var state_32955__$1 = (function (){var statearr_32978 = state_32955;
(statearr_32978[(7)] = inst_32932__$1);

return statearr_32978;
})();
var statearr_32981_34898 = state_32955__$1;
(statearr_32981_34898[(2)] = null);

(statearr_32981_34898[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto___34833,out))
;
return ((function (switch__28688__auto__,c__29782__auto___34833,out){
return (function() {
var cljs$core$async$state_machine__28689__auto__ = null;
var cljs$core$async$state_machine__28689__auto____0 = (function (){
var statearr_32996 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_32996[(0)] = cljs$core$async$state_machine__28689__auto__);

(statearr_32996[(1)] = (1));

return statearr_32996;
});
var cljs$core$async$state_machine__28689__auto____1 = (function (state_32955){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_32955);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e33001){if((e33001 instanceof Object)){
var ex__28692__auto__ = e33001;
var statearr_33002_34902 = state_32955;
(statearr_33002_34902[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_32955);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33001;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34904 = state_32955;
state_32955 = G__34904;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$state_machine__28689__auto__ = function(state_32955){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28689__auto____1.call(this,state_32955);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28689__auto____0;
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28689__auto____1;
return cljs$core$async$state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto___34833,out))
})();
var state__29784__auto__ = (function (){var statearr_33003 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_33003[(6)] = c__29782__auto___34833);

return statearr_33003;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto___34833,out))
);


return out;
});

cljs.core.async.unique.cljs$lang$maxFixedArity = 2;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__33016 = arguments.length;
switch (G__33016) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3(n,ch,null);
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__29782__auto___34906 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto___34906,out){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto___34906,out){
return (function (state_33061){
var state_val_33062 = (state_33061[(1)]);
if((state_val_33062 === (7))){
var inst_33055 = (state_33061[(2)]);
var state_33061__$1 = state_33061;
var statearr_33077_34907 = state_33061__$1;
(statearr_33077_34907[(2)] = inst_33055);

(statearr_33077_34907[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33062 === (1))){
var inst_33022 = (new Array(n));
var inst_33023 = inst_33022;
var inst_33024 = (0);
var state_33061__$1 = (function (){var statearr_33081 = state_33061;
(statearr_33081[(7)] = inst_33024);

(statearr_33081[(8)] = inst_33023);

return statearr_33081;
})();
var statearr_33082_34912 = state_33061__$1;
(statearr_33082_34912[(2)] = null);

(statearr_33082_34912[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33062 === (4))){
var inst_33027 = (state_33061[(9)]);
var inst_33027__$1 = (state_33061[(2)]);
var inst_33028 = (inst_33027__$1 == null);
var inst_33029 = cljs.core.not(inst_33028);
var state_33061__$1 = (function (){var statearr_33086 = state_33061;
(statearr_33086[(9)] = inst_33027__$1);

return statearr_33086;
})();
if(inst_33029){
var statearr_33091_34921 = state_33061__$1;
(statearr_33091_34921[(1)] = (5));

} else {
var statearr_33092_34922 = state_33061__$1;
(statearr_33092_34922[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33062 === (15))){
var inst_33049 = (state_33061[(2)]);
var state_33061__$1 = state_33061;
var statearr_33097_34923 = state_33061__$1;
(statearr_33097_34923[(2)] = inst_33049);

(statearr_33097_34923[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33062 === (13))){
var state_33061__$1 = state_33061;
var statearr_33100_34924 = state_33061__$1;
(statearr_33100_34924[(2)] = null);

(statearr_33100_34924[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33062 === (6))){
var inst_33024 = (state_33061[(7)]);
var inst_33045 = (inst_33024 > (0));
var state_33061__$1 = state_33061;
if(cljs.core.truth_(inst_33045)){
var statearr_33104_34926 = state_33061__$1;
(statearr_33104_34926[(1)] = (12));

} else {
var statearr_33106_34927 = state_33061__$1;
(statearr_33106_34927[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33062 === (3))){
var inst_33057 = (state_33061[(2)]);
var state_33061__$1 = state_33061;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33061__$1,inst_33057);
} else {
if((state_val_33062 === (12))){
var inst_33023 = (state_33061[(8)]);
var inst_33047 = cljs.core.vec(inst_33023);
var state_33061__$1 = state_33061;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33061__$1,(15),out,inst_33047);
} else {
if((state_val_33062 === (2))){
var state_33061__$1 = state_33061;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33061__$1,(4),ch);
} else {
if((state_val_33062 === (11))){
var inst_33039 = (state_33061[(2)]);
var inst_33040 = (new Array(n));
var inst_33023 = inst_33040;
var inst_33024 = (0);
var state_33061__$1 = (function (){var statearr_33111 = state_33061;
(statearr_33111[(10)] = inst_33039);

(statearr_33111[(7)] = inst_33024);

(statearr_33111[(8)] = inst_33023);

return statearr_33111;
})();
var statearr_33112_34928 = state_33061__$1;
(statearr_33112_34928[(2)] = null);

(statearr_33112_34928[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33062 === (9))){
var inst_33023 = (state_33061[(8)]);
var inst_33037 = cljs.core.vec(inst_33023);
var state_33061__$1 = state_33061;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33061__$1,(11),out,inst_33037);
} else {
if((state_val_33062 === (5))){
var inst_33032 = (state_33061[(11)]);
var inst_33024 = (state_33061[(7)]);
var inst_33023 = (state_33061[(8)]);
var inst_33027 = (state_33061[(9)]);
var inst_33031 = (inst_33023[inst_33024] = inst_33027);
var inst_33032__$1 = (inst_33024 + (1));
var inst_33033 = (inst_33032__$1 < n);
var state_33061__$1 = (function (){var statearr_33120 = state_33061;
(statearr_33120[(11)] = inst_33032__$1);

(statearr_33120[(12)] = inst_33031);

return statearr_33120;
})();
if(cljs.core.truth_(inst_33033)){
var statearr_33124_34931 = state_33061__$1;
(statearr_33124_34931[(1)] = (8));

} else {
var statearr_33125_34933 = state_33061__$1;
(statearr_33125_34933[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33062 === (14))){
var inst_33052 = (state_33061[(2)]);
var inst_33053 = cljs.core.async.close_BANG_(out);
var state_33061__$1 = (function (){var statearr_33131 = state_33061;
(statearr_33131[(13)] = inst_33052);

return statearr_33131;
})();
var statearr_33136_34935 = state_33061__$1;
(statearr_33136_34935[(2)] = inst_33053);

(statearr_33136_34935[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33062 === (10))){
var inst_33043 = (state_33061[(2)]);
var state_33061__$1 = state_33061;
var statearr_33142_34936 = state_33061__$1;
(statearr_33142_34936[(2)] = inst_33043);

(statearr_33142_34936[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33062 === (8))){
var inst_33032 = (state_33061[(11)]);
var inst_33023 = (state_33061[(8)]);
var tmp33130 = inst_33023;
var inst_33023__$1 = tmp33130;
var inst_33024 = inst_33032;
var state_33061__$1 = (function (){var statearr_33153 = state_33061;
(statearr_33153[(7)] = inst_33024);

(statearr_33153[(8)] = inst_33023__$1);

return statearr_33153;
})();
var statearr_33158_34937 = state_33061__$1;
(statearr_33158_34937[(2)] = null);

(statearr_33158_34937[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto___34906,out))
;
return ((function (switch__28688__auto__,c__29782__auto___34906,out){
return (function() {
var cljs$core$async$state_machine__28689__auto__ = null;
var cljs$core$async$state_machine__28689__auto____0 = (function (){
var statearr_33160 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33160[(0)] = cljs$core$async$state_machine__28689__auto__);

(statearr_33160[(1)] = (1));

return statearr_33160;
});
var cljs$core$async$state_machine__28689__auto____1 = (function (state_33061){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_33061);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e33164){if((e33164 instanceof Object)){
var ex__28692__auto__ = e33164;
var statearr_33168_34942 = state_33061;
(statearr_33168_34942[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33061);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33164;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34946 = state_33061;
state_33061 = G__34946;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$state_machine__28689__auto__ = function(state_33061){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28689__auto____1.call(this,state_33061);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28689__auto____0;
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28689__auto____1;
return cljs$core$async$state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto___34906,out))
})();
var state__29784__auto__ = (function (){var statearr_33174 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_33174[(6)] = c__29782__auto___34906);

return statearr_33174;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto___34906,out))
);


return out;
});

cljs.core.async.partition.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__33184 = arguments.length;
switch (G__33184) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3(f,ch,null);
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__29782__auto___34948 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto___34948,out){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto___34948,out){
return (function (state_33244){
var state_val_33245 = (state_33244[(1)]);
if((state_val_33245 === (7))){
var inst_33238 = (state_33244[(2)]);
var state_33244__$1 = state_33244;
var statearr_33254_34952 = state_33244__$1;
(statearr_33254_34952[(2)] = inst_33238);

(statearr_33254_34952[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33245 === (1))){
var inst_33192 = [];
var inst_33193 = inst_33192;
var inst_33194 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_33244__$1 = (function (){var statearr_33261 = state_33244;
(statearr_33261[(7)] = inst_33193);

(statearr_33261[(8)] = inst_33194);

return statearr_33261;
})();
var statearr_33264_34953 = state_33244__$1;
(statearr_33264_34953[(2)] = null);

(statearr_33264_34953[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33245 === (4))){
var inst_33199 = (state_33244[(9)]);
var inst_33199__$1 = (state_33244[(2)]);
var inst_33200 = (inst_33199__$1 == null);
var inst_33201 = cljs.core.not(inst_33200);
var state_33244__$1 = (function (){var statearr_33267 = state_33244;
(statearr_33267[(9)] = inst_33199__$1);

return statearr_33267;
})();
if(inst_33201){
var statearr_33268_34954 = state_33244__$1;
(statearr_33268_34954[(1)] = (5));

} else {
var statearr_33270_34955 = state_33244__$1;
(statearr_33270_34955[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33245 === (15))){
var inst_33231 = (state_33244[(2)]);
var state_33244__$1 = state_33244;
var statearr_33275_34959 = state_33244__$1;
(statearr_33275_34959[(2)] = inst_33231);

(statearr_33275_34959[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33245 === (13))){
var state_33244__$1 = state_33244;
var statearr_33285_34960 = state_33244__$1;
(statearr_33285_34960[(2)] = null);

(statearr_33285_34960[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33245 === (6))){
var inst_33193 = (state_33244[(7)]);
var inst_33225 = inst_33193.length;
var inst_33226 = (inst_33225 > (0));
var state_33244__$1 = state_33244;
if(cljs.core.truth_(inst_33226)){
var statearr_33292_34961 = state_33244__$1;
(statearr_33292_34961[(1)] = (12));

} else {
var statearr_33294_34962 = state_33244__$1;
(statearr_33294_34962[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33245 === (3))){
var inst_33240 = (state_33244[(2)]);
var state_33244__$1 = state_33244;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33244__$1,inst_33240);
} else {
if((state_val_33245 === (12))){
var inst_33193 = (state_33244[(7)]);
var inst_33229 = cljs.core.vec(inst_33193);
var state_33244__$1 = state_33244;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33244__$1,(15),out,inst_33229);
} else {
if((state_val_33245 === (2))){
var state_33244__$1 = state_33244;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33244__$1,(4),ch);
} else {
if((state_val_33245 === (11))){
var inst_33203 = (state_33244[(10)]);
var inst_33199 = (state_33244[(9)]);
var inst_33217 = (state_33244[(2)]);
var inst_33219 = [];
var inst_33220 = inst_33219.push(inst_33199);
var inst_33193 = inst_33219;
var inst_33194 = inst_33203;
var state_33244__$1 = (function (){var statearr_33305 = state_33244;
(statearr_33305[(11)] = inst_33217);

(statearr_33305[(7)] = inst_33193);

(statearr_33305[(12)] = inst_33220);

(statearr_33305[(8)] = inst_33194);

return statearr_33305;
})();
var statearr_33310_34964 = state_33244__$1;
(statearr_33310_34964[(2)] = null);

(statearr_33310_34964[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33245 === (9))){
var inst_33193 = (state_33244[(7)]);
var inst_33215 = cljs.core.vec(inst_33193);
var state_33244__$1 = state_33244;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33244__$1,(11),out,inst_33215);
} else {
if((state_val_33245 === (5))){
var inst_33203 = (state_33244[(10)]);
var inst_33199 = (state_33244[(9)]);
var inst_33194 = (state_33244[(8)]);
var inst_33203__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_33199) : f.call(null,inst_33199));
var inst_33206 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_33203__$1,inst_33194);
var inst_33207 = cljs.core.keyword_identical_QMARK_(inst_33194,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_33208 = ((inst_33206) || (inst_33207));
var state_33244__$1 = (function (){var statearr_33316 = state_33244;
(statearr_33316[(10)] = inst_33203__$1);

return statearr_33316;
})();
if(cljs.core.truth_(inst_33208)){
var statearr_33317_34968 = state_33244__$1;
(statearr_33317_34968[(1)] = (8));

} else {
var statearr_33318_34969 = state_33244__$1;
(statearr_33318_34969[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33245 === (14))){
var inst_33234 = (state_33244[(2)]);
var inst_33236 = cljs.core.async.close_BANG_(out);
var state_33244__$1 = (function (){var statearr_33321 = state_33244;
(statearr_33321[(13)] = inst_33234);

return statearr_33321;
})();
var statearr_33323_34970 = state_33244__$1;
(statearr_33323_34970[(2)] = inst_33236);

(statearr_33323_34970[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33245 === (10))){
var inst_33223 = (state_33244[(2)]);
var state_33244__$1 = state_33244;
var statearr_33325_34971 = state_33244__$1;
(statearr_33325_34971[(2)] = inst_33223);

(statearr_33325_34971[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33245 === (8))){
var inst_33203 = (state_33244[(10)]);
var inst_33193 = (state_33244[(7)]);
var inst_33199 = (state_33244[(9)]);
var inst_33212 = inst_33193.push(inst_33199);
var tmp33319 = inst_33193;
var inst_33193__$1 = tmp33319;
var inst_33194 = inst_33203;
var state_33244__$1 = (function (){var statearr_33329 = state_33244;
(statearr_33329[(14)] = inst_33212);

(statearr_33329[(7)] = inst_33193__$1);

(statearr_33329[(8)] = inst_33194);

return statearr_33329;
})();
var statearr_33331_34976 = state_33244__$1;
(statearr_33331_34976[(2)] = null);

(statearr_33331_34976[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__29782__auto___34948,out))
;
return ((function (switch__28688__auto__,c__29782__auto___34948,out){
return (function() {
var cljs$core$async$state_machine__28689__auto__ = null;
var cljs$core$async$state_machine__28689__auto____0 = (function (){
var statearr_33336 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33336[(0)] = cljs$core$async$state_machine__28689__auto__);

(statearr_33336[(1)] = (1));

return statearr_33336;
});
var cljs$core$async$state_machine__28689__auto____1 = (function (state_33244){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_33244);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e33338){if((e33338 instanceof Object)){
var ex__28692__auto__ = e33338;
var statearr_33339_34977 = state_33244;
(statearr_33339_34977[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33244);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33338;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34978 = state_33244;
state_33244 = G__34978;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
cljs$core$async$state_machine__28689__auto__ = function(state_33244){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__28689__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__28689__auto____1.call(this,state_33244);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__28689__auto____0;
cljs$core$async$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__28689__auto____1;
return cljs$core$async$state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto___34948,out))
})();
var state__29784__auto__ = (function (){var statearr_33344 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_33344[(6)] = c__29782__auto___34948);

return statearr_33344;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto___34948,out))
);


return out;
});

cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3;


//# sourceMappingURL=cljs.core.async.js.map
