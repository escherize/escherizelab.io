goog.provide('json_html.core');
goog.require('cljs.core');
goog.require('clojure.string');
goog.require('json_html.html');
json_html.core.render_keyword = (function json_html$core$render_keyword(k){
return [":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(clojure.string.join.cljs$core$IFn$_invoke$arity$2("/",cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,(function (){var fexpr__32017 = cljs.core.juxt.cljs$core$IFn$_invoke$arity$2(cljs.core.namespace,cljs.core.name);
return (fexpr__32017.cljs$core$IFn$_invoke$arity$1 ? fexpr__32017.cljs$core$IFn$_invoke$arity$1(k) : fexpr__32017.call(null,k));
})())))].join('');
});
json_html.core.str_compare = (function json_html$core$str_compare(k1,k2){
return cljs.core.compare(cljs.core.str.cljs$core$IFn$_invoke$arity$1(k1),cljs.core.str.cljs$core$IFn$_invoke$arity$1(k2));
});
json_html.core.sort_map = (function json_html$core$sort_map(m){
var m__$1 = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2((function (p__32022){
var vec__32023 = p__32022;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__32023,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__32023,(1),null);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.type(k),Object))?cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(k):k),v], null);
}),m);
try{return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.sorted_map(),m__$1);
}catch (e32030){if((e32030 instanceof Error)){
var _ = e32030;
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.sorted_map_by(json_html.core.str_compare),m__$1);
} else {
throw e32030;

}
}});
json_html.core.sort_set = (function json_html$core$sort_set(s){
try{return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.sorted_set(),s);
}catch (e32032){if((e32032 instanceof Error)){
var _ = e32032;
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.sorted_set_by(json_html.core.str_compare),s);
} else {
throw e32032;

}
}});
json_html.core.url_regex = cljs.core.re_pattern("(\\b(https?|ftp|file|ldap)://[-A-Za-z0-9+&@#/%?=~_|!:,.;]*[-A-Za-z0-9+&@#/%=~_|])");
/**
 * Make links clickable.
 */
json_html.core.linkify_links = (function json_html$core$linkify_links(string){
return clojure.string.replace(string,json_html.core.url_regex,"<a class='jh-type-string-link' href=$1>$1</a>");
});

/**
 * @interface
 */
json_html.core.Render = function(){};

/**
 * Renders the element a Hiccup structure
 */
json_html.core.render = (function json_html$core$render(this$){
if((((!((this$ == null)))) && ((!((this$.json_html$core$Render$render$arity$1 == null)))))){
return this$.json_html$core$Render$render$arity$1(this$);
} else {
var x__4433__auto__ = (((this$ == null))?null:this$);
var m__4434__auto__ = (json_html.core.render[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4434__auto__.call(null,this$));
} else {
var m__4431__auto__ = (json_html.core.render["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4431__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("Render.render",this$);
}
}
}
});

json_html.core.escape_html = (function json_html$core$escape_html(s){
return clojure.string.escape(s,new cljs.core.PersistentArrayMap(null, 4, ["&","&amp;",">","&gt;","<","&lt;","\"","&quot;"], null));
});

json_html.core.obj__GT_clj = (function json_html$core$obj__GT_clj(obj){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (props,k){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(props,cljs.core.keyword.cljs$core$IFn$_invoke$arity$1(k),(obj[k]));
}),cljs.core.PersistentArrayMap.EMPTY,Object.keys(obj));
});


json_html.core.render_collection = (function json_html$core$render_collection(col){
if(cljs.core.empty_QMARK_(col)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.jh-type-object","div.jh-type-object",1704701538),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.jh-empty-collection","span.jh-empty-collection",-107581393)], null)], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"table.jh-type-object","table.jh-type-object",-163995322),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tbody","tbody",-80678300),(function (){var iter__4523__auto__ = (function json_html$core$render_collection_$_iter__32046(s__32047){
return (new cljs.core.LazySeq(null,(function (){
var s__32047__$1 = s__32047;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__32047__$1);
if(temp__5735__auto__){
var s__32047__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__32047__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__32047__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__32049 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__32048 = (0);
while(true){
if((i__32048 < size__4522__auto__)){
var vec__32051 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__32048);
var i = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__32051,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__32051,(1),null);
cljs.core.chunk_append(b__32049,cljs.core.with_meta(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th.jh-key.jh-array-key","th.jh-key.jh-array-key",-1623829832),i], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td.jh-value.jh-array-value","td.jh-value.jh-array-value",1645079212),(json_html.core.render_html.cljs$core$IFn$_invoke$arity$1 ? json_html.core.render_html.cljs$core$IFn$_invoke$arity$1(v) : json_html.core.render_html.call(null,v))], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),i], null)));

var G__32244 = (i__32048 + (1));
i__32048 = G__32244;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__32049),json_html$core$render_collection_$_iter__32046(cljs.core.chunk_rest(s__32047__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__32049),null);
}
} else {
var vec__32056 = cljs.core.first(s__32047__$2);
var i = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__32056,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__32056,(1),null);
return cljs.core.cons(cljs.core.with_meta(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th.jh-key.jh-array-key","th.jh-key.jh-array-key",-1623829832),i], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td.jh-value.jh-array-value","td.jh-value.jh-array-value",1645079212),(json_html.core.render_html.cljs$core$IFn$_invoke$arity$1 ? json_html.core.render_html.cljs$core$IFn$_invoke$arity$1(v) : json_html.core.render_html.call(null,v))], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),i], null)),json_html$core$render_collection_$_iter__32046(cljs.core.rest(s__32047__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(cljs.core.map_indexed.cljs$core$IFn$_invoke$arity$2(cljs.core.vector,col));
})()], null)], null);
}
});

json_html.core.render_set = (function json_html$core$render_set(s){
if(cljs.core.empty_QMARK_(s)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.jh-type-set","div.jh-type-set",-367189823),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.jh-empty-set","span.jh-empty-set",-1364654969)], null)], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul","ul",-1349521403),(function (){var iter__4523__auto__ = (function json_html$core$render_set_$_iter__32069(s__32070){
return (new cljs.core.LazySeq(null,(function (){
var s__32070__$1 = s__32070;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__32070__$1);
if(temp__5735__auto__){
var s__32070__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__32070__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__32070__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__32072 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__32071 = (0);
while(true){
if((i__32071 < size__4522__auto__)){
var item = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__32071);
cljs.core.chunk_append(b__32072,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li.jh-value","li.jh-value",-344939266),(json_html.core.render_html.cljs$core$IFn$_invoke$arity$1 ? json_html.core.render_html.cljs$core$IFn$_invoke$arity$1(item) : json_html.core.render_html.call(null,item))], null));

var G__32255 = (i__32071 + (1));
i__32071 = G__32255;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__32072),json_html$core$render_set_$_iter__32069(cljs.core.chunk_rest(s__32070__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__32072),null);
}
} else {
var item = cljs.core.first(s__32070__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li.jh-value","li.jh-value",-344939266),(json_html.core.render_html.cljs$core$IFn$_invoke$arity$1 ? json_html.core.render_html.cljs$core$IFn$_invoke$arity$1(item) : json_html.core.render_html.call(null,item))], null),json_html$core$render_set_$_iter__32069(cljs.core.rest(s__32070__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(json_html.core.sort_set(s));
})()], null);
}
});

json_html.core.render_map = (function json_html$core$render_map(m){
if(cljs.core.empty_QMARK_(m)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.jh-type-object","div.jh-type-object",1704701538),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.jh-empty-map","span.jh-empty-map",-2061532971)], null)], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"table.jh-type-object","table.jh-type-object",-163995322),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tbody","tbody",-80678300),(function (){var iter__4523__auto__ = (function json_html$core$render_map_$_iter__32077(s__32078){
return (new cljs.core.LazySeq(null,(function (){
var s__32078__$1 = s__32078;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__32078__$1);
if(temp__5735__auto__){
var s__32078__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__32078__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__32078__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__32080 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__32079 = (0);
while(true){
if((i__32079 < size__4522__auto__)){
var vec__32087 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__32079);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__32087,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__32087,(1),null);
cljs.core.chunk_append(b__32080,cljs.core.with_meta(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th.jh-key.jh-object-key","th.jh-key.jh-object-key",1382268279),(json_html.core.render_html.cljs$core$IFn$_invoke$arity$1 ? json_html.core.render_html.cljs$core$IFn$_invoke$arity$1(k) : json_html.core.render_html.call(null,k))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td.jh-value.jh-object-value","td.jh-value.jh-object-value",-2080818691),(json_html.core.render_html.cljs$core$IFn$_invoke$arity$1 ? json_html.core.render_html.cljs$core$IFn$_invoke$arity$1(v) : json_html.core.render_html.call(null,v))], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),k], null)));

var G__32266 = (i__32079 + (1));
i__32079 = G__32266;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__32080),json_html$core$render_map_$_iter__32077(cljs.core.chunk_rest(s__32078__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__32080),null);
}
} else {
var vec__32095 = cljs.core.first(s__32078__$2);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__32095,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__32095,(1),null);
return cljs.core.cons(cljs.core.with_meta(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th.jh-key.jh-object-key","th.jh-key.jh-object-key",1382268279),(json_html.core.render_html.cljs$core$IFn$_invoke$arity$1 ? json_html.core.render_html.cljs$core$IFn$_invoke$arity$1(k) : json_html.core.render_html.call(null,k))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td.jh-value.jh-object-value","td.jh-value.jh-object-value",-2080818691),(json_html.core.render_html.cljs$core$IFn$_invoke$arity$1 ? json_html.core.render_html.cljs$core$IFn$_invoke$arity$1(v) : json_html.core.render_html.call(null,v))], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),k], null)),json_html$core$render_map_$_iter__32077(cljs.core.rest(s__32078__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(json_html.core.sort_map(m));
})()], null)], null);
}
});

json_html.core.render_string = (function json_html$core$render_string(s){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.jh-type-string","span.jh-type-string",-94106783),((clojure.string.blank_QMARK_(s))?new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.jh-empty-string","span.jh-empty-string",1227187446)], null):json_html.core.escape_html(s))], null);
});

json_html.core.render_html = (function json_html$core$render_html(v){
var t = cljs.core.type(v);
if((((!((v == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === v.json_html$core$Render$))))?true:(((!v.cljs$lang$protocol_mask$partition$))?cljs.core.native_satisfies_QMARK_(json_html.core.Render,v):false)):cljs.core.native_satisfies_QMARK_(json_html.core.Render,v))){
return json_html.core.render(v);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(t,cljs.core.Keyword)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.jh-type-string","span.jh-type-string",-94106783),json_html.core.render_keyword(v)], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(t,cljs.core.Symbol)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.jh-type-string","span.jh-type-string",-94106783),cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(t,String)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.jh-type-string","span.jh-type-string",-94106783),json_html.core.render_string(v)], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(t,Date)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.jh-type-date","span.jh-type-date",-1243309956),v.toString()], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(t,Boolean)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.jh-type-bool","span.jh-type-bool",53751640),cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(t,Number)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.jh-type-number","span.jh-type-number",1495617844),v], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(t,Array)){
var G__32134 = cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(v);
return (json_html.core.render_html.cljs$core$IFn$_invoke$arity$1 ? json_html.core.render_html.cljs$core$IFn$_invoke$arity$1(G__32134) : json_html.core.render_html.call(null,G__32134));
} else {
if((((!((v == null))))?(((((v.cljs$lang$protocol_mask$partition0$ & (1024))) || ((cljs.core.PROTOCOL_SENTINEL === v.cljs$core$IMap$))))?true:(((!v.cljs$lang$protocol_mask$partition0$))?cljs.core.native_satisfies_QMARK_(cljs.core.IMap,v):false)):cljs.core.native_satisfies_QMARK_(cljs.core.IMap,v))){
return json_html.core.render_map(v);
} else {
if((((!((v == null))))?(((((v.cljs$lang$protocol_mask$partition0$ & (4096))) || ((cljs.core.PROTOCOL_SENTINEL === v.cljs$core$ISet$))))?true:(((!v.cljs$lang$protocol_mask$partition0$))?cljs.core.native_satisfies_QMARK_(cljs.core.ISet,v):false)):cljs.core.native_satisfies_QMARK_(cljs.core.ISet,v))){
return json_html.core.render_set(v);
} else {
if((((!((v == null))))?(((((v.cljs$lang$protocol_mask$partition0$ & (8))) || ((cljs.core.PROTOCOL_SENTINEL === v.cljs$core$ICollection$))))?true:(((!v.cljs$lang$protocol_mask$partition0$))?cljs.core.native_satisfies_QMARK_(cljs.core.ICollection,v):false)):cljs.core.native_satisfies_QMARK_(cljs.core.ICollection,v))){
return json_html.core.render_collection(v);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(t,Object)){
var G__32197 = json_html.core.obj__GT_clj(v);
return (json_html.core.render_html.cljs$core$IFn$_invoke$arity$1 ? json_html.core.render_html.cljs$core$IFn$_invoke$arity$1(G__32197) : json_html.core.render_html.call(null,G__32197));
} else {
return null;

}
}
}
}
}
}
}
}
}
}
}
}
});

json_html.core.edn__GT_hiccup = (function json_html$core$edn__GT_hiccup(edn){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.jh-root","div.jh-root",-111047998),json_html.core.render_html(edn)], null);
});

json_html.core.edn__GT_html = (function json_html$core$edn__GT_html(edn){
return json_html.core.linkify_links(json_html.html.html(json_html.core.edn__GT_hiccup(edn)));
});

json_html.core.json__GT_hiccup = (function json_html$core$json__GT_hiccup(json){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.jh-root","div.jh-root",-111047998),json_html.core.render_html(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(json))], null);
});

json_html.core.json__GT_html = (function json_html$core$json__GT_html(json){
return json_html.core.linkify_links(json_html.html.html(json_html.core.json__GT_hiccup(json)));
});

//# sourceMappingURL=json_html.core.js.map
