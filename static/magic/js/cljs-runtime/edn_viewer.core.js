goog.provide('edn_viewer.core');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('json_html.core');
goog.require('cljs.reader');
if((typeof edn_viewer !== 'undefined') && (typeof edn_viewer.core !== 'undefined') && (typeof edn_viewer.core.app_state !== 'undefined')){
} else {
edn_viewer.core.app_state = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"text","text",-1790561697),"{:please [Enter some edn]}"], null));
}
edn_viewer.core.viewing_area = (function edn_viewer$core$viewing_area(text){
try{var edn = cljs.reader.read_string.cljs$core$IFn$_invoke$arity$1(text);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"dangerouslySetInnerHTML","dangerouslySetInnerHTML",-554971138),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"__html","__html",674048345),json_html.core.edn__GT_html(edn)], null)], null)], null);
}catch (e36384){if((e36384 instanceof Error)){
var e = e36384;
try{var edn = cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(text);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"dangerouslySetInnerHTML","dangerouslySetInnerHTML",-554971138),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"__html","__html",674048345),json_html.core.json__GT_html(edn)], null)], null)], null);
}catch (e36385){if((e36385 instanceof Error)){
var e__$1 = e36385;
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),"Sorry, but that isn't edn! ",new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"color","color",1011675173),"grey"], null)], null),"(or json)"], null),"."], null);
} else {
throw e36385;

}
}} else {
throw e36384;

}
}});
edn_viewer.core.main = (function edn_viewer$core$main(){
var map__36387 = cljs.core.deref(edn_viewer.core.app_state);
var map__36387__$1 = (((((!((map__36387 == null))))?(((((map__36387.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36387.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36387):map__36387);
var text = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36387__$1,new cljs.core.Keyword(null,"text","text",-1790561697));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"background-color","background-color",570434026),"#ddd"], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"color","color",1011675173),"purple"], null)], null),"MAGIC"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"font-weight","font-weight",2085804583),"600"], null)], null)," edn "], null),"Viewer ",new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"color","color",1011675173),"grey"], null)], null),"(json also)"], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.wrapper","div.wrapper",-1768248555),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.two","div.two",1249735052),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [edn_viewer.core.viewing_area,text], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.one","div.one",914968339),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"textarea.textarea","textarea.textarea",-1013865997),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"height","height",1025178622),"95vh"], null),new cljs.core.Keyword(null,"value","value",305978217),text,new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (map__36387,map__36387__$1,text){
return (function (p1__36386_SHARP_){
var val = p1__36386_SHARP_.target.value;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(edn_viewer.core.app_state,cljs.core.assoc,new cljs.core.Keyword(null,"text","text",-1790561697),val);
});})(map__36387,map__36387__$1,text))
], null)], null)], null)], null)], null);
});
edn_viewer.core.init = (function edn_viewer$core$init(){
return reagent.core.render.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [edn_viewer.core.main], null),document.getElementById("root"));
});

//# sourceMappingURL=edn_viewer.core.js.map
