goog.provide('shadow.dom');
goog.require('cljs.core');
goog.require('goog.dom');
goog.require('goog.dom.forms');
goog.require('goog.dom.classlist');
goog.require('goog.style');
goog.require('goog.style.transition');
goog.require('goog.string');
goog.require('clojure.string');
goog.require('cljs.core.async');
shadow.dom.transition_supported_QMARK_ = (((typeof window !== 'undefined'))?goog.style.transition.isSupported():null);

/**
 * @interface
 */
shadow.dom.IElement = function(){};

shadow.dom._to_dom = (function shadow$dom$_to_dom(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$IElement$_to_dom$arity$1 == null)))))){
return this$.shadow$dom$IElement$_to_dom$arity$1(this$);
} else {
var x__4433__auto__ = (((this$ == null))?null:this$);
var m__4434__auto__ = (shadow.dom._to_dom[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4434__auto__.call(null,this$));
} else {
var m__4431__auto__ = (shadow.dom._to_dom["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4431__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("IElement.-to-dom",this$);
}
}
}
});


/**
 * @interface
 */
shadow.dom.SVGElement = function(){};

shadow.dom._to_svg = (function shadow$dom$_to_svg(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$SVGElement$_to_svg$arity$1 == null)))))){
return this$.shadow$dom$SVGElement$_to_svg$arity$1(this$);
} else {
var x__4433__auto__ = (((this$ == null))?null:this$);
var m__4434__auto__ = (shadow.dom._to_svg[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4434__auto__.call(null,this$));
} else {
var m__4431__auto__ = (shadow.dom._to_svg["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4431__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("SVGElement.-to-svg",this$);
}
}
}
});

shadow.dom.lazy_native_coll_seq = (function shadow$dom$lazy_native_coll_seq(coll,idx){
if((idx < coll.length)){
return (new cljs.core.LazySeq(null,(function (){
return cljs.core.cons((coll[idx]),(function (){var G__33640 = coll;
var G__33641 = (idx + (1));
return (shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2 ? shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2(G__33640,G__33641) : shadow.dom.lazy_native_coll_seq.call(null,G__33640,G__33641));
})());
}),null,null));
} else {
return null;
}
});

/**
* @constructor
 * @implements {cljs.core.IIndexed}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IDeref}
 * @implements {shadow.dom.IElement}
*/
shadow.dom.NativeColl = (function (coll){
this.coll = coll;
this.cljs$lang$protocol_mask$partition0$ = 8421394;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
shadow.dom.NativeColl.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
});

shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (this$,n){
var self__ = this;
var this$__$1 = this;
return (self__.coll[n]);
});

shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (this$,n,not_found){
var self__ = this;
var this$__$1 = this;
var or__4131__auto__ = (self__.coll[n]);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return not_found;
}
});

shadow.dom.NativeColl.prototype.cljs$core$ICounted$_count$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll.length;
});

shadow.dom.NativeColl.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return shadow.dom.lazy_native_coll_seq(self__.coll,(0));
});

shadow.dom.NativeColl.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

shadow.dom.NativeColl.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
});

shadow.dom.NativeColl.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"coll","coll",-1006698606,null)], null);
});

shadow.dom.NativeColl.cljs$lang$type = true;

shadow.dom.NativeColl.cljs$lang$ctorStr = "shadow.dom/NativeColl";

shadow.dom.NativeColl.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"shadow.dom/NativeColl");
});

/**
 * Positional factory function for shadow.dom/NativeColl.
 */
shadow.dom.__GT_NativeColl = (function shadow$dom$__GT_NativeColl(coll){
return (new shadow.dom.NativeColl(coll));
});

shadow.dom.native_coll = (function shadow$dom$native_coll(coll){
return (new shadow.dom.NativeColl(coll));
});
shadow.dom.dom_node = (function shadow$dom$dom_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$IElement$))))?true:false):false)){
return el.shadow$dom$IElement$_to_dom$arity$1(null);
} else {
if(typeof el === 'string'){
return document.createTextNode(el);
} else {
if(typeof el === 'number'){
return document.createTextNode(cljs.core.str.cljs$core$IFn$_invoke$arity$1(el));
} else {
return el;

}
}
}
}
});
shadow.dom.query_one = (function shadow$dom$query_one(var_args){
var G__33702 = arguments.length;
switch (G__33702) {
case 1:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return document.querySelector(sel);
});

shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return shadow.dom.dom_node(root).querySelector(sel);
});

shadow.dom.query_one.cljs$lang$maxFixedArity = 2;

shadow.dom.query = (function shadow$dom$query(var_args){
var G__33715 = arguments.length;
switch (G__33715) {
case 1:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.query.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return (new shadow.dom.NativeColl(document.querySelectorAll(sel)));
});

shadow.dom.query.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(root).querySelectorAll(sel)));
});

shadow.dom.query.cljs$lang$maxFixedArity = 2;

shadow.dom.by_id = (function shadow$dom$by_id(var_args){
var G__33722 = arguments.length;
switch (G__33722) {
case 2:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2 = (function (id,el){
return shadow.dom.dom_node(el).getElementById(id);
});

shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1 = (function (id){
return document.getElementById(id);
});

shadow.dom.by_id.cljs$lang$maxFixedArity = 2;

shadow.dom.build = shadow.dom.dom_node;
shadow.dom.ev_stop = (function shadow$dom$ev_stop(var_args){
var G__33734 = arguments.length;
switch (G__33734) {
case 1:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1 = (function (e){
if(cljs.core.truth_(e.stopPropagation)){
e.stopPropagation();

e.preventDefault();
} else {
e.cancelBubble = true;

e.returnValue = false;
}

return e;
});

shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2 = (function (e,el){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
});

shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4 = (function (e,el,scope,owner){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
});

shadow.dom.ev_stop.cljs$lang$maxFixedArity = 4;

/**
 * check wether a parent node (or the document) contains the child
 */
shadow.dom.contains_QMARK_ = (function shadow$dom$contains_QMARK_(var_args){
var G__33745 = arguments.length;
switch (G__33745) {
case 1:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1 = (function (el){
var G__33749 = document;
var G__33750 = shadow.dom.dom_node(el);
return goog.dom.contains(G__33749,G__33750);
});

shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2 = (function (parent,el){
var G__33753 = shadow.dom.dom_node(parent);
var G__33754 = shadow.dom.dom_node(el);
return goog.dom.contains(G__33753,G__33754);
});

shadow.dom.contains_QMARK_.cljs$lang$maxFixedArity = 2;

shadow.dom.add_class = (function shadow$dom$add_class(el,cls){
var G__33755 = shadow.dom.dom_node(el);
var G__33756 = cls;
return goog.dom.classlist.add(G__33755,G__33756);
});
shadow.dom.remove_class = (function shadow$dom$remove_class(el,cls){
var G__33762 = shadow.dom.dom_node(el);
var G__33763 = cls;
return goog.dom.classlist.remove(G__33762,G__33763);
});
shadow.dom.toggle_class = (function shadow$dom$toggle_class(var_args){
var G__33769 = arguments.length;
switch (G__33769) {
case 2:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2 = (function (el,cls){
var G__33775 = shadow.dom.dom_node(el);
var G__33776 = cls;
return goog.dom.classlist.toggle(G__33775,G__33776);
});

shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3 = (function (el,cls,v){
if(cljs.core.truth_(v)){
return shadow.dom.add_class(el,cls);
} else {
return shadow.dom.remove_class(el,cls);
}
});

shadow.dom.toggle_class.cljs$lang$maxFixedArity = 3;

shadow.dom.dom_listen = (cljs.core.truth_((function (){var or__4131__auto__ = (!((typeof document !== 'undefined')));
if(or__4131__auto__){
return or__4131__auto__;
} else {
return document.addEventListener;
}
})())?(function shadow$dom$dom_listen_good(el,ev,handler){
return el.addEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_ie(el,ev,handler){
try{return el.attachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),(function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
}));
}catch (e33794){if((e33794 instanceof Object)){
var e = e33794;
return console.log("didnt support attachEvent",el,e);
} else {
throw e33794;

}
}}));
shadow.dom.dom_listen_remove = (cljs.core.truth_((function (){var or__4131__auto__ = (!((typeof document !== 'undefined')));
if(or__4131__auto__){
return or__4131__auto__;
} else {
return document.removeEventListener;
}
})())?(function shadow$dom$dom_listen_remove_good(el,ev,handler){
return el.removeEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_remove_ie(el,ev,handler){
return el.detachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),handler);
}));
shadow.dom.on_query = (function shadow$dom$on_query(root_el,ev,selector,handler){
var seq__33806 = cljs.core.seq(shadow.dom.query.cljs$core$IFn$_invoke$arity$2(selector,root_el));
var chunk__33807 = null;
var count__33808 = (0);
var i__33809 = (0);
while(true){
if((i__33809 < count__33808)){
var el = chunk__33807.cljs$core$IIndexed$_nth$arity$2(null,i__33809);
var handler_34609__$1 = ((function (seq__33806,chunk__33807,count__33808,i__33809,el){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__33806,chunk__33807,count__33808,i__33809,el))
;
var G__33825_34610 = el;
var G__33826_34611 = cljs.core.name(ev);
var G__33827_34612 = handler_34609__$1;
(shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__33825_34610,G__33826_34611,G__33827_34612) : shadow.dom.dom_listen.call(null,G__33825_34610,G__33826_34611,G__33827_34612));


var G__34613 = seq__33806;
var G__34614 = chunk__33807;
var G__34615 = count__33808;
var G__34616 = (i__33809 + (1));
seq__33806 = G__34613;
chunk__33807 = G__34614;
count__33808 = G__34615;
i__33809 = G__34616;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__33806);
if(temp__5735__auto__){
var seq__33806__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__33806__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__33806__$1);
var G__34617 = cljs.core.chunk_rest(seq__33806__$1);
var G__34618 = c__4550__auto__;
var G__34619 = cljs.core.count(c__4550__auto__);
var G__34620 = (0);
seq__33806 = G__34617;
chunk__33807 = G__34618;
count__33808 = G__34619;
i__33809 = G__34620;
continue;
} else {
var el = cljs.core.first(seq__33806__$1);
var handler_34621__$1 = ((function (seq__33806,chunk__33807,count__33808,i__33809,el,seq__33806__$1,temp__5735__auto__){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__33806,chunk__33807,count__33808,i__33809,el,seq__33806__$1,temp__5735__auto__))
;
var G__33831_34622 = el;
var G__33832_34623 = cljs.core.name(ev);
var G__33833_34624 = handler_34621__$1;
(shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__33831_34622,G__33832_34623,G__33833_34624) : shadow.dom.dom_listen.call(null,G__33831_34622,G__33832_34623,G__33833_34624));


var G__34625 = cljs.core.next(seq__33806__$1);
var G__34626 = null;
var G__34627 = (0);
var G__34628 = (0);
seq__33806 = G__34625;
chunk__33807 = G__34626;
count__33808 = G__34627;
i__33809 = G__34628;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.on = (function shadow$dom$on(var_args){
var G__33839 = arguments.length;
switch (G__33839) {
case 3:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.on.cljs$core$IFn$_invoke$arity$3 = (function (el,ev,handler){
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4(el,ev,handler,false);
});

shadow.dom.on.cljs$core$IFn$_invoke$arity$4 = (function (el,ev,handler,capture){
if(cljs.core.vector_QMARK_(ev)){
return shadow.dom.on_query(el,cljs.core.first(ev),cljs.core.second(ev),handler);
} else {
var handler__$1 = (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});
var G__33845 = shadow.dom.dom_node(el);
var G__33846 = cljs.core.name(ev);
var G__33847 = handler__$1;
return (shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__33845,G__33846,G__33847) : shadow.dom.dom_listen.call(null,G__33845,G__33846,G__33847));
}
});

shadow.dom.on.cljs$lang$maxFixedArity = 4;

shadow.dom.remove_event_handler = (function shadow$dom$remove_event_handler(el,ev,handler){
var G__33849 = shadow.dom.dom_node(el);
var G__33850 = cljs.core.name(ev);
var G__33851 = handler;
return (shadow.dom.dom_listen_remove.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen_remove.cljs$core$IFn$_invoke$arity$3(G__33849,G__33850,G__33851) : shadow.dom.dom_listen_remove.call(null,G__33849,G__33850,G__33851));
});
shadow.dom.add_event_listeners = (function shadow$dom$add_event_listeners(el,events){
var seq__33853 = cljs.core.seq(events);
var chunk__33854 = null;
var count__33855 = (0);
var i__33856 = (0);
while(true){
if((i__33856 < count__33855)){
var vec__33866 = chunk__33854.cljs$core$IIndexed$_nth$arity$2(null,i__33856);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33866,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33866,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__34643 = seq__33853;
var G__34644 = chunk__33854;
var G__34645 = count__33855;
var G__34646 = (i__33856 + (1));
seq__33853 = G__34643;
chunk__33854 = G__34644;
count__33855 = G__34645;
i__33856 = G__34646;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__33853);
if(temp__5735__auto__){
var seq__33853__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__33853__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__33853__$1);
var G__34648 = cljs.core.chunk_rest(seq__33853__$1);
var G__34649 = c__4550__auto__;
var G__34650 = cljs.core.count(c__4550__auto__);
var G__34651 = (0);
seq__33853 = G__34648;
chunk__33854 = G__34649;
count__33855 = G__34650;
i__33856 = G__34651;
continue;
} else {
var vec__33870 = cljs.core.first(seq__33853__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33870,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33870,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__34653 = cljs.core.next(seq__33853__$1);
var G__34654 = null;
var G__34655 = (0);
var G__34656 = (0);
seq__33853 = G__34653;
chunk__33854 = G__34654;
count__33855 = G__34655;
i__33856 = G__34656;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_style = (function shadow$dom$set_style(el,styles){
var dom = shadow.dom.dom_node(el);
var seq__33874 = cljs.core.seq(styles);
var chunk__33875 = null;
var count__33876 = (0);
var i__33877 = (0);
while(true){
if((i__33877 < count__33876)){
var vec__33896 = chunk__33875.cljs$core$IIndexed$_nth$arity$2(null,i__33877);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33896,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33896,(1),null);
var G__33899_34660 = dom;
var G__33900_34661 = cljs.core.name(k);
var G__33901_34662 = (((v == null))?"":v);
goog.style.setStyle(G__33899_34660,G__33900_34661,G__33901_34662);


var G__34664 = seq__33874;
var G__34665 = chunk__33875;
var G__34666 = count__33876;
var G__34667 = (i__33877 + (1));
seq__33874 = G__34664;
chunk__33875 = G__34665;
count__33876 = G__34666;
i__33877 = G__34667;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__33874);
if(temp__5735__auto__){
var seq__33874__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__33874__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__33874__$1);
var G__34668 = cljs.core.chunk_rest(seq__33874__$1);
var G__34669 = c__4550__auto__;
var G__34670 = cljs.core.count(c__4550__auto__);
var G__34671 = (0);
seq__33874 = G__34668;
chunk__33875 = G__34669;
count__33876 = G__34670;
i__33877 = G__34671;
continue;
} else {
var vec__33903 = cljs.core.first(seq__33874__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33903,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33903,(1),null);
var G__33907_34674 = dom;
var G__33908_34675 = cljs.core.name(k);
var G__33909_34676 = (((v == null))?"":v);
goog.style.setStyle(G__33907_34674,G__33908_34675,G__33909_34676);


var G__34677 = cljs.core.next(seq__33874__$1);
var G__34678 = null;
var G__34679 = (0);
var G__34680 = (0);
seq__33874 = G__34677;
chunk__33875 = G__34678;
count__33876 = G__34679;
i__33877 = G__34680;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_attr_STAR_ = (function shadow$dom$set_attr_STAR_(el,key,value){
var G__33917_34682 = key;
var G__33917_34683__$1 = (((G__33917_34682 instanceof cljs.core.Keyword))?G__33917_34682.fqn:null);
switch (G__33917_34683__$1) {
case "id":
el.id = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value);

break;
case "class":
el.className = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value);

break;
case "for":
el.htmlFor = value;

break;
case "cellpadding":
el.setAttribute("cellPadding",value);

break;
case "cellspacing":
el.setAttribute("cellSpacing",value);

break;
case "colspan":
el.setAttribute("colSpan",value);

break;
case "frameborder":
el.setAttribute("frameBorder",value);

break;
case "height":
el.setAttribute("height",value);

break;
case "maxlength":
el.setAttribute("maxLength",value);

break;
case "role":
el.setAttribute("role",value);

break;
case "rowspan":
el.setAttribute("rowSpan",value);

break;
case "type":
el.setAttribute("type",value);

break;
case "usemap":
el.setAttribute("useMap",value);

break;
case "valign":
el.setAttribute("vAlign",value);

break;
case "width":
el.setAttribute("width",value);

break;
case "on":
shadow.dom.add_event_listeners(el,value);

break;
case "style":
if((value == null)){
} else {
if(typeof value === 'string'){
el.setAttribute("style",value);
} else {
if(cljs.core.map_QMARK_(value)){
shadow.dom.set_style(el,value);
} else {
goog.style.setStyle(el,value);

}
}
}

break;
default:
var ks_34686 = cljs.core.name(key);
if(cljs.core.truth_((function (){var or__4131__auto__ = goog.string.startsWith(ks_34686,"data-");
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return goog.string.startsWith(ks_34686,"aria-");
}
})())){
el.setAttribute(ks_34686,value);
} else {
(el[ks_34686] = value);
}

}

return el;
});
shadow.dom.set_attrs = (function shadow$dom$set_attrs(el,attrs){
return cljs.core.reduce_kv((function (el__$1,key,value){
shadow.dom.set_attr_STAR_(el__$1,key,value);

return el__$1;
}),shadow.dom.dom_node(el),attrs);
});
shadow.dom.set_attr = (function shadow$dom$set_attr(el,key,value){
return shadow.dom.set_attr_STAR_(shadow.dom.dom_node(el),key,value);
});
shadow.dom.has_class_QMARK_ = (function shadow$dom$has_class_QMARK_(el,cls){
var G__33930 = shadow.dom.dom_node(el);
var G__33931 = cls;
return goog.dom.classlist.contains(G__33930,G__33931);
});
shadow.dom.merge_class_string = (function shadow$dom$merge_class_string(current,extra_class){
if(cljs.core.seq(current)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(current)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(extra_class)].join('');
} else {
return extra_class;
}
});
shadow.dom.parse_tag = (function shadow$dom$parse_tag(spec){
var spec__$1 = cljs.core.name(spec);
var fdot = spec__$1.indexOf(".");
var fhash = spec__$1.indexOf("#");
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1,null,null], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fdot),null,clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1))),null], null);
} else {
if((fhash > fdot)){
throw ["cant have id after class?",spec__$1].join('');
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1)),fdot),clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);

}
}
}
}
});
shadow.dom.create_dom_node = (function shadow$dom$create_dom_node(tag_def,p__33938){
var map__33939 = p__33938;
var map__33939__$1 = (((((!((map__33939 == null))))?(((((map__33939.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33939.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33939):map__33939);
var props = map__33939__$1;
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33939__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var tag_props = ({});
var vec__33943 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33943,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33943,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33943,(2),null);
if(cljs.core.truth_(tag_id)){
(tag_props["id"] = tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
(tag_props["class"] = shadow.dom.merge_class_string(class$,tag_classes));
} else {
}

var G__33947 = goog.dom.createDom(tag_name,tag_props);
shadow.dom.set_attrs(G__33947,cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(props,new cljs.core.Keyword(null,"class","class",-2030961996)));

return G__33947;
});
shadow.dom.append = (function shadow$dom$append(var_args){
var G__33950 = arguments.length;
switch (G__33950) {
case 1:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.append.cljs$core$IFn$_invoke$arity$1 = (function (node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
document.body.appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
});

shadow.dom.append.cljs$core$IFn$_invoke$arity$2 = (function (el,node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
shadow.dom.dom_node(el).appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
});

shadow.dom.append.cljs$lang$maxFixedArity = 2;

shadow.dom.destructure_node = (function shadow$dom$destructure_node(create_fn,p__33954){
var vec__33955 = p__33954;
var seq__33956 = cljs.core.seq(vec__33955);
var first__33957 = cljs.core.first(seq__33956);
var seq__33956__$1 = cljs.core.next(seq__33956);
var nn = first__33957;
var first__33957__$1 = cljs.core.first(seq__33956__$1);
var seq__33956__$2 = cljs.core.next(seq__33956__$1);
var np = first__33957__$1;
var nc = seq__33956__$2;
var node = vec__33955;
if((nn instanceof cljs.core.Keyword)){
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("invalid dom node",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"node","node",581201198),node], null));
}

if((((np == null)) && ((nc == null)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__33959 = nn;
var G__33960 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__33959,G__33960) : create_fn.call(null,G__33959,G__33960));
})(),cljs.core.List.EMPTY], null);
} else {
if(cljs.core.map_QMARK_(np)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(nn,np) : create_fn.call(null,nn,np)),nc], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__33962 = nn;
var G__33963 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__33962,G__33963) : create_fn.call(null,G__33962,G__33963));
})(),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(nc,np)], null);

}
}
});
shadow.dom.make_dom_node = (function shadow$dom$make_dom_node(structure){
var vec__33966 = shadow.dom.destructure_node(shadow.dom.create_dom_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33966,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33966,(1),null);
var seq__33970_34705 = cljs.core.seq(node_children);
var chunk__33971_34706 = null;
var count__33972_34707 = (0);
var i__33973_34708 = (0);
while(true){
if((i__33973_34708 < count__33972_34707)){
var child_struct_34709 = chunk__33971_34706.cljs$core$IIndexed$_nth$arity$2(null,i__33973_34708);
var children_34710 = shadow.dom.dom_node(child_struct_34709);
if(cljs.core.seq_QMARK_(children_34710)){
var seq__34005_34712 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_34710));
var chunk__34007_34713 = null;
var count__34008_34714 = (0);
var i__34009_34715 = (0);
while(true){
if((i__34009_34715 < count__34008_34714)){
var child_34717 = chunk__34007_34713.cljs$core$IIndexed$_nth$arity$2(null,i__34009_34715);
if(cljs.core.truth_(child_34717)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_34717);


var G__34720 = seq__34005_34712;
var G__34721 = chunk__34007_34713;
var G__34722 = count__34008_34714;
var G__34723 = (i__34009_34715 + (1));
seq__34005_34712 = G__34720;
chunk__34007_34713 = G__34721;
count__34008_34714 = G__34722;
i__34009_34715 = G__34723;
continue;
} else {
var G__34724 = seq__34005_34712;
var G__34725 = chunk__34007_34713;
var G__34726 = count__34008_34714;
var G__34727 = (i__34009_34715 + (1));
seq__34005_34712 = G__34724;
chunk__34007_34713 = G__34725;
count__34008_34714 = G__34726;
i__34009_34715 = G__34727;
continue;
}
} else {
var temp__5735__auto___34728 = cljs.core.seq(seq__34005_34712);
if(temp__5735__auto___34728){
var seq__34005_34729__$1 = temp__5735__auto___34728;
if(cljs.core.chunked_seq_QMARK_(seq__34005_34729__$1)){
var c__4550__auto___34730 = cljs.core.chunk_first(seq__34005_34729__$1);
var G__34732 = cljs.core.chunk_rest(seq__34005_34729__$1);
var G__34733 = c__4550__auto___34730;
var G__34734 = cljs.core.count(c__4550__auto___34730);
var G__34735 = (0);
seq__34005_34712 = G__34732;
chunk__34007_34713 = G__34733;
count__34008_34714 = G__34734;
i__34009_34715 = G__34735;
continue;
} else {
var child_34736 = cljs.core.first(seq__34005_34729__$1);
if(cljs.core.truth_(child_34736)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_34736);


var G__34737 = cljs.core.next(seq__34005_34729__$1);
var G__34738 = null;
var G__34739 = (0);
var G__34740 = (0);
seq__34005_34712 = G__34737;
chunk__34007_34713 = G__34738;
count__34008_34714 = G__34739;
i__34009_34715 = G__34740;
continue;
} else {
var G__34742 = cljs.core.next(seq__34005_34729__$1);
var G__34743 = null;
var G__34744 = (0);
var G__34745 = (0);
seq__34005_34712 = G__34742;
chunk__34007_34713 = G__34743;
count__34008_34714 = G__34744;
i__34009_34715 = G__34745;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_34710);
}


var G__34746 = seq__33970_34705;
var G__34747 = chunk__33971_34706;
var G__34748 = count__33972_34707;
var G__34749 = (i__33973_34708 + (1));
seq__33970_34705 = G__34746;
chunk__33971_34706 = G__34747;
count__33972_34707 = G__34748;
i__33973_34708 = G__34749;
continue;
} else {
var temp__5735__auto___34750 = cljs.core.seq(seq__33970_34705);
if(temp__5735__auto___34750){
var seq__33970_34751__$1 = temp__5735__auto___34750;
if(cljs.core.chunked_seq_QMARK_(seq__33970_34751__$1)){
var c__4550__auto___34752 = cljs.core.chunk_first(seq__33970_34751__$1);
var G__34753 = cljs.core.chunk_rest(seq__33970_34751__$1);
var G__34754 = c__4550__auto___34752;
var G__34755 = cljs.core.count(c__4550__auto___34752);
var G__34756 = (0);
seq__33970_34705 = G__34753;
chunk__33971_34706 = G__34754;
count__33972_34707 = G__34755;
i__33973_34708 = G__34756;
continue;
} else {
var child_struct_34758 = cljs.core.first(seq__33970_34751__$1);
var children_34760 = shadow.dom.dom_node(child_struct_34758);
if(cljs.core.seq_QMARK_(children_34760)){
var seq__34020_34761 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_34760));
var chunk__34022_34762 = null;
var count__34023_34763 = (0);
var i__34024_34764 = (0);
while(true){
if((i__34024_34764 < count__34023_34763)){
var child_34765 = chunk__34022_34762.cljs$core$IIndexed$_nth$arity$2(null,i__34024_34764);
if(cljs.core.truth_(child_34765)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_34765);


var G__34766 = seq__34020_34761;
var G__34767 = chunk__34022_34762;
var G__34768 = count__34023_34763;
var G__34769 = (i__34024_34764 + (1));
seq__34020_34761 = G__34766;
chunk__34022_34762 = G__34767;
count__34023_34763 = G__34768;
i__34024_34764 = G__34769;
continue;
} else {
var G__34771 = seq__34020_34761;
var G__34772 = chunk__34022_34762;
var G__34773 = count__34023_34763;
var G__34774 = (i__34024_34764 + (1));
seq__34020_34761 = G__34771;
chunk__34022_34762 = G__34772;
count__34023_34763 = G__34773;
i__34024_34764 = G__34774;
continue;
}
} else {
var temp__5735__auto___34776__$1 = cljs.core.seq(seq__34020_34761);
if(temp__5735__auto___34776__$1){
var seq__34020_34777__$1 = temp__5735__auto___34776__$1;
if(cljs.core.chunked_seq_QMARK_(seq__34020_34777__$1)){
var c__4550__auto___34778 = cljs.core.chunk_first(seq__34020_34777__$1);
var G__34779 = cljs.core.chunk_rest(seq__34020_34777__$1);
var G__34780 = c__4550__auto___34778;
var G__34781 = cljs.core.count(c__4550__auto___34778);
var G__34782 = (0);
seq__34020_34761 = G__34779;
chunk__34022_34762 = G__34780;
count__34023_34763 = G__34781;
i__34024_34764 = G__34782;
continue;
} else {
var child_34787 = cljs.core.first(seq__34020_34777__$1);
if(cljs.core.truth_(child_34787)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_34787);


var G__34788 = cljs.core.next(seq__34020_34777__$1);
var G__34789 = null;
var G__34790 = (0);
var G__34791 = (0);
seq__34020_34761 = G__34788;
chunk__34022_34762 = G__34789;
count__34023_34763 = G__34790;
i__34024_34764 = G__34791;
continue;
} else {
var G__34792 = cljs.core.next(seq__34020_34777__$1);
var G__34793 = null;
var G__34794 = (0);
var G__34795 = (0);
seq__34020_34761 = G__34792;
chunk__34022_34762 = G__34793;
count__34023_34763 = G__34794;
i__34024_34764 = G__34795;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_34760);
}


var G__34797 = cljs.core.next(seq__33970_34751__$1);
var G__34798 = null;
var G__34799 = (0);
var G__34800 = (0);
seq__33970_34705 = G__34797;
chunk__33971_34706 = G__34798;
count__33972_34707 = G__34799;
i__33973_34708 = G__34800;
continue;
}
} else {
}
}
break;
}

return node;
});
cljs.core.Keyword.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.Keyword.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$__$1], null));
});

cljs.core.PersistentVector.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.PersistentVector.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(this$__$1);
});

cljs.core.LazySeq.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.LazySeq.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_dom,this$__$1);
});
if(cljs.core.truth_(((typeof HTMLElement) != 'undefined'))){
HTMLElement.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

HTMLElement.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
});
} else {
}
if(cljs.core.truth_(((typeof DocumentFragment) != 'undefined'))){
DocumentFragment.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

DocumentFragment.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
});
} else {
}
/**
 * clear node children
 */
shadow.dom.reset = (function shadow$dom$reset(node){
var G__34039 = shadow.dom.dom_node(node);
return goog.dom.removeChildren(G__34039);
});
shadow.dom.remove = (function shadow$dom$remove(node){
if((((!((node == null))))?(((((node.cljs$lang$protocol_mask$partition0$ & (8388608))) || ((cljs.core.PROTOCOL_SENTINEL === node.cljs$core$ISeqable$))))?true:false):false)){
var seq__34045 = cljs.core.seq(node);
var chunk__34046 = null;
var count__34047 = (0);
var i__34048 = (0);
while(true){
if((i__34048 < count__34047)){
var n = chunk__34046.cljs$core$IIndexed$_nth$arity$2(null,i__34048);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__34809 = seq__34045;
var G__34810 = chunk__34046;
var G__34811 = count__34047;
var G__34812 = (i__34048 + (1));
seq__34045 = G__34809;
chunk__34046 = G__34810;
count__34047 = G__34811;
i__34048 = G__34812;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__34045);
if(temp__5735__auto__){
var seq__34045__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34045__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__34045__$1);
var G__34814 = cljs.core.chunk_rest(seq__34045__$1);
var G__34815 = c__4550__auto__;
var G__34816 = cljs.core.count(c__4550__auto__);
var G__34817 = (0);
seq__34045 = G__34814;
chunk__34046 = G__34815;
count__34047 = G__34816;
i__34048 = G__34817;
continue;
} else {
var n = cljs.core.first(seq__34045__$1);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__34819 = cljs.core.next(seq__34045__$1);
var G__34820 = null;
var G__34821 = (0);
var G__34822 = (0);
seq__34045 = G__34819;
chunk__34046 = G__34820;
count__34047 = G__34821;
i__34048 = G__34822;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return goog.dom.removeNode(node);
}
});
shadow.dom.replace_node = (function shadow$dom$replace_node(old,new$){
var G__34057 = shadow.dom.dom_node(new$);
var G__34058 = shadow.dom.dom_node(old);
return goog.dom.replaceNode(G__34057,G__34058);
});
shadow.dom.text = (function shadow$dom$text(var_args){
var G__34063 = arguments.length;
switch (G__34063) {
case 2:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.text.cljs$core$IFn$_invoke$arity$2 = (function (el,new_text){
return shadow.dom.dom_node(el).innerText = new_text;
});

shadow.dom.text.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.dom_node(el).innerText;
});

shadow.dom.text.cljs$lang$maxFixedArity = 2;

shadow.dom.check = (function shadow$dom$check(var_args){
var G__34068 = arguments.length;
switch (G__34068) {
case 1:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.check.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2(el,true);
});

shadow.dom.check.cljs$core$IFn$_invoke$arity$2 = (function (el,checked){
return shadow.dom.dom_node(el).checked = checked;
});

shadow.dom.check.cljs$lang$maxFixedArity = 2;

shadow.dom.checked_QMARK_ = (function shadow$dom$checked_QMARK_(el){
return shadow.dom.dom_node(el).checked;
});
shadow.dom.form_elements = (function shadow$dom$form_elements(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).elements));
});
shadow.dom.children = (function shadow$dom$children(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).children));
});
shadow.dom.child_nodes = (function shadow$dom$child_nodes(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).childNodes));
});
shadow.dom.attr = (function shadow$dom$attr(var_args){
var G__34077 = arguments.length;
switch (G__34077) {
case 2:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.attr.cljs$core$IFn$_invoke$arity$2 = (function (el,key){
return shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
});

shadow.dom.attr.cljs$core$IFn$_invoke$arity$3 = (function (el,key,default$){
var or__4131__auto__ = shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return default$;
}
});

shadow.dom.attr.cljs$lang$maxFixedArity = 3;

shadow.dom.del_attr = (function shadow$dom$del_attr(el,key){
return shadow.dom.dom_node(el).removeAttribute(cljs.core.name(key));
});
shadow.dom.data = (function shadow$dom$data(el,key){
return shadow.dom.dom_node(el).getAttribute(["data-",cljs.core.name(key)].join(''));
});
shadow.dom.set_data = (function shadow$dom$set_data(el,key,value){
return shadow.dom.dom_node(el).setAttribute(["data-",cljs.core.name(key)].join(''),cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));
});
shadow.dom.set_html = (function shadow$dom$set_html(node,text){
return shadow.dom.dom_node(node).innerHTML = text;
});
shadow.dom.get_html = (function shadow$dom$get_html(node){
return shadow.dom.dom_node(node).innerHTML;
});
shadow.dom.fragment = (function shadow$dom$fragment(var_args){
var args__4736__auto__ = [];
var len__4730__auto___34839 = arguments.length;
var i__4731__auto___34840 = (0);
while(true){
if((i__4731__auto___34840 < len__4730__auto___34839)){
args__4736__auto__.push((arguments[i__4731__auto___34840]));

var G__34841 = (i__4731__auto___34840 + (1));
i__4731__auto___34840 = G__34841;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic = (function (nodes){
var fragment = document.createDocumentFragment();
var seq__34119_34843 = cljs.core.seq(nodes);
var chunk__34121_34844 = null;
var count__34122_34845 = (0);
var i__34123_34846 = (0);
while(true){
if((i__34123_34846 < count__34122_34845)){
var node_34848 = chunk__34121_34844.cljs$core$IIndexed$_nth$arity$2(null,i__34123_34846);
fragment.appendChild(shadow.dom._to_dom(node_34848));


var G__34850 = seq__34119_34843;
var G__34851 = chunk__34121_34844;
var G__34852 = count__34122_34845;
var G__34853 = (i__34123_34846 + (1));
seq__34119_34843 = G__34850;
chunk__34121_34844 = G__34851;
count__34122_34845 = G__34852;
i__34123_34846 = G__34853;
continue;
} else {
var temp__5735__auto___34854 = cljs.core.seq(seq__34119_34843);
if(temp__5735__auto___34854){
var seq__34119_34855__$1 = temp__5735__auto___34854;
if(cljs.core.chunked_seq_QMARK_(seq__34119_34855__$1)){
var c__4550__auto___34856 = cljs.core.chunk_first(seq__34119_34855__$1);
var G__34857 = cljs.core.chunk_rest(seq__34119_34855__$1);
var G__34858 = c__4550__auto___34856;
var G__34859 = cljs.core.count(c__4550__auto___34856);
var G__34860 = (0);
seq__34119_34843 = G__34857;
chunk__34121_34844 = G__34858;
count__34122_34845 = G__34859;
i__34123_34846 = G__34860;
continue;
} else {
var node_34861 = cljs.core.first(seq__34119_34855__$1);
fragment.appendChild(shadow.dom._to_dom(node_34861));


var G__34862 = cljs.core.next(seq__34119_34855__$1);
var G__34863 = null;
var G__34864 = (0);
var G__34865 = (0);
seq__34119_34843 = G__34862;
chunk__34121_34844 = G__34863;
count__34122_34845 = G__34864;
i__34123_34846 = G__34865;
continue;
}
} else {
}
}
break;
}

return (new shadow.dom.NativeColl(fragment));
});

shadow.dom.fragment.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
shadow.dom.fragment.cljs$lang$applyTo = (function (seq34116){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq34116));
});

/**
 * given a html string, eval all <script> tags and return the html without the scripts
 * don't do this for everything, only content you trust.
 */
shadow.dom.eval_scripts = (function shadow$dom$eval_scripts(s){
var scripts = cljs.core.re_seq(/<script[^>]*?>(.+?)<\/script>/,s);
var seq__34137_34869 = cljs.core.seq(scripts);
var chunk__34138_34870 = null;
var count__34139_34871 = (0);
var i__34140_34872 = (0);
while(true){
if((i__34140_34872 < count__34139_34871)){
var vec__34151_34874 = chunk__34138_34870.cljs$core$IIndexed$_nth$arity$2(null,i__34140_34872);
var script_tag_34875 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34151_34874,(0),null);
var script_body_34876 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34151_34874,(1),null);
eval(script_body_34876);


var G__34877 = seq__34137_34869;
var G__34878 = chunk__34138_34870;
var G__34879 = count__34139_34871;
var G__34880 = (i__34140_34872 + (1));
seq__34137_34869 = G__34877;
chunk__34138_34870 = G__34878;
count__34139_34871 = G__34879;
i__34140_34872 = G__34880;
continue;
} else {
var temp__5735__auto___34881 = cljs.core.seq(seq__34137_34869);
if(temp__5735__auto___34881){
var seq__34137_34882__$1 = temp__5735__auto___34881;
if(cljs.core.chunked_seq_QMARK_(seq__34137_34882__$1)){
var c__4550__auto___34884 = cljs.core.chunk_first(seq__34137_34882__$1);
var G__34886 = cljs.core.chunk_rest(seq__34137_34882__$1);
var G__34887 = c__4550__auto___34884;
var G__34888 = cljs.core.count(c__4550__auto___34884);
var G__34889 = (0);
seq__34137_34869 = G__34886;
chunk__34138_34870 = G__34887;
count__34139_34871 = G__34888;
i__34140_34872 = G__34889;
continue;
} else {
var vec__34155_34890 = cljs.core.first(seq__34137_34882__$1);
var script_tag_34891 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34155_34890,(0),null);
var script_body_34892 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34155_34890,(1),null);
eval(script_body_34892);


var G__34894 = cljs.core.next(seq__34137_34882__$1);
var G__34895 = null;
var G__34896 = (0);
var G__34897 = (0);
seq__34137_34869 = G__34894;
chunk__34138_34870 = G__34895;
count__34139_34871 = G__34896;
i__34140_34872 = G__34897;
continue;
}
} else {
}
}
break;
}

return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (scripts){
return (function (s__$1,p__34159){
var vec__34160 = p__34159;
var script_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34160,(0),null);
var script_body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34160,(1),null);
return clojure.string.replace(s__$1,script_tag,"");
});})(scripts))
,s,scripts);
});
shadow.dom.str__GT_fragment = (function shadow$dom$str__GT_fragment(s){
var el = document.createElement("div");
el.innerHTML = s;

return (new shadow.dom.NativeColl(goog.dom.childrenToNode_(document,el)));
});
shadow.dom.node_name = (function shadow$dom$node_name(el){
return shadow.dom.dom_node(el).nodeName;
});
shadow.dom.ancestor_by_class = (function shadow$dom$ancestor_by_class(el,cls){
var G__34168 = shadow.dom.dom_node(el);
var G__34169 = cls;
return goog.dom.getAncestorByClass(G__34168,G__34169);
});
shadow.dom.ancestor_by_tag = (function shadow$dom$ancestor_by_tag(var_args){
var G__34176 = arguments.length;
switch (G__34176) {
case 2:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2 = (function (el,tag){
var G__34185 = shadow.dom.dom_node(el);
var G__34186 = cljs.core.name(tag);
return goog.dom.getAncestorByTagNameAndClass(G__34185,G__34186);
});

shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3 = (function (el,tag,cls){
var G__34187 = shadow.dom.dom_node(el);
var G__34188 = cljs.core.name(tag);
var G__34189 = cljs.core.name(cls);
return goog.dom.getAncestorByTagNameAndClass(G__34187,G__34188,G__34189);
});

shadow.dom.ancestor_by_tag.cljs$lang$maxFixedArity = 3;

shadow.dom.get_value = (function shadow$dom$get_value(dom){
var G__34190 = shadow.dom.dom_node(dom);
return goog.dom.forms.getValue(G__34190);
});
shadow.dom.set_value = (function shadow$dom$set_value(dom,value){
var G__34192 = shadow.dom.dom_node(dom);
var G__34193 = value;
return goog.dom.forms.setValue(G__34192,G__34193);
});
shadow.dom.px = (function shadow$dom$px(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((value | (0))),"px"].join('');
});
shadow.dom.pct = (function shadow$dom$pct(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"%"].join('');
});
shadow.dom.remove_style_STAR_ = (function shadow$dom$remove_style_STAR_(el,style){
return el.style.removeProperty(cljs.core.name(style));
});
shadow.dom.remove_style = (function shadow$dom$remove_style(el,style){
var el__$1 = shadow.dom.dom_node(el);
return shadow.dom.remove_style_STAR_(el__$1,style);
});
shadow.dom.remove_styles = (function shadow$dom$remove_styles(el,style_keys){
var el__$1 = shadow.dom.dom_node(el);
var seq__34200 = cljs.core.seq(style_keys);
var chunk__34201 = null;
var count__34202 = (0);
var i__34203 = (0);
while(true){
if((i__34203 < count__34202)){
var it = chunk__34201.cljs$core$IIndexed$_nth$arity$2(null,i__34203);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__34908 = seq__34200;
var G__34909 = chunk__34201;
var G__34910 = count__34202;
var G__34911 = (i__34203 + (1));
seq__34200 = G__34908;
chunk__34201 = G__34909;
count__34202 = G__34910;
i__34203 = G__34911;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__34200);
if(temp__5735__auto__){
var seq__34200__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34200__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__34200__$1);
var G__34913 = cljs.core.chunk_rest(seq__34200__$1);
var G__34914 = c__4550__auto__;
var G__34915 = cljs.core.count(c__4550__auto__);
var G__34916 = (0);
seq__34200 = G__34913;
chunk__34201 = G__34914;
count__34202 = G__34915;
i__34203 = G__34916;
continue;
} else {
var it = cljs.core.first(seq__34200__$1);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__34917 = cljs.core.next(seq__34200__$1);
var G__34918 = null;
var G__34919 = (0);
var G__34920 = (0);
seq__34200 = G__34917;
chunk__34201 = G__34918;
count__34202 = G__34919;
i__34203 = G__34920;
continue;
}
} else {
return null;
}
}
break;
}
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Coordinate = (function (x,y,__meta,__extmap,__hash){
this.x = x;
this.y = y;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4385__auto__,k__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
return this__4385__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4386__auto__,null);
});

shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4387__auto__,k34212,else__4388__auto__){
var self__ = this;
var this__4387__auto____$1 = this;
var G__34226 = k34212;
var G__34226__$1 = (((G__34226 instanceof cljs.core.Keyword))?G__34226.fqn:null);
switch (G__34226__$1) {
case "x":
return self__.x;

break;
case "y":
return self__.y;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k34212,else__4388__auto__);

}
});

shadow.dom.Coordinate.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4404__auto__,f__4405__auto__,init__4406__auto__){
var self__ = this;
var this__4404__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (this__4404__auto____$1){
return (function (ret__4407__auto__,p__34229){
var vec__34231 = p__34229;
var k__4408__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34231,(0),null);
var v__4409__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34231,(1),null);
return (f__4405__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4405__auto__.cljs$core$IFn$_invoke$arity$3(ret__4407__auto__,k__4408__auto__,v__4409__auto__) : f__4405__auto__.call(null,ret__4407__auto__,k__4408__auto__,v__4409__auto__));
});})(this__4404__auto____$1))
,init__4406__auto__,this__4404__auto____$1);
});

shadow.dom.Coordinate.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4399__auto__,writer__4400__auto__,opts__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
var pr_pair__4402__auto__ = ((function (this__4399__auto____$1){
return (function (keyval__4403__auto__){
return cljs.core.pr_sequential_writer(writer__4400__auto__,cljs.core.pr_writer,""," ","",opts__4401__auto__,keyval__4403__auto__);
});})(this__4399__auto____$1))
;
return cljs.core.pr_sequential_writer(writer__4400__auto__,pr_pair__4402__auto__,"#shadow.dom.Coordinate{",", ","}",opts__4401__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"x","x",2099068185),self__.x],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"y","y",-1757859776),self__.y],null))], null),self__.__extmap));
});

shadow.dom.Coordinate.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__34211){
var self__ = this;
var G__34211__$1 = this;
return (new cljs.core.RecordIter((0),G__34211__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"y","y",-1757859776)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
});

shadow.dom.Coordinate.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4383__auto__){
var self__ = this;
var this__4383__auto____$1 = this;
return self__.__meta;
});

shadow.dom.Coordinate.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4380__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,self__.__extmap,self__.__hash));
});

shadow.dom.Coordinate.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4389__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
});

shadow.dom.Coordinate.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4381__auto__){
var self__ = this;
var this__4381__auto____$1 = this;
var h__4243__auto__ = self__.__hash;
if((!((h__4243__auto__ == null)))){
return h__4243__auto__;
} else {
var h__4243__auto____$1 = (function (){var fexpr__34237 = ((function (h__4243__auto__,this__4381__auto____$1){
return (function (coll__4382__auto__){
return (145542109 ^ cljs.core.hash_unordered_coll(coll__4382__auto__));
});})(h__4243__auto__,this__4381__auto____$1))
;
return fexpr__34237(this__4381__auto____$1);
})();
self__.__hash = h__4243__auto____$1;

return h__4243__auto____$1;
}
});

shadow.dom.Coordinate.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this34213,other34215){
var self__ = this;
var this34213__$1 = this;
return (((!((other34215 == null)))) && ((this34213__$1.constructor === other34215.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this34213__$1.x,other34215.x)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this34213__$1.y,other34215.y)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this34213__$1.__extmap,other34215.__extmap)));
});

shadow.dom.Coordinate.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4394__auto__,k__4395__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"y","y",-1757859776),null,new cljs.core.Keyword(null,"x","x",2099068185),null], null), null),k__4395__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4394__auto____$1),self__.__meta),k__4395__auto__);
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4395__auto__)),null));
}
});

shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4392__auto__,k__4393__auto__,G__34211){
var self__ = this;
var this__4392__auto____$1 = this;
var pred__34247 = cljs.core.keyword_identical_QMARK_;
var expr__34248 = k__4393__auto__;
if(cljs.core.truth_((function (){var G__34250 = new cljs.core.Keyword(null,"x","x",2099068185);
var G__34251 = expr__34248;
return (pred__34247.cljs$core$IFn$_invoke$arity$2 ? pred__34247.cljs$core$IFn$_invoke$arity$2(G__34250,G__34251) : pred__34247.call(null,G__34250,G__34251));
})())){
return (new shadow.dom.Coordinate(G__34211,self__.y,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((function (){var G__34253 = new cljs.core.Keyword(null,"y","y",-1757859776);
var G__34254 = expr__34248;
return (pred__34247.cljs$core$IFn$_invoke$arity$2 ? pred__34247.cljs$core$IFn$_invoke$arity$2(G__34253,G__34254) : pred__34247.call(null,G__34253,G__34254));
})())){
return (new shadow.dom.Coordinate(self__.x,G__34211,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4393__auto__,G__34211),null));
}
}
});

shadow.dom.Coordinate.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4397__auto__){
var self__ = this;
var this__4397__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"x","x",2099068185),self__.x,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"y","y",-1757859776),self__.y,null))], null),self__.__extmap));
});

shadow.dom.Coordinate.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4384__auto__,G__34211){
var self__ = this;
var this__4384__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,G__34211,self__.__extmap,self__.__hash));
});

shadow.dom.Coordinate.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4390__auto__,entry__4391__auto__){
var self__ = this;
var this__4390__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4391__auto__)){
return this__4390__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(0)),cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4390__auto____$1,entry__4391__auto__);
}
});

shadow.dom.Coordinate.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"x","x",-555367584,null),new cljs.core.Symbol(null,"y","y",-117328249,null)], null);
});

shadow.dom.Coordinate.cljs$lang$type = true;

shadow.dom.Coordinate.cljs$lang$ctorPrSeq = (function (this__4428__auto__){
return (new cljs.core.List(null,"shadow.dom/Coordinate",null,(1),null));
});

shadow.dom.Coordinate.cljs$lang$ctorPrWriter = (function (this__4428__auto__,writer__4429__auto__){
return cljs.core._write(writer__4429__auto__,"shadow.dom/Coordinate");
});

/**
 * Positional factory function for shadow.dom/Coordinate.
 */
shadow.dom.__GT_Coordinate = (function shadow$dom$__GT_Coordinate(x,y){
return (new shadow.dom.Coordinate(x,y,null,null,null));
});

/**
 * Factory function for shadow.dom/Coordinate, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Coordinate = (function shadow$dom$map__GT_Coordinate(G__34219){
var extmap__4424__auto__ = (function (){var G__34266 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__34219,new cljs.core.Keyword(null,"x","x",2099068185),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776)], 0));
if(cljs.core.record_QMARK_(G__34219)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__34266);
} else {
return G__34266;
}
})();
return (new shadow.dom.Coordinate(new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(G__34219),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(G__34219),null,cljs.core.not_empty(extmap__4424__auto__),null));
});

shadow.dom.get_position = (function shadow$dom$get_position(el){
var pos = (function (){var G__34269 = shadow.dom.dom_node(el);
return goog.style.getPosition(G__34269);
})();
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_client_position = (function shadow$dom$get_client_position(el){
var pos = (function (){var G__34273 = shadow.dom.dom_node(el);
return goog.style.getClientPosition(G__34273);
})();
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_page_offset = (function shadow$dom$get_page_offset(el){
var pos = (function (){var G__34275 = shadow.dom.dom_node(el);
return goog.style.getPageOffset(G__34275);
})();
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Size = (function (w,h,__meta,__extmap,__hash){
this.w = w;
this.h = h;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4385__auto__,k__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
return this__4385__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4386__auto__,null);
});

shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4387__auto__,k34279,else__4388__auto__){
var self__ = this;
var this__4387__auto____$1 = this;
var G__34290 = k34279;
var G__34290__$1 = (((G__34290 instanceof cljs.core.Keyword))?G__34290.fqn:null);
switch (G__34290__$1) {
case "w":
return self__.w;

break;
case "h":
return self__.h;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k34279,else__4388__auto__);

}
});

shadow.dom.Size.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4404__auto__,f__4405__auto__,init__4406__auto__){
var self__ = this;
var this__4404__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (this__4404__auto____$1){
return (function (ret__4407__auto__,p__34293){
var vec__34296 = p__34293;
var k__4408__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34296,(0),null);
var v__4409__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34296,(1),null);
return (f__4405__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4405__auto__.cljs$core$IFn$_invoke$arity$3(ret__4407__auto__,k__4408__auto__,v__4409__auto__) : f__4405__auto__.call(null,ret__4407__auto__,k__4408__auto__,v__4409__auto__));
});})(this__4404__auto____$1))
,init__4406__auto__,this__4404__auto____$1);
});

shadow.dom.Size.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4399__auto__,writer__4400__auto__,opts__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
var pr_pair__4402__auto__ = ((function (this__4399__auto____$1){
return (function (keyval__4403__auto__){
return cljs.core.pr_sequential_writer(writer__4400__auto__,cljs.core.pr_writer,""," ","",opts__4401__auto__,keyval__4403__auto__);
});})(this__4399__auto____$1))
;
return cljs.core.pr_sequential_writer(writer__4400__auto__,pr_pair__4402__auto__,"#shadow.dom.Size{",", ","}",opts__4401__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"w","w",354169001),self__.w],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"h","h",1109658740),self__.h],null))], null),self__.__extmap));
});

shadow.dom.Size.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__34278){
var self__ = this;
var G__34278__$1 = this;
return (new cljs.core.RecordIter((0),G__34278__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"w","w",354169001),new cljs.core.Keyword(null,"h","h",1109658740)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
});

shadow.dom.Size.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4383__auto__){
var self__ = this;
var this__4383__auto____$1 = this;
return self__.__meta;
});

shadow.dom.Size.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4380__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,self__.__extmap,self__.__hash));
});

shadow.dom.Size.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4389__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
});

shadow.dom.Size.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4381__auto__){
var self__ = this;
var this__4381__auto____$1 = this;
var h__4243__auto__ = self__.__hash;
if((!((h__4243__auto__ == null)))){
return h__4243__auto__;
} else {
var h__4243__auto____$1 = (function (){var fexpr__34311 = ((function (h__4243__auto__,this__4381__auto____$1){
return (function (coll__4382__auto__){
return (-1228019642 ^ cljs.core.hash_unordered_coll(coll__4382__auto__));
});})(h__4243__auto__,this__4381__auto____$1))
;
return fexpr__34311(this__4381__auto____$1);
})();
self__.__hash = h__4243__auto____$1;

return h__4243__auto____$1;
}
});

shadow.dom.Size.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this34280,other34281){
var self__ = this;
var this34280__$1 = this;
return (((!((other34281 == null)))) && ((this34280__$1.constructor === other34281.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this34280__$1.w,other34281.w)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this34280__$1.h,other34281.h)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this34280__$1.__extmap,other34281.__extmap)));
});

shadow.dom.Size.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4394__auto__,k__4395__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"w","w",354169001),null,new cljs.core.Keyword(null,"h","h",1109658740),null], null), null),k__4395__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4394__auto____$1),self__.__meta),k__4395__auto__);
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4395__auto__)),null));
}
});

shadow.dom.Size.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4392__auto__,k__4393__auto__,G__34278){
var self__ = this;
var this__4392__auto____$1 = this;
var pred__34316 = cljs.core.keyword_identical_QMARK_;
var expr__34317 = k__4393__auto__;
if(cljs.core.truth_((function (){var G__34320 = new cljs.core.Keyword(null,"w","w",354169001);
var G__34321 = expr__34317;
return (pred__34316.cljs$core$IFn$_invoke$arity$2 ? pred__34316.cljs$core$IFn$_invoke$arity$2(G__34320,G__34321) : pred__34316.call(null,G__34320,G__34321));
})())){
return (new shadow.dom.Size(G__34278,self__.h,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((function (){var G__34323 = new cljs.core.Keyword(null,"h","h",1109658740);
var G__34324 = expr__34317;
return (pred__34316.cljs$core$IFn$_invoke$arity$2 ? pred__34316.cljs$core$IFn$_invoke$arity$2(G__34323,G__34324) : pred__34316.call(null,G__34323,G__34324));
})())){
return (new shadow.dom.Size(self__.w,G__34278,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4393__auto__,G__34278),null));
}
}
});

shadow.dom.Size.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4397__auto__){
var self__ = this;
var this__4397__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"w","w",354169001),self__.w,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"h","h",1109658740),self__.h,null))], null),self__.__extmap));
});

shadow.dom.Size.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4384__auto__,G__34278){
var self__ = this;
var this__4384__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,G__34278,self__.__extmap,self__.__hash));
});

shadow.dom.Size.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4390__auto__,entry__4391__auto__){
var self__ = this;
var this__4390__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4391__auto__)){
return this__4390__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(0)),cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4390__auto____$1,entry__4391__auto__);
}
});

shadow.dom.Size.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"w","w",1994700528,null),new cljs.core.Symbol(null,"h","h",-1544777029,null)], null);
});

shadow.dom.Size.cljs$lang$type = true;

shadow.dom.Size.cljs$lang$ctorPrSeq = (function (this__4428__auto__){
return (new cljs.core.List(null,"shadow.dom/Size",null,(1),null));
});

shadow.dom.Size.cljs$lang$ctorPrWriter = (function (this__4428__auto__,writer__4429__auto__){
return cljs.core._write(writer__4429__auto__,"shadow.dom/Size");
});

/**
 * Positional factory function for shadow.dom/Size.
 */
shadow.dom.__GT_Size = (function shadow$dom$__GT_Size(w,h){
return (new shadow.dom.Size(w,h,null,null,null));
});

/**
 * Factory function for shadow.dom/Size, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Size = (function shadow$dom$map__GT_Size(G__34283){
var extmap__4424__auto__ = (function (){var G__34325 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__34283,new cljs.core.Keyword(null,"w","w",354169001),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"h","h",1109658740)], 0));
if(cljs.core.record_QMARK_(G__34283)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__34325);
} else {
return G__34325;
}
})();
return (new shadow.dom.Size(new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(G__34283),new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(G__34283),null,cljs.core.not_empty(extmap__4424__auto__),null));
});

shadow.dom.size__GT_clj = (function shadow$dom$size__GT_clj(size){
return (new shadow.dom.Size(size.width,size.height,null,null,null));
});
shadow.dom.get_size = (function shadow$dom$get_size(el){
return shadow.dom.size__GT_clj((function (){var G__34329 = shadow.dom.dom_node(el);
return goog.style.getSize(G__34329);
})());
});
shadow.dom.get_height = (function shadow$dom$get_height(el){
return new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(shadow.dom.get_size(el));
});
shadow.dom.get_viewport_size = (function shadow$dom$get_viewport_size(){
return shadow.dom.size__GT_clj(goog.dom.getViewportSize());
});
shadow.dom.first_child = (function shadow$dom$first_child(el){
return (shadow.dom.dom_node(el).children[(0)]);
});
shadow.dom.select_option_values = (function shadow$dom$select_option_values(el){
var native$ = shadow.dom.dom_node(el);
var opts = (native$["options"]);
var a__4604__auto__ = opts;
var l__4605__auto__ = a__4604__auto__.length;
var i = (0);
var ret = cljs.core.PersistentVector.EMPTY;
while(true){
if((i < l__4605__auto__)){
var G__34981 = (i + (1));
var G__34982 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,(opts[i]["value"]));
i = G__34981;
ret = G__34982;
continue;
} else {
return ret;
}
break;
}
});
shadow.dom.build_url = (function shadow$dom$build_url(path,query_params){
if(cljs.core.empty_QMARK_(query_params)){
return path;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(path),"?",cljs.core.str.cljs$core$IFn$_invoke$arity$1(clojure.string.join.cljs$core$IFn$_invoke$arity$2("&",cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__34337){
var vec__34338 = p__34337;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34338,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34338,(1),null);
return [cljs.core.name(k),"=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(encodeURIComponent(cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)))].join('');
}),query_params)))].join('');
}
});
shadow.dom.redirect = (function shadow$dom$redirect(var_args){
var G__34342 = arguments.length;
switch (G__34342) {
case 1:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1 = (function (path){
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2(path,cljs.core.PersistentArrayMap.EMPTY);
});

shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2 = (function (path,query_params){
return (document["location"]["href"] = shadow.dom.build_url(path,query_params));
});

shadow.dom.redirect.cljs$lang$maxFixedArity = 2;

shadow.dom.reload_BANG_ = (function shadow$dom$reload_BANG_(){
return document.location.href = document.location.href;
});
shadow.dom.tag_name = (function shadow$dom$tag_name(el){
var dom = shadow.dom.dom_node(el);
return dom.tagName;
});
shadow.dom.insert_after = (function shadow$dom$insert_after(ref,new$){
var new_node = shadow.dom.dom_node(new$);
var G__34350_34985 = new_node;
var G__34351_34986 = shadow.dom.dom_node(ref);
goog.dom.insertSiblingAfter(G__34350_34985,G__34351_34986);

return new_node;
});
shadow.dom.insert_before = (function shadow$dom$insert_before(ref,new$){
var new_node = shadow.dom.dom_node(new$);
var G__34356_34987 = new_node;
var G__34357_34988 = shadow.dom.dom_node(ref);
goog.dom.insertSiblingBefore(G__34356_34987,G__34357_34988);

return new_node;
});
shadow.dom.insert_first = (function shadow$dom$insert_first(ref,new$){
var temp__5733__auto__ = shadow.dom.dom_node(ref).firstChild;
if(cljs.core.truth_(temp__5733__auto__)){
var child = temp__5733__auto__;
return shadow.dom.insert_before(child,new$);
} else {
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2(ref,new$);
}
});
shadow.dom.index_of = (function shadow$dom$index_of(el){
var el__$1 = shadow.dom.dom_node(el);
var i = (0);
while(true){
var ps = el__$1.previousSibling;
if((ps == null)){
return i;
} else {
var G__34989 = ps;
var G__34990 = (i + (1));
el__$1 = G__34989;
i = G__34990;
continue;
}
break;
}
});
shadow.dom.get_parent = (function shadow$dom$get_parent(el){
var G__34365 = shadow.dom.dom_node(el);
return goog.dom.getParentElement(G__34365);
});
shadow.dom.parents = (function shadow$dom$parents(el){
var parent = shadow.dom.get_parent(el);
if(cljs.core.truth_(parent)){
return cljs.core.cons(parent,(new cljs.core.LazySeq(null,((function (parent){
return (function (){
return (shadow.dom.parents.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.parents.cljs$core$IFn$_invoke$arity$1(parent) : shadow.dom.parents.call(null,parent));
});})(parent))
,null,null)));
} else {
return null;
}
});
shadow.dom.matches = (function shadow$dom$matches(el,sel){
return shadow.dom.dom_node(el).matches(sel);
});
shadow.dom.get_next_sibling = (function shadow$dom$get_next_sibling(el){
var G__34371 = shadow.dom.dom_node(el);
return goog.dom.getNextElementSibling(G__34371);
});
shadow.dom.get_previous_sibling = (function shadow$dom$get_previous_sibling(el){
var G__34374 = shadow.dom.dom_node(el);
return goog.dom.getPreviousElementSibling(G__34374);
});
shadow.dom.xmlns = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, ["svg","http://www.w3.org/2000/svg","xlink","http://www.w3.org/1999/xlink"], null));
shadow.dom.create_svg_node = (function shadow$dom$create_svg_node(tag_def,props){
var vec__34377 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34377,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34377,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34377,(2),null);
var el = document.createElementNS("http://www.w3.org/2000/svg",tag_name);
if(cljs.core.truth_(tag_id)){
el.setAttribute("id",tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
el.setAttribute("class",shadow.dom.merge_class_string(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(props),tag_classes));
} else {
}

var seq__34383_34994 = cljs.core.seq(props);
var chunk__34384_34995 = null;
var count__34385_34996 = (0);
var i__34386_34997 = (0);
while(true){
if((i__34386_34997 < count__34385_34996)){
var vec__34402_34998 = chunk__34384_34995.cljs$core$IIndexed$_nth$arity$2(null,i__34386_34997);
var k_34999 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34402_34998,(0),null);
var v_35000 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34402_34998,(1),null);
el.setAttributeNS((function (){var temp__5735__auto__ = cljs.core.namespace(k_34999);
if(cljs.core.truth_(temp__5735__auto__)){
var ns = temp__5735__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_34999),v_35000);


var G__35001 = seq__34383_34994;
var G__35002 = chunk__34384_34995;
var G__35003 = count__34385_34996;
var G__35004 = (i__34386_34997 + (1));
seq__34383_34994 = G__35001;
chunk__34384_34995 = G__35002;
count__34385_34996 = G__35003;
i__34386_34997 = G__35004;
continue;
} else {
var temp__5735__auto___35005 = cljs.core.seq(seq__34383_34994);
if(temp__5735__auto___35005){
var seq__34383_35010__$1 = temp__5735__auto___35005;
if(cljs.core.chunked_seq_QMARK_(seq__34383_35010__$1)){
var c__4550__auto___35011 = cljs.core.chunk_first(seq__34383_35010__$1);
var G__35012 = cljs.core.chunk_rest(seq__34383_35010__$1);
var G__35013 = c__4550__auto___35011;
var G__35014 = cljs.core.count(c__4550__auto___35011);
var G__35015 = (0);
seq__34383_34994 = G__35012;
chunk__34384_34995 = G__35013;
count__34385_34996 = G__35014;
i__34386_34997 = G__35015;
continue;
} else {
var vec__34405_35016 = cljs.core.first(seq__34383_35010__$1);
var k_35017 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34405_35016,(0),null);
var v_35018 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34405_35016,(1),null);
el.setAttributeNS((function (){var temp__5735__auto____$1 = cljs.core.namespace(k_35017);
if(cljs.core.truth_(temp__5735__auto____$1)){
var ns = temp__5735__auto____$1;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_35017),v_35018);


var G__35020 = cljs.core.next(seq__34383_35010__$1);
var G__35021 = null;
var G__35022 = (0);
var G__35023 = (0);
seq__34383_34994 = G__35020;
chunk__34384_34995 = G__35021;
count__34385_34996 = G__35022;
i__34386_34997 = G__35023;
continue;
}
} else {
}
}
break;
}

return el;
});
shadow.dom.svg_node = (function shadow$dom$svg_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$SVGElement$))))?true:false):false)){
return el.shadow$dom$SVGElement$_to_svg$arity$1(null);
} else {
return el;

}
}
});
shadow.dom.make_svg_node = (function shadow$dom$make_svg_node(structure){
var vec__34414 = shadow.dom.destructure_node(shadow.dom.create_svg_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34414,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34414,(1),null);
var seq__34417_35024 = cljs.core.seq(node_children);
var chunk__34419_35025 = null;
var count__34420_35026 = (0);
var i__34421_35027 = (0);
while(true){
if((i__34421_35027 < count__34420_35026)){
var child_struct_35028 = chunk__34419_35025.cljs$core$IIndexed$_nth$arity$2(null,i__34421_35027);
if((!((child_struct_35028 == null)))){
if(typeof child_struct_35028 === 'string'){
var text_35030 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_35030),child_struct_35028].join(''));
} else {
var children_35031 = shadow.dom.svg_node(child_struct_35028);
if(cljs.core.seq_QMARK_(children_35031)){
var seq__34503_35032 = cljs.core.seq(children_35031);
var chunk__34505_35033 = null;
var count__34506_35034 = (0);
var i__34507_35035 = (0);
while(true){
if((i__34507_35035 < count__34506_35034)){
var child_35036 = chunk__34505_35033.cljs$core$IIndexed$_nth$arity$2(null,i__34507_35035);
if(cljs.core.truth_(child_35036)){
node.appendChild(child_35036);


var G__35037 = seq__34503_35032;
var G__35038 = chunk__34505_35033;
var G__35039 = count__34506_35034;
var G__35040 = (i__34507_35035 + (1));
seq__34503_35032 = G__35037;
chunk__34505_35033 = G__35038;
count__34506_35034 = G__35039;
i__34507_35035 = G__35040;
continue;
} else {
var G__35041 = seq__34503_35032;
var G__35042 = chunk__34505_35033;
var G__35043 = count__34506_35034;
var G__35044 = (i__34507_35035 + (1));
seq__34503_35032 = G__35041;
chunk__34505_35033 = G__35042;
count__34506_35034 = G__35043;
i__34507_35035 = G__35044;
continue;
}
} else {
var temp__5735__auto___35046 = cljs.core.seq(seq__34503_35032);
if(temp__5735__auto___35046){
var seq__34503_35050__$1 = temp__5735__auto___35046;
if(cljs.core.chunked_seq_QMARK_(seq__34503_35050__$1)){
var c__4550__auto___35051 = cljs.core.chunk_first(seq__34503_35050__$1);
var G__35052 = cljs.core.chunk_rest(seq__34503_35050__$1);
var G__35053 = c__4550__auto___35051;
var G__35054 = cljs.core.count(c__4550__auto___35051);
var G__35055 = (0);
seq__34503_35032 = G__35052;
chunk__34505_35033 = G__35053;
count__34506_35034 = G__35054;
i__34507_35035 = G__35055;
continue;
} else {
var child_35056 = cljs.core.first(seq__34503_35050__$1);
if(cljs.core.truth_(child_35056)){
node.appendChild(child_35056);


var G__35057 = cljs.core.next(seq__34503_35050__$1);
var G__35058 = null;
var G__35059 = (0);
var G__35060 = (0);
seq__34503_35032 = G__35057;
chunk__34505_35033 = G__35058;
count__34506_35034 = G__35059;
i__34507_35035 = G__35060;
continue;
} else {
var G__35061 = cljs.core.next(seq__34503_35050__$1);
var G__35062 = null;
var G__35063 = (0);
var G__35064 = (0);
seq__34503_35032 = G__35061;
chunk__34505_35033 = G__35062;
count__34506_35034 = G__35063;
i__34507_35035 = G__35064;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_35031);
}
}


var G__35065 = seq__34417_35024;
var G__35066 = chunk__34419_35025;
var G__35067 = count__34420_35026;
var G__35068 = (i__34421_35027 + (1));
seq__34417_35024 = G__35065;
chunk__34419_35025 = G__35066;
count__34420_35026 = G__35067;
i__34421_35027 = G__35068;
continue;
} else {
var G__35069 = seq__34417_35024;
var G__35070 = chunk__34419_35025;
var G__35071 = count__34420_35026;
var G__35072 = (i__34421_35027 + (1));
seq__34417_35024 = G__35069;
chunk__34419_35025 = G__35070;
count__34420_35026 = G__35071;
i__34421_35027 = G__35072;
continue;
}
} else {
var temp__5735__auto___35073 = cljs.core.seq(seq__34417_35024);
if(temp__5735__auto___35073){
var seq__34417_35074__$1 = temp__5735__auto___35073;
if(cljs.core.chunked_seq_QMARK_(seq__34417_35074__$1)){
var c__4550__auto___35075 = cljs.core.chunk_first(seq__34417_35074__$1);
var G__35076 = cljs.core.chunk_rest(seq__34417_35074__$1);
var G__35077 = c__4550__auto___35075;
var G__35078 = cljs.core.count(c__4550__auto___35075);
var G__35079 = (0);
seq__34417_35024 = G__35076;
chunk__34419_35025 = G__35077;
count__34420_35026 = G__35078;
i__34421_35027 = G__35079;
continue;
} else {
var child_struct_35080 = cljs.core.first(seq__34417_35074__$1);
if((!((child_struct_35080 == null)))){
if(typeof child_struct_35080 === 'string'){
var text_35081 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_35081),child_struct_35080].join(''));
} else {
var children_35082 = shadow.dom.svg_node(child_struct_35080);
if(cljs.core.seq_QMARK_(children_35082)){
var seq__34519_35083 = cljs.core.seq(children_35082);
var chunk__34521_35084 = null;
var count__34522_35085 = (0);
var i__34523_35086 = (0);
while(true){
if((i__34523_35086 < count__34522_35085)){
var child_35087 = chunk__34521_35084.cljs$core$IIndexed$_nth$arity$2(null,i__34523_35086);
if(cljs.core.truth_(child_35087)){
node.appendChild(child_35087);


var G__35089 = seq__34519_35083;
var G__35090 = chunk__34521_35084;
var G__35091 = count__34522_35085;
var G__35092 = (i__34523_35086 + (1));
seq__34519_35083 = G__35089;
chunk__34521_35084 = G__35090;
count__34522_35085 = G__35091;
i__34523_35086 = G__35092;
continue;
} else {
var G__35093 = seq__34519_35083;
var G__35094 = chunk__34521_35084;
var G__35095 = count__34522_35085;
var G__35096 = (i__34523_35086 + (1));
seq__34519_35083 = G__35093;
chunk__34521_35084 = G__35094;
count__34522_35085 = G__35095;
i__34523_35086 = G__35096;
continue;
}
} else {
var temp__5735__auto___35097__$1 = cljs.core.seq(seq__34519_35083);
if(temp__5735__auto___35097__$1){
var seq__34519_35098__$1 = temp__5735__auto___35097__$1;
if(cljs.core.chunked_seq_QMARK_(seq__34519_35098__$1)){
var c__4550__auto___35099 = cljs.core.chunk_first(seq__34519_35098__$1);
var G__35100 = cljs.core.chunk_rest(seq__34519_35098__$1);
var G__35101 = c__4550__auto___35099;
var G__35102 = cljs.core.count(c__4550__auto___35099);
var G__35103 = (0);
seq__34519_35083 = G__35100;
chunk__34521_35084 = G__35101;
count__34522_35085 = G__35102;
i__34523_35086 = G__35103;
continue;
} else {
var child_35104 = cljs.core.first(seq__34519_35098__$1);
if(cljs.core.truth_(child_35104)){
node.appendChild(child_35104);


var G__35105 = cljs.core.next(seq__34519_35098__$1);
var G__35106 = null;
var G__35107 = (0);
var G__35108 = (0);
seq__34519_35083 = G__35105;
chunk__34521_35084 = G__35106;
count__34522_35085 = G__35107;
i__34523_35086 = G__35108;
continue;
} else {
var G__35109 = cljs.core.next(seq__34519_35098__$1);
var G__35110 = null;
var G__35111 = (0);
var G__35112 = (0);
seq__34519_35083 = G__35109;
chunk__34521_35084 = G__35110;
count__34522_35085 = G__35111;
i__34523_35086 = G__35112;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_35082);
}
}


var G__35114 = cljs.core.next(seq__34417_35074__$1);
var G__35115 = null;
var G__35116 = (0);
var G__35117 = (0);
seq__34417_35024 = G__35114;
chunk__34419_35025 = G__35115;
count__34420_35026 = G__35116;
i__34421_35027 = G__35117;
continue;
} else {
var G__35118 = cljs.core.next(seq__34417_35074__$1);
var G__35119 = null;
var G__35120 = (0);
var G__35121 = (0);
seq__34417_35024 = G__35118;
chunk__34419_35025 = G__35119;
count__34420_35026 = G__35120;
i__34421_35027 = G__35121;
continue;
}
}
} else {
}
}
break;
}

return node;
});
goog.object.set(shadow.dom.SVGElement,"string",true);

var G__34539_35122 = shadow.dom._to_svg;
var G__34540_35123 = "string";
var G__34541_35124 = ((function (G__34539_35122,G__34540_35123){
return (function (this$){
if((this$ instanceof cljs.core.Keyword)){
return shadow.dom.make_svg_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$], null));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("strings cannot be in svgs",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"this","this",-611633625),this$], null));
}
});})(G__34539_35122,G__34540_35123))
;
goog.object.set(G__34539_35122,G__34540_35123,G__34541_35124);

cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_svg_node(this$__$1);
});

cljs.core.LazySeq.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.LazySeq.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_svg,this$__$1);
});

goog.object.set(shadow.dom.SVGElement,"null",true);

var G__34546_35125 = shadow.dom._to_svg;
var G__34547_35126 = "null";
var G__34548_35127 = ((function (G__34546_35125,G__34547_35126){
return (function (_){
return null;
});})(G__34546_35125,G__34547_35126))
;
goog.object.set(G__34546_35125,G__34547_35126,G__34548_35127);
shadow.dom.svg = (function shadow$dom$svg(var_args){
var args__4736__auto__ = [];
var len__4730__auto___35128 = arguments.length;
var i__4731__auto___35129 = (0);
while(true){
if((i__4731__auto___35129 < len__4730__auto___35128)){
args__4736__auto__.push((arguments[i__4731__auto___35129]));

var G__35130 = (i__4731__auto___35129 + (1));
i__4731__auto___35129 = G__35130;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic = (function (attrs,children){
return shadow.dom._to_svg(cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),attrs], null),children)));
});

shadow.dom.svg.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
shadow.dom.svg.cljs$lang$applyTo = (function (seq34553){
var G__34554 = cljs.core.first(seq34553);
var seq34553__$1 = cljs.core.next(seq34553);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__34554,seq34553__$1);
});

/**
 * returns a channel for events on el
 * transform-fn should be a (fn [e el] some-val) where some-val will be put on the chan
 * once-or-cleanup handles the removal of the event handler
 * - true: remove after one event
 * - false: never removed
 * - chan: remove on msg/close
 */
shadow.dom.event_chan = (function shadow$dom$event_chan(var_args){
var G__34565 = arguments.length;
switch (G__34565) {
case 2:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2 = (function (el,event){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,null,false);
});

shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3 = (function (el,event,xf){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,xf,false);
});

shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4 = (function (el,event,xf,once_or_cleanup){
var buf = cljs.core.async.sliding_buffer((1));
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2(buf,xf);
var event_fn = ((function (buf,chan){
return (function shadow$dom$event_fn(e){
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,e);

if(once_or_cleanup === true){
shadow.dom.remove_event_handler(el,event,shadow$dom$event_fn);

return cljs.core.async.close_BANG_(chan);
} else {
return null;
}
});})(buf,chan))
;
var G__34571_35140 = shadow.dom.dom_node(el);
var G__34572_35141 = cljs.core.name(event);
var G__34573_35142 = event_fn;
(shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__34571_35140,G__34572_35141,G__34573_35142) : shadow.dom.dom_listen.call(null,G__34571_35140,G__34572_35141,G__34573_35142));

if(cljs.core.truth_((function (){var and__4120__auto__ = once_or_cleanup;
if(cljs.core.truth_(and__4120__auto__)){
return (!(once_or_cleanup === true));
} else {
return and__4120__auto__;
}
})())){
var c__29782__auto___35143 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__29782__auto___35143,buf,chan,event_fn){
return (function (){
var f__29783__auto__ = (function (){var switch__28688__auto__ = ((function (c__29782__auto___35143,buf,chan,event_fn){
return (function (state_34580){
var state_val_34581 = (state_34580[(1)]);
if((state_val_34581 === (1))){
var state_34580__$1 = state_34580;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34580__$1,(2),once_or_cleanup);
} else {
if((state_val_34581 === (2))){
var inst_34577 = (state_34580[(2)]);
var inst_34578 = shadow.dom.remove_event_handler(el,event,event_fn);
var state_34580__$1 = (function (){var statearr_34586 = state_34580;
(statearr_34586[(7)] = inst_34577);

return statearr_34586;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_34580__$1,inst_34578);
} else {
return null;
}
}
});})(c__29782__auto___35143,buf,chan,event_fn))
;
return ((function (switch__28688__auto__,c__29782__auto___35143,buf,chan,event_fn){
return (function() {
var shadow$dom$state_machine__28689__auto__ = null;
var shadow$dom$state_machine__28689__auto____0 = (function (){
var statearr_34588 = [null,null,null,null,null,null,null,null];
(statearr_34588[(0)] = shadow$dom$state_machine__28689__auto__);

(statearr_34588[(1)] = (1));

return statearr_34588;
});
var shadow$dom$state_machine__28689__auto____1 = (function (state_34580){
while(true){
var ret_value__28690__auto__ = (function (){try{while(true){
var result__28691__auto__ = switch__28688__auto__(state_34580);
if(cljs.core.keyword_identical_QMARK_(result__28691__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__28691__auto__;
}
break;
}
}catch (e34589){if((e34589 instanceof Object)){
var ex__28692__auto__ = e34589;
var statearr_34590_35151 = state_34580;
(statearr_34590_35151[(5)] = ex__28692__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_34580);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34589;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__28690__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35152 = state_34580;
state_34580 = G__35152;
continue;
} else {
return ret_value__28690__auto__;
}
break;
}
});
shadow$dom$state_machine__28689__auto__ = function(state_34580){
switch(arguments.length){
case 0:
return shadow$dom$state_machine__28689__auto____0.call(this);
case 1:
return shadow$dom$state_machine__28689__auto____1.call(this,state_34580);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
shadow$dom$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$0 = shadow$dom$state_machine__28689__auto____0;
shadow$dom$state_machine__28689__auto__.cljs$core$IFn$_invoke$arity$1 = shadow$dom$state_machine__28689__auto____1;
return shadow$dom$state_machine__28689__auto__;
})()
;})(switch__28688__auto__,c__29782__auto___35143,buf,chan,event_fn))
})();
var state__29784__auto__ = (function (){var statearr_34593 = (f__29783__auto__.cljs$core$IFn$_invoke$arity$0 ? f__29783__auto__.cljs$core$IFn$_invoke$arity$0() : f__29783__auto__.call(null));
(statearr_34593[(6)] = c__29782__auto___35143);

return statearr_34593;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__29784__auto__);
});})(c__29782__auto___35143,buf,chan,event_fn))
);

} else {
}

return chan;
});

shadow.dom.event_chan.cljs$lang$maxFixedArity = 4;


//# sourceMappingURL=shadow.dom.js.map
