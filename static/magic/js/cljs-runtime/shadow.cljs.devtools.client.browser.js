goog.provide('shadow.cljs.devtools.client.browser');
goog.require('cljs.core');
goog.require('cljs.reader');
goog.require('clojure.string');
goog.require('goog.dom');
goog.require('goog.userAgent.product');
goog.require('goog.Uri');
goog.require('goog.net.XhrIo');
goog.require('shadow.cljs.devtools.client.env');
goog.require('shadow.cljs.devtools.client.console');
goog.require('shadow.cljs.devtools.client.hud');
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.active_modules_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.active_modules_ref = cljs.core.volatile_BANG_(cljs.core.PersistentHashSet.EMPTY);
}
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.repl_ns_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.repl_ns_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
shadow.cljs.devtools.client.browser.module_loaded = (function shadow$cljs$devtools$client$browser$module_loaded(name){
return shadow.cljs.devtools.client.browser.active_modules_ref.cljs$core$IVolatile$_vreset_BANG_$arity$2(null,cljs.core.conj.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.browser.active_modules_ref.cljs$core$IDeref$_deref$arity$1(null),cljs.core.keyword.cljs$core$IFn$_invoke$arity$1(name)));
});
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.socket_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.socket_ref = cljs.core.volatile_BANG_(null);
}
shadow.cljs.devtools.client.browser.devtools_msg = (function shadow$cljs$devtools$client$browser$devtools_msg(var_args){
var args__4736__auto__ = [];
var len__4730__auto___36119 = arguments.length;
var i__4731__auto___36120 = (0);
while(true){
if((i__4731__auto___36120 < len__4730__auto___36119)){
args__4736__auto__.push((arguments[i__4731__auto___36120]));

var G__36121 = (i__4731__auto___36120 + (1));
i__4731__auto___36120 = G__36121;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic = (function (msg,args){
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [["%cshadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join(''),"color: blue;"], null),args)));
});

shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$applyTo = (function (seq35961){
var G__35962 = cljs.core.first(seq35961);
var seq35961__$1 = cljs.core.next(seq35961);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__35962,seq35961__$1);
});

shadow.cljs.devtools.client.browser.ws_msg = (function shadow$cljs$devtools$client$browser$ws_msg(msg){
var temp__5733__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5733__auto__)){
var s = temp__5733__auto__;
return s.send(cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0)));
} else {
return console.warn("WEBSOCKET NOT CONNECTED",cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0)));
}
});
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.scripts_to_load !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.scripts_to_load = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentVector.EMPTY);
}
shadow.cljs.devtools.client.browser.loaded_QMARK_ = goog.isProvided_;
shadow.cljs.devtools.client.browser.goog_is_loaded_QMARK_ = (function shadow$cljs$devtools$client$browser$goog_is_loaded_QMARK_(name){
return $CLJS.SHADOW_ENV.isLoaded(name);
});
shadow.cljs.devtools.client.browser.goog_base_rc = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("shadow.build.classpath","resource","shadow.build.classpath/resource",-879517823),"goog/base.js"], null);
shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_ = (function shadow$cljs$devtools$client$browser$src_is_loaded_QMARK_(p__35966){
var map__35967 = p__35966;
var map__35967__$1 = (((((!((map__35967 == null))))?(((((map__35967.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35967.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35967):map__35967);
var src = map__35967__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35967__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35967__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var or__4131__auto__ = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.browser.goog_base_rc,resource_id);
if(or__4131__auto__){
return or__4131__auto__;
} else {
return shadow.cljs.devtools.client.browser.goog_is_loaded_QMARK_(output_name);
}
});
shadow.cljs.devtools.client.browser.module_is_active_QMARK_ = (function shadow$cljs$devtools$client$browser$module_is_active_QMARK_(module){
return cljs.core.contains_QMARK_(cljs.core.deref(shadow.cljs.devtools.client.browser.active_modules_ref),module);
});
shadow.cljs.devtools.client.browser.script_eval = (function shadow$cljs$devtools$client$browser$script_eval(code){
return goog.globalEval(code);
});
shadow.cljs.devtools.client.browser.do_js_load = (function shadow$cljs$devtools$client$browser$do_js_load(sources){
var seq__35969 = cljs.core.seq(sources);
var chunk__35970 = null;
var count__35971 = (0);
var i__35972 = (0);
while(true){
if((i__35972 < count__35971)){
var map__35982 = chunk__35970.cljs$core$IIndexed$_nth$arity$2(null,i__35972);
var map__35982__$1 = (((((!((map__35982 == null))))?(((((map__35982.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35982.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35982):map__35982);
var src = map__35982__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35982__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35982__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35982__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35982__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''));
}catch (e35984){var e_36122 = e35984;
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_36122);

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_36122.message)].join('')));
}

var G__36123 = seq__35969;
var G__36124 = chunk__35970;
var G__36125 = count__35971;
var G__36126 = (i__35972 + (1));
seq__35969 = G__36123;
chunk__35970 = G__36124;
count__35971 = G__36125;
i__35972 = G__36126;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__35969);
if(temp__5735__auto__){
var seq__35969__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__35969__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__35969__$1);
var G__36127 = cljs.core.chunk_rest(seq__35969__$1);
var G__36128 = c__4550__auto__;
var G__36129 = cljs.core.count(c__4550__auto__);
var G__36130 = (0);
seq__35969 = G__36127;
chunk__35970 = G__36128;
count__35971 = G__36129;
i__35972 = G__36130;
continue;
} else {
var map__35986 = cljs.core.first(seq__35969__$1);
var map__35986__$1 = (((((!((map__35986 == null))))?(((((map__35986.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35986.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35986):map__35986);
var src = map__35986__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35986__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35986__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35986__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35986__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''));
}catch (e35988){var e_36131 = e35988;
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_36131);

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_36131.message)].join('')));
}

var G__36132 = cljs.core.next(seq__35969__$1);
var G__36133 = null;
var G__36134 = (0);
var G__36135 = (0);
seq__35969 = G__36132;
chunk__35970 = G__36133;
count__35971 = G__36134;
i__35972 = G__36135;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.do_js_reload = (function shadow$cljs$devtools$client$browser$do_js_reload(msg,sources,complete_fn,failure_fn){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(msg,new cljs.core.Keyword(null,"log-missing-fn","log-missing-fn",732676765),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["can't find fn ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"log-call-async","log-call-async",183826192),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call async ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),new cljs.core.Keyword(null,"log-call","log-call",412404391),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
})], 0)),(function (){
return shadow.cljs.devtools.client.browser.do_js_load(sources);
}),complete_fn,failure_fn);
});
/**
 * when (require '["some-str" :as x]) is done at the REPL we need to manually call the shadow.js.require for it
 * since the file only adds the shadow$provide. only need to do this for shadow-js.
 */
shadow.cljs.devtools.client.browser.do_js_requires = (function shadow$cljs$devtools$client$browser$do_js_requires(js_requires){
var seq__35989 = cljs.core.seq(js_requires);
var chunk__35990 = null;
var count__35991 = (0);
var i__35992 = (0);
while(true){
if((i__35992 < count__35991)){
var js_ns = chunk__35990.cljs$core$IIndexed$_nth$arity$2(null,i__35992);
var require_str_36136 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_36136);


var G__36137 = seq__35989;
var G__36138 = chunk__35990;
var G__36139 = count__35991;
var G__36140 = (i__35992 + (1));
seq__35989 = G__36137;
chunk__35990 = G__36138;
count__35991 = G__36139;
i__35992 = G__36140;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__35989);
if(temp__5735__auto__){
var seq__35989__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__35989__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__35989__$1);
var G__36141 = cljs.core.chunk_rest(seq__35989__$1);
var G__36142 = c__4550__auto__;
var G__36143 = cljs.core.count(c__4550__auto__);
var G__36144 = (0);
seq__35989 = G__36141;
chunk__35990 = G__36142;
count__35991 = G__36143;
i__35992 = G__36144;
continue;
} else {
var js_ns = cljs.core.first(seq__35989__$1);
var require_str_36145 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_36145);


var G__36146 = cljs.core.next(seq__35989__$1);
var G__36147 = null;
var G__36148 = (0);
var G__36149 = (0);
seq__35989 = G__36146;
chunk__35990 = G__36147;
count__35991 = G__36148;
i__35992 = G__36149;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.load_sources = (function shadow$cljs$devtools$client$browser$load_sources(sources,callback){
if(cljs.core.empty_QMARK_(sources)){
var G__35993 = cljs.core.PersistentVector.EMPTY;
return (callback.cljs$core$IFn$_invoke$arity$1 ? callback.cljs$core$IFn$_invoke$arity$1(G__35993) : callback.call(null,G__35993));
} else {
var G__35994 = shadow.cljs.devtools.client.env.files_url();
var G__35995 = ((function (G__35994){
return (function (res){
var req = this;
var content = cljs.reader.read_string.cljs$core$IFn$_invoke$arity$1(req.getResponseText());
return (callback.cljs$core$IFn$_invoke$arity$1 ? callback.cljs$core$IFn$_invoke$arity$1(content) : callback.call(null,content));
});})(G__35994))
;
var G__35996 = "POST";
var G__35997 = cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"client","client",-1323448117),new cljs.core.Keyword(null,"browser","browser",828191719),new cljs.core.Keyword(null,"sources","sources",-321166424),cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582)),sources)], null)], 0));
var G__35998 = ({"content-type": "application/edn; charset=utf-8"});
return goog.net.XhrIo.send(G__35994,G__35995,G__35996,G__35997,G__35998);
}
});
shadow.cljs.devtools.client.browser.handle_build_complete = (function shadow$cljs$devtools$client$browser$handle_build_complete(p__36000){
var map__36001 = p__36000;
var map__36001__$1 = (((((!((map__36001 == null))))?(((((map__36001.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36001.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36001):map__36001);
var msg = map__36001__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36001__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36001__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var map__36003 = info;
var map__36003__$1 = (((((!((map__36003 == null))))?(((((map__36003.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36003.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36003):map__36003);
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36003__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var compiled = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36003__$1,new cljs.core.Keyword(null,"compiled","compiled",850043082));
var warnings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.distinct.cljs$core$IFn$_invoke$arity$1((function (){var iter__4523__auto__ = ((function (map__36003,map__36003__$1,sources,compiled,map__36001,map__36001__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__36006(s__36007){
return (new cljs.core.LazySeq(null,((function (map__36003,map__36003__$1,sources,compiled,map__36001,map__36001__$1,msg,info,reload_info){
return (function (){
var s__36007__$1 = s__36007;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__36007__$1);
if(temp__5735__auto__){
var xs__6292__auto__ = temp__5735__auto__;
var map__36012 = cljs.core.first(xs__6292__auto__);
var map__36012__$1 = (((((!((map__36012 == null))))?(((((map__36012.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36012.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36012):map__36012);
var src = map__36012__$1;
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36012__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36012__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))){
var iterys__4519__auto__ = ((function (s__36007__$1,map__36012,map__36012__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__36003,map__36003__$1,sources,compiled,map__36001,map__36001__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__36006_$_iter__36008(s__36009){
return (new cljs.core.LazySeq(null,((function (s__36007__$1,map__36012,map__36012__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__36003,map__36003__$1,sources,compiled,map__36001,map__36001__$1,msg,info,reload_info){
return (function (){
var s__36009__$1 = s__36009;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__36009__$1);
if(temp__5735__auto____$1){
var s__36009__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__36009__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__36009__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__36011 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__36010 = (0);
while(true){
if((i__36010 < size__4522__auto__)){
var warning = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__36010);
cljs.core.chunk_append(b__36011,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name));

var G__36150 = (i__36010 + (1));
i__36010 = G__36150;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__36011),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__36006_$_iter__36008(cljs.core.chunk_rest(s__36009__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__36011),null);
}
} else {
var warning = cljs.core.first(s__36009__$2);
return cljs.core.cons(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__36006_$_iter__36008(cljs.core.rest(s__36009__$2)));
}
} else {
return null;
}
break;
}
});})(s__36007__$1,map__36012,map__36012__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__36003,map__36003__$1,sources,compiled,map__36001,map__36001__$1,msg,info,reload_info))
,null,null));
});})(s__36007__$1,map__36012,map__36012__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__36003,map__36003__$1,sources,compiled,map__36001,map__36001__$1,msg,info,reload_info))
;
var fs__4520__auto__ = cljs.core.seq(iterys__4519__auto__(warnings));
if(fs__4520__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4520__auto__,shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__36006(cljs.core.rest(s__36007__$1)));
} else {
var G__36151 = cljs.core.rest(s__36007__$1);
s__36007__$1 = G__36151;
continue;
}
} else {
var G__36152 = cljs.core.rest(s__36007__$1);
s__36007__$1 = G__36152;
continue;
}
} else {
return null;
}
break;
}
});})(map__36003,map__36003__$1,sources,compiled,map__36001,map__36001__$1,msg,info,reload_info))
,null,null));
});})(map__36003,map__36003__$1,sources,compiled,map__36001,map__36001__$1,msg,info,reload_info))
;
return iter__4523__auto__(sources);
})()));
var seq__36015_36153 = cljs.core.seq(warnings);
var chunk__36016_36154 = null;
var count__36017_36155 = (0);
var i__36018_36156 = (0);
while(true){
if((i__36018_36156 < count__36017_36155)){
var map__36023_36157 = chunk__36016_36154.cljs$core$IIndexed$_nth$arity$2(null,i__36018_36156);
var map__36023_36158__$1 = (((((!((map__36023_36157 == null))))?(((((map__36023_36157.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36023_36157.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36023_36157):map__36023_36157);
var w_36159 = map__36023_36158__$1;
var msg_36160__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36023_36158__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_36161 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36023_36158__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_36162 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36023_36158__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_36163 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36023_36158__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_36163)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_36161),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_36162),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_36160__$1)].join(''));


var G__36164 = seq__36015_36153;
var G__36165 = chunk__36016_36154;
var G__36166 = count__36017_36155;
var G__36167 = (i__36018_36156 + (1));
seq__36015_36153 = G__36164;
chunk__36016_36154 = G__36165;
count__36017_36155 = G__36166;
i__36018_36156 = G__36167;
continue;
} else {
var temp__5735__auto___36168 = cljs.core.seq(seq__36015_36153);
if(temp__5735__auto___36168){
var seq__36015_36169__$1 = temp__5735__auto___36168;
if(cljs.core.chunked_seq_QMARK_(seq__36015_36169__$1)){
var c__4550__auto___36170 = cljs.core.chunk_first(seq__36015_36169__$1);
var G__36171 = cljs.core.chunk_rest(seq__36015_36169__$1);
var G__36172 = c__4550__auto___36170;
var G__36173 = cljs.core.count(c__4550__auto___36170);
var G__36174 = (0);
seq__36015_36153 = G__36171;
chunk__36016_36154 = G__36172;
count__36017_36155 = G__36173;
i__36018_36156 = G__36174;
continue;
} else {
var map__36025_36175 = cljs.core.first(seq__36015_36169__$1);
var map__36025_36176__$1 = (((((!((map__36025_36175 == null))))?(((((map__36025_36175.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36025_36175.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36025_36175):map__36025_36175);
var w_36177 = map__36025_36176__$1;
var msg_36178__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36025_36176__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_36179 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36025_36176__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_36180 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36025_36176__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_36181 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36025_36176__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_36181)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_36179),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_36180),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_36178__$1)].join(''));


var G__36182 = cljs.core.next(seq__36015_36169__$1);
var G__36183 = null;
var G__36184 = (0);
var G__36185 = (0);
seq__36015_36153 = G__36182;
chunk__36016_36154 = G__36183;
count__36017_36155 = G__36184;
i__36018_36156 = G__36185;
continue;
}
} else {
}
}
break;
}

if((!(shadow.cljs.devtools.client.env.autoload))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(((cljs.core.empty_QMARK_(warnings)) || (shadow.cljs.devtools.client.env.ignore_warnings))){
var sources_to_get = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.filter.cljs$core$IFn$_invoke$arity$2(((function (map__36003,map__36003__$1,sources,compiled,warnings,map__36001,map__36001__$1,msg,info,reload_info){
return (function (p__36027){
var map__36028 = p__36027;
var map__36028__$1 = (((((!((map__36028 == null))))?(((((map__36028.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36028.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36028):map__36028);
var src = map__36028__$1;
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36028__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36028__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
return ((cljs.core.contains_QMARK_(new cljs.core.Keyword(null,"always-load","always-load",66405637).cljs$core$IFn$_invoke$arity$1(reload_info),ns)) || (cljs.core.not(shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_(src))) || (((cljs.core.contains_QMARK_(compiled,resource_id)) && (cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))))));
});})(map__36003,map__36003__$1,sources,compiled,warnings,map__36001,map__36001__$1,msg,info,reload_info))
,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(((function (map__36003,map__36003__$1,sources,compiled,warnings,map__36001,map__36001__$1,msg,info,reload_info){
return (function (p__36030){
var map__36031 = p__36030;
var map__36031__$1 = (((((!((map__36031 == null))))?(((((map__36031.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36031.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36031):map__36031);
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36031__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
return cljs.core.contains_QMARK_(new cljs.core.Keyword(null,"never-load","never-load",1300896819).cljs$core$IFn$_invoke$arity$1(reload_info),ns);
});})(map__36003,map__36003__$1,sources,compiled,warnings,map__36001,map__36001__$1,msg,info,reload_info))
,cljs.core.filter.cljs$core$IFn$_invoke$arity$2(((function (map__36003,map__36003__$1,sources,compiled,warnings,map__36001,map__36001__$1,msg,info,reload_info){
return (function (p__36033){
var map__36034 = p__36033;
var map__36034__$1 = (((((!((map__36034 == null))))?(((((map__36034.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36034.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36034):map__36034);
var rc = map__36034__$1;
var module = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36034__$1,new cljs.core.Keyword(null,"module","module",1424618191));
return ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("js",shadow.cljs.devtools.client.env.module_format)) || (shadow.cljs.devtools.client.browser.module_is_active_QMARK_(module)));
});})(map__36003,map__36003__$1,sources,compiled,warnings,map__36001,map__36001__$1,msg,info,reload_info))
,sources))));
if(cljs.core.not(cljs.core.seq(sources_to_get))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"after-load","after-load",-1278503285)], null)))){
} else {
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("reloading code but no :after-load hooks are configured!",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["https://shadow-cljs.github.io/docs/UsersGuide.html#_lifecycle_hooks"], 0));
}

return shadow.cljs.devtools.client.browser.load_sources(sources_to_get,((function (sources_to_get,map__36003,map__36003__$1,sources,compiled,warnings,map__36001,map__36001__$1,msg,info,reload_info){
return (function (p1__35999_SHARP_){
return shadow.cljs.devtools.client.browser.do_js_reload(msg,p1__35999_SHARP_,shadow.cljs.devtools.client.hud.load_end_success,shadow.cljs.devtools.client.hud.load_failure);
});})(sources_to_get,map__36003,map__36003__$1,sources,compiled,warnings,map__36001,map__36001__$1,msg,info,reload_info))
);
}
} else {
return null;
}
}
});
shadow.cljs.devtools.client.browser.page_load_uri = (cljs.core.truth_(goog.global.document)?goog.Uri.parse(document.location.href):null);
shadow.cljs.devtools.client.browser.match_paths = (function shadow$cljs$devtools$client$browser$match_paths(old,new$){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("file",shadow.cljs.devtools.client.browser.page_load_uri.getScheme())){
var rel_new = cljs.core.subs.cljs$core$IFn$_invoke$arity$2(new$,(1));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(old,rel_new)) || (clojure.string.starts_with_QMARK_(old,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(rel_new),"?"].join(''))))){
return rel_new;
} else {
return null;
}
} else {
var node_uri = goog.Uri.parse(old);
var node_uri_resolved = shadow.cljs.devtools.client.browser.page_load_uri.resolve(node_uri);
var node_abs = node_uri_resolved.getPath();
var and__4120__auto__ = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.browser.page_load_uri.hasSameDomainAs(node_uri))) || (cljs.core.not(node_uri.hasDomain())));
if(and__4120__auto__){
var and__4120__auto____$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(node_abs,new$);
if(and__4120__auto____$1){
return new$;
} else {
return and__4120__auto____$1;
}
} else {
return and__4120__auto__;
}
}
});
shadow.cljs.devtools.client.browser.handle_asset_watch = (function shadow$cljs$devtools$client$browser$handle_asset_watch(p__36036){
var map__36037 = p__36036;
var map__36037__$1 = (((((!((map__36037 == null))))?(((((map__36037.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36037.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36037):map__36037);
var msg = map__36037__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36037__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
var seq__36039 = cljs.core.seq(updates);
var chunk__36041 = null;
var count__36042 = (0);
var i__36043 = (0);
while(true){
if((i__36043 < count__36042)){
var path = chunk__36041.cljs$core$IIndexed$_nth$arity$2(null,i__36043);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__36069_36186 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__36072_36187 = null;
var count__36073_36188 = (0);
var i__36074_36189 = (0);
while(true){
if((i__36074_36189 < count__36073_36188)){
var node_36190 = chunk__36072_36187.cljs$core$IIndexed$_nth$arity$2(null,i__36074_36189);
var path_match_36191 = shadow.cljs.devtools.client.browser.match_paths(node_36190.getAttribute("href"),path);
if(cljs.core.truth_(path_match_36191)){
var new_link_36192 = (function (){var G__36079 = node_36190.cloneNode(true);
G__36079.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_36191),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__36079;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_36191], 0));

goog.dom.insertSiblingAfter(new_link_36192,node_36190);

goog.dom.removeNode(node_36190);


var G__36193 = seq__36069_36186;
var G__36194 = chunk__36072_36187;
var G__36195 = count__36073_36188;
var G__36196 = (i__36074_36189 + (1));
seq__36069_36186 = G__36193;
chunk__36072_36187 = G__36194;
count__36073_36188 = G__36195;
i__36074_36189 = G__36196;
continue;
} else {
var G__36197 = seq__36069_36186;
var G__36198 = chunk__36072_36187;
var G__36199 = count__36073_36188;
var G__36200 = (i__36074_36189 + (1));
seq__36069_36186 = G__36197;
chunk__36072_36187 = G__36198;
count__36073_36188 = G__36199;
i__36074_36189 = G__36200;
continue;
}
} else {
var temp__5735__auto___36201 = cljs.core.seq(seq__36069_36186);
if(temp__5735__auto___36201){
var seq__36069_36202__$1 = temp__5735__auto___36201;
if(cljs.core.chunked_seq_QMARK_(seq__36069_36202__$1)){
var c__4550__auto___36203 = cljs.core.chunk_first(seq__36069_36202__$1);
var G__36204 = cljs.core.chunk_rest(seq__36069_36202__$1);
var G__36205 = c__4550__auto___36203;
var G__36206 = cljs.core.count(c__4550__auto___36203);
var G__36207 = (0);
seq__36069_36186 = G__36204;
chunk__36072_36187 = G__36205;
count__36073_36188 = G__36206;
i__36074_36189 = G__36207;
continue;
} else {
var node_36208 = cljs.core.first(seq__36069_36202__$1);
var path_match_36209 = shadow.cljs.devtools.client.browser.match_paths(node_36208.getAttribute("href"),path);
if(cljs.core.truth_(path_match_36209)){
var new_link_36210 = (function (){var G__36080 = node_36208.cloneNode(true);
G__36080.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_36209),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__36080;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_36209], 0));

goog.dom.insertSiblingAfter(new_link_36210,node_36208);

goog.dom.removeNode(node_36208);


var G__36211 = cljs.core.next(seq__36069_36202__$1);
var G__36212 = null;
var G__36213 = (0);
var G__36214 = (0);
seq__36069_36186 = G__36211;
chunk__36072_36187 = G__36212;
count__36073_36188 = G__36213;
i__36074_36189 = G__36214;
continue;
} else {
var G__36215 = cljs.core.next(seq__36069_36202__$1);
var G__36216 = null;
var G__36217 = (0);
var G__36218 = (0);
seq__36069_36186 = G__36215;
chunk__36072_36187 = G__36216;
count__36073_36188 = G__36217;
i__36074_36189 = G__36218;
continue;
}
}
} else {
}
}
break;
}


var G__36219 = seq__36039;
var G__36220 = chunk__36041;
var G__36221 = count__36042;
var G__36222 = (i__36043 + (1));
seq__36039 = G__36219;
chunk__36041 = G__36220;
count__36042 = G__36221;
i__36043 = G__36222;
continue;
} else {
var G__36223 = seq__36039;
var G__36224 = chunk__36041;
var G__36225 = count__36042;
var G__36226 = (i__36043 + (1));
seq__36039 = G__36223;
chunk__36041 = G__36224;
count__36042 = G__36225;
i__36043 = G__36226;
continue;
}
} else {
var temp__5735__auto__ = cljs.core.seq(seq__36039);
if(temp__5735__auto__){
var seq__36039__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__36039__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__36039__$1);
var G__36227 = cljs.core.chunk_rest(seq__36039__$1);
var G__36228 = c__4550__auto__;
var G__36229 = cljs.core.count(c__4550__auto__);
var G__36230 = (0);
seq__36039 = G__36227;
chunk__36041 = G__36228;
count__36042 = G__36229;
i__36043 = G__36230;
continue;
} else {
var path = cljs.core.first(seq__36039__$1);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__36081_36231 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__36084_36232 = null;
var count__36085_36233 = (0);
var i__36086_36234 = (0);
while(true){
if((i__36086_36234 < count__36085_36233)){
var node_36235 = chunk__36084_36232.cljs$core$IIndexed$_nth$arity$2(null,i__36086_36234);
var path_match_36236 = shadow.cljs.devtools.client.browser.match_paths(node_36235.getAttribute("href"),path);
if(cljs.core.truth_(path_match_36236)){
var new_link_36237 = (function (){var G__36091 = node_36235.cloneNode(true);
G__36091.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_36236),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__36091;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_36236], 0));

goog.dom.insertSiblingAfter(new_link_36237,node_36235);

goog.dom.removeNode(node_36235);


var G__36238 = seq__36081_36231;
var G__36239 = chunk__36084_36232;
var G__36240 = count__36085_36233;
var G__36241 = (i__36086_36234 + (1));
seq__36081_36231 = G__36238;
chunk__36084_36232 = G__36239;
count__36085_36233 = G__36240;
i__36086_36234 = G__36241;
continue;
} else {
var G__36242 = seq__36081_36231;
var G__36243 = chunk__36084_36232;
var G__36244 = count__36085_36233;
var G__36245 = (i__36086_36234 + (1));
seq__36081_36231 = G__36242;
chunk__36084_36232 = G__36243;
count__36085_36233 = G__36244;
i__36086_36234 = G__36245;
continue;
}
} else {
var temp__5735__auto___36246__$1 = cljs.core.seq(seq__36081_36231);
if(temp__5735__auto___36246__$1){
var seq__36081_36247__$1 = temp__5735__auto___36246__$1;
if(cljs.core.chunked_seq_QMARK_(seq__36081_36247__$1)){
var c__4550__auto___36248 = cljs.core.chunk_first(seq__36081_36247__$1);
var G__36249 = cljs.core.chunk_rest(seq__36081_36247__$1);
var G__36250 = c__4550__auto___36248;
var G__36251 = cljs.core.count(c__4550__auto___36248);
var G__36252 = (0);
seq__36081_36231 = G__36249;
chunk__36084_36232 = G__36250;
count__36085_36233 = G__36251;
i__36086_36234 = G__36252;
continue;
} else {
var node_36253 = cljs.core.first(seq__36081_36247__$1);
var path_match_36254 = shadow.cljs.devtools.client.browser.match_paths(node_36253.getAttribute("href"),path);
if(cljs.core.truth_(path_match_36254)){
var new_link_36255 = (function (){var G__36092 = node_36253.cloneNode(true);
G__36092.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_36254),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__36092;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_36254], 0));

goog.dom.insertSiblingAfter(new_link_36255,node_36253);

goog.dom.removeNode(node_36253);


var G__36256 = cljs.core.next(seq__36081_36247__$1);
var G__36257 = null;
var G__36258 = (0);
var G__36259 = (0);
seq__36081_36231 = G__36256;
chunk__36084_36232 = G__36257;
count__36085_36233 = G__36258;
i__36086_36234 = G__36259;
continue;
} else {
var G__36260 = cljs.core.next(seq__36081_36247__$1);
var G__36261 = null;
var G__36262 = (0);
var G__36263 = (0);
seq__36081_36231 = G__36260;
chunk__36084_36232 = G__36261;
count__36085_36233 = G__36262;
i__36086_36234 = G__36263;
continue;
}
}
} else {
}
}
break;
}


var G__36264 = cljs.core.next(seq__36039__$1);
var G__36265 = null;
var G__36266 = (0);
var G__36267 = (0);
seq__36039 = G__36264;
chunk__36041 = G__36265;
count__36042 = G__36266;
i__36043 = G__36267;
continue;
} else {
var G__36268 = cljs.core.next(seq__36039__$1);
var G__36269 = null;
var G__36270 = (0);
var G__36271 = (0);
seq__36039 = G__36268;
chunk__36041 = G__36269;
count__36042 = G__36270;
i__36043 = G__36271;
continue;
}
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.get_ua_product = (function shadow$cljs$devtools$client$browser$get_ua_product(){
if(cljs.core.truth_(goog.userAgent.product.SAFARI)){
return new cljs.core.Keyword(null,"safari","safari",497115653);
} else {
if(cljs.core.truth_(goog.userAgent.product.CHROME)){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.FIREFOX)){
return new cljs.core.Keyword(null,"firefox","firefox",1283768880);
} else {
if(cljs.core.truth_(goog.userAgent.product.IE)){
return new cljs.core.Keyword(null,"ie","ie",2038473780);
} else {
return null;
}
}
}
}
});
shadow.cljs.devtools.client.browser.get_asset_root = (function shadow$cljs$devtools$client$browser$get_asset_root(){
var loc = (new goog.Uri(document.location.href));
var cbp = (new goog.Uri(CLOSURE_BASE_PATH));
var s = loc.resolve(cbp).toString();
return clojure.string.replace(s,/^file:\//,"file:///");
});
shadow.cljs.devtools.client.browser.repl_error = (function shadow$cljs$devtools$client$browser$repl_error(e){
console.error("repl/invoke error",e);

return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(shadow.cljs.devtools.client.env.repl_error(e),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),shadow.cljs.devtools.client.browser.get_ua_product(),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"asset-root","asset-root",1771735072),shadow.cljs.devtools.client.browser.get_asset_root()], 0));
});
shadow.cljs.devtools.client.browser.global_eval = (function shadow$cljs$devtools$client$browser$global_eval(js){
return (0,eval)(js);;
});
shadow.cljs.devtools.client.browser.repl_invoke = (function shadow$cljs$devtools$client$browser$repl_invoke(p__36093){
var map__36094 = p__36093;
var map__36094__$1 = (((((!((map__36094 == null))))?(((((map__36094.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36094.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36094):map__36094);
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36094__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36094__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var result = shadow.cljs.devtools.client.env.repl_call(((function (map__36094,map__36094__$1,id,js){
return (function (){
return shadow.cljs.devtools.client.browser.global_eval(js);
});})(map__36094,map__36094__$1,id,js))
,shadow.cljs.devtools.client.browser.repl_error);
return shadow.cljs.devtools.client.browser.ws_msg(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(result,new cljs.core.Keyword(null,"id","id",-1388402092),id));
});
shadow.cljs.devtools.client.browser.repl_require = (function shadow$cljs$devtools$client$browser$repl_require(p__36096,done){
var map__36097 = p__36096;
var map__36097__$1 = (((((!((map__36097 == null))))?(((((map__36097.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36097.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36097):map__36097);
var msg = map__36097__$1;
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36097__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36097__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36097__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var js_requires = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36097__$1,new cljs.core.Keyword(null,"js-requires","js-requires",-1311472051));
var sources_to_load = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(((function (map__36097,map__36097__$1,msg,id,sources,reload_namespaces,js_requires){
return (function (p__36099){
var map__36100 = p__36099;
var map__36100__$1 = (((((!((map__36100 == null))))?(((((map__36100.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36100.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36100):map__36100);
var src = map__36100__$1;
var provides = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36100__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var and__4120__auto__ = shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_(src);
if(cljs.core.truth_(and__4120__auto__)){
return cljs.core.not(cljs.core.some(reload_namespaces,provides));
} else {
return and__4120__auto__;
}
});})(map__36097,map__36097__$1,msg,id,sources,reload_namespaces,js_requires))
,sources));
return shadow.cljs.devtools.client.browser.load_sources(sources_to_load,((function (sources_to_load,map__36097,map__36097__$1,msg,id,sources,reload_namespaces,js_requires){
return (function (sources__$1){
try{shadow.cljs.devtools.client.browser.do_js_load(sources__$1);

if(cljs.core.seq(js_requires)){
shadow.cljs.devtools.client.browser.do_js_requires(js_requires);
} else {
}

return shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","require-complete","repl/require-complete",-2140254719),new cljs.core.Keyword(null,"id","id",-1388402092),id], null));
}catch (e36102){var e = e36102;
return shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","require-error","repl/require-error",1689310021),new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"error","error",-978969032),e.message], null));
}finally {(done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}});})(sources_to_load,map__36097,map__36097__$1,msg,id,sources,reload_namespaces,js_requires))
);
});
shadow.cljs.devtools.client.browser.repl_init = (function shadow$cljs$devtools$client$browser$repl_init(p__36103,done){
var map__36104 = p__36103;
var map__36104__$1 = (((((!((map__36104 == null))))?(((((map__36104.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36104.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36104):map__36104);
var repl_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36104__$1,new cljs.core.Keyword(null,"repl-state","repl-state",-1733780387));
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36104__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
return shadow.cljs.devtools.client.browser.load_sources(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535).cljs$core$IFn$_invoke$arity$1(repl_state))),((function (map__36104,map__36104__$1,repl_state,id){
return (function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","init-complete","repl/init-complete",-162252879),new cljs.core.Keyword(null,"id","id",-1388402092),id], null));

shadow.cljs.devtools.client.browser.devtools_msg("REPL session start successful");

return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
});})(map__36104,map__36104__$1,repl_state,id))
);
});
shadow.cljs.devtools.client.browser.repl_set_ns = (function shadow$cljs$devtools$client$browser$repl_set_ns(p__36106){
var map__36107 = p__36106;
var map__36107__$1 = (((((!((map__36107 == null))))?(((((map__36107.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36107.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36107):map__36107);
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36107__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36107__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
return shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","set-ns-complete","repl/set-ns-complete",680944662),new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"ns","ns",441598760),ns], null));
});
shadow.cljs.devtools.client.browser.close_reason_ref = cljs.core.volatile_BANG_(null);
shadow.cljs.devtools.client.browser.handle_message = (function shadow$cljs$devtools$client$browser$handle_message(p__36109,done){
var map__36110 = p__36109;
var map__36110__$1 = (((((!((map__36110 == null))))?(((((map__36110.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36110.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36110):map__36110);
var msg = map__36110__$1;
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36110__$1,new cljs.core.Keyword(null,"type","type",1174270348));
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

var G__36112_36272 = type;
var G__36112_36273__$1 = (((G__36112_36272 instanceof cljs.core.Keyword))?G__36112_36272.fqn:null);
switch (G__36112_36273__$1) {
case "asset-watch":
shadow.cljs.devtools.client.browser.handle_asset_watch(msg);

break;
case "repl/invoke":
shadow.cljs.devtools.client.browser.repl_invoke(msg);

break;
case "repl/require":
shadow.cljs.devtools.client.browser.repl_require(msg,done);

break;
case "repl/set-ns":
shadow.cljs.devtools.client.browser.repl_set_ns(msg);

break;
case "repl/init":
shadow.cljs.devtools.client.browser.repl_init(msg,done);

break;
case "repl/session-start":
shadow.cljs.devtools.client.browser.repl_init(msg,done);

break;
case "build-complete":
shadow.cljs.devtools.client.hud.hud_warnings(msg);

shadow.cljs.devtools.client.browser.handle_build_complete(msg);

break;
case "build-failure":
shadow.cljs.devtools.client.hud.load_end();

shadow.cljs.devtools.client.hud.hud_error(msg);

break;
case "build-init":
shadow.cljs.devtools.client.hud.hud_warnings(msg);

break;
case "build-start":
shadow.cljs.devtools.client.hud.hud_hide();

shadow.cljs.devtools.client.hud.load_start();

break;
case "pong":

break;
case "client/stale":
cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.close_reason_ref,"Stale Client! You are not using the latest compilation output!");

break;
case "client/no-worker":
cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.close_reason_ref,["watch for build \"",shadow.cljs.devtools.client.env.build_id,"\" not running"].join(''));

break;
case "custom-msg":
shadow.cljs.devtools.client.env.publish_BANG_(new cljs.core.Keyword(null,"payload","payload",-383036092).cljs$core$IFn$_invoke$arity$1(msg));

break;
default:

}

if(cljs.core.contains_QMARK_(shadow.cljs.devtools.client.env.async_ops,type)){
return null;
} else {
return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}
});
shadow.cljs.devtools.client.browser.compile = (function shadow$cljs$devtools$client$browser$compile(text,callback){
var G__36113 = ["http",((shadow.cljs.devtools.client.env.ssl)?"s":null),"://",shadow.cljs.devtools.client.env.server_host,":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.env.server_port),"/worker/compile/",shadow.cljs.devtools.client.env.build_id,"/",shadow.cljs.devtools.client.env.proc_id,"/browser"].join('');
var G__36114 = ((function (G__36113){
return (function (res){
var req = this;
var actions = cljs.reader.read_string.cljs$core$IFn$_invoke$arity$1(req.getResponseText());
if(cljs.core.truth_(callback)){
return (callback.cljs$core$IFn$_invoke$arity$1 ? callback.cljs$core$IFn$_invoke$arity$1(actions) : callback.call(null,actions));
} else {
return null;
}
});})(G__36113))
;
var G__36115 = "POST";
var G__36116 = cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"input","input",556931961),text], null)], 0));
var G__36117 = ({"content-type": "application/edn; charset=utf-8"});
return goog.net.XhrIo.send(G__36113,G__36114,G__36115,G__36116,G__36117);
});
shadow.cljs.devtools.client.browser.heartbeat_BANG_ = (function shadow$cljs$devtools$client$browser$heartbeat_BANG_(){
var temp__5735__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5735__auto__)){
var s = temp__5735__auto__;
s.send(cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"ping","ping",-1670114784),new cljs.core.Keyword(null,"v","v",21465059),Date.now()], null)], 0)));

return setTimeout(shadow.cljs.devtools.client.browser.heartbeat_BANG_,(30000));
} else {
return null;
}
});
shadow.cljs.devtools.client.browser.ws_connect = (function shadow$cljs$devtools$client$browser$ws_connect(){
try{var print_fn = cljs.core._STAR_print_fn_STAR_;
var ws_url = shadow.cljs.devtools.client.env.ws_url(new cljs.core.Keyword(null,"browser","browser",828191719));
var socket = (new WebSocket(ws_url));
cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.socket_ref,socket);

socket.onmessage = ((function (print_fn,ws_url,socket){
return (function (e){
return shadow.cljs.devtools.client.env.process_ws_msg(e.data,shadow.cljs.devtools.client.browser.handle_message);
});})(print_fn,ws_url,socket))
;

socket.onopen = ((function (print_fn,ws_url,socket){
return (function (e){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.close_reason_ref,null);

if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("goog",shadow.cljs.devtools.client.env.module_format)){
goog.provide = goog.constructNamespace_;
} else {
}

shadow.cljs.devtools.client.env.set_print_fns_BANG_(shadow.cljs.devtools.client.browser.ws_msg);

return shadow.cljs.devtools.client.browser.devtools_msg("WebSocket connected!");
});})(print_fn,ws_url,socket))
;

socket.onclose = ((function (print_fn,ws_url,socket){
return (function (e){
shadow.cljs.devtools.client.browser.devtools_msg("WebSocket disconnected!");

shadow.cljs.devtools.client.hud.connection_error((function (){var or__4131__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.close_reason_ref);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "Connection closed!";
}
})());

cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.socket_ref,null);

return shadow.cljs.devtools.client.env.reset_print_fns_BANG_();
});})(print_fn,ws_url,socket))
;

socket.onerror = ((function (print_fn,ws_url,socket){
return (function (e){
shadow.cljs.devtools.client.hud.connection_error("Connection failed!");

return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("websocket error",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([e], 0));
});})(print_fn,ws_url,socket))
;

return setTimeout(shadow.cljs.devtools.client.browser.heartbeat_BANG_,(30000));
}catch (e36118){var e = e36118;
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("WebSocket setup failed",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([e], 0));
}});
if(shadow.cljs.devtools.client.env.enabled){
var temp__5735__auto___36275 = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5735__auto___36275)){
var s_36276 = temp__5735__auto___36275;
shadow.cljs.devtools.client.browser.devtools_msg("connection reset!");

s_36276.onclose = ((function (s_36276,temp__5735__auto___36275){
return (function (e){
return null;
});})(s_36276,temp__5735__auto___36275))
;

s_36276.close();

cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.socket_ref,null);
} else {
}

window.addEventListener("beforeunload",(function (){
var temp__5735__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5735__auto__)){
var s = temp__5735__auto__;
return s.close();
} else {
return null;
}
}));

if(cljs.core.truth_((function (){var and__4120__auto__ = document;
if(cljs.core.truth_(and__4120__auto__)){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("loading",document.readyState);
} else {
return and__4120__auto__;
}
})())){
window.addEventListener("DOMContentLoaded",shadow.cljs.devtools.client.browser.ws_connect);
} else {
setTimeout(shadow.cljs.devtools.client.browser.ws_connect,(10));
}
} else {
}

//# sourceMappingURL=shadow.cljs.devtools.client.browser.js.map
