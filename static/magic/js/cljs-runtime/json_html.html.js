goog.provide('json_html.html');
goog.require('cljs.core');
goog.require('clojure.string');
goog.require('clojure.walk');
json_html.html.normalize_body = (function json_html$html$normalize_body(body){
if(cljs.core.coll_QMARK_(body)){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.doall.cljs$core$IFn$_invoke$arity$1(body));
} else {
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(body);
}
});
/**
 * Converts its arguments into a string using to-str.
 */
json_html.html.as_str = (function json_html$html$as_str(var_args){
var args__4736__auto__ = [];
var len__4730__auto___31895 = arguments.length;
var i__4731__auto___31896 = (0);
while(true){
if((i__4731__auto___31896 < len__4730__auto___31895)){
args__4736__auto__.push((arguments[i__4731__auto___31896]));

var G__31899 = (i__4731__auto___31896 + (1));
i__4731__auto___31896 = G__31899;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return json_html.html.as_str.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

json_html.html.as_str.cljs$core$IFn$_invoke$arity$variadic = (function (xs){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.map.cljs$core$IFn$_invoke$arity$2(json_html.html.normalize_body,xs));
});

json_html.html.as_str.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
json_html.html.as_str.cljs$lang$applyTo = (function (seq31743){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq31743));
});

/**
 * Change special characters into HTML character entities.
 */
json_html.html.escape_html = (function json_html$html$escape_html(text){
return clojure.string.replace(clojure.string.replace(clojure.string.replace(clojure.string.replace(json_html.html.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([text], 0)),/&/,"&amp;"),/</,"&lt;"),/>/,"&gt;"),/'/,"&apos;");
});
json_html.html.xml_attribute = (function json_html$html$xml_attribute(id,value){
return [" ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(json_html.html.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.name(id)], 0))),"=\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(json_html.html.escape_html(value)),"\""].join('');
});
json_html.html.render_attribute = (function json_html$html$render_attribute(p__31748){
var vec__31749 = p__31748;
var name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31749,(0),null);
var value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31749,(1),null);
if(value === true){
return json_html.html.xml_attribute(name,name);
} else {
if(cljs.core.not(value)){
return "";
} else {
return json_html.html.xml_attribute(name,value);

}
}
});
json_html.html.render_attr_map = (function json_html$html$render_attr_map(attrs){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.sort.cljs$core$IFn$_invoke$arity$1(cljs.core.map.cljs$core$IFn$_invoke$arity$2(json_html.html.render_attribute,attrs)));
});
json_html.html.merge_attributes = (function json_html$html$merge_attributes(p__31775,map_attrs){
var map__31785 = p__31775;
var map__31785__$1 = (((((!((map__31785 == null))))?(((((map__31785.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31785.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31785):map__31785);
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31785__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31785__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
return cljs.core.merge_with.cljs$core$IFn$_invoke$arity$variadic(((function (map__31785,map__31785__$1,id,class$){
return (function (p1__31760_SHARP_,p2__31761_SHARP_){
if(cljs.core.truth_(p1__31760_SHARP_)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__31760_SHARP_)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(p2__31761_SHARP_)].join('');
} else {
return p2__31761_SHARP_;
}
});})(map__31785,map__31785__$1,id,class$))
,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(cljs.core.truth_(class$)?new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"class","class",-2030961996),class$], null):null),cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(cljs.core.truth_(id)?new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),id], null):null),map_attrs], 0))], 0));
});
json_html.html.normalize_element = (function json_html$html$normalize_element(p__31832){
var vec__31837 = p__31832;
var seq__31838 = cljs.core.seq(vec__31837);
var first__31839 = cljs.core.first(seq__31838);
var seq__31838__$1 = cljs.core.next(seq__31838);
var tag = first__31839;
var content = seq__31838__$1;
var re_tag = /([^\s\.#]+)(?:#([^\s\.#]+))?(?:\.([^\s#]+))?/;
var vec__31852 = cljs.core.re_matches(re_tag,json_html.html.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.name(tag)], 0)));
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31852,(0),null);
var tag__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31852,(1),null);
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31852,(2),null);
var class$ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31852,(3),null);
var tag_attrs = new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"class","class",-2030961996),(cljs.core.truth_(class$)?clojure.string.replace(class$,/\./," "):null)], null);
var map_attrs = cljs.core.first(content);
if(cljs.core.map_QMARK_(map_attrs)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [tag__$1,json_html.html.merge_attributes(tag_attrs,map_attrs),cljs.core.next(content)], null);
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [tag__$1,tag_attrs,content], null);
}
});
json_html.html.render_element = (function json_html$html$render_element(p__31883){
var vec__31886 = p__31883;
var seq__31887 = cljs.core.seq(vec__31886);
var first__31888 = cljs.core.first(seq__31887);
var seq__31887__$1 = cljs.core.next(seq__31887);
var tag = first__31888;
var first__31888__$1 = cljs.core.first(seq__31887__$1);
var seq__31887__$2 = cljs.core.next(seq__31887__$1);
var attrs = first__31888__$1;
var content = seq__31887__$2;
return ["<",cljs.core.name(tag),cljs.core.str.cljs$core$IFn$_invoke$arity$1(json_html.html.render_attr_map(attrs)),">",cljs.core.str.cljs$core$IFn$_invoke$arity$1(json_html.html.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.flatten(content)], 0))),"</",cljs.core.name(tag),">"].join('');
});
json_html.html.html = (function json_html$html$html(hiccup){
return clojure.walk.postwalk((function (node){
if(cljs.core.vector_QMARK_(node)){
return json_html.html.render_element(json_html.html.normalize_element(node));
} else {
return node;
}
}),hiccup);
});

//# sourceMappingURL=json_html.html.js.map
