goog.provide('clojure.data');
goog.require('cljs.core');
goog.require('clojure.set');
/**
 * Internal helper for diff.
 */
clojure.data.atom_diff = (function clojure$data$atom_diff(a,b){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(a,b)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,null,a], null);
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [a,b,null], null);
}
});
/**
 * Convert an associative-by-numeric-index collection into
 * an equivalent vector, with nil for any missing keys
 */
clojure.data.vectorize = (function clojure$data$vectorize(m){
if(cljs.core.seq(m)){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (result,p__30704){
var vec__30705 = p__30704;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30705,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30705,(1),null);
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(result,k,v);
}),cljs.core.vec(cljs.core.repeat.cljs$core$IFn$_invoke$arity$2(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.max,cljs.core.keys(m)),null)),m);
} else {
return null;
}
});
/**
 * Diff associative things a and b, comparing only the key k.
 */
clojure.data.diff_associative_key = (function clojure$data$diff_associative_key(a,b,k){
var va = cljs.core.get.cljs$core$IFn$_invoke$arity$2(a,k);
var vb = cljs.core.get.cljs$core$IFn$_invoke$arity$2(b,k);
var vec__30712 = clojure.data.diff(va,vb);
var a_STAR_ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30712,(0),null);
var b_STAR_ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30712,(1),null);
var ab = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30712,(2),null);
var in_a = cljs.core.contains_QMARK_(a,k);
var in_b = cljs.core.contains_QMARK_(b,k);
var same = ((in_a) && (in_b) && ((((!((ab == null)))) || ((((va == null)) && ((vb == null)))))));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [((((in_a) && ((((!((a_STAR_ == null)))) || ((!(same)))))))?cljs.core.PersistentArrayMap.createAsIfByAssoc([k,a_STAR_]):null),((((in_b) && ((((!((b_STAR_ == null)))) || ((!(same)))))))?cljs.core.PersistentArrayMap.createAsIfByAssoc([k,b_STAR_]):null),((same)?cljs.core.PersistentArrayMap.createAsIfByAssoc([k,ab]):null)], null);
});
/**
 * Diff associative things a and b, comparing only keys in ks (if supplied).
 */
clojure.data.diff_associative = (function clojure$data$diff_associative(var_args){
var G__30724 = arguments.length;
switch (G__30724) {
case 2:
return clojure.data.diff_associative.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return clojure.data.diff_associative.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

clojure.data.diff_associative.cljs$core$IFn$_invoke$arity$2 = (function (a,b){
return clojure.data.diff_associative.cljs$core$IFn$_invoke$arity$3(a,b,clojure.set.union.cljs$core$IFn$_invoke$arity$2(cljs.core.keys(a),cljs.core.keys(b)));
});

clojure.data.diff_associative.cljs$core$IFn$_invoke$arity$3 = (function (a,b,ks){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (diff1,diff2){
return cljs.core.doall.cljs$core$IFn$_invoke$arity$1(cljs.core.map.cljs$core$IFn$_invoke$arity$3(cljs.core.merge,diff1,diff2));
}),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,null,null], null),cljs.core.map.cljs$core$IFn$_invoke$arity$2(cljs.core.partial.cljs$core$IFn$_invoke$arity$3(clojure.data.diff_associative_key,a,b),ks));
});

clojure.data.diff_associative.cljs$lang$maxFixedArity = 3;

clojure.data.diff_sequential = (function clojure$data$diff_sequential(a,b){
return cljs.core.vec(cljs.core.map.cljs$core$IFn$_invoke$arity$2(clojure.data.vectorize,clojure.data.diff_associative.cljs$core$IFn$_invoke$arity$3(((cljs.core.vector_QMARK_(a))?a:cljs.core.vec(a)),((cljs.core.vector_QMARK_(b))?b:cljs.core.vec(b)),cljs.core.range.cljs$core$IFn$_invoke$arity$1((function (){var x__4219__auto__ = cljs.core.count(a);
var y__4220__auto__ = cljs.core.count(b);
return ((x__4219__auto__ > y__4220__auto__) ? x__4219__auto__ : y__4220__auto__);
})()))));
});
clojure.data.diff_set = (function clojure$data$diff_set(a,b){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.not_empty(clojure.set.difference.cljs$core$IFn$_invoke$arity$2(a,b)),cljs.core.not_empty(clojure.set.difference.cljs$core$IFn$_invoke$arity$2(b,a)),cljs.core.not_empty(clojure.set.intersection.cljs$core$IFn$_invoke$arity$2(a,b))], null);
});

/**
 * Implementation detail. Subject to change.
 * @interface
 */
clojure.data.EqualityPartition = function(){};

/**
 * Implementation detail. Subject to change.
 */
clojure.data.equality_partition = (function clojure$data$equality_partition(x){
if((((!((x == null)))) && ((!((x.clojure$data$EqualityPartition$equality_partition$arity$1 == null)))))){
return x.clojure$data$EqualityPartition$equality_partition$arity$1(x);
} else {
var x__4433__auto__ = (((x == null))?null:x);
var m__4434__auto__ = (clojure.data.equality_partition[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(x) : m__4434__auto__.call(null,x));
} else {
var m__4431__auto__ = (clojure.data.equality_partition["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(x) : m__4431__auto__.call(null,x));
} else {
throw cljs.core.missing_protocol("EqualityPartition.equality-partition",x);
}
}
}
});


/**
 * Implementation detail. Subject to change.
 * @interface
 */
clojure.data.Diff = function(){};

/**
 * Implementation detail. Subject to change.
 */
clojure.data.diff_similar = (function clojure$data$diff_similar(a,b){
if((((!((a == null)))) && ((!((a.clojure$data$Diff$diff_similar$arity$2 == null)))))){
return a.clojure$data$Diff$diff_similar$arity$2(a,b);
} else {
var x__4433__auto__ = (((a == null))?null:a);
var m__4434__auto__ = (clojure.data.diff_similar[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(a,b) : m__4434__auto__.call(null,a,b));
} else {
var m__4431__auto__ = (clojure.data.diff_similar["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(a,b) : m__4431__auto__.call(null,a,b));
} else {
throw cljs.core.missing_protocol("Diff.diff-similar",a);
}
}
}
});

goog.object.set(clojure.data.EqualityPartition,"null",true);

var G__30743_30844 = clojure.data.equality_partition;
var G__30744_30845 = "null";
var G__30745_30846 = ((function (G__30743_30844,G__30744_30845){
return (function (x){
return new cljs.core.Keyword(null,"atom","atom",-397043653);
});})(G__30743_30844,G__30744_30845))
;
goog.object.set(G__30743_30844,G__30744_30845,G__30745_30846);

goog.object.set(clojure.data.EqualityPartition,"string",true);

var G__30746_30847 = clojure.data.equality_partition;
var G__30747_30848 = "string";
var G__30748_30849 = ((function (G__30746_30847,G__30747_30848){
return (function (x){
return new cljs.core.Keyword(null,"atom","atom",-397043653);
});})(G__30746_30847,G__30747_30848))
;
goog.object.set(G__30746_30847,G__30747_30848,G__30748_30849);

goog.object.set(clojure.data.EqualityPartition,"number",true);

var G__30749_30850 = clojure.data.equality_partition;
var G__30750_30851 = "number";
var G__30751_30852 = ((function (G__30749_30850,G__30750_30851){
return (function (x){
return new cljs.core.Keyword(null,"atom","atom",-397043653);
});})(G__30749_30850,G__30750_30851))
;
goog.object.set(G__30749_30850,G__30750_30851,G__30751_30852);

goog.object.set(clojure.data.EqualityPartition,"array",true);

var G__30753_30853 = clojure.data.equality_partition;
var G__30754_30854 = "array";
var G__30755_30855 = ((function (G__30753_30853,G__30754_30854){
return (function (x){
return new cljs.core.Keyword(null,"sequential","sequential",-1082983960);
});})(G__30753_30853,G__30754_30854))
;
goog.object.set(G__30753_30853,G__30754_30854,G__30755_30855);

goog.object.set(clojure.data.EqualityPartition,"function",true);

var G__30757_30856 = clojure.data.equality_partition;
var G__30758_30857 = "function";
var G__30759_30858 = ((function (G__30757_30856,G__30758_30857){
return (function (x){
return new cljs.core.Keyword(null,"atom","atom",-397043653);
});})(G__30757_30856,G__30758_30857))
;
goog.object.set(G__30757_30856,G__30758_30857,G__30759_30858);

goog.object.set(clojure.data.EqualityPartition,"boolean",true);

var G__30761_30860 = clojure.data.equality_partition;
var G__30762_30861 = "boolean";
var G__30763_30862 = ((function (G__30761_30860,G__30762_30861){
return (function (x){
return new cljs.core.Keyword(null,"atom","atom",-397043653);
});})(G__30761_30860,G__30762_30861))
;
goog.object.set(G__30761_30860,G__30762_30861,G__30763_30862);

goog.object.set(clojure.data.EqualityPartition,"_",true);

var G__30764_30864 = clojure.data.equality_partition;
var G__30765_30865 = "_";
var G__30766_30866 = ((function (G__30764_30864,G__30765_30865){
return (function (x){
if((((!((x == null))))?(((((x.cljs$lang$protocol_mask$partition0$ & (1024))) || ((cljs.core.PROTOCOL_SENTINEL === x.cljs$core$IMap$))))?true:(((!x.cljs$lang$protocol_mask$partition0$))?cljs.core.native_satisfies_QMARK_(cljs.core.IMap,x):false)):cljs.core.native_satisfies_QMARK_(cljs.core.IMap,x))){
return new cljs.core.Keyword(null,"map","map",1371690461);
} else {
if((((!((x == null))))?(((((x.cljs$lang$protocol_mask$partition0$ & (4096))) || ((cljs.core.PROTOCOL_SENTINEL === x.cljs$core$ISet$))))?true:(((!x.cljs$lang$protocol_mask$partition0$))?cljs.core.native_satisfies_QMARK_(cljs.core.ISet,x):false)):cljs.core.native_satisfies_QMARK_(cljs.core.ISet,x))){
return new cljs.core.Keyword(null,"set","set",304602554);
} else {
if((((!((x == null))))?(((((x.cljs$lang$protocol_mask$partition0$ & (16777216))) || ((cljs.core.PROTOCOL_SENTINEL === x.cljs$core$ISequential$))))?true:(((!x.cljs$lang$protocol_mask$partition0$))?cljs.core.native_satisfies_QMARK_(cljs.core.ISequential,x):false)):cljs.core.native_satisfies_QMARK_(cljs.core.ISequential,x))){
return new cljs.core.Keyword(null,"sequential","sequential",-1082983960);
} else {
return new cljs.core.Keyword(null,"atom","atom",-397043653);

}
}
}
});})(G__30764_30864,G__30765_30865))
;
goog.object.set(G__30764_30864,G__30765_30865,G__30766_30866);
goog.object.set(clojure.data.Diff,"null",true);

var G__30777_30867 = clojure.data.diff_similar;
var G__30778_30868 = "null";
var G__30779_30869 = ((function (G__30777_30867,G__30778_30868){
return (function (a,b){
return clojure.data.atom_diff(a,b);
});})(G__30777_30867,G__30778_30868))
;
goog.object.set(G__30777_30867,G__30778_30868,G__30779_30869);

goog.object.set(clojure.data.Diff,"string",true);

var G__30782_30870 = clojure.data.diff_similar;
var G__30783_30871 = "string";
var G__30784_30872 = ((function (G__30782_30870,G__30783_30871){
return (function (a,b){
return clojure.data.atom_diff(a,b);
});})(G__30782_30870,G__30783_30871))
;
goog.object.set(G__30782_30870,G__30783_30871,G__30784_30872);

goog.object.set(clojure.data.Diff,"number",true);

var G__30785_30876 = clojure.data.diff_similar;
var G__30786_30877 = "number";
var G__30787_30878 = ((function (G__30785_30876,G__30786_30877){
return (function (a,b){
return clojure.data.atom_diff(a,b);
});})(G__30785_30876,G__30786_30877))
;
goog.object.set(G__30785_30876,G__30786_30877,G__30787_30878);

goog.object.set(clojure.data.Diff,"array",true);

var G__30788_30879 = clojure.data.diff_similar;
var G__30789_30880 = "array";
var G__30790_30881 = ((function (G__30788_30879,G__30789_30880){
return (function (a,b){
return clojure.data.diff_sequential(a,b);
});})(G__30788_30879,G__30789_30880))
;
goog.object.set(G__30788_30879,G__30789_30880,G__30790_30881);

goog.object.set(clojure.data.Diff,"function",true);

var G__30791_30882 = clojure.data.diff_similar;
var G__30792_30883 = "function";
var G__30793_30884 = ((function (G__30791_30882,G__30792_30883){
return (function (a,b){
return clojure.data.atom_diff(a,b);
});})(G__30791_30882,G__30792_30883))
;
goog.object.set(G__30791_30882,G__30792_30883,G__30793_30884);

goog.object.set(clojure.data.Diff,"boolean",true);

var G__30806_30885 = clojure.data.diff_similar;
var G__30807_30886 = "boolean";
var G__30808_30887 = ((function (G__30806_30885,G__30807_30886){
return (function (a,b){
return clojure.data.atom_diff(a,b);
});})(G__30806_30885,G__30807_30886))
;
goog.object.set(G__30806_30885,G__30807_30886,G__30808_30887);

goog.object.set(clojure.data.Diff,"_",true);

var G__30818_30890 = clojure.data.diff_similar;
var G__30819_30891 = "_";
var G__30820_30892 = ((function (G__30818_30890,G__30819_30891){
return (function (a,b){
var fexpr__30826 = (function (){var G__30827 = clojure.data.equality_partition(a);
var G__30827__$1 = (((G__30827 instanceof cljs.core.Keyword))?G__30827.fqn:null);
switch (G__30827__$1) {
case "atom":
return clojure.data.atom_diff;

break;
case "set":
return clojure.data.diff_set;

break;
case "sequential":
return clojure.data.diff_sequential;

break;
case "map":
return clojure.data.diff_associative;

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__30827__$1)].join('')));

}
})();
return (fexpr__30826.cljs$core$IFn$_invoke$arity$2 ? fexpr__30826.cljs$core$IFn$_invoke$arity$2(a,b) : fexpr__30826.call(null,a,b));
});})(G__30818_30890,G__30819_30891))
;
goog.object.set(G__30818_30890,G__30819_30891,G__30820_30892);
/**
 * Recursively compares a and b, returning a tuple of
 *   [things-only-in-a things-only-in-b things-in-both].
 *   Comparison rules:
 * 
 *   * For equal a and b, return [nil nil a].
 *   * Maps are subdiffed where keys match and values differ.
 *   * Sets are never subdiffed.
 *   * All sequential things are treated as associative collections
 *  by their indexes, with results returned as vectors.
 *   * Everything else (including strings!) is treated as
 *  an atom and compared for equality.
 */
clojure.data.diff = (function clojure$data$diff(a,b){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(a,b)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,null,a], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(clojure.data.equality_partition(a),clojure.data.equality_partition(b))){
return clojure.data.diff_similar(a,b);
} else {
return clojure.data.atom_diff(a,b);
}
}
});

//# sourceMappingURL=clojure.data.js.map
