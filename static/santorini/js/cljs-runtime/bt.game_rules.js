goog.provide('bt.game_rules');
goog.require('cljs.core');
bt.game_rules.sample_board = new cljs.core.PersistentArrayMap(null, 2, [(0),new cljs.core.PersistentArrayMap(null, 4, [(0),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"height","height",1025178622),(4)], null),(1),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"height","height",1025178622),(3)], null),(2),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"height","height",1025178622),(2),new cljs.core.Keyword(null,"occupant","occupant",1823396248),new cljs.core.Keyword("team","one","team/one",942492413)], null),(3),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"height","height",1025178622),(1)], null)], null),(1),new cljs.core.PersistentArrayMap(null, 3, [(0),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"occupant","occupant",1823396248),new cljs.core.Keyword("team","one","team/one",942492413)], null),(1),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"occupant","occupant",1823396248),new cljs.core.Keyword("team","two","team/two",522109688)], null),(3),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"occupant","occupant",1823396248),new cljs.core.Keyword("team","two","team/two",522109688)], null)], null)], null);
bt.game_rules.sample_board_pieces_placed = new cljs.core.PersistentArrayMap(null, 2, [(1),new cljs.core.PersistentArrayMap(null, 2, [(0),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"occupant","occupant",1823396248),new cljs.core.Keyword("team","one","team/one",942492413)], null),(1),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"occupant","occupant",1823396248),new cljs.core.Keyword("team","one","team/one",942492413)], null)], null),(2),new cljs.core.PersistentArrayMap(null, 2, [(2),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"occupant","occupant",1823396248),new cljs.core.Keyword("team","two","team/two",522109688)], null),(3),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"occupant","occupant",1823396248),new cljs.core.Keyword("team","two","team/two",522109688)], null)], null)], null);
bt.game_rules.height_at = (function bt$game_rules$height_at(board,p__105625){
var vec__105626 = p__105625;
var x = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105626,(0),null);
var y = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105626,(1),null);
var or__4131__auto__ = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(board,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y,new cljs.core.Keyword(null,"height","height",1025178622)], null));
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (0);
}
});
/**
 * Elements are drawn into to the scene
 */
bt.game_rules.__GT_elements = (function bt$game_rules$__GT_elements(board){
return cljs.core.vec(cljs.core.conj.cljs$core$IFn$_invoke$arity$2((function (){var iter__4523__auto__ = (function bt$game_rules$__GT_elements_$_iter__105629(s__105630){
return (new cljs.core.LazySeq(null,(function (){
var s__105630__$1 = s__105630;
while(true){
var temp__5720__auto__ = cljs.core.seq(s__105630__$1);
if(temp__5720__auto__){
var xs__6277__auto__ = temp__5720__auto__;
var x = cljs.core.first(xs__6277__auto__);
var iterys__4519__auto__ = ((function (s__105630__$1,x,xs__6277__auto__,temp__5720__auto__){
return (function bt$game_rules$__GT_elements_$_iter__105629_$_iter__105631(s__105632){
return (new cljs.core.LazySeq(null,((function (s__105630__$1,x,xs__6277__auto__,temp__5720__auto__){
return (function (){
var s__105632__$1 = s__105632;
while(true){
var temp__5720__auto____$1 = cljs.core.seq(s__105632__$1);
if(temp__5720__auto____$1){
var xs__6277__auto____$1 = temp__5720__auto____$1;
var y = cljs.core.first(xs__6277__auto____$1);
var iterys__4519__auto__ = ((function (s__105632__$1,s__105630__$1,y,xs__6277__auto____$1,temp__5720__auto____$1,x,xs__6277__auto__,temp__5720__auto__){
return (function bt$game_rules$__GT_elements_$_iter__105629_$_iter__105631_$_iter__105633(s__105634){
return (new cljs.core.LazySeq(null,((function (s__105632__$1,s__105630__$1,y,xs__6277__auto____$1,temp__5720__auto____$1,x,xs__6277__auto__,temp__5720__auto__){
return (function (){
var s__105634__$1 = s__105634;
while(true){
var temp__5720__auto____$2 = cljs.core.seq(s__105634__$1);
if(temp__5720__auto____$2){
var s__105634__$2 = temp__5720__auto____$2;
if(cljs.core.chunked_seq_QMARK_(s__105634__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__105634__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__105636 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__105635 = (0);
while(true){
if((i__105635 < size__4522__auto__)){
var vec__105637 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__105635);
var e_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105637,(0),null);
var e_val = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105637,(1),null);
cljs.core.chunk_append(b__105636,((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(e_key,new cljs.core.Keyword(null,"height","height",1025178622)))?new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"element","element",1974019749),new cljs.core.Keyword("e","block","e/block",664686109),new cljs.core.Keyword(null,"x","x",2099068185),x,new cljs.core.Keyword(null,"y","y",-1757859776),y,new cljs.core.Keyword(null,"height","height",1025178622),e_val], null):new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"element","element",1974019749),new cljs.core.Keyword("e","player","e/player",-97687463),new cljs.core.Keyword(null,"x","x",2099068185),x,new cljs.core.Keyword(null,"y","y",-1757859776),y,new cljs.core.Keyword(null,"team","team",1355747699),e_val], null)
));

var G__105709 = (i__105635 + (1));
i__105635 = G__105709;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__105636),bt$game_rules$__GT_elements_$_iter__105629_$_iter__105631_$_iter__105633(cljs.core.chunk_rest(s__105634__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__105636),null);
}
} else {
var vec__105640 = cljs.core.first(s__105634__$2);
var e_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105640,(0),null);
var e_val = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105640,(1),null);
return cljs.core.cons(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(e_key,new cljs.core.Keyword(null,"height","height",1025178622)))?new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"element","element",1974019749),new cljs.core.Keyword("e","block","e/block",664686109),new cljs.core.Keyword(null,"x","x",2099068185),x,new cljs.core.Keyword(null,"y","y",-1757859776),y,new cljs.core.Keyword(null,"height","height",1025178622),e_val], null):new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"element","element",1974019749),new cljs.core.Keyword("e","player","e/player",-97687463),new cljs.core.Keyword(null,"x","x",2099068185),x,new cljs.core.Keyword(null,"y","y",-1757859776),y,new cljs.core.Keyword(null,"team","team",1355747699),e_val], null)
),bt$game_rules$__GT_elements_$_iter__105629_$_iter__105631_$_iter__105633(cljs.core.rest(s__105634__$2)));
}
} else {
return null;
}
break;
}
});})(s__105632__$1,s__105630__$1,y,xs__6277__auto____$1,temp__5720__auto____$1,x,xs__6277__auto__,temp__5720__auto__))
,null,null));
});})(s__105632__$1,s__105630__$1,y,xs__6277__auto____$1,temp__5720__auto____$1,x,xs__6277__auto__,temp__5720__auto__))
;
var fs__4520__auto__ = cljs.core.seq(iterys__4519__auto__(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(board,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null))));
if(fs__4520__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4520__auto__,bt$game_rules$__GT_elements_$_iter__105629_$_iter__105631(cljs.core.rest(s__105632__$1)));
} else {
var G__105718 = cljs.core.rest(s__105632__$1);
s__105632__$1 = G__105718;
continue;
}
} else {
return null;
}
break;
}
});})(s__105630__$1,x,xs__6277__auto__,temp__5720__auto__))
,null,null));
});})(s__105630__$1,x,xs__6277__auto__,temp__5720__auto__))
;
var fs__4520__auto__ = cljs.core.seq(iterys__4519__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$1((5))));
if(fs__4520__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4520__auto__,bt$game_rules$__GT_elements_$_iter__105629(cljs.core.rest(s__105630__$1)));
} else {
var G__105720 = cljs.core.rest(s__105630__$1);
s__105630__$1 = G__105720;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$1((5)));
})(),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"element","element",1974019749),new cljs.core.Keyword("e","floor","e/floor",1882040922)], null)));
});
bt.game_rules.in_bounds_QMARK_ = (function bt$game_rules$in_bounds_QMARK_(x){
return ((((4) >= x)) && ((x >= (0))));
});
bt.game_rules.neighbors = (function bt$game_rules$neighbors(x,y){
return cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentHashSet.createAsIfByAssoc([new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null)]),(function (){var iter__4523__auto__ = (function bt$game_rules$neighbors_$_iter__105643(s__105644){
return (new cljs.core.LazySeq(null,(function (){
var s__105644__$1 = s__105644;
while(true){
var temp__5720__auto__ = cljs.core.seq(s__105644__$1);
if(temp__5720__auto__){
var xs__6277__auto__ = temp__5720__auto__;
var x_SINGLEQUOTE_ = cljs.core.first(xs__6277__auto__);
var iterys__4519__auto__ = ((function (s__105644__$1,x_SINGLEQUOTE_,xs__6277__auto__,temp__5720__auto__){
return (function bt$game_rules$neighbors_$_iter__105643_$_iter__105645(s__105646){
return (new cljs.core.LazySeq(null,((function (s__105644__$1,x_SINGLEQUOTE_,xs__6277__auto__,temp__5720__auto__){
return (function (){
var s__105646__$1 = s__105646;
while(true){
var temp__5720__auto____$1 = cljs.core.seq(s__105646__$1);
if(temp__5720__auto____$1){
var s__105646__$2 = temp__5720__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__105646__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__105646__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__105648 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__105647 = (0);
while(true){
if((i__105647 < size__4522__auto__)){
var y_SINGLEQUOTE_ = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__105647);
cljs.core.chunk_append(b__105648,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x_SINGLEQUOTE_,y_SINGLEQUOTE_], null));

var G__105724 = (i__105647 + (1));
i__105647 = G__105724;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__105648),bt$game_rules$neighbors_$_iter__105643_$_iter__105645(cljs.core.chunk_rest(s__105646__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__105648),null);
}
} else {
var y_SINGLEQUOTE_ = cljs.core.first(s__105646__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x_SINGLEQUOTE_,y_SINGLEQUOTE_], null),bt$game_rules$neighbors_$_iter__105643_$_iter__105645(cljs.core.rest(s__105646__$2)));
}
} else {
return null;
}
break;
}
});})(s__105644__$1,x_SINGLEQUOTE_,xs__6277__auto__,temp__5720__auto__))
,null,null));
});})(s__105644__$1,x_SINGLEQUOTE_,xs__6277__auto__,temp__5720__auto__))
;
var fs__4520__auto__ = cljs.core.seq(iterys__4519__auto__(cljs.core.filter.cljs$core$IFn$_invoke$arity$2(bt.game_rules.in_bounds_QMARK_,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(y - (1)),y,(y + (1))], null))));
if(fs__4520__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4520__auto__,bt$game_rules$neighbors_$_iter__105643(cljs.core.rest(s__105644__$1)));
} else {
var G__105727 = cljs.core.rest(s__105644__$1);
s__105644__$1 = G__105727;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(cljs.core.filter.cljs$core$IFn$_invoke$arity$2(bt.game_rules.in_bounds_QMARK_,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(x - (1)),x,(x + (1))], null)));
})());
});
bt.game_rules.moves = (function bt$game_rules$moves(board,player_x,player_y){
var neighbors = bt.game_rules.neighbors(player_x,player_y);
var player_height = bt.game_rules.height_at(board,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [player_x,player_y], null));
return cljs.core.filter.cljs$core$IFn$_invoke$arity$2(((function (neighbors,player_height){
return (function (p__105649){
var vec__105650 = p__105649;
var x = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105650,(0),null);
var y = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105650,(1),null);
return ((player_height + (1)) >= bt.game_rules.height_at(board,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null)));
});})(neighbors,player_height))
,neighbors);
});
bt.game_rules.state__GT_info = (function bt$game_rules$state__GT_info(state){
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"team-one-select-worker","team-one-select-worker",1952466786),new cljs.core.Keyword(null,"place-first","place-first",735813509),new cljs.core.Keyword(null,"team-one-move-worker","team-one-move-worker",-1230853146),new cljs.core.Keyword(null,"team-two-move-worker","team-two-move-worker",1616540646),new cljs.core.Keyword(null,"place-third","place-third",-1344073910),new cljs.core.Keyword(null,"team-two-build","team-two-build",-937658130),new cljs.core.Keyword(null,"place-fourth","place-fourth",1641448114),new cljs.core.Keyword(null,"place-second","place-second",1693452979),new cljs.core.Keyword(null,"team-one-build","team-one-build",-2018106924),new cljs.core.Keyword(null,"team-two-select-worker","team-two-select-worker",1855208632)],[new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"team","team",1355747699),new cljs.core.Keyword("team","one","team/one",942492413),new cljs.core.Keyword(null,"mode","mode",654403691),new cljs.core.Keyword("state","select","state/select",1525666096),new cljs.core.Keyword(null,"title","title",636505583),"Player one, select a worker to move"], null),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"team","team",1355747699),new cljs.core.Keyword("team","one","team/one",942492413),new cljs.core.Keyword(null,"mode","mode",654403691),new cljs.core.Keyword("state","place","state/place",-674642505),new cljs.core.Keyword(null,"title","title",636505583),"Player one, place a piece"], null),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"team","team",1355747699),new cljs.core.Keyword("team","one","team/one",942492413),new cljs.core.Keyword(null,"mode","mode",654403691),new cljs.core.Keyword("state","move","state/move",-2042547590),new cljs.core.Keyword(null,"title","title",636505583),"Player one, select a space to move to"], null),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"team","team",1355747699),new cljs.core.Keyword("team","two","team/two",522109688),new cljs.core.Keyword(null,"mode","mode",654403691),new cljs.core.Keyword("state","move","state/move",-2042547590),new cljs.core.Keyword(null,"title","title",636505583),"Player two, select a space to move to"], null),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"team","team",1355747699),new cljs.core.Keyword("team","two","team/two",522109688),new cljs.core.Keyword(null,"mode","mode",654403691),new cljs.core.Keyword("state","place","state/place",-674642505),new cljs.core.Keyword(null,"title","title",636505583),"Player two, place a piece"], null),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"team","team",1355747699),new cljs.core.Keyword("team","two","team/two",522109688),new cljs.core.Keyword(null,"mode","mode",654403691),new cljs.core.Keyword("state","build","state/build",821444545),new cljs.core.Keyword(null,"title","title",636505583),"Player two, select a space to build"], null),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"team","team",1355747699),new cljs.core.Keyword("team","two","team/two",522109688),new cljs.core.Keyword(null,"mode","mode",654403691),new cljs.core.Keyword("state","place","state/place",-674642505),new cljs.core.Keyword(null,"title","title",636505583),"Player two, place your second piece"], null),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"team","team",1355747699),new cljs.core.Keyword("team","one","team/one",942492413),new cljs.core.Keyword(null,"mode","mode",654403691),new cljs.core.Keyword("state","place","state/place",-674642505),new cljs.core.Keyword(null,"title","title",636505583),"Player one, place your second piece"], null),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"team","team",1355747699),new cljs.core.Keyword("team","one","team/one",942492413),new cljs.core.Keyword(null,"mode","mode",654403691),new cljs.core.Keyword("state","build","state/build",821444545),new cljs.core.Keyword(null,"title","title",636505583),"Player one, select a space to build"], null),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"team","team",1355747699),new cljs.core.Keyword("team","two","team/two",522109688),new cljs.core.Keyword(null,"mode","mode",654403691),new cljs.core.Keyword("state","select","state/select",1525666096),new cljs.core.Keyword(null,"title","title",636505583),"Player two, select a worker to move"], null)]),state,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"team","team",1355747699),new cljs.core.Keyword("team","none","team/none",1338238689),new cljs.core.Keyword(null,"mode","mode",654403691),new cljs.core.Keyword("state","none","state/none",1446013101),new cljs.core.Keyword(null,"title","title",636505583),["state: ",cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([state], 0))," not found."].join('')], null));
});
bt.game_rules.game_states = (function (){var states = new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"place-first","place-first",735813509),new cljs.core.Keyword(null,"place-second","place-second",1693452979),new cljs.core.Keyword(null,"place-third","place-third",-1344073910),new cljs.core.Keyword(null,"place-fourth","place-fourth",1641448114),new cljs.core.Keyword(null,"team-one-select-worker","team-one-select-worker",1952466786),new cljs.core.Keyword(null,"team-one-move-worker","team-one-move-worker",-1230853146),new cljs.core.Keyword(null,"team-one-build","team-one-build",-2018106924),new cljs.core.Keyword(null,"team-two-select-worker","team-two-select-worker",1855208632),new cljs.core.Keyword(null,"team-two-move-worker","team-two-move-worker",1616540646),new cljs.core.Keyword(null,"team-two-build","team-two-build",-937658130)], null);
var destination_states = cljs.core.concat.cljs$core$IFn$_invoke$arity$2(cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),states),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"team-one-select-worker","team-one-select-worker",1952466786)], null));
return cljs.core.zipmap(states,destination_states);
})();
bt.game_rules.advance = (function bt$game_rules$advance(current_state){
return (bt.game_rules.game_states.cljs$core$IFn$_invoke$arity$1 ? bt.game_rules.game_states.cljs$core$IFn$_invoke$arity$1(current_state) : bt.game_rules.game_states.call(null,current_state));
});
bt.game_rules.occupied_QMARK_ = (function bt$game_rules$occupied_QMARK_(board,x,y){
return new cljs.core.Keyword(null,"occupant","occupant",1823396248).cljs$core$IFn$_invoke$arity$1(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(board,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null)));
});
bt.game_rules.unoccupied_QMARK_ = (function bt$game_rules$unoccupied_QMARK_(board,p__105653){
var vec__105654 = p__105653;
var x = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105654,(0),null);
var y = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105654,(1),null);
return cljs.core.not(bt.game_rules.occupied_QMARK_(board,x,y));
});
bt.game_rules.place = (function bt$game_rules$place(board,x,y,team){
if(cljs.core.truth_(bt.game_rules.occupied_QMARK_(board,x,y))){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(x),", ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(y)," is occupied. Try again."].join('')], null);
} else {
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"board","board",-1907017633),cljs.core.assoc_in(board,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y,new cljs.core.Keyword(null,"occupant","occupant",1823396248)], null),team)], null);
}
});
bt.game_rules.select = (function bt$game_rules$select(board,x,y,team){
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(team,cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(board,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y,new cljs.core.Keyword(null,"occupant","occupant",1823396248)], null)))){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(x),", ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(y)," is not occupied by your team. ","Select one of your pieces."].join('')], null);
} else {
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"board","board",-1907017633),board,new cljs.core.Keyword(null,"selected","selected",574897764),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null)], null);
}
});
bt.game_rules.move_worker = (function bt$game_rules$move_worker(board,p__105657,p__105658){
var vec__105659 = p__105657;
var from_x = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105659,(0),null);
var from_y = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105659,(1),null);
var vec__105662 = p__105658;
var to_x = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105662,(0),null);
var to_y = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105662,(1),null);
if(cljs.core.not(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(board,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [from_x,from_y,new cljs.core.Keyword(null,"occupant","occupant",1823396248)], null)))){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error","error",-978969032),"There's noone at the  selected space (uh oh)."], null);
} else {
if(cljs.core.truth_(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(board,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [to_x,to_y,new cljs.core.Keyword(null,"occupant","occupant",1823396248)], null)))){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error","error",-978969032),["There's already someone located at (",cljs.core.str.cljs$core$IFn$_invoke$arity$1(from_x),", ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(to_x),")."].join('')], null);
} else {
if(cljs.core.not((function (){var G__105666 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [to_x,to_y], null);
var fexpr__105665 = cljs.core.set(bt.game_rules.moves(board,from_x,from_y));
return (fexpr__105665.cljs$core$IFn$_invoke$arity$1 ? fexpr__105665.cljs$core$IFn$_invoke$arity$1(G__105666) : fexpr__105665.call(null,G__105666));
})())){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error","error",-978969032),"Can't move here"], null);
} else {
var temp__5718__auto__ = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(board,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [from_x,from_y,new cljs.core.Keyword(null,"occupant","occupant",1823396248)], null));
if(cljs.core.truth_(temp__5718__auto__)){
var worker_team = temp__5718__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"board","board",-1907017633),cljs.core.update_in.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc_in(board,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [to_x,to_y,new cljs.core.Keyword(null,"occupant","occupant",1823396248)], null),worker_team),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [from_x,from_y], null),cljs.core.dissoc,new cljs.core.Keyword(null,"occupant","occupant",1823396248))], null);
} else {
return null;
}

}
}
}
});
bt.game_rules.check_build = (function bt$game_rules$check_build(board,p__105667,p__105668){
var vec__105669 = p__105667;
var from_x = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105669,(0),null);
var from_y = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105669,(1),null);
var vec__105672 = p__105668;
var to_x = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105672,(0),null);
var to_y = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105672,(1),null);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(board,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [to_x,to_y,new cljs.core.Keyword(null,"height","height",1025178622)], null)),(4))){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error","error",-978969032),"Already Domed"], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [from_x,from_y], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [to_x,to_y], null))){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error","error",-978969032),"Can't build under yourself"], null);
} else {
if(cljs.core.truth_(bt.game_rules.occupied_QMARK_(board,to_x,to_y))){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error","error",-978969032),"Can't build under another player"], null);
} else {
if(cljs.core.not((function (){var G__105676 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [to_x,to_y], null);
var fexpr__105675 = cljs.core.set(bt.game_rules.neighbors(from_x,from_y));
return (fexpr__105675.cljs$core$IFn$_invoke$arity$1 ? fexpr__105675.cljs$core$IFn$_invoke$arity$1(G__105676) : fexpr__105675.call(null,G__105676));
})())){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error","error",-978969032),"Too far away"], null);
} else {
return null;

}
}
}
}
});
bt.game_rules.neighbor_of = (function bt$game_rules$neighbor_of(p__105677,to){
var vec__105678 = p__105677;
var from_x = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105678,(0),null);
var from_y = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105678,(1),null);
var fexpr__105681 = cljs.core.set(bt.game_rules.neighbors(from_x,from_y));
return (fexpr__105681.cljs$core$IFn$_invoke$arity$1 ? fexpr__105681.cljs$core$IFn$_invoke$arity$1(to) : fexpr__105681.call(null,to));
});
bt.game_rules.can_select = (function bt$game_rules$can_select(board,team,p__105682){
var vec__105683 = p__105682;
var x = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105683,(0),null);
var y = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105683,(1),null);
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(team,cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(board,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y,new cljs.core.Keyword(null,"occupant","occupant",1823396248)], null)));
});
bt.game_rules.can_move_to = (function bt$game_rules$can_move_to(board,from,to){
var and__4120__auto__ = bt.game_rules.unoccupied_QMARK_(board,to);
if(and__4120__auto__){
var and__4120__auto____$1 = bt.game_rules.neighbor_of(from,to);
if(cljs.core.truth_(and__4120__auto____$1)){
return ((bt.game_rules.height_at(board,from) + (1)) >= bt.game_rules.height_at(board,to));
} else {
return and__4120__auto____$1;
}
} else {
return and__4120__auto__;
}
});
bt.game_rules.can_build = (function bt$game_rules$can_build(board,from,to){
var and__4120__auto__ = bt.game_rules.unoccupied_QMARK_(board,to);
if(and__4120__auto__){
var and__4120__auto____$1 = bt.game_rules.neighbor_of(from,to);
if(cljs.core.truth_(and__4120__auto____$1)){
return ((4) >= bt.game_rules.height_at(board,to));
} else {
return and__4120__auto____$1;
}
} else {
return and__4120__auto__;
}
});
bt.game_rules.build = (function bt$game_rules$build(board,p__105687,p__105688){
var vec__105689 = p__105687;
var from_x = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105689,(0),null);
var from_y = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105689,(1),null);
var vec__105692 = p__105688;
var to_x = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105692,(0),null);
var to_y = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105692,(1),null);
var temp__5718__auto__ = bt.game_rules.check_build(board,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [from_x,from_y], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [to_x,to_y], null));
if(cljs.core.truth_(temp__5718__auto__)){
var error = temp__5718__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"error","error",-978969032),error], null);
} else {
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"board","board",-1907017633),cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(board,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [to_x,to_y,new cljs.core.Keyword(null,"height","height",1025178622)], null),((function (temp__5718__auto__,vec__105689,from_x,from_y,vec__105692,to_x,to_y){
return (function (p1__105686_SHARP_){
var x__4222__auto__ = (4);
var y__4223__auto__ = (p1__105686_SHARP_ + (1));
return ((x__4222__auto__ < y__4223__auto__) ? x__4222__auto__ : y__4223__auto__);
});})(temp__5718__auto__,vec__105689,from_x,from_y,vec__105692,to_x,to_y))
)], null);
}
});
bt.game_rules.over_QMARK_ = (function bt$game_rules$over_QMARK_(board){
return cljs.core.first(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,(function (){var iter__4523__auto__ = (function bt$game_rules$over_QMARK__$_iter__105695(s__105696){
return (new cljs.core.LazySeq(null,(function (){
var s__105696__$1 = s__105696;
while(true){
var temp__5720__auto__ = cljs.core.seq(s__105696__$1);
if(temp__5720__auto__){
var xs__6277__auto__ = temp__5720__auto__;
var x = cljs.core.first(xs__6277__auto__);
var iterys__4519__auto__ = ((function (s__105696__$1,x,xs__6277__auto__,temp__5720__auto__){
return (function bt$game_rules$over_QMARK__$_iter__105695_$_iter__105697(s__105698){
return (new cljs.core.LazySeq(null,((function (s__105696__$1,x,xs__6277__auto__,temp__5720__auto__){
return (function (){
var s__105698__$1 = s__105698;
while(true){
var temp__5720__auto____$1 = cljs.core.seq(s__105698__$1);
if(temp__5720__auto____$1){
var s__105698__$2 = temp__5720__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__105698__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__105698__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__105700 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__105699 = (0);
while(true){
if((i__105699 < size__4522__auto__)){
var y = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__105699);
cljs.core.chunk_append(b__105700,(function (){var map__105701 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(board,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null));
var map__105701__$1 = (((((!((map__105701 == null))))?(((((map__105701.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105701.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105701):map__105701);
var occupant = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105701__$1,new cljs.core.Keyword(null,"occupant","occupant",1823396248));
var height = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105701__$1,new cljs.core.Keyword(null,"height","height",1025178622));
if(cljs.core.truth_((function (){var and__4120__auto__ = occupant;
if(cljs.core.truth_(and__4120__auto__)){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((3),height);
} else {
return and__4120__auto__;
}
})())){
return occupant;
} else {
return null;
}
})());

var G__105783 = (i__105699 + (1));
i__105699 = G__105783;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__105700),bt$game_rules$over_QMARK__$_iter__105695_$_iter__105697(cljs.core.chunk_rest(s__105698__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__105700),null);
}
} else {
var y = cljs.core.first(s__105698__$2);
return cljs.core.cons((function (){var map__105703 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(board,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null));
var map__105703__$1 = (((((!((map__105703 == null))))?(((((map__105703.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105703.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105703):map__105703);
var occupant = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105703__$1,new cljs.core.Keyword(null,"occupant","occupant",1823396248));
var height = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105703__$1,new cljs.core.Keyword(null,"height","height",1025178622));
if(cljs.core.truth_((function (){var and__4120__auto__ = occupant;
if(cljs.core.truth_(and__4120__auto__)){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((3),height);
} else {
return and__4120__auto__;
}
})())){
return occupant;
} else {
return null;
}
})(),bt$game_rules$over_QMARK__$_iter__105695_$_iter__105697(cljs.core.rest(s__105698__$2)));
}
} else {
return null;
}
break;
}
});})(s__105696__$1,x,xs__6277__auto__,temp__5720__auto__))
,null,null));
});})(s__105696__$1,x,xs__6277__auto__,temp__5720__auto__))
;
var fs__4520__auto__ = cljs.core.seq(iterys__4519__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$1((4))));
if(fs__4520__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4520__auto__,bt$game_rules$over_QMARK__$_iter__105695(cljs.core.rest(s__105696__$1)));
} else {
var G__105794 = cljs.core.rest(s__105696__$1);
s__105696__$1 = G__105794;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$1((4)));
})()));
});

//# sourceMappingURL=bt.game_rules.js.map
