goog.provide('bt.core');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('re_frame.core');
goog.require('bt.db');
goog.require('bt.game_rules');
goog.require('bt.css_gen');
bt.core.__GT_em = (function bt$core$__GT_em(n){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(((5) * n)),"em"].join('');
});
bt.core.position = (function bt$core$position(x,y){
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"top","top",-1856271961),bt.core.__GT_em(x),new cljs.core.Keyword(null,"left","left",-399115937),bt.core.__GT_em(y)], null);
});
if((typeof bt !== 'undefined') && (typeof bt.core !== 'undefined') && (typeof bt.core.draw !== 'undefined')){
} else {
bt.core.draw = (function (){var method_table__4613__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__4614__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var method_cache__4615__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__4616__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__4617__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),(function (){var fexpr__105796 = cljs.core.get_global_hierarchy;
return (fexpr__105796.cljs$core$IFn$_invoke$arity$0 ? fexpr__105796.cljs$core$IFn$_invoke$arity$0() : fexpr__105796.call(null));
})());
return (new cljs.core.MultiFn(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2("bt.core","draw"),((function (method_table__4613__auto__,prefer_table__4614__auto__,method_cache__4615__auto__,cached_hierarchy__4616__auto__,hierarchy__4617__auto__){
return (function (e){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"element","element",1974019749).cljs$core$IFn$_invoke$arity$1(e),new cljs.core.Keyword(null,"height","height",1025178622).cljs$core$IFn$_invoke$arity$2(e,new cljs.core.Keyword("height","no-height","height/no-height",2128392696))], null);
});})(method_table__4613__auto__,prefer_table__4614__auto__,method_cache__4615__auto__,cached_hierarchy__4616__auto__,hierarchy__4617__auto__))
,new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__4617__auto__,method_table__4613__auto__,prefer_table__4614__auto__,method_cache__4615__auto__,cached_hierarchy__4616__auto__));
})();
}
bt.core.__GT_block = (function bt$core$__GT_block(class$,p__105797,p__105798){
var map__105799 = p__105797;
var map__105799__$1 = (((((!((map__105799 == null))))?(((((map__105799.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105799.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105799):map__105799);
var element = map__105799__$1;
var x = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105799__$1,new cljs.core.Keyword(null,"x","x",2099068185));
var y = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105799__$1,new cljs.core.Keyword(null,"y","y",-1757859776));
var map__105800 = p__105798;
var map__105800__$1 = (((((!((map__105800 == null))))?(((((map__105800.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105800.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105800):map__105800);
var turn_info = map__105800__$1;
var game_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105800__$1,new cljs.core.Keyword(null,"game-state","game-state",935682735));
var mode = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105800__$1,new cljs.core.Keyword(null,"mode","mode",654403691));
var selected = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105800__$1,new cljs.core.Keyword(null,"selected","selected",574897764));
var board = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105800__$1,new cljs.core.Keyword(null,"board","board",-1907017633));
var team = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105800__$1,new cljs.core.Keyword(null,"team","team",1355747699));
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.block","div.block",1082647483),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"key","key",-1516042587),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(x),"_",cljs.core.str.cljs$core$IFn$_invoke$arity$1(y),"_",cljs.core.str.cljs$core$IFn$_invoke$arity$1(class$)].join(''),new cljs.core.Keyword(null,"class","class",-2030961996),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [class$,((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null),selected))?"selected":null),(cljs.core.truth_((function (){var and__4120__auto__ = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword("state","move","state/move",-2042547590));
if(and__4120__auto__){
return bt.game_rules.can_move_to(board,selected,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null));
} else {
return and__4120__auto__;
}
})())?"canmoveto":null),(cljs.core.truth_((function (){var and__4120__auto__ = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword("state","build","state/build",821444545));
if(and__4120__auto__){
return bt.game_rules.can_build(board,selected,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null));
} else {
return and__4120__auto__;
}
})())?"canbuild":null),((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword("state","select","state/select",1525666096))) && (bt.game_rules.can_select(board,team,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null)))))?"canselect":null)], null),new cljs.core.Keyword(null,"style","style",-496642736),bt.core.position(x,y),new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (map__105799,map__105799__$1,element,x,y,map__105800,map__105800__$1,turn_info,game_state,mode,selected,board,team){
return (function (){
var G__105803 = new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","tile-click","bt.db/tile-click",-839277672),x,y,turn_info], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__105803) : re_frame.core.dispatch.call(null,G__105803));
});})(map__105799,map__105799__$1,element,x,y,map__105800,map__105800__$1,turn_info,game_state,mode,selected,board,team))
], null)], null);
});
bt.core.draw.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("e","floor","e/floor",1882040922),new cljs.core.Keyword("height","no-height","height/no-height",2128392696)], null),(function (p__105804,p__105805){
var map__105806 = p__105804;
var map__105806__$1 = (((((!((map__105806 == null))))?(((((map__105806.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105806.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105806):map__105806);
var element = map__105806__$1;
var x = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105806__$1,new cljs.core.Keyword(null,"x","x",2099068185));
var y = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105806__$1,new cljs.core.Keyword(null,"y","y",-1757859776));
var map__105807 = p__105805;
var map__105807__$1 = (((((!((map__105807 == null))))?(((((map__105807.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105807.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105807):map__105807);
var turn_info = map__105807__$1;
var game_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105807__$1,new cljs.core.Keyword(null,"game-state","game-state",935682735));
var selected = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105807__$1,new cljs.core.Keyword(null,"selected","selected",574897764));
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"<>","<>",1280186386),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),"_why_ this need a key REACT?"], null)], null),cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,(function (){var iter__4523__auto__ = ((function (map__105806,map__105806__$1,element,x,y,map__105807,map__105807__$1,turn_info,game_state,selected){
return (function bt$core$iter__105810(s__105811){
return (new cljs.core.LazySeq(null,((function (map__105806,map__105806__$1,element,x,y,map__105807,map__105807__$1,turn_info,game_state,selected){
return (function (){
var s__105811__$1 = s__105811;
while(true){
var temp__5720__auto__ = cljs.core.seq(s__105811__$1);
if(temp__5720__auto__){
var xs__6277__auto__ = temp__5720__auto__;
var x__$1 = cljs.core.first(xs__6277__auto__);
var iterys__4519__auto__ = ((function (s__105811__$1,x__$1,xs__6277__auto__,temp__5720__auto__,map__105806,map__105806__$1,element,x,y,map__105807,map__105807__$1,turn_info,game_state,selected){
return (function bt$core$iter__105810_$_iter__105812(s__105813){
return (new cljs.core.LazySeq(null,((function (s__105811__$1,x__$1,xs__6277__auto__,temp__5720__auto__,map__105806,map__105806__$1,element,x,y,map__105807,map__105807__$1,turn_info,game_state,selected){
return (function (){
var s__105813__$1 = s__105813;
while(true){
var temp__5720__auto____$1 = cljs.core.seq(s__105813__$1);
if(temp__5720__auto____$1){
var s__105813__$2 = temp__5720__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__105813__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__105813__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__105815 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__105814 = (0);
while(true){
if((i__105814 < size__4522__auto__)){
var y__$1 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__105814);
cljs.core.chunk_append(b__105815,((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((0),new cljs.core.Keyword(null,"height","height",1025178622).cljs$core$IFn$_invoke$arity$1(cljs.core.deref((function (){var G__105816 = new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","cell-info","bt.db/cell-info",1819284458),x__$1,y__$1], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__105816) : re_frame.core.subscribe.call(null,G__105816));
})()))))?bt.core.__GT_block("floor",cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(element,new cljs.core.Keyword(null,"x","x",2099068185),x__$1,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776),y__$1], 0)),turn_info):null));

var G__105897 = (i__105814 + (1));
i__105814 = G__105897;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__105815),bt$core$iter__105810_$_iter__105812(cljs.core.chunk_rest(s__105813__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__105815),null);
}
} else {
var y__$1 = cljs.core.first(s__105813__$2);
return cljs.core.cons(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((0),new cljs.core.Keyword(null,"height","height",1025178622).cljs$core$IFn$_invoke$arity$1(cljs.core.deref((function (){var G__105821 = new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","cell-info","bt.db/cell-info",1819284458),x__$1,y__$1], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__105821) : re_frame.core.subscribe.call(null,G__105821));
})()))))?bt.core.__GT_block("floor",cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(element,new cljs.core.Keyword(null,"x","x",2099068185),x__$1,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776),y__$1], 0)),turn_info):null),bt$core$iter__105810_$_iter__105812(cljs.core.rest(s__105813__$2)));
}
} else {
return null;
}
break;
}
});})(s__105811__$1,x__$1,xs__6277__auto__,temp__5720__auto__,map__105806,map__105806__$1,element,x,y,map__105807,map__105807__$1,turn_info,game_state,selected))
,null,null));
});})(s__105811__$1,x__$1,xs__6277__auto__,temp__5720__auto__,map__105806,map__105806__$1,element,x,y,map__105807,map__105807__$1,turn_info,game_state,selected))
;
var fs__4520__auto__ = cljs.core.seq(iterys__4519__auto__(new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [(0),(1),(2),(3),(4)], null)));
if(fs__4520__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4520__auto__,bt$core$iter__105810(cljs.core.rest(s__105811__$1)));
} else {
var G__105898 = cljs.core.rest(s__105811__$1);
s__105811__$1 = G__105898;
continue;
}
} else {
return null;
}
break;
}
});})(map__105806,map__105806__$1,element,x,y,map__105807,map__105807__$1,turn_info,game_state,selected))
,null,null));
});})(map__105806,map__105806__$1,element,x,y,map__105807,map__105807__$1,turn_info,game_state,selected))
;
return iter__4523__auto__(new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [(0),(1),(2),(3),(4)], null));
})()));
}));
bt.core.draw.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("e","block","e/block",664686109),(1)], null),(function (element,turn_info){
return bt.core.__GT_block("base",element,turn_info);
}));
bt.core.draw.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("e","block","e/block",664686109),(2)], null),(function (p__105832,turn_info){
var map__105835 = p__105832;
var map__105835__$1 = (((((!((map__105835 == null))))?(((((map__105835.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105835.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105835):map__105835);
var element = map__105835__$1;
var x = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105835__$1,new cljs.core.Keyword(null,"x","x",2099068185));
var y = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105835__$1,new cljs.core.Keyword(null,"y","y",-1757859776));
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"<>","<>",1280186386),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(x),"_",cljs.core.str.cljs$core$IFn$_invoke$arity$1(y),"_2"].join('')], null),bt.core.__GT_block("mid",element,turn_info),(function (){var G__105841 = cljs.core.update.cljs$core$IFn$_invoke$arity$3(element,new cljs.core.Keyword(null,"height","height",1025178622),cljs.core.dec);
return (bt.core.draw.cljs$core$IFn$_invoke$arity$1 ? bt.core.draw.cljs$core$IFn$_invoke$arity$1(G__105841) : bt.core.draw.call(null,G__105841));
})()], null);
}));
bt.core.draw.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("e","block","e/block",664686109),(3)], null),(function (p__105844,turn_info){
var map__105845 = p__105844;
var map__105845__$1 = (((((!((map__105845 == null))))?(((((map__105845.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105845.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105845):map__105845);
var element = map__105845__$1;
var x = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105845__$1,new cljs.core.Keyword(null,"x","x",2099068185));
var y = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105845__$1,new cljs.core.Keyword(null,"y","y",-1757859776));
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"<>","<>",1280186386),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(x),"_",cljs.core.str.cljs$core$IFn$_invoke$arity$1(y),"_3"].join('')], null),bt.core.__GT_block("top",element,turn_info),(function (){var G__105849 = cljs.core.update.cljs$core$IFn$_invoke$arity$3(element,new cljs.core.Keyword(null,"height","height",1025178622),cljs.core.dec);
return (bt.core.draw.cljs$core$IFn$_invoke$arity$1 ? bt.core.draw.cljs$core$IFn$_invoke$arity$1(G__105849) : bt.core.draw.call(null,G__105849));
})()], null);
}));
bt.core.draw.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("e","block","e/block",664686109),(4)], null),(function (p__105850,p__105851){
var map__105852 = p__105850;
var map__105852__$1 = (((((!((map__105852 == null))))?(((((map__105852.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105852.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105852):map__105852);
var e = map__105852__$1;
var x = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105852__$1,new cljs.core.Keyword(null,"x","x",2099068185));
var y = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105852__$1,new cljs.core.Keyword(null,"y","y",-1757859776));
var map__105853 = p__105851;
var map__105853__$1 = (((((!((map__105853 == null))))?(((((map__105853.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105853.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105853):map__105853);
var game_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105853__$1,new cljs.core.Keyword(null,"game-state","game-state",935682735));
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"<>","<>",1280186386),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(x),"_",cljs.core.str.cljs$core$IFn$_invoke$arity$1(y),"_4"].join('')], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.dome","div.dome",1107874915),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(x),"_",cljs.core.str.cljs$core$IFn$_invoke$arity$1(y),"_dome"].join(''),new cljs.core.Keyword(null,"style","style",-496642736),bt.core.position(x,y)], null)], null),(function (){var G__105859 = cljs.core.update.cljs$core$IFn$_invoke$arity$3(e,new cljs.core.Keyword(null,"height","height",1025178622),cljs.core.dec);
return (bt.core.draw.cljs$core$IFn$_invoke$arity$1 ? bt.core.draw.cljs$core$IFn$_invoke$arity$1(G__105859) : bt.core.draw.call(null,G__105859));
})()], null);
}));
bt.core.draw.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("e","player","e/player",-97687463),new cljs.core.Keyword("height","no-height","height/no-height",2128392696)], null),(function (p__105860,p__105861){
var map__105862 = p__105860;
var map__105862__$1 = (((((!((map__105862 == null))))?(((((map__105862.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105862.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105862):map__105862);
var x = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105862__$1,new cljs.core.Keyword(null,"x","x",2099068185));
var y = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105862__$1,new cljs.core.Keyword(null,"y","y",-1757859776));
var team = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105862__$1,new cljs.core.Keyword(null,"team","team",1355747699));
var map__105863 = p__105861;
var map__105863__$1 = (((((!((map__105863 == null))))?(((((map__105863.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105863.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105863):map__105863);
var game_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105863__$1,new cljs.core.Keyword(null,"game-state","game-state",935682735));
var map__105866 = cljs.core.deref((function (){var G__105867 = new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","cell-info","bt.db/cell-info",1819284458),x,y], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__105867) : re_frame.core.subscribe.call(null,G__105867));
})());
var map__105866__$1 = (((((!((map__105866 == null))))?(((((map__105866.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105866.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105866):map__105866);
var height = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105866__$1,new cljs.core.Keyword(null,"height","height",1025178622));
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.player","div.player",8830217),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"key","key",-1516042587),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(x),"_",cljs.core.str.cljs$core$IFn$_invoke$arity$1(y),"_player"].join(''),new cljs.core.Keyword(null,"class","class",-2030961996),["height-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(height)," ",cljs.core.name(team)].join(''),new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (map__105866,map__105866__$1,height,map__105862,map__105862__$1,x,y,team,map__105863,map__105863__$1,game_state){
return (function (){
var G__105869 = new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","tile-click","bt.db/tile-click",-839277672),x,y,game_state], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__105869) : re_frame.core.dispatch.call(null,G__105869));
});})(map__105866,map__105866__$1,height,map__105862,map__105862__$1,x,y,team,map__105863,map__105863__$1,game_state))
,new cljs.core.Keyword(null,"style","style",-496642736),cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([bt.core.position(x,y),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"background-color","background-color",570434026),team], null)], 0))], null)], null);
}));
bt.core.board = (function bt$core$board(){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.perspective","div.perspective",96386353),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"-webkit-transform","-webkit-transform",-624763371),cljs.core.deref((function (){var G__105870 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","perspective-angle","bt.db/perspective-angle",669347638)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__105870) : re_frame.core.subscribe.call(null,G__105870));
})())], null)], null),cljs.core.doall.cljs$core$IFn$_invoke$arity$1((function (){var iter__4523__auto__ = (function bt$core$board_$_iter__105871(s__105872){
return (new cljs.core.LazySeq(null,(function (){
var s__105872__$1 = s__105872;
while(true){
var temp__5720__auto__ = cljs.core.seq(s__105872__$1);
if(temp__5720__auto__){
var s__105872__$2 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_(s__105872__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__105872__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__105874 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__105873 = (0);
while(true){
if((i__105873 < size__4522__auto__)){
var map__105875 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__105873);
var map__105875__$1 = (((((!((map__105875 == null))))?(((((map__105875.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105875.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105875):map__105875);
var element = map__105875__$1;
var height = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105875__$1,new cljs.core.Keyword(null,"height","height",1025178622));
var x = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105875__$1,new cljs.core.Keyword(null,"x","x",2099068185));
var y = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105875__$1,new cljs.core.Keyword(null,"y","y",-1757859776));
cljs.core.chunk_append(b__105874,(function (){var G__105877 = element;
var G__105878 = cljs.core.deref((function (){var G__105879 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","turn-info","bt.db/turn-info",180891285)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__105879) : re_frame.core.subscribe.call(null,G__105879));
})());
return (bt.core.draw.cljs$core$IFn$_invoke$arity$2 ? bt.core.draw.cljs$core$IFn$_invoke$arity$2(G__105877,G__105878) : bt.core.draw.call(null,G__105877,G__105878));
})());

var G__105899 = (i__105873 + (1));
i__105873 = G__105899;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__105874),bt$core$board_$_iter__105871(cljs.core.chunk_rest(s__105872__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__105874),null);
}
} else {
var map__105880 = cljs.core.first(s__105872__$2);
var map__105880__$1 = (((((!((map__105880 == null))))?(((((map__105880.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105880.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105880):map__105880);
var element = map__105880__$1;
var height = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105880__$1,new cljs.core.Keyword(null,"height","height",1025178622));
var x = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105880__$1,new cljs.core.Keyword(null,"x","x",2099068185));
var y = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105880__$1,new cljs.core.Keyword(null,"y","y",-1757859776));
return cljs.core.cons((function (){var G__105882 = element;
var G__105883 = cljs.core.deref((function (){var G__105884 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","turn-info","bt.db/turn-info",180891285)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__105884) : re_frame.core.subscribe.call(null,G__105884));
})());
return (bt.core.draw.cljs$core$IFn$_invoke$arity$2 ? bt.core.draw.cljs$core$IFn$_invoke$arity$2(G__105882,G__105883) : bt.core.draw.call(null,G__105882,G__105883));
})(),bt$core$board_$_iter__105871(cljs.core.rest(s__105872__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(cljs.core.deref((function (){var G__105885 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","board-elements","bt.db/board-elements",-1513074900)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__105885) : re_frame.core.subscribe.call(null,G__105885));
})()));
})())], null);
});
});
bt.core.info = (function bt$core$info(){
var map__105886 = cljs.core.deref((function (){var G__105887 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","turn-info","bt.db/turn-info",180891285)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__105887) : re_frame.core.subscribe.call(null,G__105887));
})());
var map__105886__$1 = (((((!((map__105886 == null))))?(((((map__105886.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105886.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105886):map__105886);
var info = map__105886__$1;
var team = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105886__$1,new cljs.core.Keyword(null,"team","team",1355747699));
var mode = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105886__$1,new cljs.core.Keyword(null,"mode","mode",654403691));
var title = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105886__$1,new cljs.core.Keyword(null,"title","title",636505583));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1","h1",-1896887462),title], null),(cljs.core.truth_(cljs.core.deref((function (){var G__105889 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","debug","bt.db/debug",-633341834)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__105889) : re_frame.core.subscribe.call(null,G__105889));
})()))?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"<>","<>",1280186386),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword(null,"type","type",1174270348),"range",new cljs.core.Keyword(null,"value","value",305978217),cljs.core.deref((function (){var G__105890 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","view-angle","bt.db/view-angle",-1857327732)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__105890) : re_frame.core.subscribe.call(null,G__105890));
})()),new cljs.core.Keyword(null,"step","step",1288888124),0.5,new cljs.core.Keyword(null,"min","min",444991522),25.5,new cljs.core.Keyword(null,"max","max",61366548),76.5,new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"width","width",-384071477),"100%"], null),new cljs.core.Keyword(null,"on-change","on-change",-732046149),((function (map__105886,map__105886__$1,info,team,mode,title){
return (function (e){
var G__105891 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","set-view-angle","bt.db/set-view-angle",1270316349),e.target.value], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__105891) : re_frame.core.dispatch.call(null,G__105891));
});})(map__105886,map__105886__$1,info,team,mode,title))
], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pre","pre",2118456869),cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([info], 0))], null)], null):null)], null);
});
bt.core.the_rules = (function bt$core$the_rules(){
return new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"margin","margin",-995903681),"1em",new cljs.core.Keyword(null,"padding","padding",1660304693),"1em 2em",new cljs.core.Keyword(null,"border","border",1444987323),"2px #333 solid"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1","h1",-1896887462),"The Rules:"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),"Each turn consists of 2 steps:"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ol","ol",932524051),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),"Move"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),"First, move one of your builders into a neighboring space. You may move your Builder Pawn on the same level, step-up one level, or step down any number of levels"], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),"Build"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),"Then construct a building level adjacent to the builder you moved. When building on top of the third level, place a dome instead, removing that space from play"], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),"Winning the game"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),"If either of your builders reaches the third level, you win."], null)], null);
});
bt.core.landing = (function bt$core$landing(){
var expand_rules_QMARK_ = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(false);
var secret_mode = reagent.core.atom.cljs$core$IFn$_invoke$arity$1((0));
return ((function (expand_rules_QMARK_,secret_mode){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.landing","div.landing",-994480571),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1","h1",-1896887462),"Welcome to Santorini Online!"], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),"Do you know ",new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"href","href",-793805698),"#",new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (expand_rules_QMARK_,secret_mode){
return (function (){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(expand_rules_QMARK_,((function (expand_rules_QMARK_,secret_mode){
return (function (_){
return true;
});})(expand_rules_QMARK_,secret_mode))
);

return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(secret_mode,cljs.core.inc);
});})(expand_rules_QMARK_,secret_mode))
], null),"how to"], null)," play?"], null),(cljs.core.truth_(cljs.core.deref(expand_rules_QMARK_))?new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [bt.core.the_rules], null):null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button","button",1456579943),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"margin-top","margin-top",392161226),"30px"], null),new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (expand_rules_QMARK_,secret_mode){
return (function (){
var G__105892 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","start-game!","bt.db/start-game!",-420098869)], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__105892) : re_frame.core.dispatch.call(null,G__105892));
});})(expand_rules_QMARK_,secret_mode))
], null),"Let's play!"], null),(((cljs.core.deref(secret_mode) > (10)))?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button","button",1456579943),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"margin-top","margin-top",392161226),"30px"], null),new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (expand_rules_QMARK_,secret_mode){
return (function (){
var G__105893 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","start-game-debug!","bt.db/start-game-debug!",449397960)], null);
return (re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__105893) : re_frame.core.dispatch.call(null,G__105893));
});})(expand_rules_QMARK_,secret_mode))
], null),"Let's play (secret debug mode!)"], null):null)], null);
});
;})(expand_rules_QMARK_,secret_mode))
});
bt.core.game_over = (function bt$core$game_over(){
var game_over_QMARK_ = cljs.core.deref((function (){var G__105894 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","game-over","bt.db/game-over",-634918369)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__105894) : re_frame.core.subscribe.call(null,G__105894));
})());
if(cljs.core.truth_(game_over_QMARK_)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"position","position",-2011731912),"absolute",new cljs.core.Keyword(null,"top","top",-1856271961),"50vh",new cljs.core.Keyword(null,"left","left",-399115937),"30vh",new cljs.core.Keyword(null,"font-size","font-size",-1847940346),"60px"], null)], null),"Game Over!"], null);
} else {
return null;
}
});
bt.core.game = (function bt$core$game(){
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [bt.core.board], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [bt.core.game_over], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.controls","div.controls",1658515593)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [bt.core.info], null)], null);
});
bt.core.root = (function bt$core$root(){
if(cljs.core.truth_(cljs.core.deref((function (){var G__105895 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","playing-game?","bt.db/playing-game?",252464458)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__105895) : re_frame.core.subscribe.call(null,G__105895));
})()))){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [bt.core.game], null);
} else {
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [bt.core.landing], null);
}
});
bt.core.init = (function bt$core$init(){
var G__105896_105900 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","init!","bt.db/init!",-891089882)], null);
(re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.dispatch.cljs$core$IFn$_invoke$arity$1(G__105896_105900) : re_frame.core.dispatch.call(null,G__105896_105900));

return reagent.core.render.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [bt.core.root], null),document.getElementById("app"));
});
goog.exportSymbol('bt.core.init', bt.core.init);

//# sourceMappingURL=bt.core.js.map
