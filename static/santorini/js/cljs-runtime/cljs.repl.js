goog.provide('cljs.repl');
goog.require('cljs.core');
goog.require('cljs.spec.alpha');
goog.require('goog.string');
goog.require('goog.string.format');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__27977){
var map__27978 = p__27977;
var map__27978__$1 = (((((!((map__27978 == null))))?(((((map__27978.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__27978.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__27978):map__27978);
var m = map__27978__$1;
var n = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27978__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27978__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["-------------------------"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (){var or__4131__auto__ = new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return [(function (){var temp__5720__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5720__auto__)){
var ns = temp__5720__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})(),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('');
}
})()], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Protocol"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__27980_28159 = cljs.core.seq(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__27981_28160 = null;
var count__27982_28161 = (0);
var i__27983_28162 = (0);
while(true){
if((i__27983_28162 < count__27982_28161)){
var f_28163 = chunk__27981_28160.cljs$core$IIndexed$_nth$arity$2(null,i__27983_28162);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_28163], 0));


var G__28164 = seq__27980_28159;
var G__28165 = chunk__27981_28160;
var G__28166 = count__27982_28161;
var G__28167 = (i__27983_28162 + (1));
seq__27980_28159 = G__28164;
chunk__27981_28160 = G__28165;
count__27982_28161 = G__28166;
i__27983_28162 = G__28167;
continue;
} else {
var temp__5720__auto___28168 = cljs.core.seq(seq__27980_28159);
if(temp__5720__auto___28168){
var seq__27980_28169__$1 = temp__5720__auto___28168;
if(cljs.core.chunked_seq_QMARK_(seq__27980_28169__$1)){
var c__4550__auto___28170 = cljs.core.chunk_first(seq__27980_28169__$1);
var G__28171 = cljs.core.chunk_rest(seq__27980_28169__$1);
var G__28172 = c__4550__auto___28170;
var G__28173 = cljs.core.count(c__4550__auto___28170);
var G__28174 = (0);
seq__27980_28159 = G__28171;
chunk__27981_28160 = G__28172;
count__27982_28161 = G__28173;
i__27983_28162 = G__28174;
continue;
} else {
var f_28178 = cljs.core.first(seq__27980_28169__$1);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_28178], 0));


var G__28179 = cljs.core.next(seq__27980_28169__$1);
var G__28180 = null;
var G__28181 = (0);
var G__28182 = (0);
seq__27980_28159 = G__28179;
chunk__27981_28160 = G__28180;
count__27982_28161 = G__28181;
i__27983_28162 = G__28182;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_28183 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__4131__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([arglists_28183], 0));
} else {
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first(arglists_28183)))?cljs.core.second(arglists_28183):arglists_28183)], 0));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Special Form"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.contains_QMARK_(m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
} else {
return null;
}
} else {
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Macro"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["REPL Special Function"], 0));
} else {
}

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__27987_28184 = cljs.core.seq(new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__27988_28185 = null;
var count__27989_28186 = (0);
var i__27990_28187 = (0);
while(true){
if((i__27990_28187 < count__27989_28186)){
var vec__28001_28188 = chunk__27988_28185.cljs$core$IIndexed$_nth$arity$2(null,i__27990_28187);
var name_28189 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__28001_28188,(0),null);
var map__28004_28190 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__28001_28188,(1),null);
var map__28004_28191__$1 = (((((!((map__28004_28190 == null))))?(((((map__28004_28190.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28004_28190.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__28004_28190):map__28004_28190);
var doc_28192 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28004_28191__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_28193 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28004_28191__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_28189], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_28193], 0));

if(cljs.core.truth_(doc_28192)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_28192], 0));
} else {
}


var G__28202 = seq__27987_28184;
var G__28203 = chunk__27988_28185;
var G__28204 = count__27989_28186;
var G__28205 = (i__27990_28187 + (1));
seq__27987_28184 = G__28202;
chunk__27988_28185 = G__28203;
count__27989_28186 = G__28204;
i__27990_28187 = G__28205;
continue;
} else {
var temp__5720__auto___28208 = cljs.core.seq(seq__27987_28184);
if(temp__5720__auto___28208){
var seq__27987_28209__$1 = temp__5720__auto___28208;
if(cljs.core.chunked_seq_QMARK_(seq__27987_28209__$1)){
var c__4550__auto___28210 = cljs.core.chunk_first(seq__27987_28209__$1);
var G__28211 = cljs.core.chunk_rest(seq__27987_28209__$1);
var G__28212 = c__4550__auto___28210;
var G__28213 = cljs.core.count(c__4550__auto___28210);
var G__28214 = (0);
seq__27987_28184 = G__28211;
chunk__27988_28185 = G__28212;
count__27989_28186 = G__28213;
i__27990_28187 = G__28214;
continue;
} else {
var vec__28010_28216 = cljs.core.first(seq__27987_28209__$1);
var name_28217 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__28010_28216,(0),null);
var map__28013_28218 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__28010_28216,(1),null);
var map__28013_28219__$1 = (((((!((map__28013_28218 == null))))?(((((map__28013_28218.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28013_28218.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__28013_28218):map__28013_28218);
var doc_28220 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28013_28219__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_28221 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28013_28219__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_28217], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_28221], 0));

if(cljs.core.truth_(doc_28220)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_28220], 0));
} else {
}


var G__28222 = cljs.core.next(seq__27987_28209__$1);
var G__28223 = null;
var G__28224 = (0);
var G__28225 = (0);
seq__27987_28184 = G__28222;
chunk__27988_28185 = G__28223;
count__27989_28186 = G__28224;
i__27990_28187 = G__28225;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5720__auto__ = cljs.spec.alpha.get_spec(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name(n)),cljs.core.name(nm)));
if(cljs.core.truth_(temp__5720__auto__)){
var fnspec = temp__5720__auto__;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));

var seq__28016 = cljs.core.seq(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__28017 = null;
var count__28018 = (0);
var i__28019 = (0);
while(true){
if((i__28019 < count__28018)){
var role = chunk__28017.cljs$core$IIndexed$_nth$arity$2(null,i__28019);
var temp__5720__auto___28229__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5720__auto___28229__$1)){
var spec_28230 = temp__5720__auto___28229__$1;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_28230)], 0));
} else {
}


var G__28233 = seq__28016;
var G__28234 = chunk__28017;
var G__28235 = count__28018;
var G__28236 = (i__28019 + (1));
seq__28016 = G__28233;
chunk__28017 = G__28234;
count__28018 = G__28235;
i__28019 = G__28236;
continue;
} else {
var temp__5720__auto____$1 = cljs.core.seq(seq__28016);
if(temp__5720__auto____$1){
var seq__28016__$1 = temp__5720__auto____$1;
if(cljs.core.chunked_seq_QMARK_(seq__28016__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__28016__$1);
var G__28238 = cljs.core.chunk_rest(seq__28016__$1);
var G__28239 = c__4550__auto__;
var G__28240 = cljs.core.count(c__4550__auto__);
var G__28241 = (0);
seq__28016 = G__28238;
chunk__28017 = G__28239;
count__28018 = G__28240;
i__28019 = G__28241;
continue;
} else {
var role = cljs.core.first(seq__28016__$1);
var temp__5720__auto___28242__$2 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5720__auto___28242__$2)){
var spec_28243 = temp__5720__auto___28242__$2;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_28243)], 0));
} else {
}


var G__28247 = cljs.core.next(seq__28016__$1);
var G__28248 = null;
var G__28249 = (0);
var G__28250 = (0);
seq__28016 = G__28247;
chunk__28017 = G__28248;
count__28018 = G__28249;
i__28019 = G__28250;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Constructs a data representation for a Error with keys:
 *  :cause - root cause message
 *  :phase - error phase
 *  :via - cause chain, with cause keys:
 *           :type - exception class symbol
 *           :message - exception message
 *           :data - ex-data
 *           :at - top stack element
 *  :trace - root cause stack elements
 */
cljs.repl.Error__GT_map = (function cljs$repl$Error__GT_map(o){
var base = (function (t){
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),(((t instanceof cljs.core.ExceptionInfo))?new cljs.core.Symbol(null,"ExceptionInfo","ExceptionInfo",294935087,null):(((t instanceof EvalError))?new cljs.core.Symbol("js","EvalError","js/EvalError",1793498501,null):(((t instanceof RangeError))?new cljs.core.Symbol("js","RangeError","js/RangeError",1703848089,null):(((t instanceof ReferenceError))?new cljs.core.Symbol("js","ReferenceError","js/ReferenceError",-198403224,null):(((t instanceof SyntaxError))?new cljs.core.Symbol("js","SyntaxError","js/SyntaxError",-1527651665,null):(((t instanceof URIError))?new cljs.core.Symbol("js","URIError","js/URIError",505061350,null):(((t instanceof Error))?new cljs.core.Symbol("js","Error","js/Error",-1692659266,null):null
)))))))], null),(function (){var temp__5720__auto__ = cljs.core.ex_message(t);
if(cljs.core.truth_(temp__5720__auto__)){
var msg = temp__5720__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"message","message",-406056002),msg], null);
} else {
return null;
}
})(),(function (){var temp__5720__auto__ = cljs.core.ex_data(t);
if(cljs.core.truth_(temp__5720__auto__)){
var ed = temp__5720__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),ed], null);
} else {
return null;
}
})()], 0));
});
var via = (function (){var via = cljs.core.PersistentVector.EMPTY;
var t = o;
while(true){
if(cljs.core.truth_(t)){
var G__28257 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(via,t);
var G__28258 = cljs.core.ex_cause(t);
via = G__28257;
t = G__28258;
continue;
} else {
return via;
}
break;
}
})();
var root = cljs.core.peek(via);
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"via","via",-1904457336),cljs.core.vec(cljs.core.map.cljs$core$IFn$_invoke$arity$2(base,via)),new cljs.core.Keyword(null,"trace","trace",-1082747415),null], null),(function (){var temp__5720__auto__ = cljs.core.ex_message(root);
if(cljs.core.truth_(temp__5720__auto__)){
var root_msg = temp__5720__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cause","cause",231901252),root_msg], null);
} else {
return null;
}
})(),(function (){var temp__5720__auto__ = cljs.core.ex_data(root);
if(cljs.core.truth_(temp__5720__auto__)){
var data = temp__5720__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),data], null);
} else {
return null;
}
})(),(function (){var temp__5720__auto__ = new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358).cljs$core$IFn$_invoke$arity$1(cljs.core.ex_data(o));
if(cljs.core.truth_(temp__5720__auto__)){
var phase = temp__5720__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"phase","phase",575722892),phase], null);
} else {
return null;
}
})()], 0));
});
/**
 * Returns an analysis of the phase, error, cause, and location of an error that occurred
 *   based on Throwable data, as returned by Throwable->map. All attributes other than phase
 *   are optional:
 *  :clojure.error/phase - keyword phase indicator, one of:
 *    :read-source :compile-syntax-check :compilation :macro-syntax-check :macroexpansion
 *    :execution :read-eval-result :print-eval-result
 *  :clojure.error/source - file name (no path)
 *  :clojure.error/line - integer line number
 *  :clojure.error/column - integer column number
 *  :clojure.error/symbol - symbol being expanded/compiled/invoked
 *  :clojure.error/class - cause exception class symbol
 *  :clojure.error/cause - cause exception message
 *  :clojure.error/spec - explain-data for spec error
 */
cljs.repl.ex_triage = (function cljs$repl$ex_triage(datafied_throwable){
var map__28037 = datafied_throwable;
var map__28037__$1 = (((((!((map__28037 == null))))?(((((map__28037.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28037.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__28037):map__28037);
var via = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28037__$1,new cljs.core.Keyword(null,"via","via",-1904457336));
var trace = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28037__$1,new cljs.core.Keyword(null,"trace","trace",-1082747415));
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__28037__$1,new cljs.core.Keyword(null,"phase","phase",575722892),new cljs.core.Keyword(null,"execution","execution",253283524));
var map__28038 = cljs.core.last(via);
var map__28038__$1 = (((((!((map__28038 == null))))?(((((map__28038.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28038.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__28038):map__28038);
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28038__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var message = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28038__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var data = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28038__$1,new cljs.core.Keyword(null,"data","data",-232669377));
var map__28039 = data;
var map__28039__$1 = (((((!((map__28039 == null))))?(((((map__28039.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28039.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__28039):map__28039);
var problems = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28039__$1,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814));
var fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28039__$1,new cljs.core.Keyword("cljs.spec.alpha","fn","cljs.spec.alpha/fn",408600443));
var caller = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28039__$1,new cljs.core.Keyword("cljs.spec.test.alpha","caller","cljs.spec.test.alpha/caller",-398302390));
var map__28040 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.first(via));
var map__28040__$1 = (((((!((map__28040 == null))))?(((((map__28040.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28040.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__28040):map__28040);
var top_data = map__28040__$1;
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28040__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3((function (){var G__28054 = phase;
var G__28054__$1 = (((G__28054 instanceof cljs.core.Keyword))?G__28054.fqn:null);
switch (G__28054__$1) {
case "read-source":
var map__28058 = data;
var map__28058__$1 = (((((!((map__28058 == null))))?(((((map__28058.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28058.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__28058):map__28058);
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28058__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28058__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var G__28063 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.second(via)),top_data], 0));
var G__28063__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28063,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__28063);
var G__28063__$2 = (cljs.core.truth_((function (){var fexpr__28066 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__28066.cljs$core$IFn$_invoke$arity$1 ? fexpr__28066.cljs$core$IFn$_invoke$arity$1(source) : fexpr__28066.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__28063__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__28063__$1);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28063__$2,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__28063__$2;
}

break;
case "compile-syntax-check":
case "compilation":
case "macro-syntax-check":
case "macroexpansion":
var G__28067 = top_data;
var G__28067__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28067,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__28067);
var G__28067__$2 = (cljs.core.truth_((function (){var fexpr__28069 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__28069.cljs$core$IFn$_invoke$arity$1 ? fexpr__28069.cljs$core$IFn$_invoke$arity$1(source) : fexpr__28069.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__28067__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__28067__$1);
var G__28067__$3 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28067__$2,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__28067__$2);
var G__28067__$4 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28067__$3,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__28067__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28067__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__28067__$4;
}

break;
case "read-eval-result":
case "print-eval-result":
var vec__28070 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__28070,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__28070,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__28070,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__28070,(3),null);
var G__28076 = top_data;
var G__28076__$1 = (cljs.core.truth_(line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28076,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),line):G__28076);
var G__28076__$2 = (cljs.core.truth_(file)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28076__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file):G__28076__$1);
var G__28076__$3 = (cljs.core.truth_((function (){var and__4120__auto__ = source__$1;
if(cljs.core.truth_(and__4120__auto__)){
return method;
} else {
return and__4120__auto__;
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28076__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null))):G__28076__$2);
var G__28076__$4 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28076__$3,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__28076__$3);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28076__$4,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__28076__$4;
}

break;
case "execution":
var vec__28079 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__28079,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__28079,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__28079,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__28079,(3),null);
var file__$1 = cljs.core.first(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(((function (vec__28079,source__$1,method,file,line,G__28054,G__28054__$1,map__28037,map__28037__$1,via,trace,phase,map__28038,map__28038__$1,type,message,data,map__28039,map__28039__$1,problems,fn,caller,map__28040,map__28040__$1,top_data,source){
return (function (p1__28034_SHARP_){
var or__4131__auto__ = (p1__28034_SHARP_ == null);
if(or__4131__auto__){
return or__4131__auto__;
} else {
var fexpr__28086 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__28086.cljs$core$IFn$_invoke$arity$1 ? fexpr__28086.cljs$core$IFn$_invoke$arity$1(p1__28034_SHARP_) : fexpr__28086.call(null,p1__28034_SHARP_));
}
});})(vec__28079,source__$1,method,file,line,G__28054,G__28054__$1,map__28037,map__28037__$1,via,trace,phase,map__28038,map__28038__$1,type,message,data,map__28039,map__28039__$1,problems,fn,caller,map__28040,map__28040__$1,top_data,source))
,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(caller),file], null)));
var err_line = (function (){var or__4131__auto__ = new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(caller);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return line;
}
})();
var G__28089 = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type], null);
var G__28089__$1 = (cljs.core.truth_(err_line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28089,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),err_line):G__28089);
var G__28089__$2 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28089__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__28089__$1);
var G__28089__$3 = (cljs.core.truth_((function (){var or__4131__auto__ = fn;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
var and__4120__auto__ = source__$1;
if(cljs.core.truth_(and__4120__auto__)){
return method;
} else {
return and__4120__auto__;
}
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28089__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(function (){var or__4131__auto__ = fn;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null));
}
})()):G__28089__$2);
var G__28089__$4 = (cljs.core.truth_(file__$1)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28089__$3,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file__$1):G__28089__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__28089__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__28089__$4;
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__28054__$1)].join('')));

}
})(),new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358),phase);
});
/**
 * Returns a string from exception data, as produced by ex-triage.
 *   The first line summarizes the exception phase and location.
 *   The subsequent lines describe the cause.
 */
cljs.repl.ex_str = (function cljs$repl$ex_str(p__28097){
var map__28098 = p__28097;
var map__28098__$1 = (((((!((map__28098 == null))))?(((((map__28098.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__28098.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__28098):map__28098);
var triage_data = map__28098__$1;
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28098__$1,new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358));
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28098__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28098__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28098__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var symbol = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28098__$1,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994));
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28098__$1,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890));
var cause = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28098__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742));
var spec = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__28098__$1,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595));
var loc = [cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4131__auto__ = source;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "<cljs repl>";
}
})()),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4131__auto__ = line;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (1);
}
})()),(cljs.core.truth_(column)?[":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join(''):"")].join('');
var class_name = cljs.core.name((function (){var or__4131__auto__ = class$;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "";
}
})());
var simple_class = class_name;
var cause_type = ((cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["RuntimeException",null,"Exception",null], null), null),simple_class))?"":[" (",simple_class,")"].join(''));
var format = goog.string.format;
var G__28102 = phase;
var G__28102__$1 = (((G__28102 instanceof cljs.core.Keyword))?G__28102.fqn:null);
switch (G__28102__$1) {
case "read-source":
return (format.cljs$core$IFn$_invoke$arity$3 ? format.cljs$core$IFn$_invoke$arity$3("Syntax error reading source at (%s).\n%s\n",loc,cause) : format.call(null,"Syntax error reading source at (%s).\n%s\n",loc,cause));

break;
case "macro-syntax-check":
var G__28106 = "Syntax error macroexpanding %sat (%s).\n%s";
var G__28107 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__28108 = loc;
var G__28109 = (cljs.core.truth_(spec)?(function (){var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__28112_28328 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__28113_28329 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__28114_28330 = true;
var _STAR_print_fn_STAR__temp_val__28115_28331 = ((function (_STAR_print_newline_STAR__orig_val__28112_28328,_STAR_print_fn_STAR__orig_val__28113_28329,_STAR_print_newline_STAR__temp_val__28114_28330,sb__4661__auto__,G__28106,G__28107,G__28108,G__28102,G__28102__$1,loc,class_name,simple_class,cause_type,format,map__28098,map__28098__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__28112_28328,_STAR_print_fn_STAR__orig_val__28113_28329,_STAR_print_newline_STAR__temp_val__28114_28330,sb__4661__auto__,G__28106,G__28107,G__28108,G__28102,G__28102__$1,loc,class_name,simple_class,cause_type,format,map__28098,map__28098__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__28114_28330;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__28115_28331;

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),((function (_STAR_print_newline_STAR__orig_val__28112_28328,_STAR_print_fn_STAR__orig_val__28113_28329,_STAR_print_newline_STAR__temp_val__28114_28330,_STAR_print_fn_STAR__temp_val__28115_28331,sb__4661__auto__,G__28106,G__28107,G__28108,G__28102,G__28102__$1,loc,class_name,simple_class,cause_type,format,map__28098,map__28098__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (_STAR_print_newline_STAR__orig_val__28112_28328,_STAR_print_fn_STAR__orig_val__28113_28329,_STAR_print_newline_STAR__temp_val__28114_28330,_STAR_print_fn_STAR__temp_val__28115_28331,sb__4661__auto__,G__28106,G__28107,G__28108,G__28102,G__28102__$1,loc,class_name,simple_class,cause_type,format,map__28098,map__28098__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (p1__28091_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__28091_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
});})(_STAR_print_newline_STAR__orig_val__28112_28328,_STAR_print_fn_STAR__orig_val__28113_28329,_STAR_print_newline_STAR__temp_val__28114_28330,_STAR_print_fn_STAR__temp_val__28115_28331,sb__4661__auto__,G__28106,G__28107,G__28108,G__28102,G__28102__$1,loc,class_name,simple_class,cause_type,format,map__28098,map__28098__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
,probs);
});})(_STAR_print_newline_STAR__orig_val__28112_28328,_STAR_print_fn_STAR__orig_val__28113_28329,_STAR_print_newline_STAR__temp_val__28114_28330,_STAR_print_fn_STAR__temp_val__28115_28331,sb__4661__auto__,G__28106,G__28107,G__28108,G__28102,G__28102__$1,loc,class_name,simple_class,cause_type,format,map__28098,map__28098__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
)
);
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__28113_28329;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__28112_28328;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
})():(format.cljs$core$IFn$_invoke$arity$2 ? format.cljs$core$IFn$_invoke$arity$2("%s\n",cause) : format.call(null,"%s\n",cause)));
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__28106,G__28107,G__28108,G__28109) : format.call(null,G__28106,G__28107,G__28108,G__28109));

break;
case "macroexpansion":
var G__28121 = "Unexpected error%s macroexpanding %sat (%s).\n%s\n";
var G__28122 = cause_type;
var G__28123 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__28124 = loc;
var G__28125 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__28121,G__28122,G__28123,G__28124,G__28125) : format.call(null,G__28121,G__28122,G__28123,G__28124,G__28125));

break;
case "compile-syntax-check":
var G__28126 = "Syntax error%s compiling %sat (%s).\n%s\n";
var G__28127 = cause_type;
var G__28128 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__28129 = loc;
var G__28130 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__28126,G__28127,G__28128,G__28129,G__28130) : format.call(null,G__28126,G__28127,G__28128,G__28129,G__28130));

break;
case "compilation":
var G__28131 = "Unexpected error%s compiling %sat (%s).\n%s\n";
var G__28132 = cause_type;
var G__28133 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__28134 = loc;
var G__28135 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__28131,G__28132,G__28133,G__28134,G__28135) : format.call(null,G__28131,G__28132,G__28133,G__28134,G__28135));

break;
case "read-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "print-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "execution":
if(cljs.core.truth_(spec)){
var G__28137 = "Execution error - invalid arguments to %s at (%s).\n%s";
var G__28138 = symbol;
var G__28139 = loc;
var G__28140 = (function (){var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__28141_28352 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__28142_28353 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__28143_28354 = true;
var _STAR_print_fn_STAR__temp_val__28144_28355 = ((function (_STAR_print_newline_STAR__orig_val__28141_28352,_STAR_print_fn_STAR__orig_val__28142_28353,_STAR_print_newline_STAR__temp_val__28143_28354,sb__4661__auto__,G__28137,G__28138,G__28139,G__28102,G__28102__$1,loc,class_name,simple_class,cause_type,format,map__28098,map__28098__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__28141_28352,_STAR_print_fn_STAR__orig_val__28142_28353,_STAR_print_newline_STAR__temp_val__28143_28354,sb__4661__auto__,G__28137,G__28138,G__28139,G__28102,G__28102__$1,loc,class_name,simple_class,cause_type,format,map__28098,map__28098__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__28143_28354;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__28144_28355;

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),((function (_STAR_print_newline_STAR__orig_val__28141_28352,_STAR_print_fn_STAR__orig_val__28142_28353,_STAR_print_newline_STAR__temp_val__28143_28354,_STAR_print_fn_STAR__temp_val__28144_28355,sb__4661__auto__,G__28137,G__28138,G__28139,G__28102,G__28102__$1,loc,class_name,simple_class,cause_type,format,map__28098,map__28098__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (_STAR_print_newline_STAR__orig_val__28141_28352,_STAR_print_fn_STAR__orig_val__28142_28353,_STAR_print_newline_STAR__temp_val__28143_28354,_STAR_print_fn_STAR__temp_val__28144_28355,sb__4661__auto__,G__28137,G__28138,G__28139,G__28102,G__28102__$1,loc,class_name,simple_class,cause_type,format,map__28098,map__28098__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (p1__28094_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__28094_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
});})(_STAR_print_newline_STAR__orig_val__28141_28352,_STAR_print_fn_STAR__orig_val__28142_28353,_STAR_print_newline_STAR__temp_val__28143_28354,_STAR_print_fn_STAR__temp_val__28144_28355,sb__4661__auto__,G__28137,G__28138,G__28139,G__28102,G__28102__$1,loc,class_name,simple_class,cause_type,format,map__28098,map__28098__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
,probs);
});})(_STAR_print_newline_STAR__orig_val__28141_28352,_STAR_print_fn_STAR__orig_val__28142_28353,_STAR_print_newline_STAR__temp_val__28143_28354,_STAR_print_fn_STAR__temp_val__28144_28355,sb__4661__auto__,G__28137,G__28138,G__28139,G__28102,G__28102__$1,loc,class_name,simple_class,cause_type,format,map__28098,map__28098__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
)
);
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__28142_28353;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__28141_28352;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
})();
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__28137,G__28138,G__28139,G__28140) : format.call(null,G__28137,G__28138,G__28139,G__28140));
} else {
var G__28149 = "Execution error%s at %s(%s).\n%s\n";
var G__28150 = cause_type;
var G__28151 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__28152 = loc;
var G__28153 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__28149,G__28150,G__28151,G__28152,G__28153) : format.call(null,G__28149,G__28150,G__28151,G__28152,G__28153));
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__28102__$1)].join('')));

}
});
cljs.repl.error__GT_str = (function cljs$repl$error__GT_str(error){
return cljs.repl.ex_str(cljs.repl.ex_triage(cljs.repl.Error__GT_map(error)));
});

//# sourceMappingURL=cljs.repl.js.map
