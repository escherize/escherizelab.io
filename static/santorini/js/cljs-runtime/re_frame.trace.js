goog.provide('re_frame.trace');
goog.require('cljs.core');
goog.require('re_frame.interop');
goog.require('re_frame.loggers');
goog.require('goog.functions');
re_frame.trace.id = cljs.core.atom.cljs$core$IFn$_invoke$arity$1((0));
re_frame.trace._STAR_current_trace_STAR_ = null;
re_frame.trace.reset_tracing_BANG_ = (function re_frame$trace$reset_tracing_BANG_(){
return cljs.core.reset_BANG_(re_frame.trace.id,(0));
});

/** @define {boolean} */
goog.define("re_frame.trace.trace_enabled_QMARK_",false);
/**
 * See https://groups.google.com/d/msg/clojurescript/jk43kmYiMhA/IHglVr_TPdgJ for more details
 */
re_frame.trace.is_trace_enabled_QMARK_ = (function re_frame$trace$is_trace_enabled_QMARK_(){
return re_frame.trace.trace_enabled_QMARK_;
});
re_frame.trace.trace_cbs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
if((typeof re_frame !== 'undefined') && (typeof re_frame.trace !== 'undefined') && (typeof re_frame.trace.traces !== 'undefined')){
} else {
re_frame.trace.traces = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentVector.EMPTY);
}
if((typeof re_frame !== 'undefined') && (typeof re_frame.trace !== 'undefined') && (typeof re_frame.trace.next_delivery !== 'undefined')){
} else {
re_frame.trace.next_delivery = cljs.core.atom.cljs$core$IFn$_invoke$arity$1((0));
}
/**
 * Registers a tracing callback function which will receive a collection of one or more traces.
 *   Will replace an existing callback function if it shares the same key.
 */
re_frame.trace.register_trace_cb = (function re_frame$trace$register_trace_cb(key,f){
if(re_frame.trace.trace_enabled_QMARK_){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(re_frame.trace.trace_cbs,cljs.core.assoc,key,f);
} else {
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Tracing is not enabled. Please set {\"re_frame.trace.trace_enabled_QMARK_\" true} in :closure-defines. See: https://github.com/Day8/re-frame-10x#installation."], 0));
}
});
re_frame.trace.remove_trace_cb = (function re_frame$trace$remove_trace_cb(key){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(re_frame.trace.trace_cbs,cljs.core.dissoc,key);

return null;
});
re_frame.trace.next_id = (function re_frame$trace$next_id(){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(re_frame.trace.id,cljs.core.inc);
});
re_frame.trace.start_trace = (function re_frame$trace$start_trace(p__34142){
var map__34143 = p__34142;
var map__34143__$1 = (((((!((map__34143 == null))))?(((((map__34143.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__34143.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__34143):map__34143);
var operation = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34143__$1,new cljs.core.Keyword(null,"operation","operation",-1267664310));
var op_type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34143__$1,new cljs.core.Keyword(null,"op-type","op-type",-1636141668));
var tags = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34143__$1,new cljs.core.Keyword(null,"tags","tags",1771418977));
var child_of = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34143__$1,new cljs.core.Keyword(null,"child-of","child-of",-903376662));
return new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"id","id",-1388402092),re_frame.trace.next_id(),new cljs.core.Keyword(null,"operation","operation",-1267664310),operation,new cljs.core.Keyword(null,"op-type","op-type",-1636141668),op_type,new cljs.core.Keyword(null,"tags","tags",1771418977),tags,new cljs.core.Keyword(null,"child-of","child-of",-903376662),(function (){var or__4131__auto__ = child_of;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(re_frame.trace._STAR_current_trace_STAR_);
}
})(),new cljs.core.Keyword(null,"start","start",-355208981),re_frame.interop.now()], null);
});
re_frame.trace.debounce_time = (50);
re_frame.trace.debounce = (function re_frame$trace$debounce(f,interval){
return goog.functions.debounce(f,interval);
});
re_frame.trace.schedule_debounce = re_frame.trace.debounce((function re_frame$trace$tracing_cb_debounced(){
var seq__34145_34177 = cljs.core.seq(cljs.core.deref(re_frame.trace.trace_cbs));
var chunk__34146_34178 = null;
var count__34147_34179 = (0);
var i__34148_34180 = (0);
while(true){
if((i__34148_34180 < count__34147_34179)){
var vec__34160_34181 = chunk__34146_34178.cljs$core$IIndexed$_nth$arity$2(null,i__34148_34180);
var k_34182 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34160_34181,(0),null);
var cb_34183 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34160_34181,(1),null);
try{var G__34165_34188 = cljs.core.deref(re_frame.trace.traces);
(cb_34183.cljs$core$IFn$_invoke$arity$1 ? cb_34183.cljs$core$IFn$_invoke$arity$1(G__34165_34188) : cb_34183.call(null,G__34165_34188));
}catch (e34164){var e_34189 = e34164;
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Error thrown from trace cb",k_34182,"while storing",cljs.core.deref(re_frame.trace.traces),e_34189], 0));
}

var G__34193 = seq__34145_34177;
var G__34194 = chunk__34146_34178;
var G__34195 = count__34147_34179;
var G__34196 = (i__34148_34180 + (1));
seq__34145_34177 = G__34193;
chunk__34146_34178 = G__34194;
count__34147_34179 = G__34195;
i__34148_34180 = G__34196;
continue;
} else {
var temp__5720__auto___34198 = cljs.core.seq(seq__34145_34177);
if(temp__5720__auto___34198){
var seq__34145_34200__$1 = temp__5720__auto___34198;
if(cljs.core.chunked_seq_QMARK_(seq__34145_34200__$1)){
var c__4550__auto___34202 = cljs.core.chunk_first(seq__34145_34200__$1);
var G__34203 = cljs.core.chunk_rest(seq__34145_34200__$1);
var G__34204 = c__4550__auto___34202;
var G__34205 = cljs.core.count(c__4550__auto___34202);
var G__34206 = (0);
seq__34145_34177 = G__34203;
chunk__34146_34178 = G__34204;
count__34147_34179 = G__34205;
i__34148_34180 = G__34206;
continue;
} else {
var vec__34166_34207 = cljs.core.first(seq__34145_34200__$1);
var k_34208 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34166_34207,(0),null);
var cb_34209 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34166_34207,(1),null);
try{var G__34170_34213 = cljs.core.deref(re_frame.trace.traces);
(cb_34209.cljs$core$IFn$_invoke$arity$1 ? cb_34209.cljs$core$IFn$_invoke$arity$1(G__34170_34213) : cb_34209.call(null,G__34170_34213));
}catch (e34169){var e_34214 = e34169;
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Error thrown from trace cb",k_34208,"while storing",cljs.core.deref(re_frame.trace.traces),e_34214], 0));
}

var G__34218 = cljs.core.next(seq__34145_34200__$1);
var G__34219 = null;
var G__34220 = (0);
var G__34221 = (0);
seq__34145_34177 = G__34218;
chunk__34146_34178 = G__34219;
count__34147_34179 = G__34220;
i__34148_34180 = G__34221;
continue;
}
} else {
}
}
break;
}

return cljs.core.reset_BANG_(re_frame.trace.traces,cljs.core.PersistentVector.EMPTY);
}),re_frame.trace.debounce_time);
re_frame.trace.run_tracing_callbacks_BANG_ = (function re_frame$trace$run_tracing_callbacks_BANG_(now){
if(((cljs.core.deref(re_frame.trace.next_delivery) - (25)) < now)){
(re_frame.trace.schedule_debounce.cljs$core$IFn$_invoke$arity$0 ? re_frame.trace.schedule_debounce.cljs$core$IFn$_invoke$arity$0() : re_frame.trace.schedule_debounce.call(null));

return cljs.core.reset_BANG_(re_frame.trace.next_delivery,(now + re_frame.trace.debounce_time));
} else {
return null;
}
});

//# sourceMappingURL=re_frame.trace.js.map
