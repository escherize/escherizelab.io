goog.provide('bt.db');
goog.require('cljs.core');
goog.require('re_frame.core');
goog.require('bt.game_rules');
bt.db.fresh_game = new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"board","board",-1907017633),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"game-state","game-state",935682735),new cljs.core.Keyword(null,"place-first","place-first",735813509),new cljs.core.Keyword(null,"playing-game?","playing-game?",459856176),true,new cljs.core.Keyword(null,"view-angle","view-angle",1726857582),(27),new cljs.core.Keyword(null,"perspective-distance","perspective-distance",-1791386698),(1200)], null);
bt.db.half_game = cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(bt.db.fresh_game,new cljs.core.Keyword(null,"board","board",-1907017633),bt.game_rules.sample_board,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"game-state","game-state",935682735),new cljs.core.Keyword(null,"team-one-select-worker","team-one-select-worker",1952466786)], 0));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("bt.db","start-game!","bt.db/start-game!",-420098869),(function (_,___$1){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(bt.db.fresh_game,new cljs.core.Keyword(null,"playing-game?","playing-game?",459856176),true);
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("bt.db","start-game-debug!","bt.db/start-game-debug!",449397960),(function (_,___$1){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(bt.db.half_game,new cljs.core.Keyword(null,"playing-game?","playing-game?",459856176),true,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"debug","debug",-1608172596),true], 0));
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("bt.db","init!","bt.db/init!",-891089882),(function (_,___$1){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"playing-game?","playing-game?",459856176),false], null);
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("bt.db","build","bt.db/build",803606776),(function (db,p__105705){
var vec__105706 = p__105705;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105706,(0),null);
var x = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105706,(1),null);
var y = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105706,(2),null);
return cljs.core.update.cljs$core$IFn$_invoke$arity$5(db,new cljs.core.Keyword(null,"board","board",-1907017633),bt.game_rules.build,x,y);
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("bt.db","tile-click","bt.db/tile-click",-839277672),(function (p__105710,p__105711){
var map__105712 = p__105710;
var map__105712__$1 = (((((!((map__105712 == null))))?(((((map__105712.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105712.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105712):map__105712);
var db = map__105712__$1;
var board = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105712__$1,new cljs.core.Keyword(null,"board","board",-1907017633));
var selected = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105712__$1,new cljs.core.Keyword(null,"selected","selected",574897764));
var vec__105713 = p__105711;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105713,(0),null);
var x = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105713,(1),null);
var y = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105713,(2),null);
var map__105716 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105713,(3),null);
var map__105716__$1 = (((((!((map__105716 == null))))?(((((map__105716.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105716.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105716):map__105716);
var game_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105716__$1,new cljs.core.Keyword(null,"game-state","game-state",935682735));
var next_state = ((function (map__105712,map__105712__$1,db,board,selected,vec__105713,_,x,y,map__105716,map__105716__$1,game_state){
return (function (db__$1){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(cljs.core.update.cljs$core$IFn$_invoke$arity$3(db__$1,new cljs.core.Keyword(null,"game-state","game-state",935682735),bt.game_rules.advance),db__$1,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"error-message","error-message",1756021561)], 0));
});})(map__105712,map__105712__$1,db,board,selected,vec__105713,_,x,y,map__105716,map__105716__$1,game_state))
;
var G__105721 = game_state;
var G__105721__$1 = (((G__105721 instanceof cljs.core.Keyword))?G__105721.fqn:null);
switch (G__105721__$1) {
case "place-first":
var map__105722 = bt.game_rules.place(board,x,y,new cljs.core.Keyword("team","one","team/one",942492413));
var map__105722__$1 = (((((!((map__105722 == null))))?(((((map__105722.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105722.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105722):map__105722);
var error = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105722__$1,new cljs.core.Keyword(null,"error","error",-978969032));
var board__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105722__$1,new cljs.core.Keyword(null,"board","board",-1907017633));
if(cljs.core.truth_(error)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"error-message","error-message",1756021561),error);
} else {
return next_state(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"board","board",-1907017633),board__$1));
}

break;
case "place-second":
var map__105725 = bt.game_rules.place(board,x,y,new cljs.core.Keyword("team","one","team/one",942492413));
var map__105725__$1 = (((((!((map__105725 == null))))?(((((map__105725.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105725.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105725):map__105725);
var error = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105725__$1,new cljs.core.Keyword(null,"error","error",-978969032));
var board__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105725__$1,new cljs.core.Keyword(null,"board","board",-1907017633));
if(cljs.core.truth_(error)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"error-message","error-message",1756021561),error);
} else {
return next_state(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"board","board",-1907017633),board__$1));
}

break;
case "place-third":
var map__105728 = bt.game_rules.place(board,x,y,new cljs.core.Keyword("team","two","team/two",522109688));
var map__105728__$1 = (((((!((map__105728 == null))))?(((((map__105728.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105728.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105728):map__105728);
var error = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105728__$1,new cljs.core.Keyword(null,"error","error",-978969032));
var board__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105728__$1,new cljs.core.Keyword(null,"board","board",-1907017633));
if(cljs.core.truth_(error)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"error-message","error-message",1756021561),error);
} else {
return next_state(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"board","board",-1907017633),board__$1));
}

break;
case "place-fourth":
var map__105730 = bt.game_rules.place(board,x,y,new cljs.core.Keyword("team","two","team/two",522109688));
var map__105730__$1 = (((((!((map__105730 == null))))?(((((map__105730.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105730.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105730):map__105730);
var error = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105730__$1,new cljs.core.Keyword(null,"error","error",-978969032));
var board__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105730__$1,new cljs.core.Keyword(null,"board","board",-1907017633));
if(cljs.core.truth_(error)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"error-message","error-message",1756021561),error);
} else {
return next_state(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"board","board",-1907017633),board__$1));
}

break;
case "team-one-select-worker":
var map__105732 = bt.game_rules.select(board,x,y,new cljs.core.Keyword("team","one","team/one",942492413));
var map__105732__$1 = (((((!((map__105732 == null))))?(((((map__105732.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105732.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105732):map__105732);
var error = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105732__$1,new cljs.core.Keyword(null,"error","error",-978969032));
var selected__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105732__$1,new cljs.core.Keyword(null,"selected","selected",574897764));
if(cljs.core.truth_(error)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"error-message","error-message",1756021561),error);
} else {
return next_state(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"selected","selected",574897764),selected__$1));
}

break;
case "team-one-move-worker":
var map__105734 = bt.game_rules.move_worker(board,selected,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null));
var map__105734__$1 = (((((!((map__105734 == null))))?(((((map__105734.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105734.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105734):map__105734);
var error = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105734__$1,new cljs.core.Keyword(null,"error","error",-978969032));
var board__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105734__$1,new cljs.core.Keyword(null,"board","board",-1907017633));
var move_to = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105734__$1,new cljs.core.Keyword(null,"move-to","move-to",-127537048));
if(cljs.core.truth_(error)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"error-message","error-message",1756021561),error);
} else {
var temp__5718__auto__ = bt.game_rules.over_QMARK_(board__$1);
if(cljs.core.truth_(temp__5718__auto__)){
var winning_team = temp__5718__auto__;
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(db,new cljs.core.Keyword(null,"board","board",-1907017633),board__$1,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"game-over","game-over",-607322695),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(winning_team)," wins!"].join('')], 0));
} else {
return next_state(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"board","board",-1907017633),board__$1),new cljs.core.Keyword(null,"selected","selected",574897764),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null)));
}
}

break;
case "team-one-build":
var map__105736 = bt.game_rules.build(board,selected,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null));
var map__105736__$1 = (((((!((map__105736 == null))))?(((((map__105736.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105736.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105736):map__105736);
var error = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105736__$1,new cljs.core.Keyword(null,"error","error",-978969032));
var board__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105736__$1,new cljs.core.Keyword(null,"board","board",-1907017633));
if(cljs.core.truth_(error)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"error-message","error-message",1756021561),error);
} else {
return next_state(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"board","board",-1907017633),board__$1),new cljs.core.Keyword(null,"selected","selected",574897764)));
}

break;
case "team-two-select-worker":
var map__105738 = bt.game_rules.select(board,x,y,new cljs.core.Keyword("team","two","team/two",522109688));
var map__105738__$1 = (((((!((map__105738 == null))))?(((((map__105738.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105738.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105738):map__105738);
var error = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105738__$1,new cljs.core.Keyword(null,"error","error",-978969032));
var selected__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105738__$1,new cljs.core.Keyword(null,"selected","selected",574897764));
if(cljs.core.truth_(error)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"error-message","error-message",1756021561),error);
} else {
return next_state(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"selected","selected",574897764),selected__$1));
}

break;
case "team-two-move-worker":
var map__105740 = bt.game_rules.move_worker(board,selected,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null));
var map__105740__$1 = (((((!((map__105740 == null))))?(((((map__105740.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105740.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105740):map__105740);
var error = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105740__$1,new cljs.core.Keyword(null,"error","error",-978969032));
var board__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105740__$1,new cljs.core.Keyword(null,"board","board",-1907017633));
var move_to = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105740__$1,new cljs.core.Keyword(null,"move-to","move-to",-127537048));
if(cljs.core.truth_(error)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"error-message","error-message",1756021561),error);
} else {
var temp__5718__auto__ = bt.game_rules.over_QMARK_(board__$1);
if(cljs.core.truth_(temp__5718__auto__)){
var winning_team = temp__5718__auto__;
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(db,new cljs.core.Keyword(null,"board","board",-1907017633),board__$1,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"game-over","game-over",-607322695),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(winning_team)," wins!"].join('')], 0));
} else {
return next_state(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"board","board",-1907017633),board__$1),new cljs.core.Keyword(null,"selected","selected",574897764),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null)));
}
}

break;
case "team-two-build":
var map__105742 = bt.game_rules.build(board,selected,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null));
var map__105742__$1 = (((((!((map__105742 == null))))?(((((map__105742.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105742.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105742):map__105742);
var error = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105742__$1,new cljs.core.Keyword(null,"error","error",-978969032));
var board__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105742__$1,new cljs.core.Keyword(null,"board","board",-1907017633));
if(cljs.core.truth_(error)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"error-message","error-message",1756021561),error);
} else {
return next_state(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"board","board",-1907017633),board__$1),new cljs.core.Keyword(null,"selected","selected",574897764)));
}

break;
default:
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"error-message","error-message",1756021561),["game state: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(game_state)," not registered in ::tile-click"].join(''));

}
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("bt.db","set-view-angle","bt.db/set-view-angle",1270316349),(function (db,p__105744){
var vec__105745 = p__105744;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105745,(0),null);
var view_angle = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105745,(1),null);
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"view-angle","view-angle",1726857582),view_angle);
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("bt.db","set-perspective-distance","bt.db/set-perspective-distance",1532889519),(function (db,p__105748){
var vec__105749 = p__105748;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105749,(0),null);
var view_angle = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105749,(1),null);
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"perspective-distance","perspective-distance",-1791386698),view_angle);
}));
var G__105752_105817 = new cljs.core.Keyword("bt.db","board","bt.db/board",-1997560839);
var G__105753_105818 = ((function (G__105752_105817){
return (function (db,_){
return new cljs.core.Keyword(null,"board","board",-1907017633).cljs$core$IFn$_invoke$arity$1(db);
});})(G__105752_105817))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__105752_105817,G__105753_105818) : re_frame.core.reg_sub.call(null,G__105752_105817,G__105753_105818));
var G__105754_105819 = new cljs.core.Keyword("bt.db","debug","bt.db/debug",-633341834);
var G__105755_105820 = ((function (G__105754_105819){
return (function (db,_){
return new cljs.core.Keyword(null,"debug","debug",-1608172596).cljs$core$IFn$_invoke$arity$1(db);
});})(G__105754_105819))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__105754_105819,G__105755_105820) : re_frame.core.reg_sub.call(null,G__105754_105819,G__105755_105820));
var G__105756_105822 = new cljs.core.Keyword("bt.db","error-message","bt.db/error-message",1595307935);
var G__105757_105823 = ((function (G__105756_105822){
return (function (db,_){
return new cljs.core.Keyword(null,"error-message","error-message",1756021561).cljs$core$IFn$_invoke$arity$1(db);
});})(G__105756_105822))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__105756_105822,G__105757_105823) : re_frame.core.reg_sub.call(null,G__105756_105822,G__105757_105823));
var G__105758_105824 = new cljs.core.Keyword("bt.db","game-over","bt.db/game-over",-634918369);
var G__105759_105825 = ((function (G__105758_105824){
return (function (db,_){
return new cljs.core.Keyword(null,"game-over","game-over",-607322695).cljs$core$IFn$_invoke$arity$1(db);
});})(G__105758_105824))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__105758_105824,G__105759_105825) : re_frame.core.reg_sub.call(null,G__105758_105824,G__105759_105825));
var G__105760_105826 = new cljs.core.Keyword("bt.db","game-state","bt.db/game-state",543088149);
var G__105761_105827 = ((function (G__105760_105826){
return (function (db,_){
return new cljs.core.Keyword(null,"game-state","game-state",935682735).cljs$core$IFn$_invoke$arity$1(db);
});})(G__105760_105826))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__105760_105826,G__105761_105827) : re_frame.core.reg_sub.call(null,G__105760_105826,G__105761_105827));
var G__105762_105828 = new cljs.core.Keyword("bt.db","perspective-distance","bt.db/perspective-distance",-1882207200);
var G__105763_105829 = ((function (G__105762_105828){
return (function (db,_){
return new cljs.core.Keyword(null,"perspective-distance","perspective-distance",-1791386698).cljs$core$IFn$_invoke$arity$1(db);
});})(G__105762_105828))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__105762_105828,G__105763_105829) : re_frame.core.reg_sub.call(null,G__105762_105828,G__105763_105829));
var G__105764_105830 = new cljs.core.Keyword("bt.db","playing-game?","bt.db/playing-game?",252464458);
var G__105765_105831 = ((function (G__105764_105830){
return (function (db,_){
return new cljs.core.Keyword(null,"playing-game?","playing-game?",459856176).cljs$core$IFn$_invoke$arity$1(db);
});})(G__105764_105830))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__105764_105830,G__105765_105831) : re_frame.core.reg_sub.call(null,G__105764_105830,G__105765_105831));
var G__105766_105833 = new cljs.core.Keyword("bt.db","selected-player","bt.db/selected-player",1073813476);
var G__105767_105834 = ((function (G__105766_105833){
return (function (db,_){
return new cljs.core.Keyword(null,"selected","selected",574897764).cljs$core$IFn$_invoke$arity$1(db);
});})(G__105766_105833))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__105766_105833,G__105767_105834) : re_frame.core.reg_sub.call(null,G__105766_105833,G__105767_105834));
var G__105768_105837 = new cljs.core.Keyword("bt.db","view-angle","bt.db/view-angle",-1857327732);
var G__105769_105838 = ((function (G__105768_105837){
return (function (db,_){
return new cljs.core.Keyword(null,"view-angle","view-angle",1726857582).cljs$core$IFn$_invoke$arity$1(db);
});})(G__105768_105837))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__105768_105837,G__105769_105838) : re_frame.core.reg_sub.call(null,G__105768_105837,G__105769_105838));
var G__105770_105839 = new cljs.core.Keyword("bt.db","perspective-angle","bt.db/perspective-angle",669347638);
var G__105771_105840 = ((function (G__105770_105839){
return (function (db,_){
var view = new cljs.core.Keyword(null,"view-angle","view-angle",1726857582).cljs$core$IFn$_invoke$arity$1(db);
var perspective = new cljs.core.Keyword(null,"perspective-distance","perspective-distance",-1791386698).cljs$core$IFn$_invoke$arity$1(db);
return ["perspective(",cljs.core.str.cljs$core$IFn$_invoke$arity$1(perspective),"px) ","rotateX(",cljs.core.str.cljs$core$IFn$_invoke$arity$1(view),"deg) ","rotateZ(",cljs.core.str.cljs$core$IFn$_invoke$arity$1(view),"deg)"].join('');
});})(G__105770_105839))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__105770_105839,G__105771_105840) : re_frame.core.reg_sub.call(null,G__105770_105839,G__105771_105840));
var G__105772_105842 = new cljs.core.Keyword("bt.db","board-elements","bt.db/board-elements",-1513074900);
var G__105773_105843 = ((function (G__105772_105842){
return (function (db,_){
return bt.game_rules.__GT_elements(new cljs.core.Keyword(null,"board","board",-1907017633).cljs$core$IFn$_invoke$arity$1(db));
});})(G__105772_105842))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__105772_105842,G__105773_105843) : re_frame.core.reg_sub.call(null,G__105772_105842,G__105773_105843));
var G__105774_105847 = new cljs.core.Keyword("bt.db","cell-info","bt.db/cell-info",1819284458);
var G__105775_105848 = ((function (G__105774_105847){
return (function (p__105776,p__105777){
var map__105778 = p__105776;
var map__105778__$1 = (((((!((map__105778 == null))))?(((((map__105778.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__105778.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__105778):map__105778);
var db = map__105778__$1;
var game_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105778__$1,new cljs.core.Keyword(null,"game-state","game-state",935682735));
var board = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__105778__$1,new cljs.core.Keyword(null,"board","board",-1907017633));
var vec__105779 = p__105777;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105779,(0),null);
var x = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105779,(1),null);
var y = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105779,(2),null);
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"height","height",1025178622),bt.game_rules.height_at(new cljs.core.Keyword(null,"board","board",-1907017633).cljs$core$IFn$_invoke$arity$1(db),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [x,y], null))], null);
});})(G__105774_105847))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__105774_105847,G__105775_105848) : re_frame.core.reg_sub.call(null,G__105774_105847,G__105775_105848));
var G__105784_105855 = new cljs.core.Keyword("bt.db","turn-info","bt.db/turn-info",180891285);
var G__105785_105856 = ((function (G__105784_105855){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__105787 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","selected-player","bt.db/selected-player",1073813476)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__105787) : re_frame.core.subscribe.call(null,G__105787));
})(),(function (){var G__105788 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","game-state","bt.db/game-state",543088149)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__105788) : re_frame.core.subscribe.call(null,G__105788));
})(),(function (){var G__105789 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("bt.db","board","bt.db/board",-1997560839)], null);
return (re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1 ? re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(G__105789) : re_frame.core.subscribe.call(null,G__105789));
})()], null);
});})(G__105784_105855))
;
var G__105786_105857 = ((function (G__105784_105855,G__105785_105856){
return (function (p__105790){
var vec__105791 = p__105790;
var selected = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105791,(0),null);
var game_state = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105791,(1),null);
var board = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__105791,(2),null);
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([bt.game_rules.state__GT_info(game_state),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"board","board",-1907017633),board,new cljs.core.Keyword(null,"game-state","game-state",935682735),game_state], null),(cljs.core.truth_(selected)?new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"selected","selected",574897764),selected], null):null)], 0));
});})(G__105784_105855,G__105785_105856))
;
(re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$3 ? re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$3(G__105784_105855,G__105785_105856,G__105786_105857) : re_frame.core.reg_sub.call(null,G__105784_105855,G__105785_105856,G__105786_105857));

//# sourceMappingURL=bt.db.js.map
