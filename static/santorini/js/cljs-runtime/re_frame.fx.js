goog.provide('re_frame.fx');
goog.require('cljs.core');
goog.require('re_frame.router');
goog.require('re_frame.db');
goog.require('re_frame.interceptor');
goog.require('re_frame.interop');
goog.require('re_frame.events');
goog.require('re_frame.registrar');
goog.require('re_frame.loggers');
goog.require('re_frame.trace');
re_frame.fx.kind = new cljs.core.Keyword(null,"fx","fx",-1237829572);
if(cljs.core.truth_((re_frame.registrar.kinds.cljs$core$IFn$_invoke$arity$1 ? re_frame.registrar.kinds.cljs$core$IFn$_invoke$arity$1(re_frame.fx.kind) : re_frame.registrar.kinds.call(null,re_frame.fx.kind)))){
} else {
throw (new Error("Assert failed: (re-frame.registrar/kinds kind)"));
}
/**
 * Register the given effect `handler` for the given `id`.
 * 
 *   `id` is keyword, often namespaced.
 *   `handler` is a side-effecting function which takes a single argument and whose return
 *   value is ignored.
 * 
 *   Example Use
 *   -----------
 * 
 *   First, registration ... associate `:effect2` with a handler.
 * 
 *   (reg-fx
 *   :effect2
 *   (fn [value]
 *      ... do something side-effect-y))
 * 
 *   Then, later, if an event handler were to return this effects map ...
 * 
 *   {...
 * :effect2  [1 2]}
 * 
 * ... then the `handler` `fn` we registered previously, using `reg-fx`, will be
 * called with an argument of `[1 2]`.
 */
re_frame.fx.reg_fx = (function re_frame$fx$reg_fx(id,handler){
return re_frame.registrar.register_handler(re_frame.fx.kind,id,handler);
});
/**
 * An interceptor whose `:after` actions the contents of `:effects`. As a result,
 *   this interceptor is Domino 3.
 * 
 *   This interceptor is silently added (by reg-event-db etc) to the front of
 *   interceptor chains for all events.
 * 
 *   For each key in `:effects` (a map), it calls the registered `effects handler`
 *   (see `reg-fx` for registration of effect handlers).
 * 
 *   So, if `:effects` was:
 *    {:dispatch  [:hello 42]
 *     :db        {...}
 *     :undo      "set flag"}
 * 
 *   it will call the registered effect handlers for each of the map's keys:
 *   `:dispatch`, `:undo` and `:db`. When calling each handler, provides the map
 *   value for that key - so in the example above the effect handler for :dispatch
 *   will be given one arg `[:hello 42]`.
 * 
 *   You cannot rely on the ordering in which effects are executed.
 */
re_frame.fx.do_fx = re_frame.interceptor.__GT_interceptor.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"do-fx","do-fx",1194163050),new cljs.core.Keyword(null,"after","after",594996914),(function re_frame$fx$do_fx_after(context){
if(re_frame.trace.is_trace_enabled_QMARK_()){
var _STAR_current_trace_STAR__orig_val__34426 = re_frame.trace._STAR_current_trace_STAR_;
var _STAR_current_trace_STAR__temp_val__34427 = re_frame.trace.start_trace(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op-type","op-type",-1636141668),new cljs.core.Keyword("event","do-fx","event/do-fx",1357330452)], null));
re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__temp_val__34427;

try{try{var seq__34428 = cljs.core.seq(new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context));
var chunk__34429 = null;
var count__34430 = (0);
var i__34431 = (0);
while(true){
if((i__34431 < count__34430)){
var vec__34438 = chunk__34429.cljs$core$IIndexed$_nth$arity$2(null,i__34431);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34438,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34438,(1),null);
var temp__5718__auto___34485 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5718__auto___34485)){
var effect_fn_34486 = temp__5718__auto___34485;
(effect_fn_34486.cljs$core$IFn$_invoke$arity$1 ? effect_fn_34486.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_34486.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__34487 = seq__34428;
var G__34488 = chunk__34429;
var G__34489 = count__34430;
var G__34490 = (i__34431 + (1));
seq__34428 = G__34487;
chunk__34429 = G__34488;
count__34430 = G__34489;
i__34431 = G__34490;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq(seq__34428);
if(temp__5720__auto__){
var seq__34428__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34428__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__34428__$1);
var G__34495 = cljs.core.chunk_rest(seq__34428__$1);
var G__34496 = c__4550__auto__;
var G__34497 = cljs.core.count(c__4550__auto__);
var G__34498 = (0);
seq__34428 = G__34495;
chunk__34429 = G__34496;
count__34430 = G__34497;
i__34431 = G__34498;
continue;
} else {
var vec__34441 = cljs.core.first(seq__34428__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34441,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34441,(1),null);
var temp__5718__auto___34503 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5718__auto___34503)){
var effect_fn_34504 = temp__5718__auto___34503;
(effect_fn_34504.cljs$core$IFn$_invoke$arity$1 ? effect_fn_34504.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_34504.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__34505 = cljs.core.next(seq__34428__$1);
var G__34506 = null;
var G__34507 = (0);
var G__34508 = (0);
seq__34428 = G__34505;
chunk__34429 = G__34506;
count__34430 = G__34507;
i__34431 = G__34508;
continue;
}
} else {
return null;
}
}
break;
}
}finally {if(re_frame.trace.is_trace_enabled_QMARK_()){
var end__28205__auto___34513 = re_frame.interop.now();
var duration__28206__auto___34514 = (end__28205__auto___34513 - new cljs.core.Keyword(null,"start","start",-355208981).cljs$core$IFn$_invoke$arity$1(re_frame.trace._STAR_current_trace_STAR_));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(re_frame.trace.traces,cljs.core.conj,cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(re_frame.trace._STAR_current_trace_STAR_,new cljs.core.Keyword(null,"duration","duration",1444101068),duration__28206__auto___34514,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"end","end",-268185958),re_frame.interop.now()], 0)));

re_frame.trace.run_tracing_callbacks_BANG_(end__28205__auto___34513);
} else {
}
}}finally {re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__orig_val__34426;
}} else {
var seq__34444 = cljs.core.seq(new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context));
var chunk__34445 = null;
var count__34446 = (0);
var i__34447 = (0);
while(true){
if((i__34447 < count__34446)){
var vec__34454 = chunk__34445.cljs$core$IIndexed$_nth$arity$2(null,i__34447);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34454,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34454,(1),null);
var temp__5718__auto___34515 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5718__auto___34515)){
var effect_fn_34516 = temp__5718__auto___34515;
(effect_fn_34516.cljs$core$IFn$_invoke$arity$1 ? effect_fn_34516.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_34516.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__34517 = seq__34444;
var G__34518 = chunk__34445;
var G__34519 = count__34446;
var G__34520 = (i__34447 + (1));
seq__34444 = G__34517;
chunk__34445 = G__34518;
count__34446 = G__34519;
i__34447 = G__34520;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq(seq__34444);
if(temp__5720__auto__){
var seq__34444__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34444__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__34444__$1);
var G__34521 = cljs.core.chunk_rest(seq__34444__$1);
var G__34522 = c__4550__auto__;
var G__34523 = cljs.core.count(c__4550__auto__);
var G__34524 = (0);
seq__34444 = G__34521;
chunk__34445 = G__34522;
count__34446 = G__34523;
i__34447 = G__34524;
continue;
} else {
var vec__34457 = cljs.core.first(seq__34444__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34457,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34457,(1),null);
var temp__5718__auto___34525 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5718__auto___34525)){
var effect_fn_34526 = temp__5718__auto___34525;
(effect_fn_34526.cljs$core$IFn$_invoke$arity$1 ? effect_fn_34526.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_34526.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__34527 = cljs.core.next(seq__34444__$1);
var G__34528 = null;
var G__34529 = (0);
var G__34530 = (0);
seq__34444 = G__34527;
chunk__34445 = G__34528;
count__34446 = G__34529;
i__34447 = G__34530;
continue;
}
} else {
return null;
}
}
break;
}
}
})], 0));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch-later","dispatch-later",291951390),(function (value){
var seq__34460 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,value));
var chunk__34461 = null;
var count__34462 = (0);
var i__34463 = (0);
while(true){
if((i__34463 < count__34462)){
var map__34469 = chunk__34461.cljs$core$IIndexed$_nth$arity$2(null,i__34463);
var map__34469__$1 = (((((!((map__34469 == null))))?(((((map__34469.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__34469.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__34469):map__34469);
var effect = map__34469__$1;
var ms = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34469__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34469__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if(((cljs.core.empty_QMARK_(dispatch)) || ((!(typeof ms === 'number'))))){
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-later value:",effect], 0));
} else {
re_frame.interop.set_timeout_BANG_(((function (seq__34460,chunk__34461,count__34462,i__34463,map__34469,map__34469__$1,effect,ms,dispatch){
return (function (){
return re_frame.router.dispatch(dispatch);
});})(seq__34460,chunk__34461,count__34462,i__34463,map__34469,map__34469__$1,effect,ms,dispatch))
,ms);
}


var G__34532 = seq__34460;
var G__34533 = chunk__34461;
var G__34534 = count__34462;
var G__34535 = (i__34463 + (1));
seq__34460 = G__34532;
chunk__34461 = G__34533;
count__34462 = G__34534;
i__34463 = G__34535;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq(seq__34460);
if(temp__5720__auto__){
var seq__34460__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34460__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__34460__$1);
var G__34537 = cljs.core.chunk_rest(seq__34460__$1);
var G__34538 = c__4550__auto__;
var G__34539 = cljs.core.count(c__4550__auto__);
var G__34540 = (0);
seq__34460 = G__34537;
chunk__34461 = G__34538;
count__34462 = G__34539;
i__34463 = G__34540;
continue;
} else {
var map__34471 = cljs.core.first(seq__34460__$1);
var map__34471__$1 = (((((!((map__34471 == null))))?(((((map__34471.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__34471.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__34471):map__34471);
var effect = map__34471__$1;
var ms = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34471__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34471__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if(((cljs.core.empty_QMARK_(dispatch)) || ((!(typeof ms === 'number'))))){
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-later value:",effect], 0));
} else {
re_frame.interop.set_timeout_BANG_(((function (seq__34460,chunk__34461,count__34462,i__34463,map__34471,map__34471__$1,effect,ms,dispatch,seq__34460__$1,temp__5720__auto__){
return (function (){
return re_frame.router.dispatch(dispatch);
});})(seq__34460,chunk__34461,count__34462,i__34463,map__34471,map__34471__$1,effect,ms,dispatch,seq__34460__$1,temp__5720__auto__))
,ms);
}


var G__34543 = cljs.core.next(seq__34460__$1);
var G__34544 = null;
var G__34545 = (0);
var G__34546 = (0);
seq__34460 = G__34543;
chunk__34461 = G__34544;
count__34462 = G__34545;
i__34463 = G__34546;
continue;
}
} else {
return null;
}
}
break;
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch","dispatch",1319337009),(function (value){
if((!(cljs.core.vector_QMARK_(value)))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch value. Expected a vector, but got:",value], 0));
} else {
return re_frame.router.dispatch(value);
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch-n","dispatch-n",-504469236),(function (value){
if((!(cljs.core.sequential_QMARK_(value)))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-n value. Expected a collection, got got:",value], 0));
} else {
var seq__34473 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,value));
var chunk__34474 = null;
var count__34475 = (0);
var i__34476 = (0);
while(true){
if((i__34476 < count__34475)){
var event = chunk__34474.cljs$core$IIndexed$_nth$arity$2(null,i__34476);
re_frame.router.dispatch(event);


var G__34549 = seq__34473;
var G__34550 = chunk__34474;
var G__34551 = count__34475;
var G__34552 = (i__34476 + (1));
seq__34473 = G__34549;
chunk__34474 = G__34550;
count__34475 = G__34551;
i__34476 = G__34552;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq(seq__34473);
if(temp__5720__auto__){
var seq__34473__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34473__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__34473__$1);
var G__34553 = cljs.core.chunk_rest(seq__34473__$1);
var G__34554 = c__4550__auto__;
var G__34555 = cljs.core.count(c__4550__auto__);
var G__34556 = (0);
seq__34473 = G__34553;
chunk__34474 = G__34554;
count__34475 = G__34555;
i__34476 = G__34556;
continue;
} else {
var event = cljs.core.first(seq__34473__$1);
re_frame.router.dispatch(event);


var G__34557 = cljs.core.next(seq__34473__$1);
var G__34558 = null;
var G__34559 = (0);
var G__34560 = (0);
seq__34473 = G__34557;
chunk__34474 = G__34558;
count__34475 = G__34559;
i__34476 = G__34560;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"deregister-event-handler","deregister-event-handler",-1096518994),(function (value){
var clear_event = cljs.core.partial.cljs$core$IFn$_invoke$arity$2(re_frame.registrar.clear_handlers,re_frame.events.kind);
if(cljs.core.sequential_QMARK_(value)){
var seq__34480 = cljs.core.seq(value);
var chunk__34481 = null;
var count__34482 = (0);
var i__34483 = (0);
while(true){
if((i__34483 < count__34482)){
var event = chunk__34481.cljs$core$IIndexed$_nth$arity$2(null,i__34483);
(clear_event.cljs$core$IFn$_invoke$arity$1 ? clear_event.cljs$core$IFn$_invoke$arity$1(event) : clear_event.call(null,event));


var G__34561 = seq__34480;
var G__34562 = chunk__34481;
var G__34563 = count__34482;
var G__34564 = (i__34483 + (1));
seq__34480 = G__34561;
chunk__34481 = G__34562;
count__34482 = G__34563;
i__34483 = G__34564;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq(seq__34480);
if(temp__5720__auto__){
var seq__34480__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34480__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__34480__$1);
var G__34565 = cljs.core.chunk_rest(seq__34480__$1);
var G__34566 = c__4550__auto__;
var G__34567 = cljs.core.count(c__4550__auto__);
var G__34568 = (0);
seq__34480 = G__34565;
chunk__34481 = G__34566;
count__34482 = G__34567;
i__34483 = G__34568;
continue;
} else {
var event = cljs.core.first(seq__34480__$1);
(clear_event.cljs$core$IFn$_invoke$arity$1 ? clear_event.cljs$core$IFn$_invoke$arity$1(event) : clear_event.call(null,event));


var G__34569 = cljs.core.next(seq__34480__$1);
var G__34570 = null;
var G__34571 = (0);
var G__34572 = (0);
seq__34480 = G__34569;
chunk__34481 = G__34570;
count__34482 = G__34571;
i__34483 = G__34572;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return (clear_event.cljs$core$IFn$_invoke$arity$1 ? clear_event.cljs$core$IFn$_invoke$arity$1(value) : clear_event.call(null,value));
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"db","db",993250759),(function (value){
if((!((cljs.core.deref(re_frame.db.app_db) === value)))){
return cljs.core.reset_BANG_(re_frame.db.app_db,value);
} else {
return null;
}
}));

//# sourceMappingURL=re_frame.fx.js.map
