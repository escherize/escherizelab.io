goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.ioc_helpers');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__30700 = arguments.length;
switch (G__30700) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(f,true);
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async30701 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30701 = (function (f,blockable,meta30702){
this.f = f;
this.blockable = blockable;
this.meta30702 = meta30702;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async30701.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30703,meta30702__$1){
var self__ = this;
var _30703__$1 = this;
return (new cljs.core.async.t_cljs$core$async30701(self__.f,self__.blockable,meta30702__$1));
});

cljs.core.async.t_cljs$core$async30701.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30703){
var self__ = this;
var _30703__$1 = this;
return self__.meta30702;
});

cljs.core.async.t_cljs$core$async30701.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async30701.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async30701.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
});

cljs.core.async.t_cljs$core$async30701.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
});

cljs.core.async.t_cljs$core$async30701.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta30702","meta30702",-1908179728,null)], null);
});

cljs.core.async.t_cljs$core$async30701.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async30701.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30701";

cljs.core.async.t_cljs$core$async30701.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async30701");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async30701.
 */
cljs.core.async.__GT_t_cljs$core$async30701 = (function cljs$core$async$__GT_t_cljs$core$async30701(f__$1,blockable__$1,meta30702){
return (new cljs.core.async.t_cljs$core$async30701(f__$1,blockable__$1,meta30702));
});

}

return (new cljs.core.async.t_cljs$core$async30701(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
});

cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2;

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer(n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__30727 = arguments.length;
switch (G__30727) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,xform,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.cljs$core$IFn$_invoke$arity$3(((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer(buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
});

cljs.core.async.chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__30741 = arguments.length;
switch (G__30741) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1(null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2(xform,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(cljs.core.async.impl.buffers.promise_buffer(),xform,ex_handler);
});

cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout(msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__30760 = arguments.length;
switch (G__30760) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3(port,fn1,true);
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(ret)){
var val_33186 = cljs.core.deref(ret);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_33186) : fn1.call(null,val_33186));
} else {
cljs.core.async.impl.dispatch.run(((function (val_33186,ret){
return (function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_33186) : fn1.call(null,val_33186));
});})(val_33186,ret))
);
}
} else {
}

return null;
});

cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3;

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn1 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn1 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__30781 = arguments.length;
switch (G__30781) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5718__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5718__auto__)){
var ret = temp__5718__auto__;
return cljs.core.deref(ret);
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4(port,val,fn1,true);
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5718__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(temp__5718__auto__)){
var retb = temp__5718__auto__;
var ret = cljs.core.deref(retb);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
} else {
cljs.core.async.impl.dispatch.run(((function (ret,retb,temp__5718__auto__){
return (function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
});})(ret,retb,temp__5718__auto__))
);
}

return ret;
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4;

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_(port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__4607__auto___33202 = n;
var x_33203 = (0);
while(true){
if((x_33203 < n__4607__auto___33202)){
(a[x_33203] = (0));

var G__33204 = (x_33203 + (1));
x_33203 = G__33204;
continue;
} else {
}
break;
}

var i = (1);
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(i,n)){
return a;
} else {
var j = cljs.core.rand_int(i);
(a[i] = (a[j]));

(a[j] = i);

var G__33207 = (i + (1));
i = G__33207;
continue;
}
break;
}
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async30833 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30833 = (function (flag,meta30834){
this.flag = flag;
this.meta30834 = meta30834;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async30833.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_30835,meta30834__$1){
var self__ = this;
var _30835__$1 = this;
return (new cljs.core.async.t_cljs$core$async30833(self__.flag,meta30834__$1));
});})(flag))
;

cljs.core.async.t_cljs$core$async30833.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_30835){
var self__ = this;
var _30835__$1 = this;
return self__.meta30834;
});})(flag))
;

cljs.core.async.t_cljs$core$async30833.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async30833.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref(self__.flag);
});})(flag))
;

cljs.core.async.t_cljs$core$async30833.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async30833.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.flag,null);

return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async30833.getBasis = ((function (flag){
return (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta30834","meta30834",-184139134,null)], null);
});})(flag))
;

cljs.core.async.t_cljs$core$async30833.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async30833.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30833";

cljs.core.async.t_cljs$core$async30833.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async30833");
});})(flag))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async30833.
 */
cljs.core.async.__GT_t_cljs$core$async30833 = ((function (flag){
return (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async30833(flag__$1,meta30834){
return (new cljs.core.async.t_cljs$core$async30833(flag__$1,meta30834));
});})(flag))
;

}

return (new cljs.core.async.t_cljs$core$async30833(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async30838 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30838 = (function (flag,cb,meta30839){
this.flag = flag;
this.cb = cb;
this.meta30839 = meta30839;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async30838.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30840,meta30839__$1){
var self__ = this;
var _30840__$1 = this;
return (new cljs.core.async.t_cljs$core$async30838(self__.flag,self__.cb,meta30839__$1));
});

cljs.core.async.t_cljs$core$async30838.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30840){
var self__ = this;
var _30840__$1 = this;
return self__.meta30839;
});

cljs.core.async.t_cljs$core$async30838.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async30838.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.flag);
});

cljs.core.async.t_cljs$core$async30838.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async30838.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit(self__.flag);

return self__.cb;
});

cljs.core.async.t_cljs$core$async30838.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta30839","meta30839",-1779067739,null)], null);
});

cljs.core.async.t_cljs$core$async30838.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async30838.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30838";

cljs.core.async.t_cljs$core$async30838.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async30838");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async30838.
 */
cljs.core.async.__GT_t_cljs$core$async30838 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async30838(flag__$1,cb__$1,meta30839){
return (new cljs.core.async.t_cljs$core$async30838(flag__$1,cb__$1,meta30839));
});

}

return (new cljs.core.async.t_cljs$core$async30838(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
var flag = cljs.core.async.alt_flag();
var n = cljs.core.count(ports);
var idxs = cljs.core.async.random_array(n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ports,idx);
var wport = ((cljs.core.vector_QMARK_(port))?(port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((0)) : port.call(null,(0))):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = (port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((1)) : port.call(null,(1)));
return cljs.core.async.impl.protocols.put_BANG_(wport,val,cljs.core.async.alt_handler(flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__30842_SHARP_){
var G__30859 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__30842_SHARP_,wport], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__30859) : fret.call(null,G__30859));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.alt_handler(flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__30843_SHARP_){
var G__30863 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__30843_SHARP_,port], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__30863) : fret.call(null,G__30863));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref(vbox),(function (){var or__4131__auto__ = wport;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return port;
}
})()], null));
} else {
var G__33228 = (i + (1));
i = G__33228;
continue;
}
} else {
return null;
}
break;
}
})();
var or__4131__auto__ = ret;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
if(cljs.core.contains_QMARK_(opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5720__auto__ = (function (){var and__4120__auto__ = flag.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1(null);
if(cljs.core.truth_(and__4120__auto__)){
return flag.cljs$core$async$impl$protocols$Handler$commit$arity$1(null);
} else {
return and__4120__auto__;
}
})();
if(cljs.core.truth_(temp__5720__auto__)){
var got = temp__5720__auto__;
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__4736__auto__ = [];
var len__4730__auto___33235 = arguments.length;
var i__4731__auto___33236 = (0);
while(true){
if((i__4731__auto___33236 < len__4730__auto___33235)){
args__4736__auto__.push((arguments[i__4731__auto___33236]));

var G__33238 = (i__4731__auto___33236 + (1));
i__4731__auto___33236 = G__33238;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__30888){
var map__30889 = p__30888;
var map__30889__$1 = (((((!((map__30889 == null))))?(((((map__30889.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__30889.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__30889):map__30889);
var opts = map__30889__$1;
throw (new Error("alts! used not in (go ...) block"));
});

cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq30874){
var G__30875 = cljs.core.first(seq30874);
var seq30874__$1 = cljs.core.next(seq30874);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__30875,seq30874__$1);
});

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__30898 = arguments.length;
switch (G__30898) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3(from,to,true);
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__30611__auto___33245 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto___33245){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto___33245){
return (function (state_30937){
var state_val_30939 = (state_30937[(1)]);
if((state_val_30939 === (7))){
var inst_30930 = (state_30937[(2)]);
var state_30937__$1 = state_30937;
var statearr_30940_33256 = state_30937__$1;
(statearr_30940_33256[(2)] = inst_30930);

(statearr_30940_33256[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30939 === (1))){
var state_30937__$1 = state_30937;
var statearr_30941_33259 = state_30937__$1;
(statearr_30941_33259[(2)] = null);

(statearr_30941_33259[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30939 === (4))){
var inst_30910 = (state_30937[(7)]);
var inst_30910__$1 = (state_30937[(2)]);
var inst_30911 = (inst_30910__$1 == null);
var state_30937__$1 = (function (){var statearr_30942 = state_30937;
(statearr_30942[(7)] = inst_30910__$1);

return statearr_30942;
})();
if(cljs.core.truth_(inst_30911)){
var statearr_30943_33262 = state_30937__$1;
(statearr_30943_33262[(1)] = (5));

} else {
var statearr_30944_33263 = state_30937__$1;
(statearr_30944_33263[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30939 === (13))){
var state_30937__$1 = state_30937;
var statearr_30945_33264 = state_30937__$1;
(statearr_30945_33264[(2)] = null);

(statearr_30945_33264[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30939 === (6))){
var inst_30910 = (state_30937[(7)]);
var state_30937__$1 = state_30937;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_30937__$1,(11),to,inst_30910);
} else {
if((state_val_30939 === (3))){
var inst_30933 = (state_30937[(2)]);
var state_30937__$1 = state_30937;
return cljs.core.async.impl.ioc_helpers.return_chan(state_30937__$1,inst_30933);
} else {
if((state_val_30939 === (12))){
var state_30937__$1 = state_30937;
var statearr_30958_33267 = state_30937__$1;
(statearr_30958_33267[(2)] = null);

(statearr_30958_33267[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30939 === (2))){
var state_30937__$1 = state_30937;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_30937__$1,(4),from);
} else {
if((state_val_30939 === (11))){
var inst_30920 = (state_30937[(2)]);
var state_30937__$1 = state_30937;
if(cljs.core.truth_(inst_30920)){
var statearr_30959_33270 = state_30937__$1;
(statearr_30959_33270[(1)] = (12));

} else {
var statearr_30960_33271 = state_30937__$1;
(statearr_30960_33271[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30939 === (9))){
var state_30937__$1 = state_30937;
var statearr_30961_33272 = state_30937__$1;
(statearr_30961_33272[(2)] = null);

(statearr_30961_33272[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30939 === (5))){
var state_30937__$1 = state_30937;
if(cljs.core.truth_(close_QMARK_)){
var statearr_30962_33273 = state_30937__$1;
(statearr_30962_33273[(1)] = (8));

} else {
var statearr_30963_33274 = state_30937__$1;
(statearr_30963_33274[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30939 === (14))){
var inst_30928 = (state_30937[(2)]);
var state_30937__$1 = state_30937;
var statearr_30964_33275 = state_30937__$1;
(statearr_30964_33275[(2)] = inst_30928);

(statearr_30964_33275[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30939 === (10))){
var inst_30917 = (state_30937[(2)]);
var state_30937__$1 = state_30937;
var statearr_30965_33276 = state_30937__$1;
(statearr_30965_33276[(2)] = inst_30917);

(statearr_30965_33276[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30939 === (8))){
var inst_30914 = cljs.core.async.close_BANG_(to);
var state_30937__$1 = state_30937;
var statearr_30966_33277 = state_30937__$1;
(statearr_30966_33277[(2)] = inst_30914);

(statearr_30966_33277[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto___33245))
;
return ((function (switch__30294__auto__,c__30611__auto___33245){
return (function() {
var cljs$core$async$state_machine__30295__auto__ = null;
var cljs$core$async$state_machine__30295__auto____0 = (function (){
var statearr_30967 = [null,null,null,null,null,null,null,null];
(statearr_30967[(0)] = cljs$core$async$state_machine__30295__auto__);

(statearr_30967[(1)] = (1));

return statearr_30967;
});
var cljs$core$async$state_machine__30295__auto____1 = (function (state_30937){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_30937);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e30968){if((e30968 instanceof Object)){
var ex__30298__auto__ = e30968;
var statearr_30969_33278 = state_30937;
(statearr_30969_33278[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_30937);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30968;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33279 = state_30937;
state_30937 = G__33279;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$state_machine__30295__auto__ = function(state_30937){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30295__auto____1.call(this,state_30937);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30295__auto____0;
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30295__auto____1;
return cljs$core$async$state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto___33245))
})();
var state__30613__auto__ = (function (){var statearr_30970 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_30970[(6)] = c__30611__auto___33245);

return statearr_30970;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto___33245))
);


return to;
});

cljs.core.async.pipe.cljs$lang$maxFixedArity = 3;

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var results = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var process = ((function (jobs,results){
return (function (p__30975){
var vec__30976 = p__30975;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30976,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30976,(1),null);
var job = vec__30976;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((1),xf,ex_handler);
var c__30611__auto___33280 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto___33280,res,vec__30976,v,p,job,jobs,results){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto___33280,res,vec__30976,v,p,job,jobs,results){
return (function (state_30983){
var state_val_30984 = (state_30983[(1)]);
if((state_val_30984 === (1))){
var state_30983__$1 = state_30983;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_30983__$1,(2),res,v);
} else {
if((state_val_30984 === (2))){
var inst_30980 = (state_30983[(2)]);
var inst_30981 = cljs.core.async.close_BANG_(res);
var state_30983__$1 = (function (){var statearr_30985 = state_30983;
(statearr_30985[(7)] = inst_30980);

return statearr_30985;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_30983__$1,inst_30981);
} else {
return null;
}
}
});})(c__30611__auto___33280,res,vec__30976,v,p,job,jobs,results))
;
return ((function (switch__30294__auto__,c__30611__auto___33280,res,vec__30976,v,p,job,jobs,results){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____0 = (function (){
var statearr_30986 = [null,null,null,null,null,null,null,null];
(statearr_30986[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__);

(statearr_30986[(1)] = (1));

return statearr_30986;
});
var cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____1 = (function (state_30983){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_30983);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e30989){if((e30989 instanceof Object)){
var ex__30298__auto__ = e30989;
var statearr_30990_33288 = state_30983;
(statearr_30990_33288[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_30983);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30989;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33290 = state_30983;
state_30983 = G__33290;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__ = function(state_30983){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____1.call(this,state_30983);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto___33280,res,vec__30976,v,p,job,jobs,results))
})();
var state__30613__auto__ = (function (){var statearr_30993 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_30993[(6)] = c__30611__auto___33280);

return statearr_30993;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto___33280,res,vec__30976,v,p,job,jobs,results))
);


cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});})(jobs,results))
;
var async = ((function (jobs,results,process){
return (function (p__30996){
var vec__31000 = p__30996;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31000,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31000,(1),null);
var job = vec__31000;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
(xf.cljs$core$IFn$_invoke$arity$2 ? xf.cljs$core$IFn$_invoke$arity$2(v,res) : xf.call(null,v,res));

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});})(jobs,results,process))
;
var n__4607__auto___33293 = n;
var __33294 = (0);
while(true){
if((__33294 < n__4607__auto___33293)){
var G__31003_33295 = type;
var G__31003_33296__$1 = (((G__31003_33295 instanceof cljs.core.Keyword))?G__31003_33295.fqn:null);
switch (G__31003_33296__$1) {
case "compute":
var c__30611__auto___33298 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__33294,c__30611__auto___33298,G__31003_33295,G__31003_33296__$1,n__4607__auto___33293,jobs,results,process,async){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (__33294,c__30611__auto___33298,G__31003_33295,G__31003_33296__$1,n__4607__auto___33293,jobs,results,process,async){
return (function (state_31018){
var state_val_31019 = (state_31018[(1)]);
if((state_val_31019 === (1))){
var state_31018__$1 = state_31018;
var statearr_31020_33299 = state_31018__$1;
(statearr_31020_33299[(2)] = null);

(statearr_31020_33299[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31019 === (2))){
var state_31018__$1 = state_31018;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31018__$1,(4),jobs);
} else {
if((state_val_31019 === (3))){
var inst_31016 = (state_31018[(2)]);
var state_31018__$1 = state_31018;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31018__$1,inst_31016);
} else {
if((state_val_31019 === (4))){
var inst_31008 = (state_31018[(2)]);
var inst_31009 = process(inst_31008);
var state_31018__$1 = state_31018;
if(cljs.core.truth_(inst_31009)){
var statearr_31021_33302 = state_31018__$1;
(statearr_31021_33302[(1)] = (5));

} else {
var statearr_31022_33304 = state_31018__$1;
(statearr_31022_33304[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31019 === (5))){
var state_31018__$1 = state_31018;
var statearr_31023_33305 = state_31018__$1;
(statearr_31023_33305[(2)] = null);

(statearr_31023_33305[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31019 === (6))){
var state_31018__$1 = state_31018;
var statearr_31024_33306 = state_31018__$1;
(statearr_31024_33306[(2)] = null);

(statearr_31024_33306[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31019 === (7))){
var inst_31014 = (state_31018[(2)]);
var state_31018__$1 = state_31018;
var statearr_31025_33309 = state_31018__$1;
(statearr_31025_33309[(2)] = inst_31014);

(statearr_31025_33309[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__33294,c__30611__auto___33298,G__31003_33295,G__31003_33296__$1,n__4607__auto___33293,jobs,results,process,async))
;
return ((function (__33294,switch__30294__auto__,c__30611__auto___33298,G__31003_33295,G__31003_33296__$1,n__4607__auto___33293,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____0 = (function (){
var statearr_31026 = [null,null,null,null,null,null,null];
(statearr_31026[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__);

(statearr_31026[(1)] = (1));

return statearr_31026;
});
var cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____1 = (function (state_31018){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_31018);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e31027){if((e31027 instanceof Object)){
var ex__30298__auto__ = e31027;
var statearr_31028_33315 = state_31018;
(statearr_31028_33315[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31018);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31027;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33316 = state_31018;
state_31018 = G__33316;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__ = function(state_31018){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____1.call(this,state_31018);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__;
})()
;})(__33294,switch__30294__auto__,c__30611__auto___33298,G__31003_33295,G__31003_33296__$1,n__4607__auto___33293,jobs,results,process,async))
})();
var state__30613__auto__ = (function (){var statearr_31030 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_31030[(6)] = c__30611__auto___33298);

return statearr_31030;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(__33294,c__30611__auto___33298,G__31003_33295,G__31003_33296__$1,n__4607__auto___33293,jobs,results,process,async))
);


break;
case "async":
var c__30611__auto___33318 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__33294,c__30611__auto___33318,G__31003_33295,G__31003_33296__$1,n__4607__auto___33293,jobs,results,process,async){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (__33294,c__30611__auto___33318,G__31003_33295,G__31003_33296__$1,n__4607__auto___33293,jobs,results,process,async){
return (function (state_31043){
var state_val_31044 = (state_31043[(1)]);
if((state_val_31044 === (1))){
var state_31043__$1 = state_31043;
var statearr_31049_33320 = state_31043__$1;
(statearr_31049_33320[(2)] = null);

(statearr_31049_33320[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31044 === (2))){
var state_31043__$1 = state_31043;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31043__$1,(4),jobs);
} else {
if((state_val_31044 === (3))){
var inst_31041 = (state_31043[(2)]);
var state_31043__$1 = state_31043;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31043__$1,inst_31041);
} else {
if((state_val_31044 === (4))){
var inst_31033 = (state_31043[(2)]);
var inst_31034 = async(inst_31033);
var state_31043__$1 = state_31043;
if(cljs.core.truth_(inst_31034)){
var statearr_31051_33322 = state_31043__$1;
(statearr_31051_33322[(1)] = (5));

} else {
var statearr_31054_33323 = state_31043__$1;
(statearr_31054_33323[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31044 === (5))){
var state_31043__$1 = state_31043;
var statearr_31055_33325 = state_31043__$1;
(statearr_31055_33325[(2)] = null);

(statearr_31055_33325[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31044 === (6))){
var state_31043__$1 = state_31043;
var statearr_31059_33327 = state_31043__$1;
(statearr_31059_33327[(2)] = null);

(statearr_31059_33327[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31044 === (7))){
var inst_31039 = (state_31043[(2)]);
var state_31043__$1 = state_31043;
var statearr_31060_33328 = state_31043__$1;
(statearr_31060_33328[(2)] = inst_31039);

(statearr_31060_33328[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__33294,c__30611__auto___33318,G__31003_33295,G__31003_33296__$1,n__4607__auto___33293,jobs,results,process,async))
;
return ((function (__33294,switch__30294__auto__,c__30611__auto___33318,G__31003_33295,G__31003_33296__$1,n__4607__auto___33293,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____0 = (function (){
var statearr_31062 = [null,null,null,null,null,null,null];
(statearr_31062[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__);

(statearr_31062[(1)] = (1));

return statearr_31062;
});
var cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____1 = (function (state_31043){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_31043);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e31064){if((e31064 instanceof Object)){
var ex__30298__auto__ = e31064;
var statearr_31065_33329 = state_31043;
(statearr_31065_33329[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31043);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31064;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33331 = state_31043;
state_31043 = G__33331;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__ = function(state_31043){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____1.call(this,state_31043);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__;
})()
;})(__33294,switch__30294__auto__,c__30611__auto___33318,G__31003_33295,G__31003_33296__$1,n__4607__auto___33293,jobs,results,process,async))
})();
var state__30613__auto__ = (function (){var statearr_31066 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_31066[(6)] = c__30611__auto___33318);

return statearr_31066;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(__33294,c__30611__auto___33318,G__31003_33295,G__31003_33296__$1,n__4607__auto___33293,jobs,results,process,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__31003_33296__$1)].join('')));

}

var G__33333 = (__33294 + (1));
__33294 = G__33333;
continue;
} else {
}
break;
}

var c__30611__auto___33334 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto___33334,jobs,results,process,async){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto___33334,jobs,results,process,async){
return (function (state_31092){
var state_val_31093 = (state_31092[(1)]);
if((state_val_31093 === (7))){
var inst_31086 = (state_31092[(2)]);
var state_31092__$1 = state_31092;
var statearr_31103_33335 = state_31092__$1;
(statearr_31103_33335[(2)] = inst_31086);

(statearr_31103_33335[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31093 === (1))){
var state_31092__$1 = state_31092;
var statearr_31108_33336 = state_31092__$1;
(statearr_31108_33336[(2)] = null);

(statearr_31108_33336[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31093 === (4))){
var inst_31069 = (state_31092[(7)]);
var inst_31069__$1 = (state_31092[(2)]);
var inst_31070 = (inst_31069__$1 == null);
var state_31092__$1 = (function (){var statearr_31110 = state_31092;
(statearr_31110[(7)] = inst_31069__$1);

return statearr_31110;
})();
if(cljs.core.truth_(inst_31070)){
var statearr_31111_33337 = state_31092__$1;
(statearr_31111_33337[(1)] = (5));

} else {
var statearr_31113_33338 = state_31092__$1;
(statearr_31113_33338[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31093 === (6))){
var inst_31069 = (state_31092[(7)]);
var inst_31075 = (state_31092[(8)]);
var inst_31075__$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var inst_31077 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_31078 = [inst_31069,inst_31075__$1];
var inst_31079 = (new cljs.core.PersistentVector(null,2,(5),inst_31077,inst_31078,null));
var state_31092__$1 = (function (){var statearr_31118 = state_31092;
(statearr_31118[(8)] = inst_31075__$1);

return statearr_31118;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31092__$1,(8),jobs,inst_31079);
} else {
if((state_val_31093 === (3))){
var inst_31088 = (state_31092[(2)]);
var state_31092__$1 = state_31092;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31092__$1,inst_31088);
} else {
if((state_val_31093 === (2))){
var state_31092__$1 = state_31092;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31092__$1,(4),from);
} else {
if((state_val_31093 === (9))){
var inst_31083 = (state_31092[(2)]);
var state_31092__$1 = (function (){var statearr_31119 = state_31092;
(statearr_31119[(9)] = inst_31083);

return statearr_31119;
})();
var statearr_31120_33346 = state_31092__$1;
(statearr_31120_33346[(2)] = null);

(statearr_31120_33346[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31093 === (5))){
var inst_31072 = cljs.core.async.close_BANG_(jobs);
var state_31092__$1 = state_31092;
var statearr_31121_33350 = state_31092__$1;
(statearr_31121_33350[(2)] = inst_31072);

(statearr_31121_33350[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31093 === (8))){
var inst_31075 = (state_31092[(8)]);
var inst_31081 = (state_31092[(2)]);
var state_31092__$1 = (function (){var statearr_31122 = state_31092;
(statearr_31122[(10)] = inst_31081);

return statearr_31122;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31092__$1,(9),results,inst_31075);
} else {
return null;
}
}
}
}
}
}
}
}
}
});})(c__30611__auto___33334,jobs,results,process,async))
;
return ((function (switch__30294__auto__,c__30611__auto___33334,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____0 = (function (){
var statearr_31123 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_31123[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__);

(statearr_31123[(1)] = (1));

return statearr_31123;
});
var cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____1 = (function (state_31092){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_31092);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e31124){if((e31124 instanceof Object)){
var ex__30298__auto__ = e31124;
var statearr_31125_33355 = state_31092;
(statearr_31125_33355[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31092);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31124;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33358 = state_31092;
state_31092 = G__33358;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__ = function(state_31092){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____1.call(this,state_31092);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto___33334,jobs,results,process,async))
})();
var state__30613__auto__ = (function (){var statearr_31126 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_31126[(6)] = c__30611__auto___33334);

return statearr_31126;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto___33334,jobs,results,process,async))
);


var c__30611__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto__,jobs,results,process,async){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto__,jobs,results,process,async){
return (function (state_31168){
var state_val_31169 = (state_31168[(1)]);
if((state_val_31169 === (7))){
var inst_31164 = (state_31168[(2)]);
var state_31168__$1 = state_31168;
var statearr_31171_33363 = state_31168__$1;
(statearr_31171_33363[(2)] = inst_31164);

(statearr_31171_33363[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31169 === (20))){
var state_31168__$1 = state_31168;
var statearr_31172_33366 = state_31168__$1;
(statearr_31172_33366[(2)] = null);

(statearr_31172_33366[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31169 === (1))){
var state_31168__$1 = state_31168;
var statearr_31173_33367 = state_31168__$1;
(statearr_31173_33367[(2)] = null);

(statearr_31173_33367[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31169 === (4))){
var inst_31131 = (state_31168[(7)]);
var inst_31131__$1 = (state_31168[(2)]);
var inst_31132 = (inst_31131__$1 == null);
var state_31168__$1 = (function (){var statearr_31174 = state_31168;
(statearr_31174[(7)] = inst_31131__$1);

return statearr_31174;
})();
if(cljs.core.truth_(inst_31132)){
var statearr_31175_33368 = state_31168__$1;
(statearr_31175_33368[(1)] = (5));

} else {
var statearr_31176_33369 = state_31168__$1;
(statearr_31176_33369[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31169 === (15))){
var inst_31145 = (state_31168[(8)]);
var state_31168__$1 = state_31168;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31168__$1,(18),to,inst_31145);
} else {
if((state_val_31169 === (21))){
var inst_31159 = (state_31168[(2)]);
var state_31168__$1 = state_31168;
var statearr_31177_33379 = state_31168__$1;
(statearr_31177_33379[(2)] = inst_31159);

(statearr_31177_33379[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31169 === (13))){
var inst_31161 = (state_31168[(2)]);
var state_31168__$1 = (function (){var statearr_31178 = state_31168;
(statearr_31178[(9)] = inst_31161);

return statearr_31178;
})();
var statearr_31179_33386 = state_31168__$1;
(statearr_31179_33386[(2)] = null);

(statearr_31179_33386[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31169 === (6))){
var inst_31131 = (state_31168[(7)]);
var state_31168__$1 = state_31168;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31168__$1,(11),inst_31131);
} else {
if((state_val_31169 === (17))){
var inst_31154 = (state_31168[(2)]);
var state_31168__$1 = state_31168;
if(cljs.core.truth_(inst_31154)){
var statearr_31181_33387 = state_31168__$1;
(statearr_31181_33387[(1)] = (19));

} else {
var statearr_31182_33393 = state_31168__$1;
(statearr_31182_33393[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31169 === (3))){
var inst_31166 = (state_31168[(2)]);
var state_31168__$1 = state_31168;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31168__$1,inst_31166);
} else {
if((state_val_31169 === (12))){
var inst_31142 = (state_31168[(10)]);
var state_31168__$1 = state_31168;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31168__$1,(14),inst_31142);
} else {
if((state_val_31169 === (2))){
var state_31168__$1 = state_31168;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31168__$1,(4),results);
} else {
if((state_val_31169 === (19))){
var state_31168__$1 = state_31168;
var statearr_31183_33398 = state_31168__$1;
(statearr_31183_33398[(2)] = null);

(statearr_31183_33398[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31169 === (11))){
var inst_31142 = (state_31168[(2)]);
var state_31168__$1 = (function (){var statearr_31185 = state_31168;
(statearr_31185[(10)] = inst_31142);

return statearr_31185;
})();
var statearr_31188_33404 = state_31168__$1;
(statearr_31188_33404[(2)] = null);

(statearr_31188_33404[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31169 === (9))){
var state_31168__$1 = state_31168;
var statearr_31189_33405 = state_31168__$1;
(statearr_31189_33405[(2)] = null);

(statearr_31189_33405[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31169 === (5))){
var state_31168__$1 = state_31168;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31190_33406 = state_31168__$1;
(statearr_31190_33406[(1)] = (8));

} else {
var statearr_31191_33411 = state_31168__$1;
(statearr_31191_33411[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31169 === (14))){
var inst_31145 = (state_31168[(8)]);
var inst_31148 = (state_31168[(11)]);
var inst_31145__$1 = (state_31168[(2)]);
var inst_31147 = (inst_31145__$1 == null);
var inst_31148__$1 = cljs.core.not(inst_31147);
var state_31168__$1 = (function (){var statearr_31196 = state_31168;
(statearr_31196[(8)] = inst_31145__$1);

(statearr_31196[(11)] = inst_31148__$1);

return statearr_31196;
})();
if(inst_31148__$1){
var statearr_31198_33415 = state_31168__$1;
(statearr_31198_33415[(1)] = (15));

} else {
var statearr_31199_33416 = state_31168__$1;
(statearr_31199_33416[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31169 === (16))){
var inst_31148 = (state_31168[(11)]);
var state_31168__$1 = state_31168;
var statearr_31200_33417 = state_31168__$1;
(statearr_31200_33417[(2)] = inst_31148);

(statearr_31200_33417[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31169 === (10))){
var inst_31139 = (state_31168[(2)]);
var state_31168__$1 = state_31168;
var statearr_31205_33421 = state_31168__$1;
(statearr_31205_33421[(2)] = inst_31139);

(statearr_31205_33421[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31169 === (18))){
var inst_31151 = (state_31168[(2)]);
var state_31168__$1 = state_31168;
var statearr_31212_33422 = state_31168__$1;
(statearr_31212_33422[(2)] = inst_31151);

(statearr_31212_33422[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31169 === (8))){
var inst_31136 = cljs.core.async.close_BANG_(to);
var state_31168__$1 = state_31168;
var statearr_31218_33423 = state_31168__$1;
(statearr_31218_33423[(2)] = inst_31136);

(statearr_31218_33423[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto__,jobs,results,process,async))
;
return ((function (switch__30294__auto__,c__30611__auto__,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____0 = (function (){
var statearr_31225 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_31225[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__);

(statearr_31225[(1)] = (1));

return statearr_31225;
});
var cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____1 = (function (state_31168){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_31168);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e31226){if((e31226 instanceof Object)){
var ex__30298__auto__ = e31226;
var statearr_31228_33427 = state_31168;
(statearr_31228_33427[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31168);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31226;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33431 = state_31168;
state_31168 = G__33431;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__ = function(state_31168){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____1.call(this,state_31168);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__30295__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto__,jobs,results,process,async))
})();
var state__30613__auto__ = (function (){var statearr_31229 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_31229[(6)] = c__30611__auto__);

return statearr_31229;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto__,jobs,results,process,async))
);

return c__30611__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__31231 = arguments.length;
switch (G__31231) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5(n,to,af,from,true);
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_(n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
});

cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5;

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__31233 = arguments.length;
switch (G__31233) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5(n,to,xf,from,true);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6(n,to,xf,from,close_QMARK_,null);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
});

cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6;

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__31235 = arguments.length;
switch (G__31235) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4(p,ch,null,null);
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(t_buf_or_n);
var fc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(f_buf_or_n);
var c__30611__auto___33447 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto___33447,tc,fc){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto___33447,tc,fc){
return (function (state_31268){
var state_val_31269 = (state_31268[(1)]);
if((state_val_31269 === (7))){
var inst_31264 = (state_31268[(2)]);
var state_31268__$1 = state_31268;
var statearr_31270_33448 = state_31268__$1;
(statearr_31270_33448[(2)] = inst_31264);

(statearr_31270_33448[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31269 === (1))){
var state_31268__$1 = state_31268;
var statearr_31271_33453 = state_31268__$1;
(statearr_31271_33453[(2)] = null);

(statearr_31271_33453[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31269 === (4))){
var inst_31239 = (state_31268[(7)]);
var inst_31239__$1 = (state_31268[(2)]);
var inst_31240 = (inst_31239__$1 == null);
var state_31268__$1 = (function (){var statearr_31272 = state_31268;
(statearr_31272[(7)] = inst_31239__$1);

return statearr_31272;
})();
if(cljs.core.truth_(inst_31240)){
var statearr_31273_33457 = state_31268__$1;
(statearr_31273_33457[(1)] = (5));

} else {
var statearr_31274_33458 = state_31268__$1;
(statearr_31274_33458[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31269 === (13))){
var state_31268__$1 = state_31268;
var statearr_31276_33459 = state_31268__$1;
(statearr_31276_33459[(2)] = null);

(statearr_31276_33459[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31269 === (6))){
var inst_31239 = (state_31268[(7)]);
var inst_31246 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_31239) : p.call(null,inst_31239));
var state_31268__$1 = state_31268;
if(cljs.core.truth_(inst_31246)){
var statearr_31281_33460 = state_31268__$1;
(statearr_31281_33460[(1)] = (9));

} else {
var statearr_31282_33464 = state_31268__$1;
(statearr_31282_33464[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31269 === (3))){
var inst_31266 = (state_31268[(2)]);
var state_31268__$1 = state_31268;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31268__$1,inst_31266);
} else {
if((state_val_31269 === (12))){
var state_31268__$1 = state_31268;
var statearr_31283_33465 = state_31268__$1;
(statearr_31283_33465[(2)] = null);

(statearr_31283_33465[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31269 === (2))){
var state_31268__$1 = state_31268;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31268__$1,(4),ch);
} else {
if((state_val_31269 === (11))){
var inst_31239 = (state_31268[(7)]);
var inst_31250 = (state_31268[(2)]);
var state_31268__$1 = state_31268;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31268__$1,(8),inst_31250,inst_31239);
} else {
if((state_val_31269 === (9))){
var state_31268__$1 = state_31268;
var statearr_31284_33469 = state_31268__$1;
(statearr_31284_33469[(2)] = tc);

(statearr_31284_33469[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31269 === (5))){
var inst_31242 = cljs.core.async.close_BANG_(tc);
var inst_31243 = cljs.core.async.close_BANG_(fc);
var state_31268__$1 = (function (){var statearr_31285 = state_31268;
(statearr_31285[(8)] = inst_31242);

return statearr_31285;
})();
var statearr_31286_33473 = state_31268__$1;
(statearr_31286_33473[(2)] = inst_31243);

(statearr_31286_33473[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31269 === (14))){
var inst_31262 = (state_31268[(2)]);
var state_31268__$1 = state_31268;
var statearr_31287_33474 = state_31268__$1;
(statearr_31287_33474[(2)] = inst_31262);

(statearr_31287_33474[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31269 === (10))){
var state_31268__$1 = state_31268;
var statearr_31288_33475 = state_31268__$1;
(statearr_31288_33475[(2)] = fc);

(statearr_31288_33475[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31269 === (8))){
var inst_31252 = (state_31268[(2)]);
var state_31268__$1 = state_31268;
if(cljs.core.truth_(inst_31252)){
var statearr_31289_33480 = state_31268__$1;
(statearr_31289_33480[(1)] = (12));

} else {
var statearr_31290_33481 = state_31268__$1;
(statearr_31290_33481[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto___33447,tc,fc))
;
return ((function (switch__30294__auto__,c__30611__auto___33447,tc,fc){
return (function() {
var cljs$core$async$state_machine__30295__auto__ = null;
var cljs$core$async$state_machine__30295__auto____0 = (function (){
var statearr_31291 = [null,null,null,null,null,null,null,null,null];
(statearr_31291[(0)] = cljs$core$async$state_machine__30295__auto__);

(statearr_31291[(1)] = (1));

return statearr_31291;
});
var cljs$core$async$state_machine__30295__auto____1 = (function (state_31268){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_31268);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e31292){if((e31292 instanceof Object)){
var ex__30298__auto__ = e31292;
var statearr_31293_33488 = state_31268;
(statearr_31293_33488[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31268);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31292;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33489 = state_31268;
state_31268 = G__33489;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$state_machine__30295__auto__ = function(state_31268){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30295__auto____1.call(this,state_31268);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30295__auto____0;
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30295__auto____1;
return cljs$core$async$state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto___33447,tc,fc))
})();
var state__30613__auto__ = (function (){var statearr_31294 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_31294[(6)] = c__30611__auto___33447);

return statearr_31294;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto___33447,tc,fc))
);


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});

cljs.core.async.split.cljs$lang$maxFixedArity = 4;

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__30611__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto__){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto__){
return (function (state_31330){
var state_val_31331 = (state_31330[(1)]);
if((state_val_31331 === (7))){
var inst_31326 = (state_31330[(2)]);
var state_31330__$1 = state_31330;
var statearr_31332_33502 = state_31330__$1;
(statearr_31332_33502[(2)] = inst_31326);

(statearr_31332_33502[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31331 === (1))){
var inst_31299 = init;
var state_31330__$1 = (function (){var statearr_31335 = state_31330;
(statearr_31335[(7)] = inst_31299);

return statearr_31335;
})();
var statearr_31336_33503 = state_31330__$1;
(statearr_31336_33503[(2)] = null);

(statearr_31336_33503[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31331 === (4))){
var inst_31304 = (state_31330[(8)]);
var inst_31304__$1 = (state_31330[(2)]);
var inst_31306 = (inst_31304__$1 == null);
var state_31330__$1 = (function (){var statearr_31337 = state_31330;
(statearr_31337[(8)] = inst_31304__$1);

return statearr_31337;
})();
if(cljs.core.truth_(inst_31306)){
var statearr_31338_33504 = state_31330__$1;
(statearr_31338_33504[(1)] = (5));

} else {
var statearr_31339_33508 = state_31330__$1;
(statearr_31339_33508[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31331 === (6))){
var inst_31299 = (state_31330[(7)]);
var inst_31304 = (state_31330[(8)]);
var inst_31313 = (state_31330[(9)]);
var inst_31313__$1 = (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(inst_31299,inst_31304) : f.call(null,inst_31299,inst_31304));
var inst_31314 = cljs.core.reduced_QMARK_(inst_31313__$1);
var state_31330__$1 = (function (){var statearr_31344 = state_31330;
(statearr_31344[(9)] = inst_31313__$1);

return statearr_31344;
})();
if(inst_31314){
var statearr_31345_33512 = state_31330__$1;
(statearr_31345_33512[(1)] = (8));

} else {
var statearr_31346_33513 = state_31330__$1;
(statearr_31346_33513[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31331 === (3))){
var inst_31328 = (state_31330[(2)]);
var state_31330__$1 = state_31330;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31330__$1,inst_31328);
} else {
if((state_val_31331 === (2))){
var state_31330__$1 = state_31330;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31330__$1,(4),ch);
} else {
if((state_val_31331 === (9))){
var inst_31313 = (state_31330[(9)]);
var inst_31299 = inst_31313;
var state_31330__$1 = (function (){var statearr_31349 = state_31330;
(statearr_31349[(7)] = inst_31299);

return statearr_31349;
})();
var statearr_31350_33514 = state_31330__$1;
(statearr_31350_33514[(2)] = null);

(statearr_31350_33514[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31331 === (5))){
var inst_31299 = (state_31330[(7)]);
var state_31330__$1 = state_31330;
var statearr_31352_33516 = state_31330__$1;
(statearr_31352_33516[(2)] = inst_31299);

(statearr_31352_33516[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31331 === (10))){
var inst_31324 = (state_31330[(2)]);
var state_31330__$1 = state_31330;
var statearr_31353_33517 = state_31330__$1;
(statearr_31353_33517[(2)] = inst_31324);

(statearr_31353_33517[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31331 === (8))){
var inst_31313 = (state_31330[(9)]);
var inst_31316 = cljs.core.deref(inst_31313);
var state_31330__$1 = state_31330;
var statearr_31354_33518 = state_31330__$1;
(statearr_31354_33518[(2)] = inst_31316);

(statearr_31354_33518[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto__))
;
return ((function (switch__30294__auto__,c__30611__auto__){
return (function() {
var cljs$core$async$reduce_$_state_machine__30295__auto__ = null;
var cljs$core$async$reduce_$_state_machine__30295__auto____0 = (function (){
var statearr_31357 = [null,null,null,null,null,null,null,null,null,null];
(statearr_31357[(0)] = cljs$core$async$reduce_$_state_machine__30295__auto__);

(statearr_31357[(1)] = (1));

return statearr_31357;
});
var cljs$core$async$reduce_$_state_machine__30295__auto____1 = (function (state_31330){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_31330);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e31358){if((e31358 instanceof Object)){
var ex__30298__auto__ = e31358;
var statearr_31359_33519 = state_31330;
(statearr_31359_33519[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31330);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31358;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33520 = state_31330;
state_31330 = G__33520;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__30295__auto__ = function(state_31330){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__30295__auto____1.call(this,state_31330);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__30295__auto____0;
cljs$core$async$reduce_$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__30295__auto____1;
return cljs$core$async$reduce_$_state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto__))
})();
var state__30613__auto__ = (function (){var statearr_31363 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_31363[(6)] = c__30611__auto__);

return statearr_31363;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto__))
);

return c__30611__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = (xform.cljs$core$IFn$_invoke$arity$1 ? xform.cljs$core$IFn$_invoke$arity$1(f) : xform.call(null,f));
var c__30611__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto__,f__$1){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto__,f__$1){
return (function (state_31369){
var state_val_31370 = (state_31369[(1)]);
if((state_val_31370 === (1))){
var inst_31364 = cljs.core.async.reduce(f__$1,init,ch);
var state_31369__$1 = state_31369;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31369__$1,(2),inst_31364);
} else {
if((state_val_31370 === (2))){
var inst_31366 = (state_31369[(2)]);
var inst_31367 = (f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(inst_31366) : f__$1.call(null,inst_31366));
var state_31369__$1 = state_31369;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31369__$1,inst_31367);
} else {
return null;
}
}
});})(c__30611__auto__,f__$1))
;
return ((function (switch__30294__auto__,c__30611__auto__,f__$1){
return (function() {
var cljs$core$async$transduce_$_state_machine__30295__auto__ = null;
var cljs$core$async$transduce_$_state_machine__30295__auto____0 = (function (){
var statearr_31380 = [null,null,null,null,null,null,null];
(statearr_31380[(0)] = cljs$core$async$transduce_$_state_machine__30295__auto__);

(statearr_31380[(1)] = (1));

return statearr_31380;
});
var cljs$core$async$transduce_$_state_machine__30295__auto____1 = (function (state_31369){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_31369);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e31383){if((e31383 instanceof Object)){
var ex__30298__auto__ = e31383;
var statearr_31384_33523 = state_31369;
(statearr_31384_33523[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31369);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31383;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33524 = state_31369;
state_31369 = G__33524;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__30295__auto__ = function(state_31369){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__30295__auto____1.call(this,state_31369);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__30295__auto____0;
cljs$core$async$transduce_$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__30295__auto____1;
return cljs$core$async$transduce_$_state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto__,f__$1))
})();
var state__30613__auto__ = (function (){var statearr_31385 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_31385[(6)] = c__30611__auto__);

return statearr_31385;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto__,f__$1))
);

return c__30611__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__31393 = arguments.length;
switch (G__31393) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__30611__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto__){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto__){
return (function (state_31424){
var state_val_31425 = (state_31424[(1)]);
if((state_val_31425 === (7))){
var inst_31406 = (state_31424[(2)]);
var state_31424__$1 = state_31424;
var statearr_31429_33526 = state_31424__$1;
(statearr_31429_33526[(2)] = inst_31406);

(statearr_31429_33526[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31425 === (1))){
var inst_31397 = cljs.core.seq(coll);
var inst_31398 = inst_31397;
var state_31424__$1 = (function (){var statearr_31430 = state_31424;
(statearr_31430[(7)] = inst_31398);

return statearr_31430;
})();
var statearr_31431_33527 = state_31424__$1;
(statearr_31431_33527[(2)] = null);

(statearr_31431_33527[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31425 === (4))){
var inst_31398 = (state_31424[(7)]);
var inst_31404 = cljs.core.first(inst_31398);
var state_31424__$1 = state_31424;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31424__$1,(7),ch,inst_31404);
} else {
if((state_val_31425 === (13))){
var inst_31418 = (state_31424[(2)]);
var state_31424__$1 = state_31424;
var statearr_31436_33531 = state_31424__$1;
(statearr_31436_33531[(2)] = inst_31418);

(statearr_31436_33531[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31425 === (6))){
var inst_31409 = (state_31424[(2)]);
var state_31424__$1 = state_31424;
if(cljs.core.truth_(inst_31409)){
var statearr_31439_33532 = state_31424__$1;
(statearr_31439_33532[(1)] = (8));

} else {
var statearr_31441_33533 = state_31424__$1;
(statearr_31441_33533[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31425 === (3))){
var inst_31422 = (state_31424[(2)]);
var state_31424__$1 = state_31424;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31424__$1,inst_31422);
} else {
if((state_val_31425 === (12))){
var state_31424__$1 = state_31424;
var statearr_31444_33537 = state_31424__$1;
(statearr_31444_33537[(2)] = null);

(statearr_31444_33537[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31425 === (2))){
var inst_31398 = (state_31424[(7)]);
var state_31424__$1 = state_31424;
if(cljs.core.truth_(inst_31398)){
var statearr_31445_33539 = state_31424__$1;
(statearr_31445_33539[(1)] = (4));

} else {
var statearr_31446_33540 = state_31424__$1;
(statearr_31446_33540[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31425 === (11))){
var inst_31415 = cljs.core.async.close_BANG_(ch);
var state_31424__$1 = state_31424;
var statearr_31449_33541 = state_31424__$1;
(statearr_31449_33541[(2)] = inst_31415);

(statearr_31449_33541[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31425 === (9))){
var state_31424__$1 = state_31424;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31451_33543 = state_31424__$1;
(statearr_31451_33543[(1)] = (11));

} else {
var statearr_31452_33544 = state_31424__$1;
(statearr_31452_33544[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31425 === (5))){
var inst_31398 = (state_31424[(7)]);
var state_31424__$1 = state_31424;
var statearr_31455_33546 = state_31424__$1;
(statearr_31455_33546[(2)] = inst_31398);

(statearr_31455_33546[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31425 === (10))){
var inst_31420 = (state_31424[(2)]);
var state_31424__$1 = state_31424;
var statearr_31456_33547 = state_31424__$1;
(statearr_31456_33547[(2)] = inst_31420);

(statearr_31456_33547[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31425 === (8))){
var inst_31398 = (state_31424[(7)]);
var inst_31411 = cljs.core.next(inst_31398);
var inst_31398__$1 = inst_31411;
var state_31424__$1 = (function (){var statearr_31457 = state_31424;
(statearr_31457[(7)] = inst_31398__$1);

return statearr_31457;
})();
var statearr_31458_33548 = state_31424__$1;
(statearr_31458_33548[(2)] = null);

(statearr_31458_33548[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto__))
;
return ((function (switch__30294__auto__,c__30611__auto__){
return (function() {
var cljs$core$async$state_machine__30295__auto__ = null;
var cljs$core$async$state_machine__30295__auto____0 = (function (){
var statearr_31460 = [null,null,null,null,null,null,null,null];
(statearr_31460[(0)] = cljs$core$async$state_machine__30295__auto__);

(statearr_31460[(1)] = (1));

return statearr_31460;
});
var cljs$core$async$state_machine__30295__auto____1 = (function (state_31424){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_31424);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e31461){if((e31461 instanceof Object)){
var ex__30298__auto__ = e31461;
var statearr_31463_33549 = state_31424;
(statearr_31463_33549[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31424);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31461;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33551 = state_31424;
state_31424 = G__33551;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$state_machine__30295__auto__ = function(state_31424){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30295__auto____1.call(this,state_31424);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30295__auto____0;
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30295__auto____1;
return cljs$core$async$state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto__))
})();
var state__30613__auto__ = (function (){var statearr_31464 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_31464[(6)] = c__30611__auto__);

return statearr_31464;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto__))
);

return c__30611__auto__;
});

cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
var ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.bounded_count((100),coll));
cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2(ch,coll);

return ch;
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
var x__4433__auto__ = (((_ == null))?null:_);
var m__4434__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4434__auto__.call(null,_));
} else {
var m__4431__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4431__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Mux.muxch*",_);
}
}
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4434__auto__.call(null,m,ch,close_QMARK_));
} else {
var m__4431__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4431__auto__.call(null,m,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Mult.tap*",m);
}
}
}
});

cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4434__auto__.call(null,m,ch));
} else {
var m__4431__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4431__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mult.untap*",m);
}
}
}
});

cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4434__auto__.call(null,m));
} else {
var m__4431__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4431__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mult.untap-all*",m);
}
}
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async31471 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async31471 = (function (ch,cs,meta31472){
this.ch = ch;
this.cs = cs;
this.meta31472 = meta31472;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async31471.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_31473,meta31472__$1){
var self__ = this;
var _31473__$1 = this;
return (new cljs.core.async.t_cljs$core$async31471(self__.ch,self__.cs,meta31472__$1));
});})(cs))
;

cljs.core.async.t_cljs$core$async31471.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_31473){
var self__ = this;
var _31473__$1 = this;
return self__.meta31472;
});})(cs))
;

cljs.core.async.t_cljs$core$async31471.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async31471.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(cs))
;

cljs.core.async.t_cljs$core$async31471.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async31471.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async31471.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch__$1);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async31471.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async31471.getBasis = ((function (cs){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta31472","meta31472",321494357,null)], null);
});})(cs))
;

cljs.core.async.t_cljs$core$async31471.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async31471.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async31471";

cljs.core.async.t_cljs$core$async31471.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async31471");
});})(cs))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async31471.
 */
cljs.core.async.__GT_t_cljs$core$async31471 = ((function (cs){
return (function cljs$core$async$mult_$___GT_t_cljs$core$async31471(ch__$1,cs__$1,meta31472){
return (new cljs.core.async.t_cljs$core$async31471(ch__$1,cs__$1,meta31472));
});})(cs))
;

}

return (new cljs.core.async.t_cljs$core$async31471(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = ((function (cs,m,dchan,dctr){
return (function (_){
if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,true);
} else {
return null;
}
});})(cs,m,dchan,dctr))
;
var c__30611__auto___33578 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto___33578,cs,m,dchan,dctr,done){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto___33578,cs,m,dchan,dctr,done){
return (function (state_31619){
var state_val_31620 = (state_31619[(1)]);
if((state_val_31620 === (7))){
var inst_31615 = (state_31619[(2)]);
var state_31619__$1 = state_31619;
var statearr_31621_33579 = state_31619__$1;
(statearr_31621_33579[(2)] = inst_31615);

(statearr_31621_33579[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (20))){
var inst_31516 = (state_31619[(7)]);
var inst_31528 = cljs.core.first(inst_31516);
var inst_31529 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31528,(0),null);
var inst_31530 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31528,(1),null);
var state_31619__$1 = (function (){var statearr_31630 = state_31619;
(statearr_31630[(8)] = inst_31529);

return statearr_31630;
})();
if(cljs.core.truth_(inst_31530)){
var statearr_31631_33580 = state_31619__$1;
(statearr_31631_33580[(1)] = (22));

} else {
var statearr_31632_33581 = state_31619__$1;
(statearr_31632_33581[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (27))){
var inst_31567 = (state_31619[(9)]);
var inst_31485 = (state_31619[(10)]);
var inst_31558 = (state_31619[(11)]);
var inst_31560 = (state_31619[(12)]);
var inst_31567__$1 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_31558,inst_31560);
var inst_31568 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_31567__$1,inst_31485,done);
var state_31619__$1 = (function (){var statearr_31637 = state_31619;
(statearr_31637[(9)] = inst_31567__$1);

return statearr_31637;
})();
if(cljs.core.truth_(inst_31568)){
var statearr_31638_33582 = state_31619__$1;
(statearr_31638_33582[(1)] = (30));

} else {
var statearr_31639_33583 = state_31619__$1;
(statearr_31639_33583[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (1))){
var state_31619__$1 = state_31619;
var statearr_31640_33590 = state_31619__$1;
(statearr_31640_33590[(2)] = null);

(statearr_31640_33590[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (24))){
var inst_31516 = (state_31619[(7)]);
var inst_31535 = (state_31619[(2)]);
var inst_31536 = cljs.core.next(inst_31516);
var inst_31494 = inst_31536;
var inst_31495 = null;
var inst_31496 = (0);
var inst_31497 = (0);
var state_31619__$1 = (function (){var statearr_31641 = state_31619;
(statearr_31641[(13)] = inst_31535);

(statearr_31641[(14)] = inst_31496);

(statearr_31641[(15)] = inst_31494);

(statearr_31641[(16)] = inst_31495);

(statearr_31641[(17)] = inst_31497);

return statearr_31641;
})();
var statearr_31642_33591 = state_31619__$1;
(statearr_31642_33591[(2)] = null);

(statearr_31642_33591[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (39))){
var state_31619__$1 = state_31619;
var statearr_31652_33592 = state_31619__$1;
(statearr_31652_33592[(2)] = null);

(statearr_31652_33592[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (4))){
var inst_31485 = (state_31619[(10)]);
var inst_31485__$1 = (state_31619[(2)]);
var inst_31486 = (inst_31485__$1 == null);
var state_31619__$1 = (function (){var statearr_31653 = state_31619;
(statearr_31653[(10)] = inst_31485__$1);

return statearr_31653;
})();
if(cljs.core.truth_(inst_31486)){
var statearr_31654_33593 = state_31619__$1;
(statearr_31654_33593[(1)] = (5));

} else {
var statearr_31655_33594 = state_31619__$1;
(statearr_31655_33594[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (15))){
var inst_31496 = (state_31619[(14)]);
var inst_31494 = (state_31619[(15)]);
var inst_31495 = (state_31619[(16)]);
var inst_31497 = (state_31619[(17)]);
var inst_31512 = (state_31619[(2)]);
var inst_31513 = (inst_31497 + (1));
var tmp31649 = inst_31496;
var tmp31650 = inst_31494;
var tmp31651 = inst_31495;
var inst_31494__$1 = tmp31650;
var inst_31495__$1 = tmp31651;
var inst_31496__$1 = tmp31649;
var inst_31497__$1 = inst_31513;
var state_31619__$1 = (function (){var statearr_31656 = state_31619;
(statearr_31656[(18)] = inst_31512);

(statearr_31656[(14)] = inst_31496__$1);

(statearr_31656[(15)] = inst_31494__$1);

(statearr_31656[(16)] = inst_31495__$1);

(statearr_31656[(17)] = inst_31497__$1);

return statearr_31656;
})();
var statearr_31658_33595 = state_31619__$1;
(statearr_31658_33595[(2)] = null);

(statearr_31658_33595[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (21))){
var inst_31539 = (state_31619[(2)]);
var state_31619__$1 = state_31619;
var statearr_31662_33596 = state_31619__$1;
(statearr_31662_33596[(2)] = inst_31539);

(statearr_31662_33596[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (31))){
var inst_31567 = (state_31619[(9)]);
var inst_31571 = done(null);
var inst_31572 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_31567);
var state_31619__$1 = (function (){var statearr_31663 = state_31619;
(statearr_31663[(19)] = inst_31571);

return statearr_31663;
})();
var statearr_31664_33603 = state_31619__$1;
(statearr_31664_33603[(2)] = inst_31572);

(statearr_31664_33603[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (32))){
var inst_31557 = (state_31619[(20)]);
var inst_31558 = (state_31619[(11)]);
var inst_31559 = (state_31619[(21)]);
var inst_31560 = (state_31619[(12)]);
var inst_31574 = (state_31619[(2)]);
var inst_31575 = (inst_31560 + (1));
var tmp31659 = inst_31557;
var tmp31660 = inst_31558;
var tmp31661 = inst_31559;
var inst_31557__$1 = tmp31659;
var inst_31558__$1 = tmp31660;
var inst_31559__$1 = tmp31661;
var inst_31560__$1 = inst_31575;
var state_31619__$1 = (function (){var statearr_31667 = state_31619;
(statearr_31667[(20)] = inst_31557__$1);

(statearr_31667[(11)] = inst_31558__$1);

(statearr_31667[(21)] = inst_31559__$1);

(statearr_31667[(22)] = inst_31574);

(statearr_31667[(12)] = inst_31560__$1);

return statearr_31667;
})();
var statearr_31672_33604 = state_31619__$1;
(statearr_31672_33604[(2)] = null);

(statearr_31672_33604[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (40))){
var inst_31587 = (state_31619[(23)]);
var inst_31591 = done(null);
var inst_31592 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_31587);
var state_31619__$1 = (function (){var statearr_31678 = state_31619;
(statearr_31678[(24)] = inst_31591);

return statearr_31678;
})();
var statearr_31680_33605 = state_31619__$1;
(statearr_31680_33605[(2)] = inst_31592);

(statearr_31680_33605[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (33))){
var inst_31578 = (state_31619[(25)]);
var inst_31580 = cljs.core.chunked_seq_QMARK_(inst_31578);
var state_31619__$1 = state_31619;
if(inst_31580){
var statearr_31690_33606 = state_31619__$1;
(statearr_31690_33606[(1)] = (36));

} else {
var statearr_31691_33607 = state_31619__$1;
(statearr_31691_33607[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (13))){
var inst_31506 = (state_31619[(26)]);
var inst_31509 = cljs.core.async.close_BANG_(inst_31506);
var state_31619__$1 = state_31619;
var statearr_31692_33608 = state_31619__$1;
(statearr_31692_33608[(2)] = inst_31509);

(statearr_31692_33608[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (22))){
var inst_31529 = (state_31619[(8)]);
var inst_31532 = cljs.core.async.close_BANG_(inst_31529);
var state_31619__$1 = state_31619;
var statearr_31693_33609 = state_31619__$1;
(statearr_31693_33609[(2)] = inst_31532);

(statearr_31693_33609[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (36))){
var inst_31578 = (state_31619[(25)]);
var inst_31582 = cljs.core.chunk_first(inst_31578);
var inst_31583 = cljs.core.chunk_rest(inst_31578);
var inst_31584 = cljs.core.count(inst_31582);
var inst_31557 = inst_31583;
var inst_31558 = inst_31582;
var inst_31559 = inst_31584;
var inst_31560 = (0);
var state_31619__$1 = (function (){var statearr_31694 = state_31619;
(statearr_31694[(20)] = inst_31557);

(statearr_31694[(11)] = inst_31558);

(statearr_31694[(21)] = inst_31559);

(statearr_31694[(12)] = inst_31560);

return statearr_31694;
})();
var statearr_31696_33610 = state_31619__$1;
(statearr_31696_33610[(2)] = null);

(statearr_31696_33610[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (41))){
var inst_31578 = (state_31619[(25)]);
var inst_31594 = (state_31619[(2)]);
var inst_31595 = cljs.core.next(inst_31578);
var inst_31557 = inst_31595;
var inst_31558 = null;
var inst_31559 = (0);
var inst_31560 = (0);
var state_31619__$1 = (function (){var statearr_31699 = state_31619;
(statearr_31699[(20)] = inst_31557);

(statearr_31699[(27)] = inst_31594);

(statearr_31699[(11)] = inst_31558);

(statearr_31699[(21)] = inst_31559);

(statearr_31699[(12)] = inst_31560);

return statearr_31699;
})();
var statearr_31703_33611 = state_31619__$1;
(statearr_31703_33611[(2)] = null);

(statearr_31703_33611[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (43))){
var state_31619__$1 = state_31619;
var statearr_31704_33612 = state_31619__$1;
(statearr_31704_33612[(2)] = null);

(statearr_31704_33612[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (29))){
var inst_31603 = (state_31619[(2)]);
var state_31619__$1 = state_31619;
var statearr_31705_33613 = state_31619__$1;
(statearr_31705_33613[(2)] = inst_31603);

(statearr_31705_33613[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (44))){
var inst_31612 = (state_31619[(2)]);
var state_31619__$1 = (function (){var statearr_31706 = state_31619;
(statearr_31706[(28)] = inst_31612);

return statearr_31706;
})();
var statearr_31710_33615 = state_31619__$1;
(statearr_31710_33615[(2)] = null);

(statearr_31710_33615[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (6))){
var inst_31549 = (state_31619[(29)]);
var inst_31548 = cljs.core.deref(cs);
var inst_31549__$1 = cljs.core.keys(inst_31548);
var inst_31550 = cljs.core.count(inst_31549__$1);
var inst_31551 = cljs.core.reset_BANG_(dctr,inst_31550);
var inst_31556 = cljs.core.seq(inst_31549__$1);
var inst_31557 = inst_31556;
var inst_31558 = null;
var inst_31559 = (0);
var inst_31560 = (0);
var state_31619__$1 = (function (){var statearr_31711 = state_31619;
(statearr_31711[(20)] = inst_31557);

(statearr_31711[(30)] = inst_31551);

(statearr_31711[(29)] = inst_31549__$1);

(statearr_31711[(11)] = inst_31558);

(statearr_31711[(21)] = inst_31559);

(statearr_31711[(12)] = inst_31560);

return statearr_31711;
})();
var statearr_31712_33621 = state_31619__$1;
(statearr_31712_33621[(2)] = null);

(statearr_31712_33621[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (28))){
var inst_31557 = (state_31619[(20)]);
var inst_31578 = (state_31619[(25)]);
var inst_31578__$1 = cljs.core.seq(inst_31557);
var state_31619__$1 = (function (){var statearr_31713 = state_31619;
(statearr_31713[(25)] = inst_31578__$1);

return statearr_31713;
})();
if(inst_31578__$1){
var statearr_31714_33622 = state_31619__$1;
(statearr_31714_33622[(1)] = (33));

} else {
var statearr_31715_33623 = state_31619__$1;
(statearr_31715_33623[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (25))){
var inst_31559 = (state_31619[(21)]);
var inst_31560 = (state_31619[(12)]);
var inst_31564 = (inst_31560 < inst_31559);
var inst_31565 = inst_31564;
var state_31619__$1 = state_31619;
if(cljs.core.truth_(inst_31565)){
var statearr_31724_33624 = state_31619__$1;
(statearr_31724_33624[(1)] = (27));

} else {
var statearr_31733_33625 = state_31619__$1;
(statearr_31733_33625[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (34))){
var state_31619__$1 = state_31619;
var statearr_31734_33626 = state_31619__$1;
(statearr_31734_33626[(2)] = null);

(statearr_31734_33626[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (17))){
var state_31619__$1 = state_31619;
var statearr_31739_33627 = state_31619__$1;
(statearr_31739_33627[(2)] = null);

(statearr_31739_33627[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (3))){
var inst_31617 = (state_31619[(2)]);
var state_31619__$1 = state_31619;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31619__$1,inst_31617);
} else {
if((state_val_31620 === (12))){
var inst_31544 = (state_31619[(2)]);
var state_31619__$1 = state_31619;
var statearr_31740_33630 = state_31619__$1;
(statearr_31740_33630[(2)] = inst_31544);

(statearr_31740_33630[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (2))){
var state_31619__$1 = state_31619;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31619__$1,(4),ch);
} else {
if((state_val_31620 === (23))){
var state_31619__$1 = state_31619;
var statearr_31741_33631 = state_31619__$1;
(statearr_31741_33631[(2)] = null);

(statearr_31741_33631[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (35))){
var inst_31601 = (state_31619[(2)]);
var state_31619__$1 = state_31619;
var statearr_31744_33633 = state_31619__$1;
(statearr_31744_33633[(2)] = inst_31601);

(statearr_31744_33633[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (19))){
var inst_31516 = (state_31619[(7)]);
var inst_31520 = cljs.core.chunk_first(inst_31516);
var inst_31521 = cljs.core.chunk_rest(inst_31516);
var inst_31522 = cljs.core.count(inst_31520);
var inst_31494 = inst_31521;
var inst_31495 = inst_31520;
var inst_31496 = inst_31522;
var inst_31497 = (0);
var state_31619__$1 = (function (){var statearr_31745 = state_31619;
(statearr_31745[(14)] = inst_31496);

(statearr_31745[(15)] = inst_31494);

(statearr_31745[(16)] = inst_31495);

(statearr_31745[(17)] = inst_31497);

return statearr_31745;
})();
var statearr_31746_33635 = state_31619__$1;
(statearr_31746_33635[(2)] = null);

(statearr_31746_33635[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (11))){
var inst_31516 = (state_31619[(7)]);
var inst_31494 = (state_31619[(15)]);
var inst_31516__$1 = cljs.core.seq(inst_31494);
var state_31619__$1 = (function (){var statearr_31747 = state_31619;
(statearr_31747[(7)] = inst_31516__$1);

return statearr_31747;
})();
if(inst_31516__$1){
var statearr_31748_33636 = state_31619__$1;
(statearr_31748_33636[(1)] = (16));

} else {
var statearr_31749_33637 = state_31619__$1;
(statearr_31749_33637[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (9))){
var inst_31546 = (state_31619[(2)]);
var state_31619__$1 = state_31619;
var statearr_31750_33638 = state_31619__$1;
(statearr_31750_33638[(2)] = inst_31546);

(statearr_31750_33638[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (5))){
var inst_31492 = cljs.core.deref(cs);
var inst_31493 = cljs.core.seq(inst_31492);
var inst_31494 = inst_31493;
var inst_31495 = null;
var inst_31496 = (0);
var inst_31497 = (0);
var state_31619__$1 = (function (){var statearr_31752 = state_31619;
(statearr_31752[(14)] = inst_31496);

(statearr_31752[(15)] = inst_31494);

(statearr_31752[(16)] = inst_31495);

(statearr_31752[(17)] = inst_31497);

return statearr_31752;
})();
var statearr_31753_33641 = state_31619__$1;
(statearr_31753_33641[(2)] = null);

(statearr_31753_33641[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (14))){
var state_31619__$1 = state_31619;
var statearr_31754_33642 = state_31619__$1;
(statearr_31754_33642[(2)] = null);

(statearr_31754_33642[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (45))){
var inst_31609 = (state_31619[(2)]);
var state_31619__$1 = state_31619;
var statearr_31755_33643 = state_31619__$1;
(statearr_31755_33643[(2)] = inst_31609);

(statearr_31755_33643[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (26))){
var inst_31549 = (state_31619[(29)]);
var inst_31605 = (state_31619[(2)]);
var inst_31606 = cljs.core.seq(inst_31549);
var state_31619__$1 = (function (){var statearr_31756 = state_31619;
(statearr_31756[(31)] = inst_31605);

return statearr_31756;
})();
if(inst_31606){
var statearr_31757_33644 = state_31619__$1;
(statearr_31757_33644[(1)] = (42));

} else {
var statearr_31758_33645 = state_31619__$1;
(statearr_31758_33645[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (16))){
var inst_31516 = (state_31619[(7)]);
var inst_31518 = cljs.core.chunked_seq_QMARK_(inst_31516);
var state_31619__$1 = state_31619;
if(inst_31518){
var statearr_31759_33646 = state_31619__$1;
(statearr_31759_33646[(1)] = (19));

} else {
var statearr_31760_33647 = state_31619__$1;
(statearr_31760_33647[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (38))){
var inst_31598 = (state_31619[(2)]);
var state_31619__$1 = state_31619;
var statearr_31761_33648 = state_31619__$1;
(statearr_31761_33648[(2)] = inst_31598);

(statearr_31761_33648[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (30))){
var state_31619__$1 = state_31619;
var statearr_31765_33649 = state_31619__$1;
(statearr_31765_33649[(2)] = null);

(statearr_31765_33649[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (10))){
var inst_31495 = (state_31619[(16)]);
var inst_31497 = (state_31619[(17)]);
var inst_31505 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_31495,inst_31497);
var inst_31506 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31505,(0),null);
var inst_31507 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31505,(1),null);
var state_31619__$1 = (function (){var statearr_31766 = state_31619;
(statearr_31766[(26)] = inst_31506);

return statearr_31766;
})();
if(cljs.core.truth_(inst_31507)){
var statearr_31767_33651 = state_31619__$1;
(statearr_31767_33651[(1)] = (13));

} else {
var statearr_31770_33653 = state_31619__$1;
(statearr_31770_33653[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (18))){
var inst_31542 = (state_31619[(2)]);
var state_31619__$1 = state_31619;
var statearr_31772_33654 = state_31619__$1;
(statearr_31772_33654[(2)] = inst_31542);

(statearr_31772_33654[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (42))){
var state_31619__$1 = state_31619;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31619__$1,(45),dchan);
} else {
if((state_val_31620 === (37))){
var inst_31485 = (state_31619[(10)]);
var inst_31578 = (state_31619[(25)]);
var inst_31587 = (state_31619[(23)]);
var inst_31587__$1 = cljs.core.first(inst_31578);
var inst_31588 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_31587__$1,inst_31485,done);
var state_31619__$1 = (function (){var statearr_31773 = state_31619;
(statearr_31773[(23)] = inst_31587__$1);

return statearr_31773;
})();
if(cljs.core.truth_(inst_31588)){
var statearr_31776_33655 = state_31619__$1;
(statearr_31776_33655[(1)] = (39));

} else {
var statearr_31777_33656 = state_31619__$1;
(statearr_31777_33656[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31620 === (8))){
var inst_31496 = (state_31619[(14)]);
var inst_31497 = (state_31619[(17)]);
var inst_31499 = (inst_31497 < inst_31496);
var inst_31500 = inst_31499;
var state_31619__$1 = state_31619;
if(cljs.core.truth_(inst_31500)){
var statearr_31782_33657 = state_31619__$1;
(statearr_31782_33657[(1)] = (10));

} else {
var statearr_31783_33658 = state_31619__$1;
(statearr_31783_33658[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto___33578,cs,m,dchan,dctr,done))
;
return ((function (switch__30294__auto__,c__30611__auto___33578,cs,m,dchan,dctr,done){
return (function() {
var cljs$core$async$mult_$_state_machine__30295__auto__ = null;
var cljs$core$async$mult_$_state_machine__30295__auto____0 = (function (){
var statearr_31787 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_31787[(0)] = cljs$core$async$mult_$_state_machine__30295__auto__);

(statearr_31787[(1)] = (1));

return statearr_31787;
});
var cljs$core$async$mult_$_state_machine__30295__auto____1 = (function (state_31619){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_31619);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e31788){if((e31788 instanceof Object)){
var ex__30298__auto__ = e31788;
var statearr_31789_33659 = state_31619;
(statearr_31789_33659[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31619);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31788;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33660 = state_31619;
state_31619 = G__33660;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__30295__auto__ = function(state_31619){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__30295__auto____1.call(this,state_31619);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__30295__auto____0;
cljs$core$async$mult_$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__30295__auto____1;
return cljs$core$async$mult_$_state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto___33578,cs,m,dchan,dctr,done))
})();
var state__30613__auto__ = (function (){var statearr_31793 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_31793[(6)] = c__30611__auto___33578);

return statearr_31793;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto___33578,cs,m,dchan,dctr,done))
);


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__31796 = arguments.length;
switch (G__31796) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(mult,ch,true);
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_(mult,ch,close_QMARK_);

return ch;
});

cljs.core.async.tap.cljs$lang$maxFixedArity = 3;

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_(mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_(mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4434__auto__.call(null,m,ch));
} else {
var m__4431__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4431__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.admix*",m);
}
}
}
});

cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4434__auto__.call(null,m,ch));
} else {
var m__4431__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4431__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.unmix*",m);
}
}
}
});

cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4434__auto__.call(null,m));
} else {
var m__4431__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4431__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mix.unmix-all*",m);
}
}
}
});

cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4434__auto__.call(null,m,state_map));
} else {
var m__4431__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4431__auto__.call(null,m,state_map));
} else {
throw cljs.core.missing_protocol("Mix.toggle*",m);
}
}
}
});

cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4434__auto__.call(null,m,mode));
} else {
var m__4431__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4431__auto__.call(null,m,mode));
} else {
throw cljs.core.missing_protocol("Mix.solo-mode*",m);
}
}
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__4736__auto__ = [];
var len__4730__auto___33671 = arguments.length;
var i__4731__auto___33672 = (0);
while(true){
if((i__4731__auto___33672 < len__4730__auto___33671)){
args__4736__auto__.push((arguments[i__4731__auto___33672]));

var G__33673 = (i__4731__auto___33672 + (1));
i__4731__auto___33672 = G__33673;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((3) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4737__auto__);
});

cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__31858){
var map__31859 = p__31858;
var map__31859__$1 = (((((!((map__31859 == null))))?(((((map__31859.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31859.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31859):map__31859);
var opts = map__31859__$1;
var statearr_31861_33677 = state;
(statearr_31861_33677[(1)] = cont_block);


var temp__5720__auto__ = cljs.core.async.do_alts(((function (map__31859,map__31859__$1,opts){
return (function (val){
var statearr_31863_33681 = state;
(statearr_31863_33681[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state);
});})(map__31859,map__31859__$1,opts))
,ports,opts);
if(cljs.core.truth_(temp__5720__auto__)){
var cb = temp__5720__auto__;
var statearr_31864_33682 = state;
(statearr_31864_33682[(2)] = cljs.core.deref(cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
});

cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3);

/** @this {Function} */
cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq31853){
var G__31854 = cljs.core.first(seq31853);
var seq31853__$1 = cljs.core.next(seq31853);
var G__31855 = cljs.core.first(seq31853__$1);
var seq31853__$2 = cljs.core.next(seq31853__$1);
var G__31856 = cljs.core.first(seq31853__$2);
var seq31853__$3 = cljs.core.next(seq31853__$2);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__31854,G__31855,G__31856,seq31853__$3);
});

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();
var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;
var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){
return cljs.core.reduce_kv(((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){
if(cljs.core.truth_((attr.cljs$core$IFn$_invoke$arity$1 ? attr.cljs$core$IFn$_invoke$arity$1(v) : attr.call(null,v)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,c);
} else {
return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;
var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){
var chs = cljs.core.deref(cs);
var mode = cljs.core.deref(solo_mode);
var solos = pick(new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick(new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick(new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_(solos))))))?cljs.core.vec(solos):cljs.core.vec(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(pauses,cljs.core.keys(chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async31867 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async31867 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta31868){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta31868 = meta31868;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async31867.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_31869,meta31868__$1){
var self__ = this;
var _31869__$1 = this;
return (new cljs.core.async.t_cljs$core$async31867(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta31868__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31867.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_31869){
var self__ = this;
var _31869__$1 = this;
return self__.meta31868;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31867.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async31867.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31867.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async31867.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31867.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31867.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31867.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.merge_with,cljs.core.merge),state_map);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31867.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.solo_modes.cljs$core$IFn$_invoke$arity$1 ? self__.solo_modes.cljs$core$IFn$_invoke$arity$1(mode) : self__.solo_modes.call(null,mode)))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_(self__.solo_mode,mode);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31867.getBasis = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta31868","meta31868",1532783707,null)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async31867.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async31867.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async31867";

cljs.core.async.t_cljs$core$async31867.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async31867");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async31867.
 */
cljs.core.async.__GT_t_cljs$core$async31867 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function cljs$core$async$mix_$___GT_t_cljs$core$async31867(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta31868){
return (new cljs.core.async.t_cljs$core$async31867(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta31868));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

}

return (new cljs.core.async.t_cljs$core$async31867(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__30611__auto___33709 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto___33709,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto___33709,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_31988){
var state_val_31989 = (state_31988[(1)]);
if((state_val_31989 === (7))){
var inst_31896 = (state_31988[(2)]);
var state_31988__$1 = state_31988;
var statearr_31990_33714 = state_31988__$1;
(statearr_31990_33714[(2)] = inst_31896);

(statearr_31990_33714[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (20))){
var inst_31909 = (state_31988[(7)]);
var state_31988__$1 = state_31988;
var statearr_31991_33716 = state_31988__$1;
(statearr_31991_33716[(2)] = inst_31909);

(statearr_31991_33716[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (27))){
var state_31988__$1 = state_31988;
var statearr_31992_33717 = state_31988__$1;
(statearr_31992_33717[(2)] = null);

(statearr_31992_33717[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (1))){
var inst_31880 = (state_31988[(8)]);
var inst_31880__$1 = calc_state();
var inst_31882 = (inst_31880__$1 == null);
var inst_31883 = cljs.core.not(inst_31882);
var state_31988__$1 = (function (){var statearr_31993 = state_31988;
(statearr_31993[(8)] = inst_31880__$1);

return statearr_31993;
})();
if(inst_31883){
var statearr_31996_33723 = state_31988__$1;
(statearr_31996_33723[(1)] = (2));

} else {
var statearr_31997_33724 = state_31988__$1;
(statearr_31997_33724[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (24))){
var inst_31945 = (state_31988[(9)]);
var inst_31934 = (state_31988[(10)]);
var inst_31959 = (state_31988[(11)]);
var inst_31959__$1 = (inst_31934.cljs$core$IFn$_invoke$arity$1 ? inst_31934.cljs$core$IFn$_invoke$arity$1(inst_31945) : inst_31934.call(null,inst_31945));
var state_31988__$1 = (function (){var statearr_31998 = state_31988;
(statearr_31998[(11)] = inst_31959__$1);

return statearr_31998;
})();
if(cljs.core.truth_(inst_31959__$1)){
var statearr_31999_33725 = state_31988__$1;
(statearr_31999_33725[(1)] = (29));

} else {
var statearr_32000_33726 = state_31988__$1;
(statearr_32000_33726[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (4))){
var inst_31899 = (state_31988[(2)]);
var state_31988__$1 = state_31988;
if(cljs.core.truth_(inst_31899)){
var statearr_32001_33727 = state_31988__$1;
(statearr_32001_33727[(1)] = (8));

} else {
var statearr_32002_33728 = state_31988__$1;
(statearr_32002_33728[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (15))){
var inst_31928 = (state_31988[(2)]);
var state_31988__$1 = state_31988;
if(cljs.core.truth_(inst_31928)){
var statearr_32007_33729 = state_31988__$1;
(statearr_32007_33729[(1)] = (19));

} else {
var statearr_32008_33730 = state_31988__$1;
(statearr_32008_33730[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (21))){
var inst_31933 = (state_31988[(12)]);
var inst_31933__$1 = (state_31988[(2)]);
var inst_31934 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_31933__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_31935 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_31933__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_31936 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_31933__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_31988__$1 = (function (){var statearr_32009 = state_31988;
(statearr_32009[(12)] = inst_31933__$1);

(statearr_32009[(13)] = inst_31935);

(statearr_32009[(10)] = inst_31934);

return statearr_32009;
})();
return cljs.core.async.ioc_alts_BANG_(state_31988__$1,(22),inst_31936);
} else {
if((state_val_31989 === (31))){
var inst_31967 = (state_31988[(2)]);
var state_31988__$1 = state_31988;
if(cljs.core.truth_(inst_31967)){
var statearr_32018_33732 = state_31988__$1;
(statearr_32018_33732[(1)] = (32));

} else {
var statearr_32019_33733 = state_31988__$1;
(statearr_32019_33733[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (32))){
var inst_31944 = (state_31988[(14)]);
var state_31988__$1 = state_31988;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31988__$1,(35),out,inst_31944);
} else {
if((state_val_31989 === (33))){
var inst_31933 = (state_31988[(12)]);
var inst_31909 = inst_31933;
var state_31988__$1 = (function (){var statearr_32020 = state_31988;
(statearr_32020[(7)] = inst_31909);

return statearr_32020;
})();
var statearr_32021_33734 = state_31988__$1;
(statearr_32021_33734[(2)] = null);

(statearr_32021_33734[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (13))){
var inst_31909 = (state_31988[(7)]);
var inst_31917 = inst_31909.cljs$lang$protocol_mask$partition0$;
var inst_31918 = (inst_31917 & (64));
var inst_31919 = inst_31909.cljs$core$ISeq$;
var inst_31920 = (cljs.core.PROTOCOL_SENTINEL === inst_31919);
var inst_31921 = ((inst_31918) || (inst_31920));
var state_31988__$1 = state_31988;
if(cljs.core.truth_(inst_31921)){
var statearr_32023_33735 = state_31988__$1;
(statearr_32023_33735[(1)] = (16));

} else {
var statearr_32024_33736 = state_31988__$1;
(statearr_32024_33736[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (22))){
var inst_31944 = (state_31988[(14)]);
var inst_31945 = (state_31988[(9)]);
var inst_31943 = (state_31988[(2)]);
var inst_31944__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31943,(0),null);
var inst_31945__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31943,(1),null);
var inst_31946 = (inst_31944__$1 == null);
var inst_31947 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_31945__$1,change);
var inst_31948 = ((inst_31946) || (inst_31947));
var state_31988__$1 = (function (){var statearr_32026 = state_31988;
(statearr_32026[(14)] = inst_31944__$1);

(statearr_32026[(9)] = inst_31945__$1);

return statearr_32026;
})();
if(cljs.core.truth_(inst_31948)){
var statearr_32027_33744 = state_31988__$1;
(statearr_32027_33744[(1)] = (23));

} else {
var statearr_32028_33745 = state_31988__$1;
(statearr_32028_33745[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (36))){
var inst_31933 = (state_31988[(12)]);
var inst_31909 = inst_31933;
var state_31988__$1 = (function (){var statearr_32029 = state_31988;
(statearr_32029[(7)] = inst_31909);

return statearr_32029;
})();
var statearr_32030_33746 = state_31988__$1;
(statearr_32030_33746[(2)] = null);

(statearr_32030_33746[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (29))){
var inst_31959 = (state_31988[(11)]);
var state_31988__$1 = state_31988;
var statearr_32031_33747 = state_31988__$1;
(statearr_32031_33747[(2)] = inst_31959);

(statearr_32031_33747[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (6))){
var state_31988__$1 = state_31988;
var statearr_32032_33748 = state_31988__$1;
(statearr_32032_33748[(2)] = false);

(statearr_32032_33748[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (28))){
var inst_31955 = (state_31988[(2)]);
var inst_31956 = calc_state();
var inst_31909 = inst_31956;
var state_31988__$1 = (function (){var statearr_32033 = state_31988;
(statearr_32033[(15)] = inst_31955);

(statearr_32033[(7)] = inst_31909);

return statearr_32033;
})();
var statearr_32035_33749 = state_31988__$1;
(statearr_32035_33749[(2)] = null);

(statearr_32035_33749[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (25))){
var inst_31981 = (state_31988[(2)]);
var state_31988__$1 = state_31988;
var statearr_32036_33750 = state_31988__$1;
(statearr_32036_33750[(2)] = inst_31981);

(statearr_32036_33750[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (34))){
var inst_31979 = (state_31988[(2)]);
var state_31988__$1 = state_31988;
var statearr_32038_33751 = state_31988__$1;
(statearr_32038_33751[(2)] = inst_31979);

(statearr_32038_33751[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (17))){
var state_31988__$1 = state_31988;
var statearr_32039_33754 = state_31988__$1;
(statearr_32039_33754[(2)] = false);

(statearr_32039_33754[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (3))){
var state_31988__$1 = state_31988;
var statearr_32040_33755 = state_31988__$1;
(statearr_32040_33755[(2)] = false);

(statearr_32040_33755[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (12))){
var inst_31986 = (state_31988[(2)]);
var state_31988__$1 = state_31988;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31988__$1,inst_31986);
} else {
if((state_val_31989 === (2))){
var inst_31880 = (state_31988[(8)]);
var inst_31888 = inst_31880.cljs$lang$protocol_mask$partition0$;
var inst_31889 = (inst_31888 & (64));
var inst_31890 = inst_31880.cljs$core$ISeq$;
var inst_31891 = (cljs.core.PROTOCOL_SENTINEL === inst_31890);
var inst_31892 = ((inst_31889) || (inst_31891));
var state_31988__$1 = state_31988;
if(cljs.core.truth_(inst_31892)){
var statearr_32041_33757 = state_31988__$1;
(statearr_32041_33757[(1)] = (5));

} else {
var statearr_32042_33758 = state_31988__$1;
(statearr_32042_33758[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (23))){
var inst_31944 = (state_31988[(14)]);
var inst_31950 = (inst_31944 == null);
var state_31988__$1 = state_31988;
if(cljs.core.truth_(inst_31950)){
var statearr_32043_33759 = state_31988__$1;
(statearr_32043_33759[(1)] = (26));

} else {
var statearr_32044_33760 = state_31988__$1;
(statearr_32044_33760[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (35))){
var inst_31970 = (state_31988[(2)]);
var state_31988__$1 = state_31988;
if(cljs.core.truth_(inst_31970)){
var statearr_32045_33762 = state_31988__$1;
(statearr_32045_33762[(1)] = (36));

} else {
var statearr_32046_33773 = state_31988__$1;
(statearr_32046_33773[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (19))){
var inst_31909 = (state_31988[(7)]);
var inst_31930 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_31909);
var state_31988__$1 = state_31988;
var statearr_32048_33774 = state_31988__$1;
(statearr_32048_33774[(2)] = inst_31930);

(statearr_32048_33774[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (11))){
var inst_31909 = (state_31988[(7)]);
var inst_31913 = (inst_31909 == null);
var inst_31914 = cljs.core.not(inst_31913);
var state_31988__$1 = state_31988;
if(inst_31914){
var statearr_32049_33775 = state_31988__$1;
(statearr_32049_33775[(1)] = (13));

} else {
var statearr_32050_33777 = state_31988__$1;
(statearr_32050_33777[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (9))){
var inst_31880 = (state_31988[(8)]);
var state_31988__$1 = state_31988;
var statearr_32051_33778 = state_31988__$1;
(statearr_32051_33778[(2)] = inst_31880);

(statearr_32051_33778[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (5))){
var state_31988__$1 = state_31988;
var statearr_32053_33779 = state_31988__$1;
(statearr_32053_33779[(2)] = true);

(statearr_32053_33779[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (14))){
var state_31988__$1 = state_31988;
var statearr_32054_33780 = state_31988__$1;
(statearr_32054_33780[(2)] = false);

(statearr_32054_33780[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (26))){
var inst_31945 = (state_31988[(9)]);
var inst_31952 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cs,cljs.core.dissoc,inst_31945);
var state_31988__$1 = state_31988;
var statearr_32058_33781 = state_31988__$1;
(statearr_32058_33781[(2)] = inst_31952);

(statearr_32058_33781[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (16))){
var state_31988__$1 = state_31988;
var statearr_32060_33787 = state_31988__$1;
(statearr_32060_33787[(2)] = true);

(statearr_32060_33787[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (38))){
var inst_31975 = (state_31988[(2)]);
var state_31988__$1 = state_31988;
var statearr_32061_33790 = state_31988__$1;
(statearr_32061_33790[(2)] = inst_31975);

(statearr_32061_33790[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (30))){
var inst_31945 = (state_31988[(9)]);
var inst_31935 = (state_31988[(13)]);
var inst_31934 = (state_31988[(10)]);
var inst_31962 = cljs.core.empty_QMARK_(inst_31934);
var inst_31963 = (inst_31935.cljs$core$IFn$_invoke$arity$1 ? inst_31935.cljs$core$IFn$_invoke$arity$1(inst_31945) : inst_31935.call(null,inst_31945));
var inst_31964 = cljs.core.not(inst_31963);
var inst_31965 = ((inst_31962) && (inst_31964));
var state_31988__$1 = state_31988;
var statearr_32062_33791 = state_31988__$1;
(statearr_32062_33791[(2)] = inst_31965);

(statearr_32062_33791[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (10))){
var inst_31880 = (state_31988[(8)]);
var inst_31904 = (state_31988[(2)]);
var inst_31905 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_31904,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_31906 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_31904,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_31908 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_31904,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_31909 = inst_31880;
var state_31988__$1 = (function (){var statearr_32066 = state_31988;
(statearr_32066[(16)] = inst_31905);

(statearr_32066[(17)] = inst_31908);

(statearr_32066[(18)] = inst_31906);

(statearr_32066[(7)] = inst_31909);

return statearr_32066;
})();
var statearr_32067_33792 = state_31988__$1;
(statearr_32067_33792[(2)] = null);

(statearr_32067_33792[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (18))){
var inst_31925 = (state_31988[(2)]);
var state_31988__$1 = state_31988;
var statearr_32070_33793 = state_31988__$1;
(statearr_32070_33793[(2)] = inst_31925);

(statearr_32070_33793[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (37))){
var state_31988__$1 = state_31988;
var statearr_32072_33794 = state_31988__$1;
(statearr_32072_33794[(2)] = null);

(statearr_32072_33794[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31989 === (8))){
var inst_31880 = (state_31988[(8)]);
var inst_31901 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_31880);
var state_31988__$1 = state_31988;
var statearr_32073_33795 = state_31988__$1;
(statearr_32073_33795[(2)] = inst_31901);

(statearr_32073_33795[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto___33709,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;
return ((function (switch__30294__auto__,c__30611__auto___33709,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var cljs$core$async$mix_$_state_machine__30295__auto__ = null;
var cljs$core$async$mix_$_state_machine__30295__auto____0 = (function (){
var statearr_32079 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32079[(0)] = cljs$core$async$mix_$_state_machine__30295__auto__);

(statearr_32079[(1)] = (1));

return statearr_32079;
});
var cljs$core$async$mix_$_state_machine__30295__auto____1 = (function (state_31988){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_31988);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e32081){if((e32081 instanceof Object)){
var ex__30298__auto__ = e32081;
var statearr_32083_33797 = state_31988;
(statearr_32083_33797[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_31988);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32081;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33799 = state_31988;
state_31988 = G__33799;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__30295__auto__ = function(state_31988){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__30295__auto____1.call(this,state_31988);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__30295__auto____0;
cljs$core$async$mix_$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__30295__auto____1;
return cljs$core$async$mix_$_state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto___33709,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();
var state__30613__auto__ = (function (){var statearr_32091 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_32091[(6)] = c__30611__auto___33709);

return statearr_32091;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto___33709,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_(mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_(mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_(mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_(mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_(mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4434__auto__.call(null,p,v,ch,close_QMARK_));
} else {
var m__4431__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4431__auto__.call(null,p,v,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Pub.sub*",p);
}
}
}
});

cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4434__auto__.call(null,p,v,ch));
} else {
var m__4431__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4431__auto__.call(null,p,v,ch));
} else {
throw cljs.core.missing_protocol("Pub.unsub*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__32107 = arguments.length;
switch (G__32107) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4434__auto__.call(null,p));
} else {
var m__4431__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4431__auto__.call(null,p));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4434__auto__.call(null,p,v));
} else {
var m__4431__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4431__auto__.call(null,p,v));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2;


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__32117 = arguments.length;
switch (G__32117) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3(ch,topic_fn,cljs.core.constantly(null));
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = ((function (mults){
return (function (topic){
var or__4131__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(mults),topic);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(mults,((function (or__4131__auto__,mults){
return (function (p1__32115_SHARP_){
if(cljs.core.truth_((p1__32115_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p1__32115_SHARP_.cljs$core$IFn$_invoke$arity$1(topic) : p1__32115_SHARP_.call(null,topic)))){
return p1__32115_SHARP_;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__32115_SHARP_,topic,cljs.core.async.mult(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((buf_fn.cljs$core$IFn$_invoke$arity$1 ? buf_fn.cljs$core$IFn$_invoke$arity$1(topic) : buf_fn.call(null,topic)))));
}
});})(or__4131__auto__,mults))
),topic);
}
});})(mults))
;
var p = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32118 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32118 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta32119){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta32119 = meta32119;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async32118.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_32120,meta32119__$1){
var self__ = this;
var _32120__$1 = this;
return (new cljs.core.async.t_cljs$core$async32118(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta32119__$1));
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32118.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_32120){
var self__ = this;
var _32120__$1 = this;
return self__.meta32119;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32118.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32118.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32118.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32118.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = (self__.ensure_mult.cljs$core$IFn$_invoke$arity$1 ? self__.ensure_mult.cljs$core$IFn$_invoke$arity$1(topic) : self__.ensure_mult.call(null,topic));
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(m,ch__$1,close_QMARK_);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32118.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5720__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self__.mults),topic);
if(cljs.core.truth_(temp__5720__auto__)){
var m = temp__5720__auto__;
return cljs.core.async.untap(m,ch__$1);
} else {
return null;
}
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32118.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_(self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32118.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32118.getBasis = ((function (mults,ensure_mult){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta32119","meta32119",-846570278,null)], null);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async32118.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async32118.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32118";

cljs.core.async.t_cljs$core$async32118.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async32118");
});})(mults,ensure_mult))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32118.
 */
cljs.core.async.__GT_t_cljs$core$async32118 = ((function (mults,ensure_mult){
return (function cljs$core$async$__GT_t_cljs$core$async32118(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta32119){
return (new cljs.core.async.t_cljs$core$async32118(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta32119));
});})(mults,ensure_mult))
;

}

return (new cljs.core.async.t_cljs$core$async32118(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__30611__auto___33817 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto___33817,mults,ensure_mult,p){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto___33817,mults,ensure_mult,p){
return (function (state_32230){
var state_val_32231 = (state_32230[(1)]);
if((state_val_32231 === (7))){
var inst_32226 = (state_32230[(2)]);
var state_32230__$1 = state_32230;
var statearr_32232_33820 = state_32230__$1;
(statearr_32232_33820[(2)] = inst_32226);

(statearr_32232_33820[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (20))){
var state_32230__$1 = state_32230;
var statearr_32233_33821 = state_32230__$1;
(statearr_32233_33821[(2)] = null);

(statearr_32233_33821[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (1))){
var state_32230__$1 = state_32230;
var statearr_32234_33825 = state_32230__$1;
(statearr_32234_33825[(2)] = null);

(statearr_32234_33825[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (24))){
var inst_32206 = (state_32230[(7)]);
var inst_32218 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(mults,cljs.core.dissoc,inst_32206);
var state_32230__$1 = state_32230;
var statearr_32235_33826 = state_32230__$1;
(statearr_32235_33826[(2)] = inst_32218);

(statearr_32235_33826[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (4))){
var inst_32151 = (state_32230[(8)]);
var inst_32151__$1 = (state_32230[(2)]);
var inst_32157 = (inst_32151__$1 == null);
var state_32230__$1 = (function (){var statearr_32236 = state_32230;
(statearr_32236[(8)] = inst_32151__$1);

return statearr_32236;
})();
if(cljs.core.truth_(inst_32157)){
var statearr_32237_33834 = state_32230__$1;
(statearr_32237_33834[(1)] = (5));

} else {
var statearr_32239_33835 = state_32230__$1;
(statearr_32239_33835[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (15))){
var inst_32200 = (state_32230[(2)]);
var state_32230__$1 = state_32230;
var statearr_32240_33837 = state_32230__$1;
(statearr_32240_33837[(2)] = inst_32200);

(statearr_32240_33837[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (21))){
var inst_32223 = (state_32230[(2)]);
var state_32230__$1 = (function (){var statearr_32241 = state_32230;
(statearr_32241[(9)] = inst_32223);

return statearr_32241;
})();
var statearr_32242_33840 = state_32230__$1;
(statearr_32242_33840[(2)] = null);

(statearr_32242_33840[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (13))){
var inst_32182 = (state_32230[(10)]);
var inst_32184 = cljs.core.chunked_seq_QMARK_(inst_32182);
var state_32230__$1 = state_32230;
if(inst_32184){
var statearr_32243_33844 = state_32230__$1;
(statearr_32243_33844[(1)] = (16));

} else {
var statearr_32244_33845 = state_32230__$1;
(statearr_32244_33845[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (22))){
var inst_32212 = (state_32230[(2)]);
var state_32230__$1 = state_32230;
if(cljs.core.truth_(inst_32212)){
var statearr_32245_33849 = state_32230__$1;
(statearr_32245_33849[(1)] = (23));

} else {
var statearr_32246_33850 = state_32230__$1;
(statearr_32246_33850[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (6))){
var inst_32208 = (state_32230[(11)]);
var inst_32206 = (state_32230[(7)]);
var inst_32151 = (state_32230[(8)]);
var inst_32206__$1 = (topic_fn.cljs$core$IFn$_invoke$arity$1 ? topic_fn.cljs$core$IFn$_invoke$arity$1(inst_32151) : topic_fn.call(null,inst_32151));
var inst_32207 = cljs.core.deref(mults);
var inst_32208__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32207,inst_32206__$1);
var state_32230__$1 = (function (){var statearr_32247 = state_32230;
(statearr_32247[(11)] = inst_32208__$1);

(statearr_32247[(7)] = inst_32206__$1);

return statearr_32247;
})();
if(cljs.core.truth_(inst_32208__$1)){
var statearr_32248_33852 = state_32230__$1;
(statearr_32248_33852[(1)] = (19));

} else {
var statearr_32249_33853 = state_32230__$1;
(statearr_32249_33853[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (25))){
var inst_32220 = (state_32230[(2)]);
var state_32230__$1 = state_32230;
var statearr_32250_33854 = state_32230__$1;
(statearr_32250_33854[(2)] = inst_32220);

(statearr_32250_33854[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (17))){
var inst_32182 = (state_32230[(10)]);
var inst_32191 = cljs.core.first(inst_32182);
var inst_32192 = cljs.core.async.muxch_STAR_(inst_32191);
var inst_32193 = cljs.core.async.close_BANG_(inst_32192);
var inst_32194 = cljs.core.next(inst_32182);
var inst_32168 = inst_32194;
var inst_32169 = null;
var inst_32170 = (0);
var inst_32171 = (0);
var state_32230__$1 = (function (){var statearr_32255 = state_32230;
(statearr_32255[(12)] = inst_32193);

(statearr_32255[(13)] = inst_32170);

(statearr_32255[(14)] = inst_32169);

(statearr_32255[(15)] = inst_32171);

(statearr_32255[(16)] = inst_32168);

return statearr_32255;
})();
var statearr_32260_33858 = state_32230__$1;
(statearr_32260_33858[(2)] = null);

(statearr_32260_33858[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (3))){
var inst_32228 = (state_32230[(2)]);
var state_32230__$1 = state_32230;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32230__$1,inst_32228);
} else {
if((state_val_32231 === (12))){
var inst_32202 = (state_32230[(2)]);
var state_32230__$1 = state_32230;
var statearr_32265_33865 = state_32230__$1;
(statearr_32265_33865[(2)] = inst_32202);

(statearr_32265_33865[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (2))){
var state_32230__$1 = state_32230;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32230__$1,(4),ch);
} else {
if((state_val_32231 === (23))){
var state_32230__$1 = state_32230;
var statearr_32266_33872 = state_32230__$1;
(statearr_32266_33872[(2)] = null);

(statearr_32266_33872[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (19))){
var inst_32208 = (state_32230[(11)]);
var inst_32151 = (state_32230[(8)]);
var inst_32210 = cljs.core.async.muxch_STAR_(inst_32208);
var state_32230__$1 = state_32230;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32230__$1,(22),inst_32210,inst_32151);
} else {
if((state_val_32231 === (11))){
var inst_32182 = (state_32230[(10)]);
var inst_32168 = (state_32230[(16)]);
var inst_32182__$1 = cljs.core.seq(inst_32168);
var state_32230__$1 = (function (){var statearr_32268 = state_32230;
(statearr_32268[(10)] = inst_32182__$1);

return statearr_32268;
})();
if(inst_32182__$1){
var statearr_32269_33873 = state_32230__$1;
(statearr_32269_33873[(1)] = (13));

} else {
var statearr_32270_33874 = state_32230__$1;
(statearr_32270_33874[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (9))){
var inst_32204 = (state_32230[(2)]);
var state_32230__$1 = state_32230;
var statearr_32271_33875 = state_32230__$1;
(statearr_32271_33875[(2)] = inst_32204);

(statearr_32271_33875[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (5))){
var inst_32165 = cljs.core.deref(mults);
var inst_32166 = cljs.core.vals(inst_32165);
var inst_32167 = cljs.core.seq(inst_32166);
var inst_32168 = inst_32167;
var inst_32169 = null;
var inst_32170 = (0);
var inst_32171 = (0);
var state_32230__$1 = (function (){var statearr_32275 = state_32230;
(statearr_32275[(13)] = inst_32170);

(statearr_32275[(14)] = inst_32169);

(statearr_32275[(15)] = inst_32171);

(statearr_32275[(16)] = inst_32168);

return statearr_32275;
})();
var statearr_32276_33876 = state_32230__$1;
(statearr_32276_33876[(2)] = null);

(statearr_32276_33876[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (14))){
var state_32230__$1 = state_32230;
var statearr_32283_33877 = state_32230__$1;
(statearr_32283_33877[(2)] = null);

(statearr_32283_33877[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (16))){
var inst_32182 = (state_32230[(10)]);
var inst_32186 = cljs.core.chunk_first(inst_32182);
var inst_32187 = cljs.core.chunk_rest(inst_32182);
var inst_32188 = cljs.core.count(inst_32186);
var inst_32168 = inst_32187;
var inst_32169 = inst_32186;
var inst_32170 = inst_32188;
var inst_32171 = (0);
var state_32230__$1 = (function (){var statearr_32284 = state_32230;
(statearr_32284[(13)] = inst_32170);

(statearr_32284[(14)] = inst_32169);

(statearr_32284[(15)] = inst_32171);

(statearr_32284[(16)] = inst_32168);

return statearr_32284;
})();
var statearr_32287_33878 = state_32230__$1;
(statearr_32287_33878[(2)] = null);

(statearr_32287_33878[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (10))){
var inst_32170 = (state_32230[(13)]);
var inst_32169 = (state_32230[(14)]);
var inst_32171 = (state_32230[(15)]);
var inst_32168 = (state_32230[(16)]);
var inst_32176 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_32169,inst_32171);
var inst_32177 = cljs.core.async.muxch_STAR_(inst_32176);
var inst_32178 = cljs.core.async.close_BANG_(inst_32177);
var inst_32179 = (inst_32171 + (1));
var tmp32279 = inst_32170;
var tmp32280 = inst_32169;
var tmp32281 = inst_32168;
var inst_32168__$1 = tmp32281;
var inst_32169__$1 = tmp32280;
var inst_32170__$1 = tmp32279;
var inst_32171__$1 = inst_32179;
var state_32230__$1 = (function (){var statearr_32290 = state_32230;
(statearr_32290[(13)] = inst_32170__$1);

(statearr_32290[(17)] = inst_32178);

(statearr_32290[(14)] = inst_32169__$1);

(statearr_32290[(15)] = inst_32171__$1);

(statearr_32290[(16)] = inst_32168__$1);

return statearr_32290;
})();
var statearr_32294_33885 = state_32230__$1;
(statearr_32294_33885[(2)] = null);

(statearr_32294_33885[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (18))){
var inst_32197 = (state_32230[(2)]);
var state_32230__$1 = state_32230;
var statearr_32296_33886 = state_32230__$1;
(statearr_32296_33886[(2)] = inst_32197);

(statearr_32296_33886[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32231 === (8))){
var inst_32170 = (state_32230[(13)]);
var inst_32171 = (state_32230[(15)]);
var inst_32173 = (inst_32171 < inst_32170);
var inst_32174 = inst_32173;
var state_32230__$1 = state_32230;
if(cljs.core.truth_(inst_32174)){
var statearr_32297_33887 = state_32230__$1;
(statearr_32297_33887[(1)] = (10));

} else {
var statearr_32298_33888 = state_32230__$1;
(statearr_32298_33888[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto___33817,mults,ensure_mult,p))
;
return ((function (switch__30294__auto__,c__30611__auto___33817,mults,ensure_mult,p){
return (function() {
var cljs$core$async$state_machine__30295__auto__ = null;
var cljs$core$async$state_machine__30295__auto____0 = (function (){
var statearr_32299 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32299[(0)] = cljs$core$async$state_machine__30295__auto__);

(statearr_32299[(1)] = (1));

return statearr_32299;
});
var cljs$core$async$state_machine__30295__auto____1 = (function (state_32230){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_32230);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e32302){if((e32302 instanceof Object)){
var ex__30298__auto__ = e32302;
var statearr_32304_33889 = state_32230;
(statearr_32304_33889[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_32230);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32302;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33890 = state_32230;
state_32230 = G__33890;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$state_machine__30295__auto__ = function(state_32230){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30295__auto____1.call(this,state_32230);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30295__auto____0;
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30295__auto____1;
return cljs$core$async$state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto___33817,mults,ensure_mult,p))
})();
var state__30613__auto__ = (function (){var statearr_32305 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_32305[(6)] = c__30611__auto___33817);

return statearr_32305;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto___33817,mults,ensure_mult,p))
);


return p;
});

cljs.core.async.pub.cljs$lang$maxFixedArity = 3;

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__32310 = arguments.length;
switch (G__32310) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4(p,topic,ch,true);
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_(p,topic,ch,close_QMARK_);
});

cljs.core.async.sub.cljs$lang$maxFixedArity = 4;

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_(p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__32315 = arguments.length;
switch (G__32315) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1(p);
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2(p,topic);
});

cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2;

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__32324 = arguments.length;
switch (G__32324) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3(f,chs,null);
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec(chs);
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var cnt = cljs.core.count(chs__$1);
var rets = cljs.core.object_array.cljs$core$IFn$_invoke$arity$1(cnt);
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2(((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){
return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,rets.slice((0)));
} else {
return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.cljs$core$IFn$_invoke$arity$1(cnt));
var c__30611__auto___33906 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto___33906,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto___33906,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_32366){
var state_val_32367 = (state_32366[(1)]);
if((state_val_32367 === (7))){
var state_32366__$1 = state_32366;
var statearr_32368_33907 = state_32366__$1;
(statearr_32368_33907[(2)] = null);

(statearr_32368_33907[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32367 === (1))){
var state_32366__$1 = state_32366;
var statearr_32369_33908 = state_32366__$1;
(statearr_32369_33908[(2)] = null);

(statearr_32369_33908[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32367 === (4))){
var inst_32330 = (state_32366[(7)]);
var inst_32332 = (inst_32330 < cnt);
var state_32366__$1 = state_32366;
if(cljs.core.truth_(inst_32332)){
var statearr_32370_33909 = state_32366__$1;
(statearr_32370_33909[(1)] = (6));

} else {
var statearr_32371_33910 = state_32366__$1;
(statearr_32371_33910[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32367 === (15))){
var inst_32362 = (state_32366[(2)]);
var state_32366__$1 = state_32366;
var statearr_32372_33911 = state_32366__$1;
(statearr_32372_33911[(2)] = inst_32362);

(statearr_32372_33911[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32367 === (13))){
var inst_32355 = cljs.core.async.close_BANG_(out);
var state_32366__$1 = state_32366;
var statearr_32373_33915 = state_32366__$1;
(statearr_32373_33915[(2)] = inst_32355);

(statearr_32373_33915[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32367 === (6))){
var state_32366__$1 = state_32366;
var statearr_32374_33916 = state_32366__$1;
(statearr_32374_33916[(2)] = null);

(statearr_32374_33916[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32367 === (3))){
var inst_32364 = (state_32366[(2)]);
var state_32366__$1 = state_32366;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32366__$1,inst_32364);
} else {
if((state_val_32367 === (12))){
var inst_32352 = (state_32366[(8)]);
var inst_32352__$1 = (state_32366[(2)]);
var inst_32353 = cljs.core.some(cljs.core.nil_QMARK_,inst_32352__$1);
var state_32366__$1 = (function (){var statearr_32375 = state_32366;
(statearr_32375[(8)] = inst_32352__$1);

return statearr_32375;
})();
if(cljs.core.truth_(inst_32353)){
var statearr_32376_33920 = state_32366__$1;
(statearr_32376_33920[(1)] = (13));

} else {
var statearr_32377_33921 = state_32366__$1;
(statearr_32377_33921[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32367 === (2))){
var inst_32329 = cljs.core.reset_BANG_(dctr,cnt);
var inst_32330 = (0);
var state_32366__$1 = (function (){var statearr_32378 = state_32366;
(statearr_32378[(7)] = inst_32330);

(statearr_32378[(9)] = inst_32329);

return statearr_32378;
})();
var statearr_32379_33922 = state_32366__$1;
(statearr_32379_33922[(2)] = null);

(statearr_32379_33922[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32367 === (11))){
var inst_32330 = (state_32366[(7)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame(state_32366,(10),Object,null,(9));
var inst_32339 = (chs__$1.cljs$core$IFn$_invoke$arity$1 ? chs__$1.cljs$core$IFn$_invoke$arity$1(inst_32330) : chs__$1.call(null,inst_32330));
var inst_32340 = (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(inst_32330) : done.call(null,inst_32330));
var inst_32341 = cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2(inst_32339,inst_32340);
var state_32366__$1 = state_32366;
var statearr_32380_33925 = state_32366__$1;
(statearr_32380_33925[(2)] = inst_32341);


cljs.core.async.impl.ioc_helpers.process_exception(state_32366__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32367 === (9))){
var inst_32330 = (state_32366[(7)]);
var inst_32343 = (state_32366[(2)]);
var inst_32344 = (inst_32330 + (1));
var inst_32330__$1 = inst_32344;
var state_32366__$1 = (function (){var statearr_32381 = state_32366;
(statearr_32381[(10)] = inst_32343);

(statearr_32381[(7)] = inst_32330__$1);

return statearr_32381;
})();
var statearr_32382_33926 = state_32366__$1;
(statearr_32382_33926[(2)] = null);

(statearr_32382_33926[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32367 === (5))){
var inst_32350 = (state_32366[(2)]);
var state_32366__$1 = (function (){var statearr_32383 = state_32366;
(statearr_32383[(11)] = inst_32350);

return statearr_32383;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32366__$1,(12),dchan);
} else {
if((state_val_32367 === (14))){
var inst_32352 = (state_32366[(8)]);
var inst_32357 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,inst_32352);
var state_32366__$1 = state_32366;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32366__$1,(16),out,inst_32357);
} else {
if((state_val_32367 === (16))){
var inst_32359 = (state_32366[(2)]);
var state_32366__$1 = (function (){var statearr_32384 = state_32366;
(statearr_32384[(12)] = inst_32359);

return statearr_32384;
})();
var statearr_32385_33928 = state_32366__$1;
(statearr_32385_33928[(2)] = null);

(statearr_32385_33928[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32367 === (10))){
var inst_32334 = (state_32366[(2)]);
var inst_32335 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec);
var state_32366__$1 = (function (){var statearr_32386 = state_32366;
(statearr_32386[(13)] = inst_32334);

return statearr_32386;
})();
var statearr_32387_33930 = state_32366__$1;
(statearr_32387_33930[(2)] = inst_32335);


cljs.core.async.impl.ioc_helpers.process_exception(state_32366__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32367 === (8))){
var inst_32348 = (state_32366[(2)]);
var state_32366__$1 = state_32366;
var statearr_32388_33931 = state_32366__$1;
(statearr_32388_33931[(2)] = inst_32348);

(statearr_32388_33931[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto___33906,chs__$1,out,cnt,rets,dchan,dctr,done))
;
return ((function (switch__30294__auto__,c__30611__auto___33906,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var cljs$core$async$state_machine__30295__auto__ = null;
var cljs$core$async$state_machine__30295__auto____0 = (function (){
var statearr_32390 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32390[(0)] = cljs$core$async$state_machine__30295__auto__);

(statearr_32390[(1)] = (1));

return statearr_32390;
});
var cljs$core$async$state_machine__30295__auto____1 = (function (state_32366){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_32366);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e32392){if((e32392 instanceof Object)){
var ex__30298__auto__ = e32392;
var statearr_32393_33932 = state_32366;
(statearr_32393_33932[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_32366);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32392;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33936 = state_32366;
state_32366 = G__33936;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$state_machine__30295__auto__ = function(state_32366){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30295__auto____1.call(this,state_32366);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30295__auto____0;
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30295__auto____1;
return cljs$core$async$state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto___33906,chs__$1,out,cnt,rets,dchan,dctr,done))
})();
var state__30613__auto__ = (function (){var statearr_32394 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_32394[(6)] = c__30611__auto___33906);

return statearr_32394;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto___33906,chs__$1,out,cnt,rets,dchan,dctr,done))
);


return out;
});

cljs.core.async.map.cljs$lang$maxFixedArity = 3;

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__32397 = arguments.length;
switch (G__32397) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2(chs,null);
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30611__auto___33945 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto___33945,out){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto___33945,out){
return (function (state_32435){
var state_val_32436 = (state_32435[(1)]);
if((state_val_32436 === (7))){
var inst_32409 = (state_32435[(7)]);
var inst_32408 = (state_32435[(8)]);
var inst_32408__$1 = (state_32435[(2)]);
var inst_32409__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32408__$1,(0),null);
var inst_32410 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32408__$1,(1),null);
var inst_32411 = (inst_32409__$1 == null);
var state_32435__$1 = (function (){var statearr_32445 = state_32435;
(statearr_32445[(9)] = inst_32410);

(statearr_32445[(7)] = inst_32409__$1);

(statearr_32445[(8)] = inst_32408__$1);

return statearr_32445;
})();
if(cljs.core.truth_(inst_32411)){
var statearr_32448_33950 = state_32435__$1;
(statearr_32448_33950[(1)] = (8));

} else {
var statearr_32449_33951 = state_32435__$1;
(statearr_32449_33951[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32436 === (1))){
var inst_32398 = cljs.core.vec(chs);
var inst_32399 = inst_32398;
var state_32435__$1 = (function (){var statearr_32450 = state_32435;
(statearr_32450[(10)] = inst_32399);

return statearr_32450;
})();
var statearr_32451_33952 = state_32435__$1;
(statearr_32451_33952[(2)] = null);

(statearr_32451_33952[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32436 === (4))){
var inst_32399 = (state_32435[(10)]);
var state_32435__$1 = state_32435;
return cljs.core.async.ioc_alts_BANG_(state_32435__$1,(7),inst_32399);
} else {
if((state_val_32436 === (6))){
var inst_32431 = (state_32435[(2)]);
var state_32435__$1 = state_32435;
var statearr_32455_33953 = state_32435__$1;
(statearr_32455_33953[(2)] = inst_32431);

(statearr_32455_33953[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32436 === (3))){
var inst_32433 = (state_32435[(2)]);
var state_32435__$1 = state_32435;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32435__$1,inst_32433);
} else {
if((state_val_32436 === (2))){
var inst_32399 = (state_32435[(10)]);
var inst_32401 = cljs.core.count(inst_32399);
var inst_32402 = (inst_32401 > (0));
var state_32435__$1 = state_32435;
if(cljs.core.truth_(inst_32402)){
var statearr_32461_33954 = state_32435__$1;
(statearr_32461_33954[(1)] = (4));

} else {
var statearr_32464_33955 = state_32435__$1;
(statearr_32464_33955[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32436 === (11))){
var inst_32399 = (state_32435[(10)]);
var inst_32422 = (state_32435[(2)]);
var tmp32456 = inst_32399;
var inst_32399__$1 = tmp32456;
var state_32435__$1 = (function (){var statearr_32469 = state_32435;
(statearr_32469[(10)] = inst_32399__$1);

(statearr_32469[(11)] = inst_32422);

return statearr_32469;
})();
var statearr_32470_33956 = state_32435__$1;
(statearr_32470_33956[(2)] = null);

(statearr_32470_33956[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32436 === (9))){
var inst_32409 = (state_32435[(7)]);
var state_32435__$1 = state_32435;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32435__$1,(11),out,inst_32409);
} else {
if((state_val_32436 === (5))){
var inst_32429 = cljs.core.async.close_BANG_(out);
var state_32435__$1 = state_32435;
var statearr_32476_33957 = state_32435__$1;
(statearr_32476_33957[(2)] = inst_32429);

(statearr_32476_33957[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32436 === (10))){
var inst_32425 = (state_32435[(2)]);
var state_32435__$1 = state_32435;
var statearr_32478_33958 = state_32435__$1;
(statearr_32478_33958[(2)] = inst_32425);

(statearr_32478_33958[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32436 === (8))){
var inst_32399 = (state_32435[(10)]);
var inst_32410 = (state_32435[(9)]);
var inst_32409 = (state_32435[(7)]);
var inst_32408 = (state_32435[(8)]);
var inst_32417 = (function (){var cs = inst_32399;
var vec__32404 = inst_32408;
var v = inst_32409;
var c = inst_32410;
return ((function (cs,vec__32404,v,c,inst_32399,inst_32410,inst_32409,inst_32408,state_val_32436,c__30611__auto___33945,out){
return (function (p1__32395_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(c,p1__32395_SHARP_);
});
;})(cs,vec__32404,v,c,inst_32399,inst_32410,inst_32409,inst_32408,state_val_32436,c__30611__auto___33945,out))
})();
var inst_32418 = cljs.core.filterv(inst_32417,inst_32399);
var inst_32399__$1 = inst_32418;
var state_32435__$1 = (function (){var statearr_32482 = state_32435;
(statearr_32482[(10)] = inst_32399__$1);

return statearr_32482;
})();
var statearr_32483_33959 = state_32435__$1;
(statearr_32483_33959[(2)] = null);

(statearr_32483_33959[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto___33945,out))
;
return ((function (switch__30294__auto__,c__30611__auto___33945,out){
return (function() {
var cljs$core$async$state_machine__30295__auto__ = null;
var cljs$core$async$state_machine__30295__auto____0 = (function (){
var statearr_32484 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32484[(0)] = cljs$core$async$state_machine__30295__auto__);

(statearr_32484[(1)] = (1));

return statearr_32484;
});
var cljs$core$async$state_machine__30295__auto____1 = (function (state_32435){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_32435);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e32489){if((e32489 instanceof Object)){
var ex__30298__auto__ = e32489;
var statearr_32490_33961 = state_32435;
(statearr_32490_33961[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_32435);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32489;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33963 = state_32435;
state_32435 = G__33963;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$state_machine__30295__auto__ = function(state_32435){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30295__auto____1.call(this,state_32435);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30295__auto____0;
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30295__auto____1;
return cljs$core$async$state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto___33945,out))
})();
var state__30613__auto__ = (function (){var statearr_32491 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_32491[(6)] = c__30611__auto___33945);

return statearr_32491;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto___33945,out))
);


return out;
});

cljs.core.async.merge.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce(cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__32496 = arguments.length;
switch (G__32496) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3(n,ch,null);
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30611__auto___33967 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto___33967,out){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto___33967,out){
return (function (state_32523){
var state_val_32524 = (state_32523[(1)]);
if((state_val_32524 === (7))){
var inst_32505 = (state_32523[(7)]);
var inst_32505__$1 = (state_32523[(2)]);
var inst_32506 = (inst_32505__$1 == null);
var inst_32507 = cljs.core.not(inst_32506);
var state_32523__$1 = (function (){var statearr_32530 = state_32523;
(statearr_32530[(7)] = inst_32505__$1);

return statearr_32530;
})();
if(inst_32507){
var statearr_32533_33968 = state_32523__$1;
(statearr_32533_33968[(1)] = (8));

} else {
var statearr_32534_33969 = state_32523__$1;
(statearr_32534_33969[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32524 === (1))){
var inst_32500 = (0);
var state_32523__$1 = (function (){var statearr_32535 = state_32523;
(statearr_32535[(8)] = inst_32500);

return statearr_32535;
})();
var statearr_32537_33971 = state_32523__$1;
(statearr_32537_33971[(2)] = null);

(statearr_32537_33971[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32524 === (4))){
var state_32523__$1 = state_32523;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32523__$1,(7),ch);
} else {
if((state_val_32524 === (6))){
var inst_32518 = (state_32523[(2)]);
var state_32523__$1 = state_32523;
var statearr_32543_33972 = state_32523__$1;
(statearr_32543_33972[(2)] = inst_32518);

(statearr_32543_33972[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32524 === (3))){
var inst_32520 = (state_32523[(2)]);
var inst_32521 = cljs.core.async.close_BANG_(out);
var state_32523__$1 = (function (){var statearr_32545 = state_32523;
(statearr_32545[(9)] = inst_32520);

return statearr_32545;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_32523__$1,inst_32521);
} else {
if((state_val_32524 === (2))){
var inst_32500 = (state_32523[(8)]);
var inst_32502 = (inst_32500 < n);
var state_32523__$1 = state_32523;
if(cljs.core.truth_(inst_32502)){
var statearr_32549_33974 = state_32523__$1;
(statearr_32549_33974[(1)] = (4));

} else {
var statearr_32550_33975 = state_32523__$1;
(statearr_32550_33975[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32524 === (11))){
var inst_32500 = (state_32523[(8)]);
var inst_32510 = (state_32523[(2)]);
var inst_32511 = (inst_32500 + (1));
var inst_32500__$1 = inst_32511;
var state_32523__$1 = (function (){var statearr_32556 = state_32523;
(statearr_32556[(8)] = inst_32500__$1);

(statearr_32556[(10)] = inst_32510);

return statearr_32556;
})();
var statearr_32557_33976 = state_32523__$1;
(statearr_32557_33976[(2)] = null);

(statearr_32557_33976[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32524 === (9))){
var state_32523__$1 = state_32523;
var statearr_32562_33977 = state_32523__$1;
(statearr_32562_33977[(2)] = null);

(statearr_32562_33977[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32524 === (5))){
var state_32523__$1 = state_32523;
var statearr_32567_33978 = state_32523__$1;
(statearr_32567_33978[(2)] = null);

(statearr_32567_33978[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32524 === (10))){
var inst_32515 = (state_32523[(2)]);
var state_32523__$1 = state_32523;
var statearr_32569_33979 = state_32523__$1;
(statearr_32569_33979[(2)] = inst_32515);

(statearr_32569_33979[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32524 === (8))){
var inst_32505 = (state_32523[(7)]);
var state_32523__$1 = state_32523;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32523__$1,(11),out,inst_32505);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto___33967,out))
;
return ((function (switch__30294__auto__,c__30611__auto___33967,out){
return (function() {
var cljs$core$async$state_machine__30295__auto__ = null;
var cljs$core$async$state_machine__30295__auto____0 = (function (){
var statearr_32576 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_32576[(0)] = cljs$core$async$state_machine__30295__auto__);

(statearr_32576[(1)] = (1));

return statearr_32576;
});
var cljs$core$async$state_machine__30295__auto____1 = (function (state_32523){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_32523);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e32578){if((e32578 instanceof Object)){
var ex__30298__auto__ = e32578;
var statearr_32581_33988 = state_32523;
(statearr_32581_33988[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_32523);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32578;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33989 = state_32523;
state_32523 = G__33989;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$state_machine__30295__auto__ = function(state_32523){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30295__auto____1.call(this,state_32523);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30295__auto____0;
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30295__auto____1;
return cljs$core$async$state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto___33967,out))
})();
var state__30613__auto__ = (function (){var statearr_32583 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_32583[(6)] = c__30611__auto___33967);

return statearr_32583;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto___33967,out))
);


return out;
});

cljs.core.async.take.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32588 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32588 = (function (f,ch,meta32589){
this.f = f;
this.ch = ch;
this.meta32589 = meta32589;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async32588.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32590,meta32589__$1){
var self__ = this;
var _32590__$1 = this;
return (new cljs.core.async.t_cljs$core$async32588(self__.f,self__.ch,meta32589__$1));
});

cljs.core.async.t_cljs$core$async32588.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32590){
var self__ = this;
var _32590__$1 = this;
return self__.meta32589;
});

cljs.core.async.t_cljs$core$async32588.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32588.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
});

cljs.core.async.t_cljs$core$async32588.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
});

cljs.core.async.t_cljs$core$async32588.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32588.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_(self__.ch,(function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32599 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32599 = (function (f,ch,meta32589,_,fn1,meta32600){
this.f = f;
this.ch = ch;
this.meta32589 = meta32589;
this._ = _;
this.fn1 = fn1;
this.meta32600 = meta32600;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async32599.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_32601,meta32600__$1){
var self__ = this;
var _32601__$1 = this;
return (new cljs.core.async.t_cljs$core$async32599(self__.f,self__.ch,self__.meta32589,self__._,self__.fn1,meta32600__$1));
});})(___$1))
;

cljs.core.async.t_cljs$core$async32599.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_32601){
var self__ = this;
var _32601__$1 = this;
return self__.meta32600;
});})(___$1))
;

cljs.core.async.t_cljs$core$async32599.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32599.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.fn1);
});})(___$1))
;

cljs.core.async.t_cljs$core$async32599.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
});})(___$1))
;

cljs.core.async.t_cljs$core$async32599.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit(self__.fn1);
return ((function (f1,___$2,___$1){
return (function (p1__32587_SHARP_){
var G__32610 = (((p1__32587_SHARP_ == null))?null:(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(p1__32587_SHARP_) : self__.f.call(null,p1__32587_SHARP_)));
return (f1.cljs$core$IFn$_invoke$arity$1 ? f1.cljs$core$IFn$_invoke$arity$1(G__32610) : f1.call(null,G__32610));
});
;})(f1,___$2,___$1))
});})(___$1))
;

cljs.core.async.t_cljs$core$async32599.getBasis = ((function (___$1){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta32589","meta32589",1370297227,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async32588","cljs.core.async/t_cljs$core$async32588",-932857237,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta32600","meta32600",267443945,null)], null);
});})(___$1))
;

cljs.core.async.t_cljs$core$async32599.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async32599.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32599";

cljs.core.async.t_cljs$core$async32599.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async32599");
});})(___$1))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32599.
 */
cljs.core.async.__GT_t_cljs$core$async32599 = ((function (___$1){
return (function cljs$core$async$map_LT__$___GT_t_cljs$core$async32599(f__$1,ch__$1,meta32589__$1,___$2,fn1__$1,meta32600){
return (new cljs.core.async.t_cljs$core$async32599(f__$1,ch__$1,meta32589__$1,___$2,fn1__$1,meta32600));
});})(___$1))
;

}

return (new cljs.core.async.t_cljs$core$async32599(self__.f,self__.ch,self__.meta32589,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__4120__auto__ = ret;
if(cljs.core.truth_(and__4120__auto__)){
return (!((cljs.core.deref(ret) == null)));
} else {
return and__4120__auto__;
}
})())){
return cljs.core.async.impl.channels.box((function (){var G__32643 = cljs.core.deref(ret);
return (self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(G__32643) : self__.f.call(null,G__32643));
})());
} else {
return ret;
}
});

cljs.core.async.t_cljs$core$async32588.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32588.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
});

cljs.core.async.t_cljs$core$async32588.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta32589","meta32589",1370297227,null)], null);
});

cljs.core.async.t_cljs$core$async32588.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async32588.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32588";

cljs.core.async.t_cljs$core$async32588.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async32588");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32588.
 */
cljs.core.async.__GT_t_cljs$core$async32588 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async32588(f__$1,ch__$1,meta32589){
return (new cljs.core.async.t_cljs$core$async32588(f__$1,ch__$1,meta32589));
});

}

return (new cljs.core.async.t_cljs$core$async32588(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32681 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32681 = (function (f,ch,meta32682){
this.f = f;
this.ch = ch;
this.meta32682 = meta32682;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async32681.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32683,meta32682__$1){
var self__ = this;
var _32683__$1 = this;
return (new cljs.core.async.t_cljs$core$async32681(self__.f,self__.ch,meta32682__$1));
});

cljs.core.async.t_cljs$core$async32681.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32683){
var self__ = this;
var _32683__$1 = this;
return self__.meta32682;
});

cljs.core.async.t_cljs$core$async32681.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32681.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
});

cljs.core.async.t_cljs$core$async32681.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32681.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async32681.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32681.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(val) : self__.f.call(null,val)),fn1);
});

cljs.core.async.t_cljs$core$async32681.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta32682","meta32682",-1232106664,null)], null);
});

cljs.core.async.t_cljs$core$async32681.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async32681.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32681";

cljs.core.async.t_cljs$core$async32681.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async32681");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32681.
 */
cljs.core.async.__GT_t_cljs$core$async32681 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async32681(f__$1,ch__$1,meta32682){
return (new cljs.core.async.t_cljs$core$async32681(f__$1,ch__$1,meta32682));
});

}

return (new cljs.core.async.t_cljs$core$async32681(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32708 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32708 = (function (p,ch,meta32709){
this.p = p;
this.ch = ch;
this.meta32709 = meta32709;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async32708.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32710,meta32709__$1){
var self__ = this;
var _32710__$1 = this;
return (new cljs.core.async.t_cljs$core$async32708(self__.p,self__.ch,meta32709__$1));
});

cljs.core.async.t_cljs$core$async32708.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32710){
var self__ = this;
var _32710__$1 = this;
return self__.meta32709;
});

cljs.core.async.t_cljs$core$async32708.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32708.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
});

cljs.core.async.t_cljs$core$async32708.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
});

cljs.core.async.t_cljs$core$async32708.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32708.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async32708.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async32708.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.p.cljs$core$IFn$_invoke$arity$1 ? self__.p.cljs$core$IFn$_invoke$arity$1(val) : self__.p.call(null,val)))){
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box(cljs.core.not(cljs.core.async.impl.protocols.closed_QMARK_(self__.ch)));
}
});

cljs.core.async.t_cljs$core$async32708.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta32709","meta32709",-1695605243,null)], null);
});

cljs.core.async.t_cljs$core$async32708.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async32708.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32708";

cljs.core.async.t_cljs$core$async32708.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async32708");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32708.
 */
cljs.core.async.__GT_t_cljs$core$async32708 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async32708(p__$1,ch__$1,meta32709){
return (new cljs.core.async.t_cljs$core$async32708(p__$1,ch__$1,meta32709));
});

}

return (new cljs.core.async.t_cljs$core$async32708(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_(cljs.core.complement(p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__32738 = arguments.length;
switch (G__32738) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30611__auto___34045 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto___34045,out){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto___34045,out){
return (function (state_32773){
var state_val_32774 = (state_32773[(1)]);
if((state_val_32774 === (7))){
var inst_32769 = (state_32773[(2)]);
var state_32773__$1 = state_32773;
var statearr_32778_34047 = state_32773__$1;
(statearr_32778_34047[(2)] = inst_32769);

(statearr_32778_34047[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32774 === (1))){
var state_32773__$1 = state_32773;
var statearr_32779_34050 = state_32773__$1;
(statearr_32779_34050[(2)] = null);

(statearr_32779_34050[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32774 === (4))){
var inst_32751 = (state_32773[(7)]);
var inst_32751__$1 = (state_32773[(2)]);
var inst_32752 = (inst_32751__$1 == null);
var state_32773__$1 = (function (){var statearr_32780 = state_32773;
(statearr_32780[(7)] = inst_32751__$1);

return statearr_32780;
})();
if(cljs.core.truth_(inst_32752)){
var statearr_32781_34051 = state_32773__$1;
(statearr_32781_34051[(1)] = (5));

} else {
var statearr_32786_34052 = state_32773__$1;
(statearr_32786_34052[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32774 === (6))){
var inst_32751 = (state_32773[(7)]);
var inst_32756 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_32751) : p.call(null,inst_32751));
var state_32773__$1 = state_32773;
if(cljs.core.truth_(inst_32756)){
var statearr_32789_34053 = state_32773__$1;
(statearr_32789_34053[(1)] = (8));

} else {
var statearr_32790_34054 = state_32773__$1;
(statearr_32790_34054[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32774 === (3))){
var inst_32771 = (state_32773[(2)]);
var state_32773__$1 = state_32773;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32773__$1,inst_32771);
} else {
if((state_val_32774 === (2))){
var state_32773__$1 = state_32773;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32773__$1,(4),ch);
} else {
if((state_val_32774 === (11))){
var inst_32763 = (state_32773[(2)]);
var state_32773__$1 = state_32773;
var statearr_32794_34056 = state_32773__$1;
(statearr_32794_34056[(2)] = inst_32763);

(statearr_32794_34056[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32774 === (9))){
var state_32773__$1 = state_32773;
var statearr_32795_34057 = state_32773__$1;
(statearr_32795_34057[(2)] = null);

(statearr_32795_34057[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32774 === (5))){
var inst_32754 = cljs.core.async.close_BANG_(out);
var state_32773__$1 = state_32773;
var statearr_32798_34058 = state_32773__$1;
(statearr_32798_34058[(2)] = inst_32754);

(statearr_32798_34058[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32774 === (10))){
var inst_32766 = (state_32773[(2)]);
var state_32773__$1 = (function (){var statearr_32802 = state_32773;
(statearr_32802[(8)] = inst_32766);

return statearr_32802;
})();
var statearr_32803_34059 = state_32773__$1;
(statearr_32803_34059[(2)] = null);

(statearr_32803_34059[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32774 === (8))){
var inst_32751 = (state_32773[(7)]);
var state_32773__$1 = state_32773;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32773__$1,(11),out,inst_32751);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto___34045,out))
;
return ((function (switch__30294__auto__,c__30611__auto___34045,out){
return (function() {
var cljs$core$async$state_machine__30295__auto__ = null;
var cljs$core$async$state_machine__30295__auto____0 = (function (){
var statearr_32808 = [null,null,null,null,null,null,null,null,null];
(statearr_32808[(0)] = cljs$core$async$state_machine__30295__auto__);

(statearr_32808[(1)] = (1));

return statearr_32808;
});
var cljs$core$async$state_machine__30295__auto____1 = (function (state_32773){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_32773);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e32810){if((e32810 instanceof Object)){
var ex__30298__auto__ = e32810;
var statearr_32811_34082 = state_32773;
(statearr_32811_34082[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_32773);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32810;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34092 = state_32773;
state_32773 = G__34092;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$state_machine__30295__auto__ = function(state_32773){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30295__auto____1.call(this,state_32773);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30295__auto____0;
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30295__auto____1;
return cljs$core$async$state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto___34045,out))
})();
var state__30613__auto__ = (function (){var statearr_32819 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_32819[(6)] = c__30611__auto___34045);

return statearr_32819;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto___34045,out))
);


return out;
});

cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__32827 = arguments.length;
switch (G__32827) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(cljs.core.complement(p),ch,buf_or_n);
});

cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3;

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__30611__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto__){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto__){
return (function (state_32905){
var state_val_32906 = (state_32905[(1)]);
if((state_val_32906 === (7))){
var inst_32901 = (state_32905[(2)]);
var state_32905__$1 = state_32905;
var statearr_32909_34122 = state_32905__$1;
(statearr_32909_34122[(2)] = inst_32901);

(statearr_32909_34122[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (20))){
var inst_32865 = (state_32905[(7)]);
var inst_32882 = (state_32905[(2)]);
var inst_32883 = cljs.core.next(inst_32865);
var inst_32847 = inst_32883;
var inst_32848 = null;
var inst_32849 = (0);
var inst_32850 = (0);
var state_32905__$1 = (function (){var statearr_32910 = state_32905;
(statearr_32910[(8)] = inst_32849);

(statearr_32910[(9)] = inst_32850);

(statearr_32910[(10)] = inst_32882);

(statearr_32910[(11)] = inst_32848);

(statearr_32910[(12)] = inst_32847);

return statearr_32910;
})();
var statearr_32912_34139 = state_32905__$1;
(statearr_32912_34139[(2)] = null);

(statearr_32912_34139[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (1))){
var state_32905__$1 = state_32905;
var statearr_32913_34144 = state_32905__$1;
(statearr_32913_34144[(2)] = null);

(statearr_32913_34144[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (4))){
var inst_32836 = (state_32905[(13)]);
var inst_32836__$1 = (state_32905[(2)]);
var inst_32837 = (inst_32836__$1 == null);
var state_32905__$1 = (function (){var statearr_32915 = state_32905;
(statearr_32915[(13)] = inst_32836__$1);

return statearr_32915;
})();
if(cljs.core.truth_(inst_32837)){
var statearr_32916_34156 = state_32905__$1;
(statearr_32916_34156[(1)] = (5));

} else {
var statearr_32917_34157 = state_32905__$1;
(statearr_32917_34157[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (15))){
var state_32905__$1 = state_32905;
var statearr_32923_34158 = state_32905__$1;
(statearr_32923_34158[(2)] = null);

(statearr_32923_34158[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (21))){
var state_32905__$1 = state_32905;
var statearr_32925_34159 = state_32905__$1;
(statearr_32925_34159[(2)] = null);

(statearr_32925_34159[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (13))){
var inst_32849 = (state_32905[(8)]);
var inst_32850 = (state_32905[(9)]);
var inst_32848 = (state_32905[(11)]);
var inst_32847 = (state_32905[(12)]);
var inst_32859 = (state_32905[(2)]);
var inst_32860 = (inst_32850 + (1));
var tmp32919 = inst_32849;
var tmp32920 = inst_32848;
var tmp32921 = inst_32847;
var inst_32847__$1 = tmp32921;
var inst_32848__$1 = tmp32920;
var inst_32849__$1 = tmp32919;
var inst_32850__$1 = inst_32860;
var state_32905__$1 = (function (){var statearr_32930 = state_32905;
(statearr_32930[(8)] = inst_32849__$1);

(statearr_32930[(9)] = inst_32850__$1);

(statearr_32930[(14)] = inst_32859);

(statearr_32930[(11)] = inst_32848__$1);

(statearr_32930[(12)] = inst_32847__$1);

return statearr_32930;
})();
var statearr_32931_34164 = state_32905__$1;
(statearr_32931_34164[(2)] = null);

(statearr_32931_34164[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (22))){
var state_32905__$1 = state_32905;
var statearr_32934_34165 = state_32905__$1;
(statearr_32934_34165[(2)] = null);

(statearr_32934_34165[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (6))){
var inst_32836 = (state_32905[(13)]);
var inst_32845 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_32836) : f.call(null,inst_32836));
var inst_32846 = cljs.core.seq(inst_32845);
var inst_32847 = inst_32846;
var inst_32848 = null;
var inst_32849 = (0);
var inst_32850 = (0);
var state_32905__$1 = (function (){var statearr_32937 = state_32905;
(statearr_32937[(8)] = inst_32849);

(statearr_32937[(9)] = inst_32850);

(statearr_32937[(11)] = inst_32848);

(statearr_32937[(12)] = inst_32847);

return statearr_32937;
})();
var statearr_32938_34174 = state_32905__$1;
(statearr_32938_34174[(2)] = null);

(statearr_32938_34174[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (17))){
var inst_32865 = (state_32905[(7)]);
var inst_32871 = cljs.core.chunk_first(inst_32865);
var inst_32872 = cljs.core.chunk_rest(inst_32865);
var inst_32873 = cljs.core.count(inst_32871);
var inst_32847 = inst_32872;
var inst_32848 = inst_32871;
var inst_32849 = inst_32873;
var inst_32850 = (0);
var state_32905__$1 = (function (){var statearr_32939 = state_32905;
(statearr_32939[(8)] = inst_32849);

(statearr_32939[(9)] = inst_32850);

(statearr_32939[(11)] = inst_32848);

(statearr_32939[(12)] = inst_32847);

return statearr_32939;
})();
var statearr_32941_34180 = state_32905__$1;
(statearr_32941_34180[(2)] = null);

(statearr_32941_34180[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (3))){
var inst_32903 = (state_32905[(2)]);
var state_32905__$1 = state_32905;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32905__$1,inst_32903);
} else {
if((state_val_32906 === (12))){
var inst_32891 = (state_32905[(2)]);
var state_32905__$1 = state_32905;
var statearr_32942_34182 = state_32905__$1;
(statearr_32942_34182[(2)] = inst_32891);

(statearr_32942_34182[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (2))){
var state_32905__$1 = state_32905;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32905__$1,(4),in$);
} else {
if((state_val_32906 === (23))){
var inst_32899 = (state_32905[(2)]);
var state_32905__$1 = state_32905;
var statearr_32943_34183 = state_32905__$1;
(statearr_32943_34183[(2)] = inst_32899);

(statearr_32943_34183[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (19))){
var inst_32886 = (state_32905[(2)]);
var state_32905__$1 = state_32905;
var statearr_32944_34188 = state_32905__$1;
(statearr_32944_34188[(2)] = inst_32886);

(statearr_32944_34188[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (11))){
var inst_32847 = (state_32905[(12)]);
var inst_32865 = (state_32905[(7)]);
var inst_32865__$1 = cljs.core.seq(inst_32847);
var state_32905__$1 = (function (){var statearr_32945 = state_32905;
(statearr_32945[(7)] = inst_32865__$1);

return statearr_32945;
})();
if(inst_32865__$1){
var statearr_32946_34190 = state_32905__$1;
(statearr_32946_34190[(1)] = (14));

} else {
var statearr_32947_34191 = state_32905__$1;
(statearr_32947_34191[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (9))){
var inst_32893 = (state_32905[(2)]);
var inst_32894 = cljs.core.async.impl.protocols.closed_QMARK_(out);
var state_32905__$1 = (function (){var statearr_32948 = state_32905;
(statearr_32948[(15)] = inst_32893);

return statearr_32948;
})();
if(cljs.core.truth_(inst_32894)){
var statearr_32949_34192 = state_32905__$1;
(statearr_32949_34192[(1)] = (21));

} else {
var statearr_32950_34193 = state_32905__$1;
(statearr_32950_34193[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (5))){
var inst_32839 = cljs.core.async.close_BANG_(out);
var state_32905__$1 = state_32905;
var statearr_32951_34194 = state_32905__$1;
(statearr_32951_34194[(2)] = inst_32839);

(statearr_32951_34194[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (14))){
var inst_32865 = (state_32905[(7)]);
var inst_32869 = cljs.core.chunked_seq_QMARK_(inst_32865);
var state_32905__$1 = state_32905;
if(inst_32869){
var statearr_32952_34195 = state_32905__$1;
(statearr_32952_34195[(1)] = (17));

} else {
var statearr_32953_34196 = state_32905__$1;
(statearr_32953_34196[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (16))){
var inst_32889 = (state_32905[(2)]);
var state_32905__$1 = state_32905;
var statearr_32954_34197 = state_32905__$1;
(statearr_32954_34197[(2)] = inst_32889);

(statearr_32954_34197[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32906 === (10))){
var inst_32850 = (state_32905[(9)]);
var inst_32848 = (state_32905[(11)]);
var inst_32857 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_32848,inst_32850);
var state_32905__$1 = state_32905;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32905__$1,(13),out,inst_32857);
} else {
if((state_val_32906 === (18))){
var inst_32865 = (state_32905[(7)]);
var inst_32876 = cljs.core.first(inst_32865);
var state_32905__$1 = state_32905;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32905__$1,(20),out,inst_32876);
} else {
if((state_val_32906 === (8))){
var inst_32849 = (state_32905[(8)]);
var inst_32850 = (state_32905[(9)]);
var inst_32853 = (inst_32850 < inst_32849);
var inst_32854 = inst_32853;
var state_32905__$1 = state_32905;
if(cljs.core.truth_(inst_32854)){
var statearr_32955_34221 = state_32905__$1;
(statearr_32955_34221[(1)] = (10));

} else {
var statearr_32956_34226 = state_32905__$1;
(statearr_32956_34226[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto__))
;
return ((function (switch__30294__auto__,c__30611__auto__){
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__30295__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__30295__auto____0 = (function (){
var statearr_32957 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32957[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__30295__auto__);

(statearr_32957[(1)] = (1));

return statearr_32957;
});
var cljs$core$async$mapcat_STAR__$_state_machine__30295__auto____1 = (function (state_32905){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_32905);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e32958){if((e32958 instanceof Object)){
var ex__30298__auto__ = e32958;
var statearr_32959_34237 = state_32905;
(statearr_32959_34237[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_32905);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32958;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34244 = state_32905;
state_32905 = G__34244;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__30295__auto__ = function(state_32905){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__30295__auto____1.call(this,state_32905);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__30295__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__30295__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto__))
})();
var state__30613__auto__ = (function (){var statearr_32961 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_32961[(6)] = c__30611__auto__);

return statearr_32961;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto__))
);

return c__30611__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__32964 = arguments.length;
switch (G__32964) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3(f,in$,null);
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return out;
});

cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__32967 = arguments.length;
switch (G__32967) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3(f,out,null);
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return in$;
});

cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__32981 = arguments.length;
switch (G__32981) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2(ch,null);
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30611__auto___34264 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto___34264,out){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto___34264,out){
return (function (state_33005){
var state_val_33006 = (state_33005[(1)]);
if((state_val_33006 === (7))){
var inst_33000 = (state_33005[(2)]);
var state_33005__$1 = state_33005;
var statearr_33007_34269 = state_33005__$1;
(statearr_33007_34269[(2)] = inst_33000);

(statearr_33007_34269[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33006 === (1))){
var inst_32982 = null;
var state_33005__$1 = (function (){var statearr_33008 = state_33005;
(statearr_33008[(7)] = inst_32982);

return statearr_33008;
})();
var statearr_33009_34274 = state_33005__$1;
(statearr_33009_34274[(2)] = null);

(statearr_33009_34274[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33006 === (4))){
var inst_32985 = (state_33005[(8)]);
var inst_32985__$1 = (state_33005[(2)]);
var inst_32986 = (inst_32985__$1 == null);
var inst_32987 = cljs.core.not(inst_32986);
var state_33005__$1 = (function (){var statearr_33010 = state_33005;
(statearr_33010[(8)] = inst_32985__$1);

return statearr_33010;
})();
if(inst_32987){
var statearr_33011_34279 = state_33005__$1;
(statearr_33011_34279[(1)] = (5));

} else {
var statearr_33012_34280 = state_33005__$1;
(statearr_33012_34280[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33006 === (6))){
var state_33005__$1 = state_33005;
var statearr_33013_34281 = state_33005__$1;
(statearr_33013_34281[(2)] = null);

(statearr_33013_34281[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33006 === (3))){
var inst_33002 = (state_33005[(2)]);
var inst_33003 = cljs.core.async.close_BANG_(out);
var state_33005__$1 = (function (){var statearr_33014 = state_33005;
(statearr_33014[(9)] = inst_33002);

return statearr_33014;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_33005__$1,inst_33003);
} else {
if((state_val_33006 === (2))){
var state_33005__$1 = state_33005;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33005__$1,(4),ch);
} else {
if((state_val_33006 === (11))){
var inst_32985 = (state_33005[(8)]);
var inst_32994 = (state_33005[(2)]);
var inst_32982 = inst_32985;
var state_33005__$1 = (function (){var statearr_33015 = state_33005;
(statearr_33015[(7)] = inst_32982);

(statearr_33015[(10)] = inst_32994);

return statearr_33015;
})();
var statearr_33016_34283 = state_33005__$1;
(statearr_33016_34283[(2)] = null);

(statearr_33016_34283[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33006 === (9))){
var inst_32985 = (state_33005[(8)]);
var state_33005__$1 = state_33005;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33005__$1,(11),out,inst_32985);
} else {
if((state_val_33006 === (5))){
var inst_32982 = (state_33005[(7)]);
var inst_32985 = (state_33005[(8)]);
var inst_32989 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_32985,inst_32982);
var state_33005__$1 = state_33005;
if(inst_32989){
var statearr_33018_34286 = state_33005__$1;
(statearr_33018_34286[(1)] = (8));

} else {
var statearr_33019_34287 = state_33005__$1;
(statearr_33019_34287[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33006 === (10))){
var inst_32997 = (state_33005[(2)]);
var state_33005__$1 = state_33005;
var statearr_33020_34289 = state_33005__$1;
(statearr_33020_34289[(2)] = inst_32997);

(statearr_33020_34289[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33006 === (8))){
var inst_32982 = (state_33005[(7)]);
var tmp33017 = inst_32982;
var inst_32982__$1 = tmp33017;
var state_33005__$1 = (function (){var statearr_33021 = state_33005;
(statearr_33021[(7)] = inst_32982__$1);

return statearr_33021;
})();
var statearr_33022_34291 = state_33005__$1;
(statearr_33022_34291[(2)] = null);

(statearr_33022_34291[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto___34264,out))
;
return ((function (switch__30294__auto__,c__30611__auto___34264,out){
return (function() {
var cljs$core$async$state_machine__30295__auto__ = null;
var cljs$core$async$state_machine__30295__auto____0 = (function (){
var statearr_33023 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_33023[(0)] = cljs$core$async$state_machine__30295__auto__);

(statearr_33023[(1)] = (1));

return statearr_33023;
});
var cljs$core$async$state_machine__30295__auto____1 = (function (state_33005){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_33005);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e33024){if((e33024 instanceof Object)){
var ex__30298__auto__ = e33024;
var statearr_33025_34292 = state_33005;
(statearr_33025_34292[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33005);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33024;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34293 = state_33005;
state_33005 = G__34293;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$state_machine__30295__auto__ = function(state_33005){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30295__auto____1.call(this,state_33005);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30295__auto____0;
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30295__auto____1;
return cljs$core$async$state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto___34264,out))
})();
var state__30613__auto__ = (function (){var statearr_33026 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_33026[(6)] = c__30611__auto___34264);

return statearr_33026;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto___34264,out))
);


return out;
});

cljs.core.async.unique.cljs$lang$maxFixedArity = 2;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__33028 = arguments.length;
switch (G__33028) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3(n,ch,null);
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30611__auto___34302 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto___34302,out){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto___34302,out){
return (function (state_33066){
var state_val_33067 = (state_33066[(1)]);
if((state_val_33067 === (7))){
var inst_33062 = (state_33066[(2)]);
var state_33066__$1 = state_33066;
var statearr_33068_34303 = state_33066__$1;
(statearr_33068_34303[(2)] = inst_33062);

(statearr_33068_34303[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33067 === (1))){
var inst_33029 = (new Array(n));
var inst_33030 = inst_33029;
var inst_33031 = (0);
var state_33066__$1 = (function (){var statearr_33069 = state_33066;
(statearr_33069[(7)] = inst_33031);

(statearr_33069[(8)] = inst_33030);

return statearr_33069;
})();
var statearr_33070_34304 = state_33066__$1;
(statearr_33070_34304[(2)] = null);

(statearr_33070_34304[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33067 === (4))){
var inst_33034 = (state_33066[(9)]);
var inst_33034__$1 = (state_33066[(2)]);
var inst_33035 = (inst_33034__$1 == null);
var inst_33036 = cljs.core.not(inst_33035);
var state_33066__$1 = (function (){var statearr_33071 = state_33066;
(statearr_33071[(9)] = inst_33034__$1);

return statearr_33071;
})();
if(inst_33036){
var statearr_33072_34308 = state_33066__$1;
(statearr_33072_34308[(1)] = (5));

} else {
var statearr_33073_34309 = state_33066__$1;
(statearr_33073_34309[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33067 === (15))){
var inst_33056 = (state_33066[(2)]);
var state_33066__$1 = state_33066;
var statearr_33074_34310 = state_33066__$1;
(statearr_33074_34310[(2)] = inst_33056);

(statearr_33074_34310[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33067 === (13))){
var state_33066__$1 = state_33066;
var statearr_33075_34311 = state_33066__$1;
(statearr_33075_34311[(2)] = null);

(statearr_33075_34311[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33067 === (6))){
var inst_33031 = (state_33066[(7)]);
var inst_33052 = (inst_33031 > (0));
var state_33066__$1 = state_33066;
if(cljs.core.truth_(inst_33052)){
var statearr_33076_34312 = state_33066__$1;
(statearr_33076_34312[(1)] = (12));

} else {
var statearr_33077_34313 = state_33066__$1;
(statearr_33077_34313[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33067 === (3))){
var inst_33064 = (state_33066[(2)]);
var state_33066__$1 = state_33066;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33066__$1,inst_33064);
} else {
if((state_val_33067 === (12))){
var inst_33030 = (state_33066[(8)]);
var inst_33054 = cljs.core.vec(inst_33030);
var state_33066__$1 = state_33066;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33066__$1,(15),out,inst_33054);
} else {
if((state_val_33067 === (2))){
var state_33066__$1 = state_33066;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33066__$1,(4),ch);
} else {
if((state_val_33067 === (11))){
var inst_33046 = (state_33066[(2)]);
var inst_33047 = (new Array(n));
var inst_33030 = inst_33047;
var inst_33031 = (0);
var state_33066__$1 = (function (){var statearr_33078 = state_33066;
(statearr_33078[(10)] = inst_33046);

(statearr_33078[(7)] = inst_33031);

(statearr_33078[(8)] = inst_33030);

return statearr_33078;
})();
var statearr_33079_34317 = state_33066__$1;
(statearr_33079_34317[(2)] = null);

(statearr_33079_34317[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33067 === (9))){
var inst_33030 = (state_33066[(8)]);
var inst_33044 = cljs.core.vec(inst_33030);
var state_33066__$1 = state_33066;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33066__$1,(11),out,inst_33044);
} else {
if((state_val_33067 === (5))){
var inst_33031 = (state_33066[(7)]);
var inst_33039 = (state_33066[(11)]);
var inst_33034 = (state_33066[(9)]);
var inst_33030 = (state_33066[(8)]);
var inst_33038 = (inst_33030[inst_33031] = inst_33034);
var inst_33039__$1 = (inst_33031 + (1));
var inst_33040 = (inst_33039__$1 < n);
var state_33066__$1 = (function (){var statearr_33080 = state_33066;
(statearr_33080[(11)] = inst_33039__$1);

(statearr_33080[(12)] = inst_33038);

return statearr_33080;
})();
if(cljs.core.truth_(inst_33040)){
var statearr_33081_34322 = state_33066__$1;
(statearr_33081_34322[(1)] = (8));

} else {
var statearr_33082_34323 = state_33066__$1;
(statearr_33082_34323[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33067 === (14))){
var inst_33059 = (state_33066[(2)]);
var inst_33060 = cljs.core.async.close_BANG_(out);
var state_33066__$1 = (function (){var statearr_33084 = state_33066;
(statearr_33084[(13)] = inst_33059);

return statearr_33084;
})();
var statearr_33085_34324 = state_33066__$1;
(statearr_33085_34324[(2)] = inst_33060);

(statearr_33085_34324[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33067 === (10))){
var inst_33050 = (state_33066[(2)]);
var state_33066__$1 = state_33066;
var statearr_33086_34325 = state_33066__$1;
(statearr_33086_34325[(2)] = inst_33050);

(statearr_33086_34325[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33067 === (8))){
var inst_33039 = (state_33066[(11)]);
var inst_33030 = (state_33066[(8)]);
var tmp33083 = inst_33030;
var inst_33030__$1 = tmp33083;
var inst_33031 = inst_33039;
var state_33066__$1 = (function (){var statearr_33087 = state_33066;
(statearr_33087[(7)] = inst_33031);

(statearr_33087[(8)] = inst_33030__$1);

return statearr_33087;
})();
var statearr_33088_34326 = state_33066__$1;
(statearr_33088_34326[(2)] = null);

(statearr_33088_34326[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto___34302,out))
;
return ((function (switch__30294__auto__,c__30611__auto___34302,out){
return (function() {
var cljs$core$async$state_machine__30295__auto__ = null;
var cljs$core$async$state_machine__30295__auto____0 = (function (){
var statearr_33089 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33089[(0)] = cljs$core$async$state_machine__30295__auto__);

(statearr_33089[(1)] = (1));

return statearr_33089;
});
var cljs$core$async$state_machine__30295__auto____1 = (function (state_33066){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_33066);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e33090){if((e33090 instanceof Object)){
var ex__30298__auto__ = e33090;
var statearr_33091_34331 = state_33066;
(statearr_33091_34331[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33066);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33090;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34332 = state_33066;
state_33066 = G__34332;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$state_machine__30295__auto__ = function(state_33066){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30295__auto____1.call(this,state_33066);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30295__auto____0;
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30295__auto____1;
return cljs$core$async$state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto___34302,out))
})();
var state__30613__auto__ = (function (){var statearr_33092 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_33092[(6)] = c__30611__auto___34302);

return statearr_33092;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto___34302,out))
);


return out;
});

cljs.core.async.partition.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__33094 = arguments.length;
switch (G__33094) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3(f,ch,null);
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30611__auto___34334 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto___34334,out){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto___34334,out){
return (function (state_33136){
var state_val_33137 = (state_33136[(1)]);
if((state_val_33137 === (7))){
var inst_33132 = (state_33136[(2)]);
var state_33136__$1 = state_33136;
var statearr_33138_34335 = state_33136__$1;
(statearr_33138_34335[(2)] = inst_33132);

(statearr_33138_34335[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33137 === (1))){
var inst_33095 = [];
var inst_33096 = inst_33095;
var inst_33097 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_33136__$1 = (function (){var statearr_33139 = state_33136;
(statearr_33139[(7)] = inst_33096);

(statearr_33139[(8)] = inst_33097);

return statearr_33139;
})();
var statearr_33140_34336 = state_33136__$1;
(statearr_33140_34336[(2)] = null);

(statearr_33140_34336[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33137 === (4))){
var inst_33100 = (state_33136[(9)]);
var inst_33100__$1 = (state_33136[(2)]);
var inst_33101 = (inst_33100__$1 == null);
var inst_33102 = cljs.core.not(inst_33101);
var state_33136__$1 = (function (){var statearr_33141 = state_33136;
(statearr_33141[(9)] = inst_33100__$1);

return statearr_33141;
})();
if(inst_33102){
var statearr_33142_34337 = state_33136__$1;
(statearr_33142_34337[(1)] = (5));

} else {
var statearr_33143_34338 = state_33136__$1;
(statearr_33143_34338[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33137 === (15))){
var inst_33126 = (state_33136[(2)]);
var state_33136__$1 = state_33136;
var statearr_33144_34339 = state_33136__$1;
(statearr_33144_34339[(2)] = inst_33126);

(statearr_33144_34339[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33137 === (13))){
var state_33136__$1 = state_33136;
var statearr_33145_34340 = state_33136__$1;
(statearr_33145_34340[(2)] = null);

(statearr_33145_34340[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33137 === (6))){
var inst_33096 = (state_33136[(7)]);
var inst_33121 = inst_33096.length;
var inst_33122 = (inst_33121 > (0));
var state_33136__$1 = state_33136;
if(cljs.core.truth_(inst_33122)){
var statearr_33146_34342 = state_33136__$1;
(statearr_33146_34342[(1)] = (12));

} else {
var statearr_33147_34344 = state_33136__$1;
(statearr_33147_34344[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33137 === (3))){
var inst_33134 = (state_33136[(2)]);
var state_33136__$1 = state_33136;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33136__$1,inst_33134);
} else {
if((state_val_33137 === (12))){
var inst_33096 = (state_33136[(7)]);
var inst_33124 = cljs.core.vec(inst_33096);
var state_33136__$1 = state_33136;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33136__$1,(15),out,inst_33124);
} else {
if((state_val_33137 === (2))){
var state_33136__$1 = state_33136;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33136__$1,(4),ch);
} else {
if((state_val_33137 === (11))){
var inst_33104 = (state_33136[(10)]);
var inst_33100 = (state_33136[(9)]);
var inst_33114 = (state_33136[(2)]);
var inst_33115 = [];
var inst_33116 = inst_33115.push(inst_33100);
var inst_33096 = inst_33115;
var inst_33097 = inst_33104;
var state_33136__$1 = (function (){var statearr_33148 = state_33136;
(statearr_33148[(11)] = inst_33114);

(statearr_33148[(12)] = inst_33116);

(statearr_33148[(7)] = inst_33096);

(statearr_33148[(8)] = inst_33097);

return statearr_33148;
})();
var statearr_33149_34347 = state_33136__$1;
(statearr_33149_34347[(2)] = null);

(statearr_33149_34347[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33137 === (9))){
var inst_33096 = (state_33136[(7)]);
var inst_33112 = cljs.core.vec(inst_33096);
var state_33136__$1 = state_33136;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33136__$1,(11),out,inst_33112);
} else {
if((state_val_33137 === (5))){
var inst_33104 = (state_33136[(10)]);
var inst_33100 = (state_33136[(9)]);
var inst_33097 = (state_33136[(8)]);
var inst_33104__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_33100) : f.call(null,inst_33100));
var inst_33105 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_33104__$1,inst_33097);
var inst_33106 = cljs.core.keyword_identical_QMARK_(inst_33097,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_33107 = ((inst_33105) || (inst_33106));
var state_33136__$1 = (function (){var statearr_33150 = state_33136;
(statearr_33150[(10)] = inst_33104__$1);

return statearr_33150;
})();
if(cljs.core.truth_(inst_33107)){
var statearr_33151_34352 = state_33136__$1;
(statearr_33151_34352[(1)] = (8));

} else {
var statearr_33152_34353 = state_33136__$1;
(statearr_33152_34353[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33137 === (14))){
var inst_33129 = (state_33136[(2)]);
var inst_33130 = cljs.core.async.close_BANG_(out);
var state_33136__$1 = (function (){var statearr_33154 = state_33136;
(statearr_33154[(13)] = inst_33129);

return statearr_33154;
})();
var statearr_33155_34356 = state_33136__$1;
(statearr_33155_34356[(2)] = inst_33130);

(statearr_33155_34356[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33137 === (10))){
var inst_33119 = (state_33136[(2)]);
var state_33136__$1 = state_33136;
var statearr_33156_34357 = state_33136__$1;
(statearr_33156_34357[(2)] = inst_33119);

(statearr_33156_34357[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33137 === (8))){
var inst_33104 = (state_33136[(10)]);
var inst_33100 = (state_33136[(9)]);
var inst_33096 = (state_33136[(7)]);
var inst_33109 = inst_33096.push(inst_33100);
var tmp33153 = inst_33096;
var inst_33096__$1 = tmp33153;
var inst_33097 = inst_33104;
var state_33136__$1 = (function (){var statearr_33157 = state_33136;
(statearr_33157[(14)] = inst_33109);

(statearr_33157[(7)] = inst_33096__$1);

(statearr_33157[(8)] = inst_33097);

return statearr_33157;
})();
var statearr_33158_34361 = state_33136__$1;
(statearr_33158_34361[(2)] = null);

(statearr_33158_34361[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__30611__auto___34334,out))
;
return ((function (switch__30294__auto__,c__30611__auto___34334,out){
return (function() {
var cljs$core$async$state_machine__30295__auto__ = null;
var cljs$core$async$state_machine__30295__auto____0 = (function (){
var statearr_33159 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33159[(0)] = cljs$core$async$state_machine__30295__auto__);

(statearr_33159[(1)] = (1));

return statearr_33159;
});
var cljs$core$async$state_machine__30295__auto____1 = (function (state_33136){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_33136);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e33160){if((e33160 instanceof Object)){
var ex__30298__auto__ = e33160;
var statearr_33161_34363 = state_33136;
(statearr_33161_34363[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33136);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33160;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34364 = state_33136;
state_33136 = G__34364;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
cljs$core$async$state_machine__30295__auto__ = function(state_33136){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30295__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30295__auto____1.call(this,state_33136);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30295__auto____0;
cljs$core$async$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30295__auto____1;
return cljs$core$async$state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto___34334,out))
})();
var state__30613__auto__ = (function (){var statearr_33162 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_33162[(6)] = c__30611__auto___34334);

return statearr_33162;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto___34334,out))
);


return out;
});

cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3;


//# sourceMappingURL=cljs.core.async.js.map
