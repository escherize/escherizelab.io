goog.provide('shadow.cljs.devtools.client.browser');
goog.require('cljs.core');
goog.require('cljs.reader');
goog.require('clojure.string');
goog.require('goog.dom');
goog.require('goog.userAgent.product');
goog.require('goog.Uri');
goog.require('goog.net.XhrIo');
goog.require('shadow.cljs.devtools.client.env');
goog.require('shadow.cljs.devtools.client.console');
goog.require('shadow.cljs.devtools.client.hud');
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.active_modules_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.active_modules_ref = cljs.core.volatile_BANG_(cljs.core.PersistentHashSet.EMPTY);
}
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.repl_ns_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.repl_ns_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
shadow.cljs.devtools.client.browser.module_loaded = (function shadow$cljs$devtools$client$browser$module_loaded(name){
return shadow.cljs.devtools.client.browser.active_modules_ref.cljs$core$IVolatile$_vreset_BANG_$arity$2(null,cljs.core.conj.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.browser.active_modules_ref.cljs$core$IDeref$_deref$arity$1(null),cljs.core.keyword.cljs$core$IFn$_invoke$arity$1(name)));
});
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.socket_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.socket_ref = cljs.core.volatile_BANG_(null);
}
shadow.cljs.devtools.client.browser.devtools_msg = (function shadow$cljs$devtools$client$browser$devtools_msg(var_args){
var args__4736__auto__ = [];
var len__4730__auto___35444 = arguments.length;
var i__4731__auto___35445 = (0);
while(true){
if((i__4731__auto___35445 < len__4730__auto___35444)){
args__4736__auto__.push((arguments[i__4731__auto___35445]));

var G__35446 = (i__4731__auto___35445 + (1));
i__4731__auto___35445 = G__35446;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic = (function (msg,args){
return console.log.apply(null,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [["%cshadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join(''),"color: blue;"], null),args)));
});

shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$applyTo = (function (seq35277){
var G__35278 = cljs.core.first(seq35277);
var seq35277__$1 = cljs.core.next(seq35277);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__35278,seq35277__$1);
});

shadow.cljs.devtools.client.browser.ws_msg = (function shadow$cljs$devtools$client$browser$ws_msg(msg){
var temp__5718__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5718__auto__)){
var s = temp__5718__auto__;
return s.send(cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0)));
} else {
return console.warn("WEBSOCKET NOT CONNECTED",cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0)));
}
});
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.scripts_to_load !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.scripts_to_load = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentVector.EMPTY);
}
shadow.cljs.devtools.client.browser.loaded_QMARK_ = goog.isProvided_;
shadow.cljs.devtools.client.browser.goog_is_loaded_QMARK_ = (function shadow$cljs$devtools$client$browser$goog_is_loaded_QMARK_(name){
return $CLJS.SHADOW_ENV.isLoaded(name);
});
shadow.cljs.devtools.client.browser.goog_base_rc = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("shadow.build.classpath","resource","shadow.build.classpath/resource",-879517823),"goog/base.js"], null);
shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_ = (function shadow$cljs$devtools$client$browser$src_is_loaded_QMARK_(p__35293){
var map__35294 = p__35293;
var map__35294__$1 = (((((!((map__35294 == null))))?(((((map__35294.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35294.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35294):map__35294);
var src = map__35294__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35294__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35294__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var or__4131__auto__ = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.browser.goog_base_rc,resource_id);
if(or__4131__auto__){
return or__4131__auto__;
} else {
return shadow.cljs.devtools.client.browser.goog_is_loaded_QMARK_(output_name);
}
});
shadow.cljs.devtools.client.browser.module_is_active_QMARK_ = (function shadow$cljs$devtools$client$browser$module_is_active_QMARK_(module){
return cljs.core.contains_QMARK_(cljs.core.deref(shadow.cljs.devtools.client.browser.active_modules_ref),module);
});
shadow.cljs.devtools.client.browser.script_eval = (function shadow$cljs$devtools$client$browser$script_eval(code){
return goog.globalEval(code);
});
shadow.cljs.devtools.client.browser.do_js_load = (function shadow$cljs$devtools$client$browser$do_js_load(sources){
var seq__35299 = cljs.core.seq(sources);
var chunk__35300 = null;
var count__35301 = (0);
var i__35302 = (0);
while(true){
if((i__35302 < count__35301)){
var map__35307 = chunk__35300.cljs$core$IIndexed$_nth$arity$2(null,i__35302);
var map__35307__$1 = (((((!((map__35307 == null))))?(((((map__35307.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35307.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35307):map__35307);
var src = map__35307__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35307__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35307__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35307__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35307__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''));


var G__35447 = seq__35299;
var G__35448 = chunk__35300;
var G__35449 = count__35301;
var G__35450 = (i__35302 + (1));
seq__35299 = G__35447;
chunk__35300 = G__35448;
count__35301 = G__35449;
i__35302 = G__35450;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq(seq__35299);
if(temp__5720__auto__){
var seq__35299__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__35299__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__35299__$1);
var G__35451 = cljs.core.chunk_rest(seq__35299__$1);
var G__35452 = c__4550__auto__;
var G__35453 = cljs.core.count(c__4550__auto__);
var G__35454 = (0);
seq__35299 = G__35451;
chunk__35300 = G__35452;
count__35301 = G__35453;
i__35302 = G__35454;
continue;
} else {
var map__35309 = cljs.core.first(seq__35299__$1);
var map__35309__$1 = (((((!((map__35309 == null))))?(((((map__35309.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35309.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35309):map__35309);
var src = map__35309__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35309__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35309__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35309__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35309__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''));


var G__35455 = cljs.core.next(seq__35299__$1);
var G__35456 = null;
var G__35457 = (0);
var G__35458 = (0);
seq__35299 = G__35455;
chunk__35300 = G__35456;
count__35301 = G__35457;
i__35302 = G__35458;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.do_js_reload = (function shadow$cljs$devtools$client$browser$do_js_reload(msg,sources,complete_fn,failure_fn){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(msg,new cljs.core.Keyword(null,"log-missing-fn","log-missing-fn",732676765),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["can't find fn ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"log-call-async","log-call-async",183826192),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call async ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),new cljs.core.Keyword(null,"log-call","log-call",412404391),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
})], 0)),(function (){
return shadow.cljs.devtools.client.browser.do_js_load(sources);
}),complete_fn,failure_fn);
});
/**
 * when (require '["some-str" :as x]) is done at the REPL we need to manually call the shadow.js.require for it
 * since the file only adds the shadow$provide. only need to do this for shadow-js.
 */
shadow.cljs.devtools.client.browser.do_js_requires = (function shadow$cljs$devtools$client$browser$do_js_requires(js_requires){
var seq__35313 = cljs.core.seq(js_requires);
var chunk__35314 = null;
var count__35315 = (0);
var i__35316 = (0);
while(true){
if((i__35316 < count__35315)){
var js_ns = chunk__35314.cljs$core$IIndexed$_nth$arity$2(null,i__35316);
var require_str_35459 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_35459);


var G__35460 = seq__35313;
var G__35461 = chunk__35314;
var G__35462 = count__35315;
var G__35463 = (i__35316 + (1));
seq__35313 = G__35460;
chunk__35314 = G__35461;
count__35315 = G__35462;
i__35316 = G__35463;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq(seq__35313);
if(temp__5720__auto__){
var seq__35313__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__35313__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__35313__$1);
var G__35464 = cljs.core.chunk_rest(seq__35313__$1);
var G__35465 = c__4550__auto__;
var G__35466 = cljs.core.count(c__4550__auto__);
var G__35467 = (0);
seq__35313 = G__35464;
chunk__35314 = G__35465;
count__35315 = G__35466;
i__35316 = G__35467;
continue;
} else {
var js_ns = cljs.core.first(seq__35313__$1);
var require_str_35468 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_35468);


var G__35469 = cljs.core.next(seq__35313__$1);
var G__35470 = null;
var G__35471 = (0);
var G__35472 = (0);
seq__35313 = G__35469;
chunk__35314 = G__35470;
count__35315 = G__35471;
i__35316 = G__35472;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.load_sources = (function shadow$cljs$devtools$client$browser$load_sources(sources,callback){
if(cljs.core.empty_QMARK_(sources)){
var G__35319 = cljs.core.PersistentVector.EMPTY;
return (callback.cljs$core$IFn$_invoke$arity$1 ? callback.cljs$core$IFn$_invoke$arity$1(G__35319) : callback.call(null,G__35319));
} else {
var G__35320 = shadow.cljs.devtools.client.env.files_url();
var G__35321 = ((function (G__35320){
return (function (res){
var req = this;
var content = cljs.reader.read_string.cljs$core$IFn$_invoke$arity$1(req.getResponseText());
return (callback.cljs$core$IFn$_invoke$arity$1 ? callback.cljs$core$IFn$_invoke$arity$1(content) : callback.call(null,content));
});})(G__35320))
;
var G__35322 = "POST";
var G__35323 = cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"client","client",-1323448117),new cljs.core.Keyword(null,"browser","browser",828191719),new cljs.core.Keyword(null,"sources","sources",-321166424),cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582)),sources)], null)], 0));
var G__35324 = ({"content-type": "application/edn; charset=utf-8"});
return goog.net.XhrIo.send(G__35320,G__35321,G__35322,G__35323,G__35324);
}
});
shadow.cljs.devtools.client.browser.handle_build_complete = (function shadow$cljs$devtools$client$browser$handle_build_complete(p__35326){
var map__35327 = p__35326;
var map__35327__$1 = (((((!((map__35327 == null))))?(((((map__35327.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35327.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35327):map__35327);
var msg = map__35327__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35327__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35327__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var map__35329 = info;
var map__35329__$1 = (((((!((map__35329 == null))))?(((((map__35329.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35329.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35329):map__35329);
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35329__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var compiled = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35329__$1,new cljs.core.Keyword(null,"compiled","compiled",850043082));
var warnings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.distinct.cljs$core$IFn$_invoke$arity$1((function (){var iter__4523__auto__ = ((function (map__35329,map__35329__$1,sources,compiled,map__35327,map__35327__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__35331(s__35332){
return (new cljs.core.LazySeq(null,((function (map__35329,map__35329__$1,sources,compiled,map__35327,map__35327__$1,msg,info,reload_info){
return (function (){
var s__35332__$1 = s__35332;
while(true){
var temp__5720__auto__ = cljs.core.seq(s__35332__$1);
if(temp__5720__auto__){
var xs__6277__auto__ = temp__5720__auto__;
var map__35337 = cljs.core.first(xs__6277__auto__);
var map__35337__$1 = (((((!((map__35337 == null))))?(((((map__35337.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35337.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35337):map__35337);
var src = map__35337__$1;
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35337__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35337__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))){
var iterys__4519__auto__ = ((function (s__35332__$1,map__35337,map__35337__$1,src,resource_name,warnings,xs__6277__auto__,temp__5720__auto__,map__35329,map__35329__$1,sources,compiled,map__35327,map__35327__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__35331_$_iter__35333(s__35334){
return (new cljs.core.LazySeq(null,((function (s__35332__$1,map__35337,map__35337__$1,src,resource_name,warnings,xs__6277__auto__,temp__5720__auto__,map__35329,map__35329__$1,sources,compiled,map__35327,map__35327__$1,msg,info,reload_info){
return (function (){
var s__35334__$1 = s__35334;
while(true){
var temp__5720__auto____$1 = cljs.core.seq(s__35334__$1);
if(temp__5720__auto____$1){
var s__35334__$2 = temp__5720__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__35334__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__35334__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__35336 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__35335 = (0);
while(true){
if((i__35335 < size__4522__auto__)){
var warning = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__35335);
cljs.core.chunk_append(b__35336,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name));

var G__35473 = (i__35335 + (1));
i__35335 = G__35473;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__35336),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__35331_$_iter__35333(cljs.core.chunk_rest(s__35334__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__35336),null);
}
} else {
var warning = cljs.core.first(s__35334__$2);
return cljs.core.cons(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__35331_$_iter__35333(cljs.core.rest(s__35334__$2)));
}
} else {
return null;
}
break;
}
});})(s__35332__$1,map__35337,map__35337__$1,src,resource_name,warnings,xs__6277__auto__,temp__5720__auto__,map__35329,map__35329__$1,sources,compiled,map__35327,map__35327__$1,msg,info,reload_info))
,null,null));
});})(s__35332__$1,map__35337,map__35337__$1,src,resource_name,warnings,xs__6277__auto__,temp__5720__auto__,map__35329,map__35329__$1,sources,compiled,map__35327,map__35327__$1,msg,info,reload_info))
;
var fs__4520__auto__ = cljs.core.seq(iterys__4519__auto__(warnings));
if(fs__4520__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4520__auto__,shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__35331(cljs.core.rest(s__35332__$1)));
} else {
var G__35474 = cljs.core.rest(s__35332__$1);
s__35332__$1 = G__35474;
continue;
}
} else {
var G__35475 = cljs.core.rest(s__35332__$1);
s__35332__$1 = G__35475;
continue;
}
} else {
return null;
}
break;
}
});})(map__35329,map__35329__$1,sources,compiled,map__35327,map__35327__$1,msg,info,reload_info))
,null,null));
});})(map__35329,map__35329__$1,sources,compiled,map__35327,map__35327__$1,msg,info,reload_info))
;
return iter__4523__auto__(sources);
})()));
var seq__35339_35476 = cljs.core.seq(warnings);
var chunk__35340_35477 = null;
var count__35341_35478 = (0);
var i__35342_35479 = (0);
while(true){
if((i__35342_35479 < count__35341_35478)){
var map__35347_35480 = chunk__35340_35477.cljs$core$IIndexed$_nth$arity$2(null,i__35342_35479);
var map__35347_35481__$1 = (((((!((map__35347_35480 == null))))?(((((map__35347_35480.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35347_35480.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35347_35480):map__35347_35480);
var w_35482 = map__35347_35481__$1;
var msg_35483__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35347_35481__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_35484 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35347_35481__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_35485 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35347_35481__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_35486 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35347_35481__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_35486)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_35484),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_35485),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_35483__$1)].join(''));


var G__35487 = seq__35339_35476;
var G__35488 = chunk__35340_35477;
var G__35489 = count__35341_35478;
var G__35490 = (i__35342_35479 + (1));
seq__35339_35476 = G__35487;
chunk__35340_35477 = G__35488;
count__35341_35478 = G__35489;
i__35342_35479 = G__35490;
continue;
} else {
var temp__5720__auto___35491 = cljs.core.seq(seq__35339_35476);
if(temp__5720__auto___35491){
var seq__35339_35492__$1 = temp__5720__auto___35491;
if(cljs.core.chunked_seq_QMARK_(seq__35339_35492__$1)){
var c__4550__auto___35493 = cljs.core.chunk_first(seq__35339_35492__$1);
var G__35494 = cljs.core.chunk_rest(seq__35339_35492__$1);
var G__35495 = c__4550__auto___35493;
var G__35496 = cljs.core.count(c__4550__auto___35493);
var G__35497 = (0);
seq__35339_35476 = G__35494;
chunk__35340_35477 = G__35495;
count__35341_35478 = G__35496;
i__35342_35479 = G__35497;
continue;
} else {
var map__35349_35498 = cljs.core.first(seq__35339_35492__$1);
var map__35349_35499__$1 = (((((!((map__35349_35498 == null))))?(((((map__35349_35498.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35349_35498.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35349_35498):map__35349_35498);
var w_35500 = map__35349_35499__$1;
var msg_35501__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35349_35499__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_35502 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35349_35499__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_35503 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35349_35499__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_35504 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35349_35499__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_35504)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_35502),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_35503),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_35501__$1)].join(''));


var G__35505 = cljs.core.next(seq__35339_35492__$1);
var G__35506 = null;
var G__35507 = (0);
var G__35508 = (0);
seq__35339_35476 = G__35505;
chunk__35340_35477 = G__35506;
count__35341_35478 = G__35507;
i__35342_35479 = G__35508;
continue;
}
} else {
}
}
break;
}

if((!(shadow.cljs.devtools.client.env.autoload))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(((cljs.core.empty_QMARK_(warnings)) || (shadow.cljs.devtools.client.env.ignore_warnings))){
var sources_to_get = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.filter.cljs$core$IFn$_invoke$arity$2(((function (map__35329,map__35329__$1,sources,compiled,warnings,map__35327,map__35327__$1,msg,info,reload_info){
return (function (p__35353){
var map__35354 = p__35353;
var map__35354__$1 = (((((!((map__35354 == null))))?(((((map__35354.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35354.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35354):map__35354);
var src = map__35354__$1;
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35354__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35354__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
return ((cljs.core.contains_QMARK_(new cljs.core.Keyword(null,"always-load","always-load",66405637).cljs$core$IFn$_invoke$arity$1(reload_info),ns)) || (cljs.core.not(shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_(src))) || (((cljs.core.contains_QMARK_(compiled,resource_id)) && (cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))))));
});})(map__35329,map__35329__$1,sources,compiled,warnings,map__35327,map__35327__$1,msg,info,reload_info))
,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(((function (map__35329,map__35329__$1,sources,compiled,warnings,map__35327,map__35327__$1,msg,info,reload_info){
return (function (p__35356){
var map__35357 = p__35356;
var map__35357__$1 = (((((!((map__35357 == null))))?(((((map__35357.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35357.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35357):map__35357);
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35357__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
return cljs.core.contains_QMARK_(new cljs.core.Keyword(null,"never-load","never-load",1300896819).cljs$core$IFn$_invoke$arity$1(reload_info),ns);
});})(map__35329,map__35329__$1,sources,compiled,warnings,map__35327,map__35327__$1,msg,info,reload_info))
,cljs.core.filter.cljs$core$IFn$_invoke$arity$2(((function (map__35329,map__35329__$1,sources,compiled,warnings,map__35327,map__35327__$1,msg,info,reload_info){
return (function (p__35359){
var map__35360 = p__35359;
var map__35360__$1 = (((((!((map__35360 == null))))?(((((map__35360.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35360.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35360):map__35360);
var rc = map__35360__$1;
var module = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35360__$1,new cljs.core.Keyword(null,"module","module",1424618191));
return ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("js",shadow.cljs.devtools.client.env.module_format)) || (shadow.cljs.devtools.client.browser.module_is_active_QMARK_(module)));
});})(map__35329,map__35329__$1,sources,compiled,warnings,map__35327,map__35327__$1,msg,info,reload_info))
,sources))));
if(cljs.core.not(cljs.core.seq(sources_to_get))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"after-load","after-load",-1278503285)], null)))){
} else {
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("reloading code but no :after-load hooks are configured!",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["https://shadow-cljs.github.io/docs/UsersGuide.html#_lifecycle_hooks"], 0));
}

return shadow.cljs.devtools.client.browser.load_sources(sources_to_get,((function (sources_to_get,map__35329,map__35329__$1,sources,compiled,warnings,map__35327,map__35327__$1,msg,info,reload_info){
return (function (p1__35325_SHARP_){
return shadow.cljs.devtools.client.browser.do_js_reload(msg,p1__35325_SHARP_,shadow.cljs.devtools.client.hud.load_end_success,shadow.cljs.devtools.client.hud.load_failure);
});})(sources_to_get,map__35329,map__35329__$1,sources,compiled,warnings,map__35327,map__35327__$1,msg,info,reload_info))
);
}
} else {
return null;
}
}
});
shadow.cljs.devtools.client.browser.page_load_uri = (cljs.core.truth_(goog.global.document)?goog.Uri.parse(document.location.href):null);
shadow.cljs.devtools.client.browser.match_paths = (function shadow$cljs$devtools$client$browser$match_paths(old,new$){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("file",shadow.cljs.devtools.client.browser.page_load_uri.getScheme())){
var rel_new = cljs.core.subs.cljs$core$IFn$_invoke$arity$2(new$,(1));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(old,rel_new)) || (clojure.string.starts_with_QMARK_(old,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(rel_new),"?"].join(''))))){
return rel_new;
} else {
return null;
}
} else {
var node_uri = goog.Uri.parse(old);
var node_uri_resolved = shadow.cljs.devtools.client.browser.page_load_uri.resolve(node_uri);
var node_abs = node_uri_resolved.getPath();
var and__4120__auto__ = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.browser.page_load_uri.hasSameDomainAs(node_uri))) || (cljs.core.not(node_uri.hasDomain())));
if(and__4120__auto__){
var and__4120__auto____$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(node_abs,new$);
if(and__4120__auto____$1){
return new$;
} else {
return and__4120__auto____$1;
}
} else {
return and__4120__auto__;
}
}
});
shadow.cljs.devtools.client.browser.handle_asset_watch = (function shadow$cljs$devtools$client$browser$handle_asset_watch(p__35362){
var map__35363 = p__35362;
var map__35363__$1 = (((((!((map__35363 == null))))?(((((map__35363.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35363.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35363):map__35363);
var msg = map__35363__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35363__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
var seq__35365 = cljs.core.seq(updates);
var chunk__35367 = null;
var count__35368 = (0);
var i__35369 = (0);
while(true){
if((i__35369 < count__35368)){
var path = chunk__35367.cljs$core$IIndexed$_nth$arity$2(null,i__35369);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__35395_35509 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__35398_35510 = null;
var count__35399_35511 = (0);
var i__35400_35512 = (0);
while(true){
if((i__35400_35512 < count__35399_35511)){
var node_35513 = chunk__35398_35510.cljs$core$IIndexed$_nth$arity$2(null,i__35400_35512);
var path_match_35514 = shadow.cljs.devtools.client.browser.match_paths(node_35513.getAttribute("href"),path);
if(cljs.core.truth_(path_match_35514)){
var new_link_35515 = (function (){var G__35405 = node_35513.cloneNode(true);
G__35405.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_35514),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__35405;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_35514], 0));

goog.dom.insertSiblingAfter(new_link_35515,node_35513);

goog.dom.removeNode(node_35513);


var G__35516 = seq__35395_35509;
var G__35517 = chunk__35398_35510;
var G__35518 = count__35399_35511;
var G__35519 = (i__35400_35512 + (1));
seq__35395_35509 = G__35516;
chunk__35398_35510 = G__35517;
count__35399_35511 = G__35518;
i__35400_35512 = G__35519;
continue;
} else {
var G__35520 = seq__35395_35509;
var G__35521 = chunk__35398_35510;
var G__35522 = count__35399_35511;
var G__35523 = (i__35400_35512 + (1));
seq__35395_35509 = G__35520;
chunk__35398_35510 = G__35521;
count__35399_35511 = G__35522;
i__35400_35512 = G__35523;
continue;
}
} else {
var temp__5720__auto___35524 = cljs.core.seq(seq__35395_35509);
if(temp__5720__auto___35524){
var seq__35395_35525__$1 = temp__5720__auto___35524;
if(cljs.core.chunked_seq_QMARK_(seq__35395_35525__$1)){
var c__4550__auto___35526 = cljs.core.chunk_first(seq__35395_35525__$1);
var G__35527 = cljs.core.chunk_rest(seq__35395_35525__$1);
var G__35528 = c__4550__auto___35526;
var G__35529 = cljs.core.count(c__4550__auto___35526);
var G__35530 = (0);
seq__35395_35509 = G__35527;
chunk__35398_35510 = G__35528;
count__35399_35511 = G__35529;
i__35400_35512 = G__35530;
continue;
} else {
var node_35531 = cljs.core.first(seq__35395_35525__$1);
var path_match_35532 = shadow.cljs.devtools.client.browser.match_paths(node_35531.getAttribute("href"),path);
if(cljs.core.truth_(path_match_35532)){
var new_link_35533 = (function (){var G__35406 = node_35531.cloneNode(true);
G__35406.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_35532),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__35406;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_35532], 0));

goog.dom.insertSiblingAfter(new_link_35533,node_35531);

goog.dom.removeNode(node_35531);


var G__35534 = cljs.core.next(seq__35395_35525__$1);
var G__35535 = null;
var G__35536 = (0);
var G__35537 = (0);
seq__35395_35509 = G__35534;
chunk__35398_35510 = G__35535;
count__35399_35511 = G__35536;
i__35400_35512 = G__35537;
continue;
} else {
var G__35538 = cljs.core.next(seq__35395_35525__$1);
var G__35539 = null;
var G__35540 = (0);
var G__35541 = (0);
seq__35395_35509 = G__35538;
chunk__35398_35510 = G__35539;
count__35399_35511 = G__35540;
i__35400_35512 = G__35541;
continue;
}
}
} else {
}
}
break;
}


var G__35542 = seq__35365;
var G__35543 = chunk__35367;
var G__35544 = count__35368;
var G__35545 = (i__35369 + (1));
seq__35365 = G__35542;
chunk__35367 = G__35543;
count__35368 = G__35544;
i__35369 = G__35545;
continue;
} else {
var G__35546 = seq__35365;
var G__35547 = chunk__35367;
var G__35548 = count__35368;
var G__35549 = (i__35369 + (1));
seq__35365 = G__35546;
chunk__35367 = G__35547;
count__35368 = G__35548;
i__35369 = G__35549;
continue;
}
} else {
var temp__5720__auto__ = cljs.core.seq(seq__35365);
if(temp__5720__auto__){
var seq__35365__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__35365__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__35365__$1);
var G__35550 = cljs.core.chunk_rest(seq__35365__$1);
var G__35551 = c__4550__auto__;
var G__35552 = cljs.core.count(c__4550__auto__);
var G__35553 = (0);
seq__35365 = G__35550;
chunk__35367 = G__35551;
count__35368 = G__35552;
i__35369 = G__35553;
continue;
} else {
var path = cljs.core.first(seq__35365__$1);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__35407_35554 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__35410_35555 = null;
var count__35411_35556 = (0);
var i__35412_35557 = (0);
while(true){
if((i__35412_35557 < count__35411_35556)){
var node_35558 = chunk__35410_35555.cljs$core$IIndexed$_nth$arity$2(null,i__35412_35557);
var path_match_35559 = shadow.cljs.devtools.client.browser.match_paths(node_35558.getAttribute("href"),path);
if(cljs.core.truth_(path_match_35559)){
var new_link_35560 = (function (){var G__35417 = node_35558.cloneNode(true);
G__35417.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_35559),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__35417;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_35559], 0));

goog.dom.insertSiblingAfter(new_link_35560,node_35558);

goog.dom.removeNode(node_35558);


var G__35561 = seq__35407_35554;
var G__35562 = chunk__35410_35555;
var G__35563 = count__35411_35556;
var G__35564 = (i__35412_35557 + (1));
seq__35407_35554 = G__35561;
chunk__35410_35555 = G__35562;
count__35411_35556 = G__35563;
i__35412_35557 = G__35564;
continue;
} else {
var G__35565 = seq__35407_35554;
var G__35566 = chunk__35410_35555;
var G__35567 = count__35411_35556;
var G__35568 = (i__35412_35557 + (1));
seq__35407_35554 = G__35565;
chunk__35410_35555 = G__35566;
count__35411_35556 = G__35567;
i__35412_35557 = G__35568;
continue;
}
} else {
var temp__5720__auto___35569__$1 = cljs.core.seq(seq__35407_35554);
if(temp__5720__auto___35569__$1){
var seq__35407_35570__$1 = temp__5720__auto___35569__$1;
if(cljs.core.chunked_seq_QMARK_(seq__35407_35570__$1)){
var c__4550__auto___35571 = cljs.core.chunk_first(seq__35407_35570__$1);
var G__35572 = cljs.core.chunk_rest(seq__35407_35570__$1);
var G__35573 = c__4550__auto___35571;
var G__35574 = cljs.core.count(c__4550__auto___35571);
var G__35575 = (0);
seq__35407_35554 = G__35572;
chunk__35410_35555 = G__35573;
count__35411_35556 = G__35574;
i__35412_35557 = G__35575;
continue;
} else {
var node_35576 = cljs.core.first(seq__35407_35570__$1);
var path_match_35577 = shadow.cljs.devtools.client.browser.match_paths(node_35576.getAttribute("href"),path);
if(cljs.core.truth_(path_match_35577)){
var new_link_35578 = (function (){var G__35418 = node_35576.cloneNode(true);
G__35418.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_35577),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__35418;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_35577], 0));

goog.dom.insertSiblingAfter(new_link_35578,node_35576);

goog.dom.removeNode(node_35576);


var G__35579 = cljs.core.next(seq__35407_35570__$1);
var G__35580 = null;
var G__35581 = (0);
var G__35582 = (0);
seq__35407_35554 = G__35579;
chunk__35410_35555 = G__35580;
count__35411_35556 = G__35581;
i__35412_35557 = G__35582;
continue;
} else {
var G__35583 = cljs.core.next(seq__35407_35570__$1);
var G__35584 = null;
var G__35585 = (0);
var G__35586 = (0);
seq__35407_35554 = G__35583;
chunk__35410_35555 = G__35584;
count__35411_35556 = G__35585;
i__35412_35557 = G__35586;
continue;
}
}
} else {
}
}
break;
}


var G__35587 = cljs.core.next(seq__35365__$1);
var G__35588 = null;
var G__35589 = (0);
var G__35590 = (0);
seq__35365 = G__35587;
chunk__35367 = G__35588;
count__35368 = G__35589;
i__35369 = G__35590;
continue;
} else {
var G__35591 = cljs.core.next(seq__35365__$1);
var G__35592 = null;
var G__35593 = (0);
var G__35594 = (0);
seq__35365 = G__35591;
chunk__35367 = G__35592;
count__35368 = G__35593;
i__35369 = G__35594;
continue;
}
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.get_ua_product = (function shadow$cljs$devtools$client$browser$get_ua_product(){
if(cljs.core.truth_(goog.userAgent.product.SAFARI)){
return new cljs.core.Keyword(null,"safari","safari",497115653);
} else {
if(cljs.core.truth_(goog.userAgent.product.CHROME)){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.FIREFOX)){
return new cljs.core.Keyword(null,"firefox","firefox",1283768880);
} else {
if(cljs.core.truth_(goog.userAgent.product.IE)){
return new cljs.core.Keyword(null,"ie","ie",2038473780);
} else {
return null;
}
}
}
}
});
shadow.cljs.devtools.client.browser.get_asset_root = (function shadow$cljs$devtools$client$browser$get_asset_root(){
var loc = (new goog.Uri(document.location.href));
var cbp = (new goog.Uri(CLOSURE_BASE_PATH));
var s = loc.resolve(cbp).toString();
return clojure.string.replace(s,/^file:\//,"file:///");
});
shadow.cljs.devtools.client.browser.repl_error = (function shadow$cljs$devtools$client$browser$repl_error(e){
console.error("repl/invoke error",e);

return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(shadow.cljs.devtools.client.env.repl_error(e),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),shadow.cljs.devtools.client.browser.get_ua_product(),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"asset-root","asset-root",1771735072),shadow.cljs.devtools.client.browser.get_asset_root()], 0));
});
shadow.cljs.devtools.client.browser.repl_invoke = (function shadow$cljs$devtools$client$browser$repl_invoke(p__35419){
var map__35420 = p__35419;
var map__35420__$1 = (((((!((map__35420 == null))))?(((((map__35420.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35420.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35420):map__35420);
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35420__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35420__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var result = shadow.cljs.devtools.client.env.repl_call(((function (map__35420,map__35420__$1,id,js){
return (function (){
return eval(js);
});})(map__35420,map__35420__$1,id,js))
,shadow.cljs.devtools.client.browser.repl_error);
return shadow.cljs.devtools.client.browser.ws_msg(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(result,new cljs.core.Keyword(null,"id","id",-1388402092),id));
});
shadow.cljs.devtools.client.browser.repl_require = (function shadow$cljs$devtools$client$browser$repl_require(p__35422){
var map__35423 = p__35422;
var map__35423__$1 = (((((!((map__35423 == null))))?(((((map__35423.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35423.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35423):map__35423);
var msg = map__35423__$1;
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35423__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35423__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35423__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var js_requires = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35423__$1,new cljs.core.Keyword(null,"js-requires","js-requires",-1311472051));
var sources_to_load = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(((function (map__35423,map__35423__$1,msg,id,sources,reload_namespaces,js_requires){
return (function (p__35425){
var map__35426 = p__35425;
var map__35426__$1 = (((((!((map__35426 == null))))?(((((map__35426.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35426.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35426):map__35426);
var src = map__35426__$1;
var provides = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35426__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var and__4120__auto__ = shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_(src);
if(cljs.core.truth_(and__4120__auto__)){
return cljs.core.not(cljs.core.some(reload_namespaces,provides));
} else {
return and__4120__auto__;
}
});})(map__35423,map__35423__$1,msg,id,sources,reload_namespaces,js_requires))
,sources));
return shadow.cljs.devtools.client.browser.load_sources(sources_to_load,((function (sources_to_load,map__35423,map__35423__$1,msg,id,sources,reload_namespaces,js_requires){
return (function (sources__$1){
shadow.cljs.devtools.client.browser.do_js_load(sources__$1);

if(cljs.core.seq(js_requires)){
shadow.cljs.devtools.client.browser.do_js_requires(js_requires);
} else {
}

return shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","require-complete","repl/require-complete",-2140254719),new cljs.core.Keyword(null,"id","id",-1388402092),id], null));
});})(sources_to_load,map__35423,map__35423__$1,msg,id,sources,reload_namespaces,js_requires))
);
});
shadow.cljs.devtools.client.browser.repl_init = (function shadow$cljs$devtools$client$browser$repl_init(p__35428){
var map__35429 = p__35428;
var map__35429__$1 = (((((!((map__35429 == null))))?(((((map__35429.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35429.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35429):map__35429);
var repl_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35429__$1,new cljs.core.Keyword(null,"repl-state","repl-state",-1733780387));
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35429__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
return shadow.cljs.devtools.client.browser.load_sources(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535).cljs$core$IFn$_invoke$arity$1(repl_state))),((function (map__35429,map__35429__$1,repl_state,id){
return (function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","init-complete","repl/init-complete",-162252879),new cljs.core.Keyword(null,"id","id",-1388402092),id], null));

return shadow.cljs.devtools.client.browser.devtools_msg("REPL session start successful");
});})(map__35429,map__35429__$1,repl_state,id))
);
});
shadow.cljs.devtools.client.browser.repl_set_ns = (function shadow$cljs$devtools$client$browser$repl_set_ns(p__35431){
var map__35432 = p__35431;
var map__35432__$1 = (((((!((map__35432 == null))))?(((((map__35432.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35432.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35432):map__35432);
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35432__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35432__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
return shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","set-ns-complete","repl/set-ns-complete",680944662),new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"ns","ns",441598760),ns], null));
});
shadow.cljs.devtools.client.browser.close_reason_ref = cljs.core.volatile_BANG_(null);
shadow.cljs.devtools.client.browser.handle_message = (function shadow$cljs$devtools$client$browser$handle_message(p__35434){
var map__35435 = p__35434;
var map__35435__$1 = (((((!((map__35435 == null))))?(((((map__35435.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35435.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35435):map__35435);
var msg = map__35435__$1;
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35435__$1,new cljs.core.Keyword(null,"type","type",1174270348));
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

var G__35437 = type;
var G__35437__$1 = (((G__35437 instanceof cljs.core.Keyword))?G__35437.fqn:null);
switch (G__35437__$1) {
case "asset-watch":
return shadow.cljs.devtools.client.browser.handle_asset_watch(msg);

break;
case "repl/invoke":
return shadow.cljs.devtools.client.browser.repl_invoke(msg);

break;
case "repl/require":
return shadow.cljs.devtools.client.browser.repl_require(msg);

break;
case "repl/set-ns":
return shadow.cljs.devtools.client.browser.repl_set_ns(msg);

break;
case "repl/init":
return shadow.cljs.devtools.client.browser.repl_init(msg);

break;
case "repl/session-start":
return shadow.cljs.devtools.client.browser.repl_init(msg);

break;
case "build-complete":
shadow.cljs.devtools.client.hud.hud_warnings(msg);

return shadow.cljs.devtools.client.browser.handle_build_complete(msg);

break;
case "build-failure":
shadow.cljs.devtools.client.hud.load_end();

return shadow.cljs.devtools.client.hud.hud_error(msg);

break;
case "build-init":
return shadow.cljs.devtools.client.hud.hud_warnings(msg);

break;
case "build-start":
shadow.cljs.devtools.client.hud.hud_hide();

return shadow.cljs.devtools.client.hud.load_start();

break;
case "pong":
return null;

break;
case "client/stale":
return cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.close_reason_ref,"Stale Client! You are not using the latest compilation output!");

break;
case "client/no-worker":
return cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.close_reason_ref,["watch for build \"",shadow.cljs.devtools.client.env.build_id,"\" not running"].join(''));

break;
case "custom-msg":
return shadow.cljs.devtools.client.env.publish_BANG_(new cljs.core.Keyword(null,"payload","payload",-383036092).cljs$core$IFn$_invoke$arity$1(msg));

break;
default:
return new cljs.core.Keyword(null,"ignored","ignored",1227374526);

}
});
shadow.cljs.devtools.client.browser.compile = (function shadow$cljs$devtools$client$browser$compile(text,callback){
var G__35438 = ["http",((shadow.cljs.devtools.client.env.ssl)?"s":null),"://",shadow.cljs.devtools.client.env.server_host,":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.env.server_port),"/worker/compile/",shadow.cljs.devtools.client.env.build_id,"/",shadow.cljs.devtools.client.env.proc_id,"/browser"].join('');
var G__35439 = ((function (G__35438){
return (function (res){
var req = this;
var actions = cljs.reader.read_string.cljs$core$IFn$_invoke$arity$1(req.getResponseText());
if(cljs.core.truth_(callback)){
return (callback.cljs$core$IFn$_invoke$arity$1 ? callback.cljs$core$IFn$_invoke$arity$1(actions) : callback.call(null,actions));
} else {
return null;
}
});})(G__35438))
;
var G__35440 = "POST";
var G__35441 = cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"input","input",556931961),text], null)], 0));
var G__35442 = ({"content-type": "application/edn; charset=utf-8"});
return goog.net.XhrIo.send(G__35438,G__35439,G__35440,G__35441,G__35442);
});
shadow.cljs.devtools.client.browser.heartbeat_BANG_ = (function shadow$cljs$devtools$client$browser$heartbeat_BANG_(){
var temp__5720__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5720__auto__)){
var s = temp__5720__auto__;
s.send(cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"ping","ping",-1670114784),new cljs.core.Keyword(null,"v","v",21465059),Date.now()], null)], 0)));

return setTimeout(shadow.cljs.devtools.client.browser.heartbeat_BANG_,(30000));
} else {
return null;
}
});
shadow.cljs.devtools.client.browser.ws_connect = (function shadow$cljs$devtools$client$browser$ws_connect(){
try{var print_fn = cljs.core._STAR_print_fn_STAR_;
var ws_url = shadow.cljs.devtools.client.env.ws_url(new cljs.core.Keyword(null,"browser","browser",828191719));
var socket = (new WebSocket(ws_url));
cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.socket_ref,socket);

socket.onmessage = ((function (print_fn,ws_url,socket){
return (function (e){
return shadow.cljs.devtools.client.env.process_ws_msg(e.data,shadow.cljs.devtools.client.browser.handle_message);
});})(print_fn,ws_url,socket))
;

socket.onopen = ((function (print_fn,ws_url,socket){
return (function (e){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.close_reason_ref,null);

if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("goog",shadow.cljs.devtools.client.env.module_format)){
goog.provide = goog.constructNamespace_;
} else {
}

shadow.cljs.devtools.client.env.set_print_fns_BANG_(shadow.cljs.devtools.client.browser.ws_msg);

return shadow.cljs.devtools.client.browser.devtools_msg("WebSocket connected!");
});})(print_fn,ws_url,socket))
;

socket.onclose = ((function (print_fn,ws_url,socket){
return (function (e){
shadow.cljs.devtools.client.browser.devtools_msg("WebSocket disconnected!");

shadow.cljs.devtools.client.hud.connection_error((function (){var or__4131__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.close_reason_ref);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "Connection closed!";
}
})());

cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.socket_ref,null);

return shadow.cljs.devtools.client.env.reset_print_fns_BANG_();
});})(print_fn,ws_url,socket))
;

socket.onerror = ((function (print_fn,ws_url,socket){
return (function (e){
shadow.cljs.devtools.client.hud.connection_error("Connection failed!");

return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("websocket error",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([e], 0));
});})(print_fn,ws_url,socket))
;

return setTimeout(shadow.cljs.devtools.client.browser.heartbeat_BANG_,(30000));
}catch (e35443){var e = e35443;
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("WebSocket setup failed",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([e], 0));
}});
if(shadow.cljs.devtools.client.env.enabled){
var temp__5720__auto___35596 = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5720__auto___35596)){
var s_35597 = temp__5720__auto___35596;
shadow.cljs.devtools.client.browser.devtools_msg("connection reset!");

s_35597.onclose = ((function (s_35597,temp__5720__auto___35596){
return (function (e){
return null;
});})(s_35597,temp__5720__auto___35596))
;

s_35597.close();

cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.socket_ref,null);
} else {
}

window.addEventListener("beforeunload",(function (){
var temp__5720__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5720__auto__)){
var s = temp__5720__auto__;
return s.close();
} else {
return null;
}
}));

if(cljs.core.truth_((function (){var and__4120__auto__ = document;
if(cljs.core.truth_(and__4120__auto__)){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("loading",document.readyState);
} else {
return and__4120__auto__;
}
})())){
window.addEventListener("DOMContentLoaded",shadow.cljs.devtools.client.browser.ws_connect);
} else {
setTimeout(shadow.cljs.devtools.client.browser.ws_connect,(10));
}
} else {
}

//# sourceMappingURL=shadow.cljs.devtools.client.browser.js.map
