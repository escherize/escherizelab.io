goog.provide('bt.css_gen');
goog.require('cljs.core');
goog.require('garden.core');
goog.require('garden.color');
goog.require('clojure.string');
bt.css_gen.lighten = (function bt$css_gen$lighten(var_args){
var args__4736__auto__ = [];
var len__4730__auto___74664 = arguments.length;
var i__4731__auto___74665 = (0);
while(true){
if((i__4731__auto___74665 < len__4730__auto___74664)){
args__4736__auto__.push((arguments[i__4731__auto___74665]));

var G__74666 = (i__4731__auto___74665 + (1));
i__4731__auto___74665 = G__74666;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return bt.css_gen.lighten.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

bt.css_gen.lighten.cljs$core$IFn$_invoke$arity$variadic = (function (hex_color,p__74632){
var vec__74633 = p__74632;
var amount = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74633,(0),null);
return garden.color.as_hex(garden.color.lighten(garden.color.hex__GT_rgb(hex_color),(function (){var or__4131__auto__ = amount;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (10);
}
})()));
});

bt.css_gen.lighten.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
bt.css_gen.lighten.cljs$lang$applyTo = (function (seq74630){
var G__74631 = cljs.core.first(seq74630);
var seq74630__$1 = cljs.core.next(seq74630);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__74631,seq74630__$1);
});

bt.css_gen.darken = (function bt$css_gen$darken(var_args){
var args__4736__auto__ = [];
var len__4730__auto___74667 = arguments.length;
var i__4731__auto___74668 = (0);
while(true){
if((i__4731__auto___74668 < len__4730__auto___74667)){
args__4736__auto__.push((arguments[i__4731__auto___74668]));

var G__74669 = (i__4731__auto___74668 + (1));
i__4731__auto___74668 = G__74669;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return bt.css_gen.darken.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

bt.css_gen.darken.cljs$core$IFn$_invoke$arity$variadic = (function (hex_color,p__74638){
var vec__74639 = p__74638;
var amount = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74639,(0),null);
return garden.color.as_hex(garden.color.darken(garden.color.hex__GT_rgb(hex_color),(function (){var or__4131__auto__ = amount;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (10);
}
})()));
});

bt.css_gen.darken.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
bt.css_gen.darken.cljs$lang$applyTo = (function (seq74636){
var G__74637 = cljs.core.first(seq74636);
var seq74636__$1 = cljs.core.next(seq74636);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__74637,seq74636__$1);
});

bt.css_gen.rotate = (function bt$css_gen$rotate(var_args){
var args__4736__auto__ = [];
var len__4730__auto___74670 = arguments.length;
var i__4731__auto___74671 = (0);
while(true){
if((i__4731__auto___74671 < len__4730__auto___74670)){
args__4736__auto__.push((arguments[i__4731__auto___74671]));

var G__74672 = (i__4731__auto___74671 + (1));
i__4731__auto___74671 = G__74672;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return bt.css_gen.rotate.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

bt.css_gen.rotate.cljs$core$IFn$_invoke$arity$variadic = (function (hex_color,p__74644){
var vec__74645 = p__74644;
var amount = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74645,(0),null);
return garden.color.as_hex(garden.color.rotate_hue(garden.color.hex__GT_rgb(hex_color),(function (){var or__4131__auto__ = amount;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (10);
}
})()));
});

bt.css_gen.rotate.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
bt.css_gen.rotate.cljs$lang$applyTo = (function (seq74642){
var G__74643 = cljs.core.first(seq74642);
var seq74642__$1 = cljs.core.next(seq74642);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__74643,seq74642__$1);
});

bt.css_gen.team__GT_color = new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("team","one","team/one",942492413),"#a5d4f3",new cljs.core.Keyword("team","two","team/two",522109688),"#f0c58f"], null);
bt.css_gen.player_css = (function bt$css_gen$player_css(p__74648){
var map__74649 = p__74648;
var map__74649__$1 = (((((!((map__74649 == null))))?(((((map__74649.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74649.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74649):map__74649);
var team = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74649__$1,new cljs.core.Keyword(null,"team","team",1355747699));
var height = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74649__$1,new cljs.core.Keyword(null,"height","height",1025178622));
var color = (bt.css_gen.team__GT_color.cljs$core$IFn$_invoke$arity$1 ? bt.css_gen.team__GT_color.cljs$core$IFn$_invoke$arity$1(team) : bt.css_gen.team__GT_color.call(null,team));
return garden.core.css.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.keyword.cljs$core$IFn$_invoke$arity$1([".player.",cljs.core.name(team),".height-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(height)].join('')),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"box-shadow","box-shadow",1600206755),new cljs.core.Keyword(null,"top","top",-1856271961),new cljs.core.Keyword(null,"content","content",15833224),new cljs.core.Keyword(null,"background-color","background-color",570434026),new cljs.core.Keyword(null,"width","width",-384071477),new cljs.core.Keyword(null,"-webkit-transform","-webkit-transform",-624763371),new cljs.core.Keyword(null,"position","position",-2011731912),new cljs.core.Keyword(null,"-webkit-transform-style","-webkit-transform-style",1063670232),new cljs.core.Keyword(null,"-webkit-transition","-webkit-transition",1908091196),new cljs.core.Keyword(null,"height","height",1025178622),new cljs.core.Keyword(null,"left","left",-399115937)],["inset 0 0 0 .3em hsla(0,0%,0%,.4)","1em","",color,"5em",["translateZ(",cljs.core.str.cljs$core$IFn$_invoke$arity$1(((height + (1)) * (3))),"em) scaleX(0.2) scaleY(0.2)"].join(''),"absolute","preserve-3d","0.2s","5em","1em"]),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"&:after","&:after",-945103658),new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"background-color","background-color",570434026),color,new cljs.core.Keyword(null,"-webkit-transform","-webkit-transform",-624763371),"rotateX(-90deg) translateY(3em) scaleY(0.6)",new cljs.core.Keyword(null,"-webkit-transform-origin","-webkit-transform-origin",-969759694),"100% 100%",new cljs.core.Keyword(null,"box-shadow","box-shadow",1600206755),"inset 0 0 0 .3em hsla(0,0%,0%,0.4)",new cljs.core.Keyword(null,"content","content",15833224),"''",new cljs.core.Keyword(null,"height","height",1025178622),"5em",new cljs.core.Keyword(null,"width","width",-384071477),"5em",new cljs.core.Keyword(null,"position","position",-2011731912),"absolute"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"&:before","&:before",-1329379652),new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"background-color","background-color",570434026),color,new cljs.core.Keyword(null,"-webkit-transform","-webkit-transform",-624763371),"rotateY(90deg) translateX(3em) scaleX(0.6)",new cljs.core.Keyword(null,"-webkit-transform-origin","-webkit-transform-origin",-969759694),"100% 0%",new cljs.core.Keyword(null,"box-shadow","box-shadow",1600206755),"inset 0 0 0 .3em hsla(0,0%,0%,0.4)",new cljs.core.Keyword(null,"content","content",15833224),"''",new cljs.core.Keyword(null,"height","height",1025178622),"5em",new cljs.core.Keyword(null,"width","width",-384071477),"5em",new cljs.core.Keyword(null,"position","position",-2011731912),"absolute"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,".selected",".selected",867184558),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"background-color","background-color",570434026),"red"], null)], null)], null)], null)], 0));
});
bt.css_gen.class__GT_attrs = new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"base","base",185279322),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"background-color","background-color",570434026),"#e7e7e7",new cljs.core.Keyword(null,"height","height",1025178622),2.5,new cljs.core.Keyword(null,"scale","scale",-230427353),0.9], null),new cljs.core.Keyword(null,"mid","mid",-2123385246),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"background-color","background-color",570434026),bt.css_gen.lighten.cljs$core$IFn$_invoke$arity$variadic("#dadada",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(15)], 0)),new cljs.core.Keyword(null,"height","height",1025178622),(5),new cljs.core.Keyword(null,"scale","scale",-230427353),0.75], null),new cljs.core.Keyword(null,"top","top",-1856271961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"background-color","background-color",570434026),bt.css_gen.lighten.cljs$core$IFn$_invoke$arity$variadic("#dadada",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(30)], 0)),new cljs.core.Keyword(null,"height","height",1025178622),7.5,new cljs.core.Keyword(null,"scale","scale",-230427353),0.6], null),new cljs.core.Keyword(null,"dome","dome",47747471),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"background-color","background-color",570434026),bt.css_gen.lighten.cljs$core$IFn$_invoke$arity$variadic((function (){var G__74651 = new cljs.core.Keyword(null,"blue","blue",-622100620);
return (garden.color.color_name__GT_hex.cljs$core$IFn$_invoke$arity$1 ? garden.color.color_name__GT_hex.cljs$core$IFn$_invoke$arity$1(G__74651) : garden.color.color_name__GT_hex.call(null,G__74651));
})(),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(20)], 0)),new cljs.core.Keyword(null,"height","height",1025178622),8.5,new cljs.core.Keyword(null,"scale","scale",-230427353),0.4], null)], null);
bt.css_gen.block_css = (function bt$css_gen$block_css(class$){
var map__74652 = (bt.css_gen.class__GT_attrs.cljs$core$IFn$_invoke$arity$1 ? bt.css_gen.class__GT_attrs.cljs$core$IFn$_invoke$arity$1(class$) : bt.css_gen.class__GT_attrs.call(null,class$));
var map__74652__$1 = (((((!((map__74652 == null))))?(((((map__74652.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74652.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74652):map__74652);
var background_color = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74652__$1,new cljs.core.Keyword(null,"background-color","background-color",570434026));
var height = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74652__$1,new cljs.core.Keyword(null,"height","height",1025178622));
var scale = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74652__$1,new cljs.core.Keyword(null,"scale","scale",-230427353));
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.keyword.cljs$core$IFn$_invoke$arity$1([".",cljs.core.name(class$)].join('')),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"box-shadow","box-shadow",1600206755),new cljs.core.Keyword(null,"content","content",15833224),new cljs.core.Keyword(null,"background-color","background-color",570434026),new cljs.core.Keyword(null,"width","width",-384071477),new cljs.core.Keyword(null,"-webkit-transform","-webkit-transform",-624763371),new cljs.core.Keyword(null,"position","position",-2011731912),new cljs.core.Keyword(null,"-webkit-transform-style","-webkit-transform-style",1063670232),new cljs.core.Keyword(null,"-webkit-transition","-webkit-transition",1908091196),new cljs.core.Keyword(null,"height","height",1025178622)],["inset 0 0 0 .1em hsla(0,0%,0%,.1)","''",background_color,"5em",["translateZ( ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(height),"em ) scale(",cljs.core.str.cljs$core$IFn$_invoke$arity$1(scale),")"].join(''),"absolute","preserve-3d",".5s","5em"]),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"&:before","&:before",-1329379652),new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"background-color","background-color",570434026),bt.css_gen.darken(bt.css_gen.darken(background_color)),new cljs.core.Keyword(null,"-webkit-transform","-webkit-transform",-624763371),"rotateY(90deg) translateX(3em) scaleX(0.62)",new cljs.core.Keyword(null,"-webkit-transform-origin","-webkit-transform-origin",-969759694),"100% 0",new cljs.core.Keyword(null,"box-shadow","box-shadow",1600206755),"inset 0 0 0 .1em hsla(0,0%,0%,.1)",new cljs.core.Keyword(null,"content","content",15833224),"''",new cljs.core.Keyword(null,"height","height",1025178622),"5em",new cljs.core.Keyword(null,"position","position",-2011731912),"absolute",new cljs.core.Keyword(null,"width","width",-384071477),"5em"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"&:after","&:after",-945103658),new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"background-color","background-color",570434026),bt.css_gen.darken(background_color),new cljs.core.Keyword(null,"-webkit-transform","-webkit-transform",-624763371),"rotateX(-90deg) translateY(3em) scaleY(0.62)",new cljs.core.Keyword(null,"-webkit-transform-origin","-webkit-transform-origin",-969759694),"100% 100%",new cljs.core.Keyword(null,"box-shadow","box-shadow",1600206755),"inset 0 0 0 .1em hsla(0,0%,0%,0.1)",new cljs.core.Keyword(null,"content","content",15833224),"''",new cljs.core.Keyword(null,"height","height",1025178622),"5em",new cljs.core.Keyword(null,"position","position",-2011731912),"absolute",new cljs.core.Keyword(null,"width","width",-384071477),"5em"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"&:hover","&:hover",-852658855),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"background-color","background-color",570434026),bt.css_gen.rotate.cljs$core$IFn$_invoke$arity$variadic(bt.css_gen.darken(background_color),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(40)], 0))], null)], null)], null)], null);
});
bt.css_gen.generate_player_css = (function bt$css_gen$generate_player_css(){
return clojure.string.join.cljs$core$IFn$_invoke$arity$2("\n",(function (){var iter__4523__auto__ = (function bt$css_gen$generate_player_css_$_iter__74654(s__74655){
return (new cljs.core.LazySeq(null,(function (){
var s__74655__$1 = s__74655;
while(true){
var temp__5720__auto__ = cljs.core.seq(s__74655__$1);
if(temp__5720__auto__){
var xs__6277__auto__ = temp__5720__auto__;
var height = cljs.core.first(xs__6277__auto__);
var iterys__4519__auto__ = ((function (s__74655__$1,height,xs__6277__auto__,temp__5720__auto__){
return (function bt$css_gen$generate_player_css_$_iter__74654_$_iter__74656(s__74657){
return (new cljs.core.LazySeq(null,((function (s__74655__$1,height,xs__6277__auto__,temp__5720__auto__){
return (function (){
var s__74657__$1 = s__74657;
while(true){
var temp__5720__auto____$1 = cljs.core.seq(s__74657__$1);
if(temp__5720__auto____$1){
var s__74657__$2 = temp__5720__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__74657__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__74657__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__74659 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__74658 = (0);
while(true){
if((i__74658 < size__4522__auto__)){
var team = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__74658);
cljs.core.chunk_append(b__74659,bt.css_gen.player_css(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"team","team",1355747699),team,new cljs.core.Keyword(null,"height","height",1025178622),height], null)));

var G__74686 = (i__74658 + (1));
i__74658 = G__74686;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__74659),bt$css_gen$generate_player_css_$_iter__74654_$_iter__74656(cljs.core.chunk_rest(s__74657__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__74659),null);
}
} else {
var team = cljs.core.first(s__74657__$2);
return cljs.core.cons(bt.css_gen.player_css(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"team","team",1355747699),team,new cljs.core.Keyword(null,"height","height",1025178622),height], null)),bt$css_gen$generate_player_css_$_iter__74654_$_iter__74656(cljs.core.rest(s__74657__$2)));
}
} else {
return null;
}
break;
}
});})(s__74655__$1,height,xs__6277__auto__,temp__5720__auto__))
,null,null));
});})(s__74655__$1,height,xs__6277__auto__,temp__5720__auto__))
;
var fs__4520__auto__ = cljs.core.seq(iterys__4519__auto__(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("team","one","team/one",942492413),new cljs.core.Keyword("team","two","team/two",522109688)], null)));
if(fs__4520__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4520__auto__,bt$css_gen$generate_player_css_$_iter__74654(cljs.core.rest(s__74655__$1)));
} else {
var G__74688 = cljs.core.rest(s__74655__$1);
s__74655__$1 = G__74688;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [(0),(1),(2),(3)], null));
})());
});
bt.css_gen.generate_block_css = (function bt$css_gen$generate_block_css(){
return clojure.string.join.cljs$core$IFn$_invoke$arity$2("\n",(function (){var iter__4523__auto__ = (function bt$css_gen$generate_block_css_$_iter__74660(s__74661){
return (new cljs.core.LazySeq(null,(function (){
var s__74661__$1 = s__74661;
while(true){
var temp__5720__auto__ = cljs.core.seq(s__74661__$1);
if(temp__5720__auto__){
var s__74661__$2 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_(s__74661__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__74661__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__74663 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__74662 = (0);
while(true){
if((i__74662 < size__4522__auto__)){
var block_type = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__74662);
cljs.core.chunk_append(b__74663,garden.core.css.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([bt.css_gen.block_css(block_type)], 0)));

var G__74693 = (i__74662 + (1));
i__74662 = G__74693;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__74663),bt$css_gen$generate_block_css_$_iter__74660(cljs.core.chunk_rest(s__74661__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__74663),null);
}
} else {
var block_type = cljs.core.first(s__74661__$2);
return cljs.core.cons(garden.core.css.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([bt.css_gen.block_css(block_type)], 0)),bt$css_gen$generate_block_css_$_iter__74660(cljs.core.rest(s__74661__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4523__auto__(cljs.core.keys(bt.css_gen.class__GT_attrs));
})());
});

//# sourceMappingURL=bt.css_gen.js.map
