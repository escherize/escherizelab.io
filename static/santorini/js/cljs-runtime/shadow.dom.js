goog.provide('shadow.dom');
goog.require('cljs.core');
goog.require('goog.dom');
goog.require('goog.dom.forms');
goog.require('goog.dom.classlist');
goog.require('goog.style');
goog.require('goog.style.transition');
goog.require('goog.string');
goog.require('clojure.string');
goog.require('cljs.core.async');
shadow.dom.transition_supported_QMARK_ = (((typeof window !== 'undefined'))?goog.style.transition.isSupported():null);

/**
 * @interface
 */
shadow.dom.IElement = function(){};

shadow.dom._to_dom = (function shadow$dom$_to_dom(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$IElement$_to_dom$arity$1 == null)))))){
return this$.shadow$dom$IElement$_to_dom$arity$1(this$);
} else {
var x__4433__auto__ = (((this$ == null))?null:this$);
var m__4434__auto__ = (shadow.dom._to_dom[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4434__auto__.call(null,this$));
} else {
var m__4431__auto__ = (shadow.dom._to_dom["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4431__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("IElement.-to-dom",this$);
}
}
}
});


/**
 * @interface
 */
shadow.dom.SVGElement = function(){};

shadow.dom._to_svg = (function shadow$dom$_to_svg(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$SVGElement$_to_svg$arity$1 == null)))))){
return this$.shadow$dom$SVGElement$_to_svg$arity$1(this$);
} else {
var x__4433__auto__ = (((this$ == null))?null:this$);
var m__4434__auto__ = (shadow.dom._to_svg[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4434__auto__.call(null,this$));
} else {
var m__4431__auto__ = (shadow.dom._to_svg["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4431__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("SVGElement.-to-svg",this$);
}
}
}
});

shadow.dom.lazy_native_coll_seq = (function shadow$dom$lazy_native_coll_seq(coll,idx){
if((idx < coll.length)){
return (new cljs.core.LazySeq(null,(function (){
return cljs.core.cons((coll[idx]),(function (){var G__33281 = coll;
var G__33282 = (idx + (1));
return (shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2 ? shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2(G__33281,G__33282) : shadow.dom.lazy_native_coll_seq.call(null,G__33281,G__33282));
})());
}),null,null));
} else {
return null;
}
});

/**
* @constructor
 * @implements {cljs.core.IIndexed}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IDeref}
 * @implements {shadow.dom.IElement}
*/
shadow.dom.NativeColl = (function (coll){
this.coll = coll;
this.cljs$lang$protocol_mask$partition0$ = 8421394;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
shadow.dom.NativeColl.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
});

shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (this$,n){
var self__ = this;
var this$__$1 = this;
return (self__.coll[n]);
});

shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (this$,n,not_found){
var self__ = this;
var this$__$1 = this;
var or__4131__auto__ = (self__.coll[n]);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return not_found;
}
});

shadow.dom.NativeColl.prototype.cljs$core$ICounted$_count$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll.length;
});

shadow.dom.NativeColl.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return shadow.dom.lazy_native_coll_seq(self__.coll,(0));
});

shadow.dom.NativeColl.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

shadow.dom.NativeColl.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
});

shadow.dom.NativeColl.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"coll","coll",-1006698606,null)], null);
});

shadow.dom.NativeColl.cljs$lang$type = true;

shadow.dom.NativeColl.cljs$lang$ctorStr = "shadow.dom/NativeColl";

shadow.dom.NativeColl.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"shadow.dom/NativeColl");
});

/**
 * Positional factory function for shadow.dom/NativeColl.
 */
shadow.dom.__GT_NativeColl = (function shadow$dom$__GT_NativeColl(coll){
return (new shadow.dom.NativeColl(coll));
});

shadow.dom.native_coll = (function shadow$dom$native_coll(coll){
return (new shadow.dom.NativeColl(coll));
});
shadow.dom.dom_node = (function shadow$dom$dom_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$IElement$))))?true:false):false)){
return el.shadow$dom$IElement$_to_dom$arity$1(null);
} else {
if(typeof el === 'string'){
return document.createTextNode(el);
} else {
if(typeof el === 'number'){
return document.createTextNode(cljs.core.str.cljs$core$IFn$_invoke$arity$1(el));
} else {
return el;

}
}
}
}
});
shadow.dom.query_one = (function shadow$dom$query_one(var_args){
var G__33314 = arguments.length;
switch (G__33314) {
case 1:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return document.querySelector(sel);
});

shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return shadow.dom.dom_node(root).querySelector(sel);
});

shadow.dom.query_one.cljs$lang$maxFixedArity = 2;

shadow.dom.query = (function shadow$dom$query(var_args){
var G__33319 = arguments.length;
switch (G__33319) {
case 1:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.query.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return (new shadow.dom.NativeColl(document.querySelectorAll(sel)));
});

shadow.dom.query.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(root).querySelectorAll(sel)));
});

shadow.dom.query.cljs$lang$maxFixedArity = 2;

shadow.dom.by_id = (function shadow$dom$by_id(var_args){
var G__33324 = arguments.length;
switch (G__33324) {
case 2:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2 = (function (id,el){
return shadow.dom.dom_node(el).getElementById(id);
});

shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1 = (function (id){
return document.getElementById(id);
});

shadow.dom.by_id.cljs$lang$maxFixedArity = 2;

shadow.dom.build = shadow.dom.dom_node;
shadow.dom.ev_stop = (function shadow$dom$ev_stop(var_args){
var G__33332 = arguments.length;
switch (G__33332) {
case 1:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1 = (function (e){
if(cljs.core.truth_(e.stopPropagation)){
e.stopPropagation();

e.preventDefault();
} else {
e.cancelBubble = true;

e.returnValue = false;
}

return e;
});

shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2 = (function (e,el){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
});

shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4 = (function (e,el,scope,owner){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
});

shadow.dom.ev_stop.cljs$lang$maxFixedArity = 4;

/**
 * check wether a parent node (or the document) contains the child
 */
shadow.dom.contains_QMARK_ = (function shadow$dom$contains_QMARK_(var_args){
var G__33342 = arguments.length;
switch (G__33342) {
case 1:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1 = (function (el){
var G__33348 = document;
var G__33349 = shadow.dom.dom_node(el);
return goog.dom.contains(G__33348,G__33349);
});

shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2 = (function (parent,el){
var G__33351 = shadow.dom.dom_node(parent);
var G__33352 = shadow.dom.dom_node(el);
return goog.dom.contains(G__33351,G__33352);
});

shadow.dom.contains_QMARK_.cljs$lang$maxFixedArity = 2;

shadow.dom.add_class = (function shadow$dom$add_class(el,cls){
var G__33353 = shadow.dom.dom_node(el);
var G__33354 = cls;
return goog.dom.classlist.add(G__33353,G__33354);
});
shadow.dom.remove_class = (function shadow$dom$remove_class(el,cls){
var G__33359 = shadow.dom.dom_node(el);
var G__33360 = cls;
return goog.dom.classlist.remove(G__33359,G__33360);
});
shadow.dom.toggle_class = (function shadow$dom$toggle_class(var_args){
var G__33362 = arguments.length;
switch (G__33362) {
case 2:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2 = (function (el,cls){
var G__33364 = shadow.dom.dom_node(el);
var G__33365 = cls;
return goog.dom.classlist.toggle(G__33364,G__33365);
});

shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3 = (function (el,cls,v){
if(cljs.core.truth_(v)){
return shadow.dom.add_class(el,cls);
} else {
return shadow.dom.remove_class(el,cls);
}
});

shadow.dom.toggle_class.cljs$lang$maxFixedArity = 3;

shadow.dom.dom_listen = (cljs.core.truth_((function (){var or__4131__auto__ = (!((typeof document !== 'undefined')));
if(or__4131__auto__){
return or__4131__auto__;
} else {
return document.addEventListener;
}
})())?(function shadow$dom$dom_listen_good(el,ev,handler){
return el.addEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_ie(el,ev,handler){
try{return el.attachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),(function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
}));
}catch (e33380){if((e33380 instanceof Object)){
var e = e33380;
return console.log("didnt support attachEvent",el,e);
} else {
throw e33380;

}
}}));
shadow.dom.dom_listen_remove = (cljs.core.truth_((function (){var or__4131__auto__ = (!((typeof document !== 'undefined')));
if(or__4131__auto__){
return or__4131__auto__;
} else {
return document.removeEventListener;
}
})())?(function shadow$dom$dom_listen_remove_good(el,ev,handler){
return el.removeEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_remove_ie(el,ev,handler){
return el.detachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),handler);
}));
shadow.dom.on_query = (function shadow$dom$on_query(root_el,ev,selector,handler){
var seq__33407 = cljs.core.seq(shadow.dom.query.cljs$core$IFn$_invoke$arity$2(selector,root_el));
var chunk__33408 = null;
var count__33409 = (0);
var i__33410 = (0);
while(true){
if((i__33410 < count__33409)){
var el = chunk__33408.cljs$core$IIndexed$_nth$arity$2(null,i__33410);
var handler_33980__$1 = ((function (seq__33407,chunk__33408,count__33409,i__33410,el){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__33407,chunk__33408,count__33409,i__33410,el))
;
var G__33424_33981 = el;
var G__33425_33982 = cljs.core.name(ev);
var G__33426_33983 = handler_33980__$1;
(shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__33424_33981,G__33425_33982,G__33426_33983) : shadow.dom.dom_listen.call(null,G__33424_33981,G__33425_33982,G__33426_33983));


var G__33984 = seq__33407;
var G__33985 = chunk__33408;
var G__33986 = count__33409;
var G__33987 = (i__33410 + (1));
seq__33407 = G__33984;
chunk__33408 = G__33985;
count__33409 = G__33986;
i__33410 = G__33987;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq(seq__33407);
if(temp__5720__auto__){
var seq__33407__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__33407__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__33407__$1);
var G__33990 = cljs.core.chunk_rest(seq__33407__$1);
var G__33991 = c__4550__auto__;
var G__33992 = cljs.core.count(c__4550__auto__);
var G__33993 = (0);
seq__33407 = G__33990;
chunk__33408 = G__33991;
count__33409 = G__33992;
i__33410 = G__33993;
continue;
} else {
var el = cljs.core.first(seq__33407__$1);
var handler_33994__$1 = ((function (seq__33407,chunk__33408,count__33409,i__33410,el,seq__33407__$1,temp__5720__auto__){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__33407,chunk__33408,count__33409,i__33410,el,seq__33407__$1,temp__5720__auto__))
;
var G__33428_33995 = el;
var G__33429_33996 = cljs.core.name(ev);
var G__33430_33997 = handler_33994__$1;
(shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__33428_33995,G__33429_33996,G__33430_33997) : shadow.dom.dom_listen.call(null,G__33428_33995,G__33429_33996,G__33430_33997));


var G__33998 = cljs.core.next(seq__33407__$1);
var G__33999 = null;
var G__34000 = (0);
var G__34001 = (0);
seq__33407 = G__33998;
chunk__33408 = G__33999;
count__33409 = G__34000;
i__33410 = G__34001;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.on = (function shadow$dom$on(var_args){
var G__33436 = arguments.length;
switch (G__33436) {
case 3:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.on.cljs$core$IFn$_invoke$arity$3 = (function (el,ev,handler){
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4(el,ev,handler,false);
});

shadow.dom.on.cljs$core$IFn$_invoke$arity$4 = (function (el,ev,handler,capture){
if(cljs.core.vector_QMARK_(ev)){
return shadow.dom.on_query(el,cljs.core.first(ev),cljs.core.second(ev),handler);
} else {
var handler__$1 = (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});
var G__33439 = shadow.dom.dom_node(el);
var G__33440 = cljs.core.name(ev);
var G__33441 = handler__$1;
return (shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__33439,G__33440,G__33441) : shadow.dom.dom_listen.call(null,G__33439,G__33440,G__33441));
}
});

shadow.dom.on.cljs$lang$maxFixedArity = 4;

shadow.dom.remove_event_handler = (function shadow$dom$remove_event_handler(el,ev,handler){
var G__33443 = shadow.dom.dom_node(el);
var G__33444 = cljs.core.name(ev);
var G__33445 = handler;
return (shadow.dom.dom_listen_remove.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen_remove.cljs$core$IFn$_invoke$arity$3(G__33443,G__33444,G__33445) : shadow.dom.dom_listen_remove.call(null,G__33443,G__33444,G__33445));
});
shadow.dom.add_event_listeners = (function shadow$dom$add_event_listeners(el,events){
var seq__33449 = cljs.core.seq(events);
var chunk__33450 = null;
var count__33451 = (0);
var i__33452 = (0);
while(true){
if((i__33452 < count__33451)){
var vec__33466 = chunk__33450.cljs$core$IIndexed$_nth$arity$2(null,i__33452);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33466,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33466,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__34005 = seq__33449;
var G__34006 = chunk__33450;
var G__34007 = count__33451;
var G__34008 = (i__33452 + (1));
seq__33449 = G__34005;
chunk__33450 = G__34006;
count__33451 = G__34007;
i__33452 = G__34008;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq(seq__33449);
if(temp__5720__auto__){
var seq__33449__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__33449__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__33449__$1);
var G__34009 = cljs.core.chunk_rest(seq__33449__$1);
var G__34010 = c__4550__auto__;
var G__34011 = cljs.core.count(c__4550__auto__);
var G__34012 = (0);
seq__33449 = G__34009;
chunk__33450 = G__34010;
count__33451 = G__34011;
i__33452 = G__34012;
continue;
} else {
var vec__33470 = cljs.core.first(seq__33449__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33470,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33470,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__34013 = cljs.core.next(seq__33449__$1);
var G__34014 = null;
var G__34015 = (0);
var G__34016 = (0);
seq__33449 = G__34013;
chunk__33450 = G__34014;
count__33451 = G__34015;
i__33452 = G__34016;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_style = (function shadow$dom$set_style(el,styles){
var dom = shadow.dom.dom_node(el);
var seq__33476 = cljs.core.seq(styles);
var chunk__33477 = null;
var count__33478 = (0);
var i__33479 = (0);
while(true){
if((i__33479 < count__33478)){
var vec__33496 = chunk__33477.cljs$core$IIndexed$_nth$arity$2(null,i__33479);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33496,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33496,(1),null);
var G__33499_34017 = dom;
var G__33500_34018 = cljs.core.name(k);
var G__33501_34019 = (((v == null))?"":v);
goog.style.setStyle(G__33499_34017,G__33500_34018,G__33501_34019);


var G__34020 = seq__33476;
var G__34021 = chunk__33477;
var G__34022 = count__33478;
var G__34023 = (i__33479 + (1));
seq__33476 = G__34020;
chunk__33477 = G__34021;
count__33478 = G__34022;
i__33479 = G__34023;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq(seq__33476);
if(temp__5720__auto__){
var seq__33476__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__33476__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__33476__$1);
var G__34024 = cljs.core.chunk_rest(seq__33476__$1);
var G__34025 = c__4550__auto__;
var G__34026 = cljs.core.count(c__4550__auto__);
var G__34027 = (0);
seq__33476 = G__34024;
chunk__33477 = G__34025;
count__33478 = G__34026;
i__33479 = G__34027;
continue;
} else {
var vec__33505 = cljs.core.first(seq__33476__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33505,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33505,(1),null);
var G__33509_34028 = dom;
var G__33510_34029 = cljs.core.name(k);
var G__33511_34030 = (((v == null))?"":v);
goog.style.setStyle(G__33509_34028,G__33510_34029,G__33511_34030);


var G__34032 = cljs.core.next(seq__33476__$1);
var G__34033 = null;
var G__34034 = (0);
var G__34035 = (0);
seq__33476 = G__34032;
chunk__33477 = G__34033;
count__33478 = G__34034;
i__33479 = G__34035;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_attr_STAR_ = (function shadow$dom$set_attr_STAR_(el,key,value){
var G__33515_34037 = key;
var G__33515_34038__$1 = (((G__33515_34037 instanceof cljs.core.Keyword))?G__33515_34037.fqn:null);
switch (G__33515_34038__$1) {
case "id":
el.id = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value);

break;
case "class":
el.className = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value);

break;
case "for":
el.htmlFor = value;

break;
case "cellpadding":
el.setAttribute("cellPadding",value);

break;
case "cellspacing":
el.setAttribute("cellSpacing",value);

break;
case "colspan":
el.setAttribute("colSpan",value);

break;
case "frameborder":
el.setAttribute("frameBorder",value);

break;
case "height":
el.setAttribute("height",value);

break;
case "maxlength":
el.setAttribute("maxLength",value);

break;
case "role":
el.setAttribute("role",value);

break;
case "rowspan":
el.setAttribute("rowSpan",value);

break;
case "type":
el.setAttribute("type",value);

break;
case "usemap":
el.setAttribute("useMap",value);

break;
case "valign":
el.setAttribute("vAlign",value);

break;
case "width":
el.setAttribute("width",value);

break;
case "on":
shadow.dom.add_event_listeners(el,value);

break;
case "style":
if((value == null)){
} else {
if(typeof value === 'string'){
el.setAttribute("style",value);
} else {
if(cljs.core.map_QMARK_(value)){
shadow.dom.set_style(el,value);
} else {
goog.style.setStyle(el,value);

}
}
}

break;
default:
var ks_34041 = cljs.core.name(key);
if(cljs.core.truth_((function (){var or__4131__auto__ = goog.string.startsWith(ks_34041,"data-");
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return goog.string.startsWith(ks_34041,"aria-");
}
})())){
el.setAttribute(ks_34041,value);
} else {
(el[ks_34041] = value);
}

}

return el;
});
shadow.dom.set_attrs = (function shadow$dom$set_attrs(el,attrs){
return cljs.core.reduce_kv((function (el__$1,key,value){
shadow.dom.set_attr_STAR_(el__$1,key,value);

return el__$1;
}),shadow.dom.dom_node(el),attrs);
});
shadow.dom.set_attr = (function shadow$dom$set_attr(el,key,value){
return shadow.dom.set_attr_STAR_(shadow.dom.dom_node(el),key,value);
});
shadow.dom.has_class_QMARK_ = (function shadow$dom$has_class_QMARK_(el,cls){
var G__33521 = shadow.dom.dom_node(el);
var G__33522 = cls;
return goog.dom.classlist.contains(G__33521,G__33522);
});
shadow.dom.merge_class_string = (function shadow$dom$merge_class_string(current,extra_class){
if(cljs.core.seq(current)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(current)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(extra_class)].join('');
} else {
return extra_class;
}
});
shadow.dom.parse_tag = (function shadow$dom$parse_tag(spec){
var spec__$1 = cljs.core.name(spec);
var fdot = spec__$1.indexOf(".");
var fhash = spec__$1.indexOf("#");
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1,null,null], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fdot),null,clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1))),null], null);
} else {
if((fhash > fdot)){
throw ["cant have id after class?",spec__$1].join('');
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1)),fdot),clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);

}
}
}
}
});
shadow.dom.create_dom_node = (function shadow$dom$create_dom_node(tag_def,p__33528){
var map__33529 = p__33528;
var map__33529__$1 = (((((!((map__33529 == null))))?(((((map__33529.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33529.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33529):map__33529);
var props = map__33529__$1;
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33529__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var tag_props = ({});
var vec__33534 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33534,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33534,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33534,(2),null);
if(cljs.core.truth_(tag_id)){
(tag_props["id"] = tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
(tag_props["class"] = shadow.dom.merge_class_string(class$,tag_classes));
} else {
}

var G__33538 = goog.dom.createDom(tag_name,tag_props);
shadow.dom.set_attrs(G__33538,cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(props,new cljs.core.Keyword(null,"class","class",-2030961996)));

return G__33538;
});
shadow.dom.append = (function shadow$dom$append(var_args){
var G__33545 = arguments.length;
switch (G__33545) {
case 1:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.append.cljs$core$IFn$_invoke$arity$1 = (function (node){
if(cljs.core.truth_(node)){
var temp__5720__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5720__auto__)){
var n = temp__5720__auto__;
document.body.appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
});

shadow.dom.append.cljs$core$IFn$_invoke$arity$2 = (function (el,node){
if(cljs.core.truth_(node)){
var temp__5720__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5720__auto__)){
var n = temp__5720__auto__;
shadow.dom.dom_node(el).appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
});

shadow.dom.append.cljs$lang$maxFixedArity = 2;

shadow.dom.destructure_node = (function shadow$dom$destructure_node(create_fn,p__33550){
var vec__33552 = p__33550;
var seq__33553 = cljs.core.seq(vec__33552);
var first__33554 = cljs.core.first(seq__33553);
var seq__33553__$1 = cljs.core.next(seq__33553);
var nn = first__33554;
var first__33554__$1 = cljs.core.first(seq__33553__$1);
var seq__33553__$2 = cljs.core.next(seq__33553__$1);
var np = first__33554__$1;
var nc = seq__33553__$2;
var node = vec__33552;
if((nn instanceof cljs.core.Keyword)){
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("invalid dom node",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"node","node",581201198),node], null));
}

if((((np == null)) && ((nc == null)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__33555 = nn;
var G__33556 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__33555,G__33556) : create_fn.call(null,G__33555,G__33556));
})(),cljs.core.List.EMPTY], null);
} else {
if(cljs.core.map_QMARK_(np)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(nn,np) : create_fn.call(null,nn,np)),nc], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__33557 = nn;
var G__33558 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__33557,G__33558) : create_fn.call(null,G__33557,G__33558));
})(),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(nc,np)], null);

}
}
});
shadow.dom.make_dom_node = (function shadow$dom$make_dom_node(structure){
var vec__33559 = shadow.dom.destructure_node(shadow.dom.create_dom_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33559,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33559,(1),null);
var seq__33562_34061 = cljs.core.seq(node_children);
var chunk__33563_34062 = null;
var count__33564_34063 = (0);
var i__33565_34064 = (0);
while(true){
if((i__33565_34064 < count__33564_34063)){
var child_struct_34066 = chunk__33563_34062.cljs$core$IIndexed$_nth$arity$2(null,i__33565_34064);
var children_34068 = shadow.dom.dom_node(child_struct_34066);
if(cljs.core.seq_QMARK_(children_34068)){
var seq__33584_34069 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_34068));
var chunk__33586_34070 = null;
var count__33587_34071 = (0);
var i__33588_34072 = (0);
while(true){
if((i__33588_34072 < count__33587_34071)){
var child_34073 = chunk__33586_34070.cljs$core$IIndexed$_nth$arity$2(null,i__33588_34072);
if(cljs.core.truth_(child_34073)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_34073);


var G__34074 = seq__33584_34069;
var G__34075 = chunk__33586_34070;
var G__34076 = count__33587_34071;
var G__34077 = (i__33588_34072 + (1));
seq__33584_34069 = G__34074;
chunk__33586_34070 = G__34075;
count__33587_34071 = G__34076;
i__33588_34072 = G__34077;
continue;
} else {
var G__34078 = seq__33584_34069;
var G__34079 = chunk__33586_34070;
var G__34080 = count__33587_34071;
var G__34081 = (i__33588_34072 + (1));
seq__33584_34069 = G__34078;
chunk__33586_34070 = G__34079;
count__33587_34071 = G__34080;
i__33588_34072 = G__34081;
continue;
}
} else {
var temp__5720__auto___34084 = cljs.core.seq(seq__33584_34069);
if(temp__5720__auto___34084){
var seq__33584_34085__$1 = temp__5720__auto___34084;
if(cljs.core.chunked_seq_QMARK_(seq__33584_34085__$1)){
var c__4550__auto___34086 = cljs.core.chunk_first(seq__33584_34085__$1);
var G__34087 = cljs.core.chunk_rest(seq__33584_34085__$1);
var G__34088 = c__4550__auto___34086;
var G__34089 = cljs.core.count(c__4550__auto___34086);
var G__34090 = (0);
seq__33584_34069 = G__34087;
chunk__33586_34070 = G__34088;
count__33587_34071 = G__34089;
i__33588_34072 = G__34090;
continue;
} else {
var child_34093 = cljs.core.first(seq__33584_34085__$1);
if(cljs.core.truth_(child_34093)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_34093);


var G__34095 = cljs.core.next(seq__33584_34085__$1);
var G__34096 = null;
var G__34097 = (0);
var G__34098 = (0);
seq__33584_34069 = G__34095;
chunk__33586_34070 = G__34096;
count__33587_34071 = G__34097;
i__33588_34072 = G__34098;
continue;
} else {
var G__34099 = cljs.core.next(seq__33584_34085__$1);
var G__34100 = null;
var G__34101 = (0);
var G__34102 = (0);
seq__33584_34069 = G__34099;
chunk__33586_34070 = G__34100;
count__33587_34071 = G__34101;
i__33588_34072 = G__34102;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_34068);
}


var G__34104 = seq__33562_34061;
var G__34105 = chunk__33563_34062;
var G__34106 = count__33564_34063;
var G__34107 = (i__33565_34064 + (1));
seq__33562_34061 = G__34104;
chunk__33563_34062 = G__34105;
count__33564_34063 = G__34106;
i__33565_34064 = G__34107;
continue;
} else {
var temp__5720__auto___34108 = cljs.core.seq(seq__33562_34061);
if(temp__5720__auto___34108){
var seq__33562_34109__$1 = temp__5720__auto___34108;
if(cljs.core.chunked_seq_QMARK_(seq__33562_34109__$1)){
var c__4550__auto___34110 = cljs.core.chunk_first(seq__33562_34109__$1);
var G__34111 = cljs.core.chunk_rest(seq__33562_34109__$1);
var G__34112 = c__4550__auto___34110;
var G__34113 = cljs.core.count(c__4550__auto___34110);
var G__34114 = (0);
seq__33562_34061 = G__34111;
chunk__33563_34062 = G__34112;
count__33564_34063 = G__34113;
i__33565_34064 = G__34114;
continue;
} else {
var child_struct_34115 = cljs.core.first(seq__33562_34109__$1);
var children_34116 = shadow.dom.dom_node(child_struct_34115);
if(cljs.core.seq_QMARK_(children_34116)){
var seq__33597_34117 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_34116));
var chunk__33599_34118 = null;
var count__33600_34119 = (0);
var i__33601_34120 = (0);
while(true){
if((i__33601_34120 < count__33600_34119)){
var child_34121 = chunk__33599_34118.cljs$core$IIndexed$_nth$arity$2(null,i__33601_34120);
if(cljs.core.truth_(child_34121)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_34121);


var G__34123 = seq__33597_34117;
var G__34124 = chunk__33599_34118;
var G__34125 = count__33600_34119;
var G__34126 = (i__33601_34120 + (1));
seq__33597_34117 = G__34123;
chunk__33599_34118 = G__34124;
count__33600_34119 = G__34125;
i__33601_34120 = G__34126;
continue;
} else {
var G__34127 = seq__33597_34117;
var G__34128 = chunk__33599_34118;
var G__34129 = count__33600_34119;
var G__34130 = (i__33601_34120 + (1));
seq__33597_34117 = G__34127;
chunk__33599_34118 = G__34128;
count__33600_34119 = G__34129;
i__33601_34120 = G__34130;
continue;
}
} else {
var temp__5720__auto___34131__$1 = cljs.core.seq(seq__33597_34117);
if(temp__5720__auto___34131__$1){
var seq__33597_34132__$1 = temp__5720__auto___34131__$1;
if(cljs.core.chunked_seq_QMARK_(seq__33597_34132__$1)){
var c__4550__auto___34133 = cljs.core.chunk_first(seq__33597_34132__$1);
var G__34134 = cljs.core.chunk_rest(seq__33597_34132__$1);
var G__34135 = c__4550__auto___34133;
var G__34136 = cljs.core.count(c__4550__auto___34133);
var G__34137 = (0);
seq__33597_34117 = G__34134;
chunk__33599_34118 = G__34135;
count__33600_34119 = G__34136;
i__33601_34120 = G__34137;
continue;
} else {
var child_34138 = cljs.core.first(seq__33597_34132__$1);
if(cljs.core.truth_(child_34138)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_34138);


var G__34140 = cljs.core.next(seq__33597_34132__$1);
var G__34141 = null;
var G__34142 = (0);
var G__34143 = (0);
seq__33597_34117 = G__34140;
chunk__33599_34118 = G__34141;
count__33600_34119 = G__34142;
i__33601_34120 = G__34143;
continue;
} else {
var G__34145 = cljs.core.next(seq__33597_34132__$1);
var G__34146 = null;
var G__34147 = (0);
var G__34148 = (0);
seq__33597_34117 = G__34145;
chunk__33599_34118 = G__34146;
count__33600_34119 = G__34147;
i__33601_34120 = G__34148;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_34116);
}


var G__34149 = cljs.core.next(seq__33562_34109__$1);
var G__34150 = null;
var G__34151 = (0);
var G__34152 = (0);
seq__33562_34061 = G__34149;
chunk__33563_34062 = G__34150;
count__33564_34063 = G__34151;
i__33565_34064 = G__34152;
continue;
}
} else {
}
}
break;
}

return node;
});
cljs.core.Keyword.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.Keyword.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$__$1], null));
});

cljs.core.PersistentVector.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.PersistentVector.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(this$__$1);
});

cljs.core.LazySeq.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.LazySeq.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_dom,this$__$1);
});
if(cljs.core.truth_(((typeof HTMLElement) != 'undefined'))){
HTMLElement.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

HTMLElement.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
});
} else {
}
if(cljs.core.truth_(((typeof DocumentFragment) != 'undefined'))){
DocumentFragment.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

DocumentFragment.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
});
} else {
}
/**
 * clear node children
 */
shadow.dom.reset = (function shadow$dom$reset(node){
var G__33614 = shadow.dom.dom_node(node);
return goog.dom.removeChildren(G__33614);
});
shadow.dom.remove = (function shadow$dom$remove(node){
if((((!((node == null))))?(((((node.cljs$lang$protocol_mask$partition0$ & (8388608))) || ((cljs.core.PROTOCOL_SENTINEL === node.cljs$core$ISeqable$))))?true:false):false)){
var seq__33617 = cljs.core.seq(node);
var chunk__33618 = null;
var count__33619 = (0);
var i__33620 = (0);
while(true){
if((i__33620 < count__33619)){
var n = chunk__33618.cljs$core$IIndexed$_nth$arity$2(null,i__33620);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__34166 = seq__33617;
var G__34167 = chunk__33618;
var G__34168 = count__33619;
var G__34169 = (i__33620 + (1));
seq__33617 = G__34166;
chunk__33618 = G__34167;
count__33619 = G__34168;
i__33620 = G__34169;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq(seq__33617);
if(temp__5720__auto__){
var seq__33617__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__33617__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__33617__$1);
var G__34170 = cljs.core.chunk_rest(seq__33617__$1);
var G__34171 = c__4550__auto__;
var G__34172 = cljs.core.count(c__4550__auto__);
var G__34173 = (0);
seq__33617 = G__34170;
chunk__33618 = G__34171;
count__33619 = G__34172;
i__33620 = G__34173;
continue;
} else {
var n = cljs.core.first(seq__33617__$1);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__34175 = cljs.core.next(seq__33617__$1);
var G__34176 = null;
var G__34177 = (0);
var G__34178 = (0);
seq__33617 = G__34175;
chunk__33618 = G__34176;
count__33619 = G__34177;
i__33620 = G__34178;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return goog.dom.removeNode(node);
}
});
shadow.dom.replace_node = (function shadow$dom$replace_node(old,new$){
var G__33628 = shadow.dom.dom_node(new$);
var G__33629 = shadow.dom.dom_node(old);
return goog.dom.replaceNode(G__33628,G__33629);
});
shadow.dom.text = (function shadow$dom$text(var_args){
var G__33634 = arguments.length;
switch (G__33634) {
case 2:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.text.cljs$core$IFn$_invoke$arity$2 = (function (el,new_text){
return shadow.dom.dom_node(el).innerText = new_text;
});

shadow.dom.text.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.dom_node(el).innerText;
});

shadow.dom.text.cljs$lang$maxFixedArity = 2;

shadow.dom.check = (function shadow$dom$check(var_args){
var G__33640 = arguments.length;
switch (G__33640) {
case 1:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.check.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2(el,true);
});

shadow.dom.check.cljs$core$IFn$_invoke$arity$2 = (function (el,checked){
return shadow.dom.dom_node(el).checked = checked;
});

shadow.dom.check.cljs$lang$maxFixedArity = 2;

shadow.dom.checked_QMARK_ = (function shadow$dom$checked_QMARK_(el){
return shadow.dom.dom_node(el).checked;
});
shadow.dom.form_elements = (function shadow$dom$form_elements(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).elements));
});
shadow.dom.children = (function shadow$dom$children(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).children));
});
shadow.dom.child_nodes = (function shadow$dom$child_nodes(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).childNodes));
});
shadow.dom.attr = (function shadow$dom$attr(var_args){
var G__33652 = arguments.length;
switch (G__33652) {
case 2:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.attr.cljs$core$IFn$_invoke$arity$2 = (function (el,key){
return shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
});

shadow.dom.attr.cljs$core$IFn$_invoke$arity$3 = (function (el,key,default$){
var or__4131__auto__ = shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return default$;
}
});

shadow.dom.attr.cljs$lang$maxFixedArity = 3;

shadow.dom.del_attr = (function shadow$dom$del_attr(el,key){
return shadow.dom.dom_node(el).removeAttribute(cljs.core.name(key));
});
shadow.dom.data = (function shadow$dom$data(el,key){
return shadow.dom.dom_node(el).getAttribute(["data-",cljs.core.name(key)].join(''));
});
shadow.dom.set_data = (function shadow$dom$set_data(el,key,value){
return shadow.dom.dom_node(el).setAttribute(["data-",cljs.core.name(key)].join(''),cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));
});
shadow.dom.set_html = (function shadow$dom$set_html(node,text){
return shadow.dom.dom_node(node).innerHTML = text;
});
shadow.dom.get_html = (function shadow$dom$get_html(node){
return shadow.dom.dom_node(node).innerHTML;
});
shadow.dom.fragment = (function shadow$dom$fragment(var_args){
var args__4736__auto__ = [];
var len__4730__auto___34201 = arguments.length;
var i__4731__auto___34202 = (0);
while(true){
if((i__4731__auto___34202 < len__4730__auto___34201)){
args__4736__auto__.push((arguments[i__4731__auto___34202]));

var G__34203 = (i__4731__auto___34202 + (1));
i__4731__auto___34202 = G__34203;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic = (function (nodes){
var fragment = document.createDocumentFragment();
var seq__33663_34204 = cljs.core.seq(nodes);
var chunk__33664_34205 = null;
var count__33665_34206 = (0);
var i__33666_34207 = (0);
while(true){
if((i__33666_34207 < count__33665_34206)){
var node_34208 = chunk__33664_34205.cljs$core$IIndexed$_nth$arity$2(null,i__33666_34207);
fragment.appendChild(shadow.dom._to_dom(node_34208));


var G__34209 = seq__33663_34204;
var G__34210 = chunk__33664_34205;
var G__34211 = count__33665_34206;
var G__34212 = (i__33666_34207 + (1));
seq__33663_34204 = G__34209;
chunk__33664_34205 = G__34210;
count__33665_34206 = G__34211;
i__33666_34207 = G__34212;
continue;
} else {
var temp__5720__auto___34213 = cljs.core.seq(seq__33663_34204);
if(temp__5720__auto___34213){
var seq__33663_34214__$1 = temp__5720__auto___34213;
if(cljs.core.chunked_seq_QMARK_(seq__33663_34214__$1)){
var c__4550__auto___34215 = cljs.core.chunk_first(seq__33663_34214__$1);
var G__34216 = cljs.core.chunk_rest(seq__33663_34214__$1);
var G__34217 = c__4550__auto___34215;
var G__34218 = cljs.core.count(c__4550__auto___34215);
var G__34219 = (0);
seq__33663_34204 = G__34216;
chunk__33664_34205 = G__34217;
count__33665_34206 = G__34218;
i__33666_34207 = G__34219;
continue;
} else {
var node_34220 = cljs.core.first(seq__33663_34214__$1);
fragment.appendChild(shadow.dom._to_dom(node_34220));


var G__34222 = cljs.core.next(seq__33663_34214__$1);
var G__34223 = null;
var G__34224 = (0);
var G__34225 = (0);
seq__33663_34204 = G__34222;
chunk__33664_34205 = G__34223;
count__33665_34206 = G__34224;
i__33666_34207 = G__34225;
continue;
}
} else {
}
}
break;
}

return (new shadow.dom.NativeColl(fragment));
});

shadow.dom.fragment.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
shadow.dom.fragment.cljs$lang$applyTo = (function (seq33662){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq33662));
});

/**
 * given a html string, eval all <script> tags and return the html without the scripts
 * don't do this for everything, only content you trust.
 */
shadow.dom.eval_scripts = (function shadow$dom$eval_scripts(s){
var scripts = cljs.core.re_seq(/<script[^>]*?>(.+?)<\/script>/,s);
var seq__33667_34227 = cljs.core.seq(scripts);
var chunk__33668_34228 = null;
var count__33669_34229 = (0);
var i__33670_34230 = (0);
while(true){
if((i__33670_34230 < count__33669_34229)){
var vec__33683_34234 = chunk__33668_34228.cljs$core$IIndexed$_nth$arity$2(null,i__33670_34230);
var script_tag_34235 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33683_34234,(0),null);
var script_body_34236 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33683_34234,(1),null);
eval(script_body_34236);


var G__34238 = seq__33667_34227;
var G__34239 = chunk__33668_34228;
var G__34240 = count__33669_34229;
var G__34241 = (i__33670_34230 + (1));
seq__33667_34227 = G__34238;
chunk__33668_34228 = G__34239;
count__33669_34229 = G__34240;
i__33670_34230 = G__34241;
continue;
} else {
var temp__5720__auto___34242 = cljs.core.seq(seq__33667_34227);
if(temp__5720__auto___34242){
var seq__33667_34243__$1 = temp__5720__auto___34242;
if(cljs.core.chunked_seq_QMARK_(seq__33667_34243__$1)){
var c__4550__auto___34245 = cljs.core.chunk_first(seq__33667_34243__$1);
var G__34246 = cljs.core.chunk_rest(seq__33667_34243__$1);
var G__34247 = c__4550__auto___34245;
var G__34248 = cljs.core.count(c__4550__auto___34245);
var G__34249 = (0);
seq__33667_34227 = G__34246;
chunk__33668_34228 = G__34247;
count__33669_34229 = G__34248;
i__33670_34230 = G__34249;
continue;
} else {
var vec__33686_34250 = cljs.core.first(seq__33667_34243__$1);
var script_tag_34251 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33686_34250,(0),null);
var script_body_34252 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33686_34250,(1),null);
eval(script_body_34252);


var G__34253 = cljs.core.next(seq__33667_34243__$1);
var G__34254 = null;
var G__34255 = (0);
var G__34256 = (0);
seq__33667_34227 = G__34253;
chunk__33668_34228 = G__34254;
count__33669_34229 = G__34255;
i__33670_34230 = G__34256;
continue;
}
} else {
}
}
break;
}

return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (scripts){
return (function (s__$1,p__33689){
var vec__33690 = p__33689;
var script_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33690,(0),null);
var script_body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33690,(1),null);
return clojure.string.replace(s__$1,script_tag,"");
});})(scripts))
,s,scripts);
});
shadow.dom.str__GT_fragment = (function shadow$dom$str__GT_fragment(s){
var el = document.createElement("div");
el.innerHTML = s;

return (new shadow.dom.NativeColl(goog.dom.childrenToNode_(document,el)));
});
shadow.dom.node_name = (function shadow$dom$node_name(el){
return shadow.dom.dom_node(el).nodeName;
});
shadow.dom.ancestor_by_class = (function shadow$dom$ancestor_by_class(el,cls){
var G__33693 = shadow.dom.dom_node(el);
var G__33694 = cls;
return goog.dom.getAncestorByClass(G__33693,G__33694);
});
shadow.dom.ancestor_by_tag = (function shadow$dom$ancestor_by_tag(var_args){
var G__33696 = arguments.length;
switch (G__33696) {
case 2:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2 = (function (el,tag){
var G__33697 = shadow.dom.dom_node(el);
var G__33698 = cljs.core.name(tag);
return goog.dom.getAncestorByTagNameAndClass(G__33697,G__33698);
});

shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3 = (function (el,tag,cls){
var G__33699 = shadow.dom.dom_node(el);
var G__33700 = cljs.core.name(tag);
var G__33701 = cljs.core.name(cls);
return goog.dom.getAncestorByTagNameAndClass(G__33699,G__33700,G__33701);
});

shadow.dom.ancestor_by_tag.cljs$lang$maxFixedArity = 3;

shadow.dom.get_value = (function shadow$dom$get_value(dom){
var G__33702 = shadow.dom.dom_node(dom);
return goog.dom.forms.getValue(G__33702);
});
shadow.dom.set_value = (function shadow$dom$set_value(dom,value){
var G__33703 = shadow.dom.dom_node(dom);
var G__33704 = value;
return goog.dom.forms.setValue(G__33703,G__33704);
});
shadow.dom.px = (function shadow$dom$px(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((value | (0))),"px"].join('');
});
shadow.dom.pct = (function shadow$dom$pct(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"%"].join('');
});
shadow.dom.remove_style_STAR_ = (function shadow$dom$remove_style_STAR_(el,style){
return el.style.removeProperty(cljs.core.name(style));
});
shadow.dom.remove_style = (function shadow$dom$remove_style(el,style){
var el__$1 = shadow.dom.dom_node(el);
return shadow.dom.remove_style_STAR_(el__$1,style);
});
shadow.dom.remove_styles = (function shadow$dom$remove_styles(el,style_keys){
var el__$1 = shadow.dom.dom_node(el);
var seq__33705 = cljs.core.seq(style_keys);
var chunk__33706 = null;
var count__33707 = (0);
var i__33708 = (0);
while(true){
if((i__33708 < count__33707)){
var it = chunk__33706.cljs$core$IIndexed$_nth$arity$2(null,i__33708);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__34265 = seq__33705;
var G__34266 = chunk__33706;
var G__34267 = count__33707;
var G__34268 = (i__33708 + (1));
seq__33705 = G__34265;
chunk__33706 = G__34266;
count__33707 = G__34267;
i__33708 = G__34268;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq(seq__33705);
if(temp__5720__auto__){
var seq__33705__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__33705__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__33705__$1);
var G__34270 = cljs.core.chunk_rest(seq__33705__$1);
var G__34271 = c__4550__auto__;
var G__34272 = cljs.core.count(c__4550__auto__);
var G__34273 = (0);
seq__33705 = G__34270;
chunk__33706 = G__34271;
count__33707 = G__34272;
i__33708 = G__34273;
continue;
} else {
var it = cljs.core.first(seq__33705__$1);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__34275 = cljs.core.next(seq__33705__$1);
var G__34276 = null;
var G__34277 = (0);
var G__34278 = (0);
seq__33705 = G__34275;
chunk__33706 = G__34276;
count__33707 = G__34277;
i__33708 = G__34278;
continue;
}
} else {
return null;
}
}
break;
}
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Coordinate = (function (x,y,__meta,__extmap,__hash){
this.x = x;
this.y = y;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4385__auto__,k__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
return this__4385__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4386__auto__,null);
});

shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4387__auto__,k33711,else__4388__auto__){
var self__ = this;
var this__4387__auto____$1 = this;
var G__33718 = k33711;
var G__33718__$1 = (((G__33718 instanceof cljs.core.Keyword))?G__33718.fqn:null);
switch (G__33718__$1) {
case "x":
return self__.x;

break;
case "y":
return self__.y;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k33711,else__4388__auto__);

}
});

shadow.dom.Coordinate.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4404__auto__,f__4405__auto__,init__4406__auto__){
var self__ = this;
var this__4404__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (this__4404__auto____$1){
return (function (ret__4407__auto__,p__33719){
var vec__33720 = p__33719;
var k__4408__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33720,(0),null);
var v__4409__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33720,(1),null);
return (f__4405__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4405__auto__.cljs$core$IFn$_invoke$arity$3(ret__4407__auto__,k__4408__auto__,v__4409__auto__) : f__4405__auto__.call(null,ret__4407__auto__,k__4408__auto__,v__4409__auto__));
});})(this__4404__auto____$1))
,init__4406__auto__,this__4404__auto____$1);
});

shadow.dom.Coordinate.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4399__auto__,writer__4400__auto__,opts__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
var pr_pair__4402__auto__ = ((function (this__4399__auto____$1){
return (function (keyval__4403__auto__){
return cljs.core.pr_sequential_writer(writer__4400__auto__,cljs.core.pr_writer,""," ","",opts__4401__auto__,keyval__4403__auto__);
});})(this__4399__auto____$1))
;
return cljs.core.pr_sequential_writer(writer__4400__auto__,pr_pair__4402__auto__,"#shadow.dom.Coordinate{",", ","}",opts__4401__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"x","x",2099068185),self__.x],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"y","y",-1757859776),self__.y],null))], null),self__.__extmap));
});

shadow.dom.Coordinate.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__33710){
var self__ = this;
var G__33710__$1 = this;
return (new cljs.core.RecordIter((0),G__33710__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"y","y",-1757859776)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
});

shadow.dom.Coordinate.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4383__auto__){
var self__ = this;
var this__4383__auto____$1 = this;
return self__.__meta;
});

shadow.dom.Coordinate.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4380__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,self__.__extmap,self__.__hash));
});

shadow.dom.Coordinate.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4389__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
});

shadow.dom.Coordinate.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4381__auto__){
var self__ = this;
var this__4381__auto____$1 = this;
var h__4243__auto__ = self__.__hash;
if((!((h__4243__auto__ == null)))){
return h__4243__auto__;
} else {
var h__4243__auto____$1 = (function (){var fexpr__33731 = ((function (h__4243__auto__,this__4381__auto____$1){
return (function (coll__4382__auto__){
return (145542109 ^ cljs.core.hash_unordered_coll(coll__4382__auto__));
});})(h__4243__auto__,this__4381__auto____$1))
;
return fexpr__33731(this__4381__auto____$1);
})();
self__.__hash = h__4243__auto____$1;

return h__4243__auto____$1;
}
});

shadow.dom.Coordinate.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this33712,other33713){
var self__ = this;
var this33712__$1 = this;
return (((!((other33713 == null)))) && ((this33712__$1.constructor === other33713.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this33712__$1.x,other33713.x)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this33712__$1.y,other33713.y)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this33712__$1.__extmap,other33713.__extmap)));
});

shadow.dom.Coordinate.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4394__auto__,k__4395__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"y","y",-1757859776),null,new cljs.core.Keyword(null,"x","x",2099068185),null], null), null),k__4395__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4394__auto____$1),self__.__meta),k__4395__auto__);
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4395__auto__)),null));
}
});

shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4392__auto__,k__4393__auto__,G__33710){
var self__ = this;
var this__4392__auto____$1 = this;
var pred__33737 = cljs.core.keyword_identical_QMARK_;
var expr__33738 = k__4393__auto__;
if(cljs.core.truth_((function (){var G__33740 = new cljs.core.Keyword(null,"x","x",2099068185);
var G__33741 = expr__33738;
return (pred__33737.cljs$core$IFn$_invoke$arity$2 ? pred__33737.cljs$core$IFn$_invoke$arity$2(G__33740,G__33741) : pred__33737.call(null,G__33740,G__33741));
})())){
return (new shadow.dom.Coordinate(G__33710,self__.y,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((function (){var G__33742 = new cljs.core.Keyword(null,"y","y",-1757859776);
var G__33743 = expr__33738;
return (pred__33737.cljs$core$IFn$_invoke$arity$2 ? pred__33737.cljs$core$IFn$_invoke$arity$2(G__33742,G__33743) : pred__33737.call(null,G__33742,G__33743));
})())){
return (new shadow.dom.Coordinate(self__.x,G__33710,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4393__auto__,G__33710),null));
}
}
});

shadow.dom.Coordinate.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4397__auto__){
var self__ = this;
var this__4397__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"x","x",2099068185),self__.x,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"y","y",-1757859776),self__.y,null))], null),self__.__extmap));
});

shadow.dom.Coordinate.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4384__auto__,G__33710){
var self__ = this;
var this__4384__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,G__33710,self__.__extmap,self__.__hash));
});

shadow.dom.Coordinate.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4390__auto__,entry__4391__auto__){
var self__ = this;
var this__4390__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4391__auto__)){
return this__4390__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(0)),cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4390__auto____$1,entry__4391__auto__);
}
});

shadow.dom.Coordinate.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"x","x",-555367584,null),new cljs.core.Symbol(null,"y","y",-117328249,null)], null);
});

shadow.dom.Coordinate.cljs$lang$type = true;

shadow.dom.Coordinate.cljs$lang$ctorPrSeq = (function (this__4428__auto__){
return (new cljs.core.List(null,"shadow.dom/Coordinate",null,(1),null));
});

shadow.dom.Coordinate.cljs$lang$ctorPrWriter = (function (this__4428__auto__,writer__4429__auto__){
return cljs.core._write(writer__4429__auto__,"shadow.dom/Coordinate");
});

/**
 * Positional factory function for shadow.dom/Coordinate.
 */
shadow.dom.__GT_Coordinate = (function shadow$dom$__GT_Coordinate(x,y){
return (new shadow.dom.Coordinate(x,y,null,null,null));
});

/**
 * Factory function for shadow.dom/Coordinate, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Coordinate = (function shadow$dom$map__GT_Coordinate(G__33715){
var extmap__4424__auto__ = (function (){var G__33752 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__33715,new cljs.core.Keyword(null,"x","x",2099068185),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776)], 0));
if(cljs.core.record_QMARK_(G__33715)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__33752);
} else {
return G__33752;
}
})();
return (new shadow.dom.Coordinate(new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(G__33715),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(G__33715),null,cljs.core.not_empty(extmap__4424__auto__),null));
});

shadow.dom.get_position = (function shadow$dom$get_position(el){
var pos = (function (){var G__33753 = shadow.dom.dom_node(el);
return goog.style.getPosition(G__33753);
})();
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_client_position = (function shadow$dom$get_client_position(el){
var pos = (function (){var G__33756 = shadow.dom.dom_node(el);
return goog.style.getClientPosition(G__33756);
})();
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_page_offset = (function shadow$dom$get_page_offset(el){
var pos = (function (){var G__33761 = shadow.dom.dom_node(el);
return goog.style.getPageOffset(G__33761);
})();
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Size = (function (w,h,__meta,__extmap,__hash){
this.w = w;
this.h = h;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4385__auto__,k__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
return this__4385__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4386__auto__,null);
});

shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4387__auto__,k33764,else__4388__auto__){
var self__ = this;
var this__4387__auto____$1 = this;
var G__33768 = k33764;
var G__33768__$1 = (((G__33768 instanceof cljs.core.Keyword))?G__33768.fqn:null);
switch (G__33768__$1) {
case "w":
return self__.w;

break;
case "h":
return self__.h;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k33764,else__4388__auto__);

}
});

shadow.dom.Size.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4404__auto__,f__4405__auto__,init__4406__auto__){
var self__ = this;
var this__4404__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (this__4404__auto____$1){
return (function (ret__4407__auto__,p__33769){
var vec__33770 = p__33769;
var k__4408__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33770,(0),null);
var v__4409__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33770,(1),null);
return (f__4405__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4405__auto__.cljs$core$IFn$_invoke$arity$3(ret__4407__auto__,k__4408__auto__,v__4409__auto__) : f__4405__auto__.call(null,ret__4407__auto__,k__4408__auto__,v__4409__auto__));
});})(this__4404__auto____$1))
,init__4406__auto__,this__4404__auto____$1);
});

shadow.dom.Size.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4399__auto__,writer__4400__auto__,opts__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
var pr_pair__4402__auto__ = ((function (this__4399__auto____$1){
return (function (keyval__4403__auto__){
return cljs.core.pr_sequential_writer(writer__4400__auto__,cljs.core.pr_writer,""," ","",opts__4401__auto__,keyval__4403__auto__);
});})(this__4399__auto____$1))
;
return cljs.core.pr_sequential_writer(writer__4400__auto__,pr_pair__4402__auto__,"#shadow.dom.Size{",", ","}",opts__4401__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"w","w",354169001),self__.w],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"h","h",1109658740),self__.h],null))], null),self__.__extmap));
});

shadow.dom.Size.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__33763){
var self__ = this;
var G__33763__$1 = this;
return (new cljs.core.RecordIter((0),G__33763__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"w","w",354169001),new cljs.core.Keyword(null,"h","h",1109658740)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
});

shadow.dom.Size.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4383__auto__){
var self__ = this;
var this__4383__auto____$1 = this;
return self__.__meta;
});

shadow.dom.Size.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4380__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,self__.__extmap,self__.__hash));
});

shadow.dom.Size.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4389__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
});

shadow.dom.Size.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4381__auto__){
var self__ = this;
var this__4381__auto____$1 = this;
var h__4243__auto__ = self__.__hash;
if((!((h__4243__auto__ == null)))){
return h__4243__auto__;
} else {
var h__4243__auto____$1 = (function (){var fexpr__33776 = ((function (h__4243__auto__,this__4381__auto____$1){
return (function (coll__4382__auto__){
return (-1228019642 ^ cljs.core.hash_unordered_coll(coll__4382__auto__));
});})(h__4243__auto__,this__4381__auto____$1))
;
return fexpr__33776(this__4381__auto____$1);
})();
self__.__hash = h__4243__auto____$1;

return h__4243__auto____$1;
}
});

shadow.dom.Size.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this33765,other33766){
var self__ = this;
var this33765__$1 = this;
return (((!((other33766 == null)))) && ((this33765__$1.constructor === other33766.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this33765__$1.w,other33766.w)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this33765__$1.h,other33766.h)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this33765__$1.__extmap,other33766.__extmap)));
});

shadow.dom.Size.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4394__auto__,k__4395__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"w","w",354169001),null,new cljs.core.Keyword(null,"h","h",1109658740),null], null), null),k__4395__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4394__auto____$1),self__.__meta),k__4395__auto__);
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4395__auto__)),null));
}
});

shadow.dom.Size.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4392__auto__,k__4393__auto__,G__33763){
var self__ = this;
var this__4392__auto____$1 = this;
var pred__33782 = cljs.core.keyword_identical_QMARK_;
var expr__33783 = k__4393__auto__;
if(cljs.core.truth_((function (){var G__33785 = new cljs.core.Keyword(null,"w","w",354169001);
var G__33786 = expr__33783;
return (pred__33782.cljs$core$IFn$_invoke$arity$2 ? pred__33782.cljs$core$IFn$_invoke$arity$2(G__33785,G__33786) : pred__33782.call(null,G__33785,G__33786));
})())){
return (new shadow.dom.Size(G__33763,self__.h,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((function (){var G__33788 = new cljs.core.Keyword(null,"h","h",1109658740);
var G__33789 = expr__33783;
return (pred__33782.cljs$core$IFn$_invoke$arity$2 ? pred__33782.cljs$core$IFn$_invoke$arity$2(G__33788,G__33789) : pred__33782.call(null,G__33788,G__33789));
})())){
return (new shadow.dom.Size(self__.w,G__33763,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4393__auto__,G__33763),null));
}
}
});

shadow.dom.Size.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4397__auto__){
var self__ = this;
var this__4397__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"w","w",354169001),self__.w,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"h","h",1109658740),self__.h,null))], null),self__.__extmap));
});

shadow.dom.Size.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4384__auto__,G__33763){
var self__ = this;
var this__4384__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,G__33763,self__.__extmap,self__.__hash));
});

shadow.dom.Size.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4390__auto__,entry__4391__auto__){
var self__ = this;
var this__4390__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4391__auto__)){
return this__4390__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(0)),cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4390__auto____$1,entry__4391__auto__);
}
});

shadow.dom.Size.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"w","w",1994700528,null),new cljs.core.Symbol(null,"h","h",-1544777029,null)], null);
});

shadow.dom.Size.cljs$lang$type = true;

shadow.dom.Size.cljs$lang$ctorPrSeq = (function (this__4428__auto__){
return (new cljs.core.List(null,"shadow.dom/Size",null,(1),null));
});

shadow.dom.Size.cljs$lang$ctorPrWriter = (function (this__4428__auto__,writer__4429__auto__){
return cljs.core._write(writer__4429__auto__,"shadow.dom/Size");
});

/**
 * Positional factory function for shadow.dom/Size.
 */
shadow.dom.__GT_Size = (function shadow$dom$__GT_Size(w,h){
return (new shadow.dom.Size(w,h,null,null,null));
});

/**
 * Factory function for shadow.dom/Size, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Size = (function shadow$dom$map__GT_Size(G__33767){
var extmap__4424__auto__ = (function (){var G__33796 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__33767,new cljs.core.Keyword(null,"w","w",354169001),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"h","h",1109658740)], 0));
if(cljs.core.record_QMARK_(G__33767)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__33796);
} else {
return G__33796;
}
})();
return (new shadow.dom.Size(new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(G__33767),new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(G__33767),null,cljs.core.not_empty(extmap__4424__auto__),null));
});

shadow.dom.size__GT_clj = (function shadow$dom$size__GT_clj(size){
return (new shadow.dom.Size(size.width,size.height,null,null,null));
});
shadow.dom.get_size = (function shadow$dom$get_size(el){
return shadow.dom.size__GT_clj((function (){var G__33798 = shadow.dom.dom_node(el);
return goog.style.getSize(G__33798);
})());
});
shadow.dom.get_height = (function shadow$dom$get_height(el){
return new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(shadow.dom.get_size(el));
});
shadow.dom.get_viewport_size = (function shadow$dom$get_viewport_size(){
return shadow.dom.size__GT_clj(goog.dom.getViewportSize());
});
shadow.dom.first_child = (function shadow$dom$first_child(el){
return (shadow.dom.dom_node(el).children[(0)]);
});
shadow.dom.select_option_values = (function shadow$dom$select_option_values(el){
var native$ = shadow.dom.dom_node(el);
var opts = (native$["options"]);
var a__4604__auto__ = opts;
var l__4605__auto__ = a__4604__auto__.length;
var i = (0);
var ret = cljs.core.PersistentVector.EMPTY;
while(true){
if((i < l__4605__auto__)){
var G__34345 = (i + (1));
var G__34346 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,(opts[i]["value"]));
i = G__34345;
ret = G__34346;
continue;
} else {
return ret;
}
break;
}
});
shadow.dom.build_url = (function shadow$dom$build_url(path,query_params){
if(cljs.core.empty_QMARK_(query_params)){
return path;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(path),"?",cljs.core.str.cljs$core$IFn$_invoke$arity$1(clojure.string.join.cljs$core$IFn$_invoke$arity$2("&",cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__33805){
var vec__33806 = p__33805;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33806,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33806,(1),null);
return [cljs.core.name(k),"=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(encodeURIComponent(cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)))].join('');
}),query_params)))].join('');
}
});
shadow.dom.redirect = (function shadow$dom$redirect(var_args){
var G__33810 = arguments.length;
switch (G__33810) {
case 1:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1 = (function (path){
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2(path,cljs.core.PersistentArrayMap.EMPTY);
});

shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2 = (function (path,query_params){
return (document["location"]["href"] = shadow.dom.build_url(path,query_params));
});

shadow.dom.redirect.cljs$lang$maxFixedArity = 2;

shadow.dom.reload_BANG_ = (function shadow$dom$reload_BANG_(){
return document.location.href = document.location.href;
});
shadow.dom.tag_name = (function shadow$dom$tag_name(el){
var dom = shadow.dom.dom_node(el);
return dom.tagName;
});
shadow.dom.insert_after = (function shadow$dom$insert_after(ref,new$){
var new_node = shadow.dom.dom_node(new$);
var G__33812_34350 = new_node;
var G__33813_34351 = shadow.dom.dom_node(ref);
goog.dom.insertSiblingAfter(G__33812_34350,G__33813_34351);

return new_node;
});
shadow.dom.insert_before = (function shadow$dom$insert_before(ref,new$){
var new_node = shadow.dom.dom_node(new$);
var G__33814_34354 = new_node;
var G__33815_34355 = shadow.dom.dom_node(ref);
goog.dom.insertSiblingBefore(G__33814_34354,G__33815_34355);

return new_node;
});
shadow.dom.insert_first = (function shadow$dom$insert_first(ref,new$){
var temp__5718__auto__ = shadow.dom.dom_node(ref).firstChild;
if(cljs.core.truth_(temp__5718__auto__)){
var child = temp__5718__auto__;
return shadow.dom.insert_before(child,new$);
} else {
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2(ref,new$);
}
});
shadow.dom.index_of = (function shadow$dom$index_of(el){
var el__$1 = shadow.dom.dom_node(el);
var i = (0);
while(true){
var ps = el__$1.previousSibling;
if((ps == null)){
return i;
} else {
var G__34358 = ps;
var G__34359 = (i + (1));
el__$1 = G__34358;
i = G__34359;
continue;
}
break;
}
});
shadow.dom.get_parent = (function shadow$dom$get_parent(el){
var G__33816 = shadow.dom.dom_node(el);
return goog.dom.getParentElement(G__33816);
});
shadow.dom.parents = (function shadow$dom$parents(el){
var parent = shadow.dom.get_parent(el);
if(cljs.core.truth_(parent)){
return cljs.core.cons(parent,(new cljs.core.LazySeq(null,((function (parent){
return (function (){
return (shadow.dom.parents.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.parents.cljs$core$IFn$_invoke$arity$1(parent) : shadow.dom.parents.call(null,parent));
});})(parent))
,null,null)));
} else {
return null;
}
});
shadow.dom.matches = (function shadow$dom$matches(el,sel){
return shadow.dom.dom_node(el).matches(sel);
});
shadow.dom.get_next_sibling = (function shadow$dom$get_next_sibling(el){
var G__33818 = shadow.dom.dom_node(el);
return goog.dom.getNextElementSibling(G__33818);
});
shadow.dom.get_previous_sibling = (function shadow$dom$get_previous_sibling(el){
var G__33819 = shadow.dom.dom_node(el);
return goog.dom.getPreviousElementSibling(G__33819);
});
shadow.dom.xmlns = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, ["svg","http://www.w3.org/2000/svg","xlink","http://www.w3.org/1999/xlink"], null));
shadow.dom.create_svg_node = (function shadow$dom$create_svg_node(tag_def,props){
var vec__33822 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33822,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33822,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33822,(2),null);
var el = document.createElementNS("http://www.w3.org/2000/svg",tag_name);
if(cljs.core.truth_(tag_id)){
el.setAttribute("id",tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
el.setAttribute("class",shadow.dom.merge_class_string(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(props),tag_classes));
} else {
}

var seq__33827_34366 = cljs.core.seq(props);
var chunk__33828_34367 = null;
var count__33829_34368 = (0);
var i__33830_34369 = (0);
while(true){
if((i__33830_34369 < count__33829_34368)){
var vec__33841_34370 = chunk__33828_34367.cljs$core$IIndexed$_nth$arity$2(null,i__33830_34369);
var k_34371 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33841_34370,(0),null);
var v_34372 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33841_34370,(1),null);
el.setAttributeNS((function (){var temp__5720__auto__ = cljs.core.namespace(k_34371);
if(cljs.core.truth_(temp__5720__auto__)){
var ns = temp__5720__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_34371),v_34372);


var G__34373 = seq__33827_34366;
var G__34374 = chunk__33828_34367;
var G__34375 = count__33829_34368;
var G__34376 = (i__33830_34369 + (1));
seq__33827_34366 = G__34373;
chunk__33828_34367 = G__34374;
count__33829_34368 = G__34375;
i__33830_34369 = G__34376;
continue;
} else {
var temp__5720__auto___34381 = cljs.core.seq(seq__33827_34366);
if(temp__5720__auto___34381){
var seq__33827_34382__$1 = temp__5720__auto___34381;
if(cljs.core.chunked_seq_QMARK_(seq__33827_34382__$1)){
var c__4550__auto___34383 = cljs.core.chunk_first(seq__33827_34382__$1);
var G__34384 = cljs.core.chunk_rest(seq__33827_34382__$1);
var G__34385 = c__4550__auto___34383;
var G__34386 = cljs.core.count(c__4550__auto___34383);
var G__34387 = (0);
seq__33827_34366 = G__34384;
chunk__33828_34367 = G__34385;
count__33829_34368 = G__34386;
i__33830_34369 = G__34387;
continue;
} else {
var vec__33846_34389 = cljs.core.first(seq__33827_34382__$1);
var k_34390 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33846_34389,(0),null);
var v_34391 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33846_34389,(1),null);
el.setAttributeNS((function (){var temp__5720__auto____$1 = cljs.core.namespace(k_34390);
if(cljs.core.truth_(temp__5720__auto____$1)){
var ns = temp__5720__auto____$1;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_34390),v_34391);


var G__34393 = cljs.core.next(seq__33827_34382__$1);
var G__34394 = null;
var G__34395 = (0);
var G__34396 = (0);
seq__33827_34366 = G__34393;
chunk__33828_34367 = G__34394;
count__33829_34368 = G__34395;
i__33830_34369 = G__34396;
continue;
}
} else {
}
}
break;
}

return el;
});
shadow.dom.svg_node = (function shadow$dom$svg_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$SVGElement$))))?true:false):false)){
return el.shadow$dom$SVGElement$_to_svg$arity$1(null);
} else {
return el;

}
}
});
shadow.dom.make_svg_node = (function shadow$dom$make_svg_node(structure){
var vec__33855 = shadow.dom.destructure_node(shadow.dom.create_svg_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33855,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33855,(1),null);
var seq__33859_34397 = cljs.core.seq(node_children);
var chunk__33861_34398 = null;
var count__33862_34399 = (0);
var i__33863_34400 = (0);
while(true){
if((i__33863_34400 < count__33862_34399)){
var child_struct_34402 = chunk__33861_34398.cljs$core$IIndexed$_nth$arity$2(null,i__33863_34400);
if((!((child_struct_34402 == null)))){
if(typeof child_struct_34402 === 'string'){
var text_34406 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_34406),child_struct_34402].join(''));
} else {
var children_34407 = shadow.dom.svg_node(child_struct_34402);
if(cljs.core.seq_QMARK_(children_34407)){
var seq__33892_34408 = cljs.core.seq(children_34407);
var chunk__33894_34409 = null;
var count__33895_34410 = (0);
var i__33896_34411 = (0);
while(true){
if((i__33896_34411 < count__33895_34410)){
var child_34412 = chunk__33894_34409.cljs$core$IIndexed$_nth$arity$2(null,i__33896_34411);
if(cljs.core.truth_(child_34412)){
node.appendChild(child_34412);


var G__34413 = seq__33892_34408;
var G__34414 = chunk__33894_34409;
var G__34415 = count__33895_34410;
var G__34416 = (i__33896_34411 + (1));
seq__33892_34408 = G__34413;
chunk__33894_34409 = G__34414;
count__33895_34410 = G__34415;
i__33896_34411 = G__34416;
continue;
} else {
var G__34417 = seq__33892_34408;
var G__34418 = chunk__33894_34409;
var G__34419 = count__33895_34410;
var G__34420 = (i__33896_34411 + (1));
seq__33892_34408 = G__34417;
chunk__33894_34409 = G__34418;
count__33895_34410 = G__34419;
i__33896_34411 = G__34420;
continue;
}
} else {
var temp__5720__auto___34421 = cljs.core.seq(seq__33892_34408);
if(temp__5720__auto___34421){
var seq__33892_34422__$1 = temp__5720__auto___34421;
if(cljs.core.chunked_seq_QMARK_(seq__33892_34422__$1)){
var c__4550__auto___34423 = cljs.core.chunk_first(seq__33892_34422__$1);
var G__34424 = cljs.core.chunk_rest(seq__33892_34422__$1);
var G__34425 = c__4550__auto___34423;
var G__34426 = cljs.core.count(c__4550__auto___34423);
var G__34427 = (0);
seq__33892_34408 = G__34424;
chunk__33894_34409 = G__34425;
count__33895_34410 = G__34426;
i__33896_34411 = G__34427;
continue;
} else {
var child_34428 = cljs.core.first(seq__33892_34422__$1);
if(cljs.core.truth_(child_34428)){
node.appendChild(child_34428);


var G__34429 = cljs.core.next(seq__33892_34422__$1);
var G__34430 = null;
var G__34431 = (0);
var G__34432 = (0);
seq__33892_34408 = G__34429;
chunk__33894_34409 = G__34430;
count__33895_34410 = G__34431;
i__33896_34411 = G__34432;
continue;
} else {
var G__34433 = cljs.core.next(seq__33892_34422__$1);
var G__34434 = null;
var G__34435 = (0);
var G__34436 = (0);
seq__33892_34408 = G__34433;
chunk__33894_34409 = G__34434;
count__33895_34410 = G__34435;
i__33896_34411 = G__34436;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_34407);
}
}


var G__34437 = seq__33859_34397;
var G__34438 = chunk__33861_34398;
var G__34439 = count__33862_34399;
var G__34440 = (i__33863_34400 + (1));
seq__33859_34397 = G__34437;
chunk__33861_34398 = G__34438;
count__33862_34399 = G__34439;
i__33863_34400 = G__34440;
continue;
} else {
var G__34441 = seq__33859_34397;
var G__34442 = chunk__33861_34398;
var G__34443 = count__33862_34399;
var G__34444 = (i__33863_34400 + (1));
seq__33859_34397 = G__34441;
chunk__33861_34398 = G__34442;
count__33862_34399 = G__34443;
i__33863_34400 = G__34444;
continue;
}
} else {
var temp__5720__auto___34445 = cljs.core.seq(seq__33859_34397);
if(temp__5720__auto___34445){
var seq__33859_34446__$1 = temp__5720__auto___34445;
if(cljs.core.chunked_seq_QMARK_(seq__33859_34446__$1)){
var c__4550__auto___34447 = cljs.core.chunk_first(seq__33859_34446__$1);
var G__34448 = cljs.core.chunk_rest(seq__33859_34446__$1);
var G__34449 = c__4550__auto___34447;
var G__34450 = cljs.core.count(c__4550__auto___34447);
var G__34451 = (0);
seq__33859_34397 = G__34448;
chunk__33861_34398 = G__34449;
count__33862_34399 = G__34450;
i__33863_34400 = G__34451;
continue;
} else {
var child_struct_34452 = cljs.core.first(seq__33859_34446__$1);
if((!((child_struct_34452 == null)))){
if(typeof child_struct_34452 === 'string'){
var text_34453 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_34453),child_struct_34452].join(''));
} else {
var children_34455 = shadow.dom.svg_node(child_struct_34452);
if(cljs.core.seq_QMARK_(children_34455)){
var seq__33900_34456 = cljs.core.seq(children_34455);
var chunk__33902_34457 = null;
var count__33903_34458 = (0);
var i__33904_34459 = (0);
while(true){
if((i__33904_34459 < count__33903_34458)){
var child_34460 = chunk__33902_34457.cljs$core$IIndexed$_nth$arity$2(null,i__33904_34459);
if(cljs.core.truth_(child_34460)){
node.appendChild(child_34460);


var G__34461 = seq__33900_34456;
var G__34462 = chunk__33902_34457;
var G__34463 = count__33903_34458;
var G__34464 = (i__33904_34459 + (1));
seq__33900_34456 = G__34461;
chunk__33902_34457 = G__34462;
count__33903_34458 = G__34463;
i__33904_34459 = G__34464;
continue;
} else {
var G__34465 = seq__33900_34456;
var G__34466 = chunk__33902_34457;
var G__34467 = count__33903_34458;
var G__34468 = (i__33904_34459 + (1));
seq__33900_34456 = G__34465;
chunk__33902_34457 = G__34466;
count__33903_34458 = G__34467;
i__33904_34459 = G__34468;
continue;
}
} else {
var temp__5720__auto___34469__$1 = cljs.core.seq(seq__33900_34456);
if(temp__5720__auto___34469__$1){
var seq__33900_34470__$1 = temp__5720__auto___34469__$1;
if(cljs.core.chunked_seq_QMARK_(seq__33900_34470__$1)){
var c__4550__auto___34471 = cljs.core.chunk_first(seq__33900_34470__$1);
var G__34472 = cljs.core.chunk_rest(seq__33900_34470__$1);
var G__34473 = c__4550__auto___34471;
var G__34474 = cljs.core.count(c__4550__auto___34471);
var G__34475 = (0);
seq__33900_34456 = G__34472;
chunk__33902_34457 = G__34473;
count__33903_34458 = G__34474;
i__33904_34459 = G__34475;
continue;
} else {
var child_34476 = cljs.core.first(seq__33900_34470__$1);
if(cljs.core.truth_(child_34476)){
node.appendChild(child_34476);


var G__34477 = cljs.core.next(seq__33900_34470__$1);
var G__34478 = null;
var G__34479 = (0);
var G__34480 = (0);
seq__33900_34456 = G__34477;
chunk__33902_34457 = G__34478;
count__33903_34458 = G__34479;
i__33904_34459 = G__34480;
continue;
} else {
var G__34481 = cljs.core.next(seq__33900_34470__$1);
var G__34482 = null;
var G__34483 = (0);
var G__34484 = (0);
seq__33900_34456 = G__34481;
chunk__33902_34457 = G__34482;
count__33903_34458 = G__34483;
i__33904_34459 = G__34484;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_34455);
}
}


var G__34485 = cljs.core.next(seq__33859_34446__$1);
var G__34486 = null;
var G__34487 = (0);
var G__34488 = (0);
seq__33859_34397 = G__34485;
chunk__33861_34398 = G__34486;
count__33862_34399 = G__34487;
i__33863_34400 = G__34488;
continue;
} else {
var G__34489 = cljs.core.next(seq__33859_34446__$1);
var G__34490 = null;
var G__34491 = (0);
var G__34492 = (0);
seq__33859_34397 = G__34489;
chunk__33861_34398 = G__34490;
count__33862_34399 = G__34491;
i__33863_34400 = G__34492;
continue;
}
}
} else {
}
}
break;
}

return node;
});
goog.object.set(shadow.dom.SVGElement,"string",true);

var G__33912_34493 = shadow.dom._to_svg;
var G__33913_34494 = "string";
var G__33914_34495 = ((function (G__33912_34493,G__33913_34494){
return (function (this$){
if((this$ instanceof cljs.core.Keyword)){
return shadow.dom.make_svg_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$], null));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("strings cannot be in svgs",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"this","this",-611633625),this$], null));
}
});})(G__33912_34493,G__33913_34494))
;
goog.object.set(G__33912_34493,G__33913_34494,G__33914_34495);

cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_svg_node(this$__$1);
});

cljs.core.LazySeq.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.LazySeq.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_svg,this$__$1);
});

goog.object.set(shadow.dom.SVGElement,"null",true);

var G__33917_34503 = shadow.dom._to_svg;
var G__33918_34504 = "null";
var G__33919_34505 = ((function (G__33917_34503,G__33918_34504){
return (function (_){
return null;
});})(G__33917_34503,G__33918_34504))
;
goog.object.set(G__33917_34503,G__33918_34504,G__33919_34505);
shadow.dom.svg = (function shadow$dom$svg(var_args){
var args__4736__auto__ = [];
var len__4730__auto___34506 = arguments.length;
var i__4731__auto___34507 = (0);
while(true){
if((i__4731__auto___34507 < len__4730__auto___34506)){
args__4736__auto__.push((arguments[i__4731__auto___34507]));

var G__34508 = (i__4731__auto___34507 + (1));
i__4731__auto___34507 = G__34508;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic = (function (attrs,children){
return shadow.dom._to_svg(cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),attrs], null),children)));
});

shadow.dom.svg.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
shadow.dom.svg.cljs$lang$applyTo = (function (seq33923){
var G__33924 = cljs.core.first(seq33923);
var seq33923__$1 = cljs.core.next(seq33923);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__33924,seq33923__$1);
});

/**
 * returns a channel for events on el
 * transform-fn should be a (fn [e el] some-val) where some-val will be put on the chan
 * once-or-cleanup handles the removal of the event handler
 * - true: remove after one event
 * - false: never removed
 * - chan: remove on msg/close
 */
shadow.dom.event_chan = (function shadow$dom$event_chan(var_args){
var G__33929 = arguments.length;
switch (G__33929) {
case 2:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2 = (function (el,event){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,null,false);
});

shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3 = (function (el,event,xf){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,xf,false);
});

shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4 = (function (el,event,xf,once_or_cleanup){
var buf = cljs.core.async.sliding_buffer((1));
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2(buf,xf);
var event_fn = ((function (buf,chan){
return (function shadow$dom$event_fn(e){
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,e);

if(once_or_cleanup === true){
shadow.dom.remove_event_handler(el,event,shadow$dom$event_fn);

return cljs.core.async.close_BANG_(chan);
} else {
return null;
}
});})(buf,chan))
;
var G__33933_34510 = shadow.dom.dom_node(el);
var G__33934_34511 = cljs.core.name(event);
var G__33935_34512 = event_fn;
(shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__33933_34510,G__33934_34511,G__33935_34512) : shadow.dom.dom_listen.call(null,G__33933_34510,G__33934_34511,G__33935_34512));

if(cljs.core.truth_((function (){var and__4120__auto__ = once_or_cleanup;
if(cljs.core.truth_(and__4120__auto__)){
return (!(once_or_cleanup === true));
} else {
return and__4120__auto__;
}
})())){
var c__30611__auto___34514 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__30611__auto___34514,buf,chan,event_fn){
return (function (){
var f__30612__auto__ = (function (){var switch__30294__auto__ = ((function (c__30611__auto___34514,buf,chan,event_fn){
return (function (state_33941){
var state_val_33942 = (state_33941[(1)]);
if((state_val_33942 === (1))){
var state_33941__$1 = state_33941;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33941__$1,(2),once_or_cleanup);
} else {
if((state_val_33942 === (2))){
var inst_33938 = (state_33941[(2)]);
var inst_33939 = shadow.dom.remove_event_handler(el,event,event_fn);
var state_33941__$1 = (function (){var statearr_33944 = state_33941;
(statearr_33944[(7)] = inst_33938);

return statearr_33944;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_33941__$1,inst_33939);
} else {
return null;
}
}
});})(c__30611__auto___34514,buf,chan,event_fn))
;
return ((function (switch__30294__auto__,c__30611__auto___34514,buf,chan,event_fn){
return (function() {
var shadow$dom$state_machine__30295__auto__ = null;
var shadow$dom$state_machine__30295__auto____0 = (function (){
var statearr_33946 = [null,null,null,null,null,null,null,null];
(statearr_33946[(0)] = shadow$dom$state_machine__30295__auto__);

(statearr_33946[(1)] = (1));

return statearr_33946;
});
var shadow$dom$state_machine__30295__auto____1 = (function (state_33941){
while(true){
var ret_value__30296__auto__ = (function (){try{while(true){
var result__30297__auto__ = switch__30294__auto__(state_33941);
if(cljs.core.keyword_identical_QMARK_(result__30297__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30297__auto__;
}
break;
}
}catch (e33947){if((e33947 instanceof Object)){
var ex__30298__auto__ = e33947;
var statearr_33948_34516 = state_33941;
(statearr_33948_34516[(5)] = ex__30298__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33941);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33947;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30296__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34517 = state_33941;
state_33941 = G__34517;
continue;
} else {
return ret_value__30296__auto__;
}
break;
}
});
shadow$dom$state_machine__30295__auto__ = function(state_33941){
switch(arguments.length){
case 0:
return shadow$dom$state_machine__30295__auto____0.call(this);
case 1:
return shadow$dom$state_machine__30295__auto____1.call(this,state_33941);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
shadow$dom$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$0 = shadow$dom$state_machine__30295__auto____0;
shadow$dom$state_machine__30295__auto__.cljs$core$IFn$_invoke$arity$1 = shadow$dom$state_machine__30295__auto____1;
return shadow$dom$state_machine__30295__auto__;
})()
;})(switch__30294__auto__,c__30611__auto___34514,buf,chan,event_fn))
})();
var state__30613__auto__ = (function (){var statearr_33949 = (f__30612__auto__.cljs$core$IFn$_invoke$arity$0 ? f__30612__auto__.cljs$core$IFn$_invoke$arity$0() : f__30612__auto__.call(null));
(statearr_33949[(6)] = c__30611__auto___34514);

return statearr_33949;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30613__auto__);
});})(c__30611__auto___34514,buf,chan,event_fn))
);

} else {
}

return chan;
});

shadow.dom.event_chan.cljs$lang$maxFixedArity = 4;


//# sourceMappingURL=shadow.dom.js.map
