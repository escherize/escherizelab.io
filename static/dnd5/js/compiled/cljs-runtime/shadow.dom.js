goog.provide('shadow.dom');
goog.require('cljs.core');
goog.require('goog.dom');
goog.require('goog.dom.forms');
goog.require('goog.dom.classlist');
goog.require('goog.style');
goog.require('goog.style.transition');
goog.require('goog.string');
goog.require('clojure.string');
goog.require('cljs.core.async');
shadow.dom.transition_supported_QMARK_ = (((typeof window !== 'undefined'))?goog.style.transition.isSupported():null);

/**
 * @interface
 */
shadow.dom.IElement = function(){};

shadow.dom._to_dom = (function shadow$dom$_to_dom(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$IElement$_to_dom$arity$1 == null)))))){
return this$.shadow$dom$IElement$_to_dom$arity$1(this$);
} else {
var x__4433__auto__ = (((this$ == null))?null:this$);
var m__4434__auto__ = (shadow.dom._to_dom[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4434__auto__.call(null,this$));
} else {
var m__4431__auto__ = (shadow.dom._to_dom["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4431__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("IElement.-to-dom",this$);
}
}
}
});


/**
 * @interface
 */
shadow.dom.SVGElement = function(){};

shadow.dom._to_svg = (function shadow$dom$_to_svg(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$SVGElement$_to_svg$arity$1 == null)))))){
return this$.shadow$dom$SVGElement$_to_svg$arity$1(this$);
} else {
var x__4433__auto__ = (((this$ == null))?null:this$);
var m__4434__auto__ = (shadow.dom._to_svg[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4434__auto__.call(null,this$));
} else {
var m__4431__auto__ = (shadow.dom._to_svg["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4431__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("SVGElement.-to-svg",this$);
}
}
}
});

shadow.dom.lazy_native_coll_seq = (function shadow$dom$lazy_native_coll_seq(coll,idx){
if((idx < coll.length)){
return (new cljs.core.LazySeq(null,(function (){
return cljs.core.cons((coll[idx]),(function (){var G__35161 = coll;
var G__35162 = (idx + (1));
return (shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2 ? shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2(G__35161,G__35162) : shadow.dom.lazy_native_coll_seq.call(null,G__35161,G__35162));
})());
}),null,null));
} else {
return null;
}
});

/**
* @constructor
 * @implements {cljs.core.IIndexed}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IDeref}
 * @implements {shadow.dom.IElement}
*/
shadow.dom.NativeColl = (function (coll){
this.coll = coll;
this.cljs$lang$protocol_mask$partition0$ = 8421394;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
shadow.dom.NativeColl.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
});

shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (this$,n){
var self__ = this;
var this$__$1 = this;
return (self__.coll[n]);
});

shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (this$,n,not_found){
var self__ = this;
var this$__$1 = this;
var or__4131__auto__ = (self__.coll[n]);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return not_found;
}
});

shadow.dom.NativeColl.prototype.cljs$core$ICounted$_count$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll.length;
});

shadow.dom.NativeColl.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return shadow.dom.lazy_native_coll_seq(self__.coll,(0));
});

shadow.dom.NativeColl.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

shadow.dom.NativeColl.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
});

shadow.dom.NativeColl.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"coll","coll",-1006698606,null)], null);
});

shadow.dom.NativeColl.cljs$lang$type = true;

shadow.dom.NativeColl.cljs$lang$ctorStr = "shadow.dom/NativeColl";

shadow.dom.NativeColl.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"shadow.dom/NativeColl");
});

/**
 * Positional factory function for shadow.dom/NativeColl.
 */
shadow.dom.__GT_NativeColl = (function shadow$dom$__GT_NativeColl(coll){
return (new shadow.dom.NativeColl(coll));
});

shadow.dom.native_coll = (function shadow$dom$native_coll(coll){
return (new shadow.dom.NativeColl(coll));
});
shadow.dom.dom_node = (function shadow$dom$dom_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$IElement$))))?true:false):false)){
return el.shadow$dom$IElement$_to_dom$arity$1(null);
} else {
if(typeof el === 'string'){
return document.createTextNode(el);
} else {
if(typeof el === 'number'){
return document.createTextNode(cljs.core.str.cljs$core$IFn$_invoke$arity$1(el));
} else {
return el;

}
}
}
}
});
shadow.dom.query_one = (function shadow$dom$query_one(var_args){
var G__35197 = arguments.length;
switch (G__35197) {
case 1:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return document.querySelector(sel);
});

shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return shadow.dom.dom_node(root).querySelector(sel);
});

shadow.dom.query_one.cljs$lang$maxFixedArity = 2;

shadow.dom.query = (function shadow$dom$query(var_args){
var G__35201 = arguments.length;
switch (G__35201) {
case 1:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.query.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return (new shadow.dom.NativeColl(document.querySelectorAll(sel)));
});

shadow.dom.query.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(root).querySelectorAll(sel)));
});

shadow.dom.query.cljs$lang$maxFixedArity = 2;

shadow.dom.by_id = (function shadow$dom$by_id(var_args){
var G__35204 = arguments.length;
switch (G__35204) {
case 2:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2 = (function (id,el){
return shadow.dom.dom_node(el).getElementById(id);
});

shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1 = (function (id){
return document.getElementById(id);
});

shadow.dom.by_id.cljs$lang$maxFixedArity = 2;

shadow.dom.build = shadow.dom.dom_node;
shadow.dom.ev_stop = (function shadow$dom$ev_stop(var_args){
var G__35210 = arguments.length;
switch (G__35210) {
case 1:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1 = (function (e){
if(cljs.core.truth_(e.stopPropagation)){
e.stopPropagation();

e.preventDefault();
} else {
e.cancelBubble = true;

e.returnValue = false;
}

return e;
});

shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2 = (function (e,el){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
});

shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4 = (function (e,el,scope,owner){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
});

shadow.dom.ev_stop.cljs$lang$maxFixedArity = 4;

/**
 * check wether a parent node (or the document) contains the child
 */
shadow.dom.contains_QMARK_ = (function shadow$dom$contains_QMARK_(var_args){
var G__35218 = arguments.length;
switch (G__35218) {
case 1:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1 = (function (el){
var G__35222 = document;
var G__35223 = shadow.dom.dom_node(el);
return goog.dom.contains(G__35222,G__35223);
});

shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2 = (function (parent,el){
var G__35226 = shadow.dom.dom_node(parent);
var G__35227 = shadow.dom.dom_node(el);
return goog.dom.contains(G__35226,G__35227);
});

shadow.dom.contains_QMARK_.cljs$lang$maxFixedArity = 2;

shadow.dom.add_class = (function shadow$dom$add_class(el,cls){
var G__35229 = shadow.dom.dom_node(el);
var G__35230 = cls;
return goog.dom.classlist.add(G__35229,G__35230);
});
shadow.dom.remove_class = (function shadow$dom$remove_class(el,cls){
var G__35232 = shadow.dom.dom_node(el);
var G__35233 = cls;
return goog.dom.classlist.remove(G__35232,G__35233);
});
shadow.dom.toggle_class = (function shadow$dom$toggle_class(var_args){
var G__35236 = arguments.length;
switch (G__35236) {
case 2:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2 = (function (el,cls){
var G__35239 = shadow.dom.dom_node(el);
var G__35240 = cls;
return goog.dom.classlist.toggle(G__35239,G__35240);
});

shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3 = (function (el,cls,v){
if(cljs.core.truth_(v)){
return shadow.dom.add_class(el,cls);
} else {
return shadow.dom.remove_class(el,cls);
}
});

shadow.dom.toggle_class.cljs$lang$maxFixedArity = 3;

shadow.dom.dom_listen = (cljs.core.truth_((function (){var or__4131__auto__ = (!((typeof document !== 'undefined')));
if(or__4131__auto__){
return or__4131__auto__;
} else {
return document.addEventListener;
}
})())?(function shadow$dom$dom_listen_good(el,ev,handler){
return el.addEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_ie(el,ev,handler){
try{return el.attachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),(function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
}));
}catch (e35244){if((e35244 instanceof Object)){
var e = e35244;
return console.log("didnt support attachEvent",el,e);
} else {
throw e35244;

}
}}));
shadow.dom.dom_listen_remove = (cljs.core.truth_((function (){var or__4131__auto__ = (!((typeof document !== 'undefined')));
if(or__4131__auto__){
return or__4131__auto__;
} else {
return document.removeEventListener;
}
})())?(function shadow$dom$dom_listen_remove_good(el,ev,handler){
return el.removeEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_remove_ie(el,ev,handler){
return el.detachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),handler);
}));
shadow.dom.on_query = (function shadow$dom$on_query(root_el,ev,selector,handler){
var seq__35248 = cljs.core.seq(shadow.dom.query.cljs$core$IFn$_invoke$arity$2(selector,root_el));
var chunk__35249 = null;
var count__35250 = (0);
var i__35251 = (0);
while(true){
if((i__35251 < count__35250)){
var el = chunk__35249.cljs$core$IIndexed$_nth$arity$2(null,i__35251);
var handler_35983__$1 = ((function (seq__35248,chunk__35249,count__35250,i__35251,el){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__35248,chunk__35249,count__35250,i__35251,el))
;
var G__35266_35985 = el;
var G__35267_35986 = cljs.core.name(ev);
var G__35268_35987 = handler_35983__$1;
(shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__35266_35985,G__35267_35986,G__35268_35987) : shadow.dom.dom_listen.call(null,G__35266_35985,G__35267_35986,G__35268_35987));


var G__35989 = seq__35248;
var G__35990 = chunk__35249;
var G__35991 = count__35250;
var G__35992 = (i__35251 + (1));
seq__35248 = G__35989;
chunk__35249 = G__35990;
count__35250 = G__35991;
i__35251 = G__35992;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__35248);
if(temp__5735__auto__){
var seq__35248__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__35248__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__35248__$1);
var G__35994 = cljs.core.chunk_rest(seq__35248__$1);
var G__35995 = c__4550__auto__;
var G__35996 = cljs.core.count(c__4550__auto__);
var G__35997 = (0);
seq__35248 = G__35994;
chunk__35249 = G__35995;
count__35250 = G__35996;
i__35251 = G__35997;
continue;
} else {
var el = cljs.core.first(seq__35248__$1);
var handler_35999__$1 = ((function (seq__35248,chunk__35249,count__35250,i__35251,el,seq__35248__$1,temp__5735__auto__){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__35248,chunk__35249,count__35250,i__35251,el,seq__35248__$1,temp__5735__auto__))
;
var G__35273_36001 = el;
var G__35274_36002 = cljs.core.name(ev);
var G__35275_36003 = handler_35999__$1;
(shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__35273_36001,G__35274_36002,G__35275_36003) : shadow.dom.dom_listen.call(null,G__35273_36001,G__35274_36002,G__35275_36003));


var G__36004 = cljs.core.next(seq__35248__$1);
var G__36005 = null;
var G__36006 = (0);
var G__36007 = (0);
seq__35248 = G__36004;
chunk__35249 = G__36005;
count__35250 = G__36006;
i__35251 = G__36007;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.on = (function shadow$dom$on(var_args){
var G__35280 = arguments.length;
switch (G__35280) {
case 3:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.on.cljs$core$IFn$_invoke$arity$3 = (function (el,ev,handler){
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4(el,ev,handler,false);
});

shadow.dom.on.cljs$core$IFn$_invoke$arity$4 = (function (el,ev,handler,capture){
if(cljs.core.vector_QMARK_(ev)){
return shadow.dom.on_query(el,cljs.core.first(ev),cljs.core.second(ev),handler);
} else {
var handler__$1 = (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});
var G__35285 = shadow.dom.dom_node(el);
var G__35286 = cljs.core.name(ev);
var G__35287 = handler__$1;
return (shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__35285,G__35286,G__35287) : shadow.dom.dom_listen.call(null,G__35285,G__35286,G__35287));
}
});

shadow.dom.on.cljs$lang$maxFixedArity = 4;

shadow.dom.remove_event_handler = (function shadow$dom$remove_event_handler(el,ev,handler){
var G__35294 = shadow.dom.dom_node(el);
var G__35295 = cljs.core.name(ev);
var G__35296 = handler;
return (shadow.dom.dom_listen_remove.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen_remove.cljs$core$IFn$_invoke$arity$3(G__35294,G__35295,G__35296) : shadow.dom.dom_listen_remove.call(null,G__35294,G__35295,G__35296));
});
shadow.dom.add_event_listeners = (function shadow$dom$add_event_listeners(el,events){
var seq__35299 = cljs.core.seq(events);
var chunk__35300 = null;
var count__35301 = (0);
var i__35302 = (0);
while(true){
if((i__35302 < count__35301)){
var vec__35317 = chunk__35300.cljs$core$IIndexed$_nth$arity$2(null,i__35302);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35317,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35317,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__36014 = seq__35299;
var G__36015 = chunk__35300;
var G__36016 = count__35301;
var G__36017 = (i__35302 + (1));
seq__35299 = G__36014;
chunk__35300 = G__36015;
count__35301 = G__36016;
i__35302 = G__36017;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__35299);
if(temp__5735__auto__){
var seq__35299__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__35299__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__35299__$1);
var G__36018 = cljs.core.chunk_rest(seq__35299__$1);
var G__36019 = c__4550__auto__;
var G__36020 = cljs.core.count(c__4550__auto__);
var G__36021 = (0);
seq__35299 = G__36018;
chunk__35300 = G__36019;
count__35301 = G__36020;
i__35302 = G__36021;
continue;
} else {
var vec__35322 = cljs.core.first(seq__35299__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35322,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35322,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__36025 = cljs.core.next(seq__35299__$1);
var G__36026 = null;
var G__36027 = (0);
var G__36028 = (0);
seq__35299 = G__36025;
chunk__35300 = G__36026;
count__35301 = G__36027;
i__35302 = G__36028;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_style = (function shadow$dom$set_style(el,styles){
var dom = shadow.dom.dom_node(el);
var seq__35329 = cljs.core.seq(styles);
var chunk__35330 = null;
var count__35331 = (0);
var i__35332 = (0);
while(true){
if((i__35332 < count__35331)){
var vec__35351 = chunk__35330.cljs$core$IIndexed$_nth$arity$2(null,i__35332);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35351,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35351,(1),null);
var G__35356_36031 = dom;
var G__35357_36032 = cljs.core.name(k);
var G__35358_36033 = (((v == null))?"":v);
goog.style.setStyle(G__35356_36031,G__35357_36032,G__35358_36033);


var G__36034 = seq__35329;
var G__36035 = chunk__35330;
var G__36036 = count__35331;
var G__36037 = (i__35332 + (1));
seq__35329 = G__36034;
chunk__35330 = G__36035;
count__35331 = G__36036;
i__35332 = G__36037;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__35329);
if(temp__5735__auto__){
var seq__35329__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__35329__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__35329__$1);
var G__36038 = cljs.core.chunk_rest(seq__35329__$1);
var G__36039 = c__4550__auto__;
var G__36040 = cljs.core.count(c__4550__auto__);
var G__36041 = (0);
seq__35329 = G__36038;
chunk__35330 = G__36039;
count__35331 = G__36040;
i__35332 = G__36041;
continue;
} else {
var vec__35362 = cljs.core.first(seq__35329__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35362,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35362,(1),null);
var G__35366_36043 = dom;
var G__35367_36044 = cljs.core.name(k);
var G__35368_36045 = (((v == null))?"":v);
goog.style.setStyle(G__35366_36043,G__35367_36044,G__35368_36045);


var G__36047 = cljs.core.next(seq__35329__$1);
var G__36048 = null;
var G__36049 = (0);
var G__36050 = (0);
seq__35329 = G__36047;
chunk__35330 = G__36048;
count__35331 = G__36049;
i__35332 = G__36050;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_attr_STAR_ = (function shadow$dom$set_attr_STAR_(el,key,value){
var G__35374_36051 = key;
var G__35374_36052__$1 = (((G__35374_36051 instanceof cljs.core.Keyword))?G__35374_36051.fqn:null);
switch (G__35374_36052__$1) {
case "id":
el.id = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value);

break;
case "class":
el.className = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value);

break;
case "for":
el.htmlFor = value;

break;
case "cellpadding":
el.setAttribute("cellPadding",value);

break;
case "cellspacing":
el.setAttribute("cellSpacing",value);

break;
case "colspan":
el.setAttribute("colSpan",value);

break;
case "frameborder":
el.setAttribute("frameBorder",value);

break;
case "height":
el.setAttribute("height",value);

break;
case "maxlength":
el.setAttribute("maxLength",value);

break;
case "role":
el.setAttribute("role",value);

break;
case "rowspan":
el.setAttribute("rowSpan",value);

break;
case "type":
el.setAttribute("type",value);

break;
case "usemap":
el.setAttribute("useMap",value);

break;
case "valign":
el.setAttribute("vAlign",value);

break;
case "width":
el.setAttribute("width",value);

break;
case "on":
shadow.dom.add_event_listeners(el,value);

break;
case "style":
if((value == null)){
} else {
if(typeof value === 'string'){
el.setAttribute("style",value);
} else {
if(cljs.core.map_QMARK_(value)){
shadow.dom.set_style(el,value);
} else {
goog.style.setStyle(el,value);

}
}
}

break;
default:
var ks_36060 = cljs.core.name(key);
if(cljs.core.truth_((function (){var or__4131__auto__ = goog.string.startsWith(ks_36060,"data-");
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return goog.string.startsWith(ks_36060,"aria-");
}
})())){
el.setAttribute(ks_36060,value);
} else {
(el[ks_36060] = value);
}

}

return el;
});
shadow.dom.set_attrs = (function shadow$dom$set_attrs(el,attrs){
return cljs.core.reduce_kv((function (el__$1,key,value){
shadow.dom.set_attr_STAR_(el__$1,key,value);

return el__$1;
}),shadow.dom.dom_node(el),attrs);
});
shadow.dom.set_attr = (function shadow$dom$set_attr(el,key,value){
return shadow.dom.set_attr_STAR_(shadow.dom.dom_node(el),key,value);
});
shadow.dom.has_class_QMARK_ = (function shadow$dom$has_class_QMARK_(el,cls){
var G__35402 = shadow.dom.dom_node(el);
var G__35403 = cls;
return goog.dom.classlist.contains(G__35402,G__35403);
});
shadow.dom.merge_class_string = (function shadow$dom$merge_class_string(current,extra_class){
if(cljs.core.seq(current)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(current)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(extra_class)].join('');
} else {
return extra_class;
}
});
shadow.dom.parse_tag = (function shadow$dom$parse_tag(spec){
var spec__$1 = cljs.core.name(spec);
var fdot = spec__$1.indexOf(".");
var fhash = spec__$1.indexOf("#");
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1,null,null], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fdot),null,clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1))),null], null);
} else {
if((fhash > fdot)){
throw ["cant have id after class?",spec__$1].join('');
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1)),fdot),clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);

}
}
}
}
});
shadow.dom.create_dom_node = (function shadow$dom$create_dom_node(tag_def,p__35414){
var map__35415 = p__35414;
var map__35415__$1 = (((((!((map__35415 == null))))?(((((map__35415.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__35415.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__35415):map__35415);
var props = map__35415__$1;
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__35415__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var tag_props = ({});
var vec__35421 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35421,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35421,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35421,(2),null);
if(cljs.core.truth_(tag_id)){
(tag_props["id"] = tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
(tag_props["class"] = shadow.dom.merge_class_string(class$,tag_classes));
} else {
}

var G__35426 = goog.dom.createDom(tag_name,tag_props);
shadow.dom.set_attrs(G__35426,cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(props,new cljs.core.Keyword(null,"class","class",-2030961996)));

return G__35426;
});
shadow.dom.append = (function shadow$dom$append(var_args){
var G__35429 = arguments.length;
switch (G__35429) {
case 1:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.append.cljs$core$IFn$_invoke$arity$1 = (function (node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
document.body.appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
});

shadow.dom.append.cljs$core$IFn$_invoke$arity$2 = (function (el,node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
shadow.dom.dom_node(el).appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
});

shadow.dom.append.cljs$lang$maxFixedArity = 2;

shadow.dom.destructure_node = (function shadow$dom$destructure_node(create_fn,p__35435){
var vec__35436 = p__35435;
var seq__35437 = cljs.core.seq(vec__35436);
var first__35438 = cljs.core.first(seq__35437);
var seq__35437__$1 = cljs.core.next(seq__35437);
var nn = first__35438;
var first__35438__$1 = cljs.core.first(seq__35437__$1);
var seq__35437__$2 = cljs.core.next(seq__35437__$1);
var np = first__35438__$1;
var nc = seq__35437__$2;
var node = vec__35436;
if((nn instanceof cljs.core.Keyword)){
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("invalid dom node",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"node","node",581201198),node], null));
}

if((((np == null)) && ((nc == null)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__35443 = nn;
var G__35444 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__35443,G__35444) : create_fn.call(null,G__35443,G__35444));
})(),cljs.core.List.EMPTY], null);
} else {
if(cljs.core.map_QMARK_(np)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(nn,np) : create_fn.call(null,nn,np)),nc], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__35446 = nn;
var G__35447 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__35446,G__35447) : create_fn.call(null,G__35446,G__35447));
})(),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(nc,np)], null);

}
}
});
shadow.dom.make_dom_node = (function shadow$dom$make_dom_node(structure){
var vec__35452 = shadow.dom.destructure_node(shadow.dom.create_dom_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35452,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35452,(1),null);
var seq__35456_36082 = cljs.core.seq(node_children);
var chunk__35457_36083 = null;
var count__35459_36084 = (0);
var i__35460_36085 = (0);
while(true){
if((i__35460_36085 < count__35459_36084)){
var child_struct_36087 = chunk__35457_36083.cljs$core$IIndexed$_nth$arity$2(null,i__35460_36085);
var children_36088 = shadow.dom.dom_node(child_struct_36087);
if(cljs.core.seq_QMARK_(children_36088)){
var seq__35507_36090 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_36088));
var chunk__35509_36091 = null;
var count__35510_36092 = (0);
var i__35511_36093 = (0);
while(true){
if((i__35511_36093 < count__35510_36092)){
var child_36095 = chunk__35509_36091.cljs$core$IIndexed$_nth$arity$2(null,i__35511_36093);
if(cljs.core.truth_(child_36095)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_36095);


var G__36098 = seq__35507_36090;
var G__36099 = chunk__35509_36091;
var G__36100 = count__35510_36092;
var G__36101 = (i__35511_36093 + (1));
seq__35507_36090 = G__36098;
chunk__35509_36091 = G__36099;
count__35510_36092 = G__36100;
i__35511_36093 = G__36101;
continue;
} else {
var G__36102 = seq__35507_36090;
var G__36103 = chunk__35509_36091;
var G__36104 = count__35510_36092;
var G__36105 = (i__35511_36093 + (1));
seq__35507_36090 = G__36102;
chunk__35509_36091 = G__36103;
count__35510_36092 = G__36104;
i__35511_36093 = G__36105;
continue;
}
} else {
var temp__5735__auto___36107 = cljs.core.seq(seq__35507_36090);
if(temp__5735__auto___36107){
var seq__35507_36109__$1 = temp__5735__auto___36107;
if(cljs.core.chunked_seq_QMARK_(seq__35507_36109__$1)){
var c__4550__auto___36110 = cljs.core.chunk_first(seq__35507_36109__$1);
var G__36111 = cljs.core.chunk_rest(seq__35507_36109__$1);
var G__36112 = c__4550__auto___36110;
var G__36113 = cljs.core.count(c__4550__auto___36110);
var G__36114 = (0);
seq__35507_36090 = G__36111;
chunk__35509_36091 = G__36112;
count__35510_36092 = G__36113;
i__35511_36093 = G__36114;
continue;
} else {
var child_36116 = cljs.core.first(seq__35507_36109__$1);
if(cljs.core.truth_(child_36116)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_36116);


var G__36118 = cljs.core.next(seq__35507_36109__$1);
var G__36119 = null;
var G__36120 = (0);
var G__36121 = (0);
seq__35507_36090 = G__36118;
chunk__35509_36091 = G__36119;
count__35510_36092 = G__36120;
i__35511_36093 = G__36121;
continue;
} else {
var G__36122 = cljs.core.next(seq__35507_36109__$1);
var G__36123 = null;
var G__36124 = (0);
var G__36125 = (0);
seq__35507_36090 = G__36122;
chunk__35509_36091 = G__36123;
count__35510_36092 = G__36124;
i__35511_36093 = G__36125;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_36088);
}


var G__36128 = seq__35456_36082;
var G__36129 = chunk__35457_36083;
var G__36130 = count__35459_36084;
var G__36131 = (i__35460_36085 + (1));
seq__35456_36082 = G__36128;
chunk__35457_36083 = G__36129;
count__35459_36084 = G__36130;
i__35460_36085 = G__36131;
continue;
} else {
var temp__5735__auto___36133 = cljs.core.seq(seq__35456_36082);
if(temp__5735__auto___36133){
var seq__35456_36134__$1 = temp__5735__auto___36133;
if(cljs.core.chunked_seq_QMARK_(seq__35456_36134__$1)){
var c__4550__auto___36136 = cljs.core.chunk_first(seq__35456_36134__$1);
var G__36137 = cljs.core.chunk_rest(seq__35456_36134__$1);
var G__36138 = c__4550__auto___36136;
var G__36139 = cljs.core.count(c__4550__auto___36136);
var G__36140 = (0);
seq__35456_36082 = G__36137;
chunk__35457_36083 = G__36138;
count__35459_36084 = G__36139;
i__35460_36085 = G__36140;
continue;
} else {
var child_struct_36143 = cljs.core.first(seq__35456_36134__$1);
var children_36144 = shadow.dom.dom_node(child_struct_36143);
if(cljs.core.seq_QMARK_(children_36144)){
var seq__35524_36145 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_36144));
var chunk__35527_36146 = null;
var count__35528_36147 = (0);
var i__35529_36148 = (0);
while(true){
if((i__35529_36148 < count__35528_36147)){
var child_36150 = chunk__35527_36146.cljs$core$IIndexed$_nth$arity$2(null,i__35529_36148);
if(cljs.core.truth_(child_36150)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_36150);


var G__36152 = seq__35524_36145;
var G__36153 = chunk__35527_36146;
var G__36154 = count__35528_36147;
var G__36155 = (i__35529_36148 + (1));
seq__35524_36145 = G__36152;
chunk__35527_36146 = G__36153;
count__35528_36147 = G__36154;
i__35529_36148 = G__36155;
continue;
} else {
var G__36160 = seq__35524_36145;
var G__36161 = chunk__35527_36146;
var G__36162 = count__35528_36147;
var G__36163 = (i__35529_36148 + (1));
seq__35524_36145 = G__36160;
chunk__35527_36146 = G__36161;
count__35528_36147 = G__36162;
i__35529_36148 = G__36163;
continue;
}
} else {
var temp__5735__auto___36164__$1 = cljs.core.seq(seq__35524_36145);
if(temp__5735__auto___36164__$1){
var seq__35524_36167__$1 = temp__5735__auto___36164__$1;
if(cljs.core.chunked_seq_QMARK_(seq__35524_36167__$1)){
var c__4550__auto___36168 = cljs.core.chunk_first(seq__35524_36167__$1);
var G__36170 = cljs.core.chunk_rest(seq__35524_36167__$1);
var G__36171 = c__4550__auto___36168;
var G__36172 = cljs.core.count(c__4550__auto___36168);
var G__36173 = (0);
seq__35524_36145 = G__36170;
chunk__35527_36146 = G__36171;
count__35528_36147 = G__36172;
i__35529_36148 = G__36173;
continue;
} else {
var child_36174 = cljs.core.first(seq__35524_36167__$1);
if(cljs.core.truth_(child_36174)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_36174);


var G__36175 = cljs.core.next(seq__35524_36167__$1);
var G__36176 = null;
var G__36177 = (0);
var G__36178 = (0);
seq__35524_36145 = G__36175;
chunk__35527_36146 = G__36176;
count__35528_36147 = G__36177;
i__35529_36148 = G__36178;
continue;
} else {
var G__36180 = cljs.core.next(seq__35524_36167__$1);
var G__36181 = null;
var G__36182 = (0);
var G__36183 = (0);
seq__35524_36145 = G__36180;
chunk__35527_36146 = G__36181;
count__35528_36147 = G__36182;
i__35529_36148 = G__36183;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_36144);
}


var G__36186 = cljs.core.next(seq__35456_36134__$1);
var G__36187 = null;
var G__36188 = (0);
var G__36189 = (0);
seq__35456_36082 = G__36186;
chunk__35457_36083 = G__36187;
count__35459_36084 = G__36188;
i__35460_36085 = G__36189;
continue;
}
} else {
}
}
break;
}

return node;
});
cljs.core.Keyword.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.Keyword.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$__$1], null));
});

cljs.core.PersistentVector.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.PersistentVector.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(this$__$1);
});

cljs.core.LazySeq.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.LazySeq.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_dom,this$__$1);
});
if(cljs.core.truth_(((typeof HTMLElement) != 'undefined'))){
HTMLElement.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

HTMLElement.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
});
} else {
}
if(cljs.core.truth_(((typeof DocumentFragment) != 'undefined'))){
DocumentFragment.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL;

DocumentFragment.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
});
} else {
}
/**
 * clear node children
 */
shadow.dom.reset = (function shadow$dom$reset(node){
var G__35550 = shadow.dom.dom_node(node);
return goog.dom.removeChildren(G__35550);
});
shadow.dom.remove = (function shadow$dom$remove(node){
if((((!((node == null))))?(((((node.cljs$lang$protocol_mask$partition0$ & (8388608))) || ((cljs.core.PROTOCOL_SENTINEL === node.cljs$core$ISeqable$))))?true:false):false)){
var seq__35554 = cljs.core.seq(node);
var chunk__35555 = null;
var count__35556 = (0);
var i__35557 = (0);
while(true){
if((i__35557 < count__35556)){
var n = chunk__35555.cljs$core$IIndexed$_nth$arity$2(null,i__35557);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__36192 = seq__35554;
var G__36193 = chunk__35555;
var G__36194 = count__35556;
var G__36195 = (i__35557 + (1));
seq__35554 = G__36192;
chunk__35555 = G__36193;
count__35556 = G__36194;
i__35557 = G__36195;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__35554);
if(temp__5735__auto__){
var seq__35554__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__35554__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__35554__$1);
var G__36199 = cljs.core.chunk_rest(seq__35554__$1);
var G__36200 = c__4550__auto__;
var G__36201 = cljs.core.count(c__4550__auto__);
var G__36202 = (0);
seq__35554 = G__36199;
chunk__35555 = G__36200;
count__35556 = G__36201;
i__35557 = G__36202;
continue;
} else {
var n = cljs.core.first(seq__35554__$1);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__36203 = cljs.core.next(seq__35554__$1);
var G__36204 = null;
var G__36205 = (0);
var G__36206 = (0);
seq__35554 = G__36203;
chunk__35555 = G__36204;
count__35556 = G__36205;
i__35557 = G__36206;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return goog.dom.removeNode(node);
}
});
shadow.dom.replace_node = (function shadow$dom$replace_node(old,new$){
var G__35565 = shadow.dom.dom_node(new$);
var G__35566 = shadow.dom.dom_node(old);
return goog.dom.replaceNode(G__35565,G__35566);
});
shadow.dom.text = (function shadow$dom$text(var_args){
var G__35570 = arguments.length;
switch (G__35570) {
case 2:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.text.cljs$core$IFn$_invoke$arity$2 = (function (el,new_text){
return shadow.dom.dom_node(el).innerText = new_text;
});

shadow.dom.text.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.dom_node(el).innerText;
});

shadow.dom.text.cljs$lang$maxFixedArity = 2;

shadow.dom.check = (function shadow$dom$check(var_args){
var G__35573 = arguments.length;
switch (G__35573) {
case 1:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.check.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2(el,true);
});

shadow.dom.check.cljs$core$IFn$_invoke$arity$2 = (function (el,checked){
return shadow.dom.dom_node(el).checked = checked;
});

shadow.dom.check.cljs$lang$maxFixedArity = 2;

shadow.dom.checked_QMARK_ = (function shadow$dom$checked_QMARK_(el){
return shadow.dom.dom_node(el).checked;
});
shadow.dom.form_elements = (function shadow$dom$form_elements(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).elements));
});
shadow.dom.children = (function shadow$dom$children(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).children));
});
shadow.dom.child_nodes = (function shadow$dom$child_nodes(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).childNodes));
});
shadow.dom.attr = (function shadow$dom$attr(var_args){
var G__35585 = arguments.length;
switch (G__35585) {
case 2:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.attr.cljs$core$IFn$_invoke$arity$2 = (function (el,key){
return shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
});

shadow.dom.attr.cljs$core$IFn$_invoke$arity$3 = (function (el,key,default$){
var or__4131__auto__ = shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return default$;
}
});

shadow.dom.attr.cljs$lang$maxFixedArity = 3;

shadow.dom.del_attr = (function shadow$dom$del_attr(el,key){
return shadow.dom.dom_node(el).removeAttribute(cljs.core.name(key));
});
shadow.dom.data = (function shadow$dom$data(el,key){
return shadow.dom.dom_node(el).getAttribute(["data-",cljs.core.name(key)].join(''));
});
shadow.dom.set_data = (function shadow$dom$set_data(el,key,value){
return shadow.dom.dom_node(el).setAttribute(["data-",cljs.core.name(key)].join(''),cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));
});
shadow.dom.set_html = (function shadow$dom$set_html(node,text){
return shadow.dom.dom_node(node).innerHTML = text;
});
shadow.dom.get_html = (function shadow$dom$get_html(node){
return shadow.dom.dom_node(node).innerHTML;
});
shadow.dom.fragment = (function shadow$dom$fragment(var_args){
var args__4736__auto__ = [];
var len__4730__auto___36218 = arguments.length;
var i__4731__auto___36219 = (0);
while(true){
if((i__4731__auto___36219 < len__4730__auto___36218)){
args__4736__auto__.push((arguments[i__4731__auto___36219]));

var G__36220 = (i__4731__auto___36219 + (1));
i__4731__auto___36219 = G__36220;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((0) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((0)),(0),null)):null);
return shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic(argseq__4737__auto__);
});

shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic = (function (nodes){
var fragment = document.createDocumentFragment();
var seq__35606_36221 = cljs.core.seq(nodes);
var chunk__35607_36222 = null;
var count__35608_36223 = (0);
var i__35609_36224 = (0);
while(true){
if((i__35609_36224 < count__35608_36223)){
var node_36225 = chunk__35607_36222.cljs$core$IIndexed$_nth$arity$2(null,i__35609_36224);
fragment.appendChild(shadow.dom._to_dom(node_36225));


var G__36226 = seq__35606_36221;
var G__36227 = chunk__35607_36222;
var G__36228 = count__35608_36223;
var G__36229 = (i__35609_36224 + (1));
seq__35606_36221 = G__36226;
chunk__35607_36222 = G__36227;
count__35608_36223 = G__36228;
i__35609_36224 = G__36229;
continue;
} else {
var temp__5735__auto___36230 = cljs.core.seq(seq__35606_36221);
if(temp__5735__auto___36230){
var seq__35606_36231__$1 = temp__5735__auto___36230;
if(cljs.core.chunked_seq_QMARK_(seq__35606_36231__$1)){
var c__4550__auto___36232 = cljs.core.chunk_first(seq__35606_36231__$1);
var G__36233 = cljs.core.chunk_rest(seq__35606_36231__$1);
var G__36234 = c__4550__auto___36232;
var G__36235 = cljs.core.count(c__4550__auto___36232);
var G__36236 = (0);
seq__35606_36221 = G__36233;
chunk__35607_36222 = G__36234;
count__35608_36223 = G__36235;
i__35609_36224 = G__36236;
continue;
} else {
var node_36237 = cljs.core.first(seq__35606_36231__$1);
fragment.appendChild(shadow.dom._to_dom(node_36237));


var G__36238 = cljs.core.next(seq__35606_36231__$1);
var G__36239 = null;
var G__36240 = (0);
var G__36241 = (0);
seq__35606_36221 = G__36238;
chunk__35607_36222 = G__36239;
count__35608_36223 = G__36240;
i__35609_36224 = G__36241;
continue;
}
} else {
}
}
break;
}

return (new shadow.dom.NativeColl(fragment));
});

shadow.dom.fragment.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
shadow.dom.fragment.cljs$lang$applyTo = (function (seq35601){
var self__4718__auto__ = this;
return self__4718__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq35601));
});

/**
 * given a html string, eval all <script> tags and return the html without the scripts
 * don't do this for everything, only content you trust.
 */
shadow.dom.eval_scripts = (function shadow$dom$eval_scripts(s){
var scripts = cljs.core.re_seq(/<script[^>]*?>(.+?)<\/script>/,s);
var seq__35618_36245 = cljs.core.seq(scripts);
var chunk__35619_36246 = null;
var count__35620_36247 = (0);
var i__35621_36248 = (0);
while(true){
if((i__35621_36248 < count__35620_36247)){
var vec__35628_36249 = chunk__35619_36246.cljs$core$IIndexed$_nth$arity$2(null,i__35621_36248);
var script_tag_36250 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35628_36249,(0),null);
var script_body_36251 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35628_36249,(1),null);
eval(script_body_36251);


var G__36252 = seq__35618_36245;
var G__36253 = chunk__35619_36246;
var G__36254 = count__35620_36247;
var G__36255 = (i__35621_36248 + (1));
seq__35618_36245 = G__36252;
chunk__35619_36246 = G__36253;
count__35620_36247 = G__36254;
i__35621_36248 = G__36255;
continue;
} else {
var temp__5735__auto___36256 = cljs.core.seq(seq__35618_36245);
if(temp__5735__auto___36256){
var seq__35618_36257__$1 = temp__5735__auto___36256;
if(cljs.core.chunked_seq_QMARK_(seq__35618_36257__$1)){
var c__4550__auto___36258 = cljs.core.chunk_first(seq__35618_36257__$1);
var G__36259 = cljs.core.chunk_rest(seq__35618_36257__$1);
var G__36260 = c__4550__auto___36258;
var G__36261 = cljs.core.count(c__4550__auto___36258);
var G__36262 = (0);
seq__35618_36245 = G__36259;
chunk__35619_36246 = G__36260;
count__35620_36247 = G__36261;
i__35621_36248 = G__36262;
continue;
} else {
var vec__35632_36263 = cljs.core.first(seq__35618_36257__$1);
var script_tag_36264 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35632_36263,(0),null);
var script_body_36265 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35632_36263,(1),null);
eval(script_body_36265);


var G__36266 = cljs.core.next(seq__35618_36257__$1);
var G__36267 = null;
var G__36268 = (0);
var G__36269 = (0);
seq__35618_36245 = G__36266;
chunk__35619_36246 = G__36267;
count__35620_36247 = G__36268;
i__35621_36248 = G__36269;
continue;
}
} else {
}
}
break;
}

return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (scripts){
return (function (s__$1,p__35638){
var vec__35639 = p__35638;
var script_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35639,(0),null);
var script_body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35639,(1),null);
return clojure.string.replace(s__$1,script_tag,"");
});})(scripts))
,s,scripts);
});
shadow.dom.str__GT_fragment = (function shadow$dom$str__GT_fragment(s){
var el = document.createElement("div");
el.innerHTML = s;

return (new shadow.dom.NativeColl(goog.dom.childrenToNode_(document,el)));
});
shadow.dom.node_name = (function shadow$dom$node_name(el){
return shadow.dom.dom_node(el).nodeName;
});
shadow.dom.ancestor_by_class = (function shadow$dom$ancestor_by_class(el,cls){
var G__35650 = shadow.dom.dom_node(el);
var G__35651 = cls;
return goog.dom.getAncestorByClass(G__35650,G__35651);
});
shadow.dom.ancestor_by_tag = (function shadow$dom$ancestor_by_tag(var_args){
var G__35655 = arguments.length;
switch (G__35655) {
case 2:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2 = (function (el,tag){
var G__35658 = shadow.dom.dom_node(el);
var G__35659 = cljs.core.name(tag);
return goog.dom.getAncestorByTagNameAndClass(G__35658,G__35659);
});

shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3 = (function (el,tag,cls){
var G__35661 = shadow.dom.dom_node(el);
var G__35662 = cljs.core.name(tag);
var G__35663 = cljs.core.name(cls);
return goog.dom.getAncestorByTagNameAndClass(G__35661,G__35662,G__35663);
});

shadow.dom.ancestor_by_tag.cljs$lang$maxFixedArity = 3;

shadow.dom.get_value = (function shadow$dom$get_value(dom){
var G__35667 = shadow.dom.dom_node(dom);
return goog.dom.forms.getValue(G__35667);
});
shadow.dom.set_value = (function shadow$dom$set_value(dom,value){
var G__35670 = shadow.dom.dom_node(dom);
var G__35671 = value;
return goog.dom.forms.setValue(G__35670,G__35671);
});
shadow.dom.px = (function shadow$dom$px(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((value | (0))),"px"].join('');
});
shadow.dom.pct = (function shadow$dom$pct(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"%"].join('');
});
shadow.dom.remove_style_STAR_ = (function shadow$dom$remove_style_STAR_(el,style){
return el.style.removeProperty(cljs.core.name(style));
});
shadow.dom.remove_style = (function shadow$dom$remove_style(el,style){
var el__$1 = shadow.dom.dom_node(el);
return shadow.dom.remove_style_STAR_(el__$1,style);
});
shadow.dom.remove_styles = (function shadow$dom$remove_styles(el,style_keys){
var el__$1 = shadow.dom.dom_node(el);
var seq__35685 = cljs.core.seq(style_keys);
var chunk__35686 = null;
var count__35687 = (0);
var i__35688 = (0);
while(true){
if((i__35688 < count__35687)){
var it = chunk__35686.cljs$core$IIndexed$_nth$arity$2(null,i__35688);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__36277 = seq__35685;
var G__36278 = chunk__35686;
var G__36279 = count__35687;
var G__36280 = (i__35688 + (1));
seq__35685 = G__36277;
chunk__35686 = G__36278;
count__35687 = G__36279;
i__35688 = G__36280;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__35685);
if(temp__5735__auto__){
var seq__35685__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__35685__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__35685__$1);
var G__36281 = cljs.core.chunk_rest(seq__35685__$1);
var G__36282 = c__4550__auto__;
var G__36283 = cljs.core.count(c__4550__auto__);
var G__36284 = (0);
seq__35685 = G__36281;
chunk__35686 = G__36282;
count__35687 = G__36283;
i__35688 = G__36284;
continue;
} else {
var it = cljs.core.first(seq__35685__$1);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__36285 = cljs.core.next(seq__35685__$1);
var G__36286 = null;
var G__36287 = (0);
var G__36288 = (0);
seq__35685 = G__36285;
chunk__35686 = G__36286;
count__35687 = G__36287;
i__35688 = G__36288;
continue;
}
} else {
return null;
}
}
break;
}
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Coordinate = (function (x,y,__meta,__extmap,__hash){
this.x = x;
this.y = y;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4385__auto__,k__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
return this__4385__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4386__auto__,null);
});

shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4387__auto__,k35699,else__4388__auto__){
var self__ = this;
var this__4387__auto____$1 = this;
var G__35710 = k35699;
var G__35710__$1 = (((G__35710 instanceof cljs.core.Keyword))?G__35710.fqn:null);
switch (G__35710__$1) {
case "x":
return self__.x;

break;
case "y":
return self__.y;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k35699,else__4388__auto__);

}
});

shadow.dom.Coordinate.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4404__auto__,f__4405__auto__,init__4406__auto__){
var self__ = this;
var this__4404__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (this__4404__auto____$1){
return (function (ret__4407__auto__,p__35713){
var vec__35714 = p__35713;
var k__4408__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35714,(0),null);
var v__4409__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35714,(1),null);
return (f__4405__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4405__auto__.cljs$core$IFn$_invoke$arity$3(ret__4407__auto__,k__4408__auto__,v__4409__auto__) : f__4405__auto__.call(null,ret__4407__auto__,k__4408__auto__,v__4409__auto__));
});})(this__4404__auto____$1))
,init__4406__auto__,this__4404__auto____$1);
});

shadow.dom.Coordinate.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4399__auto__,writer__4400__auto__,opts__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
var pr_pair__4402__auto__ = ((function (this__4399__auto____$1){
return (function (keyval__4403__auto__){
return cljs.core.pr_sequential_writer(writer__4400__auto__,cljs.core.pr_writer,""," ","",opts__4401__auto__,keyval__4403__auto__);
});})(this__4399__auto____$1))
;
return cljs.core.pr_sequential_writer(writer__4400__auto__,pr_pair__4402__auto__,"#shadow.dom.Coordinate{",", ","}",opts__4401__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"x","x",2099068185),self__.x],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"y","y",-1757859776),self__.y],null))], null),self__.__extmap));
});

shadow.dom.Coordinate.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__35698){
var self__ = this;
var G__35698__$1 = this;
return (new cljs.core.RecordIter((0),G__35698__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"y","y",-1757859776)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
});

shadow.dom.Coordinate.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4383__auto__){
var self__ = this;
var this__4383__auto____$1 = this;
return self__.__meta;
});

shadow.dom.Coordinate.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4380__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,self__.__extmap,self__.__hash));
});

shadow.dom.Coordinate.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4389__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
});

shadow.dom.Coordinate.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4381__auto__){
var self__ = this;
var this__4381__auto____$1 = this;
var h__4243__auto__ = self__.__hash;
if((!((h__4243__auto__ == null)))){
return h__4243__auto__;
} else {
var h__4243__auto____$1 = (function (){var fexpr__35721 = ((function (h__4243__auto__,this__4381__auto____$1){
return (function (coll__4382__auto__){
return (145542109 ^ cljs.core.hash_unordered_coll(coll__4382__auto__));
});})(h__4243__auto__,this__4381__auto____$1))
;
return fexpr__35721(this__4381__auto____$1);
})();
self__.__hash = h__4243__auto____$1;

return h__4243__auto____$1;
}
});

shadow.dom.Coordinate.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this35700,other35701){
var self__ = this;
var this35700__$1 = this;
return (((!((other35701 == null)))) && ((this35700__$1.constructor === other35701.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35700__$1.x,other35701.x)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35700__$1.y,other35701.y)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35700__$1.__extmap,other35701.__extmap)));
});

shadow.dom.Coordinate.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4394__auto__,k__4395__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"y","y",-1757859776),null,new cljs.core.Keyword(null,"x","x",2099068185),null], null), null),k__4395__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4394__auto____$1),self__.__meta),k__4395__auto__);
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4395__auto__)),null));
}
});

shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4392__auto__,k__4393__auto__,G__35698){
var self__ = this;
var this__4392__auto____$1 = this;
var pred__35723 = cljs.core.keyword_identical_QMARK_;
var expr__35724 = k__4393__auto__;
if(cljs.core.truth_((function (){var G__35726 = new cljs.core.Keyword(null,"x","x",2099068185);
var G__35727 = expr__35724;
return (pred__35723.cljs$core$IFn$_invoke$arity$2 ? pred__35723.cljs$core$IFn$_invoke$arity$2(G__35726,G__35727) : pred__35723.call(null,G__35726,G__35727));
})())){
return (new shadow.dom.Coordinate(G__35698,self__.y,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((function (){var G__35728 = new cljs.core.Keyword(null,"y","y",-1757859776);
var G__35729 = expr__35724;
return (pred__35723.cljs$core$IFn$_invoke$arity$2 ? pred__35723.cljs$core$IFn$_invoke$arity$2(G__35728,G__35729) : pred__35723.call(null,G__35728,G__35729));
})())){
return (new shadow.dom.Coordinate(self__.x,G__35698,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4393__auto__,G__35698),null));
}
}
});

shadow.dom.Coordinate.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4397__auto__){
var self__ = this;
var this__4397__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"x","x",2099068185),self__.x,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"y","y",-1757859776),self__.y,null))], null),self__.__extmap));
});

shadow.dom.Coordinate.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4384__auto__,G__35698){
var self__ = this;
var this__4384__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,G__35698,self__.__extmap,self__.__hash));
});

shadow.dom.Coordinate.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4390__auto__,entry__4391__auto__){
var self__ = this;
var this__4390__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4391__auto__)){
return this__4390__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(0)),cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4390__auto____$1,entry__4391__auto__);
}
});

shadow.dom.Coordinate.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"x","x",-555367584,null),new cljs.core.Symbol(null,"y","y",-117328249,null)], null);
});

shadow.dom.Coordinate.cljs$lang$type = true;

shadow.dom.Coordinate.cljs$lang$ctorPrSeq = (function (this__4428__auto__){
return (new cljs.core.List(null,"shadow.dom/Coordinate",null,(1),null));
});

shadow.dom.Coordinate.cljs$lang$ctorPrWriter = (function (this__4428__auto__,writer__4429__auto__){
return cljs.core._write(writer__4429__auto__,"shadow.dom/Coordinate");
});

/**
 * Positional factory function for shadow.dom/Coordinate.
 */
shadow.dom.__GT_Coordinate = (function shadow$dom$__GT_Coordinate(x,y){
return (new shadow.dom.Coordinate(x,y,null,null,null));
});

/**
 * Factory function for shadow.dom/Coordinate, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Coordinate = (function shadow$dom$map__GT_Coordinate(G__35705){
var extmap__4424__auto__ = (function (){var G__35731 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__35705,new cljs.core.Keyword(null,"x","x",2099068185),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776)], 0));
if(cljs.core.record_QMARK_(G__35705)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__35731);
} else {
return G__35731;
}
})();
return (new shadow.dom.Coordinate(new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(G__35705),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(G__35705),null,cljs.core.not_empty(extmap__4424__auto__),null));
});

shadow.dom.get_position = (function shadow$dom$get_position(el){
var pos = (function (){var G__35732 = shadow.dom.dom_node(el);
return goog.style.getPosition(G__35732);
})();
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_client_position = (function shadow$dom$get_client_position(el){
var pos = (function (){var G__35733 = shadow.dom.dom_node(el);
return goog.style.getClientPosition(G__35733);
})();
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_page_offset = (function shadow$dom$get_page_offset(el){
var pos = (function (){var G__35734 = shadow.dom.dom_node(el);
return goog.style.getPageOffset(G__35734);
})();
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Size = (function (w,h,__meta,__extmap,__hash){
this.w = w;
this.h = h;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4385__auto__,k__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
return this__4385__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4386__auto__,null);
});

shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4387__auto__,k35736,else__4388__auto__){
var self__ = this;
var this__4387__auto____$1 = this;
var G__35744 = k35736;
var G__35744__$1 = (((G__35744 instanceof cljs.core.Keyword))?G__35744.fqn:null);
switch (G__35744__$1) {
case "w":
return self__.w;

break;
case "h":
return self__.h;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k35736,else__4388__auto__);

}
});

shadow.dom.Size.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4404__auto__,f__4405__auto__,init__4406__auto__){
var self__ = this;
var this__4404__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (this__4404__auto____$1){
return (function (ret__4407__auto__,p__35746){
var vec__35747 = p__35746;
var k__4408__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35747,(0),null);
var v__4409__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35747,(1),null);
return (f__4405__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4405__auto__.cljs$core$IFn$_invoke$arity$3(ret__4407__auto__,k__4408__auto__,v__4409__auto__) : f__4405__auto__.call(null,ret__4407__auto__,k__4408__auto__,v__4409__auto__));
});})(this__4404__auto____$1))
,init__4406__auto__,this__4404__auto____$1);
});

shadow.dom.Size.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4399__auto__,writer__4400__auto__,opts__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
var pr_pair__4402__auto__ = ((function (this__4399__auto____$1){
return (function (keyval__4403__auto__){
return cljs.core.pr_sequential_writer(writer__4400__auto__,cljs.core.pr_writer,""," ","",opts__4401__auto__,keyval__4403__auto__);
});})(this__4399__auto____$1))
;
return cljs.core.pr_sequential_writer(writer__4400__auto__,pr_pair__4402__auto__,"#shadow.dom.Size{",", ","}",opts__4401__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"w","w",354169001),self__.w],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"h","h",1109658740),self__.h],null))], null),self__.__extmap));
});

shadow.dom.Size.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__35735){
var self__ = this;
var G__35735__$1 = this;
return (new cljs.core.RecordIter((0),G__35735__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"w","w",354169001),new cljs.core.Keyword(null,"h","h",1109658740)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
});

shadow.dom.Size.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4383__auto__){
var self__ = this;
var this__4383__auto____$1 = this;
return self__.__meta;
});

shadow.dom.Size.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4380__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,self__.__extmap,self__.__hash));
});

shadow.dom.Size.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4389__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
});

shadow.dom.Size.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4381__auto__){
var self__ = this;
var this__4381__auto____$1 = this;
var h__4243__auto__ = self__.__hash;
if((!((h__4243__auto__ == null)))){
return h__4243__auto__;
} else {
var h__4243__auto____$1 = (function (){var fexpr__35761 = ((function (h__4243__auto__,this__4381__auto____$1){
return (function (coll__4382__auto__){
return (-1228019642 ^ cljs.core.hash_unordered_coll(coll__4382__auto__));
});})(h__4243__auto__,this__4381__auto____$1))
;
return fexpr__35761(this__4381__auto____$1);
})();
self__.__hash = h__4243__auto____$1;

return h__4243__auto____$1;
}
});

shadow.dom.Size.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this35737,other35738){
var self__ = this;
var this35737__$1 = this;
return (((!((other35738 == null)))) && ((this35737__$1.constructor === other35738.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35737__$1.w,other35738.w)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35737__$1.h,other35738.h)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35737__$1.__extmap,other35738.__extmap)));
});

shadow.dom.Size.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4394__auto__,k__4395__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"w","w",354169001),null,new cljs.core.Keyword(null,"h","h",1109658740),null], null), null),k__4395__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4394__auto____$1),self__.__meta),k__4395__auto__);
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4395__auto__)),null));
}
});

shadow.dom.Size.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4392__auto__,k__4393__auto__,G__35735){
var self__ = this;
var this__4392__auto____$1 = this;
var pred__35765 = cljs.core.keyword_identical_QMARK_;
var expr__35766 = k__4393__auto__;
if(cljs.core.truth_((function (){var G__35768 = new cljs.core.Keyword(null,"w","w",354169001);
var G__35769 = expr__35766;
return (pred__35765.cljs$core$IFn$_invoke$arity$2 ? pred__35765.cljs$core$IFn$_invoke$arity$2(G__35768,G__35769) : pred__35765.call(null,G__35768,G__35769));
})())){
return (new shadow.dom.Size(G__35735,self__.h,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((function (){var G__35771 = new cljs.core.Keyword(null,"h","h",1109658740);
var G__35772 = expr__35766;
return (pred__35765.cljs$core$IFn$_invoke$arity$2 ? pred__35765.cljs$core$IFn$_invoke$arity$2(G__35771,G__35772) : pred__35765.call(null,G__35771,G__35772));
})())){
return (new shadow.dom.Size(self__.w,G__35735,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4393__auto__,G__35735),null));
}
}
});

shadow.dom.Size.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4397__auto__){
var self__ = this;
var this__4397__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"w","w",354169001),self__.w,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"h","h",1109658740),self__.h,null))], null),self__.__extmap));
});

shadow.dom.Size.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4384__auto__,G__35735){
var self__ = this;
var this__4384__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,G__35735,self__.__extmap,self__.__hash));
});

shadow.dom.Size.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4390__auto__,entry__4391__auto__){
var self__ = this;
var this__4390__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4391__auto__)){
return this__4390__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(0)),cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4391__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4390__auto____$1,entry__4391__auto__);
}
});

shadow.dom.Size.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"w","w",1994700528,null),new cljs.core.Symbol(null,"h","h",-1544777029,null)], null);
});

shadow.dom.Size.cljs$lang$type = true;

shadow.dom.Size.cljs$lang$ctorPrSeq = (function (this__4428__auto__){
return (new cljs.core.List(null,"shadow.dom/Size",null,(1),null));
});

shadow.dom.Size.cljs$lang$ctorPrWriter = (function (this__4428__auto__,writer__4429__auto__){
return cljs.core._write(writer__4429__auto__,"shadow.dom/Size");
});

/**
 * Positional factory function for shadow.dom/Size.
 */
shadow.dom.__GT_Size = (function shadow$dom$__GT_Size(w,h){
return (new shadow.dom.Size(w,h,null,null,null));
});

/**
 * Factory function for shadow.dom/Size, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Size = (function shadow$dom$map__GT_Size(G__35739){
var extmap__4424__auto__ = (function (){var G__35778 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__35739,new cljs.core.Keyword(null,"w","w",354169001),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"h","h",1109658740)], 0));
if(cljs.core.record_QMARK_(G__35739)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__35778);
} else {
return G__35778;
}
})();
return (new shadow.dom.Size(new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(G__35739),new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(G__35739),null,cljs.core.not_empty(extmap__4424__auto__),null));
});

shadow.dom.size__GT_clj = (function shadow$dom$size__GT_clj(size){
return (new shadow.dom.Size(size.width,size.height,null,null,null));
});
shadow.dom.get_size = (function shadow$dom$get_size(el){
return shadow.dom.size__GT_clj((function (){var G__35782 = shadow.dom.dom_node(el);
return goog.style.getSize(G__35782);
})());
});
shadow.dom.get_height = (function shadow$dom$get_height(el){
return new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(shadow.dom.get_size(el));
});
shadow.dom.get_viewport_size = (function shadow$dom$get_viewport_size(){
return shadow.dom.size__GT_clj(goog.dom.getViewportSize());
});
shadow.dom.first_child = (function shadow$dom$first_child(el){
return (shadow.dom.dom_node(el).children[(0)]);
});
shadow.dom.select_option_values = (function shadow$dom$select_option_values(el){
var native$ = shadow.dom.dom_node(el);
var opts = (native$["options"]);
var a__4604__auto__ = opts;
var l__4605__auto__ = a__4604__auto__.length;
var i = (0);
var ret = cljs.core.PersistentVector.EMPTY;
while(true){
if((i < l__4605__auto__)){
var G__36350 = (i + (1));
var G__36351 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,(opts[i]["value"]));
i = G__36350;
ret = G__36351;
continue;
} else {
return ret;
}
break;
}
});
shadow.dom.build_url = (function shadow$dom$build_url(path,query_params){
if(cljs.core.empty_QMARK_(query_params)){
return path;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(path),"?",cljs.core.str.cljs$core$IFn$_invoke$arity$1(clojure.string.join.cljs$core$IFn$_invoke$arity$2("&",cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__35792){
var vec__35794 = p__35792;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35794,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35794,(1),null);
return [cljs.core.name(k),"=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(encodeURIComponent(cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)))].join('');
}),query_params)))].join('');
}
});
shadow.dom.redirect = (function shadow$dom$redirect(var_args){
var G__35800 = arguments.length;
switch (G__35800) {
case 1:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1 = (function (path){
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2(path,cljs.core.PersistentArrayMap.EMPTY);
});

shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2 = (function (path,query_params){
return (document["location"]["href"] = shadow.dom.build_url(path,query_params));
});

shadow.dom.redirect.cljs$lang$maxFixedArity = 2;

shadow.dom.reload_BANG_ = (function shadow$dom$reload_BANG_(){
return document.location.href = document.location.href;
});
shadow.dom.tag_name = (function shadow$dom$tag_name(el){
var dom = shadow.dom.dom_node(el);
return dom.tagName;
});
shadow.dom.insert_after = (function shadow$dom$insert_after(ref,new$){
var new_node = shadow.dom.dom_node(new$);
var G__35814_36355 = new_node;
var G__35815_36356 = shadow.dom.dom_node(ref);
goog.dom.insertSiblingAfter(G__35814_36355,G__35815_36356);

return new_node;
});
shadow.dom.insert_before = (function shadow$dom$insert_before(ref,new$){
var new_node = shadow.dom.dom_node(new$);
var G__35817_36357 = new_node;
var G__35818_36358 = shadow.dom.dom_node(ref);
goog.dom.insertSiblingBefore(G__35817_36357,G__35818_36358);

return new_node;
});
shadow.dom.insert_first = (function shadow$dom$insert_first(ref,new$){
var temp__5733__auto__ = shadow.dom.dom_node(ref).firstChild;
if(cljs.core.truth_(temp__5733__auto__)){
var child = temp__5733__auto__;
return shadow.dom.insert_before(child,new$);
} else {
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2(ref,new$);
}
});
shadow.dom.index_of = (function shadow$dom$index_of(el){
var el__$1 = shadow.dom.dom_node(el);
var i = (0);
while(true){
var ps = el__$1.previousSibling;
if((ps == null)){
return i;
} else {
var G__36361 = ps;
var G__36362 = (i + (1));
el__$1 = G__36361;
i = G__36362;
continue;
}
break;
}
});
shadow.dom.get_parent = (function shadow$dom$get_parent(el){
var G__35824 = shadow.dom.dom_node(el);
return goog.dom.getParentElement(G__35824);
});
shadow.dom.parents = (function shadow$dom$parents(el){
var parent = shadow.dom.get_parent(el);
if(cljs.core.truth_(parent)){
return cljs.core.cons(parent,(new cljs.core.LazySeq(null,((function (parent){
return (function (){
return (shadow.dom.parents.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.parents.cljs$core$IFn$_invoke$arity$1(parent) : shadow.dom.parents.call(null,parent));
});})(parent))
,null,null)));
} else {
return null;
}
});
shadow.dom.matches = (function shadow$dom$matches(el,sel){
return shadow.dom.dom_node(el).matches(sel);
});
shadow.dom.get_next_sibling = (function shadow$dom$get_next_sibling(el){
var G__35829 = shadow.dom.dom_node(el);
return goog.dom.getNextElementSibling(G__35829);
});
shadow.dom.get_previous_sibling = (function shadow$dom$get_previous_sibling(el){
var G__35831 = shadow.dom.dom_node(el);
return goog.dom.getPreviousElementSibling(G__35831);
});
shadow.dom.xmlns = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, ["svg","http://www.w3.org/2000/svg","xlink","http://www.w3.org/1999/xlink"], null));
shadow.dom.create_svg_node = (function shadow$dom$create_svg_node(tag_def,props){
var vec__35835 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35835,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35835,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35835,(2),null);
var el = document.createElementNS("http://www.w3.org/2000/svg",tag_name);
if(cljs.core.truth_(tag_id)){
el.setAttribute("id",tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
el.setAttribute("class",shadow.dom.merge_class_string(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(props),tag_classes));
} else {
}

var seq__35839_36364 = cljs.core.seq(props);
var chunk__35840_36365 = null;
var count__35841_36366 = (0);
var i__35842_36367 = (0);
while(true){
if((i__35842_36367 < count__35841_36366)){
var vec__35852_36368 = chunk__35840_36365.cljs$core$IIndexed$_nth$arity$2(null,i__35842_36367);
var k_36369 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35852_36368,(0),null);
var v_36370 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35852_36368,(1),null);
el.setAttributeNS((function (){var temp__5735__auto__ = cljs.core.namespace(k_36369);
if(cljs.core.truth_(temp__5735__auto__)){
var ns = temp__5735__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_36369),v_36370);


var G__36371 = seq__35839_36364;
var G__36372 = chunk__35840_36365;
var G__36373 = count__35841_36366;
var G__36374 = (i__35842_36367 + (1));
seq__35839_36364 = G__36371;
chunk__35840_36365 = G__36372;
count__35841_36366 = G__36373;
i__35842_36367 = G__36374;
continue;
} else {
var temp__5735__auto___36375 = cljs.core.seq(seq__35839_36364);
if(temp__5735__auto___36375){
var seq__35839_36376__$1 = temp__5735__auto___36375;
if(cljs.core.chunked_seq_QMARK_(seq__35839_36376__$1)){
var c__4550__auto___36377 = cljs.core.chunk_first(seq__35839_36376__$1);
var G__36378 = cljs.core.chunk_rest(seq__35839_36376__$1);
var G__36379 = c__4550__auto___36377;
var G__36380 = cljs.core.count(c__4550__auto___36377);
var G__36381 = (0);
seq__35839_36364 = G__36378;
chunk__35840_36365 = G__36379;
count__35841_36366 = G__36380;
i__35842_36367 = G__36381;
continue;
} else {
var vec__35857_36383 = cljs.core.first(seq__35839_36376__$1);
var k_36384 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35857_36383,(0),null);
var v_36385 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35857_36383,(1),null);
el.setAttributeNS((function (){var temp__5735__auto____$1 = cljs.core.namespace(k_36384);
if(cljs.core.truth_(temp__5735__auto____$1)){
var ns = temp__5735__auto____$1;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_36384),v_36385);


var G__36387 = cljs.core.next(seq__35839_36376__$1);
var G__36388 = null;
var G__36389 = (0);
var G__36390 = (0);
seq__35839_36364 = G__36387;
chunk__35840_36365 = G__36388;
count__35841_36366 = G__36389;
i__35842_36367 = G__36390;
continue;
}
} else {
}
}
break;
}

return el;
});
shadow.dom.svg_node = (function shadow$dom$svg_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$SVGElement$))))?true:false):false)){
return el.shadow$dom$SVGElement$_to_svg$arity$1(null);
} else {
return el;

}
}
});
shadow.dom.make_svg_node = (function shadow$dom$make_svg_node(structure){
var vec__35866 = shadow.dom.destructure_node(shadow.dom.create_svg_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35866,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35866,(1),null);
var seq__35869_36391 = cljs.core.seq(node_children);
var chunk__35871_36392 = null;
var count__35872_36393 = (0);
var i__35873_36394 = (0);
while(true){
if((i__35873_36394 < count__35872_36393)){
var child_struct_36395 = chunk__35871_36392.cljs$core$IIndexed$_nth$arity$2(null,i__35873_36394);
if((!((child_struct_36395 == null)))){
if(typeof child_struct_36395 === 'string'){
var text_36397 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_36397),child_struct_36395].join(''));
} else {
var children_36400 = shadow.dom.svg_node(child_struct_36395);
if(cljs.core.seq_QMARK_(children_36400)){
var seq__35896_36401 = cljs.core.seq(children_36400);
var chunk__35898_36402 = null;
var count__35899_36403 = (0);
var i__35900_36404 = (0);
while(true){
if((i__35900_36404 < count__35899_36403)){
var child_36405 = chunk__35898_36402.cljs$core$IIndexed$_nth$arity$2(null,i__35900_36404);
if(cljs.core.truth_(child_36405)){
node.appendChild(child_36405);


var G__36407 = seq__35896_36401;
var G__36408 = chunk__35898_36402;
var G__36409 = count__35899_36403;
var G__36410 = (i__35900_36404 + (1));
seq__35896_36401 = G__36407;
chunk__35898_36402 = G__36408;
count__35899_36403 = G__36409;
i__35900_36404 = G__36410;
continue;
} else {
var G__36411 = seq__35896_36401;
var G__36412 = chunk__35898_36402;
var G__36413 = count__35899_36403;
var G__36414 = (i__35900_36404 + (1));
seq__35896_36401 = G__36411;
chunk__35898_36402 = G__36412;
count__35899_36403 = G__36413;
i__35900_36404 = G__36414;
continue;
}
} else {
var temp__5735__auto___36415 = cljs.core.seq(seq__35896_36401);
if(temp__5735__auto___36415){
var seq__35896_36416__$1 = temp__5735__auto___36415;
if(cljs.core.chunked_seq_QMARK_(seq__35896_36416__$1)){
var c__4550__auto___36417 = cljs.core.chunk_first(seq__35896_36416__$1);
var G__36419 = cljs.core.chunk_rest(seq__35896_36416__$1);
var G__36420 = c__4550__auto___36417;
var G__36421 = cljs.core.count(c__4550__auto___36417);
var G__36422 = (0);
seq__35896_36401 = G__36419;
chunk__35898_36402 = G__36420;
count__35899_36403 = G__36421;
i__35900_36404 = G__36422;
continue;
} else {
var child_36423 = cljs.core.first(seq__35896_36416__$1);
if(cljs.core.truth_(child_36423)){
node.appendChild(child_36423);


var G__36428 = cljs.core.next(seq__35896_36416__$1);
var G__36429 = null;
var G__36430 = (0);
var G__36431 = (0);
seq__35896_36401 = G__36428;
chunk__35898_36402 = G__36429;
count__35899_36403 = G__36430;
i__35900_36404 = G__36431;
continue;
} else {
var G__36432 = cljs.core.next(seq__35896_36416__$1);
var G__36433 = null;
var G__36434 = (0);
var G__36435 = (0);
seq__35896_36401 = G__36432;
chunk__35898_36402 = G__36433;
count__35899_36403 = G__36434;
i__35900_36404 = G__36435;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_36400);
}
}


var G__36437 = seq__35869_36391;
var G__36438 = chunk__35871_36392;
var G__36439 = count__35872_36393;
var G__36440 = (i__35873_36394 + (1));
seq__35869_36391 = G__36437;
chunk__35871_36392 = G__36438;
count__35872_36393 = G__36439;
i__35873_36394 = G__36440;
continue;
} else {
var G__36441 = seq__35869_36391;
var G__36442 = chunk__35871_36392;
var G__36443 = count__35872_36393;
var G__36444 = (i__35873_36394 + (1));
seq__35869_36391 = G__36441;
chunk__35871_36392 = G__36442;
count__35872_36393 = G__36443;
i__35873_36394 = G__36444;
continue;
}
} else {
var temp__5735__auto___36445 = cljs.core.seq(seq__35869_36391);
if(temp__5735__auto___36445){
var seq__35869_36446__$1 = temp__5735__auto___36445;
if(cljs.core.chunked_seq_QMARK_(seq__35869_36446__$1)){
var c__4550__auto___36447 = cljs.core.chunk_first(seq__35869_36446__$1);
var G__36448 = cljs.core.chunk_rest(seq__35869_36446__$1);
var G__36449 = c__4550__auto___36447;
var G__36450 = cljs.core.count(c__4550__auto___36447);
var G__36451 = (0);
seq__35869_36391 = G__36448;
chunk__35871_36392 = G__36449;
count__35872_36393 = G__36450;
i__35873_36394 = G__36451;
continue;
} else {
var child_struct_36452 = cljs.core.first(seq__35869_36446__$1);
if((!((child_struct_36452 == null)))){
if(typeof child_struct_36452 === 'string'){
var text_36453 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_36453),child_struct_36452].join(''));
} else {
var children_36454 = shadow.dom.svg_node(child_struct_36452);
if(cljs.core.seq_QMARK_(children_36454)){
var seq__35902_36455 = cljs.core.seq(children_36454);
var chunk__35904_36456 = null;
var count__35905_36457 = (0);
var i__35906_36458 = (0);
while(true){
if((i__35906_36458 < count__35905_36457)){
var child_36461 = chunk__35904_36456.cljs$core$IIndexed$_nth$arity$2(null,i__35906_36458);
if(cljs.core.truth_(child_36461)){
node.appendChild(child_36461);


var G__36463 = seq__35902_36455;
var G__36464 = chunk__35904_36456;
var G__36465 = count__35905_36457;
var G__36466 = (i__35906_36458 + (1));
seq__35902_36455 = G__36463;
chunk__35904_36456 = G__36464;
count__35905_36457 = G__36465;
i__35906_36458 = G__36466;
continue;
} else {
var G__36467 = seq__35902_36455;
var G__36468 = chunk__35904_36456;
var G__36469 = count__35905_36457;
var G__36470 = (i__35906_36458 + (1));
seq__35902_36455 = G__36467;
chunk__35904_36456 = G__36468;
count__35905_36457 = G__36469;
i__35906_36458 = G__36470;
continue;
}
} else {
var temp__5735__auto___36471__$1 = cljs.core.seq(seq__35902_36455);
if(temp__5735__auto___36471__$1){
var seq__35902_36472__$1 = temp__5735__auto___36471__$1;
if(cljs.core.chunked_seq_QMARK_(seq__35902_36472__$1)){
var c__4550__auto___36473 = cljs.core.chunk_first(seq__35902_36472__$1);
var G__36474 = cljs.core.chunk_rest(seq__35902_36472__$1);
var G__36475 = c__4550__auto___36473;
var G__36476 = cljs.core.count(c__4550__auto___36473);
var G__36477 = (0);
seq__35902_36455 = G__36474;
chunk__35904_36456 = G__36475;
count__35905_36457 = G__36476;
i__35906_36458 = G__36477;
continue;
} else {
var child_36478 = cljs.core.first(seq__35902_36472__$1);
if(cljs.core.truth_(child_36478)){
node.appendChild(child_36478);


var G__36479 = cljs.core.next(seq__35902_36472__$1);
var G__36480 = null;
var G__36481 = (0);
var G__36482 = (0);
seq__35902_36455 = G__36479;
chunk__35904_36456 = G__36480;
count__35905_36457 = G__36481;
i__35906_36458 = G__36482;
continue;
} else {
var G__36483 = cljs.core.next(seq__35902_36472__$1);
var G__36484 = null;
var G__36485 = (0);
var G__36486 = (0);
seq__35902_36455 = G__36483;
chunk__35904_36456 = G__36484;
count__35905_36457 = G__36485;
i__35906_36458 = G__36486;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_36454);
}
}


var G__36487 = cljs.core.next(seq__35869_36446__$1);
var G__36488 = null;
var G__36489 = (0);
var G__36490 = (0);
seq__35869_36391 = G__36487;
chunk__35871_36392 = G__36488;
count__35872_36393 = G__36489;
i__35873_36394 = G__36490;
continue;
} else {
var G__36491 = cljs.core.next(seq__35869_36446__$1);
var G__36492 = null;
var G__36493 = (0);
var G__36494 = (0);
seq__35869_36391 = G__36491;
chunk__35871_36392 = G__36492;
count__35872_36393 = G__36493;
i__35873_36394 = G__36494;
continue;
}
}
} else {
}
}
break;
}

return node;
});
goog.object.set(shadow.dom.SVGElement,"string",true);

var G__35908_36499 = shadow.dom._to_svg;
var G__35909_36500 = "string";
var G__35910_36501 = ((function (G__35908_36499,G__35909_36500){
return (function (this$){
if((this$ instanceof cljs.core.Keyword)){
return shadow.dom.make_svg_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$], null));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("strings cannot be in svgs",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"this","this",-611633625),this$], null));
}
});})(G__35908_36499,G__35909_36500))
;
goog.object.set(G__35908_36499,G__35909_36500,G__35910_36501);

cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_svg_node(this$__$1);
});

cljs.core.LazySeq.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.LazySeq.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_svg,this$__$1);
});

goog.object.set(shadow.dom.SVGElement,"null",true);

var G__35912_36502 = shadow.dom._to_svg;
var G__35913_36503 = "null";
var G__35914_36504 = ((function (G__35912_36502,G__35913_36503){
return (function (_){
return null;
});})(G__35912_36502,G__35913_36503))
;
goog.object.set(G__35912_36502,G__35913_36503,G__35914_36504);
shadow.dom.svg = (function shadow$dom$svg(var_args){
var args__4736__auto__ = [];
var len__4730__auto___36505 = arguments.length;
var i__4731__auto___36506 = (0);
while(true){
if((i__4731__auto___36506 < len__4730__auto___36505)){
args__4736__auto__.push((arguments[i__4731__auto___36506]));

var G__36507 = (i__4731__auto___36506 + (1));
i__4731__auto___36506 = G__36507;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic = (function (attrs,children){
return shadow.dom._to_svg(cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),attrs], null),children)));
});

shadow.dom.svg.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
shadow.dom.svg.cljs$lang$applyTo = (function (seq35918){
var G__35919 = cljs.core.first(seq35918);
var seq35918__$1 = cljs.core.next(seq35918);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__35919,seq35918__$1);
});

/**
 * returns a channel for events on el
 * transform-fn should be a (fn [e el] some-val) where some-val will be put on the chan
 * once-or-cleanup handles the removal of the event handler
 * - true: remove after one event
 * - false: never removed
 * - chan: remove on msg/close
 */
shadow.dom.event_chan = (function shadow$dom$event_chan(var_args){
var G__35929 = arguments.length;
switch (G__35929) {
case 2:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2 = (function (el,event){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,null,false);
});

shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3 = (function (el,event,xf){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,xf,false);
});

shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4 = (function (el,event,xf,once_or_cleanup){
var buf = cljs.core.async.sliding_buffer((1));
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2(buf,xf);
var event_fn = ((function (buf,chan){
return (function shadow$dom$event_fn(e){
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,e);

if(once_or_cleanup === true){
shadow.dom.remove_event_handler(el,event,shadow$dom$event_fn);

return cljs.core.async.close_BANG_(chan);
} else {
return null;
}
});})(buf,chan))
;
var G__35933_36510 = shadow.dom.dom_node(el);
var G__35934_36511 = cljs.core.name(event);
var G__35935_36512 = event_fn;
(shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__35933_36510,G__35934_36511,G__35935_36512) : shadow.dom.dom_listen.call(null,G__35933_36510,G__35934_36511,G__35935_36512));

if(cljs.core.truth_((function (){var and__4120__auto__ = once_or_cleanup;
if(cljs.core.truth_(and__4120__auto__)){
return (!(once_or_cleanup === true));
} else {
return and__4120__auto__;
}
})())){
var c__33535__auto___36513 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto___36513,buf,chan,event_fn){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto___36513,buf,chan,event_fn){
return (function (state_35940){
var state_val_35941 = (state_35940[(1)]);
if((state_val_35941 === (1))){
var state_35940__$1 = state_35940;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_35940__$1,(2),once_or_cleanup);
} else {
if((state_val_35941 === (2))){
var inst_35937 = (state_35940[(2)]);
var inst_35938 = shadow.dom.remove_event_handler(el,event,event_fn);
var state_35940__$1 = (function (){var statearr_35943 = state_35940;
(statearr_35943[(7)] = inst_35937);

return statearr_35943;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_35940__$1,inst_35938);
} else {
return null;
}
}
});})(c__33535__auto___36513,buf,chan,event_fn))
;
return ((function (switch__33434__auto__,c__33535__auto___36513,buf,chan,event_fn){
return (function() {
var shadow$dom$state_machine__33435__auto__ = null;
var shadow$dom$state_machine__33435__auto____0 = (function (){
var statearr_35946 = [null,null,null,null,null,null,null,null];
(statearr_35946[(0)] = shadow$dom$state_machine__33435__auto__);

(statearr_35946[(1)] = (1));

return statearr_35946;
});
var shadow$dom$state_machine__33435__auto____1 = (function (state_35940){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_35940);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e35947){if((e35947 instanceof Object)){
var ex__33438__auto__ = e35947;
var statearr_35950_36521 = state_35940;
(statearr_35950_36521[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_35940);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e35947;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36522 = state_35940;
state_35940 = G__36522;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
shadow$dom$state_machine__33435__auto__ = function(state_35940){
switch(arguments.length){
case 0:
return shadow$dom$state_machine__33435__auto____0.call(this);
case 1:
return shadow$dom$state_machine__33435__auto____1.call(this,state_35940);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
shadow$dom$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = shadow$dom$state_machine__33435__auto____0;
shadow$dom$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = shadow$dom$state_machine__33435__auto____1;
return shadow$dom$state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto___36513,buf,chan,event_fn))
})();
var state__33537__auto__ = (function (){var statearr_35953 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_35953[(6)] = c__33535__auto___36513);

return statearr_35953;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto___36513,buf,chan,event_fn))
);

} else {
}

return chan;
});

shadow.dom.event_chan.cljs$lang$maxFixedArity = 4;


//# sourceMappingURL=shadow.dom.js.map
