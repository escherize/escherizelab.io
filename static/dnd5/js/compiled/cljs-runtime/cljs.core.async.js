goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.ioc_helpers');
goog.require('goog.array');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__33595 = arguments.length;
switch (G__33595) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(f,true);
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33596 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33596 = (function (f,blockable,meta33597){
this.f = f;
this.blockable = blockable;
this.meta33597 = meta33597;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async33596.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33598,meta33597__$1){
var self__ = this;
var _33598__$1 = this;
return (new cljs.core.async.t_cljs$core$async33596(self__.f,self__.blockable,meta33597__$1));
});

cljs.core.async.t_cljs$core$async33596.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33598){
var self__ = this;
var _33598__$1 = this;
return self__.meta33597;
});

cljs.core.async.t_cljs$core$async33596.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async33596.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async33596.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
});

cljs.core.async.t_cljs$core$async33596.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
});

cljs.core.async.t_cljs$core$async33596.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta33597","meta33597",1864834947,null)], null);
});

cljs.core.async.t_cljs$core$async33596.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async33596.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33596";

cljs.core.async.t_cljs$core$async33596.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async33596");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33596.
 */
cljs.core.async.__GT_t_cljs$core$async33596 = (function cljs$core$async$__GT_t_cljs$core$async33596(f__$1,blockable__$1,meta33597){
return (new cljs.core.async.t_cljs$core$async33596(f__$1,blockable__$1,meta33597));
});

}

return (new cljs.core.async.t_cljs$core$async33596(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
});

cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2;

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer(n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__33601 = arguments.length;
switch (G__33601) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,xform,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.cljs$core$IFn$_invoke$arity$3(((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer(buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
});

cljs.core.async.chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__33603 = arguments.length;
switch (G__33603) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1(null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2(xform,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(cljs.core.async.impl.buffers.promise_buffer(),xform,ex_handler);
});

cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout(msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__33605 = arguments.length;
switch (G__33605) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3(port,fn1,true);
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(ret)){
var val_35041 = cljs.core.deref(ret);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_35041) : fn1.call(null,val_35041));
} else {
cljs.core.async.impl.dispatch.run(((function (val_35041,ret){
return (function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_35041) : fn1.call(null,val_35041));
});})(val_35041,ret))
);
}
} else {
}

return null;
});

cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3;

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn1 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn1 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__33607 = arguments.length;
switch (G__33607) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5733__auto__)){
var ret = temp__5733__auto__;
return cljs.core.deref(ret);
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4(port,val,fn1,true);
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(temp__5733__auto__)){
var retb = temp__5733__auto__;
var ret = cljs.core.deref(retb);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
} else {
cljs.core.async.impl.dispatch.run(((function (ret,retb,temp__5733__auto__){
return (function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
});})(ret,retb,temp__5733__auto__))
);
}

return ret;
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4;

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_(port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__4607__auto___35056 = n;
var x_35057 = (0);
while(true){
if((x_35057 < n__4607__auto___35056)){
(a[x_35057] = x_35057);

var G__35058 = (x_35057 + (1));
x_35057 = G__35058;
continue;
} else {
}
break;
}

goog.array.shuffle(a);

return a;
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33608 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33608 = (function (flag,meta33609){
this.flag = flag;
this.meta33609 = meta33609;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async33608.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_33610,meta33609__$1){
var self__ = this;
var _33610__$1 = this;
return (new cljs.core.async.t_cljs$core$async33608(self__.flag,meta33609__$1));
});})(flag))
;

cljs.core.async.t_cljs$core$async33608.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_33610){
var self__ = this;
var _33610__$1 = this;
return self__.meta33609;
});})(flag))
;

cljs.core.async.t_cljs$core$async33608.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async33608.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref(self__.flag);
});})(flag))
;

cljs.core.async.t_cljs$core$async33608.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async33608.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.flag,null);

return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async33608.getBasis = ((function (flag){
return (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta33609","meta33609",-540970328,null)], null);
});})(flag))
;

cljs.core.async.t_cljs$core$async33608.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async33608.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33608";

cljs.core.async.t_cljs$core$async33608.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async33608");
});})(flag))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33608.
 */
cljs.core.async.__GT_t_cljs$core$async33608 = ((function (flag){
return (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async33608(flag__$1,meta33609){
return (new cljs.core.async.t_cljs$core$async33608(flag__$1,meta33609));
});})(flag))
;

}

return (new cljs.core.async.t_cljs$core$async33608(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33611 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33611 = (function (flag,cb,meta33612){
this.flag = flag;
this.cb = cb;
this.meta33612 = meta33612;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async33611.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33613,meta33612__$1){
var self__ = this;
var _33613__$1 = this;
return (new cljs.core.async.t_cljs$core$async33611(self__.flag,self__.cb,meta33612__$1));
});

cljs.core.async.t_cljs$core$async33611.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33613){
var self__ = this;
var _33613__$1 = this;
return self__.meta33612;
});

cljs.core.async.t_cljs$core$async33611.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async33611.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.flag);
});

cljs.core.async.t_cljs$core$async33611.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async33611.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit(self__.flag);

return self__.cb;
});

cljs.core.async.t_cljs$core$async33611.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta33612","meta33612",819460152,null)], null);
});

cljs.core.async.t_cljs$core$async33611.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async33611.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33611";

cljs.core.async.t_cljs$core$async33611.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async33611");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33611.
 */
cljs.core.async.__GT_t_cljs$core$async33611 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async33611(flag__$1,cb__$1,meta33612){
return (new cljs.core.async.t_cljs$core$async33611(flag__$1,cb__$1,meta33612));
});

}

return (new cljs.core.async.t_cljs$core$async33611(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
if((cljs.core.count(ports) > (0))){
} else {
throw (new Error(["Assert failed: ","alts must have at least one channel operation","\n","(pos? (count ports))"].join('')));
}

var flag = cljs.core.async.alt_flag();
var n = cljs.core.count(ports);
var idxs = cljs.core.async.random_array(n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ports,idx);
var wport = ((cljs.core.vector_QMARK_(port))?(port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((0)) : port.call(null,(0))):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = (port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((1)) : port.call(null,(1)));
return cljs.core.async.impl.protocols.put_BANG_(wport,val,cljs.core.async.alt_handler(flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__33614_SHARP_){
var G__33616 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__33614_SHARP_,wport], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__33616) : fret.call(null,G__33616));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.alt_handler(flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__33615_SHARP_){
var G__33617 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__33615_SHARP_,port], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__33617) : fret.call(null,G__33617));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref(vbox),(function (){var or__4131__auto__ = wport;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return port;
}
})()], null));
} else {
var G__35085 = (i + (1));
i = G__35085;
continue;
}
} else {
return null;
}
break;
}
})();
var or__4131__auto__ = ret;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
if(cljs.core.contains_QMARK_(opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5735__auto__ = (function (){var and__4120__auto__ = flag.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1(null);
if(cljs.core.truth_(and__4120__auto__)){
return flag.cljs$core$async$impl$protocols$Handler$commit$arity$1(null);
} else {
return and__4120__auto__;
}
})();
if(cljs.core.truth_(temp__5735__auto__)){
var got = temp__5735__auto__;
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__4736__auto__ = [];
var len__4730__auto___35092 = arguments.length;
var i__4731__auto___35093 = (0);
while(true){
if((i__4731__auto___35093 < len__4730__auto___35092)){
args__4736__auto__.push((arguments[i__4731__auto___35093]));

var G__35094 = (i__4731__auto___35093 + (1));
i__4731__auto___35093 = G__35094;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__33620){
var map__33621 = p__33620;
var map__33621__$1 = (((((!((map__33621 == null))))?(((((map__33621.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33621.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33621):map__33621);
var opts = map__33621__$1;
throw (new Error("alts! used not in (go ...) block"));
});

cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq33618){
var G__33619 = cljs.core.first(seq33618);
var seq33618__$1 = cljs.core.next(seq33618);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__33619,seq33618__$1);
});

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__33624 = arguments.length;
switch (G__33624) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3(from,to,true);
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__33535__auto___35107 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto___35107){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto___35107){
return (function (state_33648){
var state_val_33649 = (state_33648[(1)]);
if((state_val_33649 === (7))){
var inst_33644 = (state_33648[(2)]);
var state_33648__$1 = state_33648;
var statearr_33650_35108 = state_33648__$1;
(statearr_33650_35108[(2)] = inst_33644);

(statearr_33650_35108[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (1))){
var state_33648__$1 = state_33648;
var statearr_33651_35110 = state_33648__$1;
(statearr_33651_35110[(2)] = null);

(statearr_33651_35110[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (4))){
var inst_33627 = (state_33648[(7)]);
var inst_33627__$1 = (state_33648[(2)]);
var inst_33628 = (inst_33627__$1 == null);
var state_33648__$1 = (function (){var statearr_33652 = state_33648;
(statearr_33652[(7)] = inst_33627__$1);

return statearr_33652;
})();
if(cljs.core.truth_(inst_33628)){
var statearr_33653_35111 = state_33648__$1;
(statearr_33653_35111[(1)] = (5));

} else {
var statearr_33654_35112 = state_33648__$1;
(statearr_33654_35112[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (13))){
var state_33648__$1 = state_33648;
var statearr_33655_35114 = state_33648__$1;
(statearr_33655_35114[(2)] = null);

(statearr_33655_35114[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (6))){
var inst_33627 = (state_33648[(7)]);
var state_33648__$1 = state_33648;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33648__$1,(11),to,inst_33627);
} else {
if((state_val_33649 === (3))){
var inst_33646 = (state_33648[(2)]);
var state_33648__$1 = state_33648;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33648__$1,inst_33646);
} else {
if((state_val_33649 === (12))){
var state_33648__$1 = state_33648;
var statearr_33656_35118 = state_33648__$1;
(statearr_33656_35118[(2)] = null);

(statearr_33656_35118[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (2))){
var state_33648__$1 = state_33648;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33648__$1,(4),from);
} else {
if((state_val_33649 === (11))){
var inst_33637 = (state_33648[(2)]);
var state_33648__$1 = state_33648;
if(cljs.core.truth_(inst_33637)){
var statearr_33657_35119 = state_33648__$1;
(statearr_33657_35119[(1)] = (12));

} else {
var statearr_33658_35120 = state_33648__$1;
(statearr_33658_35120[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (9))){
var state_33648__$1 = state_33648;
var statearr_33659_35121 = state_33648__$1;
(statearr_33659_35121[(2)] = null);

(statearr_33659_35121[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (5))){
var state_33648__$1 = state_33648;
if(cljs.core.truth_(close_QMARK_)){
var statearr_33660_35122 = state_33648__$1;
(statearr_33660_35122[(1)] = (8));

} else {
var statearr_33661_35123 = state_33648__$1;
(statearr_33661_35123[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (14))){
var inst_33642 = (state_33648[(2)]);
var state_33648__$1 = state_33648;
var statearr_33662_35126 = state_33648__$1;
(statearr_33662_35126[(2)] = inst_33642);

(statearr_33662_35126[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (10))){
var inst_33634 = (state_33648[(2)]);
var state_33648__$1 = state_33648;
var statearr_33663_35129 = state_33648__$1;
(statearr_33663_35129[(2)] = inst_33634);

(statearr_33663_35129[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33649 === (8))){
var inst_33631 = cljs.core.async.close_BANG_(to);
var state_33648__$1 = state_33648;
var statearr_33664_35139 = state_33648__$1;
(statearr_33664_35139[(2)] = inst_33631);

(statearr_33664_35139[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto___35107))
;
return ((function (switch__33434__auto__,c__33535__auto___35107){
return (function() {
var cljs$core$async$state_machine__33435__auto__ = null;
var cljs$core$async$state_machine__33435__auto____0 = (function (){
var statearr_33665 = [null,null,null,null,null,null,null,null];
(statearr_33665[(0)] = cljs$core$async$state_machine__33435__auto__);

(statearr_33665[(1)] = (1));

return statearr_33665;
});
var cljs$core$async$state_machine__33435__auto____1 = (function (state_33648){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_33648);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e33666){if((e33666 instanceof Object)){
var ex__33438__auto__ = e33666;
var statearr_33667_35145 = state_33648;
(statearr_33667_35145[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33648);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33666;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35146 = state_33648;
state_33648 = G__35146;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$state_machine__33435__auto__ = function(state_33648){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__33435__auto____1.call(this,state_33648);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__33435__auto____0;
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__33435__auto____1;
return cljs$core$async$state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto___35107))
})();
var state__33537__auto__ = (function (){var statearr_33668 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_33668[(6)] = c__33535__auto___35107);

return statearr_33668;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto___35107))
);


return to;
});

cljs.core.async.pipe.cljs$lang$maxFixedArity = 3;

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var results = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var process = ((function (jobs,results){
return (function (p__33669){
var vec__33670 = p__33669;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33670,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33670,(1),null);
var job = vec__33670;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((1),xf,ex_handler);
var c__33535__auto___35147 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto___35147,res,vec__33670,v,p,job,jobs,results){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto___35147,res,vec__33670,v,p,job,jobs,results){
return (function (state_33677){
var state_val_33678 = (state_33677[(1)]);
if((state_val_33678 === (1))){
var state_33677__$1 = state_33677;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33677__$1,(2),res,v);
} else {
if((state_val_33678 === (2))){
var inst_33674 = (state_33677[(2)]);
var inst_33675 = cljs.core.async.close_BANG_(res);
var state_33677__$1 = (function (){var statearr_33679 = state_33677;
(statearr_33679[(7)] = inst_33674);

return statearr_33679;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_33677__$1,inst_33675);
} else {
return null;
}
}
});})(c__33535__auto___35147,res,vec__33670,v,p,job,jobs,results))
;
return ((function (switch__33434__auto__,c__33535__auto___35147,res,vec__33670,v,p,job,jobs,results){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____0 = (function (){
var statearr_33680 = [null,null,null,null,null,null,null,null];
(statearr_33680[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__);

(statearr_33680[(1)] = (1));

return statearr_33680;
});
var cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____1 = (function (state_33677){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_33677);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e33681){if((e33681 instanceof Object)){
var ex__33438__auto__ = e33681;
var statearr_33682_35148 = state_33677;
(statearr_33682_35148[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33677);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33681;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35149 = state_33677;
state_33677 = G__35149;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__ = function(state_33677){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____1.call(this,state_33677);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto___35147,res,vec__33670,v,p,job,jobs,results))
})();
var state__33537__auto__ = (function (){var statearr_33683 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_33683[(6)] = c__33535__auto___35147);

return statearr_33683;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto___35147,res,vec__33670,v,p,job,jobs,results))
);


cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});})(jobs,results))
;
var async = ((function (jobs,results,process){
return (function (p__33684){
var vec__33685 = p__33684;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33685,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33685,(1),null);
var job = vec__33685;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
(xf.cljs$core$IFn$_invoke$arity$2 ? xf.cljs$core$IFn$_invoke$arity$2(v,res) : xf.call(null,v,res));

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});})(jobs,results,process))
;
var n__4607__auto___35150 = n;
var __35151 = (0);
while(true){
if((__35151 < n__4607__auto___35150)){
var G__33688_35152 = type;
var G__33688_35153__$1 = (((G__33688_35152 instanceof cljs.core.Keyword))?G__33688_35152.fqn:null);
switch (G__33688_35153__$1) {
case "compute":
var c__33535__auto___35155 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__35151,c__33535__auto___35155,G__33688_35152,G__33688_35153__$1,n__4607__auto___35150,jobs,results,process,async){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (__35151,c__33535__auto___35155,G__33688_35152,G__33688_35153__$1,n__4607__auto___35150,jobs,results,process,async){
return (function (state_33701){
var state_val_33702 = (state_33701[(1)]);
if((state_val_33702 === (1))){
var state_33701__$1 = state_33701;
var statearr_33703_35157 = state_33701__$1;
(statearr_33703_35157[(2)] = null);

(statearr_33703_35157[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33702 === (2))){
var state_33701__$1 = state_33701;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33701__$1,(4),jobs);
} else {
if((state_val_33702 === (3))){
var inst_33699 = (state_33701[(2)]);
var state_33701__$1 = state_33701;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33701__$1,inst_33699);
} else {
if((state_val_33702 === (4))){
var inst_33691 = (state_33701[(2)]);
var inst_33692 = process(inst_33691);
var state_33701__$1 = state_33701;
if(cljs.core.truth_(inst_33692)){
var statearr_33704_35163 = state_33701__$1;
(statearr_33704_35163[(1)] = (5));

} else {
var statearr_33705_35164 = state_33701__$1;
(statearr_33705_35164[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33702 === (5))){
var state_33701__$1 = state_33701;
var statearr_33706_35165 = state_33701__$1;
(statearr_33706_35165[(2)] = null);

(statearr_33706_35165[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33702 === (6))){
var state_33701__$1 = state_33701;
var statearr_33707_35167 = state_33701__$1;
(statearr_33707_35167[(2)] = null);

(statearr_33707_35167[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33702 === (7))){
var inst_33697 = (state_33701[(2)]);
var state_33701__$1 = state_33701;
var statearr_33708_35168 = state_33701__$1;
(statearr_33708_35168[(2)] = inst_33697);

(statearr_33708_35168[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__35151,c__33535__auto___35155,G__33688_35152,G__33688_35153__$1,n__4607__auto___35150,jobs,results,process,async))
;
return ((function (__35151,switch__33434__auto__,c__33535__auto___35155,G__33688_35152,G__33688_35153__$1,n__4607__auto___35150,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____0 = (function (){
var statearr_33709 = [null,null,null,null,null,null,null];
(statearr_33709[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__);

(statearr_33709[(1)] = (1));

return statearr_33709;
});
var cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____1 = (function (state_33701){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_33701);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e33710){if((e33710 instanceof Object)){
var ex__33438__auto__ = e33710;
var statearr_33711_35171 = state_33701;
(statearr_33711_35171[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33701);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33710;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35172 = state_33701;
state_33701 = G__35172;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__ = function(state_33701){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____1.call(this,state_33701);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__;
})()
;})(__35151,switch__33434__auto__,c__33535__auto___35155,G__33688_35152,G__33688_35153__$1,n__4607__auto___35150,jobs,results,process,async))
})();
var state__33537__auto__ = (function (){var statearr_33712 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_33712[(6)] = c__33535__auto___35155);

return statearr_33712;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(__35151,c__33535__auto___35155,G__33688_35152,G__33688_35153__$1,n__4607__auto___35150,jobs,results,process,async))
);


break;
case "async":
var c__33535__auto___35173 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__35151,c__33535__auto___35173,G__33688_35152,G__33688_35153__$1,n__4607__auto___35150,jobs,results,process,async){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (__35151,c__33535__auto___35173,G__33688_35152,G__33688_35153__$1,n__4607__auto___35150,jobs,results,process,async){
return (function (state_33725){
var state_val_33726 = (state_33725[(1)]);
if((state_val_33726 === (1))){
var state_33725__$1 = state_33725;
var statearr_33727_35174 = state_33725__$1;
(statearr_33727_35174[(2)] = null);

(statearr_33727_35174[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33726 === (2))){
var state_33725__$1 = state_33725;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33725__$1,(4),jobs);
} else {
if((state_val_33726 === (3))){
var inst_33723 = (state_33725[(2)]);
var state_33725__$1 = state_33725;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33725__$1,inst_33723);
} else {
if((state_val_33726 === (4))){
var inst_33715 = (state_33725[(2)]);
var inst_33716 = async(inst_33715);
var state_33725__$1 = state_33725;
if(cljs.core.truth_(inst_33716)){
var statearr_33728_35175 = state_33725__$1;
(statearr_33728_35175[(1)] = (5));

} else {
var statearr_33729_35176 = state_33725__$1;
(statearr_33729_35176[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33726 === (5))){
var state_33725__$1 = state_33725;
var statearr_33730_35177 = state_33725__$1;
(statearr_33730_35177[(2)] = null);

(statearr_33730_35177[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33726 === (6))){
var state_33725__$1 = state_33725;
var statearr_33731_35181 = state_33725__$1;
(statearr_33731_35181[(2)] = null);

(statearr_33731_35181[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33726 === (7))){
var inst_33721 = (state_33725[(2)]);
var state_33725__$1 = state_33725;
var statearr_33732_35182 = state_33725__$1;
(statearr_33732_35182[(2)] = inst_33721);

(statearr_33732_35182[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__35151,c__33535__auto___35173,G__33688_35152,G__33688_35153__$1,n__4607__auto___35150,jobs,results,process,async))
;
return ((function (__35151,switch__33434__auto__,c__33535__auto___35173,G__33688_35152,G__33688_35153__$1,n__4607__auto___35150,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____0 = (function (){
var statearr_33733 = [null,null,null,null,null,null,null];
(statearr_33733[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__);

(statearr_33733[(1)] = (1));

return statearr_33733;
});
var cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____1 = (function (state_33725){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_33725);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e33734){if((e33734 instanceof Object)){
var ex__33438__auto__ = e33734;
var statearr_33735_35184 = state_33725;
(statearr_33735_35184[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33725);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33734;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35185 = state_33725;
state_33725 = G__35185;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__ = function(state_33725){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____1.call(this,state_33725);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__;
})()
;})(__35151,switch__33434__auto__,c__33535__auto___35173,G__33688_35152,G__33688_35153__$1,n__4607__auto___35150,jobs,results,process,async))
})();
var state__33537__auto__ = (function (){var statearr_33736 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_33736[(6)] = c__33535__auto___35173);

return statearr_33736;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(__35151,c__33535__auto___35173,G__33688_35152,G__33688_35153__$1,n__4607__auto___35150,jobs,results,process,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__33688_35153__$1)].join('')));

}

var G__35189 = (__35151 + (1));
__35151 = G__35189;
continue;
} else {
}
break;
}

var c__33535__auto___35190 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto___35190,jobs,results,process,async){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto___35190,jobs,results,process,async){
return (function (state_33758){
var state_val_33759 = (state_33758[(1)]);
if((state_val_33759 === (7))){
var inst_33754 = (state_33758[(2)]);
var state_33758__$1 = state_33758;
var statearr_33760_35192 = state_33758__$1;
(statearr_33760_35192[(2)] = inst_33754);

(statearr_33760_35192[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33759 === (1))){
var state_33758__$1 = state_33758;
var statearr_33761_35193 = state_33758__$1;
(statearr_33761_35193[(2)] = null);

(statearr_33761_35193[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33759 === (4))){
var inst_33739 = (state_33758[(7)]);
var inst_33739__$1 = (state_33758[(2)]);
var inst_33740 = (inst_33739__$1 == null);
var state_33758__$1 = (function (){var statearr_33762 = state_33758;
(statearr_33762[(7)] = inst_33739__$1);

return statearr_33762;
})();
if(cljs.core.truth_(inst_33740)){
var statearr_33763_35195 = state_33758__$1;
(statearr_33763_35195[(1)] = (5));

} else {
var statearr_33764_35196 = state_33758__$1;
(statearr_33764_35196[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33759 === (6))){
var inst_33739 = (state_33758[(7)]);
var inst_33744 = (state_33758[(8)]);
var inst_33744__$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var inst_33745 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_33746 = [inst_33739,inst_33744__$1];
var inst_33747 = (new cljs.core.PersistentVector(null,2,(5),inst_33745,inst_33746,null));
var state_33758__$1 = (function (){var statearr_33765 = state_33758;
(statearr_33765[(8)] = inst_33744__$1);

return statearr_33765;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33758__$1,(8),jobs,inst_33747);
} else {
if((state_val_33759 === (3))){
var inst_33756 = (state_33758[(2)]);
var state_33758__$1 = state_33758;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33758__$1,inst_33756);
} else {
if((state_val_33759 === (2))){
var state_33758__$1 = state_33758;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33758__$1,(4),from);
} else {
if((state_val_33759 === (9))){
var inst_33751 = (state_33758[(2)]);
var state_33758__$1 = (function (){var statearr_33766 = state_33758;
(statearr_33766[(9)] = inst_33751);

return statearr_33766;
})();
var statearr_33767_35198 = state_33758__$1;
(statearr_33767_35198[(2)] = null);

(statearr_33767_35198[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33759 === (5))){
var inst_33742 = cljs.core.async.close_BANG_(jobs);
var state_33758__$1 = state_33758;
var statearr_33768_35199 = state_33758__$1;
(statearr_33768_35199[(2)] = inst_33742);

(statearr_33768_35199[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33759 === (8))){
var inst_33744 = (state_33758[(8)]);
var inst_33749 = (state_33758[(2)]);
var state_33758__$1 = (function (){var statearr_33769 = state_33758;
(statearr_33769[(10)] = inst_33749);

return statearr_33769;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33758__$1,(9),results,inst_33744);
} else {
return null;
}
}
}
}
}
}
}
}
}
});})(c__33535__auto___35190,jobs,results,process,async))
;
return ((function (switch__33434__auto__,c__33535__auto___35190,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____0 = (function (){
var statearr_33770 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_33770[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__);

(statearr_33770[(1)] = (1));

return statearr_33770;
});
var cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____1 = (function (state_33758){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_33758);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e33771){if((e33771 instanceof Object)){
var ex__33438__auto__ = e33771;
var statearr_33772_35202 = state_33758;
(statearr_33772_35202[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33758);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33771;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35205 = state_33758;
state_33758 = G__35205;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__ = function(state_33758){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____1.call(this,state_33758);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto___35190,jobs,results,process,async))
})();
var state__33537__auto__ = (function (){var statearr_33773 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_33773[(6)] = c__33535__auto___35190);

return statearr_33773;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto___35190,jobs,results,process,async))
);


var c__33535__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto__,jobs,results,process,async){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto__,jobs,results,process,async){
return (function (state_33811){
var state_val_33812 = (state_33811[(1)]);
if((state_val_33812 === (7))){
var inst_33807 = (state_33811[(2)]);
var state_33811__$1 = state_33811;
var statearr_33813_35206 = state_33811__$1;
(statearr_33813_35206[(2)] = inst_33807);

(statearr_33813_35206[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33812 === (20))){
var state_33811__$1 = state_33811;
var statearr_33814_35207 = state_33811__$1;
(statearr_33814_35207[(2)] = null);

(statearr_33814_35207[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33812 === (1))){
var state_33811__$1 = state_33811;
var statearr_33815_35208 = state_33811__$1;
(statearr_33815_35208[(2)] = null);

(statearr_33815_35208[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33812 === (4))){
var inst_33776 = (state_33811[(7)]);
var inst_33776__$1 = (state_33811[(2)]);
var inst_33777 = (inst_33776__$1 == null);
var state_33811__$1 = (function (){var statearr_33816 = state_33811;
(statearr_33816[(7)] = inst_33776__$1);

return statearr_33816;
})();
if(cljs.core.truth_(inst_33777)){
var statearr_33817_35211 = state_33811__$1;
(statearr_33817_35211[(1)] = (5));

} else {
var statearr_33818_35212 = state_33811__$1;
(statearr_33818_35212[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33812 === (15))){
var inst_33789 = (state_33811[(8)]);
var state_33811__$1 = state_33811;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33811__$1,(18),to,inst_33789);
} else {
if((state_val_33812 === (21))){
var inst_33802 = (state_33811[(2)]);
var state_33811__$1 = state_33811;
var statearr_33819_35213 = state_33811__$1;
(statearr_33819_35213[(2)] = inst_33802);

(statearr_33819_35213[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33812 === (13))){
var inst_33804 = (state_33811[(2)]);
var state_33811__$1 = (function (){var statearr_33820 = state_33811;
(statearr_33820[(9)] = inst_33804);

return statearr_33820;
})();
var statearr_33821_35214 = state_33811__$1;
(statearr_33821_35214[(2)] = null);

(statearr_33821_35214[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33812 === (6))){
var inst_33776 = (state_33811[(7)]);
var state_33811__$1 = state_33811;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33811__$1,(11),inst_33776);
} else {
if((state_val_33812 === (17))){
var inst_33797 = (state_33811[(2)]);
var state_33811__$1 = state_33811;
if(cljs.core.truth_(inst_33797)){
var statearr_33822_35215 = state_33811__$1;
(statearr_33822_35215[(1)] = (19));

} else {
var statearr_33823_35216 = state_33811__$1;
(statearr_33823_35216[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33812 === (3))){
var inst_33809 = (state_33811[(2)]);
var state_33811__$1 = state_33811;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33811__$1,inst_33809);
} else {
if((state_val_33812 === (12))){
var inst_33786 = (state_33811[(10)]);
var state_33811__$1 = state_33811;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33811__$1,(14),inst_33786);
} else {
if((state_val_33812 === (2))){
var state_33811__$1 = state_33811;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33811__$1,(4),results);
} else {
if((state_val_33812 === (19))){
var state_33811__$1 = state_33811;
var statearr_33824_35219 = state_33811__$1;
(statearr_33824_35219[(2)] = null);

(statearr_33824_35219[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33812 === (11))){
var inst_33786 = (state_33811[(2)]);
var state_33811__$1 = (function (){var statearr_33825 = state_33811;
(statearr_33825[(10)] = inst_33786);

return statearr_33825;
})();
var statearr_33826_35220 = state_33811__$1;
(statearr_33826_35220[(2)] = null);

(statearr_33826_35220[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33812 === (9))){
var state_33811__$1 = state_33811;
var statearr_33827_35221 = state_33811__$1;
(statearr_33827_35221[(2)] = null);

(statearr_33827_35221[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33812 === (5))){
var state_33811__$1 = state_33811;
if(cljs.core.truth_(close_QMARK_)){
var statearr_33828_35224 = state_33811__$1;
(statearr_33828_35224[(1)] = (8));

} else {
var statearr_33829_35225 = state_33811__$1;
(statearr_33829_35225[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33812 === (14))){
var inst_33791 = (state_33811[(11)]);
var inst_33789 = (state_33811[(8)]);
var inst_33789__$1 = (state_33811[(2)]);
var inst_33790 = (inst_33789__$1 == null);
var inst_33791__$1 = cljs.core.not(inst_33790);
var state_33811__$1 = (function (){var statearr_33830 = state_33811;
(statearr_33830[(11)] = inst_33791__$1);

(statearr_33830[(8)] = inst_33789__$1);

return statearr_33830;
})();
if(inst_33791__$1){
var statearr_33831_35228 = state_33811__$1;
(statearr_33831_35228[(1)] = (15));

} else {
var statearr_33832_35231 = state_33811__$1;
(statearr_33832_35231[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33812 === (16))){
var inst_33791 = (state_33811[(11)]);
var state_33811__$1 = state_33811;
var statearr_33833_35234 = state_33811__$1;
(statearr_33833_35234[(2)] = inst_33791);

(statearr_33833_35234[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33812 === (10))){
var inst_33783 = (state_33811[(2)]);
var state_33811__$1 = state_33811;
var statearr_33834_35237 = state_33811__$1;
(statearr_33834_35237[(2)] = inst_33783);

(statearr_33834_35237[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33812 === (18))){
var inst_33794 = (state_33811[(2)]);
var state_33811__$1 = state_33811;
var statearr_33835_35238 = state_33811__$1;
(statearr_33835_35238[(2)] = inst_33794);

(statearr_33835_35238[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33812 === (8))){
var inst_33780 = cljs.core.async.close_BANG_(to);
var state_33811__$1 = state_33811;
var statearr_33836_35241 = state_33811__$1;
(statearr_33836_35241[(2)] = inst_33780);

(statearr_33836_35241[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto__,jobs,results,process,async))
;
return ((function (switch__33434__auto__,c__33535__auto__,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____0 = (function (){
var statearr_33837 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33837[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__);

(statearr_33837[(1)] = (1));

return statearr_33837;
});
var cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____1 = (function (state_33811){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_33811);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e33838){if((e33838 instanceof Object)){
var ex__33438__auto__ = e33838;
var statearr_33839_35242 = state_33811;
(statearr_33839_35242[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33811);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33838;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35243 = state_33811;
state_33811 = G__35243;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__ = function(state_33811){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____1.call(this,state_33811);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__33435__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto__,jobs,results,process,async))
})();
var state__33537__auto__ = (function (){var statearr_33840 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_33840[(6)] = c__33535__auto__);

return statearr_33840;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto__,jobs,results,process,async))
);

return c__33535__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__33842 = arguments.length;
switch (G__33842) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5(n,to,af,from,true);
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_(n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
});

cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5;

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__33844 = arguments.length;
switch (G__33844) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5(n,to,xf,from,true);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6(n,to,xf,from,close_QMARK_,null);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
});

cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6;

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__33846 = arguments.length;
switch (G__33846) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4(p,ch,null,null);
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(t_buf_or_n);
var fc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(f_buf_or_n);
var c__33535__auto___35252 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto___35252,tc,fc){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto___35252,tc,fc){
return (function (state_33872){
var state_val_33873 = (state_33872[(1)]);
if((state_val_33873 === (7))){
var inst_33868 = (state_33872[(2)]);
var state_33872__$1 = state_33872;
var statearr_33874_35256 = state_33872__$1;
(statearr_33874_35256[(2)] = inst_33868);

(statearr_33874_35256[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (1))){
var state_33872__$1 = state_33872;
var statearr_33875_35257 = state_33872__$1;
(statearr_33875_35257[(2)] = null);

(statearr_33875_35257[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (4))){
var inst_33849 = (state_33872[(7)]);
var inst_33849__$1 = (state_33872[(2)]);
var inst_33850 = (inst_33849__$1 == null);
var state_33872__$1 = (function (){var statearr_33876 = state_33872;
(statearr_33876[(7)] = inst_33849__$1);

return statearr_33876;
})();
if(cljs.core.truth_(inst_33850)){
var statearr_33877_35258 = state_33872__$1;
(statearr_33877_35258[(1)] = (5));

} else {
var statearr_33878_35259 = state_33872__$1;
(statearr_33878_35259[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (13))){
var state_33872__$1 = state_33872;
var statearr_33879_35263 = state_33872__$1;
(statearr_33879_35263[(2)] = null);

(statearr_33879_35263[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (6))){
var inst_33849 = (state_33872[(7)]);
var inst_33855 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_33849) : p.call(null,inst_33849));
var state_33872__$1 = state_33872;
if(cljs.core.truth_(inst_33855)){
var statearr_33880_35264 = state_33872__$1;
(statearr_33880_35264[(1)] = (9));

} else {
var statearr_33881_35265 = state_33872__$1;
(statearr_33881_35265[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (3))){
var inst_33870 = (state_33872[(2)]);
var state_33872__$1 = state_33872;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33872__$1,inst_33870);
} else {
if((state_val_33873 === (12))){
var state_33872__$1 = state_33872;
var statearr_33882_35269 = state_33872__$1;
(statearr_33882_35269[(2)] = null);

(statearr_33882_35269[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (2))){
var state_33872__$1 = state_33872;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33872__$1,(4),ch);
} else {
if((state_val_33873 === (11))){
var inst_33849 = (state_33872[(7)]);
var inst_33859 = (state_33872[(2)]);
var state_33872__$1 = state_33872;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33872__$1,(8),inst_33859,inst_33849);
} else {
if((state_val_33873 === (9))){
var state_33872__$1 = state_33872;
var statearr_33883_35270 = state_33872__$1;
(statearr_33883_35270[(2)] = tc);

(statearr_33883_35270[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (5))){
var inst_33852 = cljs.core.async.close_BANG_(tc);
var inst_33853 = cljs.core.async.close_BANG_(fc);
var state_33872__$1 = (function (){var statearr_33884 = state_33872;
(statearr_33884[(8)] = inst_33852);

return statearr_33884;
})();
var statearr_33885_35271 = state_33872__$1;
(statearr_33885_35271[(2)] = inst_33853);

(statearr_33885_35271[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (14))){
var inst_33866 = (state_33872[(2)]);
var state_33872__$1 = state_33872;
var statearr_33886_35272 = state_33872__$1;
(statearr_33886_35272[(2)] = inst_33866);

(statearr_33886_35272[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (10))){
var state_33872__$1 = state_33872;
var statearr_33887_35276 = state_33872__$1;
(statearr_33887_35276[(2)] = fc);

(statearr_33887_35276[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33873 === (8))){
var inst_33861 = (state_33872[(2)]);
var state_33872__$1 = state_33872;
if(cljs.core.truth_(inst_33861)){
var statearr_33888_35277 = state_33872__$1;
(statearr_33888_35277[(1)] = (12));

} else {
var statearr_33889_35278 = state_33872__$1;
(statearr_33889_35278[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto___35252,tc,fc))
;
return ((function (switch__33434__auto__,c__33535__auto___35252,tc,fc){
return (function() {
var cljs$core$async$state_machine__33435__auto__ = null;
var cljs$core$async$state_machine__33435__auto____0 = (function (){
var statearr_33890 = [null,null,null,null,null,null,null,null,null];
(statearr_33890[(0)] = cljs$core$async$state_machine__33435__auto__);

(statearr_33890[(1)] = (1));

return statearr_33890;
});
var cljs$core$async$state_machine__33435__auto____1 = (function (state_33872){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_33872);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e33891){if((e33891 instanceof Object)){
var ex__33438__auto__ = e33891;
var statearr_33892_35281 = state_33872;
(statearr_33892_35281[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33872);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33891;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35282 = state_33872;
state_33872 = G__35282;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$state_machine__33435__auto__ = function(state_33872){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__33435__auto____1.call(this,state_33872);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__33435__auto____0;
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__33435__auto____1;
return cljs$core$async$state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto___35252,tc,fc))
})();
var state__33537__auto__ = (function (){var statearr_33893 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_33893[(6)] = c__33535__auto___35252);

return statearr_33893;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto___35252,tc,fc))
);


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});

cljs.core.async.split.cljs$lang$maxFixedArity = 4;

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__33535__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto__){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto__){
return (function (state_33914){
var state_val_33915 = (state_33914[(1)]);
if((state_val_33915 === (7))){
var inst_33910 = (state_33914[(2)]);
var state_33914__$1 = state_33914;
var statearr_33916_35290 = state_33914__$1;
(statearr_33916_35290[(2)] = inst_33910);

(statearr_33916_35290[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33915 === (1))){
var inst_33894 = init;
var state_33914__$1 = (function (){var statearr_33917 = state_33914;
(statearr_33917[(7)] = inst_33894);

return statearr_33917;
})();
var statearr_33918_35292 = state_33914__$1;
(statearr_33918_35292[(2)] = null);

(statearr_33918_35292[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33915 === (4))){
var inst_33897 = (state_33914[(8)]);
var inst_33897__$1 = (state_33914[(2)]);
var inst_33898 = (inst_33897__$1 == null);
var state_33914__$1 = (function (){var statearr_33919 = state_33914;
(statearr_33919[(8)] = inst_33897__$1);

return statearr_33919;
})();
if(cljs.core.truth_(inst_33898)){
var statearr_33920_35297 = state_33914__$1;
(statearr_33920_35297[(1)] = (5));

} else {
var statearr_33921_35298 = state_33914__$1;
(statearr_33921_35298[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33915 === (6))){
var inst_33901 = (state_33914[(9)]);
var inst_33897 = (state_33914[(8)]);
var inst_33894 = (state_33914[(7)]);
var inst_33901__$1 = (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(inst_33894,inst_33897) : f.call(null,inst_33894,inst_33897));
var inst_33902 = cljs.core.reduced_QMARK_(inst_33901__$1);
var state_33914__$1 = (function (){var statearr_33922 = state_33914;
(statearr_33922[(9)] = inst_33901__$1);

return statearr_33922;
})();
if(inst_33902){
var statearr_33923_35303 = state_33914__$1;
(statearr_33923_35303[(1)] = (8));

} else {
var statearr_33924_35304 = state_33914__$1;
(statearr_33924_35304[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33915 === (3))){
var inst_33912 = (state_33914[(2)]);
var state_33914__$1 = state_33914;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33914__$1,inst_33912);
} else {
if((state_val_33915 === (2))){
var state_33914__$1 = state_33914;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33914__$1,(4),ch);
} else {
if((state_val_33915 === (9))){
var inst_33901 = (state_33914[(9)]);
var inst_33894 = inst_33901;
var state_33914__$1 = (function (){var statearr_33925 = state_33914;
(statearr_33925[(7)] = inst_33894);

return statearr_33925;
})();
var statearr_33926_35311 = state_33914__$1;
(statearr_33926_35311[(2)] = null);

(statearr_33926_35311[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33915 === (5))){
var inst_33894 = (state_33914[(7)]);
var state_33914__$1 = state_33914;
var statearr_33927_35316 = state_33914__$1;
(statearr_33927_35316[(2)] = inst_33894);

(statearr_33927_35316[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33915 === (10))){
var inst_33908 = (state_33914[(2)]);
var state_33914__$1 = state_33914;
var statearr_33928_35320 = state_33914__$1;
(statearr_33928_35320[(2)] = inst_33908);

(statearr_33928_35320[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33915 === (8))){
var inst_33901 = (state_33914[(9)]);
var inst_33904 = cljs.core.deref(inst_33901);
var state_33914__$1 = state_33914;
var statearr_33929_35321 = state_33914__$1;
(statearr_33929_35321[(2)] = inst_33904);

(statearr_33929_35321[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto__))
;
return ((function (switch__33434__auto__,c__33535__auto__){
return (function() {
var cljs$core$async$reduce_$_state_machine__33435__auto__ = null;
var cljs$core$async$reduce_$_state_machine__33435__auto____0 = (function (){
var statearr_33930 = [null,null,null,null,null,null,null,null,null,null];
(statearr_33930[(0)] = cljs$core$async$reduce_$_state_machine__33435__auto__);

(statearr_33930[(1)] = (1));

return statearr_33930;
});
var cljs$core$async$reduce_$_state_machine__33435__auto____1 = (function (state_33914){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_33914);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e33931){if((e33931 instanceof Object)){
var ex__33438__auto__ = e33931;
var statearr_33932_35328 = state_33914;
(statearr_33932_35328[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33914);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33931;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35335 = state_33914;
state_33914 = G__35335;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__33435__auto__ = function(state_33914){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__33435__auto____1.call(this,state_33914);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__33435__auto____0;
cljs$core$async$reduce_$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__33435__auto____1;
return cljs$core$async$reduce_$_state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto__))
})();
var state__33537__auto__ = (function (){var statearr_33933 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_33933[(6)] = c__33535__auto__);

return statearr_33933;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto__))
);

return c__33535__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = (xform.cljs$core$IFn$_invoke$arity$1 ? xform.cljs$core$IFn$_invoke$arity$1(f) : xform.call(null,f));
var c__33535__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto__,f__$1){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto__,f__$1){
return (function (state_33939){
var state_val_33940 = (state_33939[(1)]);
if((state_val_33940 === (1))){
var inst_33934 = cljs.core.async.reduce(f__$1,init,ch);
var state_33939__$1 = state_33939;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33939__$1,(2),inst_33934);
} else {
if((state_val_33940 === (2))){
var inst_33936 = (state_33939[(2)]);
var inst_33937 = (f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(inst_33936) : f__$1.call(null,inst_33936));
var state_33939__$1 = state_33939;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33939__$1,inst_33937);
} else {
return null;
}
}
});})(c__33535__auto__,f__$1))
;
return ((function (switch__33434__auto__,c__33535__auto__,f__$1){
return (function() {
var cljs$core$async$transduce_$_state_machine__33435__auto__ = null;
var cljs$core$async$transduce_$_state_machine__33435__auto____0 = (function (){
var statearr_33941 = [null,null,null,null,null,null,null];
(statearr_33941[(0)] = cljs$core$async$transduce_$_state_machine__33435__auto__);

(statearr_33941[(1)] = (1));

return statearr_33941;
});
var cljs$core$async$transduce_$_state_machine__33435__auto____1 = (function (state_33939){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_33939);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e33942){if((e33942 instanceof Object)){
var ex__33438__auto__ = e33942;
var statearr_33943_35359 = state_33939;
(statearr_33943_35359[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33939);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33942;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35360 = state_33939;
state_33939 = G__35360;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__33435__auto__ = function(state_33939){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__33435__auto____1.call(this,state_33939);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__33435__auto____0;
cljs$core$async$transduce_$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__33435__auto____1;
return cljs$core$async$transduce_$_state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto__,f__$1))
})();
var state__33537__auto__ = (function (){var statearr_33944 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_33944[(6)] = c__33535__auto__);

return statearr_33944;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto__,f__$1))
);

return c__33535__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__33946 = arguments.length;
switch (G__33946) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__33535__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto__){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto__){
return (function (state_33971){
var state_val_33972 = (state_33971[(1)]);
if((state_val_33972 === (7))){
var inst_33953 = (state_33971[(2)]);
var state_33971__$1 = state_33971;
var statearr_33973_35373 = state_33971__$1;
(statearr_33973_35373[(2)] = inst_33953);

(statearr_33973_35373[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33972 === (1))){
var inst_33947 = cljs.core.seq(coll);
var inst_33948 = inst_33947;
var state_33971__$1 = (function (){var statearr_33974 = state_33971;
(statearr_33974[(7)] = inst_33948);

return statearr_33974;
})();
var statearr_33975_35375 = state_33971__$1;
(statearr_33975_35375[(2)] = null);

(statearr_33975_35375[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33972 === (4))){
var inst_33948 = (state_33971[(7)]);
var inst_33951 = cljs.core.first(inst_33948);
var state_33971__$1 = state_33971;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33971__$1,(7),ch,inst_33951);
} else {
if((state_val_33972 === (13))){
var inst_33965 = (state_33971[(2)]);
var state_33971__$1 = state_33971;
var statearr_33976_35377 = state_33971__$1;
(statearr_33976_35377[(2)] = inst_33965);

(statearr_33976_35377[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33972 === (6))){
var inst_33956 = (state_33971[(2)]);
var state_33971__$1 = state_33971;
if(cljs.core.truth_(inst_33956)){
var statearr_33977_35378 = state_33971__$1;
(statearr_33977_35378[(1)] = (8));

} else {
var statearr_33978_35379 = state_33971__$1;
(statearr_33978_35379[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33972 === (3))){
var inst_33969 = (state_33971[(2)]);
var state_33971__$1 = state_33971;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33971__$1,inst_33969);
} else {
if((state_val_33972 === (12))){
var state_33971__$1 = state_33971;
var statearr_33979_35382 = state_33971__$1;
(statearr_33979_35382[(2)] = null);

(statearr_33979_35382[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33972 === (2))){
var inst_33948 = (state_33971[(7)]);
var state_33971__$1 = state_33971;
if(cljs.core.truth_(inst_33948)){
var statearr_33980_35385 = state_33971__$1;
(statearr_33980_35385[(1)] = (4));

} else {
var statearr_33981_35386 = state_33971__$1;
(statearr_33981_35386[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33972 === (11))){
var inst_33962 = cljs.core.async.close_BANG_(ch);
var state_33971__$1 = state_33971;
var statearr_33982_35387 = state_33971__$1;
(statearr_33982_35387[(2)] = inst_33962);

(statearr_33982_35387[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33972 === (9))){
var state_33971__$1 = state_33971;
if(cljs.core.truth_(close_QMARK_)){
var statearr_33983_35388 = state_33971__$1;
(statearr_33983_35388[(1)] = (11));

} else {
var statearr_33984_35390 = state_33971__$1;
(statearr_33984_35390[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33972 === (5))){
var inst_33948 = (state_33971[(7)]);
var state_33971__$1 = state_33971;
var statearr_33985_35391 = state_33971__$1;
(statearr_33985_35391[(2)] = inst_33948);

(statearr_33985_35391[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33972 === (10))){
var inst_33967 = (state_33971[(2)]);
var state_33971__$1 = state_33971;
var statearr_33986_35394 = state_33971__$1;
(statearr_33986_35394[(2)] = inst_33967);

(statearr_33986_35394[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33972 === (8))){
var inst_33948 = (state_33971[(7)]);
var inst_33958 = cljs.core.next(inst_33948);
var inst_33948__$1 = inst_33958;
var state_33971__$1 = (function (){var statearr_33987 = state_33971;
(statearr_33987[(7)] = inst_33948__$1);

return statearr_33987;
})();
var statearr_33988_35397 = state_33971__$1;
(statearr_33988_35397[(2)] = null);

(statearr_33988_35397[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto__))
;
return ((function (switch__33434__auto__,c__33535__auto__){
return (function() {
var cljs$core$async$state_machine__33435__auto__ = null;
var cljs$core$async$state_machine__33435__auto____0 = (function (){
var statearr_33989 = [null,null,null,null,null,null,null,null];
(statearr_33989[(0)] = cljs$core$async$state_machine__33435__auto__);

(statearr_33989[(1)] = (1));

return statearr_33989;
});
var cljs$core$async$state_machine__33435__auto____1 = (function (state_33971){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_33971);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e33990){if((e33990 instanceof Object)){
var ex__33438__auto__ = e33990;
var statearr_33991_35401 = state_33971;
(statearr_33991_35401[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_33971);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33990;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35406 = state_33971;
state_33971 = G__35406;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$state_machine__33435__auto__ = function(state_33971){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__33435__auto____1.call(this,state_33971);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__33435__auto____0;
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__33435__auto____1;
return cljs$core$async$state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto__))
})();
var state__33537__auto__ = (function (){var statearr_33992 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_33992[(6)] = c__33535__auto__);

return statearr_33992;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto__))
);

return c__33535__auto__;
});

cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
var ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.bounded_count((100),coll));
cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2(ch,coll);

return ch;
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
var x__4433__auto__ = (((_ == null))?null:_);
var m__4434__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4434__auto__.call(null,_));
} else {
var m__4431__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4431__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Mux.muxch*",_);
}
}
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4434__auto__.call(null,m,ch,close_QMARK_));
} else {
var m__4431__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4431__auto__.call(null,m,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Mult.tap*",m);
}
}
}
});

cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4434__auto__.call(null,m,ch));
} else {
var m__4431__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4431__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mult.untap*",m);
}
}
}
});

cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4434__auto__.call(null,m));
} else {
var m__4431__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4431__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mult.untap-all*",m);
}
}
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33993 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33993 = (function (ch,cs,meta33994){
this.ch = ch;
this.cs = cs;
this.meta33994 = meta33994;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async33993.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_33995,meta33994__$1){
var self__ = this;
var _33995__$1 = this;
return (new cljs.core.async.t_cljs$core$async33993(self__.ch,self__.cs,meta33994__$1));
});})(cs))
;

cljs.core.async.t_cljs$core$async33993.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_33995){
var self__ = this;
var _33995__$1 = this;
return self__.meta33994;
});})(cs))
;

cljs.core.async.t_cljs$core$async33993.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async33993.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(cs))
;

cljs.core.async.t_cljs$core$async33993.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async33993.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async33993.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch__$1);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async33993.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async33993.getBasis = ((function (cs){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta33994","meta33994",1051810472,null)], null);
});})(cs))
;

cljs.core.async.t_cljs$core$async33993.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async33993.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33993";

cljs.core.async.t_cljs$core$async33993.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async33993");
});})(cs))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33993.
 */
cljs.core.async.__GT_t_cljs$core$async33993 = ((function (cs){
return (function cljs$core$async$mult_$___GT_t_cljs$core$async33993(ch__$1,cs__$1,meta33994){
return (new cljs.core.async.t_cljs$core$async33993(ch__$1,cs__$1,meta33994));
});})(cs))
;

}

return (new cljs.core.async.t_cljs$core$async33993(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = ((function (cs,m,dchan,dctr){
return (function (_){
if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,true);
} else {
return null;
}
});})(cs,m,dchan,dctr))
;
var c__33535__auto___35445 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto___35445,cs,m,dchan,dctr,done){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto___35445,cs,m,dchan,dctr,done){
return (function (state_34130){
var state_val_34131 = (state_34130[(1)]);
if((state_val_34131 === (7))){
var inst_34126 = (state_34130[(2)]);
var state_34130__$1 = state_34130;
var statearr_34132_35448 = state_34130__$1;
(statearr_34132_35448[(2)] = inst_34126);

(statearr_34132_35448[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (20))){
var inst_34029 = (state_34130[(7)]);
var inst_34041 = cljs.core.first(inst_34029);
var inst_34042 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_34041,(0),null);
var inst_34043 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_34041,(1),null);
var state_34130__$1 = (function (){var statearr_34133 = state_34130;
(statearr_34133[(8)] = inst_34042);

return statearr_34133;
})();
if(cljs.core.truth_(inst_34043)){
var statearr_34134_35450 = state_34130__$1;
(statearr_34134_35450[(1)] = (22));

} else {
var statearr_34135_35451 = state_34130__$1;
(statearr_34135_35451[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (27))){
var inst_34071 = (state_34130[(9)]);
var inst_33998 = (state_34130[(10)]);
var inst_34073 = (state_34130[(11)]);
var inst_34078 = (state_34130[(12)]);
var inst_34078__$1 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_34071,inst_34073);
var inst_34079 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_34078__$1,inst_33998,done);
var state_34130__$1 = (function (){var statearr_34136 = state_34130;
(statearr_34136[(12)] = inst_34078__$1);

return statearr_34136;
})();
if(cljs.core.truth_(inst_34079)){
var statearr_34137_35461 = state_34130__$1;
(statearr_34137_35461[(1)] = (30));

} else {
var statearr_34138_35462 = state_34130__$1;
(statearr_34138_35462[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (1))){
var state_34130__$1 = state_34130;
var statearr_34139_35465 = state_34130__$1;
(statearr_34139_35465[(2)] = null);

(statearr_34139_35465[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (24))){
var inst_34029 = (state_34130[(7)]);
var inst_34048 = (state_34130[(2)]);
var inst_34049 = cljs.core.next(inst_34029);
var inst_34007 = inst_34049;
var inst_34008 = null;
var inst_34009 = (0);
var inst_34010 = (0);
var state_34130__$1 = (function (){var statearr_34140 = state_34130;
(statearr_34140[(13)] = inst_34007);

(statearr_34140[(14)] = inst_34010);

(statearr_34140[(15)] = inst_34048);

(statearr_34140[(16)] = inst_34008);

(statearr_34140[(17)] = inst_34009);

return statearr_34140;
})();
var statearr_34141_35472 = state_34130__$1;
(statearr_34141_35472[(2)] = null);

(statearr_34141_35472[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (39))){
var state_34130__$1 = state_34130;
var statearr_34145_35473 = state_34130__$1;
(statearr_34145_35473[(2)] = null);

(statearr_34145_35473[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (4))){
var inst_33998 = (state_34130[(10)]);
var inst_33998__$1 = (state_34130[(2)]);
var inst_33999 = (inst_33998__$1 == null);
var state_34130__$1 = (function (){var statearr_34146 = state_34130;
(statearr_34146[(10)] = inst_33998__$1);

return statearr_34146;
})();
if(cljs.core.truth_(inst_33999)){
var statearr_34147_35475 = state_34130__$1;
(statearr_34147_35475[(1)] = (5));

} else {
var statearr_34148_35476 = state_34130__$1;
(statearr_34148_35476[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (15))){
var inst_34007 = (state_34130[(13)]);
var inst_34010 = (state_34130[(14)]);
var inst_34008 = (state_34130[(16)]);
var inst_34009 = (state_34130[(17)]);
var inst_34025 = (state_34130[(2)]);
var inst_34026 = (inst_34010 + (1));
var tmp34142 = inst_34007;
var tmp34143 = inst_34008;
var tmp34144 = inst_34009;
var inst_34007__$1 = tmp34142;
var inst_34008__$1 = tmp34143;
var inst_34009__$1 = tmp34144;
var inst_34010__$1 = inst_34026;
var state_34130__$1 = (function (){var statearr_34149 = state_34130;
(statearr_34149[(13)] = inst_34007__$1);

(statearr_34149[(14)] = inst_34010__$1);

(statearr_34149[(18)] = inst_34025);

(statearr_34149[(16)] = inst_34008__$1);

(statearr_34149[(17)] = inst_34009__$1);

return statearr_34149;
})();
var statearr_34150_35482 = state_34130__$1;
(statearr_34150_35482[(2)] = null);

(statearr_34150_35482[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (21))){
var inst_34052 = (state_34130[(2)]);
var state_34130__$1 = state_34130;
var statearr_34154_35483 = state_34130__$1;
(statearr_34154_35483[(2)] = inst_34052);

(statearr_34154_35483[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (31))){
var inst_34078 = (state_34130[(12)]);
var inst_34082 = done(null);
var inst_34083 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_34078);
var state_34130__$1 = (function (){var statearr_34155 = state_34130;
(statearr_34155[(19)] = inst_34082);

return statearr_34155;
})();
var statearr_34156_35494 = state_34130__$1;
(statearr_34156_35494[(2)] = inst_34083);

(statearr_34156_35494[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (32))){
var inst_34071 = (state_34130[(9)]);
var inst_34072 = (state_34130[(20)]);
var inst_34070 = (state_34130[(21)]);
var inst_34073 = (state_34130[(11)]);
var inst_34085 = (state_34130[(2)]);
var inst_34086 = (inst_34073 + (1));
var tmp34151 = inst_34071;
var tmp34152 = inst_34072;
var tmp34153 = inst_34070;
var inst_34070__$1 = tmp34153;
var inst_34071__$1 = tmp34151;
var inst_34072__$1 = tmp34152;
var inst_34073__$1 = inst_34086;
var state_34130__$1 = (function (){var statearr_34157 = state_34130;
(statearr_34157[(9)] = inst_34071__$1);

(statearr_34157[(20)] = inst_34072__$1);

(statearr_34157[(22)] = inst_34085);

(statearr_34157[(21)] = inst_34070__$1);

(statearr_34157[(11)] = inst_34073__$1);

return statearr_34157;
})();
var statearr_34158_35495 = state_34130__$1;
(statearr_34158_35495[(2)] = null);

(statearr_34158_35495[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (40))){
var inst_34098 = (state_34130[(23)]);
var inst_34102 = done(null);
var inst_34103 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_34098);
var state_34130__$1 = (function (){var statearr_34159 = state_34130;
(statearr_34159[(24)] = inst_34102);

return statearr_34159;
})();
var statearr_34160_35497 = state_34130__$1;
(statearr_34160_35497[(2)] = inst_34103);

(statearr_34160_35497[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (33))){
var inst_34089 = (state_34130[(25)]);
var inst_34091 = cljs.core.chunked_seq_QMARK_(inst_34089);
var state_34130__$1 = state_34130;
if(inst_34091){
var statearr_34161_35500 = state_34130__$1;
(statearr_34161_35500[(1)] = (36));

} else {
var statearr_34162_35501 = state_34130__$1;
(statearr_34162_35501[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (13))){
var inst_34019 = (state_34130[(26)]);
var inst_34022 = cljs.core.async.close_BANG_(inst_34019);
var state_34130__$1 = state_34130;
var statearr_34163_35504 = state_34130__$1;
(statearr_34163_35504[(2)] = inst_34022);

(statearr_34163_35504[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (22))){
var inst_34042 = (state_34130[(8)]);
var inst_34045 = cljs.core.async.close_BANG_(inst_34042);
var state_34130__$1 = state_34130;
var statearr_34164_35505 = state_34130__$1;
(statearr_34164_35505[(2)] = inst_34045);

(statearr_34164_35505[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (36))){
var inst_34089 = (state_34130[(25)]);
var inst_34093 = cljs.core.chunk_first(inst_34089);
var inst_34094 = cljs.core.chunk_rest(inst_34089);
var inst_34095 = cljs.core.count(inst_34093);
var inst_34070 = inst_34094;
var inst_34071 = inst_34093;
var inst_34072 = inst_34095;
var inst_34073 = (0);
var state_34130__$1 = (function (){var statearr_34165 = state_34130;
(statearr_34165[(9)] = inst_34071);

(statearr_34165[(20)] = inst_34072);

(statearr_34165[(21)] = inst_34070);

(statearr_34165[(11)] = inst_34073);

return statearr_34165;
})();
var statearr_34166_35506 = state_34130__$1;
(statearr_34166_35506[(2)] = null);

(statearr_34166_35506[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (41))){
var inst_34089 = (state_34130[(25)]);
var inst_34105 = (state_34130[(2)]);
var inst_34106 = cljs.core.next(inst_34089);
var inst_34070 = inst_34106;
var inst_34071 = null;
var inst_34072 = (0);
var inst_34073 = (0);
var state_34130__$1 = (function (){var statearr_34167 = state_34130;
(statearr_34167[(9)] = inst_34071);

(statearr_34167[(20)] = inst_34072);

(statearr_34167[(21)] = inst_34070);

(statearr_34167[(11)] = inst_34073);

(statearr_34167[(27)] = inst_34105);

return statearr_34167;
})();
var statearr_34168_35513 = state_34130__$1;
(statearr_34168_35513[(2)] = null);

(statearr_34168_35513[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (43))){
var state_34130__$1 = state_34130;
var statearr_34169_35514 = state_34130__$1;
(statearr_34169_35514[(2)] = null);

(statearr_34169_35514[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (29))){
var inst_34114 = (state_34130[(2)]);
var state_34130__$1 = state_34130;
var statearr_34170_35515 = state_34130__$1;
(statearr_34170_35515[(2)] = inst_34114);

(statearr_34170_35515[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (44))){
var inst_34123 = (state_34130[(2)]);
var state_34130__$1 = (function (){var statearr_34171 = state_34130;
(statearr_34171[(28)] = inst_34123);

return statearr_34171;
})();
var statearr_34172_35516 = state_34130__$1;
(statearr_34172_35516[(2)] = null);

(statearr_34172_35516[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (6))){
var inst_34062 = (state_34130[(29)]);
var inst_34061 = cljs.core.deref(cs);
var inst_34062__$1 = cljs.core.keys(inst_34061);
var inst_34063 = cljs.core.count(inst_34062__$1);
var inst_34064 = cljs.core.reset_BANG_(dctr,inst_34063);
var inst_34069 = cljs.core.seq(inst_34062__$1);
var inst_34070 = inst_34069;
var inst_34071 = null;
var inst_34072 = (0);
var inst_34073 = (0);
var state_34130__$1 = (function (){var statearr_34173 = state_34130;
(statearr_34173[(9)] = inst_34071);

(statearr_34173[(30)] = inst_34064);

(statearr_34173[(29)] = inst_34062__$1);

(statearr_34173[(20)] = inst_34072);

(statearr_34173[(21)] = inst_34070);

(statearr_34173[(11)] = inst_34073);

return statearr_34173;
})();
var statearr_34174_35521 = state_34130__$1;
(statearr_34174_35521[(2)] = null);

(statearr_34174_35521[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (28))){
var inst_34089 = (state_34130[(25)]);
var inst_34070 = (state_34130[(21)]);
var inst_34089__$1 = cljs.core.seq(inst_34070);
var state_34130__$1 = (function (){var statearr_34175 = state_34130;
(statearr_34175[(25)] = inst_34089__$1);

return statearr_34175;
})();
if(inst_34089__$1){
var statearr_34176_35525 = state_34130__$1;
(statearr_34176_35525[(1)] = (33));

} else {
var statearr_34177_35531 = state_34130__$1;
(statearr_34177_35531[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (25))){
var inst_34072 = (state_34130[(20)]);
var inst_34073 = (state_34130[(11)]);
var inst_34075 = (inst_34073 < inst_34072);
var inst_34076 = inst_34075;
var state_34130__$1 = state_34130;
if(cljs.core.truth_(inst_34076)){
var statearr_34178_35532 = state_34130__$1;
(statearr_34178_35532[(1)] = (27));

} else {
var statearr_34179_35533 = state_34130__$1;
(statearr_34179_35533[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (34))){
var state_34130__$1 = state_34130;
var statearr_34180_35534 = state_34130__$1;
(statearr_34180_35534[(2)] = null);

(statearr_34180_35534[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (17))){
var state_34130__$1 = state_34130;
var statearr_34181_35535 = state_34130__$1;
(statearr_34181_35535[(2)] = null);

(statearr_34181_35535[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (3))){
var inst_34128 = (state_34130[(2)]);
var state_34130__$1 = state_34130;
return cljs.core.async.impl.ioc_helpers.return_chan(state_34130__$1,inst_34128);
} else {
if((state_val_34131 === (12))){
var inst_34057 = (state_34130[(2)]);
var state_34130__$1 = state_34130;
var statearr_34182_35536 = state_34130__$1;
(statearr_34182_35536[(2)] = inst_34057);

(statearr_34182_35536[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (2))){
var state_34130__$1 = state_34130;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34130__$1,(4),ch);
} else {
if((state_val_34131 === (23))){
var state_34130__$1 = state_34130;
var statearr_34183_35537 = state_34130__$1;
(statearr_34183_35537[(2)] = null);

(statearr_34183_35537[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (35))){
var inst_34112 = (state_34130[(2)]);
var state_34130__$1 = state_34130;
var statearr_34184_35538 = state_34130__$1;
(statearr_34184_35538[(2)] = inst_34112);

(statearr_34184_35538[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (19))){
var inst_34029 = (state_34130[(7)]);
var inst_34033 = cljs.core.chunk_first(inst_34029);
var inst_34034 = cljs.core.chunk_rest(inst_34029);
var inst_34035 = cljs.core.count(inst_34033);
var inst_34007 = inst_34034;
var inst_34008 = inst_34033;
var inst_34009 = inst_34035;
var inst_34010 = (0);
var state_34130__$1 = (function (){var statearr_34185 = state_34130;
(statearr_34185[(13)] = inst_34007);

(statearr_34185[(14)] = inst_34010);

(statearr_34185[(16)] = inst_34008);

(statearr_34185[(17)] = inst_34009);

return statearr_34185;
})();
var statearr_34186_35539 = state_34130__$1;
(statearr_34186_35539[(2)] = null);

(statearr_34186_35539[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (11))){
var inst_34007 = (state_34130[(13)]);
var inst_34029 = (state_34130[(7)]);
var inst_34029__$1 = cljs.core.seq(inst_34007);
var state_34130__$1 = (function (){var statearr_34187 = state_34130;
(statearr_34187[(7)] = inst_34029__$1);

return statearr_34187;
})();
if(inst_34029__$1){
var statearr_34188_35540 = state_34130__$1;
(statearr_34188_35540[(1)] = (16));

} else {
var statearr_34189_35541 = state_34130__$1;
(statearr_34189_35541[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (9))){
var inst_34059 = (state_34130[(2)]);
var state_34130__$1 = state_34130;
var statearr_34190_35542 = state_34130__$1;
(statearr_34190_35542[(2)] = inst_34059);

(statearr_34190_35542[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (5))){
var inst_34005 = cljs.core.deref(cs);
var inst_34006 = cljs.core.seq(inst_34005);
var inst_34007 = inst_34006;
var inst_34008 = null;
var inst_34009 = (0);
var inst_34010 = (0);
var state_34130__$1 = (function (){var statearr_34191 = state_34130;
(statearr_34191[(13)] = inst_34007);

(statearr_34191[(14)] = inst_34010);

(statearr_34191[(16)] = inst_34008);

(statearr_34191[(17)] = inst_34009);

return statearr_34191;
})();
var statearr_34192_35543 = state_34130__$1;
(statearr_34192_35543[(2)] = null);

(statearr_34192_35543[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (14))){
var state_34130__$1 = state_34130;
var statearr_34193_35544 = state_34130__$1;
(statearr_34193_35544[(2)] = null);

(statearr_34193_35544[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (45))){
var inst_34120 = (state_34130[(2)]);
var state_34130__$1 = state_34130;
var statearr_34194_35545 = state_34130__$1;
(statearr_34194_35545[(2)] = inst_34120);

(statearr_34194_35545[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (26))){
var inst_34062 = (state_34130[(29)]);
var inst_34116 = (state_34130[(2)]);
var inst_34117 = cljs.core.seq(inst_34062);
var state_34130__$1 = (function (){var statearr_34195 = state_34130;
(statearr_34195[(31)] = inst_34116);

return statearr_34195;
})();
if(inst_34117){
var statearr_34196_35546 = state_34130__$1;
(statearr_34196_35546[(1)] = (42));

} else {
var statearr_34197_35547 = state_34130__$1;
(statearr_34197_35547[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (16))){
var inst_34029 = (state_34130[(7)]);
var inst_34031 = cljs.core.chunked_seq_QMARK_(inst_34029);
var state_34130__$1 = state_34130;
if(inst_34031){
var statearr_34198_35548 = state_34130__$1;
(statearr_34198_35548[(1)] = (19));

} else {
var statearr_34199_35549 = state_34130__$1;
(statearr_34199_35549[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (38))){
var inst_34109 = (state_34130[(2)]);
var state_34130__$1 = state_34130;
var statearr_34200_35551 = state_34130__$1;
(statearr_34200_35551[(2)] = inst_34109);

(statearr_34200_35551[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (30))){
var state_34130__$1 = state_34130;
var statearr_34201_35552 = state_34130__$1;
(statearr_34201_35552[(2)] = null);

(statearr_34201_35552[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (10))){
var inst_34010 = (state_34130[(14)]);
var inst_34008 = (state_34130[(16)]);
var inst_34018 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_34008,inst_34010);
var inst_34019 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_34018,(0),null);
var inst_34020 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_34018,(1),null);
var state_34130__$1 = (function (){var statearr_34202 = state_34130;
(statearr_34202[(26)] = inst_34019);

return statearr_34202;
})();
if(cljs.core.truth_(inst_34020)){
var statearr_34203_35558 = state_34130__$1;
(statearr_34203_35558[(1)] = (13));

} else {
var statearr_34204_35559 = state_34130__$1;
(statearr_34204_35559[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (18))){
var inst_34055 = (state_34130[(2)]);
var state_34130__$1 = state_34130;
var statearr_34205_35560 = state_34130__$1;
(statearr_34205_35560[(2)] = inst_34055);

(statearr_34205_35560[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (42))){
var state_34130__$1 = state_34130;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34130__$1,(45),dchan);
} else {
if((state_val_34131 === (37))){
var inst_34098 = (state_34130[(23)]);
var inst_34089 = (state_34130[(25)]);
var inst_33998 = (state_34130[(10)]);
var inst_34098__$1 = cljs.core.first(inst_34089);
var inst_34099 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_34098__$1,inst_33998,done);
var state_34130__$1 = (function (){var statearr_34206 = state_34130;
(statearr_34206[(23)] = inst_34098__$1);

return statearr_34206;
})();
if(cljs.core.truth_(inst_34099)){
var statearr_34207_35561 = state_34130__$1;
(statearr_34207_35561[(1)] = (39));

} else {
var statearr_34208_35562 = state_34130__$1;
(statearr_34208_35562[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34131 === (8))){
var inst_34010 = (state_34130[(14)]);
var inst_34009 = (state_34130[(17)]);
var inst_34012 = (inst_34010 < inst_34009);
var inst_34013 = inst_34012;
var state_34130__$1 = state_34130;
if(cljs.core.truth_(inst_34013)){
var statearr_34209_35563 = state_34130__$1;
(statearr_34209_35563[(1)] = (10));

} else {
var statearr_34210_35564 = state_34130__$1;
(statearr_34210_35564[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto___35445,cs,m,dchan,dctr,done))
;
return ((function (switch__33434__auto__,c__33535__auto___35445,cs,m,dchan,dctr,done){
return (function() {
var cljs$core$async$mult_$_state_machine__33435__auto__ = null;
var cljs$core$async$mult_$_state_machine__33435__auto____0 = (function (){
var statearr_34211 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34211[(0)] = cljs$core$async$mult_$_state_machine__33435__auto__);

(statearr_34211[(1)] = (1));

return statearr_34211;
});
var cljs$core$async$mult_$_state_machine__33435__auto____1 = (function (state_34130){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_34130);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e34212){if((e34212 instanceof Object)){
var ex__33438__auto__ = e34212;
var statearr_34213_35568 = state_34130;
(statearr_34213_35568[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_34130);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34212;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35569 = state_34130;
state_34130 = G__35569;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__33435__auto__ = function(state_34130){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__33435__auto____1.call(this,state_34130);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__33435__auto____0;
cljs$core$async$mult_$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__33435__auto____1;
return cljs$core$async$mult_$_state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto___35445,cs,m,dchan,dctr,done))
})();
var state__33537__auto__ = (function (){var statearr_34214 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_34214[(6)] = c__33535__auto___35445);

return statearr_34214;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto___35445,cs,m,dchan,dctr,done))
);


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__34216 = arguments.length;
switch (G__34216) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(mult,ch,true);
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_(mult,ch,close_QMARK_);

return ch;
});

cljs.core.async.tap.cljs$lang$maxFixedArity = 3;

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_(mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_(mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4434__auto__.call(null,m,ch));
} else {
var m__4431__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4431__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.admix*",m);
}
}
}
});

cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4434__auto__.call(null,m,ch));
} else {
var m__4431__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4431__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.unmix*",m);
}
}
}
});

cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4434__auto__.call(null,m));
} else {
var m__4431__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4431__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mix.unmix-all*",m);
}
}
}
});

cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4434__auto__.call(null,m,state_map));
} else {
var m__4431__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4431__auto__.call(null,m,state_map));
} else {
throw cljs.core.missing_protocol("Mix.toggle*",m);
}
}
}
});

cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
var x__4433__auto__ = (((m == null))?null:m);
var m__4434__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4434__auto__.call(null,m,mode));
} else {
var m__4431__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4431__auto__.call(null,m,mode));
} else {
throw cljs.core.missing_protocol("Mix.solo-mode*",m);
}
}
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__4736__auto__ = [];
var len__4730__auto___35587 = arguments.length;
var i__4731__auto___35588 = (0);
while(true){
if((i__4731__auto___35588 < len__4730__auto___35587)){
args__4736__auto__.push((arguments[i__4731__auto___35588]));

var G__35589 = (i__4731__auto___35588 + (1));
i__4731__auto___35588 = G__35589;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((3) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4737__auto__);
});

cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__34221){
var map__34222 = p__34221;
var map__34222__$1 = (((((!((map__34222 == null))))?(((((map__34222.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__34222.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__34222):map__34222);
var opts = map__34222__$1;
var statearr_34224_35590 = state;
(statearr_34224_35590[(1)] = cont_block);


var temp__5735__auto__ = cljs.core.async.do_alts(((function (map__34222,map__34222__$1,opts){
return (function (val){
var statearr_34225_35595 = state;
(statearr_34225_35595[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state);
});})(map__34222,map__34222__$1,opts))
,ports,opts);
if(cljs.core.truth_(temp__5735__auto__)){
var cb = temp__5735__auto__;
var statearr_34226_35596 = state;
(statearr_34226_35596[(2)] = cljs.core.deref(cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
});

cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3);

/** @this {Function} */
cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq34217){
var G__34218 = cljs.core.first(seq34217);
var seq34217__$1 = cljs.core.next(seq34217);
var G__34219 = cljs.core.first(seq34217__$1);
var seq34217__$2 = cljs.core.next(seq34217__$1);
var G__34220 = cljs.core.first(seq34217__$2);
var seq34217__$3 = cljs.core.next(seq34217__$2);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__34218,G__34219,G__34220,seq34217__$3);
});

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();
var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;
var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){
return cljs.core.reduce_kv(((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){
if(cljs.core.truth_((attr.cljs$core$IFn$_invoke$arity$1 ? attr.cljs$core$IFn$_invoke$arity$1(v) : attr.call(null,v)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,c);
} else {
return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;
var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){
var chs = cljs.core.deref(cs);
var mode = cljs.core.deref(solo_mode);
var solos = pick(new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick(new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick(new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_(solos))))))?cljs.core.vec(solos):cljs.core.vec(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(pauses,cljs.core.keys(chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async34227 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async34227 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta34228){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta34228 = meta34228;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async34227.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_34229,meta34228__$1){
var self__ = this;
var _34229__$1 = this;
return (new cljs.core.async.t_cljs$core$async34227(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta34228__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async34227.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_34229){
var self__ = this;
var _34229__$1 = this;
return self__.meta34228;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async34227.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async34227.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async34227.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async34227.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async34227.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async34227.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async34227.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.merge_with,cljs.core.merge),state_map);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async34227.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.solo_modes.cljs$core$IFn$_invoke$arity$1 ? self__.solo_modes.cljs$core$IFn$_invoke$arity$1(mode) : self__.solo_modes.call(null,mode)))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_(self__.solo_mode,mode);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async34227.getBasis = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta34228","meta34228",757909216,null)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async34227.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async34227.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async34227";

cljs.core.async.t_cljs$core$async34227.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async34227");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async34227.
 */
cljs.core.async.__GT_t_cljs$core$async34227 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function cljs$core$async$mix_$___GT_t_cljs$core$async34227(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta34228){
return (new cljs.core.async.t_cljs$core$async34227(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta34228));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

}

return (new cljs.core.async.t_cljs$core$async34227(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__33535__auto___35631 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto___35631,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto___35631,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_34331){
var state_val_34332 = (state_34331[(1)]);
if((state_val_34332 === (7))){
var inst_34246 = (state_34331[(2)]);
var state_34331__$1 = state_34331;
var statearr_34333_35635 = state_34331__$1;
(statearr_34333_35635[(2)] = inst_34246);

(statearr_34333_35635[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (20))){
var inst_34258 = (state_34331[(7)]);
var state_34331__$1 = state_34331;
var statearr_34334_35636 = state_34331__$1;
(statearr_34334_35636[(2)] = inst_34258);

(statearr_34334_35636[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (27))){
var state_34331__$1 = state_34331;
var statearr_34335_35637 = state_34331__$1;
(statearr_34335_35637[(2)] = null);

(statearr_34335_35637[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (1))){
var inst_34233 = (state_34331[(8)]);
var inst_34233__$1 = calc_state();
var inst_34235 = (inst_34233__$1 == null);
var inst_34236 = cljs.core.not(inst_34235);
var state_34331__$1 = (function (){var statearr_34336 = state_34331;
(statearr_34336[(8)] = inst_34233__$1);

return statearr_34336;
})();
if(inst_34236){
var statearr_34337_35642 = state_34331__$1;
(statearr_34337_35642[(1)] = (2));

} else {
var statearr_34338_35643 = state_34331__$1;
(statearr_34338_35643[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (24))){
var inst_34305 = (state_34331[(9)]);
var inst_34282 = (state_34331[(10)]);
var inst_34291 = (state_34331[(11)]);
var inst_34305__$1 = (inst_34282.cljs$core$IFn$_invoke$arity$1 ? inst_34282.cljs$core$IFn$_invoke$arity$1(inst_34291) : inst_34282.call(null,inst_34291));
var state_34331__$1 = (function (){var statearr_34339 = state_34331;
(statearr_34339[(9)] = inst_34305__$1);

return statearr_34339;
})();
if(cljs.core.truth_(inst_34305__$1)){
var statearr_34340_35644 = state_34331__$1;
(statearr_34340_35644[(1)] = (29));

} else {
var statearr_34341_35645 = state_34331__$1;
(statearr_34341_35645[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (4))){
var inst_34249 = (state_34331[(2)]);
var state_34331__$1 = state_34331;
if(cljs.core.truth_(inst_34249)){
var statearr_34342_35646 = state_34331__$1;
(statearr_34342_35646[(1)] = (8));

} else {
var statearr_34343_35647 = state_34331__$1;
(statearr_34343_35647[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (15))){
var inst_34276 = (state_34331[(2)]);
var state_34331__$1 = state_34331;
if(cljs.core.truth_(inst_34276)){
var statearr_34344_35648 = state_34331__$1;
(statearr_34344_35648[(1)] = (19));

} else {
var statearr_34345_35649 = state_34331__$1;
(statearr_34345_35649[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (21))){
var inst_34281 = (state_34331[(12)]);
var inst_34281__$1 = (state_34331[(2)]);
var inst_34282 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_34281__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_34283 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_34281__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_34284 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_34281__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_34331__$1 = (function (){var statearr_34346 = state_34331;
(statearr_34346[(13)] = inst_34283);

(statearr_34346[(10)] = inst_34282);

(statearr_34346[(12)] = inst_34281__$1);

return statearr_34346;
})();
return cljs.core.async.ioc_alts_BANG_(state_34331__$1,(22),inst_34284);
} else {
if((state_val_34332 === (31))){
var inst_34313 = (state_34331[(2)]);
var state_34331__$1 = state_34331;
if(cljs.core.truth_(inst_34313)){
var statearr_34347_35652 = state_34331__$1;
(statearr_34347_35652[(1)] = (32));

} else {
var statearr_34348_35653 = state_34331__$1;
(statearr_34348_35653[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (32))){
var inst_34290 = (state_34331[(14)]);
var state_34331__$1 = state_34331;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34331__$1,(35),out,inst_34290);
} else {
if((state_val_34332 === (33))){
var inst_34281 = (state_34331[(12)]);
var inst_34258 = inst_34281;
var state_34331__$1 = (function (){var statearr_34349 = state_34331;
(statearr_34349[(7)] = inst_34258);

return statearr_34349;
})();
var statearr_34350_35656 = state_34331__$1;
(statearr_34350_35656[(2)] = null);

(statearr_34350_35656[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (13))){
var inst_34258 = (state_34331[(7)]);
var inst_34265 = inst_34258.cljs$lang$protocol_mask$partition0$;
var inst_34266 = (inst_34265 & (64));
var inst_34267 = inst_34258.cljs$core$ISeq$;
var inst_34268 = (cljs.core.PROTOCOL_SENTINEL === inst_34267);
var inst_34269 = ((inst_34266) || (inst_34268));
var state_34331__$1 = state_34331;
if(cljs.core.truth_(inst_34269)){
var statearr_34351_35657 = state_34331__$1;
(statearr_34351_35657[(1)] = (16));

} else {
var statearr_34352_35660 = state_34331__$1;
(statearr_34352_35660[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (22))){
var inst_34291 = (state_34331[(11)]);
var inst_34290 = (state_34331[(14)]);
var inst_34289 = (state_34331[(2)]);
var inst_34290__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_34289,(0),null);
var inst_34291__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_34289,(1),null);
var inst_34292 = (inst_34290__$1 == null);
var inst_34293 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_34291__$1,change);
var inst_34294 = ((inst_34292) || (inst_34293));
var state_34331__$1 = (function (){var statearr_34353 = state_34331;
(statearr_34353[(11)] = inst_34291__$1);

(statearr_34353[(14)] = inst_34290__$1);

return statearr_34353;
})();
if(cljs.core.truth_(inst_34294)){
var statearr_34354_35664 = state_34331__$1;
(statearr_34354_35664[(1)] = (23));

} else {
var statearr_34355_35665 = state_34331__$1;
(statearr_34355_35665[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (36))){
var inst_34281 = (state_34331[(12)]);
var inst_34258 = inst_34281;
var state_34331__$1 = (function (){var statearr_34356 = state_34331;
(statearr_34356[(7)] = inst_34258);

return statearr_34356;
})();
var statearr_34357_35666 = state_34331__$1;
(statearr_34357_35666[(2)] = null);

(statearr_34357_35666[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (29))){
var inst_34305 = (state_34331[(9)]);
var state_34331__$1 = state_34331;
var statearr_34358_35668 = state_34331__$1;
(statearr_34358_35668[(2)] = inst_34305);

(statearr_34358_35668[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (6))){
var state_34331__$1 = state_34331;
var statearr_34359_35669 = state_34331__$1;
(statearr_34359_35669[(2)] = false);

(statearr_34359_35669[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (28))){
var inst_34301 = (state_34331[(2)]);
var inst_34302 = calc_state();
var inst_34258 = inst_34302;
var state_34331__$1 = (function (){var statearr_34360 = state_34331;
(statearr_34360[(15)] = inst_34301);

(statearr_34360[(7)] = inst_34258);

return statearr_34360;
})();
var statearr_34361_35672 = state_34331__$1;
(statearr_34361_35672[(2)] = null);

(statearr_34361_35672[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (25))){
var inst_34327 = (state_34331[(2)]);
var state_34331__$1 = state_34331;
var statearr_34362_35673 = state_34331__$1;
(statearr_34362_35673[(2)] = inst_34327);

(statearr_34362_35673[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (34))){
var inst_34325 = (state_34331[(2)]);
var state_34331__$1 = state_34331;
var statearr_34363_35674 = state_34331__$1;
(statearr_34363_35674[(2)] = inst_34325);

(statearr_34363_35674[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (17))){
var state_34331__$1 = state_34331;
var statearr_34364_35675 = state_34331__$1;
(statearr_34364_35675[(2)] = false);

(statearr_34364_35675[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (3))){
var state_34331__$1 = state_34331;
var statearr_34365_35676 = state_34331__$1;
(statearr_34365_35676[(2)] = false);

(statearr_34365_35676[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (12))){
var inst_34329 = (state_34331[(2)]);
var state_34331__$1 = state_34331;
return cljs.core.async.impl.ioc_helpers.return_chan(state_34331__$1,inst_34329);
} else {
if((state_val_34332 === (2))){
var inst_34233 = (state_34331[(8)]);
var inst_34238 = inst_34233.cljs$lang$protocol_mask$partition0$;
var inst_34239 = (inst_34238 & (64));
var inst_34240 = inst_34233.cljs$core$ISeq$;
var inst_34241 = (cljs.core.PROTOCOL_SENTINEL === inst_34240);
var inst_34242 = ((inst_34239) || (inst_34241));
var state_34331__$1 = state_34331;
if(cljs.core.truth_(inst_34242)){
var statearr_34366_35677 = state_34331__$1;
(statearr_34366_35677[(1)] = (5));

} else {
var statearr_34367_35678 = state_34331__$1;
(statearr_34367_35678[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (23))){
var inst_34290 = (state_34331[(14)]);
var inst_34296 = (inst_34290 == null);
var state_34331__$1 = state_34331;
if(cljs.core.truth_(inst_34296)){
var statearr_34368_35682 = state_34331__$1;
(statearr_34368_35682[(1)] = (26));

} else {
var statearr_34369_35683 = state_34331__$1;
(statearr_34369_35683[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (35))){
var inst_34316 = (state_34331[(2)]);
var state_34331__$1 = state_34331;
if(cljs.core.truth_(inst_34316)){
var statearr_34370_35684 = state_34331__$1;
(statearr_34370_35684[(1)] = (36));

} else {
var statearr_34371_35689 = state_34331__$1;
(statearr_34371_35689[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (19))){
var inst_34258 = (state_34331[(7)]);
var inst_34278 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_34258);
var state_34331__$1 = state_34331;
var statearr_34372_35690 = state_34331__$1;
(statearr_34372_35690[(2)] = inst_34278);

(statearr_34372_35690[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (11))){
var inst_34258 = (state_34331[(7)]);
var inst_34262 = (inst_34258 == null);
var inst_34263 = cljs.core.not(inst_34262);
var state_34331__$1 = state_34331;
if(inst_34263){
var statearr_34373_35691 = state_34331__$1;
(statearr_34373_35691[(1)] = (13));

} else {
var statearr_34374_35692 = state_34331__$1;
(statearr_34374_35692[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (9))){
var inst_34233 = (state_34331[(8)]);
var state_34331__$1 = state_34331;
var statearr_34375_35693 = state_34331__$1;
(statearr_34375_35693[(2)] = inst_34233);

(statearr_34375_35693[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (5))){
var state_34331__$1 = state_34331;
var statearr_34376_35695 = state_34331__$1;
(statearr_34376_35695[(2)] = true);

(statearr_34376_35695[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (14))){
var state_34331__$1 = state_34331;
var statearr_34377_35696 = state_34331__$1;
(statearr_34377_35696[(2)] = false);

(statearr_34377_35696[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (26))){
var inst_34291 = (state_34331[(11)]);
var inst_34298 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cs,cljs.core.dissoc,inst_34291);
var state_34331__$1 = state_34331;
var statearr_34378_35702 = state_34331__$1;
(statearr_34378_35702[(2)] = inst_34298);

(statearr_34378_35702[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (16))){
var state_34331__$1 = state_34331;
var statearr_34379_35704 = state_34331__$1;
(statearr_34379_35704[(2)] = true);

(statearr_34379_35704[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (38))){
var inst_34321 = (state_34331[(2)]);
var state_34331__$1 = state_34331;
var statearr_34380_35706 = state_34331__$1;
(statearr_34380_35706[(2)] = inst_34321);

(statearr_34380_35706[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (30))){
var inst_34283 = (state_34331[(13)]);
var inst_34282 = (state_34331[(10)]);
var inst_34291 = (state_34331[(11)]);
var inst_34308 = cljs.core.empty_QMARK_(inst_34282);
var inst_34309 = (inst_34283.cljs$core$IFn$_invoke$arity$1 ? inst_34283.cljs$core$IFn$_invoke$arity$1(inst_34291) : inst_34283.call(null,inst_34291));
var inst_34310 = cljs.core.not(inst_34309);
var inst_34311 = ((inst_34308) && (inst_34310));
var state_34331__$1 = state_34331;
var statearr_34381_35708 = state_34331__$1;
(statearr_34381_35708[(2)] = inst_34311);

(statearr_34381_35708[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (10))){
var inst_34233 = (state_34331[(8)]);
var inst_34254 = (state_34331[(2)]);
var inst_34255 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_34254,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_34256 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_34254,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_34257 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_34254,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_34258 = inst_34233;
var state_34331__$1 = (function (){var statearr_34382 = state_34331;
(statearr_34382[(16)] = inst_34257);

(statearr_34382[(17)] = inst_34256);

(statearr_34382[(7)] = inst_34258);

(statearr_34382[(18)] = inst_34255);

return statearr_34382;
})();
var statearr_34383_35709 = state_34331__$1;
(statearr_34383_35709[(2)] = null);

(statearr_34383_35709[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (18))){
var inst_34273 = (state_34331[(2)]);
var state_34331__$1 = state_34331;
var statearr_34384_35711 = state_34331__$1;
(statearr_34384_35711[(2)] = inst_34273);

(statearr_34384_35711[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (37))){
var state_34331__$1 = state_34331;
var statearr_34385_35712 = state_34331__$1;
(statearr_34385_35712[(2)] = null);

(statearr_34385_35712[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34332 === (8))){
var inst_34233 = (state_34331[(8)]);
var inst_34251 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_34233);
var state_34331__$1 = state_34331;
var statearr_34386_35717 = state_34331__$1;
(statearr_34386_35717[(2)] = inst_34251);

(statearr_34386_35717[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto___35631,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;
return ((function (switch__33434__auto__,c__33535__auto___35631,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var cljs$core$async$mix_$_state_machine__33435__auto__ = null;
var cljs$core$async$mix_$_state_machine__33435__auto____0 = (function (){
var statearr_34387 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34387[(0)] = cljs$core$async$mix_$_state_machine__33435__auto__);

(statearr_34387[(1)] = (1));

return statearr_34387;
});
var cljs$core$async$mix_$_state_machine__33435__auto____1 = (function (state_34331){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_34331);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e34388){if((e34388 instanceof Object)){
var ex__33438__auto__ = e34388;
var statearr_34389_35719 = state_34331;
(statearr_34389_35719[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_34331);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34388;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35720 = state_34331;
state_34331 = G__35720;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__33435__auto__ = function(state_34331){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__33435__auto____1.call(this,state_34331);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__33435__auto____0;
cljs$core$async$mix_$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__33435__auto____1;
return cljs$core$async$mix_$_state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto___35631,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();
var state__33537__auto__ = (function (){var statearr_34390 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_34390[(6)] = c__33535__auto___35631);

return statearr_34390;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto___35631,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_(mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_(mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_(mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_(mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_(mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4434__auto__.call(null,p,v,ch,close_QMARK_));
} else {
var m__4431__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4431__auto__.call(null,p,v,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Pub.sub*",p);
}
}
}
});

cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4434__auto__.call(null,p,v,ch));
} else {
var m__4431__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4431__auto__.call(null,p,v,ch));
} else {
throw cljs.core.missing_protocol("Pub.unsub*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__34392 = arguments.length;
switch (G__34392) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4434__auto__.call(null,p));
} else {
var m__4431__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4431__auto__.call(null,p));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
var x__4433__auto__ = (((p == null))?null:p);
var m__4434__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4433__auto__)]);
if((!((m__4434__auto__ == null)))){
return (m__4434__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4434__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4434__auto__.call(null,p,v));
} else {
var m__4431__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4431__auto__ == null)))){
return (m__4431__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4431__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4431__auto__.call(null,p,v));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2;


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__34395 = arguments.length;
switch (G__34395) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3(ch,topic_fn,cljs.core.constantly(null));
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = ((function (mults){
return (function (topic){
var or__4131__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(mults),topic);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(mults,((function (or__4131__auto__,mults){
return (function (p1__34393_SHARP_){
if(cljs.core.truth_((p1__34393_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p1__34393_SHARP_.cljs$core$IFn$_invoke$arity$1(topic) : p1__34393_SHARP_.call(null,topic)))){
return p1__34393_SHARP_;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__34393_SHARP_,topic,cljs.core.async.mult(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((buf_fn.cljs$core$IFn$_invoke$arity$1 ? buf_fn.cljs$core$IFn$_invoke$arity$1(topic) : buf_fn.call(null,topic)))));
}
});})(or__4131__auto__,mults))
),topic);
}
});})(mults))
;
var p = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async34396 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async34396 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta34397){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta34397 = meta34397;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async34396.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_34398,meta34397__$1){
var self__ = this;
var _34398__$1 = this;
return (new cljs.core.async.t_cljs$core$async34396(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta34397__$1));
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async34396.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_34398){
var self__ = this;
var _34398__$1 = this;
return self__.meta34397;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async34396.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async34396.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async34396.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async34396.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = (self__.ensure_mult.cljs$core$IFn$_invoke$arity$1 ? self__.ensure_mult.cljs$core$IFn$_invoke$arity$1(topic) : self__.ensure_mult.call(null,topic));
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(m,ch__$1,close_QMARK_);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async34396.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5735__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self__.mults),topic);
if(cljs.core.truth_(temp__5735__auto__)){
var m = temp__5735__auto__;
return cljs.core.async.untap(m,ch__$1);
} else {
return null;
}
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async34396.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_(self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async34396.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async34396.getBasis = ((function (mults,ensure_mult){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta34397","meta34397",-1610854817,null)], null);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async34396.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async34396.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async34396";

cljs.core.async.t_cljs$core$async34396.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async34396");
});})(mults,ensure_mult))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async34396.
 */
cljs.core.async.__GT_t_cljs$core$async34396 = ((function (mults,ensure_mult){
return (function cljs$core$async$__GT_t_cljs$core$async34396(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta34397){
return (new cljs.core.async.t_cljs$core$async34396(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta34397));
});})(mults,ensure_mult))
;

}

return (new cljs.core.async.t_cljs$core$async34396(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__33535__auto___35740 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto___35740,mults,ensure_mult,p){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto___35740,mults,ensure_mult,p){
return (function (state_34470){
var state_val_34471 = (state_34470[(1)]);
if((state_val_34471 === (7))){
var inst_34466 = (state_34470[(2)]);
var state_34470__$1 = state_34470;
var statearr_34472_35741 = state_34470__$1;
(statearr_34472_35741[(2)] = inst_34466);

(statearr_34472_35741[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (20))){
var state_34470__$1 = state_34470;
var statearr_34473_35742 = state_34470__$1;
(statearr_34473_35742[(2)] = null);

(statearr_34473_35742[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (1))){
var state_34470__$1 = state_34470;
var statearr_34474_35743 = state_34470__$1;
(statearr_34474_35743[(2)] = null);

(statearr_34474_35743[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (24))){
var inst_34449 = (state_34470[(7)]);
var inst_34458 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(mults,cljs.core.dissoc,inst_34449);
var state_34470__$1 = state_34470;
var statearr_34475_35745 = state_34470__$1;
(statearr_34475_35745[(2)] = inst_34458);

(statearr_34475_35745[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (4))){
var inst_34401 = (state_34470[(8)]);
var inst_34401__$1 = (state_34470[(2)]);
var inst_34402 = (inst_34401__$1 == null);
var state_34470__$1 = (function (){var statearr_34476 = state_34470;
(statearr_34476[(8)] = inst_34401__$1);

return statearr_34476;
})();
if(cljs.core.truth_(inst_34402)){
var statearr_34477_35750 = state_34470__$1;
(statearr_34477_35750[(1)] = (5));

} else {
var statearr_34478_35751 = state_34470__$1;
(statearr_34478_35751[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (15))){
var inst_34443 = (state_34470[(2)]);
var state_34470__$1 = state_34470;
var statearr_34479_35752 = state_34470__$1;
(statearr_34479_35752[(2)] = inst_34443);

(statearr_34479_35752[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (21))){
var inst_34463 = (state_34470[(2)]);
var state_34470__$1 = (function (){var statearr_34480 = state_34470;
(statearr_34480[(9)] = inst_34463);

return statearr_34480;
})();
var statearr_34481_35753 = state_34470__$1;
(statearr_34481_35753[(2)] = null);

(statearr_34481_35753[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (13))){
var inst_34425 = (state_34470[(10)]);
var inst_34427 = cljs.core.chunked_seq_QMARK_(inst_34425);
var state_34470__$1 = state_34470;
if(inst_34427){
var statearr_34482_35754 = state_34470__$1;
(statearr_34482_35754[(1)] = (16));

} else {
var statearr_34483_35755 = state_34470__$1;
(statearr_34483_35755[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (22))){
var inst_34455 = (state_34470[(2)]);
var state_34470__$1 = state_34470;
if(cljs.core.truth_(inst_34455)){
var statearr_34484_35756 = state_34470__$1;
(statearr_34484_35756[(1)] = (23));

} else {
var statearr_34485_35757 = state_34470__$1;
(statearr_34485_35757[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (6))){
var inst_34451 = (state_34470[(11)]);
var inst_34449 = (state_34470[(7)]);
var inst_34401 = (state_34470[(8)]);
var inst_34449__$1 = (topic_fn.cljs$core$IFn$_invoke$arity$1 ? topic_fn.cljs$core$IFn$_invoke$arity$1(inst_34401) : topic_fn.call(null,inst_34401));
var inst_34450 = cljs.core.deref(mults);
var inst_34451__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_34450,inst_34449__$1);
var state_34470__$1 = (function (){var statearr_34486 = state_34470;
(statearr_34486[(11)] = inst_34451__$1);

(statearr_34486[(7)] = inst_34449__$1);

return statearr_34486;
})();
if(cljs.core.truth_(inst_34451__$1)){
var statearr_34487_35758 = state_34470__$1;
(statearr_34487_35758[(1)] = (19));

} else {
var statearr_34488_35759 = state_34470__$1;
(statearr_34488_35759[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (25))){
var inst_34460 = (state_34470[(2)]);
var state_34470__$1 = state_34470;
var statearr_34489_35760 = state_34470__$1;
(statearr_34489_35760[(2)] = inst_34460);

(statearr_34489_35760[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (17))){
var inst_34425 = (state_34470[(10)]);
var inst_34434 = cljs.core.first(inst_34425);
var inst_34435 = cljs.core.async.muxch_STAR_(inst_34434);
var inst_34436 = cljs.core.async.close_BANG_(inst_34435);
var inst_34437 = cljs.core.next(inst_34425);
var inst_34411 = inst_34437;
var inst_34412 = null;
var inst_34413 = (0);
var inst_34414 = (0);
var state_34470__$1 = (function (){var statearr_34490 = state_34470;
(statearr_34490[(12)] = inst_34411);

(statearr_34490[(13)] = inst_34436);

(statearr_34490[(14)] = inst_34412);

(statearr_34490[(15)] = inst_34414);

(statearr_34490[(16)] = inst_34413);

return statearr_34490;
})();
var statearr_34491_35762 = state_34470__$1;
(statearr_34491_35762[(2)] = null);

(statearr_34491_35762[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (3))){
var inst_34468 = (state_34470[(2)]);
var state_34470__$1 = state_34470;
return cljs.core.async.impl.ioc_helpers.return_chan(state_34470__$1,inst_34468);
} else {
if((state_val_34471 === (12))){
var inst_34445 = (state_34470[(2)]);
var state_34470__$1 = state_34470;
var statearr_34492_35763 = state_34470__$1;
(statearr_34492_35763[(2)] = inst_34445);

(statearr_34492_35763[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (2))){
var state_34470__$1 = state_34470;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34470__$1,(4),ch);
} else {
if((state_val_34471 === (23))){
var state_34470__$1 = state_34470;
var statearr_34493_35764 = state_34470__$1;
(statearr_34493_35764[(2)] = null);

(statearr_34493_35764[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (19))){
var inst_34451 = (state_34470[(11)]);
var inst_34401 = (state_34470[(8)]);
var inst_34453 = cljs.core.async.muxch_STAR_(inst_34451);
var state_34470__$1 = state_34470;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34470__$1,(22),inst_34453,inst_34401);
} else {
if((state_val_34471 === (11))){
var inst_34411 = (state_34470[(12)]);
var inst_34425 = (state_34470[(10)]);
var inst_34425__$1 = cljs.core.seq(inst_34411);
var state_34470__$1 = (function (){var statearr_34494 = state_34470;
(statearr_34494[(10)] = inst_34425__$1);

return statearr_34494;
})();
if(inst_34425__$1){
var statearr_34495_35770 = state_34470__$1;
(statearr_34495_35770[(1)] = (13));

} else {
var statearr_34496_35773 = state_34470__$1;
(statearr_34496_35773[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (9))){
var inst_34447 = (state_34470[(2)]);
var state_34470__$1 = state_34470;
var statearr_34497_35774 = state_34470__$1;
(statearr_34497_35774[(2)] = inst_34447);

(statearr_34497_35774[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (5))){
var inst_34408 = cljs.core.deref(mults);
var inst_34409 = cljs.core.vals(inst_34408);
var inst_34410 = cljs.core.seq(inst_34409);
var inst_34411 = inst_34410;
var inst_34412 = null;
var inst_34413 = (0);
var inst_34414 = (0);
var state_34470__$1 = (function (){var statearr_34498 = state_34470;
(statearr_34498[(12)] = inst_34411);

(statearr_34498[(14)] = inst_34412);

(statearr_34498[(15)] = inst_34414);

(statearr_34498[(16)] = inst_34413);

return statearr_34498;
})();
var statearr_34499_35775 = state_34470__$1;
(statearr_34499_35775[(2)] = null);

(statearr_34499_35775[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (14))){
var state_34470__$1 = state_34470;
var statearr_34503_35776 = state_34470__$1;
(statearr_34503_35776[(2)] = null);

(statearr_34503_35776[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (16))){
var inst_34425 = (state_34470[(10)]);
var inst_34429 = cljs.core.chunk_first(inst_34425);
var inst_34430 = cljs.core.chunk_rest(inst_34425);
var inst_34431 = cljs.core.count(inst_34429);
var inst_34411 = inst_34430;
var inst_34412 = inst_34429;
var inst_34413 = inst_34431;
var inst_34414 = (0);
var state_34470__$1 = (function (){var statearr_34504 = state_34470;
(statearr_34504[(12)] = inst_34411);

(statearr_34504[(14)] = inst_34412);

(statearr_34504[(15)] = inst_34414);

(statearr_34504[(16)] = inst_34413);

return statearr_34504;
})();
var statearr_34505_35777 = state_34470__$1;
(statearr_34505_35777[(2)] = null);

(statearr_34505_35777[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (10))){
var inst_34411 = (state_34470[(12)]);
var inst_34412 = (state_34470[(14)]);
var inst_34414 = (state_34470[(15)]);
var inst_34413 = (state_34470[(16)]);
var inst_34419 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_34412,inst_34414);
var inst_34420 = cljs.core.async.muxch_STAR_(inst_34419);
var inst_34421 = cljs.core.async.close_BANG_(inst_34420);
var inst_34422 = (inst_34414 + (1));
var tmp34500 = inst_34411;
var tmp34501 = inst_34412;
var tmp34502 = inst_34413;
var inst_34411__$1 = tmp34500;
var inst_34412__$1 = tmp34501;
var inst_34413__$1 = tmp34502;
var inst_34414__$1 = inst_34422;
var state_34470__$1 = (function (){var statearr_34506 = state_34470;
(statearr_34506[(12)] = inst_34411__$1);

(statearr_34506[(17)] = inst_34421);

(statearr_34506[(14)] = inst_34412__$1);

(statearr_34506[(15)] = inst_34414__$1);

(statearr_34506[(16)] = inst_34413__$1);

return statearr_34506;
})();
var statearr_34507_35779 = state_34470__$1;
(statearr_34507_35779[(2)] = null);

(statearr_34507_35779[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (18))){
var inst_34440 = (state_34470[(2)]);
var state_34470__$1 = state_34470;
var statearr_34508_35780 = state_34470__$1;
(statearr_34508_35780[(2)] = inst_34440);

(statearr_34508_35780[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34471 === (8))){
var inst_34414 = (state_34470[(15)]);
var inst_34413 = (state_34470[(16)]);
var inst_34416 = (inst_34414 < inst_34413);
var inst_34417 = inst_34416;
var state_34470__$1 = state_34470;
if(cljs.core.truth_(inst_34417)){
var statearr_34509_35781 = state_34470__$1;
(statearr_34509_35781[(1)] = (10));

} else {
var statearr_34510_35783 = state_34470__$1;
(statearr_34510_35783[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto___35740,mults,ensure_mult,p))
;
return ((function (switch__33434__auto__,c__33535__auto___35740,mults,ensure_mult,p){
return (function() {
var cljs$core$async$state_machine__33435__auto__ = null;
var cljs$core$async$state_machine__33435__auto____0 = (function (){
var statearr_34511 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34511[(0)] = cljs$core$async$state_machine__33435__auto__);

(statearr_34511[(1)] = (1));

return statearr_34511;
});
var cljs$core$async$state_machine__33435__auto____1 = (function (state_34470){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_34470);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e34512){if((e34512 instanceof Object)){
var ex__33438__auto__ = e34512;
var statearr_34513_35784 = state_34470;
(statearr_34513_35784[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_34470);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34512;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35785 = state_34470;
state_34470 = G__35785;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$state_machine__33435__auto__ = function(state_34470){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__33435__auto____1.call(this,state_34470);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__33435__auto____0;
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__33435__auto____1;
return cljs$core$async$state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto___35740,mults,ensure_mult,p))
})();
var state__33537__auto__ = (function (){var statearr_34514 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_34514[(6)] = c__33535__auto___35740);

return statearr_34514;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto___35740,mults,ensure_mult,p))
);


return p;
});

cljs.core.async.pub.cljs$lang$maxFixedArity = 3;

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__34516 = arguments.length;
switch (G__34516) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4(p,topic,ch,true);
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_(p,topic,ch,close_QMARK_);
});

cljs.core.async.sub.cljs$lang$maxFixedArity = 4;

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_(p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__34518 = arguments.length;
switch (G__34518) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1(p);
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2(p,topic);
});

cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2;

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__34520 = arguments.length;
switch (G__34520) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3(f,chs,null);
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec(chs);
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var cnt = cljs.core.count(chs__$1);
var rets = cljs.core.object_array.cljs$core$IFn$_invoke$arity$1(cnt);
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2(((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){
return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,rets.slice((0)));
} else {
return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.cljs$core$IFn$_invoke$arity$1(cnt));
var c__33535__auto___35801 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto___35801,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto___35801,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_34559){
var state_val_34560 = (state_34559[(1)]);
if((state_val_34560 === (7))){
var state_34559__$1 = state_34559;
var statearr_34561_35802 = state_34559__$1;
(statearr_34561_35802[(2)] = null);

(statearr_34561_35802[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34560 === (1))){
var state_34559__$1 = state_34559;
var statearr_34562_35803 = state_34559__$1;
(statearr_34562_35803[(2)] = null);

(statearr_34562_35803[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34560 === (4))){
var inst_34523 = (state_34559[(7)]);
var inst_34525 = (inst_34523 < cnt);
var state_34559__$1 = state_34559;
if(cljs.core.truth_(inst_34525)){
var statearr_34563_35804 = state_34559__$1;
(statearr_34563_35804[(1)] = (6));

} else {
var statearr_34564_35805 = state_34559__$1;
(statearr_34564_35805[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34560 === (15))){
var inst_34555 = (state_34559[(2)]);
var state_34559__$1 = state_34559;
var statearr_34565_35806 = state_34559__$1;
(statearr_34565_35806[(2)] = inst_34555);

(statearr_34565_35806[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34560 === (13))){
var inst_34548 = cljs.core.async.close_BANG_(out);
var state_34559__$1 = state_34559;
var statearr_34566_35807 = state_34559__$1;
(statearr_34566_35807[(2)] = inst_34548);

(statearr_34566_35807[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34560 === (6))){
var state_34559__$1 = state_34559;
var statearr_34567_35808 = state_34559__$1;
(statearr_34567_35808[(2)] = null);

(statearr_34567_35808[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34560 === (3))){
var inst_34557 = (state_34559[(2)]);
var state_34559__$1 = state_34559;
return cljs.core.async.impl.ioc_helpers.return_chan(state_34559__$1,inst_34557);
} else {
if((state_val_34560 === (12))){
var inst_34545 = (state_34559[(8)]);
var inst_34545__$1 = (state_34559[(2)]);
var inst_34546 = cljs.core.some(cljs.core.nil_QMARK_,inst_34545__$1);
var state_34559__$1 = (function (){var statearr_34568 = state_34559;
(statearr_34568[(8)] = inst_34545__$1);

return statearr_34568;
})();
if(cljs.core.truth_(inst_34546)){
var statearr_34569_35809 = state_34559__$1;
(statearr_34569_35809[(1)] = (13));

} else {
var statearr_34570_35810 = state_34559__$1;
(statearr_34570_35810[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34560 === (2))){
var inst_34522 = cljs.core.reset_BANG_(dctr,cnt);
var inst_34523 = (0);
var state_34559__$1 = (function (){var statearr_34571 = state_34559;
(statearr_34571[(7)] = inst_34523);

(statearr_34571[(9)] = inst_34522);

return statearr_34571;
})();
var statearr_34572_35811 = state_34559__$1;
(statearr_34572_35811[(2)] = null);

(statearr_34572_35811[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34560 === (11))){
var inst_34523 = (state_34559[(7)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame(state_34559,(10),Object,null,(9));
var inst_34532 = (chs__$1.cljs$core$IFn$_invoke$arity$1 ? chs__$1.cljs$core$IFn$_invoke$arity$1(inst_34523) : chs__$1.call(null,inst_34523));
var inst_34533 = (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(inst_34523) : done.call(null,inst_34523));
var inst_34534 = cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2(inst_34532,inst_34533);
var state_34559__$1 = state_34559;
var statearr_34573_35813 = state_34559__$1;
(statearr_34573_35813[(2)] = inst_34534);


cljs.core.async.impl.ioc_helpers.process_exception(state_34559__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34560 === (9))){
var inst_34523 = (state_34559[(7)]);
var inst_34536 = (state_34559[(2)]);
var inst_34537 = (inst_34523 + (1));
var inst_34523__$1 = inst_34537;
var state_34559__$1 = (function (){var statearr_34574 = state_34559;
(statearr_34574[(10)] = inst_34536);

(statearr_34574[(7)] = inst_34523__$1);

return statearr_34574;
})();
var statearr_34575_35816 = state_34559__$1;
(statearr_34575_35816[(2)] = null);

(statearr_34575_35816[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34560 === (5))){
var inst_34543 = (state_34559[(2)]);
var state_34559__$1 = (function (){var statearr_34576 = state_34559;
(statearr_34576[(11)] = inst_34543);

return statearr_34576;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34559__$1,(12),dchan);
} else {
if((state_val_34560 === (14))){
var inst_34545 = (state_34559[(8)]);
var inst_34550 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,inst_34545);
var state_34559__$1 = state_34559;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34559__$1,(16),out,inst_34550);
} else {
if((state_val_34560 === (16))){
var inst_34552 = (state_34559[(2)]);
var state_34559__$1 = (function (){var statearr_34577 = state_34559;
(statearr_34577[(12)] = inst_34552);

return statearr_34577;
})();
var statearr_34578_35819 = state_34559__$1;
(statearr_34578_35819[(2)] = null);

(statearr_34578_35819[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34560 === (10))){
var inst_34527 = (state_34559[(2)]);
var inst_34528 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec);
var state_34559__$1 = (function (){var statearr_34579 = state_34559;
(statearr_34579[(13)] = inst_34527);

return statearr_34579;
})();
var statearr_34580_35820 = state_34559__$1;
(statearr_34580_35820[(2)] = inst_34528);


cljs.core.async.impl.ioc_helpers.process_exception(state_34559__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34560 === (8))){
var inst_34541 = (state_34559[(2)]);
var state_34559__$1 = state_34559;
var statearr_34581_35821 = state_34559__$1;
(statearr_34581_35821[(2)] = inst_34541);

(statearr_34581_35821[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto___35801,chs__$1,out,cnt,rets,dchan,dctr,done))
;
return ((function (switch__33434__auto__,c__33535__auto___35801,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var cljs$core$async$state_machine__33435__auto__ = null;
var cljs$core$async$state_machine__33435__auto____0 = (function (){
var statearr_34582 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34582[(0)] = cljs$core$async$state_machine__33435__auto__);

(statearr_34582[(1)] = (1));

return statearr_34582;
});
var cljs$core$async$state_machine__33435__auto____1 = (function (state_34559){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_34559);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e34583){if((e34583 instanceof Object)){
var ex__33438__auto__ = e34583;
var statearr_34584_35822 = state_34559;
(statearr_34584_35822[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_34559);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34583;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35823 = state_34559;
state_34559 = G__35823;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$state_machine__33435__auto__ = function(state_34559){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__33435__auto____1.call(this,state_34559);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__33435__auto____0;
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__33435__auto____1;
return cljs$core$async$state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto___35801,chs__$1,out,cnt,rets,dchan,dctr,done))
})();
var state__33537__auto__ = (function (){var statearr_34585 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_34585[(6)] = c__33535__auto___35801);

return statearr_34585;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto___35801,chs__$1,out,cnt,rets,dchan,dctr,done))
);


return out;
});

cljs.core.async.map.cljs$lang$maxFixedArity = 3;

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__34588 = arguments.length;
switch (G__34588) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2(chs,null);
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__33535__auto___35826 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto___35826,out){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto___35826,out){
return (function (state_34620){
var state_val_34621 = (state_34620[(1)]);
if((state_val_34621 === (7))){
var inst_34599 = (state_34620[(7)]);
var inst_34600 = (state_34620[(8)]);
var inst_34599__$1 = (state_34620[(2)]);
var inst_34600__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_34599__$1,(0),null);
var inst_34601 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_34599__$1,(1),null);
var inst_34602 = (inst_34600__$1 == null);
var state_34620__$1 = (function (){var statearr_34622 = state_34620;
(statearr_34622[(9)] = inst_34601);

(statearr_34622[(7)] = inst_34599__$1);

(statearr_34622[(8)] = inst_34600__$1);

return statearr_34622;
})();
if(cljs.core.truth_(inst_34602)){
var statearr_34623_35827 = state_34620__$1;
(statearr_34623_35827[(1)] = (8));

} else {
var statearr_34624_35828 = state_34620__$1;
(statearr_34624_35828[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34621 === (1))){
var inst_34589 = cljs.core.vec(chs);
var inst_34590 = inst_34589;
var state_34620__$1 = (function (){var statearr_34625 = state_34620;
(statearr_34625[(10)] = inst_34590);

return statearr_34625;
})();
var statearr_34626_35830 = state_34620__$1;
(statearr_34626_35830[(2)] = null);

(statearr_34626_35830[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34621 === (4))){
var inst_34590 = (state_34620[(10)]);
var state_34620__$1 = state_34620;
return cljs.core.async.ioc_alts_BANG_(state_34620__$1,(7),inst_34590);
} else {
if((state_val_34621 === (6))){
var inst_34616 = (state_34620[(2)]);
var state_34620__$1 = state_34620;
var statearr_34627_35832 = state_34620__$1;
(statearr_34627_35832[(2)] = inst_34616);

(statearr_34627_35832[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34621 === (3))){
var inst_34618 = (state_34620[(2)]);
var state_34620__$1 = state_34620;
return cljs.core.async.impl.ioc_helpers.return_chan(state_34620__$1,inst_34618);
} else {
if((state_val_34621 === (2))){
var inst_34590 = (state_34620[(10)]);
var inst_34592 = cljs.core.count(inst_34590);
var inst_34593 = (inst_34592 > (0));
var state_34620__$1 = state_34620;
if(cljs.core.truth_(inst_34593)){
var statearr_34629_35833 = state_34620__$1;
(statearr_34629_35833[(1)] = (4));

} else {
var statearr_34630_35834 = state_34620__$1;
(statearr_34630_35834[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34621 === (11))){
var inst_34590 = (state_34620[(10)]);
var inst_34609 = (state_34620[(2)]);
var tmp34628 = inst_34590;
var inst_34590__$1 = tmp34628;
var state_34620__$1 = (function (){var statearr_34631 = state_34620;
(statearr_34631[(10)] = inst_34590__$1);

(statearr_34631[(11)] = inst_34609);

return statearr_34631;
})();
var statearr_34632_35838 = state_34620__$1;
(statearr_34632_35838[(2)] = null);

(statearr_34632_35838[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34621 === (9))){
var inst_34600 = (state_34620[(8)]);
var state_34620__$1 = state_34620;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34620__$1,(11),out,inst_34600);
} else {
if((state_val_34621 === (5))){
var inst_34614 = cljs.core.async.close_BANG_(out);
var state_34620__$1 = state_34620;
var statearr_34633_35843 = state_34620__$1;
(statearr_34633_35843[(2)] = inst_34614);

(statearr_34633_35843[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34621 === (10))){
var inst_34612 = (state_34620[(2)]);
var state_34620__$1 = state_34620;
var statearr_34634_35847 = state_34620__$1;
(statearr_34634_35847[(2)] = inst_34612);

(statearr_34634_35847[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34621 === (8))){
var inst_34601 = (state_34620[(9)]);
var inst_34599 = (state_34620[(7)]);
var inst_34590 = (state_34620[(10)]);
var inst_34600 = (state_34620[(8)]);
var inst_34604 = (function (){var cs = inst_34590;
var vec__34595 = inst_34599;
var v = inst_34600;
var c = inst_34601;
return ((function (cs,vec__34595,v,c,inst_34601,inst_34599,inst_34590,inst_34600,state_val_34621,c__33535__auto___35826,out){
return (function (p1__34586_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(c,p1__34586_SHARP_);
});
;})(cs,vec__34595,v,c,inst_34601,inst_34599,inst_34590,inst_34600,state_val_34621,c__33535__auto___35826,out))
})();
var inst_34605 = cljs.core.filterv(inst_34604,inst_34590);
var inst_34590__$1 = inst_34605;
var state_34620__$1 = (function (){var statearr_34635 = state_34620;
(statearr_34635[(10)] = inst_34590__$1);

return statearr_34635;
})();
var statearr_34636_35851 = state_34620__$1;
(statearr_34636_35851[(2)] = null);

(statearr_34636_35851[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto___35826,out))
;
return ((function (switch__33434__auto__,c__33535__auto___35826,out){
return (function() {
var cljs$core$async$state_machine__33435__auto__ = null;
var cljs$core$async$state_machine__33435__auto____0 = (function (){
var statearr_34637 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34637[(0)] = cljs$core$async$state_machine__33435__auto__);

(statearr_34637[(1)] = (1));

return statearr_34637;
});
var cljs$core$async$state_machine__33435__auto____1 = (function (state_34620){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_34620);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e34638){if((e34638 instanceof Object)){
var ex__33438__auto__ = e34638;
var statearr_34639_35855 = state_34620;
(statearr_34639_35855[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_34620);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34638;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35856 = state_34620;
state_34620 = G__35856;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$state_machine__33435__auto__ = function(state_34620){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__33435__auto____1.call(this,state_34620);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__33435__auto____0;
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__33435__auto____1;
return cljs$core$async$state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto___35826,out))
})();
var state__33537__auto__ = (function (){var statearr_34640 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_34640[(6)] = c__33535__auto___35826);

return statearr_34640;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto___35826,out))
);


return out;
});

cljs.core.async.merge.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce(cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__34642 = arguments.length;
switch (G__34642) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3(n,ch,null);
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__33535__auto___35862 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto___35862,out){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto___35862,out){
return (function (state_34666){
var state_val_34667 = (state_34666[(1)]);
if((state_val_34667 === (7))){
var inst_34648 = (state_34666[(7)]);
var inst_34648__$1 = (state_34666[(2)]);
var inst_34649 = (inst_34648__$1 == null);
var inst_34650 = cljs.core.not(inst_34649);
var state_34666__$1 = (function (){var statearr_34668 = state_34666;
(statearr_34668[(7)] = inst_34648__$1);

return statearr_34668;
})();
if(inst_34650){
var statearr_34669_35863 = state_34666__$1;
(statearr_34669_35863[(1)] = (8));

} else {
var statearr_34670_35864 = state_34666__$1;
(statearr_34670_35864[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34667 === (1))){
var inst_34643 = (0);
var state_34666__$1 = (function (){var statearr_34671 = state_34666;
(statearr_34671[(8)] = inst_34643);

return statearr_34671;
})();
var statearr_34672_35865 = state_34666__$1;
(statearr_34672_35865[(2)] = null);

(statearr_34672_35865[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34667 === (4))){
var state_34666__$1 = state_34666;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34666__$1,(7),ch);
} else {
if((state_val_34667 === (6))){
var inst_34661 = (state_34666[(2)]);
var state_34666__$1 = state_34666;
var statearr_34673_35875 = state_34666__$1;
(statearr_34673_35875[(2)] = inst_34661);

(statearr_34673_35875[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34667 === (3))){
var inst_34663 = (state_34666[(2)]);
var inst_34664 = cljs.core.async.close_BANG_(out);
var state_34666__$1 = (function (){var statearr_34674 = state_34666;
(statearr_34674[(9)] = inst_34663);

return statearr_34674;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_34666__$1,inst_34664);
} else {
if((state_val_34667 === (2))){
var inst_34643 = (state_34666[(8)]);
var inst_34645 = (inst_34643 < n);
var state_34666__$1 = state_34666;
if(cljs.core.truth_(inst_34645)){
var statearr_34675_35882 = state_34666__$1;
(statearr_34675_35882[(1)] = (4));

} else {
var statearr_34676_35883 = state_34666__$1;
(statearr_34676_35883[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34667 === (11))){
var inst_34643 = (state_34666[(8)]);
var inst_34653 = (state_34666[(2)]);
var inst_34654 = (inst_34643 + (1));
var inst_34643__$1 = inst_34654;
var state_34666__$1 = (function (){var statearr_34677 = state_34666;
(statearr_34677[(8)] = inst_34643__$1);

(statearr_34677[(10)] = inst_34653);

return statearr_34677;
})();
var statearr_34678_35884 = state_34666__$1;
(statearr_34678_35884[(2)] = null);

(statearr_34678_35884[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34667 === (9))){
var state_34666__$1 = state_34666;
var statearr_34679_35885 = state_34666__$1;
(statearr_34679_35885[(2)] = null);

(statearr_34679_35885[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34667 === (5))){
var state_34666__$1 = state_34666;
var statearr_34680_35886 = state_34666__$1;
(statearr_34680_35886[(2)] = null);

(statearr_34680_35886[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34667 === (10))){
var inst_34658 = (state_34666[(2)]);
var state_34666__$1 = state_34666;
var statearr_34681_35887 = state_34666__$1;
(statearr_34681_35887[(2)] = inst_34658);

(statearr_34681_35887[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34667 === (8))){
var inst_34648 = (state_34666[(7)]);
var state_34666__$1 = state_34666;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34666__$1,(11),out,inst_34648);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto___35862,out))
;
return ((function (switch__33434__auto__,c__33535__auto___35862,out){
return (function() {
var cljs$core$async$state_machine__33435__auto__ = null;
var cljs$core$async$state_machine__33435__auto____0 = (function (){
var statearr_34682 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_34682[(0)] = cljs$core$async$state_machine__33435__auto__);

(statearr_34682[(1)] = (1));

return statearr_34682;
});
var cljs$core$async$state_machine__33435__auto____1 = (function (state_34666){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_34666);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e34683){if((e34683 instanceof Object)){
var ex__33438__auto__ = e34683;
var statearr_34684_35888 = state_34666;
(statearr_34684_35888[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_34666);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34683;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35889 = state_34666;
state_34666 = G__35889;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$state_machine__33435__auto__ = function(state_34666){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__33435__auto____1.call(this,state_34666);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__33435__auto____0;
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__33435__auto____1;
return cljs$core$async$state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto___35862,out))
})();
var state__33537__auto__ = (function (){var statearr_34685 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_34685[(6)] = c__33535__auto___35862);

return statearr_34685;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto___35862,out))
);


return out;
});

cljs.core.async.take.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async34687 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async34687 = (function (f,ch,meta34688){
this.f = f;
this.ch = ch;
this.meta34688 = meta34688;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async34687.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_34689,meta34688__$1){
var self__ = this;
var _34689__$1 = this;
return (new cljs.core.async.t_cljs$core$async34687(self__.f,self__.ch,meta34688__$1));
});

cljs.core.async.t_cljs$core$async34687.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_34689){
var self__ = this;
var _34689__$1 = this;
return self__.meta34688;
});

cljs.core.async.t_cljs$core$async34687.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async34687.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
});

cljs.core.async.t_cljs$core$async34687.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
});

cljs.core.async.t_cljs$core$async34687.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async34687.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_(self__.ch,(function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async34690 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async34690 = (function (f,ch,meta34688,_,fn1,meta34691){
this.f = f;
this.ch = ch;
this.meta34688 = meta34688;
this._ = _;
this.fn1 = fn1;
this.meta34691 = meta34691;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async34690.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_34692,meta34691__$1){
var self__ = this;
var _34692__$1 = this;
return (new cljs.core.async.t_cljs$core$async34690(self__.f,self__.ch,self__.meta34688,self__._,self__.fn1,meta34691__$1));
});})(___$1))
;

cljs.core.async.t_cljs$core$async34690.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_34692){
var self__ = this;
var _34692__$1 = this;
return self__.meta34691;
});})(___$1))
;

cljs.core.async.t_cljs$core$async34690.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async34690.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.fn1);
});})(___$1))
;

cljs.core.async.t_cljs$core$async34690.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
});})(___$1))
;

cljs.core.async.t_cljs$core$async34690.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit(self__.fn1);
return ((function (f1,___$2,___$1){
return (function (p1__34686_SHARP_){
var G__34693 = (((p1__34686_SHARP_ == null))?null:(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(p1__34686_SHARP_) : self__.f.call(null,p1__34686_SHARP_)));
return (f1.cljs$core$IFn$_invoke$arity$1 ? f1.cljs$core$IFn$_invoke$arity$1(G__34693) : f1.call(null,G__34693));
});
;})(f1,___$2,___$1))
});})(___$1))
;

cljs.core.async.t_cljs$core$async34690.getBasis = ((function (___$1){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta34688","meta34688",-276299391,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async34687","cljs.core.async/t_cljs$core$async34687",-920083196,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta34691","meta34691",1237846814,null)], null);
});})(___$1))
;

cljs.core.async.t_cljs$core$async34690.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async34690.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async34690";

cljs.core.async.t_cljs$core$async34690.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async34690");
});})(___$1))
;

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async34690.
 */
cljs.core.async.__GT_t_cljs$core$async34690 = ((function (___$1){
return (function cljs$core$async$map_LT__$___GT_t_cljs$core$async34690(f__$1,ch__$1,meta34688__$1,___$2,fn1__$1,meta34691){
return (new cljs.core.async.t_cljs$core$async34690(f__$1,ch__$1,meta34688__$1,___$2,fn1__$1,meta34691));
});})(___$1))
;

}

return (new cljs.core.async.t_cljs$core$async34690(self__.f,self__.ch,self__.meta34688,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__4120__auto__ = ret;
if(cljs.core.truth_(and__4120__auto__)){
return (!((cljs.core.deref(ret) == null)));
} else {
return and__4120__auto__;
}
})())){
return cljs.core.async.impl.channels.box((function (){var G__34694 = cljs.core.deref(ret);
return (self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(G__34694) : self__.f.call(null,G__34694));
})());
} else {
return ret;
}
});

cljs.core.async.t_cljs$core$async34687.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async34687.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
});

cljs.core.async.t_cljs$core$async34687.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta34688","meta34688",-276299391,null)], null);
});

cljs.core.async.t_cljs$core$async34687.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async34687.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async34687";

cljs.core.async.t_cljs$core$async34687.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async34687");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async34687.
 */
cljs.core.async.__GT_t_cljs$core$async34687 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async34687(f__$1,ch__$1,meta34688){
return (new cljs.core.async.t_cljs$core$async34687(f__$1,ch__$1,meta34688));
});

}

return (new cljs.core.async.t_cljs$core$async34687(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async34695 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async34695 = (function (f,ch,meta34696){
this.f = f;
this.ch = ch;
this.meta34696 = meta34696;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async34695.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_34697,meta34696__$1){
var self__ = this;
var _34697__$1 = this;
return (new cljs.core.async.t_cljs$core$async34695(self__.f,self__.ch,meta34696__$1));
});

cljs.core.async.t_cljs$core$async34695.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_34697){
var self__ = this;
var _34697__$1 = this;
return self__.meta34696;
});

cljs.core.async.t_cljs$core$async34695.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async34695.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
});

cljs.core.async.t_cljs$core$async34695.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async34695.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async34695.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async34695.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(val) : self__.f.call(null,val)),fn1);
});

cljs.core.async.t_cljs$core$async34695.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta34696","meta34696",1298886682,null)], null);
});

cljs.core.async.t_cljs$core$async34695.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async34695.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async34695";

cljs.core.async.t_cljs$core$async34695.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async34695");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async34695.
 */
cljs.core.async.__GT_t_cljs$core$async34695 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async34695(f__$1,ch__$1,meta34696){
return (new cljs.core.async.t_cljs$core$async34695(f__$1,ch__$1,meta34696));
});

}

return (new cljs.core.async.t_cljs$core$async34695(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async34698 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async34698 = (function (p,ch,meta34699){
this.p = p;
this.ch = ch;
this.meta34699 = meta34699;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async34698.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_34700,meta34699__$1){
var self__ = this;
var _34700__$1 = this;
return (new cljs.core.async.t_cljs$core$async34698(self__.p,self__.ch,meta34699__$1));
});

cljs.core.async.t_cljs$core$async34698.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_34700){
var self__ = this;
var _34700__$1 = this;
return self__.meta34699;
});

cljs.core.async.t_cljs$core$async34698.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async34698.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
});

cljs.core.async.t_cljs$core$async34698.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
});

cljs.core.async.t_cljs$core$async34698.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async34698.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async34698.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async34698.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.p.cljs$core$IFn$_invoke$arity$1 ? self__.p.cljs$core$IFn$_invoke$arity$1(val) : self__.p.call(null,val)))){
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box(cljs.core.not(cljs.core.async.impl.protocols.closed_QMARK_(self__.ch)));
}
});

cljs.core.async.t_cljs$core$async34698.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta34699","meta34699",-357762566,null)], null);
});

cljs.core.async.t_cljs$core$async34698.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async34698.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async34698";

cljs.core.async.t_cljs$core$async34698.cljs$lang$ctorPrWriter = (function (this__4374__auto__,writer__4375__auto__,opt__4376__auto__){
return cljs.core._write(writer__4375__auto__,"cljs.core.async/t_cljs$core$async34698");
});

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async34698.
 */
cljs.core.async.__GT_t_cljs$core$async34698 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async34698(p__$1,ch__$1,meta34699){
return (new cljs.core.async.t_cljs$core$async34698(p__$1,ch__$1,meta34699));
});

}

return (new cljs.core.async.t_cljs$core$async34698(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_(cljs.core.complement(p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__34702 = arguments.length;
switch (G__34702) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__33535__auto___35915 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto___35915,out){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto___35915,out){
return (function (state_34723){
var state_val_34724 = (state_34723[(1)]);
if((state_val_34724 === (7))){
var inst_34719 = (state_34723[(2)]);
var state_34723__$1 = state_34723;
var statearr_34725_35916 = state_34723__$1;
(statearr_34725_35916[(2)] = inst_34719);

(statearr_34725_35916[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34724 === (1))){
var state_34723__$1 = state_34723;
var statearr_34726_35917 = state_34723__$1;
(statearr_34726_35917[(2)] = null);

(statearr_34726_35917[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34724 === (4))){
var inst_34705 = (state_34723[(7)]);
var inst_34705__$1 = (state_34723[(2)]);
var inst_34706 = (inst_34705__$1 == null);
var state_34723__$1 = (function (){var statearr_34727 = state_34723;
(statearr_34727[(7)] = inst_34705__$1);

return statearr_34727;
})();
if(cljs.core.truth_(inst_34706)){
var statearr_34728_35920 = state_34723__$1;
(statearr_34728_35920[(1)] = (5));

} else {
var statearr_34729_35921 = state_34723__$1;
(statearr_34729_35921[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34724 === (6))){
var inst_34705 = (state_34723[(7)]);
var inst_34710 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_34705) : p.call(null,inst_34705));
var state_34723__$1 = state_34723;
if(cljs.core.truth_(inst_34710)){
var statearr_34730_35922 = state_34723__$1;
(statearr_34730_35922[(1)] = (8));

} else {
var statearr_34731_35923 = state_34723__$1;
(statearr_34731_35923[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34724 === (3))){
var inst_34721 = (state_34723[(2)]);
var state_34723__$1 = state_34723;
return cljs.core.async.impl.ioc_helpers.return_chan(state_34723__$1,inst_34721);
} else {
if((state_val_34724 === (2))){
var state_34723__$1 = state_34723;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34723__$1,(4),ch);
} else {
if((state_val_34724 === (11))){
var inst_34713 = (state_34723[(2)]);
var state_34723__$1 = state_34723;
var statearr_34732_35924 = state_34723__$1;
(statearr_34732_35924[(2)] = inst_34713);

(statearr_34732_35924[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34724 === (9))){
var state_34723__$1 = state_34723;
var statearr_34733_35925 = state_34723__$1;
(statearr_34733_35925[(2)] = null);

(statearr_34733_35925[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34724 === (5))){
var inst_34708 = cljs.core.async.close_BANG_(out);
var state_34723__$1 = state_34723;
var statearr_34734_35926 = state_34723__$1;
(statearr_34734_35926[(2)] = inst_34708);

(statearr_34734_35926[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34724 === (10))){
var inst_34716 = (state_34723[(2)]);
var state_34723__$1 = (function (){var statearr_34735 = state_34723;
(statearr_34735[(8)] = inst_34716);

return statearr_34735;
})();
var statearr_34736_35927 = state_34723__$1;
(statearr_34736_35927[(2)] = null);

(statearr_34736_35927[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34724 === (8))){
var inst_34705 = (state_34723[(7)]);
var state_34723__$1 = state_34723;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34723__$1,(11),out,inst_34705);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto___35915,out))
;
return ((function (switch__33434__auto__,c__33535__auto___35915,out){
return (function() {
var cljs$core$async$state_machine__33435__auto__ = null;
var cljs$core$async$state_machine__33435__auto____0 = (function (){
var statearr_34737 = [null,null,null,null,null,null,null,null,null];
(statearr_34737[(0)] = cljs$core$async$state_machine__33435__auto__);

(statearr_34737[(1)] = (1));

return statearr_34737;
});
var cljs$core$async$state_machine__33435__auto____1 = (function (state_34723){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_34723);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e34738){if((e34738 instanceof Object)){
var ex__33438__auto__ = e34738;
var statearr_34739_35930 = state_34723;
(statearr_34739_35930[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_34723);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34738;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35931 = state_34723;
state_34723 = G__35931;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$state_machine__33435__auto__ = function(state_34723){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__33435__auto____1.call(this,state_34723);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__33435__auto____0;
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__33435__auto____1;
return cljs$core$async$state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto___35915,out))
})();
var state__33537__auto__ = (function (){var statearr_34740 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_34740[(6)] = c__33535__auto___35915);

return statearr_34740;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto___35915,out))
);


return out;
});

cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__34742 = arguments.length;
switch (G__34742) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(cljs.core.complement(p),ch,buf_or_n);
});

cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3;

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__33535__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto__){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto__){
return (function (state_34804){
var state_val_34805 = (state_34804[(1)]);
if((state_val_34805 === (7))){
var inst_34800 = (state_34804[(2)]);
var state_34804__$1 = state_34804;
var statearr_34806_35942 = state_34804__$1;
(statearr_34806_35942[(2)] = inst_34800);

(statearr_34806_35942[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (20))){
var inst_34770 = (state_34804[(7)]);
var inst_34781 = (state_34804[(2)]);
var inst_34782 = cljs.core.next(inst_34770);
var inst_34756 = inst_34782;
var inst_34757 = null;
var inst_34758 = (0);
var inst_34759 = (0);
var state_34804__$1 = (function (){var statearr_34807 = state_34804;
(statearr_34807[(8)] = inst_34758);

(statearr_34807[(9)] = inst_34757);

(statearr_34807[(10)] = inst_34781);

(statearr_34807[(11)] = inst_34759);

(statearr_34807[(12)] = inst_34756);

return statearr_34807;
})();
var statearr_34808_35944 = state_34804__$1;
(statearr_34808_35944[(2)] = null);

(statearr_34808_35944[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (1))){
var state_34804__$1 = state_34804;
var statearr_34809_35945 = state_34804__$1;
(statearr_34809_35945[(2)] = null);

(statearr_34809_35945[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (4))){
var inst_34745 = (state_34804[(13)]);
var inst_34745__$1 = (state_34804[(2)]);
var inst_34746 = (inst_34745__$1 == null);
var state_34804__$1 = (function (){var statearr_34810 = state_34804;
(statearr_34810[(13)] = inst_34745__$1);

return statearr_34810;
})();
if(cljs.core.truth_(inst_34746)){
var statearr_34811_35948 = state_34804__$1;
(statearr_34811_35948[(1)] = (5));

} else {
var statearr_34812_35949 = state_34804__$1;
(statearr_34812_35949[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (15))){
var state_34804__$1 = state_34804;
var statearr_34816_35951 = state_34804__$1;
(statearr_34816_35951[(2)] = null);

(statearr_34816_35951[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (21))){
var state_34804__$1 = state_34804;
var statearr_34817_35952 = state_34804__$1;
(statearr_34817_35952[(2)] = null);

(statearr_34817_35952[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (13))){
var inst_34758 = (state_34804[(8)]);
var inst_34757 = (state_34804[(9)]);
var inst_34759 = (state_34804[(11)]);
var inst_34756 = (state_34804[(12)]);
var inst_34766 = (state_34804[(2)]);
var inst_34767 = (inst_34759 + (1));
var tmp34813 = inst_34758;
var tmp34814 = inst_34757;
var tmp34815 = inst_34756;
var inst_34756__$1 = tmp34815;
var inst_34757__$1 = tmp34814;
var inst_34758__$1 = tmp34813;
var inst_34759__$1 = inst_34767;
var state_34804__$1 = (function (){var statearr_34818 = state_34804;
(statearr_34818[(8)] = inst_34758__$1);

(statearr_34818[(9)] = inst_34757__$1);

(statearr_34818[(14)] = inst_34766);

(statearr_34818[(11)] = inst_34759__$1);

(statearr_34818[(12)] = inst_34756__$1);

return statearr_34818;
})();
var statearr_34819_35954 = state_34804__$1;
(statearr_34819_35954[(2)] = null);

(statearr_34819_35954[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (22))){
var state_34804__$1 = state_34804;
var statearr_34820_35955 = state_34804__$1;
(statearr_34820_35955[(2)] = null);

(statearr_34820_35955[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (6))){
var inst_34745 = (state_34804[(13)]);
var inst_34754 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_34745) : f.call(null,inst_34745));
var inst_34755 = cljs.core.seq(inst_34754);
var inst_34756 = inst_34755;
var inst_34757 = null;
var inst_34758 = (0);
var inst_34759 = (0);
var state_34804__$1 = (function (){var statearr_34821 = state_34804;
(statearr_34821[(8)] = inst_34758);

(statearr_34821[(9)] = inst_34757);

(statearr_34821[(11)] = inst_34759);

(statearr_34821[(12)] = inst_34756);

return statearr_34821;
})();
var statearr_34822_35956 = state_34804__$1;
(statearr_34822_35956[(2)] = null);

(statearr_34822_35956[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (17))){
var inst_34770 = (state_34804[(7)]);
var inst_34774 = cljs.core.chunk_first(inst_34770);
var inst_34775 = cljs.core.chunk_rest(inst_34770);
var inst_34776 = cljs.core.count(inst_34774);
var inst_34756 = inst_34775;
var inst_34757 = inst_34774;
var inst_34758 = inst_34776;
var inst_34759 = (0);
var state_34804__$1 = (function (){var statearr_34823 = state_34804;
(statearr_34823[(8)] = inst_34758);

(statearr_34823[(9)] = inst_34757);

(statearr_34823[(11)] = inst_34759);

(statearr_34823[(12)] = inst_34756);

return statearr_34823;
})();
var statearr_34824_35957 = state_34804__$1;
(statearr_34824_35957[(2)] = null);

(statearr_34824_35957[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (3))){
var inst_34802 = (state_34804[(2)]);
var state_34804__$1 = state_34804;
return cljs.core.async.impl.ioc_helpers.return_chan(state_34804__$1,inst_34802);
} else {
if((state_val_34805 === (12))){
var inst_34790 = (state_34804[(2)]);
var state_34804__$1 = state_34804;
var statearr_34825_35958 = state_34804__$1;
(statearr_34825_35958[(2)] = inst_34790);

(statearr_34825_35958[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (2))){
var state_34804__$1 = state_34804;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34804__$1,(4),in$);
} else {
if((state_val_34805 === (23))){
var inst_34798 = (state_34804[(2)]);
var state_34804__$1 = state_34804;
var statearr_34826_35959 = state_34804__$1;
(statearr_34826_35959[(2)] = inst_34798);

(statearr_34826_35959[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (19))){
var inst_34785 = (state_34804[(2)]);
var state_34804__$1 = state_34804;
var statearr_34827_35960 = state_34804__$1;
(statearr_34827_35960[(2)] = inst_34785);

(statearr_34827_35960[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (11))){
var inst_34770 = (state_34804[(7)]);
var inst_34756 = (state_34804[(12)]);
var inst_34770__$1 = cljs.core.seq(inst_34756);
var state_34804__$1 = (function (){var statearr_34828 = state_34804;
(statearr_34828[(7)] = inst_34770__$1);

return statearr_34828;
})();
if(inst_34770__$1){
var statearr_34829_35961 = state_34804__$1;
(statearr_34829_35961[(1)] = (14));

} else {
var statearr_34830_35962 = state_34804__$1;
(statearr_34830_35962[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (9))){
var inst_34792 = (state_34804[(2)]);
var inst_34793 = cljs.core.async.impl.protocols.closed_QMARK_(out);
var state_34804__$1 = (function (){var statearr_34831 = state_34804;
(statearr_34831[(15)] = inst_34792);

return statearr_34831;
})();
if(cljs.core.truth_(inst_34793)){
var statearr_34832_35964 = state_34804__$1;
(statearr_34832_35964[(1)] = (21));

} else {
var statearr_34833_35965 = state_34804__$1;
(statearr_34833_35965[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (5))){
var inst_34748 = cljs.core.async.close_BANG_(out);
var state_34804__$1 = state_34804;
var statearr_34834_35966 = state_34804__$1;
(statearr_34834_35966[(2)] = inst_34748);

(statearr_34834_35966[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (14))){
var inst_34770 = (state_34804[(7)]);
var inst_34772 = cljs.core.chunked_seq_QMARK_(inst_34770);
var state_34804__$1 = state_34804;
if(inst_34772){
var statearr_34835_35968 = state_34804__$1;
(statearr_34835_35968[(1)] = (17));

} else {
var statearr_34836_35969 = state_34804__$1;
(statearr_34836_35969[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (16))){
var inst_34788 = (state_34804[(2)]);
var state_34804__$1 = state_34804;
var statearr_34837_35970 = state_34804__$1;
(statearr_34837_35970[(2)] = inst_34788);

(statearr_34837_35970[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34805 === (10))){
var inst_34757 = (state_34804[(9)]);
var inst_34759 = (state_34804[(11)]);
var inst_34764 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_34757,inst_34759);
var state_34804__$1 = state_34804;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34804__$1,(13),out,inst_34764);
} else {
if((state_val_34805 === (18))){
var inst_34770 = (state_34804[(7)]);
var inst_34779 = cljs.core.first(inst_34770);
var state_34804__$1 = state_34804;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34804__$1,(20),out,inst_34779);
} else {
if((state_val_34805 === (8))){
var inst_34758 = (state_34804[(8)]);
var inst_34759 = (state_34804[(11)]);
var inst_34761 = (inst_34759 < inst_34758);
var inst_34762 = inst_34761;
var state_34804__$1 = state_34804;
if(cljs.core.truth_(inst_34762)){
var statearr_34838_35973 = state_34804__$1;
(statearr_34838_35973[(1)] = (10));

} else {
var statearr_34839_35974 = state_34804__$1;
(statearr_34839_35974[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto__))
;
return ((function (switch__33434__auto__,c__33535__auto__){
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__33435__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__33435__auto____0 = (function (){
var statearr_34840 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34840[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__33435__auto__);

(statearr_34840[(1)] = (1));

return statearr_34840;
});
var cljs$core$async$mapcat_STAR__$_state_machine__33435__auto____1 = (function (state_34804){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_34804);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e34841){if((e34841 instanceof Object)){
var ex__33438__auto__ = e34841;
var statearr_34842_35975 = state_34804;
(statearr_34842_35975[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_34804);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34841;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35977 = state_34804;
state_34804 = G__35977;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__33435__auto__ = function(state_34804){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__33435__auto____1.call(this,state_34804);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__33435__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__33435__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto__))
})();
var state__33537__auto__ = (function (){var statearr_34843 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_34843[(6)] = c__33535__auto__);

return statearr_34843;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto__))
);

return c__33535__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__34845 = arguments.length;
switch (G__34845) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3(f,in$,null);
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return out;
});

cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__34847 = arguments.length;
switch (G__34847) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3(f,out,null);
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return in$;
});

cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__34849 = arguments.length;
switch (G__34849) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2(ch,null);
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__33535__auto___35982 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto___35982,out){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto___35982,out){
return (function (state_34873){
var state_val_34874 = (state_34873[(1)]);
if((state_val_34874 === (7))){
var inst_34868 = (state_34873[(2)]);
var state_34873__$1 = state_34873;
var statearr_34875_35984 = state_34873__$1;
(statearr_34875_35984[(2)] = inst_34868);

(statearr_34875_35984[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34874 === (1))){
var inst_34850 = null;
var state_34873__$1 = (function (){var statearr_34876 = state_34873;
(statearr_34876[(7)] = inst_34850);

return statearr_34876;
})();
var statearr_34877_35988 = state_34873__$1;
(statearr_34877_35988[(2)] = null);

(statearr_34877_35988[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34874 === (4))){
var inst_34853 = (state_34873[(8)]);
var inst_34853__$1 = (state_34873[(2)]);
var inst_34854 = (inst_34853__$1 == null);
var inst_34855 = cljs.core.not(inst_34854);
var state_34873__$1 = (function (){var statearr_34878 = state_34873;
(statearr_34878[(8)] = inst_34853__$1);

return statearr_34878;
})();
if(inst_34855){
var statearr_34879_35993 = state_34873__$1;
(statearr_34879_35993[(1)] = (5));

} else {
var statearr_34880_35998 = state_34873__$1;
(statearr_34880_35998[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34874 === (6))){
var state_34873__$1 = state_34873;
var statearr_34881_36000 = state_34873__$1;
(statearr_34881_36000[(2)] = null);

(statearr_34881_36000[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34874 === (3))){
var inst_34870 = (state_34873[(2)]);
var inst_34871 = cljs.core.async.close_BANG_(out);
var state_34873__$1 = (function (){var statearr_34882 = state_34873;
(statearr_34882[(9)] = inst_34870);

return statearr_34882;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_34873__$1,inst_34871);
} else {
if((state_val_34874 === (2))){
var state_34873__$1 = state_34873;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34873__$1,(4),ch);
} else {
if((state_val_34874 === (11))){
var inst_34853 = (state_34873[(8)]);
var inst_34862 = (state_34873[(2)]);
var inst_34850 = inst_34853;
var state_34873__$1 = (function (){var statearr_34883 = state_34873;
(statearr_34883[(10)] = inst_34862);

(statearr_34883[(7)] = inst_34850);

return statearr_34883;
})();
var statearr_34884_36009 = state_34873__$1;
(statearr_34884_36009[(2)] = null);

(statearr_34884_36009[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34874 === (9))){
var inst_34853 = (state_34873[(8)]);
var state_34873__$1 = state_34873;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34873__$1,(11),out,inst_34853);
} else {
if((state_val_34874 === (5))){
var inst_34853 = (state_34873[(8)]);
var inst_34850 = (state_34873[(7)]);
var inst_34857 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_34853,inst_34850);
var state_34873__$1 = state_34873;
if(inst_34857){
var statearr_34886_36010 = state_34873__$1;
(statearr_34886_36010[(1)] = (8));

} else {
var statearr_34887_36011 = state_34873__$1;
(statearr_34887_36011[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34874 === (10))){
var inst_34865 = (state_34873[(2)]);
var state_34873__$1 = state_34873;
var statearr_34888_36012 = state_34873__$1;
(statearr_34888_36012[(2)] = inst_34865);

(statearr_34888_36012[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34874 === (8))){
var inst_34850 = (state_34873[(7)]);
var tmp34885 = inst_34850;
var inst_34850__$1 = tmp34885;
var state_34873__$1 = (function (){var statearr_34889 = state_34873;
(statearr_34889[(7)] = inst_34850__$1);

return statearr_34889;
})();
var statearr_34890_36013 = state_34873__$1;
(statearr_34890_36013[(2)] = null);

(statearr_34890_36013[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto___35982,out))
;
return ((function (switch__33434__auto__,c__33535__auto___35982,out){
return (function() {
var cljs$core$async$state_machine__33435__auto__ = null;
var cljs$core$async$state_machine__33435__auto____0 = (function (){
var statearr_34891 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_34891[(0)] = cljs$core$async$state_machine__33435__auto__);

(statearr_34891[(1)] = (1));

return statearr_34891;
});
var cljs$core$async$state_machine__33435__auto____1 = (function (state_34873){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_34873);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e34892){if((e34892 instanceof Object)){
var ex__33438__auto__ = e34892;
var statearr_34893_36023 = state_34873;
(statearr_34893_36023[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_34873);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34892;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36029 = state_34873;
state_34873 = G__36029;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$state_machine__33435__auto__ = function(state_34873){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__33435__auto____1.call(this,state_34873);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__33435__auto____0;
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__33435__auto____1;
return cljs$core$async$state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto___35982,out))
})();
var state__33537__auto__ = (function (){var statearr_34894 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_34894[(6)] = c__33535__auto___35982);

return statearr_34894;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto___35982,out))
);


return out;
});

cljs.core.async.unique.cljs$lang$maxFixedArity = 2;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__34896 = arguments.length;
switch (G__34896) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3(n,ch,null);
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__33535__auto___36042 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto___36042,out){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto___36042,out){
return (function (state_34934){
var state_val_34935 = (state_34934[(1)]);
if((state_val_34935 === (7))){
var inst_34930 = (state_34934[(2)]);
var state_34934__$1 = state_34934;
var statearr_34936_36046 = state_34934__$1;
(statearr_34936_36046[(2)] = inst_34930);

(statearr_34936_36046[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34935 === (1))){
var inst_34897 = (new Array(n));
var inst_34898 = inst_34897;
var inst_34899 = (0);
var state_34934__$1 = (function (){var statearr_34937 = state_34934;
(statearr_34937[(7)] = inst_34898);

(statearr_34937[(8)] = inst_34899);

return statearr_34937;
})();
var statearr_34938_36054 = state_34934__$1;
(statearr_34938_36054[(2)] = null);

(statearr_34938_36054[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34935 === (4))){
var inst_34902 = (state_34934[(9)]);
var inst_34902__$1 = (state_34934[(2)]);
var inst_34903 = (inst_34902__$1 == null);
var inst_34904 = cljs.core.not(inst_34903);
var state_34934__$1 = (function (){var statearr_34939 = state_34934;
(statearr_34939[(9)] = inst_34902__$1);

return statearr_34939;
})();
if(inst_34904){
var statearr_34940_36055 = state_34934__$1;
(statearr_34940_36055[(1)] = (5));

} else {
var statearr_34941_36057 = state_34934__$1;
(statearr_34941_36057[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34935 === (15))){
var inst_34924 = (state_34934[(2)]);
var state_34934__$1 = state_34934;
var statearr_34942_36058 = state_34934__$1;
(statearr_34942_36058[(2)] = inst_34924);

(statearr_34942_36058[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34935 === (13))){
var state_34934__$1 = state_34934;
var statearr_34943_36059 = state_34934__$1;
(statearr_34943_36059[(2)] = null);

(statearr_34943_36059[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34935 === (6))){
var inst_34899 = (state_34934[(8)]);
var inst_34920 = (inst_34899 > (0));
var state_34934__$1 = state_34934;
if(cljs.core.truth_(inst_34920)){
var statearr_34944_36061 = state_34934__$1;
(statearr_34944_36061[(1)] = (12));

} else {
var statearr_34945_36062 = state_34934__$1;
(statearr_34945_36062[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34935 === (3))){
var inst_34932 = (state_34934[(2)]);
var state_34934__$1 = state_34934;
return cljs.core.async.impl.ioc_helpers.return_chan(state_34934__$1,inst_34932);
} else {
if((state_val_34935 === (12))){
var inst_34898 = (state_34934[(7)]);
var inst_34922 = cljs.core.vec(inst_34898);
var state_34934__$1 = state_34934;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34934__$1,(15),out,inst_34922);
} else {
if((state_val_34935 === (2))){
var state_34934__$1 = state_34934;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34934__$1,(4),ch);
} else {
if((state_val_34935 === (11))){
var inst_34914 = (state_34934[(2)]);
var inst_34915 = (new Array(n));
var inst_34898 = inst_34915;
var inst_34899 = (0);
var state_34934__$1 = (function (){var statearr_34946 = state_34934;
(statearr_34946[(7)] = inst_34898);

(statearr_34946[(8)] = inst_34899);

(statearr_34946[(10)] = inst_34914);

return statearr_34946;
})();
var statearr_34947_36066 = state_34934__$1;
(statearr_34947_36066[(2)] = null);

(statearr_34947_36066[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34935 === (9))){
var inst_34898 = (state_34934[(7)]);
var inst_34912 = cljs.core.vec(inst_34898);
var state_34934__$1 = state_34934;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34934__$1,(11),out,inst_34912);
} else {
if((state_val_34935 === (5))){
var inst_34898 = (state_34934[(7)]);
var inst_34899 = (state_34934[(8)]);
var inst_34907 = (state_34934[(11)]);
var inst_34902 = (state_34934[(9)]);
var inst_34906 = (inst_34898[inst_34899] = inst_34902);
var inst_34907__$1 = (inst_34899 + (1));
var inst_34908 = (inst_34907__$1 < n);
var state_34934__$1 = (function (){var statearr_34948 = state_34934;
(statearr_34948[(12)] = inst_34906);

(statearr_34948[(11)] = inst_34907__$1);

return statearr_34948;
})();
if(cljs.core.truth_(inst_34908)){
var statearr_34949_36069 = state_34934__$1;
(statearr_34949_36069[(1)] = (8));

} else {
var statearr_34950_36070 = state_34934__$1;
(statearr_34950_36070[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34935 === (14))){
var inst_34927 = (state_34934[(2)]);
var inst_34928 = cljs.core.async.close_BANG_(out);
var state_34934__$1 = (function (){var statearr_34952 = state_34934;
(statearr_34952[(13)] = inst_34927);

return statearr_34952;
})();
var statearr_34953_36071 = state_34934__$1;
(statearr_34953_36071[(2)] = inst_34928);

(statearr_34953_36071[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34935 === (10))){
var inst_34918 = (state_34934[(2)]);
var state_34934__$1 = state_34934;
var statearr_34954_36072 = state_34934__$1;
(statearr_34954_36072[(2)] = inst_34918);

(statearr_34954_36072[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34935 === (8))){
var inst_34898 = (state_34934[(7)]);
var inst_34907 = (state_34934[(11)]);
var tmp34951 = inst_34898;
var inst_34898__$1 = tmp34951;
var inst_34899 = inst_34907;
var state_34934__$1 = (function (){var statearr_34955 = state_34934;
(statearr_34955[(7)] = inst_34898__$1);

(statearr_34955[(8)] = inst_34899);

return statearr_34955;
})();
var statearr_34956_36073 = state_34934__$1;
(statearr_34956_36073[(2)] = null);

(statearr_34956_36073[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto___36042,out))
;
return ((function (switch__33434__auto__,c__33535__auto___36042,out){
return (function() {
var cljs$core$async$state_machine__33435__auto__ = null;
var cljs$core$async$state_machine__33435__auto____0 = (function (){
var statearr_34957 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34957[(0)] = cljs$core$async$state_machine__33435__auto__);

(statearr_34957[(1)] = (1));

return statearr_34957;
});
var cljs$core$async$state_machine__33435__auto____1 = (function (state_34934){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_34934);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e34958){if((e34958 instanceof Object)){
var ex__33438__auto__ = e34958;
var statearr_34959_36076 = state_34934;
(statearr_34959_36076[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_34934);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34958;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36077 = state_34934;
state_34934 = G__36077;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$state_machine__33435__auto__ = function(state_34934){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__33435__auto____1.call(this,state_34934);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__33435__auto____0;
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__33435__auto____1;
return cljs$core$async$state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto___36042,out))
})();
var state__33537__auto__ = (function (){var statearr_34960 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_34960[(6)] = c__33535__auto___36042);

return statearr_34960;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto___36042,out))
);


return out;
});

cljs.core.async.partition.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__34962 = arguments.length;
switch (G__34962) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3(f,ch,null);
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__33535__auto___36081 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (c__33535__auto___36081,out){
return (function (){
var f__33536__auto__ = (function (){var switch__33434__auto__ = ((function (c__33535__auto___36081,out){
return (function (state_35004){
var state_val_35005 = (state_35004[(1)]);
if((state_val_35005 === (7))){
var inst_35000 = (state_35004[(2)]);
var state_35004__$1 = state_35004;
var statearr_35006_36086 = state_35004__$1;
(statearr_35006_36086[(2)] = inst_35000);

(statearr_35006_36086[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35005 === (1))){
var inst_34963 = [];
var inst_34964 = inst_34963;
var inst_34965 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_35004__$1 = (function (){var statearr_35007 = state_35004;
(statearr_35007[(7)] = inst_34964);

(statearr_35007[(8)] = inst_34965);

return statearr_35007;
})();
var statearr_35008_36089 = state_35004__$1;
(statearr_35008_36089[(2)] = null);

(statearr_35008_36089[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35005 === (4))){
var inst_34968 = (state_35004[(9)]);
var inst_34968__$1 = (state_35004[(2)]);
var inst_34969 = (inst_34968__$1 == null);
var inst_34970 = cljs.core.not(inst_34969);
var state_35004__$1 = (function (){var statearr_35009 = state_35004;
(statearr_35009[(9)] = inst_34968__$1);

return statearr_35009;
})();
if(inst_34970){
var statearr_35010_36096 = state_35004__$1;
(statearr_35010_36096[(1)] = (5));

} else {
var statearr_35011_36097 = state_35004__$1;
(statearr_35011_36097[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35005 === (15))){
var inst_34994 = (state_35004[(2)]);
var state_35004__$1 = state_35004;
var statearr_35012_36106 = state_35004__$1;
(statearr_35012_36106[(2)] = inst_34994);

(statearr_35012_36106[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35005 === (13))){
var state_35004__$1 = state_35004;
var statearr_35013_36108 = state_35004__$1;
(statearr_35013_36108[(2)] = null);

(statearr_35013_36108[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35005 === (6))){
var inst_34964 = (state_35004[(7)]);
var inst_34989 = inst_34964.length;
var inst_34990 = (inst_34989 > (0));
var state_35004__$1 = state_35004;
if(cljs.core.truth_(inst_34990)){
var statearr_35014_36115 = state_35004__$1;
(statearr_35014_36115[(1)] = (12));

} else {
var statearr_35015_36117 = state_35004__$1;
(statearr_35015_36117[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35005 === (3))){
var inst_35002 = (state_35004[(2)]);
var state_35004__$1 = state_35004;
return cljs.core.async.impl.ioc_helpers.return_chan(state_35004__$1,inst_35002);
} else {
if((state_val_35005 === (12))){
var inst_34964 = (state_35004[(7)]);
var inst_34992 = cljs.core.vec(inst_34964);
var state_35004__$1 = state_35004;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_35004__$1,(15),out,inst_34992);
} else {
if((state_val_35005 === (2))){
var state_35004__$1 = state_35004;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_35004__$1,(4),ch);
} else {
if((state_val_35005 === (11))){
var inst_34972 = (state_35004[(10)]);
var inst_34968 = (state_35004[(9)]);
var inst_34982 = (state_35004[(2)]);
var inst_34983 = [];
var inst_34984 = inst_34983.push(inst_34968);
var inst_34964 = inst_34983;
var inst_34965 = inst_34972;
var state_35004__$1 = (function (){var statearr_35016 = state_35004;
(statearr_35016[(7)] = inst_34964);

(statearr_35016[(8)] = inst_34965);

(statearr_35016[(11)] = inst_34982);

(statearr_35016[(12)] = inst_34984);

return statearr_35016;
})();
var statearr_35017_36141 = state_35004__$1;
(statearr_35017_36141[(2)] = null);

(statearr_35017_36141[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35005 === (9))){
var inst_34964 = (state_35004[(7)]);
var inst_34980 = cljs.core.vec(inst_34964);
var state_35004__$1 = state_35004;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_35004__$1,(11),out,inst_34980);
} else {
if((state_val_35005 === (5))){
var inst_34972 = (state_35004[(10)]);
var inst_34965 = (state_35004[(8)]);
var inst_34968 = (state_35004[(9)]);
var inst_34972__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_34968) : f.call(null,inst_34968));
var inst_34973 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_34972__$1,inst_34965);
var inst_34974 = cljs.core.keyword_identical_QMARK_(inst_34965,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_34975 = ((inst_34973) || (inst_34974));
var state_35004__$1 = (function (){var statearr_35018 = state_35004;
(statearr_35018[(10)] = inst_34972__$1);

return statearr_35018;
})();
if(cljs.core.truth_(inst_34975)){
var statearr_35019_36156 = state_35004__$1;
(statearr_35019_36156[(1)] = (8));

} else {
var statearr_35020_36157 = state_35004__$1;
(statearr_35020_36157[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35005 === (14))){
var inst_34997 = (state_35004[(2)]);
var inst_34998 = cljs.core.async.close_BANG_(out);
var state_35004__$1 = (function (){var statearr_35022 = state_35004;
(statearr_35022[(13)] = inst_34997);

return statearr_35022;
})();
var statearr_35023_36165 = state_35004__$1;
(statearr_35023_36165[(2)] = inst_34998);

(statearr_35023_36165[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35005 === (10))){
var inst_34987 = (state_35004[(2)]);
var state_35004__$1 = state_35004;
var statearr_35024_36169 = state_35004__$1;
(statearr_35024_36169[(2)] = inst_34987);

(statearr_35024_36169[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35005 === (8))){
var inst_34972 = (state_35004[(10)]);
var inst_34964 = (state_35004[(7)]);
var inst_34968 = (state_35004[(9)]);
var inst_34977 = inst_34964.push(inst_34968);
var tmp35021 = inst_34964;
var inst_34964__$1 = tmp35021;
var inst_34965 = inst_34972;
var state_35004__$1 = (function (){var statearr_35025 = state_35004;
(statearr_35025[(7)] = inst_34964__$1);

(statearr_35025[(14)] = inst_34977);

(statearr_35025[(8)] = inst_34965);

return statearr_35025;
})();
var statearr_35026_36185 = state_35004__$1;
(statearr_35026_36185[(2)] = null);

(statearr_35026_36185[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__33535__auto___36081,out))
;
return ((function (switch__33434__auto__,c__33535__auto___36081,out){
return (function() {
var cljs$core$async$state_machine__33435__auto__ = null;
var cljs$core$async$state_machine__33435__auto____0 = (function (){
var statearr_35027 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_35027[(0)] = cljs$core$async$state_machine__33435__auto__);

(statearr_35027[(1)] = (1));

return statearr_35027;
});
var cljs$core$async$state_machine__33435__auto____1 = (function (state_35004){
while(true){
var ret_value__33436__auto__ = (function (){try{while(true){
var result__33437__auto__ = switch__33434__auto__(state_35004);
if(cljs.core.keyword_identical_QMARK_(result__33437__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__33437__auto__;
}
break;
}
}catch (e35028){if((e35028 instanceof Object)){
var ex__33438__auto__ = e35028;
var statearr_35029_36190 = state_35004;
(statearr_35029_36190[(5)] = ex__33438__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_35004);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e35028;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__33436__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36191 = state_35004;
state_35004 = G__36191;
continue;
} else {
return ret_value__33436__auto__;
}
break;
}
});
cljs$core$async$state_machine__33435__auto__ = function(state_35004){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__33435__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__33435__auto____1.call(this,state_35004);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__33435__auto____0;
cljs$core$async$state_machine__33435__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__33435__auto____1;
return cljs$core$async$state_machine__33435__auto__;
})()
;})(switch__33434__auto__,c__33535__auto___36081,out))
})();
var state__33537__auto__ = (function (){var statearr_35030 = (f__33536__auto__.cljs$core$IFn$_invoke$arity$0 ? f__33536__auto__.cljs$core$IFn$_invoke$arity$0() : f__33536__auto__.call(null));
(statearr_35030[(6)] = c__33535__auto___36081);

return statearr_35030;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__33537__auto__);
});})(c__33535__auto___36081,out))
);


return out;
});

cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3;


//# sourceMappingURL=cljs.core.async.js.map
