goog.provide('dnd5_project.core');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('dnd5_project.data');
goog.require('clojure.string');
goog.require('goog.async.Debouncer');
if((typeof dnd5_project !== 'undefined') && (typeof dnd5_project.core !== 'undefined') && (typeof dnd5_project.core.app_state !== 'undefined')){
} else {
dnd5_project.core.app_state = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"filter-by","filter-by",-1855851983),"",new cljs.core.Keyword(null,"sort-by","sort-by",-322599303),cljs.core.PersistentArrayMap.EMPTY], null));
}
dnd5_project.core.filter_field = (function dnd5_project$core$filter_field(data){
var update_BANG_ = (function (p1__27412_SHARP_){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(dnd5_project.core.app_state,cljs.core.assoc,new cljs.core.Keyword(null,"filter-by","filter-by",-1855851983),p1__27412_SHARP_.target.value);
});
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"font-size","font-size",-1847940346),"20px",new cljs.core.Keyword(null,"border","border",1444987323),"3px brown solid",new cljs.core.Keyword(null,"padding","padding",1660304693),"10px",new cljs.core.Keyword(null,"margin","margin",-995903681),"5px",new cljs.core.Keyword(null,"border-radius","border-radius",419594011),"3px"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label","label",1718410804),"Search: "], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"on-blur","on-blur",814300747),update_BANG_,new cljs.core.Keyword(null,"on-key-down","on-key-down",-1374733765),((function (update_BANG_){
return (function (e){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(e.key,"Enter")){
return update_BANG_(e);
} else {
return null;
}
});})(update_BANG_))
], null)], null)], null);
});
dnd5_project.core.filter_it = (function dnd5_project$core$filter_it(filter_str,data){
var regex_str = ["(?i).*",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.interpose.cljs$core$IFn$_invoke$arity$2(".*",clojure.string.split.cljs$core$IFn$_invoke$arity$2(filter_str,/\s+/)))),".*"].join('');
var _ = console.log(regex_str);
var regex = cljs.core.re_pattern(regex_str);
return cljs.core.sort_by.cljs$core$IFn$_invoke$arity$2(((function (regex_str,_,regex){
return (function (p1__27413_SHARP_){
return clojure.string.join.cljs$core$IFn$_invoke$arity$2(cljs.core.juxt.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"title","title",636505583)),p1__27413_SHARP_);
});})(regex_str,_,regex))
,cljs.core.filter.cljs$core$IFn$_invoke$arity$2(((function (regex_str,_,regex){
return (function (datum){
return cljs.core.re_matches(regex,new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(datum));
});})(regex_str,_,regex))
,data));
});
dnd5_project.core.show = (function dnd5_project$core$show(s){
if(cljs.core.truth_(s)){
return s;
} else {
return "None";
}
});
dnd5_project.core.show_cost = (function dnd5_project$core$show_cost(p__27414){
var map__27415 = p__27414;
var map__27415__$1 = (((((!((map__27415 == null))))?(((((map__27415.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__27415.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__27415):map__27415);
var quantity = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27415__$1,new cljs.core.Keyword(null,"quantity","quantity",-1929050694));
var unit = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27415__$1,new cljs.core.Keyword(null,"unit","unit",375175175));
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(quantity)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(unit)].join('');
});
dnd5_project.core.show_range = (function dnd5_project$core$show_range(p__27417){
var map__27418 = p__27417;
var map__27418__$1 = (((((!((map__27418 == null))))?(((((map__27418.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__27418.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__27418):map__27418);
var normal = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27418__$1,new cljs.core.Keyword(null,"normal","normal",-1519123858));
var long$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27418__$1,new cljs.core.Keyword(null,"long","long",-171452093));
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(dnd5_project.core.show(normal))," (normal), ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(dnd5_project.core.show(long$))," (long)"].join('');
});
dnd5_project.core.show_damage = (function dnd5_project$core$show_damage(p__27420){
var map__27421 = p__27420;
var map__27421__$1 = (((((!((map__27421 == null))))?(((((map__27421.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__27421.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__27421):map__27421);
var dice_count = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27421__$1,new cljs.core.Keyword(null,"dice_count","dice_count",-815475553));
var dice_value = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27421__$1,new cljs.core.Keyword(null,"dice_value","dice_value",1780901890));
var damage_type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27421__$1,new cljs.core.Keyword(null,"damage_type","damage_type",2086892106));
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(dice_count),"d",cljs.core.str.cljs$core$IFn$_invoke$arity$1(dice_value)," (",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(damage_type)),")"].join('');
});
dnd5_project.core.show_prop = (function dnd5_project$core$show_prop(title,value){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(clojure.string.capitalize(title)),": "].join(''),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),(((value == null))?"None":((typeof value === 'string')?value:(cljs.core.truth_((function (){var and__4120__auto__ = cljs.core.map_QMARK_(value);
if(and__4120__auto__){
var G__27426 = cljs.core.keys(value);
var fexpr__27425 = cljs.core.PersistentHashSet.createAsIfByAssoc([new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"quantity","quantity",-1929050694),new cljs.core.Keyword(null,"unit","unit",375175175)], null)]);
return (fexpr__27425.cljs$core$IFn$_invoke$arity$1 ? fexpr__27425.cljs$core$IFn$_invoke$arity$1(G__27426) : fexpr__27425.call(null,G__27426));
} else {
return and__4120__auto__;
}
})())?dnd5_project.core.show_cost(value):(cljs.core.truth_((function (){var and__4120__auto__ = cljs.core.map_QMARK_(value);
if(and__4120__auto__){
var G__27430 = cljs.core.keys(value);
var fexpr__27429 = cljs.core.PersistentHashSet.createAsIfByAssoc([new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"normal","normal",-1519123858),new cljs.core.Keyword(null,"long","long",-171452093)], null)]);
return (fexpr__27429.cljs$core$IFn$_invoke$arity$1 ? fexpr__27429.cljs$core$IFn$_invoke$arity$1(G__27430) : fexpr__27429.call(null,G__27430));
} else {
return and__4120__auto__;
}
})())?dnd5_project.core.show_range(value):(cljs.core.truth_((function (){var and__4120__auto__ = cljs.core.map_QMARK_(value);
if(and__4120__auto__){
var G__27434 = cljs.core.keys(value);
var fexpr__27433 = cljs.core.PersistentHashSet.createAsIfByAssoc([new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dice_count","dice_count",-815475553),new cljs.core.Keyword(null,"dice_value","dice_value",1780901890)], null)]);
return (fexpr__27433.cljs$core$IFn$_invoke$arity$1 ? fexpr__27433.cljs$core$IFn$_invoke$arity$1(G__27434) : fexpr__27433.call(null,G__27434));
} else {
return and__4120__auto__;
}
})())?dnd5_project.core.show_damage(value):((((cljs.core.coll_QMARK_(value)) && (cljs.core.map_QMARK_(cljs.core.first(value)))))?clojure.string.join.cljs$core$IFn$_invoke$arity$2(", ",cljs.core.map.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"name","name",1843675177),value)):((cljs.core.coll_QMARK_(value))?clojure.string.join.cljs$core$IFn$_invoke$arity$2(", ",value):cljs.core.str.cljs$core$IFn$_invoke$arity$1(value)
)))))))], null)], null);
});
if((typeof dnd5_project !== 'undefined') && (typeof dnd5_project.core !== 'undefined') && (typeof dnd5_project.core.show_datum !== 'undefined')){
} else {
dnd5_project.core.show_datum = (function (){var method_table__4613__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__4614__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var method_cache__4615__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__4616__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__4617__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),(function (){var fexpr__27435 = cljs.core.get_global_hierarchy;
return (fexpr__27435.cljs$core$IFn$_invoke$arity$0 ? fexpr__27435.cljs$core$IFn$_invoke$arity$0() : fexpr__27435.call(null));
})());
return (new cljs.core.MultiFn(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2("dnd5_project.core","show-datum"),new cljs.core.Keyword("dnd-encyclopedia","model","dnd-encyclopedia/model",1170045188),new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__4617__auto__,method_table__4613__auto__,prefer_table__4614__auto__,method_cache__4615__auto__,cached_hierarchy__4616__auto__));
})();
}
dnd5_project.core.show_datum.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword(null,"spells","spells",377137164),(function (p__27436){
var map__27437 = p__27436;
var map__27437__$1 = (((((!((map__27437 == null))))?(((((map__27437.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__27437.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__27437):map__27437);
var info = map__27437__$1;
var classes = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27437__$1,new cljs.core.Keyword(null,"classes","classes",2037804510));
var range = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27437__$1,new cljs.core.Keyword(null,"range","range",1639692286));
var casting_time = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27437__$1,new cljs.core.Keyword(null,"casting_time","casting_time",-1512467550));
var desc = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27437__$1,new cljs.core.Keyword(null,"desc","desc",2093485764));
var school = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27437__$1,new cljs.core.Keyword(null,"school","school",578332550));
var name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27437__$1,new cljs.core.Keyword(null,"name","name",1843675177));
var concentration = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27437__$1,new cljs.core.Keyword(null,"concentration","concentration",1539606986));
var duration = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27437__$1,new cljs.core.Keyword(null,"duration","duration",1444101068));
var page = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27437__$1,new cljs.core.Keyword(null,"page","page",849072397));
var level = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27437__$1,new cljs.core.Keyword(null,"level","level",1290497552));
var components = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27437__$1,new cljs.core.Keyword(null,"components","components",-1073188942));
var ritual = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27437__$1,new cljs.core.Keyword(null,"ritual","ritual",1818141075));
return new cljs.core.PersistentVector(null, 15, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"border","border",1444987323),"2px solid black",new cljs.core.Keyword(null,"padding","padding",1660304693),"5px",new cljs.core.Keyword(null,"margin","margin",-995903681),"4px",new cljs.core.Keyword(null,"width","width",-384071477),"400px",new cljs.core.Keyword(null,"border-radius","border-radius",419594011),"5px"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),name,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"font-size","font-size",-1847940346),"10px",new cljs.core.Keyword(null,"float","float",-1732389368),"right"], null)], null),new cljs.core.Keyword("dnd-encyclopedia","model","dnd-encyclopedia/model",1170045188).cljs$core$IFn$_invoke$arity$1(info)], null)], null)], null),cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632)], null),desc),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"br","br",934104792)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),"School: ",new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(school)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"Casting Time",casting_time], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"Concentration",concentration], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"Duration",duration], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"Page",page], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"Level",cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([level], 0))], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"Components",components], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"Ritual",ritual], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"Classes",classes], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"Range",range], null)], null);
}));
dnd5_project.core.show_datum.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword(null,"features","features",-1146962336),(function (p__27439){
var map__27440 = p__27439;
var map__27440__$1 = (((((!((map__27440 == null))))?(((((map__27440.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__27440.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__27440):map__27440);
var info = map__27440__$1;
var name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27440__$1,new cljs.core.Keyword(null,"name","name",1843675177));
var level = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27440__$1,new cljs.core.Keyword(null,"level","level",1290497552));
var desc = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27440__$1,new cljs.core.Keyword(null,"desc","desc",2093485764));
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27440__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var model = new cljs.core.Keyword("dnd-encyclopedia","model","dnd-encyclopedia/model",1170045188).cljs$core$IFn$_invoke$arity$1(info);
return new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"border","border",1444987323),"2px solid black",new cljs.core.Keyword(null,"padding","padding",1660304693),"5px",new cljs.core.Keyword(null,"margin","margin",-995903681),"4px",new cljs.core.Keyword(null,"max-width","max-width",-1939924051),"400px",new cljs.core.Keyword(null,"border-radius","border-radius",419594011),"5px"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),name,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"font-size","font-size",-1847940346),"10px",new cljs.core.Keyword(null,"float","float",-1732389368),"right"], null)], null),model], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),clojure.string.join.cljs$core$IFn$_invoke$arity$2("\n",desc)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"br","br",934104792)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"Class",new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(class$)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"Level",level], null)], null);
}));
dnd5_project.core.show_datum.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword(null,"equipment","equipment",790435115),(function (p__27442){
var map__27443 = p__27442;
var map__27443__$1 = (((((!((map__27443 == null))))?(((((map__27443.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__27443.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__27443):map__27443);
var info = map__27443__$1;
var cost = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27443__$1,new cljs.core.Keyword(null,"cost","cost",-1094861735));
var category_range = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27443__$1,new cljs.core.Keyword(null,"category_range","category_range",-1139690115));
var range = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27443__$1,new cljs.core.Keyword(null,"range","range",1639692286));
var properties = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27443__$1,new cljs.core.Keyword(null,"properties","properties",685819552));
var weapon_category = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27443__$1,new cljs.core.Keyword(null,"weapon_category","weapon_category",785190338));
var equipment_category = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27443__$1,new cljs.core.Keyword(null,"equipment_category","equipment_category",-107383033));
var name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27443__$1,new cljs.core.Keyword(null,"name","name",1843675177));
var weapon_range = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27443__$1,new cljs.core.Keyword(null,"weapon_range","weapon_range",-736483031));
var damage = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27443__$1,new cljs.core.Keyword(null,"damage","damage",970520018));
var weight = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27443__$1,new cljs.core.Keyword(null,"weight","weight",-1262796205));
var model = new cljs.core.Keyword("dnd-encyclopedia","model","dnd-encyclopedia/model",1170045188).cljs$core$IFn$_invoke$arity$1(info);
return new cljs.core.PersistentVector(null, 13, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"border","border",1444987323),"2px solid black",new cljs.core.Keyword(null,"padding","padding",1660304693),"5px",new cljs.core.Keyword(null,"margin","margin",-995903681),"4px",new cljs.core.Keyword(null,"border-radius","border-radius",419594011),"5px"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),name,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"font-size","font-size",-1847940346),"10px",new cljs.core.Keyword(null,"float","float",-1732389368),"right"], null)], null),model], null)], null)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"br","br",934104792)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"Properties: ",properties], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"weapon_category",weapon_category], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"weapon_range",weapon_range], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"damage",dnd5_project.core.show_damage(damage)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"weight",weight], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"cost",cost], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"category_range",category_range], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"range",range], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.show_prop,"equipment_category",equipment_category], null)], null);
}));
dnd5_project.core.show_datum.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword(null,"damage-types","damage-types",167065664),(function (p__27445){
var map__27446 = p__27445;
var map__27446__$1 = (((((!((map__27446 == null))))?(((((map__27446.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__27446.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__27446):map__27446);
var info = map__27446__$1;
var name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27446__$1,new cljs.core.Keyword(null,"name","name",1843675177));
var desc = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27446__$1,new cljs.core.Keyword(null,"desc","desc",2093485764));
var model = new cljs.core.Keyword("dnd-encyclopedia","model","dnd-encyclopedia/model",1170045188).cljs$core$IFn$_invoke$arity$1(info);
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"border","border",1444987323),"2px solid black",new cljs.core.Keyword(null,"padding","padding",1660304693),"5px",new cljs.core.Keyword(null,"margin","margin",-995903681),"4px",new cljs.core.Keyword(null,"border-radius","border-radius",419594011),"5px"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),name,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"font-size","font-size",-1847940346),"10px",new cljs.core.Keyword(null,"float","float",-1732389368),"right"], null)], null),model], null)], null)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"br","br",934104792)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),clojure.string.join.cljs$core$IFn$_invoke$arity$1(desc)], null)], null);
}));
dnd5_project.core.show_datum.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword(null,"default","default",-1987822328),(function (p__27448){
var map__27449 = p__27448;
var map__27449__$1 = (((((!((map__27449 == null))))?(((((map__27449.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__27449.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__27449):map__27449);
var info = map__27449__$1;
var name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__27449__$1,new cljs.core.Keyword(null,"name","name",1843675177));
var model = new cljs.core.Keyword("dnd-encyclopedia","model","dnd-encyclopedia/model",1170045188).cljs$core$IFn$_invoke$arity$1(info);
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"border","border",1444987323),"2px solid black",new cljs.core.Keyword(null,"padding","padding",1660304693),"5px",new cljs.core.Keyword(null,"margin","margin",-995903681),"4px",new cljs.core.Keyword(null,"border-radius","border-radius",419594011),"5px"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),name,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"font-size","font-size",-1847940346),"10px",new cljs.core.Keyword(null,"float","float",-1732389368),"right"], null)], null),model], null)], null)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"br","br",934104792)], null),cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632)], null),(function (){var iter__4523__auto__ = ((function (model,map__27449,map__27449__$1,info,name){
return (function dnd5_project$core$iter__27451(s__27452){
return (new cljs.core.LazySeq(null,((function (model,map__27449,map__27449__$1,info,name){
return (function (){
var s__27452__$1 = s__27452;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__27452__$1);
if(temp__5735__auto__){
var s__27452__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__27452__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__27452__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__27454 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__27453 = (0);
while(true){
if((i__27453 < size__4522__auto__)){
var vec__27455 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__27453);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__27455,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__27455,(1),null);
cljs.core.chunk_append(b__27454,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),cljs.core.str.cljs$core$IFn$_invoke$arity$1(k)," - ",cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([v], 0))], null));

var G__27463 = (i__27453 + (1));
i__27453 = G__27463;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__27454),dnd5_project$core$iter__27451(cljs.core.chunk_rest(s__27452__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__27454),null);
}
} else {
var vec__27458 = cljs.core.first(s__27452__$2);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__27458,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__27458,(1),null);
return cljs.core.cons(new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),cljs.core.str.cljs$core$IFn$_invoke$arity$1(k)," - ",cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([v], 0))], null),dnd5_project$core$iter__27451(cljs.core.rest(s__27452__$2)));
}
} else {
return null;
}
break;
}
});})(model,map__27449,map__27449__$1,info,name))
,null,null));
});})(model,map__27449,map__27449__$1,info,name))
;
return iter__4523__auto__(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(info,new cljs.core.Keyword(null,"name","name",1843675177),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"url","url",276297046),new cljs.core.Keyword(null,"_id","_id",-789960287),new cljs.core.Keyword("dnd-encyclopedia","model","dnd-encyclopedia/model",1170045188),new cljs.core.Keyword(null,"index","index",-1531685915)], 0)));
})())], null);
}));
dnd5_project.core.hello_world = (function dnd5_project$core$hello_world(){
var shown_data = dnd5_project.core.filter_it(new cljs.core.Keyword(null,"filter-by","filter-by",-1855851983).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(dnd5_project.core.app_state)),dnd5_project.data.data);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.filter_field], null),cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"display","display",242065432),"flex",new cljs.core.Keyword(null,"flex-wrap","flex-wrap",455413707),"wrap"], null)], null)], null),cljs.core.map.cljs$core$IFn$_invoke$arity$2(dnd5_project.core.show_datum,shown_data))], null);
});
dnd5_project.core.start = (function dnd5_project$core$start(){
var G__27461 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [dnd5_project.core.hello_world], null);
var G__27462 = document.getElementById("app");
return (reagent.core.render_component.cljs$core$IFn$_invoke$arity$2 ? reagent.core.render_component.cljs$core$IFn$_invoke$arity$2(G__27461,G__27462) : reagent.core.render_component.call(null,G__27461,G__27462));
});
dnd5_project.core.init = (function dnd5_project$core$init(){
return dnd5_project.core.start();
});
goog.exportSymbol('dnd5_project.core.init', dnd5_project.core.init);
dnd5_project.core.stop = (function dnd5_project$core$stop(){
return console.log("stop");
});

//# sourceMappingURL=dnd5_project.core.js.map
