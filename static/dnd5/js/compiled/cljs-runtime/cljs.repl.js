goog.provide('cljs.repl');
goog.require('cljs.core');
goog.require('cljs.spec.alpha');
goog.require('goog.string');
goog.require('goog.string.format');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__31911){
var map__31912 = p__31911;
var map__31912__$1 = (((((!((map__31912 == null))))?(((((map__31912.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31912.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31912):map__31912);
var m = map__31912__$1;
var n = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31912__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31912__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["-------------------------"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (){var or__4131__auto__ = new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return [(function (){var temp__5735__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5735__auto__)){
var ns = temp__5735__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})(),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('');
}
})()], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Protocol"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__31914_32036 = cljs.core.seq(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__31915_32037 = null;
var count__31916_32038 = (0);
var i__31917_32039 = (0);
while(true){
if((i__31917_32039 < count__31916_32038)){
var f_32041 = chunk__31915_32037.cljs$core$IIndexed$_nth$arity$2(null,i__31917_32039);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_32041], 0));


var G__32042 = seq__31914_32036;
var G__32043 = chunk__31915_32037;
var G__32044 = count__31916_32038;
var G__32045 = (i__31917_32039 + (1));
seq__31914_32036 = G__32042;
chunk__31915_32037 = G__32043;
count__31916_32038 = G__32044;
i__31917_32039 = G__32045;
continue;
} else {
var temp__5735__auto___32046 = cljs.core.seq(seq__31914_32036);
if(temp__5735__auto___32046){
var seq__31914_32047__$1 = temp__5735__auto___32046;
if(cljs.core.chunked_seq_QMARK_(seq__31914_32047__$1)){
var c__4550__auto___32048 = cljs.core.chunk_first(seq__31914_32047__$1);
var G__32049 = cljs.core.chunk_rest(seq__31914_32047__$1);
var G__32050 = c__4550__auto___32048;
var G__32051 = cljs.core.count(c__4550__auto___32048);
var G__32052 = (0);
seq__31914_32036 = G__32049;
chunk__31915_32037 = G__32050;
count__31916_32038 = G__32051;
i__31917_32039 = G__32052;
continue;
} else {
var f_32053 = cljs.core.first(seq__31914_32047__$1);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_32053], 0));


var G__32054 = cljs.core.next(seq__31914_32047__$1);
var G__32055 = null;
var G__32056 = (0);
var G__32057 = (0);
seq__31914_32036 = G__32054;
chunk__31915_32037 = G__32055;
count__31916_32038 = G__32056;
i__31917_32039 = G__32057;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_32058 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__4131__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([arglists_32058], 0));
} else {
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first(arglists_32058)))?cljs.core.second(arglists_32058):arglists_32058)], 0));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Special Form"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.contains_QMARK_(m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
} else {
return null;
}
} else {
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Macro"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["REPL Special Function"], 0));
} else {
}

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__31918_32059 = cljs.core.seq(new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__31919_32060 = null;
var count__31920_32061 = (0);
var i__31921_32062 = (0);
while(true){
if((i__31921_32062 < count__31920_32061)){
var vec__31932_32064 = chunk__31919_32060.cljs$core$IIndexed$_nth$arity$2(null,i__31921_32062);
var name_32065 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31932_32064,(0),null);
var map__31935_32066 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31932_32064,(1),null);
var map__31935_32067__$1 = (((((!((map__31935_32066 == null))))?(((((map__31935_32066.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31935_32066.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31935_32066):map__31935_32066);
var doc_32068 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31935_32067__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_32069 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31935_32067__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_32065], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_32069], 0));

if(cljs.core.truth_(doc_32068)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_32068], 0));
} else {
}


var G__32076 = seq__31918_32059;
var G__32077 = chunk__31919_32060;
var G__32078 = count__31920_32061;
var G__32079 = (i__31921_32062 + (1));
seq__31918_32059 = G__32076;
chunk__31919_32060 = G__32077;
count__31920_32061 = G__32078;
i__31921_32062 = G__32079;
continue;
} else {
var temp__5735__auto___32081 = cljs.core.seq(seq__31918_32059);
if(temp__5735__auto___32081){
var seq__31918_32082__$1 = temp__5735__auto___32081;
if(cljs.core.chunked_seq_QMARK_(seq__31918_32082__$1)){
var c__4550__auto___32083 = cljs.core.chunk_first(seq__31918_32082__$1);
var G__32084 = cljs.core.chunk_rest(seq__31918_32082__$1);
var G__32085 = c__4550__auto___32083;
var G__32086 = cljs.core.count(c__4550__auto___32083);
var G__32087 = (0);
seq__31918_32059 = G__32084;
chunk__31919_32060 = G__32085;
count__31920_32061 = G__32086;
i__31921_32062 = G__32087;
continue;
} else {
var vec__31937_32088 = cljs.core.first(seq__31918_32082__$1);
var name_32089 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31937_32088,(0),null);
var map__31940_32090 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31937_32088,(1),null);
var map__31940_32091__$1 = (((((!((map__31940_32090 == null))))?(((((map__31940_32090.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31940_32090.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31940_32090):map__31940_32090);
var doc_32092 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31940_32091__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_32093 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31940_32091__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_32089], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_32093], 0));

if(cljs.core.truth_(doc_32092)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_32092], 0));
} else {
}


var G__32099 = cljs.core.next(seq__31918_32082__$1);
var G__32100 = null;
var G__32101 = (0);
var G__32102 = (0);
seq__31918_32059 = G__32099;
chunk__31919_32060 = G__32100;
count__31920_32061 = G__32101;
i__31921_32062 = G__32102;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5735__auto__ = cljs.spec.alpha.get_spec(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name(n)),cljs.core.name(nm)));
if(cljs.core.truth_(temp__5735__auto__)){
var fnspec = temp__5735__auto__;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));

var seq__31944 = cljs.core.seq(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__31945 = null;
var count__31946 = (0);
var i__31947 = (0);
while(true){
if((i__31947 < count__31946)){
var role = chunk__31945.cljs$core$IIndexed$_nth$arity$2(null,i__31947);
var temp__5735__auto___32104__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5735__auto___32104__$1)){
var spec_32105 = temp__5735__auto___32104__$1;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_32105)], 0));
} else {
}


var G__32106 = seq__31944;
var G__32107 = chunk__31945;
var G__32108 = count__31946;
var G__32109 = (i__31947 + (1));
seq__31944 = G__32106;
chunk__31945 = G__32107;
count__31946 = G__32108;
i__31947 = G__32109;
continue;
} else {
var temp__5735__auto____$1 = cljs.core.seq(seq__31944);
if(temp__5735__auto____$1){
var seq__31944__$1 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(seq__31944__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__31944__$1);
var G__32110 = cljs.core.chunk_rest(seq__31944__$1);
var G__32111 = c__4550__auto__;
var G__32112 = cljs.core.count(c__4550__auto__);
var G__32113 = (0);
seq__31944 = G__32110;
chunk__31945 = G__32111;
count__31946 = G__32112;
i__31947 = G__32113;
continue;
} else {
var role = cljs.core.first(seq__31944__$1);
var temp__5735__auto___32114__$2 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5735__auto___32114__$2)){
var spec_32116 = temp__5735__auto___32114__$2;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_32116)], 0));
} else {
}


var G__32119 = cljs.core.next(seq__31944__$1);
var G__32120 = null;
var G__32121 = (0);
var G__32122 = (0);
seq__31944 = G__32119;
chunk__31945 = G__32120;
count__31946 = G__32121;
i__31947 = G__32122;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Constructs a data representation for a Error with keys:
 *  :cause - root cause message
 *  :phase - error phase
 *  :via - cause chain, with cause keys:
 *           :type - exception class symbol
 *           :message - exception message
 *           :data - ex-data
 *           :at - top stack element
 *  :trace - root cause stack elements
 */
cljs.repl.Error__GT_map = (function cljs$repl$Error__GT_map(o){
var base = (function (t){
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),(((t instanceof cljs.core.ExceptionInfo))?new cljs.core.Symbol(null,"ExceptionInfo","ExceptionInfo",294935087,null):(((t instanceof EvalError))?new cljs.core.Symbol("js","EvalError","js/EvalError",1793498501,null):(((t instanceof RangeError))?new cljs.core.Symbol("js","RangeError","js/RangeError",1703848089,null):(((t instanceof ReferenceError))?new cljs.core.Symbol("js","ReferenceError","js/ReferenceError",-198403224,null):(((t instanceof SyntaxError))?new cljs.core.Symbol("js","SyntaxError","js/SyntaxError",-1527651665,null):(((t instanceof URIError))?new cljs.core.Symbol("js","URIError","js/URIError",505061350,null):(((t instanceof Error))?new cljs.core.Symbol("js","Error","js/Error",-1692659266,null):null
)))))))], null),(function (){var temp__5735__auto__ = cljs.core.ex_message(t);
if(cljs.core.truth_(temp__5735__auto__)){
var msg = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"message","message",-406056002),msg], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = cljs.core.ex_data(t);
if(cljs.core.truth_(temp__5735__auto__)){
var ed = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),ed], null);
} else {
return null;
}
})()], 0));
});
var via = (function (){var via = cljs.core.PersistentVector.EMPTY;
var t = o;
while(true){
if(cljs.core.truth_(t)){
var G__32127 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(via,t);
var G__32128 = cljs.core.ex_cause(t);
via = G__32127;
t = G__32128;
continue;
} else {
return via;
}
break;
}
})();
var root = cljs.core.peek(via);
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"via","via",-1904457336),cljs.core.vec(cljs.core.map.cljs$core$IFn$_invoke$arity$2(base,via)),new cljs.core.Keyword(null,"trace","trace",-1082747415),null], null),(function (){var temp__5735__auto__ = cljs.core.ex_message(root);
if(cljs.core.truth_(temp__5735__auto__)){
var root_msg = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cause","cause",231901252),root_msg], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = cljs.core.ex_data(root);
if(cljs.core.truth_(temp__5735__auto__)){
var data = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),data], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358).cljs$core$IFn$_invoke$arity$1(cljs.core.ex_data(o));
if(cljs.core.truth_(temp__5735__auto__)){
var phase = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"phase","phase",575722892),phase], null);
} else {
return null;
}
})()], 0));
});
/**
 * Returns an analysis of the phase, error, cause, and location of an error that occurred
 *   based on Throwable data, as returned by Throwable->map. All attributes other than phase
 *   are optional:
 *  :clojure.error/phase - keyword phase indicator, one of:
 *    :read-source :compile-syntax-check :compilation :macro-syntax-check :macroexpansion
 *    :execution :read-eval-result :print-eval-result
 *  :clojure.error/source - file name (no path)
 *  :clojure.error/line - integer line number
 *  :clojure.error/column - integer column number
 *  :clojure.error/symbol - symbol being expanded/compiled/invoked
 *  :clojure.error/class - cause exception class symbol
 *  :clojure.error/cause - cause exception message
 *  :clojure.error/spec - explain-data for spec error
 */
cljs.repl.ex_triage = (function cljs$repl$ex_triage(datafied_throwable){
var map__31953 = datafied_throwable;
var map__31953__$1 = (((((!((map__31953 == null))))?(((((map__31953.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31953.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31953):map__31953);
var via = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31953__$1,new cljs.core.Keyword(null,"via","via",-1904457336));
var trace = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31953__$1,new cljs.core.Keyword(null,"trace","trace",-1082747415));
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__31953__$1,new cljs.core.Keyword(null,"phase","phase",575722892),new cljs.core.Keyword(null,"execution","execution",253283524));
var map__31954 = cljs.core.last(via);
var map__31954__$1 = (((((!((map__31954 == null))))?(((((map__31954.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31954.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31954):map__31954);
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31954__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var message = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31954__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var data = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31954__$1,new cljs.core.Keyword(null,"data","data",-232669377));
var map__31955 = data;
var map__31955__$1 = (((((!((map__31955 == null))))?(((((map__31955.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31955.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31955):map__31955);
var problems = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31955__$1,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814));
var fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31955__$1,new cljs.core.Keyword("cljs.spec.alpha","fn","cljs.spec.alpha/fn",408600443));
var caller = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31955__$1,new cljs.core.Keyword("cljs.spec.test.alpha","caller","cljs.spec.test.alpha/caller",-398302390));
var map__31956 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.first(via));
var map__31956__$1 = (((((!((map__31956 == null))))?(((((map__31956.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31956.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31956):map__31956);
var top_data = map__31956__$1;
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31956__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3((function (){var G__31961 = phase;
var G__31961__$1 = (((G__31961 instanceof cljs.core.Keyword))?G__31961.fqn:null);
switch (G__31961__$1) {
case "read-source":
var map__31962 = data;
var map__31962__$1 = (((((!((map__31962 == null))))?(((((map__31962.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31962.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31962):map__31962);
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31962__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31962__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var G__31965 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.second(via)),top_data], 0));
var G__31965__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31965,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__31965);
var G__31965__$2 = (cljs.core.truth_((function (){var fexpr__31966 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__31966.cljs$core$IFn$_invoke$arity$1 ? fexpr__31966.cljs$core$IFn$_invoke$arity$1(source) : fexpr__31966.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__31965__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__31965__$1);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31965__$2,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__31965__$2;
}

break;
case "compile-syntax-check":
case "compilation":
case "macro-syntax-check":
case "macroexpansion":
var G__31967 = top_data;
var G__31967__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31967,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__31967);
var G__31967__$2 = (cljs.core.truth_((function (){var fexpr__31968 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__31968.cljs$core$IFn$_invoke$arity$1 ? fexpr__31968.cljs$core$IFn$_invoke$arity$1(source) : fexpr__31968.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__31967__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__31967__$1);
var G__31967__$3 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31967__$2,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__31967__$2);
var G__31967__$4 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31967__$3,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__31967__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31967__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__31967__$4;
}

break;
case "read-eval-result":
case "print-eval-result":
var vec__31972 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31972,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31972,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31972,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31972,(3),null);
var G__31979 = top_data;
var G__31979__$1 = (cljs.core.truth_(line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31979,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),line):G__31979);
var G__31979__$2 = (cljs.core.truth_(file)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31979__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file):G__31979__$1);
var G__31979__$3 = (cljs.core.truth_((function (){var and__4120__auto__ = source__$1;
if(cljs.core.truth_(and__4120__auto__)){
return method;
} else {
return and__4120__auto__;
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31979__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null))):G__31979__$2);
var G__31979__$4 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31979__$3,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__31979__$3);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31979__$4,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__31979__$4;
}

break;
case "execution":
var vec__31982 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31982,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31982,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31982,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31982,(3),null);
var file__$1 = cljs.core.first(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(((function (vec__31982,source__$1,method,file,line,G__31961,G__31961__$1,map__31953,map__31953__$1,via,trace,phase,map__31954,map__31954__$1,type,message,data,map__31955,map__31955__$1,problems,fn,caller,map__31956,map__31956__$1,top_data,source){
return (function (p1__31952_SHARP_){
var or__4131__auto__ = (p1__31952_SHARP_ == null);
if(or__4131__auto__){
return or__4131__auto__;
} else {
var fexpr__31986 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__31986.cljs$core$IFn$_invoke$arity$1 ? fexpr__31986.cljs$core$IFn$_invoke$arity$1(p1__31952_SHARP_) : fexpr__31986.call(null,p1__31952_SHARP_));
}
});})(vec__31982,source__$1,method,file,line,G__31961,G__31961__$1,map__31953,map__31953__$1,via,trace,phase,map__31954,map__31954__$1,type,message,data,map__31955,map__31955__$1,problems,fn,caller,map__31956,map__31956__$1,top_data,source))
,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(caller),file], null)));
var err_line = (function (){var or__4131__auto__ = new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(caller);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return line;
}
})();
var G__31987 = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type], null);
var G__31987__$1 = (cljs.core.truth_(err_line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31987,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),err_line):G__31987);
var G__31987__$2 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31987__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__31987__$1);
var G__31987__$3 = (cljs.core.truth_((function (){var or__4131__auto__ = fn;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
var and__4120__auto__ = source__$1;
if(cljs.core.truth_(and__4120__auto__)){
return method;
} else {
return and__4120__auto__;
}
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31987__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(function (){var or__4131__auto__ = fn;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null));
}
})()):G__31987__$2);
var G__31987__$4 = (cljs.core.truth_(file__$1)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31987__$3,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file__$1):G__31987__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__31987__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__31987__$4;
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__31961__$1)].join('')));

}
})(),new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358),phase);
});
/**
 * Returns a string from exception data, as produced by ex-triage.
 *   The first line summarizes the exception phase and location.
 *   The subsequent lines describe the cause.
 */
cljs.repl.ex_str = (function cljs$repl$ex_str(p__31993){
var map__31994 = p__31993;
var map__31994__$1 = (((((!((map__31994 == null))))?(((((map__31994.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31994.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31994):map__31994);
var triage_data = map__31994__$1;
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31994__$1,new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358));
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31994__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31994__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31994__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var symbol = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31994__$1,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994));
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31994__$1,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890));
var cause = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31994__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742));
var spec = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31994__$1,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595));
var loc = [cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4131__auto__ = source;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "<cljs repl>";
}
})()),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4131__auto__ = line;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return (1);
}
})()),(cljs.core.truth_(column)?[":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join(''):"")].join('');
var class_name = cljs.core.name((function (){var or__4131__auto__ = class$;
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "";
}
})());
var simple_class = class_name;
var cause_type = ((cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["RuntimeException",null,"Exception",null], null), null),simple_class))?"":[" (",simple_class,")"].join(''));
var format = goog.string.format;
var G__31999 = phase;
var G__31999__$1 = (((G__31999 instanceof cljs.core.Keyword))?G__31999.fqn:null);
switch (G__31999__$1) {
case "read-source":
return (format.cljs$core$IFn$_invoke$arity$3 ? format.cljs$core$IFn$_invoke$arity$3("Syntax error reading source at (%s).\n%s\n",loc,cause) : format.call(null,"Syntax error reading source at (%s).\n%s\n",loc,cause));

break;
case "macro-syntax-check":
var G__32000 = "Syntax error macroexpanding %sat (%s).\n%s";
var G__32001 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__32002 = loc;
var G__32003 = (cljs.core.truth_(spec)?(function (){var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__32004_32197 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__32005_32198 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__32006_32199 = true;
var _STAR_print_fn_STAR__temp_val__32007_32200 = ((function (_STAR_print_newline_STAR__orig_val__32004_32197,_STAR_print_fn_STAR__orig_val__32005_32198,_STAR_print_newline_STAR__temp_val__32006_32199,sb__4661__auto__,G__32000,G__32001,G__32002,G__31999,G__31999__$1,loc,class_name,simple_class,cause_type,format,map__31994,map__31994__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__32004_32197,_STAR_print_fn_STAR__orig_val__32005_32198,_STAR_print_newline_STAR__temp_val__32006_32199,sb__4661__auto__,G__32000,G__32001,G__32002,G__31999,G__31999__$1,loc,class_name,simple_class,cause_type,format,map__31994,map__31994__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__32006_32199;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__32007_32200;

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),((function (_STAR_print_newline_STAR__orig_val__32004_32197,_STAR_print_fn_STAR__orig_val__32005_32198,_STAR_print_newline_STAR__temp_val__32006_32199,_STAR_print_fn_STAR__temp_val__32007_32200,sb__4661__auto__,G__32000,G__32001,G__32002,G__31999,G__31999__$1,loc,class_name,simple_class,cause_type,format,map__31994,map__31994__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (_STAR_print_newline_STAR__orig_val__32004_32197,_STAR_print_fn_STAR__orig_val__32005_32198,_STAR_print_newline_STAR__temp_val__32006_32199,_STAR_print_fn_STAR__temp_val__32007_32200,sb__4661__auto__,G__32000,G__32001,G__32002,G__31999,G__31999__$1,loc,class_name,simple_class,cause_type,format,map__31994,map__31994__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (p1__31991_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__31991_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
});})(_STAR_print_newline_STAR__orig_val__32004_32197,_STAR_print_fn_STAR__orig_val__32005_32198,_STAR_print_newline_STAR__temp_val__32006_32199,_STAR_print_fn_STAR__temp_val__32007_32200,sb__4661__auto__,G__32000,G__32001,G__32002,G__31999,G__31999__$1,loc,class_name,simple_class,cause_type,format,map__31994,map__31994__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
,probs);
});})(_STAR_print_newline_STAR__orig_val__32004_32197,_STAR_print_fn_STAR__orig_val__32005_32198,_STAR_print_newline_STAR__temp_val__32006_32199,_STAR_print_fn_STAR__temp_val__32007_32200,sb__4661__auto__,G__32000,G__32001,G__32002,G__31999,G__31999__$1,loc,class_name,simple_class,cause_type,format,map__31994,map__31994__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
)
);
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__32005_32198;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__32004_32197;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
})():(format.cljs$core$IFn$_invoke$arity$2 ? format.cljs$core$IFn$_invoke$arity$2("%s\n",cause) : format.call(null,"%s\n",cause)));
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__32000,G__32001,G__32002,G__32003) : format.call(null,G__32000,G__32001,G__32002,G__32003));

break;
case "macroexpansion":
var G__32008 = "Unexpected error%s macroexpanding %sat (%s).\n%s\n";
var G__32009 = cause_type;
var G__32010 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__32011 = loc;
var G__32012 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__32008,G__32009,G__32010,G__32011,G__32012) : format.call(null,G__32008,G__32009,G__32010,G__32011,G__32012));

break;
case "compile-syntax-check":
var G__32013 = "Syntax error%s compiling %sat (%s).\n%s\n";
var G__32014 = cause_type;
var G__32015 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__32016 = loc;
var G__32017 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__32013,G__32014,G__32015,G__32016,G__32017) : format.call(null,G__32013,G__32014,G__32015,G__32016,G__32017));

break;
case "compilation":
var G__32018 = "Unexpected error%s compiling %sat (%s).\n%s\n";
var G__32019 = cause_type;
var G__32020 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__32021 = loc;
var G__32022 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__32018,G__32019,G__32020,G__32021,G__32022) : format.call(null,G__32018,G__32019,G__32020,G__32021,G__32022));

break;
case "read-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "print-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "execution":
if(cljs.core.truth_(spec)){
var G__32023 = "Execution error - invalid arguments to %s at (%s).\n%s";
var G__32024 = symbol;
var G__32025 = loc;
var G__32026 = (function (){var sb__4661__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__32027_32217 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__32028_32218 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__32029_32219 = true;
var _STAR_print_fn_STAR__temp_val__32030_32220 = ((function (_STAR_print_newline_STAR__orig_val__32027_32217,_STAR_print_fn_STAR__orig_val__32028_32218,_STAR_print_newline_STAR__temp_val__32029_32219,sb__4661__auto__,G__32023,G__32024,G__32025,G__31999,G__31999__$1,loc,class_name,simple_class,cause_type,format,map__31994,map__31994__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (x__4662__auto__){
return sb__4661__auto__.append(x__4662__auto__);
});})(_STAR_print_newline_STAR__orig_val__32027_32217,_STAR_print_fn_STAR__orig_val__32028_32218,_STAR_print_newline_STAR__temp_val__32029_32219,sb__4661__auto__,G__32023,G__32024,G__32025,G__31999,G__31999__$1,loc,class_name,simple_class,cause_type,format,map__31994,map__31994__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
;
cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__32029_32219;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__32030_32220;

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),((function (_STAR_print_newline_STAR__orig_val__32027_32217,_STAR_print_fn_STAR__orig_val__32028_32218,_STAR_print_newline_STAR__temp_val__32029_32219,_STAR_print_fn_STAR__temp_val__32030_32220,sb__4661__auto__,G__32023,G__32024,G__32025,G__31999,G__31999__$1,loc,class_name,simple_class,cause_type,format,map__31994,map__31994__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (_STAR_print_newline_STAR__orig_val__32027_32217,_STAR_print_fn_STAR__orig_val__32028_32218,_STAR_print_newline_STAR__temp_val__32029_32219,_STAR_print_fn_STAR__temp_val__32030_32220,sb__4661__auto__,G__32023,G__32024,G__32025,G__31999,G__31999__$1,loc,class_name,simple_class,cause_type,format,map__31994,map__31994__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec){
return (function (p1__31992_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__31992_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
});})(_STAR_print_newline_STAR__orig_val__32027_32217,_STAR_print_fn_STAR__orig_val__32028_32218,_STAR_print_newline_STAR__temp_val__32029_32219,_STAR_print_fn_STAR__temp_val__32030_32220,sb__4661__auto__,G__32023,G__32024,G__32025,G__31999,G__31999__$1,loc,class_name,simple_class,cause_type,format,map__31994,map__31994__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
,probs);
});})(_STAR_print_newline_STAR__orig_val__32027_32217,_STAR_print_fn_STAR__orig_val__32028_32218,_STAR_print_newline_STAR__temp_val__32029_32219,_STAR_print_fn_STAR__temp_val__32030_32220,sb__4661__auto__,G__32023,G__32024,G__32025,G__31999,G__31999__$1,loc,class_name,simple_class,cause_type,format,map__31994,map__31994__$1,triage_data,phase,source,line,column,symbol,class$,cause,spec))
)
);
}finally {cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__32028_32218;

cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__32027_32217;
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4661__auto__);
})();
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__32023,G__32024,G__32025,G__32026) : format.call(null,G__32023,G__32024,G__32025,G__32026));
} else {
var G__32031 = "Execution error%s at %s(%s).\n%s\n";
var G__32032 = cause_type;
var G__32033 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__32034 = loc;
var G__32035 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__32031,G__32032,G__32033,G__32034,G__32035) : format.call(null,G__32031,G__32032,G__32033,G__32034,G__32035));
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__31999__$1)].join('')));

}
});
cljs.repl.error__GT_str = (function cljs$repl$error__GT_str(error){
return cljs.repl.ex_str(cljs.repl.ex_triage(cljs.repl.Error__GT_map(error)));
});

//# sourceMappingURL=cljs.repl.js.map
