goog.provide('shadow.cljs.devtools.client.browser');
goog.require('cljs.core');
goog.require('cljs.reader');
goog.require('clojure.string');
goog.require('goog.dom');
goog.require('goog.userAgent.product');
goog.require('goog.Uri');
goog.require('goog.net.XhrIo');
goog.require('shadow.cljs.devtools.client.env');
goog.require('shadow.cljs.devtools.client.console');
goog.require('shadow.cljs.devtools.client.hud');
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.active_modules_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.active_modules_ref = cljs.core.volatile_BANG_(cljs.core.PersistentHashSet.EMPTY);
}
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.repl_ns_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.repl_ns_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
shadow.cljs.devtools.client.browser.module_loaded = (function shadow$cljs$devtools$client$browser$module_loaded(name){
return shadow.cljs.devtools.client.browser.active_modules_ref.cljs$core$IVolatile$_vreset_BANG_$arity$2(null,cljs.core.conj.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.browser.active_modules_ref.cljs$core$IDeref$_deref$arity$1(null),cljs.core.keyword.cljs$core$IFn$_invoke$arity$1(name)));
});
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.socket_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.socket_ref = cljs.core.volatile_BANG_(null);
}
shadow.cljs.devtools.client.browser.devtools_msg = (function shadow$cljs$devtools$client$browser$devtools_msg(var_args){
var args__4736__auto__ = [];
var len__4730__auto___37810 = arguments.length;
var i__4731__auto___37811 = (0);
while(true){
if((i__4731__auto___37811 < len__4730__auto___37810)){
args__4736__auto__.push((arguments[i__4731__auto___37811]));

var G__37812 = (i__4731__auto___37811 + (1));
i__4731__auto___37811 = G__37812;
continue;
} else {
}
break;
}

var argseq__4737__auto__ = ((((1) < args__4736__auto__.length))?(new cljs.core.IndexedSeq(args__4736__auto__.slice((1)),(0),null)):null);
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4737__auto__);
});

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic = (function (msg,args){
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [["%cshadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join(''),"color: blue;"], null),args)));
});

shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$applyTo = (function (seq37524){
var G__37525 = cljs.core.first(seq37524);
var seq37524__$1 = cljs.core.next(seq37524);
var self__4717__auto__ = this;
return self__4717__auto__.cljs$core$IFn$_invoke$arity$variadic(G__37525,seq37524__$1);
});

shadow.cljs.devtools.client.browser.ws_msg = (function shadow$cljs$devtools$client$browser$ws_msg(msg){
var temp__5733__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5733__auto__)){
var s = temp__5733__auto__;
return s.send(cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0)));
} else {
return console.warn("WEBSOCKET NOT CONNECTED",cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0)));
}
});
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.scripts_to_load !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.scripts_to_load = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentVector.EMPTY);
}
shadow.cljs.devtools.client.browser.loaded_QMARK_ = goog.isProvided_;
shadow.cljs.devtools.client.browser.goog_is_loaded_QMARK_ = (function shadow$cljs$devtools$client$browser$goog_is_loaded_QMARK_(name){
return $CLJS.SHADOW_ENV.isLoaded(name);
});
shadow.cljs.devtools.client.browser.goog_base_rc = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("shadow.build.classpath","resource","shadow.build.classpath/resource",-879517823),"goog/base.js"], null);
shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_ = (function shadow$cljs$devtools$client$browser$src_is_loaded_QMARK_(p__37568){
var map__37569 = p__37568;
var map__37569__$1 = (((((!((map__37569 == null))))?(((((map__37569.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37569.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37569):map__37569);
var src = map__37569__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37569__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37569__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var or__4131__auto__ = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.browser.goog_base_rc,resource_id);
if(or__4131__auto__){
return or__4131__auto__;
} else {
return shadow.cljs.devtools.client.browser.goog_is_loaded_QMARK_(output_name);
}
});
shadow.cljs.devtools.client.browser.module_is_active_QMARK_ = (function shadow$cljs$devtools$client$browser$module_is_active_QMARK_(module){
return cljs.core.contains_QMARK_(cljs.core.deref(shadow.cljs.devtools.client.browser.active_modules_ref),module);
});
shadow.cljs.devtools.client.browser.script_eval = (function shadow$cljs$devtools$client$browser$script_eval(code){
return goog.globalEval(code);
});
shadow.cljs.devtools.client.browser.do_js_load = (function shadow$cljs$devtools$client$browser$do_js_load(sources){
var seq__37580 = cljs.core.seq(sources);
var chunk__37581 = null;
var count__37582 = (0);
var i__37583 = (0);
while(true){
if((i__37583 < count__37582)){
var map__37600 = chunk__37581.cljs$core$IIndexed$_nth$arity$2(null,i__37583);
var map__37600__$1 = (((((!((map__37600 == null))))?(((((map__37600.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37600.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37600):map__37600);
var src = map__37600__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37600__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37600__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37600__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37600__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''));
}catch (e37602){var e_37813 = e37602;
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_37813);

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_37813.message)].join('')));
}

var G__37814 = seq__37580;
var G__37815 = chunk__37581;
var G__37816 = count__37582;
var G__37817 = (i__37583 + (1));
seq__37580 = G__37814;
chunk__37581 = G__37815;
count__37582 = G__37816;
i__37583 = G__37817;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__37580);
if(temp__5735__auto__){
var seq__37580__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__37580__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__37580__$1);
var G__37818 = cljs.core.chunk_rest(seq__37580__$1);
var G__37819 = c__4550__auto__;
var G__37820 = cljs.core.count(c__4550__auto__);
var G__37821 = (0);
seq__37580 = G__37818;
chunk__37581 = G__37819;
count__37582 = G__37820;
i__37583 = G__37821;
continue;
} else {
var map__37605 = cljs.core.first(seq__37580__$1);
var map__37605__$1 = (((((!((map__37605 == null))))?(((((map__37605.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37605.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37605):map__37605);
var src = map__37605__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37605__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37605__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37605__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37605__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''));
}catch (e37609){var e_37822 = e37609;
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_37822);

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_37822.message)].join('')));
}

var G__37823 = cljs.core.next(seq__37580__$1);
var G__37824 = null;
var G__37825 = (0);
var G__37826 = (0);
seq__37580 = G__37823;
chunk__37581 = G__37824;
count__37582 = G__37825;
i__37583 = G__37826;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.do_js_reload = (function shadow$cljs$devtools$client$browser$do_js_reload(msg,sources,complete_fn,failure_fn){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(msg,new cljs.core.Keyword(null,"log-missing-fn","log-missing-fn",732676765),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["can't find fn ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"log-call-async","log-call-async",183826192),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call async ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),new cljs.core.Keyword(null,"log-call","log-call",412404391),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
})], 0)),(function (){
return shadow.cljs.devtools.client.browser.do_js_load(sources);
}),complete_fn,failure_fn);
});
/**
 * when (require '["some-str" :as x]) is done at the REPL we need to manually call the shadow.js.require for it
 * since the file only adds the shadow$provide. only need to do this for shadow-js.
 */
shadow.cljs.devtools.client.browser.do_js_requires = (function shadow$cljs$devtools$client$browser$do_js_requires(js_requires){
var seq__37612 = cljs.core.seq(js_requires);
var chunk__37613 = null;
var count__37614 = (0);
var i__37615 = (0);
while(true){
if((i__37615 < count__37614)){
var js_ns = chunk__37613.cljs$core$IIndexed$_nth$arity$2(null,i__37615);
var require_str_37827 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_37827);


var G__37828 = seq__37612;
var G__37829 = chunk__37613;
var G__37830 = count__37614;
var G__37831 = (i__37615 + (1));
seq__37612 = G__37828;
chunk__37613 = G__37829;
count__37614 = G__37830;
i__37615 = G__37831;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__37612);
if(temp__5735__auto__){
var seq__37612__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__37612__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__37612__$1);
var G__37832 = cljs.core.chunk_rest(seq__37612__$1);
var G__37833 = c__4550__auto__;
var G__37834 = cljs.core.count(c__4550__auto__);
var G__37835 = (0);
seq__37612 = G__37832;
chunk__37613 = G__37833;
count__37614 = G__37834;
i__37615 = G__37835;
continue;
} else {
var js_ns = cljs.core.first(seq__37612__$1);
var require_str_37836 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_37836);


var G__37837 = cljs.core.next(seq__37612__$1);
var G__37838 = null;
var G__37839 = (0);
var G__37840 = (0);
seq__37612 = G__37837;
chunk__37613 = G__37838;
count__37614 = G__37839;
i__37615 = G__37840;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.load_sources = (function shadow$cljs$devtools$client$browser$load_sources(sources,callback){
if(cljs.core.empty_QMARK_(sources)){
var G__37622 = cljs.core.PersistentVector.EMPTY;
return (callback.cljs$core$IFn$_invoke$arity$1 ? callback.cljs$core$IFn$_invoke$arity$1(G__37622) : callback.call(null,G__37622));
} else {
var G__37623 = shadow.cljs.devtools.client.env.files_url();
var G__37624 = ((function (G__37623){
return (function (res){
var req = this;
var content = cljs.reader.read_string.cljs$core$IFn$_invoke$arity$1(req.getResponseText());
return (callback.cljs$core$IFn$_invoke$arity$1 ? callback.cljs$core$IFn$_invoke$arity$1(content) : callback.call(null,content));
});})(G__37623))
;
var G__37625 = "POST";
var G__37626 = cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"client","client",-1323448117),new cljs.core.Keyword(null,"browser","browser",828191719),new cljs.core.Keyword(null,"sources","sources",-321166424),cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582)),sources)], null)], 0));
var G__37627 = ({"content-type": "application/edn; charset=utf-8"});
return goog.net.XhrIo.send(G__37623,G__37624,G__37625,G__37626,G__37627);
}
});
shadow.cljs.devtools.client.browser.handle_build_complete = (function shadow$cljs$devtools$client$browser$handle_build_complete(p__37631){
var map__37632 = p__37631;
var map__37632__$1 = (((((!((map__37632 == null))))?(((((map__37632.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37632.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37632):map__37632);
var msg = map__37632__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37632__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37632__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var map__37634 = info;
var map__37634__$1 = (((((!((map__37634 == null))))?(((((map__37634.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37634.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37634):map__37634);
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37634__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var compiled = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37634__$1,new cljs.core.Keyword(null,"compiled","compiled",850043082));
var warnings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.distinct.cljs$core$IFn$_invoke$arity$1((function (){var iter__4523__auto__ = ((function (map__37634,map__37634__$1,sources,compiled,map__37632,map__37632__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__37636(s__37637){
return (new cljs.core.LazySeq(null,((function (map__37634,map__37634__$1,sources,compiled,map__37632,map__37632__$1,msg,info,reload_info){
return (function (){
var s__37637__$1 = s__37637;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__37637__$1);
if(temp__5735__auto__){
var xs__6292__auto__ = temp__5735__auto__;
var map__37642 = cljs.core.first(xs__6292__auto__);
var map__37642__$1 = (((((!((map__37642 == null))))?(((((map__37642.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37642.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37642):map__37642);
var src = map__37642__$1;
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37642__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37642__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))){
var iterys__4519__auto__ = ((function (s__37637__$1,map__37642,map__37642__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__37634,map__37634__$1,sources,compiled,map__37632,map__37632__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__37636_$_iter__37638(s__37639){
return (new cljs.core.LazySeq(null,((function (s__37637__$1,map__37642,map__37642__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__37634,map__37634__$1,sources,compiled,map__37632,map__37632__$1,msg,info,reload_info){
return (function (){
var s__37639__$1 = s__37639;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__37639__$1);
if(temp__5735__auto____$1){
var s__37639__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__37639__$2)){
var c__4521__auto__ = cljs.core.chunk_first(s__37639__$2);
var size__4522__auto__ = cljs.core.count(c__4521__auto__);
var b__37641 = cljs.core.chunk_buffer(size__4522__auto__);
if((function (){var i__37640 = (0);
while(true){
if((i__37640 < size__4522__auto__)){
var warning = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4521__auto__,i__37640);
cljs.core.chunk_append(b__37641,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name));

var G__37841 = (i__37640 + (1));
i__37640 = G__37841;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__37641),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__37636_$_iter__37638(cljs.core.chunk_rest(s__37639__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__37641),null);
}
} else {
var warning = cljs.core.first(s__37639__$2);
return cljs.core.cons(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__37636_$_iter__37638(cljs.core.rest(s__37639__$2)));
}
} else {
return null;
}
break;
}
});})(s__37637__$1,map__37642,map__37642__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__37634,map__37634__$1,sources,compiled,map__37632,map__37632__$1,msg,info,reload_info))
,null,null));
});})(s__37637__$1,map__37642,map__37642__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__37634,map__37634__$1,sources,compiled,map__37632,map__37632__$1,msg,info,reload_info))
;
var fs__4520__auto__ = cljs.core.seq(iterys__4519__auto__(warnings));
if(fs__4520__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4520__auto__,shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__37636(cljs.core.rest(s__37637__$1)));
} else {
var G__37842 = cljs.core.rest(s__37637__$1);
s__37637__$1 = G__37842;
continue;
}
} else {
var G__37843 = cljs.core.rest(s__37637__$1);
s__37637__$1 = G__37843;
continue;
}
} else {
return null;
}
break;
}
});})(map__37634,map__37634__$1,sources,compiled,map__37632,map__37632__$1,msg,info,reload_info))
,null,null));
});})(map__37634,map__37634__$1,sources,compiled,map__37632,map__37632__$1,msg,info,reload_info))
;
return iter__4523__auto__(sources);
})()));
var seq__37644_37844 = cljs.core.seq(warnings);
var chunk__37645_37845 = null;
var count__37646_37846 = (0);
var i__37647_37847 = (0);
while(true){
if((i__37647_37847 < count__37646_37846)){
var map__37657_37848 = chunk__37645_37845.cljs$core$IIndexed$_nth$arity$2(null,i__37647_37847);
var map__37657_37849__$1 = (((((!((map__37657_37848 == null))))?(((((map__37657_37848.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37657_37848.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37657_37848):map__37657_37848);
var w_37850 = map__37657_37849__$1;
var msg_37851__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37657_37849__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_37852 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37657_37849__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_37853 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37657_37849__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_37854 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37657_37849__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_37854)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_37852),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_37853),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_37851__$1)].join(''));


var G__37855 = seq__37644_37844;
var G__37856 = chunk__37645_37845;
var G__37857 = count__37646_37846;
var G__37858 = (i__37647_37847 + (1));
seq__37644_37844 = G__37855;
chunk__37645_37845 = G__37856;
count__37646_37846 = G__37857;
i__37647_37847 = G__37858;
continue;
} else {
var temp__5735__auto___37859 = cljs.core.seq(seq__37644_37844);
if(temp__5735__auto___37859){
var seq__37644_37860__$1 = temp__5735__auto___37859;
if(cljs.core.chunked_seq_QMARK_(seq__37644_37860__$1)){
var c__4550__auto___37861 = cljs.core.chunk_first(seq__37644_37860__$1);
var G__37862 = cljs.core.chunk_rest(seq__37644_37860__$1);
var G__37863 = c__4550__auto___37861;
var G__37864 = cljs.core.count(c__4550__auto___37861);
var G__37865 = (0);
seq__37644_37844 = G__37862;
chunk__37645_37845 = G__37863;
count__37646_37846 = G__37864;
i__37647_37847 = G__37865;
continue;
} else {
var map__37667_37866 = cljs.core.first(seq__37644_37860__$1);
var map__37667_37867__$1 = (((((!((map__37667_37866 == null))))?(((((map__37667_37866.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37667_37866.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37667_37866):map__37667_37866);
var w_37868 = map__37667_37867__$1;
var msg_37869__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37667_37867__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_37870 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37667_37867__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_37871 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37667_37867__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_37872 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37667_37867__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_37872)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_37870),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_37871),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_37869__$1)].join(''));


var G__37873 = cljs.core.next(seq__37644_37860__$1);
var G__37874 = null;
var G__37875 = (0);
var G__37876 = (0);
seq__37644_37844 = G__37873;
chunk__37645_37845 = G__37874;
count__37646_37846 = G__37875;
i__37647_37847 = G__37876;
continue;
}
} else {
}
}
break;
}

if((!(shadow.cljs.devtools.client.env.autoload))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(((cljs.core.empty_QMARK_(warnings)) || (shadow.cljs.devtools.client.env.ignore_warnings))){
var sources_to_get = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.filter.cljs$core$IFn$_invoke$arity$2(((function (map__37634,map__37634__$1,sources,compiled,warnings,map__37632,map__37632__$1,msg,info,reload_info){
return (function (p__37670){
var map__37671 = p__37670;
var map__37671__$1 = (((((!((map__37671 == null))))?(((((map__37671.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37671.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37671):map__37671);
var src = map__37671__$1;
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37671__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37671__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
return ((cljs.core.contains_QMARK_(new cljs.core.Keyword(null,"always-load","always-load",66405637).cljs$core$IFn$_invoke$arity$1(reload_info),ns)) || (cljs.core.not(shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_(src))) || (((cljs.core.contains_QMARK_(compiled,resource_id)) && (cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))))));
});})(map__37634,map__37634__$1,sources,compiled,warnings,map__37632,map__37632__$1,msg,info,reload_info))
,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(((function (map__37634,map__37634__$1,sources,compiled,warnings,map__37632,map__37632__$1,msg,info,reload_info){
return (function (p__37673){
var map__37674 = p__37673;
var map__37674__$1 = (((((!((map__37674 == null))))?(((((map__37674.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37674.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37674):map__37674);
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37674__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
return cljs.core.contains_QMARK_(new cljs.core.Keyword(null,"never-load","never-load",1300896819).cljs$core$IFn$_invoke$arity$1(reload_info),ns);
});})(map__37634,map__37634__$1,sources,compiled,warnings,map__37632,map__37632__$1,msg,info,reload_info))
,cljs.core.filter.cljs$core$IFn$_invoke$arity$2(((function (map__37634,map__37634__$1,sources,compiled,warnings,map__37632,map__37632__$1,msg,info,reload_info){
return (function (p__37676){
var map__37677 = p__37676;
var map__37677__$1 = (((((!((map__37677 == null))))?(((((map__37677.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37677.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37677):map__37677);
var rc = map__37677__$1;
var module = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37677__$1,new cljs.core.Keyword(null,"module","module",1424618191));
return ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("js",shadow.cljs.devtools.client.env.module_format)) || (shadow.cljs.devtools.client.browser.module_is_active_QMARK_(module)));
});})(map__37634,map__37634__$1,sources,compiled,warnings,map__37632,map__37632__$1,msg,info,reload_info))
,sources))));
if(cljs.core.not(cljs.core.seq(sources_to_get))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"after-load","after-load",-1278503285)], null)))){
} else {
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("reloading code but no :after-load hooks are configured!",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["https://shadow-cljs.github.io/docs/UsersGuide.html#_lifecycle_hooks"], 0));
}

return shadow.cljs.devtools.client.browser.load_sources(sources_to_get,((function (sources_to_get,map__37634,map__37634__$1,sources,compiled,warnings,map__37632,map__37632__$1,msg,info,reload_info){
return (function (p1__37630_SHARP_){
return shadow.cljs.devtools.client.browser.do_js_reload(msg,p1__37630_SHARP_,shadow.cljs.devtools.client.hud.load_end_success,shadow.cljs.devtools.client.hud.load_failure);
});})(sources_to_get,map__37634,map__37634__$1,sources,compiled,warnings,map__37632,map__37632__$1,msg,info,reload_info))
);
}
} else {
return null;
}
}
});
shadow.cljs.devtools.client.browser.page_load_uri = (cljs.core.truth_(goog.global.document)?goog.Uri.parse(document.location.href):null);
shadow.cljs.devtools.client.browser.match_paths = (function shadow$cljs$devtools$client$browser$match_paths(old,new$){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("file",shadow.cljs.devtools.client.browser.page_load_uri.getScheme())){
var rel_new = cljs.core.subs.cljs$core$IFn$_invoke$arity$2(new$,(1));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(old,rel_new)) || (clojure.string.starts_with_QMARK_(old,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(rel_new),"?"].join(''))))){
return rel_new;
} else {
return null;
}
} else {
var node_uri = goog.Uri.parse(old);
var node_uri_resolved = shadow.cljs.devtools.client.browser.page_load_uri.resolve(node_uri);
var node_abs = node_uri_resolved.getPath();
var and__4120__auto__ = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.browser.page_load_uri.hasSameDomainAs(node_uri))) || (cljs.core.not(node_uri.hasDomain())));
if(and__4120__auto__){
var and__4120__auto____$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(node_abs,new$);
if(and__4120__auto____$1){
return new$;
} else {
return and__4120__auto____$1;
}
} else {
return and__4120__auto__;
}
}
});
shadow.cljs.devtools.client.browser.handle_asset_watch = (function shadow$cljs$devtools$client$browser$handle_asset_watch(p__37683){
var map__37684 = p__37683;
var map__37684__$1 = (((((!((map__37684 == null))))?(((((map__37684.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37684.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37684):map__37684);
var msg = map__37684__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37684__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
var seq__37686 = cljs.core.seq(updates);
var chunk__37688 = null;
var count__37689 = (0);
var i__37690 = (0);
while(true){
if((i__37690 < count__37689)){
var path = chunk__37688.cljs$core$IIndexed$_nth$arity$2(null,i__37690);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__37727_37877 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__37730_37878 = null;
var count__37731_37879 = (0);
var i__37732_37880 = (0);
while(true){
if((i__37732_37880 < count__37731_37879)){
var node_37881 = chunk__37730_37878.cljs$core$IIndexed$_nth$arity$2(null,i__37732_37880);
var path_match_37882 = shadow.cljs.devtools.client.browser.match_paths(node_37881.getAttribute("href"),path);
if(cljs.core.truth_(path_match_37882)){
var new_link_37883 = (function (){var G__37739 = node_37881.cloneNode(true);
G__37739.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_37882),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__37739;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_37882], 0));

goog.dom.insertSiblingAfter(new_link_37883,node_37881);

goog.dom.removeNode(node_37881);


var G__37884 = seq__37727_37877;
var G__37885 = chunk__37730_37878;
var G__37886 = count__37731_37879;
var G__37887 = (i__37732_37880 + (1));
seq__37727_37877 = G__37884;
chunk__37730_37878 = G__37885;
count__37731_37879 = G__37886;
i__37732_37880 = G__37887;
continue;
} else {
var G__37888 = seq__37727_37877;
var G__37889 = chunk__37730_37878;
var G__37890 = count__37731_37879;
var G__37891 = (i__37732_37880 + (1));
seq__37727_37877 = G__37888;
chunk__37730_37878 = G__37889;
count__37731_37879 = G__37890;
i__37732_37880 = G__37891;
continue;
}
} else {
var temp__5735__auto___37892 = cljs.core.seq(seq__37727_37877);
if(temp__5735__auto___37892){
var seq__37727_37893__$1 = temp__5735__auto___37892;
if(cljs.core.chunked_seq_QMARK_(seq__37727_37893__$1)){
var c__4550__auto___37894 = cljs.core.chunk_first(seq__37727_37893__$1);
var G__37895 = cljs.core.chunk_rest(seq__37727_37893__$1);
var G__37896 = c__4550__auto___37894;
var G__37897 = cljs.core.count(c__4550__auto___37894);
var G__37898 = (0);
seq__37727_37877 = G__37895;
chunk__37730_37878 = G__37896;
count__37731_37879 = G__37897;
i__37732_37880 = G__37898;
continue;
} else {
var node_37899 = cljs.core.first(seq__37727_37893__$1);
var path_match_37900 = shadow.cljs.devtools.client.browser.match_paths(node_37899.getAttribute("href"),path);
if(cljs.core.truth_(path_match_37900)){
var new_link_37901 = (function (){var G__37740 = node_37899.cloneNode(true);
G__37740.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_37900),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__37740;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_37900], 0));

goog.dom.insertSiblingAfter(new_link_37901,node_37899);

goog.dom.removeNode(node_37899);


var G__37902 = cljs.core.next(seq__37727_37893__$1);
var G__37903 = null;
var G__37904 = (0);
var G__37905 = (0);
seq__37727_37877 = G__37902;
chunk__37730_37878 = G__37903;
count__37731_37879 = G__37904;
i__37732_37880 = G__37905;
continue;
} else {
var G__37906 = cljs.core.next(seq__37727_37893__$1);
var G__37907 = null;
var G__37908 = (0);
var G__37909 = (0);
seq__37727_37877 = G__37906;
chunk__37730_37878 = G__37907;
count__37731_37879 = G__37908;
i__37732_37880 = G__37909;
continue;
}
}
} else {
}
}
break;
}


var G__37910 = seq__37686;
var G__37911 = chunk__37688;
var G__37912 = count__37689;
var G__37913 = (i__37690 + (1));
seq__37686 = G__37910;
chunk__37688 = G__37911;
count__37689 = G__37912;
i__37690 = G__37913;
continue;
} else {
var G__37914 = seq__37686;
var G__37915 = chunk__37688;
var G__37916 = count__37689;
var G__37917 = (i__37690 + (1));
seq__37686 = G__37914;
chunk__37688 = G__37915;
count__37689 = G__37916;
i__37690 = G__37917;
continue;
}
} else {
var temp__5735__auto__ = cljs.core.seq(seq__37686);
if(temp__5735__auto__){
var seq__37686__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__37686__$1)){
var c__4550__auto__ = cljs.core.chunk_first(seq__37686__$1);
var G__37918 = cljs.core.chunk_rest(seq__37686__$1);
var G__37919 = c__4550__auto__;
var G__37920 = cljs.core.count(c__4550__auto__);
var G__37921 = (0);
seq__37686 = G__37918;
chunk__37688 = G__37919;
count__37689 = G__37920;
i__37690 = G__37921;
continue;
} else {
var path = cljs.core.first(seq__37686__$1);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__37743_37922 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__37746_37923 = null;
var count__37747_37924 = (0);
var i__37748_37925 = (0);
while(true){
if((i__37748_37925 < count__37747_37924)){
var node_37926 = chunk__37746_37923.cljs$core$IIndexed$_nth$arity$2(null,i__37748_37925);
var path_match_37927 = shadow.cljs.devtools.client.browser.match_paths(node_37926.getAttribute("href"),path);
if(cljs.core.truth_(path_match_37927)){
var new_link_37928 = (function (){var G__37753 = node_37926.cloneNode(true);
G__37753.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_37927),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__37753;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_37927], 0));

goog.dom.insertSiblingAfter(new_link_37928,node_37926);

goog.dom.removeNode(node_37926);


var G__37929 = seq__37743_37922;
var G__37930 = chunk__37746_37923;
var G__37931 = count__37747_37924;
var G__37932 = (i__37748_37925 + (1));
seq__37743_37922 = G__37929;
chunk__37746_37923 = G__37930;
count__37747_37924 = G__37931;
i__37748_37925 = G__37932;
continue;
} else {
var G__37933 = seq__37743_37922;
var G__37934 = chunk__37746_37923;
var G__37935 = count__37747_37924;
var G__37936 = (i__37748_37925 + (1));
seq__37743_37922 = G__37933;
chunk__37746_37923 = G__37934;
count__37747_37924 = G__37935;
i__37748_37925 = G__37936;
continue;
}
} else {
var temp__5735__auto___37937__$1 = cljs.core.seq(seq__37743_37922);
if(temp__5735__auto___37937__$1){
var seq__37743_37938__$1 = temp__5735__auto___37937__$1;
if(cljs.core.chunked_seq_QMARK_(seq__37743_37938__$1)){
var c__4550__auto___37939 = cljs.core.chunk_first(seq__37743_37938__$1);
var G__37940 = cljs.core.chunk_rest(seq__37743_37938__$1);
var G__37941 = c__4550__auto___37939;
var G__37942 = cljs.core.count(c__4550__auto___37939);
var G__37943 = (0);
seq__37743_37922 = G__37940;
chunk__37746_37923 = G__37941;
count__37747_37924 = G__37942;
i__37748_37925 = G__37943;
continue;
} else {
var node_37944 = cljs.core.first(seq__37743_37938__$1);
var path_match_37945 = shadow.cljs.devtools.client.browser.match_paths(node_37944.getAttribute("href"),path);
if(cljs.core.truth_(path_match_37945)){
var new_link_37946 = (function (){var G__37758 = node_37944.cloneNode(true);
G__37758.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_37945),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__37758;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_37945], 0));

goog.dom.insertSiblingAfter(new_link_37946,node_37944);

goog.dom.removeNode(node_37944);


var G__37947 = cljs.core.next(seq__37743_37938__$1);
var G__37948 = null;
var G__37949 = (0);
var G__37950 = (0);
seq__37743_37922 = G__37947;
chunk__37746_37923 = G__37948;
count__37747_37924 = G__37949;
i__37748_37925 = G__37950;
continue;
} else {
var G__37951 = cljs.core.next(seq__37743_37938__$1);
var G__37952 = null;
var G__37953 = (0);
var G__37954 = (0);
seq__37743_37922 = G__37951;
chunk__37746_37923 = G__37952;
count__37747_37924 = G__37953;
i__37748_37925 = G__37954;
continue;
}
}
} else {
}
}
break;
}


var G__37955 = cljs.core.next(seq__37686__$1);
var G__37956 = null;
var G__37957 = (0);
var G__37958 = (0);
seq__37686 = G__37955;
chunk__37688 = G__37956;
count__37689 = G__37957;
i__37690 = G__37958;
continue;
} else {
var G__37959 = cljs.core.next(seq__37686__$1);
var G__37960 = null;
var G__37961 = (0);
var G__37962 = (0);
seq__37686 = G__37959;
chunk__37688 = G__37960;
count__37689 = G__37961;
i__37690 = G__37962;
continue;
}
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.get_ua_product = (function shadow$cljs$devtools$client$browser$get_ua_product(){
if(cljs.core.truth_(goog.userAgent.product.SAFARI)){
return new cljs.core.Keyword(null,"safari","safari",497115653);
} else {
if(cljs.core.truth_(goog.userAgent.product.CHROME)){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.FIREFOX)){
return new cljs.core.Keyword(null,"firefox","firefox",1283768880);
} else {
if(cljs.core.truth_(goog.userAgent.product.IE)){
return new cljs.core.Keyword(null,"ie","ie",2038473780);
} else {
return null;
}
}
}
}
});
shadow.cljs.devtools.client.browser.get_asset_root = (function shadow$cljs$devtools$client$browser$get_asset_root(){
var loc = (new goog.Uri(document.location.href));
var cbp = (new goog.Uri(CLOSURE_BASE_PATH));
var s = loc.resolve(cbp).toString();
return clojure.string.replace(s,/^file:\//,"file:///");
});
shadow.cljs.devtools.client.browser.repl_error = (function shadow$cljs$devtools$client$browser$repl_error(e){
console.error("repl/invoke error",e);

return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(shadow.cljs.devtools.client.env.repl_error(e),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),shadow.cljs.devtools.client.browser.get_ua_product(),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"asset-root","asset-root",1771735072),shadow.cljs.devtools.client.browser.get_asset_root()], 0));
});
shadow.cljs.devtools.client.browser.global_eval = (function shadow$cljs$devtools$client$browser$global_eval(js){
return (0,eval)(js);;
});
shadow.cljs.devtools.client.browser.repl_invoke = (function shadow$cljs$devtools$client$browser$repl_invoke(p__37762){
var map__37763 = p__37762;
var map__37763__$1 = (((((!((map__37763 == null))))?(((((map__37763.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37763.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37763):map__37763);
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37763__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37763__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var result = shadow.cljs.devtools.client.env.repl_call(((function (map__37763,map__37763__$1,id,js){
return (function (){
return shadow.cljs.devtools.client.browser.global_eval(js);
});})(map__37763,map__37763__$1,id,js))
,shadow.cljs.devtools.client.browser.repl_error);
return shadow.cljs.devtools.client.browser.ws_msg(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(result,new cljs.core.Keyword(null,"id","id",-1388402092),id));
});
shadow.cljs.devtools.client.browser.repl_require = (function shadow$cljs$devtools$client$browser$repl_require(p__37766,done){
var map__37767 = p__37766;
var map__37767__$1 = (((((!((map__37767 == null))))?(((((map__37767.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37767.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37767):map__37767);
var msg = map__37767__$1;
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37767__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37767__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37767__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var js_requires = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37767__$1,new cljs.core.Keyword(null,"js-requires","js-requires",-1311472051));
var sources_to_load = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(((function (map__37767,map__37767__$1,msg,id,sources,reload_namespaces,js_requires){
return (function (p__37769){
var map__37770 = p__37769;
var map__37770__$1 = (((((!((map__37770 == null))))?(((((map__37770.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37770.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37770):map__37770);
var src = map__37770__$1;
var provides = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37770__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var and__4120__auto__ = shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_(src);
if(cljs.core.truth_(and__4120__auto__)){
return cljs.core.not(cljs.core.some(reload_namespaces,provides));
} else {
return and__4120__auto__;
}
});})(map__37767,map__37767__$1,msg,id,sources,reload_namespaces,js_requires))
,sources));
return shadow.cljs.devtools.client.browser.load_sources(sources_to_load,((function (sources_to_load,map__37767,map__37767__$1,msg,id,sources,reload_namespaces,js_requires){
return (function (sources__$1){
try{shadow.cljs.devtools.client.browser.do_js_load(sources__$1);

if(cljs.core.seq(js_requires)){
shadow.cljs.devtools.client.browser.do_js_requires(js_requires);
} else {
}

return shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","require-complete","repl/require-complete",-2140254719),new cljs.core.Keyword(null,"id","id",-1388402092),id], null));
}catch (e37772){var e = e37772;
return shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","require-error","repl/require-error",1689310021),new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"error","error",-978969032),e.message], null));
}finally {(done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}});})(sources_to_load,map__37767,map__37767__$1,msg,id,sources,reload_namespaces,js_requires))
);
});
shadow.cljs.devtools.client.browser.repl_init = (function shadow$cljs$devtools$client$browser$repl_init(p__37774,done){
var map__37775 = p__37774;
var map__37775__$1 = (((((!((map__37775 == null))))?(((((map__37775.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37775.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37775):map__37775);
var repl_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37775__$1,new cljs.core.Keyword(null,"repl-state","repl-state",-1733780387));
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37775__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
return shadow.cljs.devtools.client.browser.load_sources(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535).cljs$core$IFn$_invoke$arity$1(repl_state))),((function (map__37775,map__37775__$1,repl_state,id){
return (function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","init-complete","repl/init-complete",-162252879),new cljs.core.Keyword(null,"id","id",-1388402092),id], null));

shadow.cljs.devtools.client.browser.devtools_msg("REPL session start successful");

return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
});})(map__37775,map__37775__$1,repl_state,id))
);
});
shadow.cljs.devtools.client.browser.repl_set_ns = (function shadow$cljs$devtools$client$browser$repl_set_ns(p__37778){
var map__37780 = p__37778;
var map__37780__$1 = (((((!((map__37780 == null))))?(((((map__37780.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37780.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37780):map__37780);
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37780__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37780__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
return shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","set-ns-complete","repl/set-ns-complete",680944662),new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"ns","ns",441598760),ns], null));
});
shadow.cljs.devtools.client.browser.close_reason_ref = cljs.core.volatile_BANG_(null);
shadow.cljs.devtools.client.browser.handle_message = (function shadow$cljs$devtools$client$browser$handle_message(p__37787,done){
var map__37788 = p__37787;
var map__37788__$1 = (((((!((map__37788 == null))))?(((((map__37788.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37788.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37788):map__37788);
var msg = map__37788__$1;
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37788__$1,new cljs.core.Keyword(null,"type","type",1174270348));
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

var G__37790_37963 = type;
var G__37790_37964__$1 = (((G__37790_37963 instanceof cljs.core.Keyword))?G__37790_37963.fqn:null);
switch (G__37790_37964__$1) {
case "asset-watch":
shadow.cljs.devtools.client.browser.handle_asset_watch(msg);

break;
case "repl/invoke":
shadow.cljs.devtools.client.browser.repl_invoke(msg);

break;
case "repl/require":
shadow.cljs.devtools.client.browser.repl_require(msg,done);

break;
case "repl/set-ns":
shadow.cljs.devtools.client.browser.repl_set_ns(msg);

break;
case "repl/init":
shadow.cljs.devtools.client.browser.repl_init(msg,done);

break;
case "repl/session-start":
shadow.cljs.devtools.client.browser.repl_init(msg,done);

break;
case "build-complete":
shadow.cljs.devtools.client.hud.hud_warnings(msg);

shadow.cljs.devtools.client.browser.handle_build_complete(msg);

break;
case "build-failure":
shadow.cljs.devtools.client.hud.load_end();

shadow.cljs.devtools.client.hud.hud_error(msg);

break;
case "build-init":
shadow.cljs.devtools.client.hud.hud_warnings(msg);

break;
case "build-start":
shadow.cljs.devtools.client.hud.hud_hide();

shadow.cljs.devtools.client.hud.load_start();

break;
case "pong":

break;
case "client/stale":
cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.close_reason_ref,"Stale Client! You are not using the latest compilation output!");

break;
case "client/no-worker":
cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.close_reason_ref,["watch for build \"",shadow.cljs.devtools.client.env.build_id,"\" not running"].join(''));

break;
case "custom-msg":
shadow.cljs.devtools.client.env.publish_BANG_(new cljs.core.Keyword(null,"payload","payload",-383036092).cljs$core$IFn$_invoke$arity$1(msg));

break;
default:

}

if(cljs.core.contains_QMARK_(shadow.cljs.devtools.client.env.async_ops,type)){
return null;
} else {
return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}
});
shadow.cljs.devtools.client.browser.compile = (function shadow$cljs$devtools$client$browser$compile(text,callback){
var G__37797 = ["http",((shadow.cljs.devtools.client.env.ssl)?"s":null),"://",shadow.cljs.devtools.client.env.server_host,":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.env.server_port),"/worker/compile/",shadow.cljs.devtools.client.env.build_id,"/",shadow.cljs.devtools.client.env.proc_id,"/browser"].join('');
var G__37798 = ((function (G__37797){
return (function (res){
var req = this;
var actions = cljs.reader.read_string.cljs$core$IFn$_invoke$arity$1(req.getResponseText());
if(cljs.core.truth_(callback)){
return (callback.cljs$core$IFn$_invoke$arity$1 ? callback.cljs$core$IFn$_invoke$arity$1(actions) : callback.call(null,actions));
} else {
return null;
}
});})(G__37797))
;
var G__37799 = "POST";
var G__37800 = cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"input","input",556931961),text], null)], 0));
var G__37801 = ({"content-type": "application/edn; charset=utf-8"});
return goog.net.XhrIo.send(G__37797,G__37798,G__37799,G__37800,G__37801);
});
shadow.cljs.devtools.client.browser.heartbeat_BANG_ = (function shadow$cljs$devtools$client$browser$heartbeat_BANG_(){
var temp__5735__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5735__auto__)){
var s = temp__5735__auto__;
s.send(cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"ping","ping",-1670114784),new cljs.core.Keyword(null,"v","v",21465059),Date.now()], null)], 0)));

return setTimeout(shadow.cljs.devtools.client.browser.heartbeat_BANG_,(30000));
} else {
return null;
}
});
shadow.cljs.devtools.client.browser.ws_connect = (function shadow$cljs$devtools$client$browser$ws_connect(){
try{var print_fn = cljs.core._STAR_print_fn_STAR_;
var ws_url = shadow.cljs.devtools.client.env.ws_url(new cljs.core.Keyword(null,"browser","browser",828191719));
var socket = (new WebSocket(ws_url));
cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.socket_ref,socket);

socket.onmessage = ((function (print_fn,ws_url,socket){
return (function (e){
return shadow.cljs.devtools.client.env.process_ws_msg(e.data,shadow.cljs.devtools.client.browser.handle_message);
});})(print_fn,ws_url,socket))
;

socket.onopen = ((function (print_fn,ws_url,socket){
return (function (e){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.close_reason_ref,null);

if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("goog",shadow.cljs.devtools.client.env.module_format)){
goog.provide = goog.constructNamespace_;
} else {
}

shadow.cljs.devtools.client.env.set_print_fns_BANG_(shadow.cljs.devtools.client.browser.ws_msg);

return shadow.cljs.devtools.client.browser.devtools_msg("WebSocket connected!");
});})(print_fn,ws_url,socket))
;

socket.onclose = ((function (print_fn,ws_url,socket){
return (function (e){
shadow.cljs.devtools.client.browser.devtools_msg("WebSocket disconnected!");

shadow.cljs.devtools.client.hud.connection_error((function (){var or__4131__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.close_reason_ref);
if(cljs.core.truth_(or__4131__auto__)){
return or__4131__auto__;
} else {
return "Connection closed!";
}
})());

cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.socket_ref,null);

return shadow.cljs.devtools.client.env.reset_print_fns_BANG_();
});})(print_fn,ws_url,socket))
;

socket.onerror = ((function (print_fn,ws_url,socket){
return (function (e){
shadow.cljs.devtools.client.hud.connection_error("Connection failed!");

return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("websocket error",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([e], 0));
});})(print_fn,ws_url,socket))
;

return setTimeout(shadow.cljs.devtools.client.browser.heartbeat_BANG_,(30000));
}catch (e37805){var e = e37805;
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("WebSocket setup failed",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([e], 0));
}});
if(shadow.cljs.devtools.client.env.enabled){
var temp__5735__auto___37966 = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5735__auto___37966)){
var s_37967 = temp__5735__auto___37966;
shadow.cljs.devtools.client.browser.devtools_msg("connection reset!");

s_37967.onclose = ((function (s_37967,temp__5735__auto___37966){
return (function (e){
return null;
});})(s_37967,temp__5735__auto___37966))
;

s_37967.close();

cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.socket_ref,null);
} else {
}

window.addEventListener("beforeunload",(function (){
var temp__5735__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5735__auto__)){
var s = temp__5735__auto__;
return s.close();
} else {
return null;
}
}));

if(cljs.core.truth_((function (){var and__4120__auto__ = document;
if(cljs.core.truth_(and__4120__auto__)){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("loading",document.readyState);
} else {
return and__4120__auto__;
}
})())){
window.addEventListener("DOMContentLoaded",shadow.cljs.devtools.client.browser.ws_connect);
} else {
setTimeout(shadow.cljs.devtools.client.browser.ws_connect,(10));
}
} else {
}

//# sourceMappingURL=shadow.cljs.devtools.client.browser.js.map
